'use strict';

module.exports = function(config) {

  config.set({
    basePath : '..',

    frameworks: ['jasmine'],

    autoWatch : false,

    files : [
      // dependencies
      'bower_components/jquery/dist/jquery.js',
      'bower_components/angular/angular.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'bower_components/angular-animate/angular-animate.js',
      'bower_components/angular-touch/angular-touch.js',
      'bower_components/angular-sanitize/angular-sanitize.js',
      'bower_components/angular-ui-router/release/angular-ui-router.js',
      'bower_components/moment/moment.js',
      'bower_components/angular-moment/angular-moment.js',
      'bower_components/bootstrap-daterangepicker/daterangepicker.js',
      'bower_components/angular-strap/dist/angular-strap.js',
      'bower_components/angular-strap/dist/angular-strap.tpl.js',
      'bower_components/angular-off-click/dist/angular-off-click.js',
      'bower_components/underscore/underscore.js',
      'bower_components/angular-underscore/angular-underscore.js',
      'bower_components/angular-local-storage/dist/angular-local-storage.js',
      'bower_components/jquery-ui/jquery-ui.js',
      'bower_components/angular-ui-sortable/sortable.js',
      'bower_components/chartist/dist/chartist.min.js',
      'bower_components/highcharts/highcharts.js',
      'bower_components/highcharts/highcharts-more.js',
      'bower_components/highcharts-ng/dist/highcharts-ng.js',
      'bower_components/FileSaver/FileSaver.js',
      'bower_components/leaflet/dist/leaflet.js',
      'bower_components/angular-http-batcher/dist/angular-http-batch.js',
      'bower_components/angular-translate/angular-translate.js',
      'bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
      'bower_components/perfect-scrollbar/src/perfect-scrollbar.js',
      'bower_components/angular-perfect-scrollbar/src/angular-perfect-scrollbar.js',
      'bower_components/angular-data-table/release/dataTable.js',
      'bower_components/ng-file-upload/ng-file-upload.js',
      'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.min.js',
      'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
      'bower_components/angularjs-dragula/dist/angular-dragula.js',
      'bower_components/angular-slugify/angular-slugify.js',

      // mock file
      'test/mock/multi-location-kpi-fetcher.service.mock.js',

      // project files
      'src/app/retailmanager.module.js',
      'src/app/main.controller.js',
      'src/app/main.controller.spec.js',
      'src/app/retailmanager.constants.js',

      'src/l10n/translateProvider.js',
      'src/components/**/*.module.js',
      'src/app/**/*.module.js',

      'src/**/*.js',
      'src/**/*.!(module).js',
      'src/**/*.*.*.js',

      // Other test data
      'test/fixture/**/*.json'
    ],

    browsers : ['PhantomJS'],

    debugInfoEnabled:true,

    batchEnabled:true,

    plugins : [
      'karma-phantomjs-launcher',
      'karma-jasmine',
      'karma-ng-json2js-preprocessor',
      'karma-coverage'
    ],

    preprocessors: {
      '**/*.json': ['ng-json2js'],
      'src/**/!(*spec).js': 'coverage'
    },

    reporters: [
      'progress', 'coverage'
    ],

    coverageReporter: {
      type: 'html',
      dir: 'coverage/'
    },

    ngJson2JsPreprocessor: {
      /*
        creates new ng module (based on name) from .json files
        e.g. '/test/fixture/example.json'
        creates
          http://localhost:9876/base/test/fixture/example.json.js
        which contains:
          angular.module('served/example.json', []).constant('served/example', { data: 'example data' });
        To use this in your tests:
          var myData = angular.injector(['ng', 'served/examples.json']).get('served/examples').data;
      */

      // strip this from the file path
      stripPrefix: 'test/fixture/',
      // prepend this to the path
      prependPrefix: 'served/'
    },

    // increase timeout settings
    // https://github.com/karma-runner/karma-phantomjs-launcher/issues/126
    browserNoActivityTimeout: 60000,
    browserDisconnectTimeout: 30000,
    captureTimeout: 60000
  });
};
