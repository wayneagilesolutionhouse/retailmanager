'use strict';

module.exports = {

  mondayUser: { // used for testing Monday-based calendars and custom periods
    userName: 'e2e-org-tester',
    password: 'Able-Crack-5',

    locale: 'en_US',
    numberFormat: {
      thousandsSeparator: ',',
      decimalSeparator: '.'
    },
    dateFormat: 'MM/DD/YYYY',
    calendar: 'Standard Monday Weeks',
    comparePd1Text: 'Custom period 1',
    comparePd2Text: 'Custom period 2',

    comparePd1WeeksBack: 2,
    comparePd2WeeksBack: 4,
    weatherOptions: {
      enabled: false
    }
  },

  superUser: { // used for majority of tests
    userName: 'e2e-tester',
    password: 'Feast-Bribery-2',

    locale: 'en_US',
    numberFormat: {
      thousandsSeparator: '.',
      decimalSeparator: ','
    },
    dateFormat: 'D.M.YYYY',
    calendar: 'Standard Monthly',
    comparePd1Text: 'Prior period',
    comparePd2Text: 'Prior year',
    weatherOptions: {
      enabled: true,
      windSpeedUnits: 'KPH',
      tempUnits: 'C'
    }
  },

  testLanguageUsers: { // used for testing translations
    es_MX: {
      defaultPd: {// Mexican Spanish, prior pd/prior yr
        userName: 'e2e-test-lang-es-mx',
        password: 'Steer-Dry-Bath-8',

        locale: 'es_MX',
        numberFormat: {
          thousandsSeparator: ',',
          decimalSeparator: '.'
        },
        dateFormat: 'MM/DD/YYYY',
        calendar: 'Standard Monthly',
        comparePd1Text: 'Prior period',
        comparePd2Text: 'Prior year',
        weatherOptions: {
          enabled: true,
          windSpeedUnits: 'KPH',
          tempUnits: '°C'
        }
      },

      customPd: { // Mexican Spanish, custom 2- and 4-weeks back
        userName: 'e2e-test-lang-es-mx-custom',
        password: 'Prompt-Fill-Hide-4',

        locale: 'es_MX',
        numberFormat: {
          thousandsSeparator: ',',
          decimalSeparator: '.'
        },
        dateFormat: 'MM/DD/YYYY',
        calendar: 'Standard Monthly',
        comparePd1Text: 'Custom period 1',
        comparePd2Text: 'Custom period 2',

        comparePd1WeeksBack: 2,
        comparePd2WeeksBack: 4,
        weatherOptions: {
          enabled: true,
          windSpeedUnits: 'KPH',
          tempUnits: '°C'
        }
      }
    }
    // other language users to follow
  }

};
