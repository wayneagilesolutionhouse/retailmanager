'use strict';

describe('Org navigation', function() {
  var orgSummarySharedTests = require('../../shared-specs/org-summary-shared.spec.js');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var orgPage = require('../../pages/org-summary-page.js');
  var nav = require('../../pages/components/nav-header.js');
  var dateSelector = require('../../pages/components/time-period-picker.js');

  beforeAll(function (done) {
    if (browser.params.indirectNav){
      login.go();
      login.loginAsSuperUser();
      nav.pickMSOrg();
      dateSelector.clickYearButton();
      done();
    } else {
      login.getToken(function (token) {
        browser.get('#/' + orgData.MSOrg.id + '/summary?dateRangeStart=' + dateSelector.getURLDate('year', true) +
          '&dateRangeEnd=' + dateSelector.getURLDate('year', false) + '&compareRange1Start=' + dateSelector.getURLDate('year', true, 1) +
          '&compareRange1End=' + dateSelector.getURLDate('year', false, 1) + '&compareRange2Start=' + dateSelector.getURLDate('year', true, 2) +
          '&compareRange2End=' + dateSelector.getURLDate('year', false, 2) + '&token=' + token);
        done();
      });
    }
  });


  describe('when selected reporting period is "year"', function() {

    orgSummarySharedTests.orgSummarySharedTests('year');

  });

  afterAll(function() {
    nav.logout();
  });
});
