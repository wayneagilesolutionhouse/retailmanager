'use strict';

describe('Org navigation', function() {
  var orgSummarySharedTests = require('../../shared-specs/org-summary-shared.spec.js');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var orgPage = require('../../pages/org-summary-page.js');
  var nav = require('../../pages/components/nav-header.js');
  var dateSelector = require('../../pages/components/time-period-picker.js');

  beforeAll(function (done) {
    if (browser.params.indirectNav){
      nav.pickMSOrg();
      done();
    } else {
      login.getToken(function(token) {
        browser.get('#/' + orgData.MSOrg.id + '/summary?dateRangeStart=' + dateSelector.getURLDate('week', true) +
          '&dateRangeEnd=' + dateSelector.getURLDate('week', false) + '&compareRange1Start=' + dateSelector.getURLDate('week', true, 1) +
          '&compareRange1End=' + dateSelector.getURLDate('week', false, 1) + '&compareRange2Start=' + dateSelector.getURLDate('week', true, 2) +
          '&compareRange2End=' + dateSelector.getURLDate('week', false, 2) + '&token=' + token);
        done();
      });
    }
  });

  describe('when selected reporting period is "week"', function () {

    orgSummarySharedTests.orgSummarySharedTests('week');

  });

  afterAll(function () {
    nav.logout();
  });

});
