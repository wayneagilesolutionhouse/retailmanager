'use strict';

describe('Org navigation', function() {
  var orgSummarySharedTests = require('../../shared-specs/org-summary-shared.spec.js');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var orgPage = require('../../pages/org-summary-page.js');
  var nav = require('../../pages/components/nav-header.js');
  var dateSelector = require('../../pages/components/time-period-picker.js');


  beforeAll(function (done) {
    if (browser.params.indirectNav){
      login.go();
      login.loginAsSuperUser();
      nav.pickMSOrg();
      dateSelector.clickMonthButton();
      done();
    } else {
      login.getToken(function(token) {
        browser.get('#/' + orgData.MSOrg.id + '/summary?dateRangeStart=' + dateSelector.getURLDate('month', true) +
          '&dateRangeEnd=' + dateSelector.getURLDate('month', false) + '&compareRange1Start=' + dateSelector.getURLDate('month', true, 1) +
          '&compareRange1End=' + dateSelector.getURLDate('month', false, 1) + '&compareRange2Start=' + dateSelector.getURLDate('month', true, 2) +
          '&compareRange2End=' + dateSelector.getURLDate('month', false, 2) + '&token=' + token);
        done();
      });
    }
  });

  describe('when selected reporting period is "month"', function () {

    orgSummarySharedTests.orgSummarySharedTests('month');

  });

  afterAll(function () {
    nav.logout();
  });

});
