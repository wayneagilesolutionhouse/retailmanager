'use strict';

describe('Translations in usage of areas tab', function() {

  var usageOfAreasSharedLangTests = require('../../shared-specs/usage-of-areas-shared-translations.spec.js');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var nav = require('../../pages/components/nav-header.js');
  var sitePage = require('../../pages/site-summary-page.js');
  var tabNav = require('../../pages/components/tab-nav.js');
  var dateSelector = require('../../pages/components/time-period-picker.js');

  var locale = 'es_MX';

  beforeAll(function (done) {
    if (browser.params.indirectNav){
      login.go();
      login.loginAsTestLangUser(locale);
      nav.pickSSOrg();
      tabNav.navToUsageOfAreasTab();
      done();
    } else {
      login.getTestLangUserToken(function(token) {
        browser.get('#/' + orgData.SSOrg.id + '/' + orgData.SSOrg.ssOrgSiteId + '/usage-of-areas?dateRangeStart=' +
          dateSelector.getURLDate('week', true) + '&dateRangeEnd=' + dateSelector.getURLDate('week', false) + '&compareRange1Start=' +
          dateSelector.getURLDate('week', true, 1) + '&compareRange1End=' + dateSelector.getURLDate('week', false, 1) + '&compareRange2Start=' +
          dateSelector.getURLDate('week', true, 2) + '&compareRange2End=' + dateSelector.getURLDate('week', false, 2)
          +'&token='+token);
        done();
      }, locale, false);
    }
  });

  describe('when user\'s language is Mexican Spanish', function () {

    usageOfAreasSharedLangTests.usageOfAreasSharedLangTests(locale);

  });

  afterAll(function() {
    nav.logout();
  });

});

