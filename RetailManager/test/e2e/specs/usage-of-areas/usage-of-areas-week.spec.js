'use strict';

describe('Usage of areas tab', function() {

  var usageOfAreasSharedTests = require('../../shared-specs/usage-of-areas-shared.spec.js');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var nav = require('../../pages/components/nav-header.js');
  var sitePage = require('../../pages/site-summary-page.js');
  var tabNav = require('../../pages/components/tab-nav.js');
  var dateSelector = require('../../pages/components/time-period-picker.js');

  beforeAll(function (done) {
    if (browser.params.indirectNav){
      login.go();
      login.loginAsSuperUser();
      nav.pickSSOrg();
      tabNav.navToUsageOfAreasTab();
      done();
    } else {
      login.getToken(function(token) {
        browser.get('#/' + orgData.SSOrg.id + '/' + orgData.SSOrg.ssOrgSiteId + '/usage-of-areas?dateRangeStart=' +
          dateSelector.getURLDate('week', true) + '&dateRangeEnd=' + dateSelector.getURLDate('week', false) + '&compareRange1Start=' +
          dateSelector.getURLDate('week', true, 1) + '&compareRange1End=' + dateSelector.getURLDate('week', false, 1) + '&compareRange2Start=' +
          dateSelector.getURLDate('week', true, 2) + '&compareRange2End=' + dateSelector.getURLDate('week', false, 2)
          + '&token=' + token);
        done();
      });
    }
  });

  describe('when selected reporting period is "week"', function () {

    usageOfAreasSharedTests.usageOfAreasSharedTests('week');

  });

  afterAll(function() {
    nav.logout();
  });

});

