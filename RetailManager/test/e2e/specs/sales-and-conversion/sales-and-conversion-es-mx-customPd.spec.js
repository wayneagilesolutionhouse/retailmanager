'use strict';

describe('Translations in sales and conversion tab (custom date ranges):', function() {

  var moment = require('moment');
  var salesAndConversionSharedLangCustomPdTests = require('../../shared-specs/sales-and-conversion-shared-translations-customPd.spec.js');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var nav = require('../../pages/components/nav-header.js');
  var tabNav = require('../../pages/components/tab-nav.js');
  var dateSelector = require('../../pages/components/time-period-picker.js');

  var locale = 'es_MX';

  beforeAll(function (done) {
    if (browser.params.indirectNav){
      login.go();
      login.loginAsTestLangUser(locale, true);
      nav.pickMSOrg();
      nav.navToMSOrgSite();
      tabNav.navToSalesTab();
      done();
    } else {
      login.getTestLangUserToken(function(token) {
        browser.get('#/' + orgData.MSOrg.id + '/' + orgData.MSOrgSite.id + '/sales-and-conversion?dateRangeStart=' +
          dateSelector.getURLDate('week', true) + '&dateRangeEnd='+dateSelector.getURLDate('week', false)
          + '&token=' + token);
        done();
      }, locale, true);
    }
  });

  describe('when user\'s language is Mexican Spanish', function () {

    salesAndConversionSharedLangCustomPdTests.salesAndConversionSharedLangCustomPdTests(locale);

  });

  afterAll(function() {
    nav.logout();
  });

});
