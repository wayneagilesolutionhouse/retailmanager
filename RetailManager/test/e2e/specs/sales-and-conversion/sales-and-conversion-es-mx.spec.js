'use strict';

describe('Translations in sales and conversion tab:', function() {

  var moment = require('moment');
  var salesAndConversionSharedLangTests = require('../../shared-specs/sales-and-conversion-shared-translations.spec');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var nav = require('../../pages/components/nav-header.js');
  var tabNav = require('../../pages/components/tab-nav.js');
  var dateSelector = require('../../pages/components/time-period-picker.js');

  var locale = 'es_MX';

  beforeAll(function (done) {
    if (browser.params.indirectNav){
      login.go();
      login.loginAsTestLangUser(locale);
      nav.pickMSOrg();
      nav.navToMSOrgSite();
      tabNav.navToSalesTab();
      done();
    } else {
      login.getTestLangUserToken(function(token) {
        browser.get('#/' + orgData.MSOrg.id + '/' + orgData.MSOrgSite.id + '/sales-and-conversion?dateRangeStart=' +
          dateSelector.getURLDate('week', true) + '&dateRangeEnd=' + dateSelector.getURLDate('week', false) + '&compareRange1Start=' +
          dateSelector.getURLDate('week', true, 1) + '&compareRange1End=' + dateSelector.getURLDate('week', false, 1) + '&compareRange2Start=' +
          dateSelector.getURLDate('week', true, 2) + '&compareRange2End=' + dateSelector.getURLDate('week', false, 2)
          + '&token=' + token);
        done();
      }, locale, false);
    }
  });

  describe('when user\'s language is Mexican Spanish', function () {

    salesAndConversionSharedLangTests.salesAndConversionSharedLangTests(locale);

  });

  afterAll(function() {
    nav.logout();
  });

});
