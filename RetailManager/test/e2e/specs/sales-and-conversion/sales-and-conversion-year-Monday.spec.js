'use strict';

describe('Sales and conversion tab - Monday calendar:', function() {

  var salesAndConversionSharedTestsMonday = require('../../shared-specs/sales-and-conversion-shared-Monday.spec.js');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var nav = require('../../pages/components/nav-header.js');
  var sitePage = require('../../pages/site-summary-page.js');
  var tabNav = require('../../pages/components/tab-nav.js');
  var dateSelector = require('../../pages/components/time-period-picker.js');

  beforeAll(function () {
      login.go();
      login.loginAsMondayUser();
      nav.pickMSOrg();
      nav.navToMSOrgSite();
      tabNav.navToSalesTab();
    dateSelector.clickYearButton();
  });

  describe('when selected reporting period is "year"', function () {

    salesAndConversionSharedTestsMonday.salesAndConversionSharedTestsMonday('year');

  });

  afterAll(function() {
    nav.logout();
  });

});
