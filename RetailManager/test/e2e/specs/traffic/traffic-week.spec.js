'use strict';

describe('Traffic tab:', function() {

  var moment = require('moment');
  var trafficSharedTests = require('../../shared-specs/traffic-shared.spec.js');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var nav = require('../../pages/components/nav-header.js');
  var tabNav = require('../../pages/components/tab-nav.js');
  var dateSelector = require('../../pages/components/time-period-picker.js');

  beforeAll(function (done) {
    if (browser.params.indirectNav){
      login.go();
      login.loginAsSuperUser();
      nav.pickMSOrg();
      nav.navToMSOrgSite();
      tabNav.navToTrafficTab();
      done();
    } else {
      login.getToken(function(token) {
        browser.get('#/' + orgData.MSOrg.id + '/' + orgData.MSOrgSite.id + '/traffic?dateRangeStart=' + dateSelector.getURLDate('week', true) +
          '&dateRangeEnd=' + dateSelector.getURLDate('week', false) + '&compareRange1Start=' + dateSelector.getURLDate('week', true, 1) +
          '&compareRange1End=' + dateSelector.getURLDate('week', false, 1) + '&compareRange2Start=' + dateSelector.getURLDate('week', true, 2) +
          '&compareRange2End=' + dateSelector.getURLDate('week', false, 2)
          + '&token=' + token);
        done();
      });
    }
  });

  describe('when selected reporting period is "week"', function () {

    trafficSharedTests.trafficSharedTests('week');

  });

  afterAll(function() {
    nav.logout();
  });

});
