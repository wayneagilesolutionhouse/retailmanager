'use strict';

describe('Traffic tab - Monday calendar:', function() {

  var moment = require('moment');
  var trafficSharedTestsMonday = require('../../shared-specs/traffic-shared-Monday.spec.js');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var nav = require('../../pages/components/nav-header.js');
  var tabNav = require('../../pages/components/tab-nav.js');
  var dateSelector = require('../../pages/components/time-period-picker.js');

  beforeAll(function () {
      login.go();
      login.loginAsMondayUser();
      nav.pickMSOrg();
      nav.navToMSOrgSite();
      tabNav.navToTrafficTab();
    dateSelector.clickYearButton();
  });

  describe('when selected reporting period is "year"', function () {

    trafficSharedTestsMonday.trafficSharedTestsMonday('year');
  });

  afterAll(function() {
    nav.logout();
  });

});
