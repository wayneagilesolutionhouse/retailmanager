'use strict';

describe('Retail site traffic tab:', function() {

  var moment = require('moment');
  var retailTrafficSharedTests = require('../../shared-specs/retail-traffic-shared.spec.js');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var nav = require('../../pages/components/nav-header.js');
  var tabNav = require('../../pages/components/tab-nav.js');
  var dateSelector = require('../../pages/components/time-period-picker.js');

  beforeAll(function (done) {
    if (browser.params.indirectNav){
      login.go();
      login.loginAsSuperUser();
      nav.pickRetailOrg();
      nav.navToRetailOrgSite();
      tabNav.navToTrafficTab();
      dateSelector.clickYearButton();
      done();
    } else {
      login.getToken(function(token) {
        browser.get('#/' + orgData.MSRetailOrg.id + '/' + orgData.MSRetailSite.testSiteId + '/traffic?dateRangeStart=' + dateSelector.getURLDate('year', true) +
          '&dateRangeEnd=' + dateSelector.getURLDate('year', false) + '&compareRange1Start=' + dateSelector.getURLDate('year', true, 1) +
          '&compareRange1End=' + dateSelector.getURLDate('year', false, 1) + '&compareRange2Start=' + dateSelector.getURLDate('year', true, 2) +
          '&compareRange2End=' + dateSelector.getURLDate('year', false, 2) + '&token=' + token);
        done();
      });
    }
  });

  describe('when selected reporting period is "year"', function () {

    retailTrafficSharedTests.retailTrafficSharedTests('year');

  });

  afterAll(function() {
    nav.logout();
  });

});


