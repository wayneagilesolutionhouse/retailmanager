'use strict';

describe('Traffic tab:', function() {

  var moment = require('moment');
  var trafficSharedTests = require('../../shared-specs/traffic-shared.spec.js');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var nav = require('../../pages/components/nav-header.js');
  var tabNav = require('../../pages/components/tab-nav.js');
  var dateSelector = require('../../pages/components/time-period-picker.js');

  beforeAll(function (done) {
    if (browser.params.indirectNav){
      login.go();
      login.loginAsSuperUser();
      nav.pickMSOrg();
      nav.navToMSOrgSite();
      tabNav.navToTrafficTab();
      dateSelector.clickMonthButton();
      done();
    } else {
      login.getToken(function(token) {
        browser.get('#/' + orgData.MSOrg.id + '/' + orgData.MSOrgSite.id + '/traffic?dateRangeStart=' + dateSelector.getURLDate('month', true) +
          '&dateRangeEnd=' + dateSelector.getURLDate('month', false) + '&compareRange1Start=' + dateSelector.getURLDate('month', true, 1) +
          '&compareRange1End=' + dateSelector.getURLDate('month', false, 1) + '&compareRange2Start=' + dateSelector.getURLDate('month', true, 2) +
          '&compareRange2End=' + dateSelector.getURLDate('month', false, 2) + '&token=' + token);
        done();
      });
    }
  });

  describe('when selected reporting period is "month"', function () {

    trafficSharedTests.trafficSharedTests('month');

  });

  afterAll(function() {
    nav.logout();
  });

});

