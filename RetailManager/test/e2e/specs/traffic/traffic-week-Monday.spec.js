'use strict';

describe('Traffic tab - Monday calendar:', function() {

  var moment = require('moment');
  var trafficSharedTestsMonday = require('../../shared-specs/traffic-shared-Monday.spec.js');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var nav = require('../../pages/components/nav-header.js');
  var tabNav = require('../../pages/components/tab-nav.js');

  beforeAll(function () {
      login.go();
      login.loginAsMondayUser();
      nav.pickMSOrg();
      nav.navToMSOrgSite();
      tabNav.navToTrafficTab();
  });

  describe('when selected reporting period is "week"', function () {

    trafficSharedTestsMonday.trafficSharedTestsMonday('week');
  });

  afterAll(function() {
    nav.logout();
  });

});
