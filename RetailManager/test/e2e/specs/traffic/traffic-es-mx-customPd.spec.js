'use strict';

describe('Translations in traffic tab (custom date ranges):', function() {

  var moment = require('moment');
  var trafficSharedLangCustomPdTests = require('../../shared-specs/traffic-shared-translations-customPd.spec.js');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var nav = require('../../pages/components/nav-header.js');
  var tabNav = require('../../pages/components/tab-nav.js');
  var dateSelector = require('../../pages/components/time-period-picker.js');

  var locale = 'es_MX';

  beforeAll(function (done) {
    if (browser.params.indirectNav){
      login.go();
      login.loginAsTestLangUser(locale, true);
      nav.pickMSOrg();
      nav.navToMSOrgSite();
      tabNav.navToTrafficTab();
      done();
    } else {
      login.getTestLangUserToken(function(token) { //shortened URL forces app to take compare period ranges from user settings
        browser.get('#/' + orgData.MSOrg.id + '/'+orgData.MSOrgSite.id + '/traffic?dateRangeStart=' +
          dateSelector.getURLDate('week', true) + '&dateRangeEnd=' + dateSelector.getURLDate('week', false)
          + '&token=' + token);
        done();
      }, locale, true);
    }
  });

  describe('when user\'s language is Mexican Spanish', function () {

    trafficSharedLangCustomPdTests.trafficSharedLangCustomPdTests(locale);

  });

  afterAll(function() {
    nav.logout();
  });

});

