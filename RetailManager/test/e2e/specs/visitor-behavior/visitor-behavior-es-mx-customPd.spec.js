'use strict';

describe('Translations in visitor behavior tab (custom date ranges):', function() {

  var visitorBehaviorSharedLangCustomPdTests = require('../../shared-specs/visitor-behavior-shared-translations-customPd.spec.js');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var nav = require('../../pages/components/nav-header.js');
  var sitePage = require('../../pages/site-summary-page.js');
  var tabNav = require('../../pages/components/tab-nav.js');
  var dateSelector = require('../../pages/components/time-period-picker.js');

  var locale = 'es_MX';

  beforeAll(function (done) {
    if (browser.params.indirectNav){
      login.go();
      login.loginAsTestLangUser(locale, true);
      nav.pickSSOrg();
      tabNav.navToVisitorBehaviorTab();
      done();
    } else {
      login.getTestLangUserToken(function(token) {  //shortened URL forces app to take compare period ranges from user settings
        browser.get('#/' + orgData.SSOrg.id + '/' + orgData.SSOrg.ssOrgSiteId + '/visitor-behavior?dateRangeStart=' +
          dateSelector.getURLDate('week', true) + '&dateRangeEnd=' + dateSelector.getURLDate('week', false)
          + '&token=' + token);
        done();
      }, locale, true);
    }
  });

  describe('when user\'s language is Mexican Spanish', function () {

    visitorBehaviorSharedLangCustomPdTests.visitorBehaviorSharedLangCustomPdTests(locale);

  });

  afterAll(function() {
    nav.logout();
  });

});

