'use strict';

describe('Visitor behavior tab - Monday calendar:', function() {

  var visitorBehaviorSharedTestsMonday = require('../../shared-specs/visitor-behavior-shared-Monday.spec.js');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var nav = require('../../pages/components/nav-header.js');
  var sitePage = require('../../pages/site-summary-page.js');
  var tabNav = require('../../pages/components/tab-nav.js');
  var dateSelector = require('../../pages/components/time-period-picker.js');


  beforeAll(function () {
      login.go();
      login.loginAsMondayUser();
      nav.pickSSOrg();
      tabNav.navToVisitorBehaviorTab();
    dateSelector.clickMonthButton();
  });

  describe('when selected reporting period is "month"', function () {

    visitorBehaviorSharedTestsMonday.visitorBehaviorSharedTestsMonday('month');

  });

  afterAll(function() {
    nav.logout();
  });

});
