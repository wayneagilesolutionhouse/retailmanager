'use strict';

describe('Visitor behavior tab - Monday calendar:', function() {

  var visitorBehaviorSharedTestsMonday = require('../../shared-specs/visitor-behavior-shared-Monday.spec.js');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var nav = require('../../pages/components/nav-header.js');
  var sitePage = require('../../pages/site-summary-page.js');
  var tabNav = require('../../pages/components/tab-nav.js');


  beforeAll(function () {
      login.go();
      login.loginAsMondayUser();
      nav.pickSSOrg();
      tabNav.navToVisitorBehaviorTab();
  });

  describe('when selected reporting period is "week"', function () {

    visitorBehaviorSharedTestsMonday.visitorBehaviorSharedTestsMonday('week');

  });

  afterAll(function() {
    nav.logout();
  });

});
