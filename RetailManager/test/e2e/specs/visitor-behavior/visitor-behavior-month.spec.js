'use strict';

describe('Visitor behavior tab', function() {

  var visitorBehaviorSharedTests = require('../../shared-specs/visitor-behavior-shared.spec.js');
  var orgData = require('../../data/orgs.js');
  var login = require('../../pages/login.js');
  var nav = require('../../pages/components/nav-header.js');
  var sitePage = require('../../pages/site-summary-page.js');
  var tabNav = require('../../pages/components/tab-nav.js');
  var dateSelector = require('../../pages/components/time-period-picker.js');

  beforeAll(function (done) {
    if (browser.params.indirectNav){
      login.go();
      login.loginAsSuperUser();
      nav.pickSSOrg();
      tabNav.navToVisitorBehaviorTab();
      dateSelector.clickMonthButton();
      done();
    } else {
      login.getToken(function(token) {
        browser.get('#/' + orgData.SSOrg.id + '/' + orgData.SSOrg.ssOrgSiteId + '/visitor-behavior?dateRangeStart=' +
          dateSelector.getURLDate('month', true) + '&dateRangeEnd=' + dateSelector.getURLDate('month', false) + '&compareRange1Start=' +
          dateSelector.getURLDate('month', true, 1) + '&compareRange1End=' + dateSelector.getURLDate('month', false, 1) + '&compareRange2Start=' +
          dateSelector.getURLDate('month', true, 2) + '&compareRange2End=' + dateSelector.getURLDate('month', false, 2) + '&token=' + token);
        done();
      });
    }
  });

  describe('when selected reporting period is "month"', function () {

    visitorBehaviorSharedTests.visitorBehaviorSharedTests('month');

  });

  afterAll(function () {
    nav.logout();
  });

});
