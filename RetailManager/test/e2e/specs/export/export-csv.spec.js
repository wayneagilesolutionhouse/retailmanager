'use strict';

describe('Export CSV function:', function() {

  var moment = require('moment');
  var orgData = require('../../data/orgs.js');
  var userData = require('../../data/users.js');
  var login = require('../../pages/login.js');
  var nav = require('../../pages/components/nav-header.js');
  var dateSelector = require('../../pages/components/time-period-picker.js');
  var exportCSVPage = require('../../pages/components/export-csv.js');
  var fs = require('fs');

  let userId;
  let token;

  beforeAll(function (done) {
    if (browser.params.indirectNav) {
      login.go();
      login.loginAsSuperUser();
      nav.pickMSOrg();
      nav.navToMSOrgSite();
      tabNav.navToTrafficTab();
      done();
    } else {
      login.getUserWithToken(function (tokenAndId) {
        ({token, userId} = tokenAndId);
        browser.get('#/' + orgData.MSOrg.id + '/' + orgData.MSOrgSite.id + '/traffic?dateRangeStart=' +
          dateSelector.getURLDate('week', true) + '&dateRangeEnd=' + dateSelector.getURLDate('week', false) + '&compareRange1Start=' +
          dateSelector.getURLDate('week', true, 1) + '&compareRange1End=' + dateSelector.getURLDate('week', false, 1) + '&compareRange2Start=' +
          dateSelector.getURLDate('week', true, 2) + '&compareRange2End=' + dateSelector.getURLDate('week', false, 2) + '&token=' + token);
        done();
      }, userData.superUser);
    }
  });

  describe('Multi-site org', function () {

    it('date range on metric tab should match range on export page', function () {
      dateSelector.getReportingPeriod(userData.superUser.dateFormat).then(function (metricTabDate) {
        nav.navToExportCSV();
        var exportTabDate = dateSelector.getExportCSVReportingPeriod(userData.superUser.dateFormat);

        expect(exportTabDate).toEqual(metricTabDate);
      });
    });

    it('should show correct site name', function () {
      var siteName = exportCSVPage.getExportSiteName();
      expect(siteName).toEqual(orgData.MSOrgSite.name);
    });

    it('should show the correct metrics', function () {
      var metrics = exportCSVPage.getMetrics();

      expect(metrics.getText()).toEqual(orgData.MSOrgSite.metrics.csv);
    });

    it('each metric should be clickable', function () {
      var metrics = exportCSVPage.getMetrics();

      metrics.each(function (elm) {
        expect(elm.getAttribute('class')).not.toMatch('selected');
        elm.click();
        browser.waitForAngular();
        expect(elm.getAttribute('class')).toMatch('selected');
        elm.click();
        browser.waitForAngular();
        expect(elm.getAttribute('class')).not.toMatch('selected');
      });
    });

    it('the export buttons should be enabled when a metric and an area are selected, and disabled otherwise', function () {
      var exportButton = exportCSVPage.getExportButton();
      var scheduleReportButton = exportCSVPage.getScheduleReportButton();
      var metrics = exportCSVPage.getMetrics();

      expect(exportButton.getAttribute('disabled')).toMatch('true');
      expect(scheduleReportButton.getAttribute('disabled')).toMatch('true');

      metrics.then(function (array) {
        return array[0].click();
      });

      expect(exportButton.getAttribute('disabled')).toMatch('true');
      expect(scheduleReportButton.getAttribute('disabled')).toMatch('true');

      exportCSVPage.setAreaPickerLocation('zone', 'New Yorker');
      expect(exportButton.getAttribute('disabled')).toEqual(null);
      expect(scheduleReportButton.getAttribute('disabled')).toEqual(null);

      metrics.then(function (array) {  //revert state
        return array[0].click();
      });

      expect(exportButton.getAttribute('disabled')).toMatch('true');
      expect(scheduleReportButton.getAttribute('disabled')).toMatch('true');

      exportCSVPage.setAreaPickerLocation('zone', 'New Yorker');
    });

    //todo: This test will fail until Jira LFR-21 is fixed - right now tenant names do not exactly match names in orgData
    //it('the area/zone dropdown should be correctly populated', function() {
    // exportCSVPage.getAreaPicker().click();
    // var locationList = exportCSVPage.getAreaPickerList('zone');
    //
    //  orgData.MSOrgSite.getAllAreas().forEach(function(location){
    //    expect(locationList).toContain(location);
    //  });
    //  exportCSVPage.getAreaPicker().click();
    //});

    it('the search bar in the area/zone dropdown should function correctly', function () {
      var searchBar = exportCSVPage.getPickerSearchBar('zone');

      exportCSVPage.getAreaPicker().click();
      expect(exportCSVPage.getAreaPickerList('zone')).toContain('New Yorker 119905');
      searchBar.clear();

      searchBar.sendKeys('amusing test string');
      expect(exportCSVPage.getAreaPickerList('zone')).toEqual([]);
      searchBar.clear();

      searchBar.sendKeys('yorker');
      expect(exportCSVPage.getAreaPickerList('zone')).toContain('New Yorker 119905');
      expect(exportCSVPage.getAreaPickerList('zone')).not.toContain('Gap Ext 109522');
      searchBar.clear();

      exportCSVPage.getAreaPicker().click();  //revert state
    });

    it('"select all" button in area/zone dropdown should function correctly', function () {
      exportCSVPage.getAreaPicker().click();
      var locationListInMenu = exportCSVPage.getAreaPickerList('zone');

      locationListInMenu.then(function (menuList) {
        exportCSVPage.getSelectAllBtn('zone').click();
        exportCSVPage.getAreaPicker().click();
        var locationListOnPage = exportCSVPage.getSelectedAreaList('zone');

        locationListOnPage.then(function (pageList) {
          expect(menuList.sort()).toEqual(pageList.sort());
        });
      });

      exportCSVPage.getAreaPicker().click();     //revert state
      exportCSVPage.getSelectAllBtn('zone').click();   //revert state
      exportCSVPage.getAreaPicker().click();     //revert state

    });

    it('should export a CSV', function () {

      exportCSVPage.setAreaPickerLocation('zone', 'New Yorker');

      var metrics = exportCSVPage.getMetrics();
      metrics.then(function (array) {
        browser.executeScript('window.scrollTo(0,0);').then(function () {
          return array[0].click();
        });
      });

      // //group report metrics by week to make it simpler to check accuracy of file contents
      exportCSVPage.setExportGroupBy('Week');
      dateSelector.getExportCSVReportingPeriod(userData.superUser.dateFormat).then(function (exportTabDate) {
        browser.executeScript('window.scrollTo(0,0);').then(function () {
          exportCSVPage.exportCSV();
        });
        browser.waitForAngular();
        if (browser.params.localBrowser) { // different checks if testing locally vs testing remotely
          var fileName = browser.params.downloadPath + '/report-site.csv';
          var folderContents;

          browser.driver.wait(function () {
            // Wait until the file has been downloaded.
            // We need to wait thus as otherwise protractor has a nasty habit of
            // trying to do any following tests while the file is still being
            // downloaded and hasn't been moved to its final location.
            folderContents = fs.readdirSync(browser.params.downloadPath);
            var returnValue = false;
            folderContents.forEach(function (file) { //checks to make sure a CSV has been downloaded before proceeding
              if (file.indexOf('.csv') === file.length - 4) { //checks for file ending in .csv
                returnValue = true;
              }
            });
            return returnValue;
          }, 30000).then(function () {

            expect(fs.readFileSync(fileName, {encoding: 'utf8'})).toMatch( //checks file content to make sure headers and org/site/zone/date info are correct
              '"organization_name","site_name","zone_name","period_start_date","period_end_date","traffic"' + '\r\n' +
              '"' + orgData.MSOrg.name + '","' + orgData.MSOrgSite.name + '","' + orgData.MSOrgSite.testZone + '","' +
              moment(exportTabDate[0]).format(userData.superUser.dateFormat) + '","' + moment(exportTabDate[1]).format(userData.superUser.dateFormat) + '"'
            );
          });
        }
//todo: no check for successful download in remote case yet - need a download success/error statement (LFR-129)
      });
    });

    it('should schedule a report correctly', function () {

      var initialScheduledReports = exportCSVPage.getScheduledCSVs();

      initialScheduledReports.then(function (list) {
        expect(list.length).toBe(0);     //check that scheduled report list is initially empty, otherwise expectations below will fail
        if (list.length >0) {
          list.forEach(function () {
            exportCSVPage.removeScheduledCSV();
          });
        }
      });

      browser.executeScript('window.scrollTo(0,0);').then(function () {
        exportCSVPage.getScheduleReportButton().click();
      });

      exportCSVPage.scheduleTestCSV('Daily', orgData.MSOrgSite.name);

      browser.executeScript('window.scrollTo(0,0);').then(function () {
        exportCSVPage.getScheduleReportButton().click();
      });

      exportCSVPage.scheduleTestCSV('Weekly', orgData.MSOrgSite.name);

      browser.executeScript('window.scrollTo(0,0);').then(function () {
        exportCSVPage.getScheduleReportButton().click();
      });

      exportCSVPage.scheduleTestCSV('Monthly', orgData.MSOrgSite.name);

      browser.executeScript('window.scrollTo(0,0);').then(function () {
        exportCSVPage.getScheduleReportButton().click();
      });

      exportCSVPage.scheduleTestCSV('Yearly', orgData.MSOrgSite.name);

      var scheduledReportList = exportCSVPage.getScheduledCSVs();
      scheduledReportList.then(function (list) {
        expect(list[0].getText()).toMatch('Day');
        expect(list[1].getText()).toMatch('Week');
        expect(list[2].getText()).toMatch('Month');
        expect(list[3].getText()).toMatch('Year');


        list.forEach(function () {
          exportCSVPage.removeScheduledCSV();
          browser.waitForAngular();
        });

        initialScheduledReports.then(function (list) {
          expect(list.length).toBe(0);
        });

      });
    });

    afterAll(function () {
      if (browser.params.localBrowser) { //delete file to restore base state/avoid naming issues
        fs.unlinkSync(browser.params.downloadPath + '/report-site.csv');
        console.log('deleting ' + browser.params.downloadPath + '/report-site.csv');
      }
    });
  });

  describe('Single-site org', function () {

    beforeAll(function () {
      browser.get('#/' + orgData.SSOrg.id + '/' + orgData.SSOrg.ssOrgSiteId + '/traffic?dateRangeStart=' +
        dateSelector.getURLDate('week', true) + '&dateRangeEnd=' + dateSelector.getURLDate('week', false) +  '&compareRange1Start=' +
        dateSelector.getURLDate('week', true, 1) + '&compareRange1End=' + dateSelector.getURLDate('week', false, 1) + '&compareRange2Start=' +
        dateSelector.getURLDate('week', true, 2) + '&compareRange2End=' + dateSelector.getURLDate('week', false, 2) + '&token=' + token
      );
    });

    it('date range on metric tab should match range on export page', function () {
      dateSelector.getReportingPeriod(userData.superUser.dateFormat).then(function (metricTabDate) {
        nav.navToExportCSV();
        var exportTabDate = dateSelector.getExportCSVReportingPeriod(userData.superUser.dateFormat);

        expect(exportTabDate).toEqual(metricTabDate);
      });
    });

    it('should show correct site name', function () {
      var siteName = exportCSVPage.getExportSiteName();
      expect(siteName).toEqual(orgData.SSOrg.ssOrgSiteName);
      expect(nav.getSingleSiteName()).toEqual(orgData.SSOrg.ssOrgSiteName);
    });

    it('should show the correct metrics', function () {
      exportCSVPage.setMetricType('perimeter');
      var perimeterMetrics = exportCSVPage.getMetrics();
      expect(perimeterMetrics.getText()).toEqual(orgData.SSOrg.metrics.csv.perimeter);

      exportCSVPage.setMetricType('visitor behavior');
      var visitorBehaviorMetrics = exportCSVPage.getMetrics();
      expect(visitorBehaviorMetrics.getText()).toEqual(orgData.SSOrg.metrics.csv.visitorBehavior);
    });

    it('each metric should be clickable', function () {
      var metrics = exportCSVPage.getMetrics();

      metrics.each(function (elm) {
        expect(elm.getAttribute('class')).not.toMatch('selected');
        elm.click();
        browser.waitForAngular();
        expect(elm.getAttribute('class')).toMatch('selected');
        elm.click();
        browser.waitForAngular();
        expect(elm.getAttribute('class')).not.toMatch('selected');
      });
    });

    it('the export buttons should be enabled when a metric and an area are selected, and disabled otherwise', function () {
      var exportButton = exportCSVPage.getExportButton();
      var scheduleReportButton = exportCSVPage.getScheduleReportButton();
      var metrics = exportCSVPage.getMetrics();

      expect(exportButton.getAttribute('disabled')).toMatch('true');
      expect(scheduleReportButton.getAttribute('disabled')).toMatch('true');

      metrics.then(function (array) {
        browser.executeScript('window.scrollTo(0,0);').then(function () {
          return array[0].click();
        });
      });

      expect(exportButton.getAttribute('disabled')).toMatch('true');
      expect(scheduleReportButton.getAttribute('disabled')).toMatch('true');

      exportCSVPage.setAreaPickerLocation('area', 'Irish Pub');
      expect(exportButton.getAttribute('disabled')).toEqual(null);
      expect(scheduleReportButton.getAttribute('disabled')).toEqual(null);

      metrics.then(function (array) {  //revert state
        browser.executeScript('window.scrollTo(0,0);').then(function () {
          return array[0].click();
        });
      });

      expect(exportButton.getAttribute('disabled')).toMatch('true');
      expect(scheduleReportButton.getAttribute('disabled')).toMatch('true');

      exportCSVPage.setAreaPickerLocation('area', 'Irish Pub');
    });

    it('the area/zone dropdown should be correctly populated', function () {
      exportCSVPage.getAreaPicker().click();
      var locationList = exportCSVPage.getAreaPickerList('area');

      orgData.SSOrg.getAllAreas().forEach(function (location) {
        expect(locationList).toContain(location);
      });
      exportCSVPage.getAreaPicker().click();
    });

    it('the search bar in the area/zone dropdown should function correctly', function () {
      var searchBar = exportCSVPage.getPickerSearchBar('area');

      exportCSVPage.getAreaPicker().click();
      expect(exportCSVPage.getAreaPickerList('area')).toContain('B1-128 - Ri Ra Irish Pub');
      searchBar.clear();

      searchBar.sendKeys('amusing test string');
      expect(exportCSVPage.getAreaPickerList('area')).toEqual([]);
      searchBar.clear();

      searchBar.sendKeys('irish');
      expect(exportCSVPage.getAreaPickerList('area')).toContain('B1-128 - Ri Ra Irish Pub');
      expect(exportCSVPage.getAreaPickerList('area')).not.toContain('B1-109B - The Art of Shaving');
      searchBar.clear();

      exportCSVPage.getAreaPicker().click();  //revert state
    });

    it('"select all" button in area/zone dropdown should function correctly', function () {
      exportCSVPage.getAreaPicker().click();
      var locationListInMenu = exportCSVPage.getAreaPickerList('area');

      locationListInMenu.then(function (menuList) {
        exportCSVPage.getSelectAllBtn('area').click();
        exportCSVPage.getAreaPicker().click();
        var locationListOnPage = exportCSVPage.getSelectedAreaList('area');

        locationListOnPage.then(function (pageList) {
          expect(menuList.sort()).toEqual(pageList.sort());
        });
      });

      exportCSVPage.getAreaPicker().click();     //revert state
      exportCSVPage.getSelectAllBtn('area').click();   //revert state
      exportCSVPage.getAreaPicker().click();     //revert state

    });

    it('should export a CSV', function () {

      browser.executeScript('window.scrollTo(0,0);').then(function () {
        exportCSVPage.setAreaPickerLocation('area', 'Irish Pub');
      });

      var metrics = exportCSVPage.getMetrics();
      metrics.then(function (array) {
        browser.executeScript('window.scrollTo(0,0);').then(function () {
          return array[0].click();
        });
      });

      //group report metrics by week to make it simpler to check accuracy of file contents
      exportCSVPage.setExportGroupBy('Week');

      dateSelector.getExportCSVReportingPeriod(userData.superUser.dateFormat).then(function (exportTabDate) {
        browser.executeScript('window.scrollTo(0,0);').then(function () {
          exportCSVPage.exportCSV();
        });
        browser.waitForAngular();
        if (browser.params.localBrowser) { // different checks if testing locally vs testing remotely
          var fileName = browser.params.downloadPath + '/report-site.csv';
          var folderContents;

          browser.driver.wait(function () {
            // Wait until the file has been downloaded.
            // We need to wait thus as otherwise protractor has a nasty habit of
            // trying to do any following tests while the file is still being
            // downloaded and hasn't been moved to its final location.
            folderContents = fs.readdirSync(browser.params.downloadPath);
            var returnValue = false;
            folderContents.forEach(function (file) { //checks to make sure a CSV has been downloaded before proceeding
              if (file.indexOf('.csv') === file.length - 4) { //checks for file ending in .csv
                returnValue = true;
              }
            });
            return returnValue;
          }, 30000).then(function () {
            expect(fs.readFileSync(fileName, {encoding: 'utf8'})).toMatch(
              '"organization_name","site_name","location_name","period_start_date","period_end_date","traffic"' + '\r\n' +
              '"' + orgData.SSOrg.name + '","' + orgData.SSOrg.ssOrgSiteName + '","' + orgData.SSOrg.testArea + '","' +
              moment(exportTabDate[0]).format(userData.superUser.dateFormat) + '","' + moment(exportTabDate[1]).format(userData.superUser.dateFormat) + '"'
            );
          });
        }
      });
//todo: no check for successful download in remote case yet - need a download success/error statement (LFR-129)
    });

    it('should schedule a report correctly', function () {
      var initialScheduledReports = exportCSVPage.getScheduledCSVs();

      initialScheduledReports.then(function (list) {
        expect(list.length).toBe(0);     //check that scheduled report list is initially empty, otherwise expectations below will fail
        if (list.length >0) {
          list.forEach(function () {
            exportCSVPage.removeScheduledCSV();
          });
        }
      });

      browser.executeScript('window.scrollTo(0,0);').then(function () {
        exportCSVPage.getScheduleReportButton().click();
      });
      exportCSVPage.scheduleTestCSV('Daily', orgData.SSOrg.name);

      browser.executeScript('window.scrollTo(0,0);').then(function () {
        exportCSVPage.getScheduleReportButton().click();
      });

      exportCSVPage.scheduleTestCSV('Weekly', orgData.SSOrg.name);

      browser.executeScript('window.scrollTo(0,0);').then(function () {
        exportCSVPage.getScheduleReportButton().click();
      });

      exportCSVPage.scheduleTestCSV('Monthly', orgData.SSOrg.name);

      browser.executeScript('window.scrollTo(0,0);').then(function () {
        exportCSVPage.getScheduleReportButton().click();
      });

      exportCSVPage.scheduleTestCSV('Yearly', orgData.SSOrg.name);

      var scheduledReportList = exportCSVPage.getScheduledCSVs();
      scheduledReportList.then(function (list) {
        expect(list[0].getText()).toMatch('Day');
        expect(list[1].getText()).toMatch('Week');
        expect(list[2].getText()).toMatch('Month');
        expect(list[3].getText()).toMatch('Year');

        list.forEach(function () {
          exportCSVPage.removeScheduledCSV();
          browser.waitForAngular();
        });

        initialScheduledReports = exportCSVPage.getScheduledCSVs();

        initialScheduledReports.then(function (list) {
          expect(list.length).toBe(0);
        });
      });
    });


    afterAll(function () {
      if (browser.params.localBrowser) { //delete file to restore base state/avoid naming issues
        fs.unlinkSync(browser.params.downloadPath + '/report-site.csv');
        console.log('deleting ' + browser.params.downloadPath + '/report-site.csv');
      }
    });
  });

  describe('Retail org', function () {

    beforeAll(function () {
      browser.get('#/' + orgData.MSRetailOrg.id + '/retail?dateRangeStart=' + dateSelector.getURLDate('week', true) +
        '&dateRangeEnd=' + dateSelector.getURLDate('week', false) + '&compareRange1Start=' + dateSelector.getURLDate('week', true, 1) +
        '&compareRange1End=' + dateSelector.getURLDate('week', false, 1) + '&compareRange2Start=' + dateSelector.getURLDate('week', true, 2) +
        '&compareRange2End=' + dateSelector.getURLDate('week', false, 2) + '&token=' + token
      );
    });

    it('date range on metric tab should match range on export page', function () {
      dateSelector.getReportingPeriod(userData.superUser.dateFormat).then(function (metricTabDate) {
        nav.navToExportCSV();
        var exportTabDate = dateSelector.getExportCSVReportingPeriod(userData.superUser.dateFormat);

        expect(exportTabDate).toEqual(metricTabDate);
      });
    });

    it('should show the correct metrics', function () {
      exportCSVPage.setMetricType('perimeter');
      var perimeterMetrics = exportCSVPage.getMetrics();
      expect(perimeterMetrics.getText()).toEqual(orgData.MSRetailOrg.metrics.csv);
    });

    it('each metric should be clickable', function () {
      var metrics = exportCSVPage.getMetrics();

      metrics.each(function (elm) {
        expect(elm.getAttribute('class')).not.toMatch('selected');
        elm.click();
        browser.waitForAngular();
        expect(elm.getAttribute('class')).toMatch('selected');
        elm.click();
        browser.waitForAngular();
        expect(elm.getAttribute('class')).not.toMatch('selected');
      });
    });

    it('the export buttons should be enabled when a metric and an area are selected, and disabled otherwise', function () {
      var exportButton = exportCSVPage.getExportButton();
      var scheduleReportButton = exportCSVPage.getScheduleReportButton();
      var metrics = exportCSVPage.getMetrics();

      expect(exportButton.getAttribute('disabled')).toMatch('true');
      expect(scheduleReportButton.getAttribute('disabled')).toMatch('true');

      metrics.then(function (array) {
        browser.executeScript('window.scrollTo(0,0);').then(function () {
          return array[0].click();
        });
      });

      expect(exportButton.getAttribute('disabled')).toMatch('true');
      expect(scheduleReportButton.getAttribute('disabled')).toMatch('true');

      exportCSVPage.setAreaPickerLocation('site', 'Chicago');
      expect(exportButton.getAttribute('disabled')).toEqual(null);
      expect(scheduleReportButton.getAttribute('disabled')).toEqual(null);

      metrics.then(function (array) {  //revert state
        browser.executeScript('window.scrollTo(0,0);').then(function () {
          return array[0].click();
        });
      });

      expect(exportButton.getAttribute('disabled')).toMatch('true');
      expect(scheduleReportButton.getAttribute('disabled')).toMatch('true');

      exportCSVPage.setAreaPickerLocation('site', 'Chicago');
    });


    it('should export a CSV', function() {

      var metrics = exportCSVPage.getMetrics();
      metrics.then(function (array) {
        browser.executeScript('window.scrollTo(0,0);').then(function () {
          return array[0].click();
        });
      });

      browser.executeScript('window.scrollTo(0,0);').then(function () {
        exportCSVPage.setAreaPickerLocation('site', 'Chicago');
      });

      //group report metrics by week to make it simpler to check accuracy of file contents
      exportCSVPage.setExportGroupBy('Week');

      dateSelector.getExportCSVReportingPeriod(userData.superUser.dateFormat).then(function(exportTabDate) {
        browser.executeScript('window.scrollTo(0,0);').then(function () {
          exportCSVPage.exportCSV();
        });
        browser.waitForAngular();
        if (browser.params.localBrowser) { // different checks if testing locally vs testing remotely
          var fileName = browser.params.downloadPath + '/report-site.csv';
          var folderContents;

          browser.driver.wait(function () {
            // Wait until the file has been downloaded.
            // We need to wait thus as otherwise protractor has a nasty habit of
            // trying to do any following tests while the file is still being
            // downloaded and hasn't been moved to its final location.
            folderContents =  fs.readdirSync(browser.params.downloadPath);
            var returnValue = false;
            folderContents.forEach(function(file){ //checks to make sure a CSV has been downloaded before proceeding
              if(file.indexOf('.csv') === file.length - 4){ //checks for file ending in .csv
                returnValue = true;
              }
            });
            return returnValue;
          }, 30000).then(function () {
            expect(fs.readFileSync(fileName, {encoding: 'utf8'})).toMatch(
              '"organization_name","site_name","period_start_date","period_end_date","traffic"' + '\r\n' +
              '"' + orgData.MSRetailOrg.name + '","' +  orgData.MSRetailSite.testSiteName + '","' +
              moment(exportTabDate[0]).format(userData.superUser.dateFormat) + '","' + moment(exportTabDate[1]).format(userData.superUser.dateFormat) + '"'
            );
          });
        }
      });
//todo: no check for successful download in remote case yet - need a download success/error statement (LFR-129)
    });

    it('should schedule a report correctly', function () {

      var initialScheduledReports = exportCSVPage.getScheduledCSVs();

      initialScheduledReports.then(function (list) {
        expect(list.length).toBe(0);     //check that scheduled report list is initially empty, otherwise expectations below will fail
        if (list.length >0) {
          list.forEach(function () {
            exportCSVPage.removeScheduledCSV();
          });
        }
      });

      browser.executeScript('window.scrollTo(0,0);').then(function () {
        exportCSVPage.getScheduleReportButton().click();
      });
      exportCSVPage.scheduleTestCSV('Daily', orgData.MSRetailOrg.name);

      browser.executeScript('window.scrollTo(0,0);').then(function () {
        exportCSVPage.getScheduleReportButton().click();
      });

      exportCSVPage.scheduleTestCSV('Weekly', orgData.MSRetailOrg.name);

      browser.executeScript('window.scrollTo(0,0);').then(function () {
        exportCSVPage.getScheduleReportButton().click();
      });

      exportCSVPage.scheduleTestCSV('Monthly', orgData.MSRetailOrg.name);

      browser.executeScript('window.scrollTo(0,0);').then(function () {
        exportCSVPage.getScheduleReportButton().click();
      });

      exportCSVPage.scheduleTestCSV('Yearly', orgData.MSRetailOrg.name);

      var scheduledReportList = exportCSVPage.getScheduledCSVs();
      scheduledReportList.then(function (list) {
        expect(list[0].getText()).toMatch('Day');
        expect(list[1].getText()).toMatch('Week');
        expect(list[2].getText()).toMatch('Month');
        expect(list[3].getText()).toMatch('Year');

        list.forEach(function () {
          exportCSVPage.removeScheduledCSV();
          browser.waitForAngular();
        });

        initialScheduledReports.then(function (list) {
          expect(list.length).toBe(0);
        });
      });
    });

    afterAll(function (done) {
      nav.logout();
      if (browser.params.localBrowser) { //delete file to restore base state/avoid naming issues
        fs.unlinkSync(browser.params.downloadPath + '/report-site.csv');
        console.log('deleting ' + browser.params.downloadPath + '/report-site.csv');
      }
      login.deleteUser(done, userId);
    });
  });
});
