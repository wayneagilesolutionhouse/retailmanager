/* global angular _ */

const jasmineReporters = require('jasmine-reporters');
const SpecReporter = require('jasmine-spec-reporter');

function buildElementArrayFinderFromPromise(promiseOfElementFinders) {
  const getPromise = () => {
    return promiseOfElementFinders;
  };
  return new protractor.ElementArrayFinder(protractor, getPromise);
}

function onPrepareCiHandler(jasmine, screenSize) {
  // add teamcity jasmine spec reporter
  const teamCityReporter = new jasmineReporters.TeamCityReporter({
    savePath: '..',
    consolidateAll: false,
    displayStacktrace: 'all'
  });
  return onPrepareHandlerWithReporter(jasmine, teamCityReporter, screenSize);
}

function onPrepareHandler(jasmine, screenSize) {
  // add jasmine spec reporter
  const reporter = new SpecReporter({ displayStacktrace: 'all' });

  return onPrepareHandlerWithReporter(jasmine, reporter, screenSize);
}

function onPrepareHandlerWithReporter(jasmine, reporter, screenSize = 'full') {
  jasmine.getEnv().addReporter(reporter);
  let width = 1100;
  let height = 1000;  // full screen defaults
  if (screenSize === 'mobile') {
    width = 350;
    height = 550;
  }
  browser.manage().window().setSize(width, height);

  // todo: investigate removing functionality below when upgraded to protractor v3 (Jira LFR-38)
  // Disable animations so e2e tests run more quickly
  const disableNgAnimate = () => {
    angular.module('disableNgAnimate', []).run(['$animate', $animate => {
      $animate.enabled(false);
    }]);
  };
  browser.addMockModule('disableNgAnimate', disableNgAnimate);

  const debounceRightNow = () => {
    angular.module('debounceRightNow', []).run(
      () => {
        _.debounce = callback => {
          return callback;
        };
      });
  };
  browser.addMockModule('debounceRightNow', debounceRightNow);

  addHelperMethods();
}

function addHelperMethods() {
  // ManagedPromise.asElementArrayFinder
  protractor.promise.Promise.prototype.asElementArrayFinder =
    function asElementArrayFinder() {
      return buildElementArrayFinderFromPromise(this);
    };

  // ElementArrayFinder.getTextLowerCase
  protractor.ElementArrayFinder.prototype.getTextLowerCase =
    function getTextArrayLowerCase() {
      return this.getText().then(getTextResult => {
        // assumes getTextResult is an array
        if (getTextResult == null) {
          return getTextResult;
        }

        return getTextResult.map(singleElementText => {
          return singleElementText.toLowerCase();
        });
      });
    };

  // ElementFinder.getTextLowerCase
  protractor.ElementFinder.prototype.getTextLowerCase =
    function getTextLowerCase() {
      return this.getText().then(getTextResult => {
        if (getTextResult == null) {
          return getTextResult;
        }

        return getTextResult.toLowerCase();
      });
    };
}

module.exports = {
  onPrepareHandler,
  onPrepareCiHandler
};
