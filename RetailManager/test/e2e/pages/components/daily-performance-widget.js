'use strict';

var widgetTag = 'daily-performance-widget';

module.exports = {

  getTableHeaders: function() {
    return ['sales', 'sales-delta', 'traffic', 'traffic-delta', 'labor', 'labor-delta', 'transactions', 'transactions-delta',
      'conversion', 'star', 'day'];
  },

  widgetTitle: function () {
    return element(by.tagName(widgetTag)).element(by.className('widget-title')).element(by.css('span.ng-binding'));
  },

  getExpandTableButton: function(){
    var buttonList = element(by.tagName(widgetTag)).all(by.className('table-toggle'));
    var activeButton =  buttonList.filter(function(button) {
      return button.isDisplayed();
    }).first();
    return activeButton.element(by.css('a'));
  },

  isTableDisplayed: function() {
    var table = element(by.tagName(widgetTag)).element(by.css('table.daily-performance-widget-table'));
    return table.isPresent();
  },

  getHeaderColumnNum: function(metricColumn) {
    switch (metricColumn.toLowerCase()) {
      case 'day':
        return 0;
      case 'sales':
        return 1;
      case 'sales-delta':
        return 2;
      case 'traffic':
        return 3;
      case 'traffic-delta':
        return 4;
      case 'labor':
        return 5;
      case 'labor-delta':
        return 6;
      case 'transactions':
        return 7;
      case 'transactions-delta':
        return 8;
      case 'conversion':
        return 9;
      case 'star':
        return 10;
      default :
        throw new Error(metricColumn + ' is not a valid option for metricColumn.');
    }
  },

  sortTableBy: function(metricColumn) {
    var columnNum = this.getHeaderColumnNum(metricColumn);
    var columnHeader = element(by.tagName(widgetTag)).element(by.css('table.daily-performance-widget-table')).all(by.css('th')).get(columnNum).element(by.css('a'));
    columnHeader.click();
  },

  getMetricColumnNum: function (metricColumn) {
    switch (metricColumn.toLowerCase()) {
      case 'traffic':
        return 0;
      case 'sales':
        return 1;
      case 'labor':
        return 2;
      case 'transactions':
        return 3;
      case 'conversion':
        return 4;
      case 'star':
        return 5;
      default :
        throw new Error(metricColumn + ' is not a valid option for metricColumn.');
    }
  },

  getDeltaColumnNum: function (metricColumn) {
    switch (metricColumn.toLowerCase()) {
      case 'traffic-delta':
        return 0;
      case 'sales-delta':
        return 1;
      case 'labor-delta':
        return 2;
      case 'transactions-delta':
        return 3;
      default :
        throw new Error(metricColumn + ' is not a valid option for metricColumn.');
    }
  },

  getColumnData: function(metricColumn) {
    var columnNum;

    if(metricColumn.toLowerCase() === 'day') {
      return element(by.tagName(widgetTag)).element(by.css('table.daily-performance-widget-table')).all(by.repeater('row in vm.tableData track by row.dayOfWeek').column('row.dayOfWeek')).getText();
    } else if (['sales',  'traffic',  'labor', 'conversion',  'transactions', 'star'].indexOf(metricColumn.toLowerCase()) > -1){
      columnNum = this.getMetricColumnNum(metricColumn);
      return element(by.tagName(widgetTag)).element(by.css('table.daily-performance-widget-table')).all(by.repeater('row in vm.tableData track by row.dayOfWeek')).all(by.repeater('metric in vm.metricDisplayInfo track by metric.value').row(columnNum).column('row[metric.apiPropertyName] ')).getText();
    } else if (['sales-delta',  'traffic-delta',  'labor-delta',  'transactions-delta'].indexOf(metricColumn.toLowerCase()) > -1) {
      columnNum = this.getDeltaColumnNum(metricColumn);
      return element(by.tagName(widgetTag)).element(by.css('table.daily-performance-widget-table')).all(by.repeater('row in vm.tableData track by row.dayOfWeek')).all(by.repeater('metric in vm.metricDisplayInfo track by metric.value').row(columnNum).column('row[metric.apiPropertyName + \'_contribution\'] ')).getText();
    }
  },

  openDaySelectorDropdown: function() {
    var dropdown =  element(by.tagName(widgetTag)).element(by.tagName('day-selector')).element(by.css('button'));
    return browser.executeScript('window.scrollTo(0, 0);').then(function() {
      dropdown.click();
    });
  },

  getDaySelectorOptions: function() {
    this.openDaySelectorDropdown();
    return element(by.tagName(widgetTag)).element(by.tagName('day-selector')).all(by.repeater('item in vm.items track by $index')).getText();
  },

  getColumnDataAsNumber: function(metricColumn) {
    return this.getColumnData(metricColumn).then(function(columnData) {
      var numberArray = [];
      columnData.forEach(function(columnDatum) {
        columnDatum = Number(columnDatum.replace(/\./g, '').replace(/,/g, '.').replace('%', ''));
        numberArray.push(columnDatum);
      });
      return numberArray;
    });
  },

  getHighestYAxisValue: function(position, yAxisPosition) {
    var highestValue;
    if (position === 'left'){
      highestValue = element(by.tagName(widgetTag)).element(by.className('data-section')).element(by.className('left-chart')).element(by.className('highcharts-yaxis-labels')).all(by.css('text')).last().getText();
    } else if (position === 'right') {
      if (yAxisPosition === 'left') {
        highestValue = element(by.tagName(widgetTag)).element(by.className('data-section')).element(by.className('right-chart')).all(by.className('highcharts-yaxis-labels')).first().all(by.css('text')).last().getText();
      } else if (yAxisPosition === 'right') {
        highestValue = element(by.tagName(widgetTag)).element(by.className('data-section')).element(by.className('right-chart')).all(by.className('highcharts-yaxis-labels')).last().all(by.css('text')).last().getText();
      }
    }

    return highestValue.then(function(value) {
      return Number(value);
    });
  },

  getChartTitle: function(position) {
    if (position === 'left'){
      return element(by.tagName(widgetTag)).element(by.className('data-section')).element(by.className('left-chart')).element(by.className('chart-title')).element(by.css('h3')).getText();
    } else if (position === 'right') {
      return element(by.tagName(widgetTag)).element(by.className('data-section')).element(by.className('right-chart')).element(by.className('chart-title')).element(by.css('h3')).getText();
    }
  },

  getChartYAxisLabels: function(position) {
    if (position === 'left'){
      return element(by.tagName(widgetTag)).element(by.className('data-section')).element(by.className('left-chart')).element(by.className('y-axis-titles')).getText();
    } else if (position === 'right') {
      return element(by.tagName(widgetTag)).element(by.className('data-section')).element(by.className('right-chart')).element(by.className('y-axis-titles')).getText();
    }
  },

  getChartXAxisLabels: function(position) {
    if (position === 'left'){
      return element(by.tagName(widgetTag)).element(by.className('data-section')).element(by.className('left-chart')).element(by.className('chart-body')).element(by.className('highcharts-xaxis-labels')).all(by.css('text')).getText();
    } else if (position === 'right') {
      return element(by.tagName(widgetTag)).element(by.className('data-section')).element(by.className('right-chart')).element(by.className('chart-body')).element(by.className('highcharts-xaxis-labels')).all(by.css('text')).getText();
    }
  },

  getChartLegend: function(position) {
    if (position === 'left'){
      return element(by.tagName(widgetTag)).element(by.className('data-section')).element(by.className('left-chart')).element(by.className('chart-body')).element(by.className('chart-legend')).all(by.css('span.chart-legend-label')).getText();
    } else if (position === 'right') {
      return element(by.tagName(widgetTag)).element(by.className('data-section')).element(by.className('right-chart')).element(by.className('chart-body')).element(by.className('chart-legend')).all(by.css('span.chart-legend-label')).getText();
    }
  },

  getChartTooltip: function(position) {
    var chartClass;
    if (position === 'left'){
      chartClass = 'left-chart';
    } else if (position === 'right') {
      chartClass = 'right-chart';
    }

    var chartSeries =  element(by.tagName(widgetTag)).element(by.className(chartClass)).element(by.className('highcharts-container')).element(by.className('highcharts-series-group')).element(by.css('g.highcharts-markers')).all(by.css('path'));
    chartSeries.each(function(point){
      browser.actions().mouseMove(point).perform();
    });

    var tooltips = element(by.tagName(widgetTag)).element(by.className('data-section')).element(by.className(chartClass)).element(by.className('chart-body')).all(by.css('div.highcharts-tooltip'));
    return tooltips.filter(function(tooltip) {
      return tooltip.isDisplayed();
    }).all(by.className('tooltip-option')).getText();
  },

  getChartClass: function(position) {
    var chartClass;
    if (position === 'left') {
      chartClass = 'left-chart';
    } else if (position === 'right') {
      chartClass = 'right-chart';
    }
    return chartClass;
  },

  getLineNum: function(metric) {
    switch (metric.toLowerCase()) {
      //left chart
      case 'traffic':
        return 0;
      case 'sales':
        return 1;
      case 'labor':
        return 2;
      case 'transactions':
        return 3;
      //right chart
      case 'conversion':
        return 0;
      case 'star':
        return 1;
      default :
        throw new Error(metric + ' is not a valid option for metric.');
    }
  },

  getChartLineLength: function(position, metric) {
    var chartClass = this.getChartClass(position);
    var lineNum = this.getLineNum(metric);
    var points = element(by.tagName(widgetTag)).element(by.className(chartClass)).element(by.className('highcharts-container')).element(by.className('highcharts-series-group')).all(by.css('g.highcharts-markers')).get(lineNum).all(by.css('path'));
    return points.then(function (linePoints) {
      return linePoints.length;
    });
  },

  getDayRow: function(day){
    switch (day.toLowerCase()) {
      case 'all':
        return 0;
      case 'weekend':
        return 1;
      case 'sunday':
        return 2;
      case 'monday':
        return 3;
      case 'tuesday':
        return 4;
      case 'wednesday':
        return 5;
      case 'thursday':
        return 6;
      case 'friday':
        return 7;
      case 'saturday':
        return 8;
      default :
        throw new Error(day + ' is not a valid day.');
    }
  },

  selectDay: function(day) {
    var dayRow = this.getDayRow(day);
    element(by.tagName(widgetTag)).element(by.tagName('day-selector')).all(by.repeater('item in vm.items track by $index').row(dayRow)).click();
  },

  getTableHeaderMetricLabels: function() {
    return element(by.tagName(widgetTag)).element(by.css('table.table-bordered')).all(by.repeater('metric in vm.metricDisplayInfo track by metric.value').column('::metric.shortTranslationLabel')).getText();
  },

  getTableDayLabels: function() {
    return element(by.tagName(widgetTag)).element(by.css('table.table-bordered')).all(by.repeater('row in vm.tableData track by row.dayOfWeek').column('row.dayOfWeek')).getText();
  },

  getTableFooterLabel: function() {
    return element(by.tagName(widgetTag)).element(by.css('table.table-bordered')).element(by.css('tfoot')).all(by.css('td')).first().getText();
  }

};
