'use strict';

var tableWidget = require('./common/table-widget.js');

module.exports = {

  columnHeadings: ['Selected period', 'Change', 'Prior period traffic', 'Change', 'Prior year traffic'],

  widgetTitle: function() {
    return element(by.tagName('entrance-contribution-widget')).element(by.className('widget-title'));
  },

  clickExportButton: function() {
    var exportButton = element(by.tagName('entrance-contribution-widget')).element(by.className('export-widget')).element(by.css('a'));
    exportButton.click();
  },

  clickExpandButtonIfPresent: function() {
    return tableWidget.clickExpandButton('entrance-contribution-widget');
  },

  getFilteredEntranceList: function() {
    return tableWidget.getColumnByBinding('entrance-contribution-widget', 'entranceItem in (vm.filteredEntrances = (vm.entranceItems | orderBy: vm.sortType | filter: vm.filterEntrances))', 'entranceItem.name').getText();
  },

  getFilter: function() {
    return tableWidget.getFilter('entrance-contribution-widget', 'vm.filterEntrances');
  },

  getCurrentColumnHeader: function(){
    return element(by.tagName('entrance-contribution-widget')).element(by.css('[ng-click="vm.orderBy(\'traffic*1\')"]'));
  },

  getPeriodChangeColumnHeader: function() {
    return element(by.tagName('entrance-contribution-widget')).element(by.css('[ng-click="vm.orderBy(\'change*1\')"]'));
  },

  getPeriodColumnHeader: function() {
    return element(by.tagName('entrance-contribution-widget')).element(by.css('[ng-click="vm.orderBy(\'trafficCompare*1\')"]'));
  },
  getYearChangeColumnHeader: function() {
    return element(by.tagName('entrance-contribution-widget')).element(by.css('[ng-click="vm.orderBy(\'changeYear*1\')"]'));
  },

  getYearColumnHeader: function() {
    return element(by.tagName('entrance-contribution-widget')).element(by.css('[ng-click="vm.orderBy(\'trafficYearCompare*1\')"]'));
  },

  getColumnText: function(columnBinding) {
    return tableWidget.getColumnByBinding('entrance-contribution-widget', 'entranceItem in (vm.filteredEntrances = (vm.entranceItems | orderBy: vm.sortType | filter: vm.filterEntrances))', columnBinding).getText();
  },

  getCurrentPeriodColumn: function() {
    return tableWidget.formatNumberArray(this.getColumnText('entranceItem.traffic | formatNumber:0:vm.numberFormatName'))
  },

  getPriorPeriodColumn: function() {
    return tableWidget.formatNumberArray(this.getColumnText('entranceItem.trafficCompare | formatNumber:0:vm.numberFormatName'))
  },

  getPriorYearColumn: function() {
    return tableWidget.formatNumberArray(this.getColumnText('entranceItem.trafficYearCompare | formatNumber:0:vm.numberFormatName'))
  },

  getPriorPeriodDeltaColumn: function() {
    //return tableWidget.getDeltaColumn('entrance-contribution-widget', 'entranceItem in (vm.filteredEntrances = (vm.entranceItems | orderBy: vm.sortType | filter: vm.filterEntrances))', 'entranceItem.change ');
    return tableWidget.formatDeltaColumn(element(by.tagName('entrance-contribution-widget')).all(by.repeater('entranceItem in (vm.filteredEntrances = (vm.entranceItems | orderBy: vm.sortType | filter: vm.filterEntrances))').column('vm.displayPercentageChangeLabel( entranceItem.change, vm.numberFormatName )')).getText());
  },

  getPriorYearDeltaColumn: function() {
    //return tableWidget.getDeltaColumn('entrance-contribution-widget', 'entranceItem in (vm.filteredEntrances = (vm.entranceItems | orderBy: vm.sortType | filter: vm.filterEntrances))', 'entranceItem.changeYear');
    return tableWidget.formatDeltaColumn(element(by.tagName('entrance-contribution-widget')).all(by.repeater('entranceItem in (vm.filteredEntrances = (vm.entranceItems | orderBy: vm.sortType | filter: vm.filterEntrances))').column('vm.displayPercentageChangeLabel( entranceItem.changeYear, vm.numberFormatName )')).getText());
  },

  calculatePriorPeriodDeltaColumn: function() {
    return tableWidget.calculateDelta(this.getCurrentPeriodColumn(), this.getPriorPeriodColumn());
  },

  calculatePriorYearDeltaColumn: function() {
    return tableWidget.calculateDelta(this.getCurrentPeriodColumn(), this.getPriorYearColumn());
  },

  //used in translation checks
  getFilterBarText: function() {
    return tableWidget.getFilter('entrance-contribution-widget', 'vm.filterEntrances').getAttribute('placeholder');
  },

  //used in translation checks
  getExpandButtonText: function() {
    return tableWidget.getExpandButtonText('entrance-contribution-widget');
  },

  //used in translation checks
  getContractButtonText: function() {
    return tableWidget.getContractButtonText('entrance-contribution-widget')
  }
};
