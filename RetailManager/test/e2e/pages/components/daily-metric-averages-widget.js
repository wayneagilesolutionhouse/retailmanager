'use strict';

var lineChartWidget = require('./common/line-chart-widget.js');
var tagName = 'traffic-per-weekday-widget';

module.exports = {

  getDayHeaders: function() {
    return [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ];
  },

  getMondayHeaders: function() {
    return ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  },

  sortedDayHeaders: function(){
    return this.getDayHeaders().reverse();
  },

  sortedMondayHeaders: function(){
    return this.getMondayHeaders().reverse();
  },

  widgetTitle: function() {
    return element(by.tagName(tagName)).element(by.className('widget-title-name'));
  },

  getExpandTableButton: function() {
    var buttonList = element(by.tagName(tagName)).element(by.className('panel-body')).element(by.className('widget-actions')).element(by.css('a.toggle-table-button')).all(by.css('span'));
    return buttonList.filter(function(button) {
      return button.isDisplayed();
    }).first();
  },

  isTableDisplayed: function() {
    var table = element(by.tagName(tagName)).element(by.className('traffic-per-weekday-widget-table-area'));
    return table.isDisplayed();
  },

  openDaySelectorDropdown: function() {
    var dropdown =  element(by.tagName(tagName)).element(by.tagName('day-selector')).element(by.css('button'));
    dropdown.click();
  },

  getDayRow: function(day){
    switch (day.toLowerCase()) {
      case 'hours':
        return 0;
      case 'all':
        return 1;
      case 'weekend':
        return 2;
      case 'sunday':
        return 3;
      case 'monday':
        return 4;
      case 'tuesday':
        return 5;
      case 'wednesday':
        return 6;
      case 'thursday':
        return 7;
      case 'friday':
        return 8;
      case 'saturday':
        return 9;
      default :
        throw new Error(day + ' is not a valid day.');
    }
  },

  selectDay: function(day) {
    var dayRow = this.getDayRow(day);
    element(by.tagName(tagName)).element(by.tagName('day-selector')).all(by.repeater('item in vm.items track by $index').row(dayRow)).click();
  },


  getDaySelectorOptions: function() {
    this.openDaySelectorDropdown();
    return element(by.tagName(tagName)).element(by.tagName('day-selector')).all(by.repeater('item in vm.items track by $index')).getText();
  },

  openMetricSelectorDropdown: function() {
    var dropdown =  element(by.tagName(tagName)).element(by.css('span.select-chart-metric-dropdown')).element(by.css('button'));
    dropdown.click();
  },

  getMetricSelectorOptions: function() {
    this.openMetricSelectorDropdown();
    return element(by.tagName(tagName)).element(by.css('span.select-chart-metric-dropdown')).element(by.css('ul.dropdown-menu')).all(by.repeater('metric in vm.availableMetrics')).getText();
  },

  getMetricRowNum: function (metric) {
    switch(metric.toLowerCase()) {
      case 'traffic':
        return 0;
      case 'sales':
        return 1;
      case 'conversion':
        return 2;
      case 'ats':
        return 3;
      case 'star':
        return 4;
      default:
        throw new Error(metric + ' is not a valid option for metric.');
    }
  },

  selectMetric: function(metric) {
    var metricNum = this.getMetricRowNum(metric);
    this.openMetricSelectorDropdown();
    browser.waitForAngular();
    var metricList = element(by.tagName(tagName)).element(by.css('span.select-chart-metric-dropdown')).all(by.css('ul.dropdown-menu')).all(by.repeater('metric in vm.availableMetrics').row(metricNum));
    metricList.click();
  },

  getHeaderColumnNum: function(metricColumn) {
    switch (metricColumn.toLowerCase()) {
      case 'day':
        return 0;
      case 'traffic':
        return 1;
      case 'traffic-delta':
        return 2;
      case 'sales':
        return 3;
      case 'sales-delta':
        return 4;
      case 'conversion':
        return 5;
      case 'conversion-delta':
        return 6;
      case 'ats':
        return 7;
      case 'ats-delta':
        return 8;
      case 'star':
        return 9;
      case 'star-delta':
        return 10;
      default :
        throw new Error(metricColumn + ' is not a valid option for metricColumn.');
    }
  },

  sortTableBy: function(metricColumn) {
    var columnNum = this.getHeaderColumnNum(metricColumn);
    var columnHeader = element(by.tagName(tagName)).element(by.css('table.traffic-per-weekday-widget-table')).all(by.css('th')).get(columnNum).element(by.css('a'));
    columnHeader.click();
  },

  getMetricColumnNum: function (metricColumn) {
    var metric = metricColumn.toLowerCase();
    if(metric === 'traffic'){
      return 0;
    } else if (metric === 'sales') {
      return 1;
    } else if (metric === 'conversion') {
      return 2;
    } else if (metric === 'ats') {
      return 3;
    } else if (metric === 'star') {
      return 4;
    }
  },

  getDeltaColumnNum: function (metricColumn) {
    var deltaMetric = metricColumn.toLowerCase();
    if(deltaMetric === 'traffic-delta'){
      return 0;
    } else if (deltaMetric === 'sales-delta') {
      return 1;
    } else if (deltaMetric === 'conversion-delta') {
      return 2;
    } else if (deltaMetric === 'ats-delta') {
      return 3;
    } else if (deltaMetric === 'star-delta') {
      return 4;
    }
  },

  getColumnData: function(metricColumn) {
    var columnNum;

    if(metricColumn.toLowerCase() === 'day') {
      return element(by.tagName(tagName)).element(by.css('table.traffic-per-weekday-widget-table')).all(by.repeater('row in vm.tableData track by $index').column('row.dayOfWeek ')).getText();
    } else if (['sales',  'traffic',  'conversion',  'ats', 'star'].indexOf(metricColumn.toLowerCase()) > -1){
      columnNum = this.getMetricColumnNum(metricColumn);
      return element(by.tagName(tagName)).element(by.css('table.traffic-per-weekday-widget-table')).all(by.repeater('row in vm.tableData track by $index')).all(by.repeater('metric in vm.availableMetrics track by $index').row(columnNum).column('row[ metric ] | formatNumber:vm.metricsConfiguration[ metric ].precision:vm.numberFormatName')).getText();
    } else if (['sales-delta', 'traffic-delta', 'conversion-delta', 'ats-delta', 'star-delta'].indexOf(metricColumn.toLowerCase()) > -1) {
      columnNum = this.getDeltaColumnNum(metricColumn);
      return element(by.tagName(tagName)).element(by.css('table.traffic-per-weekday-widget-table')).all(by.repeater('row in vm.tableData track by $index')).all(by.repeater('metric in vm.availableMetrics track by $index').row(columnNum).column('row[ metric + \'_change\' ]')).getText();
    }
  },

  getTableHeaderMetricLabels: function() {
    return element(by.tagName(tagName)).element(by.css('table.traffic-per-weekday-widget-table')).all(by.repeater('metric in vm.availableMetrics track by $index').column(' \'kpis.kpiTitle.\' + vm.metricsConfiguration[metric].translationLabel | translate ')).getText();
  },

  getTableDayLabels: function() {
    return element(by.tagName(tagName)).element(by.css('table.traffic-per-weekday-widget-table')).all(by.repeater('row in vm.tableData track by $index').column('row.dayOfWeek ')).getText();
  },

  getTableFooterLabel: function() {
    return element(by.tagName(tagName)).element(by.css('table.traffic-per-weekday-widget-table')).element(by.css('tfoot')).all(by.css('td')).first().getText();
  },

  clickExportButton: function() {
    var exportButton = element(by.tagName(tagName)).element(by.className('export-widget')).element(by.css('a'));
    exportButton.click();
  },

  getHighestYAxisValue: function() {
    return lineChartWidget.getHighestYAxisValue(tagName);
  },

  getSelectedPeriodDataValues: function() {
    return lineChartWidget.getSelectedPeriodDataValuesAsNumbers(tagName);
  },

  getPriorPeriodDataValues: function() {
    return lineChartWidget.getPriorPeriodDataValuesAsNumbers(tagName);
  },

  getPriorYearDataValues: function() {
    return lineChartWidget.getPriorYearDataValuesAsNumbers(tagName);
  },

  getXAxisLabels: function() {
    return element(by.tagName(tagName)).element(by.className('ct-labels')).all(by.className('ct-horizontal')).getText();
  },

  getLegendText: function(){
    return lineChartWidget.getLegendText(tagName);
  },

  //used in translation checks
  getTooltipTotalText: function() {
    return lineChartWidget.getBarChartTooltipTotalText(tagName);
  }
};
