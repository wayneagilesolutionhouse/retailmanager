'use strict';

var orgData = require('../../data/orgs.js');

module.exports = {

  logout: function() {

    var collapsedNav = element(by.css('button.navbar-toggle.collapsed'));
    return collapsedNav.isDisplayed().then(function (isMenuButtonPresent) {
      if (isMenuButtonPresent) {
        collapsedNav.click();
        var dropDown = element(by.css('.nav-login-link'));
        var logoutButton = element(by.className('nav-logout'));

        dropDown.click();
        logoutButton.click();
      } else {
        var dropDown = element(by.css('.nav-login-link'));
        var logoutButton = element(by.className('nav-logout'));

        dropDown.click();
        logoutButton.click();
      }
    });
  },

  pickMSOrg: function() {
    navToOrgOrSite('organisation-picker', orgData.MSOrg.name);
  },

  navToMSOrgSite: function() {
    navToOrgOrSite('site-picker', orgData.MSOrgSite.name);
  },

  pickSSOrg: function() {
    navToOrgOrSite('organisation-picker', orgData.SSOrg.name);
  },

  pickRetailOrg: function() {
    navToOrgOrSite('organisation-picker', orgData.MSRetailOrg.name);
  },

  navToRetailOrgSite: function() {
    navToOrgOrSite('site-picker', orgData.MSRetailSite.testSiteName);
  },

  getOrgName: function() {
    var collapsedNav = element(by.css('button.navbar-toggle.collapsed'));
    return collapsedNav.isDisplayed().then(function (isMenuButtonPresent) {
      if (isMenuButtonPresent) {
        collapsedNav.click();
        var orgName = element(by.id('organisation-picker')).element(by.className('dropdown-toggle')).getText();
        collapsedNav.click();
        return orgName;
      } else {
        return element(by.id('organisation-picker')).element(by.className('dropdown-toggle')).getText();
      }
    });
  },

  getSiteName: function() {
    var collapsedNav = element(by.css('button.navbar-toggle.collapsed'));
    return collapsedNav.isDisplayed().then(function (isMenuButtonPresent) {
      if (isMenuButtonPresent) {
        collapsedNav.click();
        var siteName = element(by.id('site-picker')).element(by.className('dropdown-toggle')).getText();
        collapsedNav.click();
        return siteName;
      } else {
        return element(by.id('site-picker')).element(by.className('dropdown-toggle')).getText();
      }
    });
  },

  getSingleSiteName: function() {
    var collapsedNav = element(by.css('button.navbar-toggle.collapsed'));
    return collapsedNav.isDisplayed().then(function (isMenuButtonPresent) {
      if (isMenuButtonPresent) {
        collapsedNav.click();
        var siteName = element(by.id('single-site-link')).getText();
        collapsedNav.click();
        return siteName;
      } else {
        return element(by.id('single-site-link')).getText();
      }
    });
  },

  navToExportCSV: function() {
    openExportDropdown();
    navToExportFeature('navbar-export-csv');
  },

  navToExportPDFView: function() {
    openExportDropdown();
    navToExportFeature('navbar-export-current-view');
  },

  navToExportSelected: function() {
    openExportDropdown();
    navToExportFeature('navbar-export-selected');
  },

  isExportSelectedEnabled: function() {
    return openExportDropdown().then(function() {
      return element(by.id('navbar')).element(by.className('navbar-export-selected')).getAttribute('disabled');
    }).then(function(disabledFlag) {
      return openExportDropdown().then(function() {
        return disabledFlag !== 'true';
      });
    });
  },

  getHeaderBar() {
    return element(by.id('navbar'));
  }
};

function navToOrgOrSite(pickerId, linkText) {
  var collapsedNav = element(by.css('button.navbar-toggle.collapsed'));
  return collapsedNav.isDisplayed().then(function(isMenuButtonPresent){
    if(isMenuButtonPresent){
      collapsedNav.click();
      var picker = element(by.id(pickerId));
      picker.click();
      var loc = picker.element(by.className('dropdown-menu')).element(by.linkText(linkText));
      loc.click();
      browser.waitForAngular();
      var dropdown = element(by.id('navbar'));
      return dropdown.isDisplayed().then(function(isDropdownPresent) { //necessary because dropdown sometimes retracts on its own (when nav-ing from export page), so click is not always needed
        if (isDropdownPresent) {
          element(by.css('button.navbar-toggle.collapsed')).click();
        }
      });
    } else {
      var picker = element(by.id(pickerId));
      picker.click();
      browser.waitForAngular();
      var loc = picker.element(by.className('dropdown-menu')).element(by.linkText(linkText));
      loc.click();
    }
  });
}

function openExportDropdown() {
  var collapsedNav = element(by.css('button.navbar-toggle.collapsed'));
  return collapsedNav.isDisplayed().then(function (isMenuButtonPresent) {
    if (isMenuButtonPresent) {
      return collapsedNav.click().then(function() {
        browser.waitForAngular();
        var picker = element(by.id('navbar')).element(by.className('dropdown-toggle-export'));
        picker.click();
      });
    } else {
      var picker = element(by.id('navbar')).element(by.className('dropdown-toggle-export'));
      picker.click();
    }
  });
}

function navToExportFeature(exportFeatureClassName) {
  element(by.id('navbar')).element(by.className(exportFeatureClassName)).click();
}

