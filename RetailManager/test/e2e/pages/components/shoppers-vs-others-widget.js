'use strict';

var lineChartWidget = require('./common/line-chart-widget.js');
var dateSelector = require('./time-period-picker.js');
var widgetID = 'visitor-behavior-tab-shoppers-vs-others-line-chart-widget';

module.exports = {

  widgetTitle: function() {
    return element(by.id(widgetID)).element(by.className('widget-title')).element(by.css('span.ng-binding'));
  },

  clickExportButton: function() {
    var exportButton = element(by.id(widgetID)).element(by.className('export-widget')).element(by.css('a'));
    exportButton.click();
  },

  getXAxisDates: function(dateFormat) {
    return lineChartWidget.getXAxisDates(widgetID, dateFormat);
  },

  getHighestYAxisValue: function() {
    return lineChartWidget.getHighestYAxisValue(widgetID)
  },

  getSelectedPeriodDateRange: function(dateFormat){
  return getFrameDateRange(widgetID, '.shoppers-vs-others-detail-widget-current', dateFormat);
  },

  getSelectedPeriodDataValues: function() {
    return lineChartWidget.getSelectedPeriodDataValues(widgetID);
  },

  getSelectedPeriodShoppers: function() {
    return element(by.id(widgetID)).element(by.className('shoppers-vs-others-detail-widget-current')).element(by.css('div.shoppers')).element(by.tagName('span')).getText().then(function(percent){
      return percentFormatter(percent);
    });
  },

  getSelectedPeriodOthers: function() {
    return element(by.id(widgetID)).element(by.className('shoppers-vs-others-detail-widget-current')).element(by.css('div.others')).element(by.tagName('span')).getText().then(function(percent){
      return percentFormatter(percent);
    });
  },

  getPriorPeriodDateRange: function(dateFormat) {
    return getFrameDateRange(widgetID, '.shoppers-vs-others-detail-widget-compare.prior-period', dateFormat);
  },

  getPriorPeriodDataValues: function() {
    return lineChartWidget.getPriorPeriodDataValues(widgetID);
  },

  getPriorPeriodShoppers: function() {
    return element(by.id(widgetID)).element(by.css('.shoppers-vs-others-detail-widget-compare.prior-period')).element(by.className('share-info')).all(by.tagName('span')).first().getText().then(function(percent){
      return percentFormatter(percent);
    });
  },

  getPriorPeriodOthers: function() {
    return element(by.id(widgetID)).element(by.css('.shoppers-vs-others-detail-widget-compare.prior-period')).element(by.className('share-info')).all(by.tagName('span')).last().getText().then(function(percent){
      return percentFormatter(percent);
    });
  },

  getPriorYearDateRange: function(dateFormat) {
    return getFrameDateRange(widgetID, '.shoppers-vs-others-detail-widget-compare.prior-year', dateFormat);
  },

  getPriorYearDataValues: function() {
    return lineChartWidget.getPriorYearDataValues(widgetID);
  },

  getPriorYearShoppers: function() {
    return element(by.id(widgetID)).element(by.css('.shoppers-vs-others-detail-widget-compare.prior-year')).element(by.className('share-info')).all(by.tagName('span')).first().getText().then(function(percent){
      return percentFormatter(percent);
    });
  },

  getPriorYearOthers: function() {
    return element(by.id(widgetID)).element(by.css('.shoppers-vs-others-detail-widget-compare.prior-year')).element(by.className('share-info')).all(by.tagName('span')).last().getText().then(function(percent){
      return percentFormatter(percent);
    });
  },

  //used in translation checks.
  getLegendText: function(){
    return element(by.id(widgetID)).element(by.className('chart-legend')).all(by.className('chart-legend-label')).getText();
  },

  //used in translation checks
  getSummaryFrameSelectedPeriodLabel: function(){
    return element(by.id(widgetID)).element(by.className('shoppers-vs-others-detail-widget-current')).element(by.className('period-label')).getText();
  },

  //used in translation checks
  getSummaryFrameCompare1Label: function(){
    return element(by.id(widgetID)).element(by.css('.shoppers-vs-others-detail-widget-compare.prior-period')).element(by.className('period-label')).getText();
  },

  //used in translation checks
  getSummaryFrameCompare2Label: function(){
    return element(by.id(widgetID)).element(by.css('.shoppers-vs-others-detail-widget-compare.prior-year')).element(by.className('period-label')).getText();
  },

  //used in translation checks - traffic parameter should be string value of either 'shoppers' or 'others' to assist with getting translated string in widget
  getSummaryFrameMetricLabel: function(traffic){
    return element(by.id(widgetID)).element(by.className('shoppers-vs-others-detail-widget-current')).element(by.className(traffic)).element(by.css('small')).getText()
  },

  //used in translation checks - traffic parameter should be string value of either 'shoppers' or 'others' to assist with getting translated string in widget tooltip
  getTooltipTotalText: function(traffic) {
    var lineChart = element(by.id(widgetID)).element(by.className('ct-chart-container'));
    lineChart.click();
    if (traffic === 'shoppers'){
      return element(by.id(widgetID)).element(by.tagName('linechart-tooltip')).element(by.css('tbody')).all(by.css('th')).first().getText();
    } else if (traffic === 'others'){
      return element(by.id(widgetID)).element(by.tagName('linechart-tooltip')).element(by.css('tbody')).all(by.css('th')).last().getText();
    }
  }
};
function getFrameDateRange(widgetTag, cssSelector, dateFormat){
  var date = element(by.id(widgetTag)).element(by.css(cssSelector)).element(by.css('div.selected-period'));
  var dateText = date.getText();
  return dateText.then(function (dateTextString) {
    var formattedDate = dateSelector.makeDateArray(dateTextString, dateFormat);
    return formattedDate;
  });
}

function percentFormatter(percent){
  return Number(percent.replace(/,/g, '.').replace('%', ''));

}
