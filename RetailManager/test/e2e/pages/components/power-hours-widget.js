'use strict';

var tableWidget = require('./common/table-widget.js');
var tagName = 'power-hours-widget';

module.exports = {
  dayHeaders: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
  mondayHeaders: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
  totalRow: ['13.9%', '14.0%', '14.1%', '14.6%', '15.0%', '13.9%', '14%'],

  widgetTitle: function() {
    return element(by.tagName(tagName)).element(by.className('widget-title')).element(by.className('ng-binding'));
  },

  clickExportButton: function() {
    var exportButton = element(by.tagName(tagName)).element(by.className('export-widget')).element(by.css('a'));
    exportButton.click();
  },

  getMetricRowNum:  function(metric) {
    switch(metric.toLowerCase()) {
      case 'average traffic':
        return 0;
      case 'traffic%':
        return 1;
      case 'average sales':
        return 2;
      case 'conversion':
        return 3;
      case 'ats':
        return 4;
      default:
        throw new Error(metric + ' is not a valid option for metric.');
    }
  },

  openMetricDropdown: function() {
    element(by.tagName(tagName)).element(by.css('button.dropdown-toggle')).click();
  },

  getMetricListItem: function(metricNum, repeater) {
    return element(by.tagName(tagName)).element(by.className('dropdown-menu')).all(by.repeater(repeater).row(metricNum));
  },

  selectMetricDropdown: function(metric) {
    var metricNum = this.getMetricRowNum(metric);
    this.openMetricDropdown();
    var metricListItem = this.getMetricListItem(metricNum, 'option in vm.options ');
    metricListItem.click();
  },

  getMetricOptions: function() {
    this.openMetricDropdown();
    var metricList = element(by.tagName(tagName)).element(by.className('dropdown-menu')).all(by.repeater('option in vm.options ')).getText();
    return metricList;
  },

  getWeeklyTotalTraffic: function() {
    var totalRowTotal = element(by.tagName(tagName)).element(by.className('power-hours-table')).element(by.className('summary-row')).all(by.css('td')).last().getText();
    return totalRowTotal.then(function(num){
      num = Number(num.replace( /\./g, '').replace( /,/g, '.'));
      return num;
    });
  },

  //used in translation checks
  getChartLegendText: function(volume) {
    if (volume.toLowerCase() === 'medium') {
      return element(by.tagName(tagName)).element(by.className('power-hours-table-legend')).element(by.css('div.medium-traffic')).getText();
    } else if (volume.toLowerCase() === 'high') {
      return element(by.tagName(tagName)).element(by.className('power-hours-table-legend')).element(by.css('div.high-traffic')).getText();
    } else {
      throw new Error(volume + ' is not a valid option for volume.');
    }
  },

  getDayHeaders: function() {
    return element(by.tagName(tagName)).element(by.tagName('thead')).all(by.repeater('day in vm.days')).getText();
  },

  getTotalHeader: function() {
    return element(by.tagName(tagName)).element(by.className('power-hours-table')).element(by.tagName('thead')).all(by.tagName('td')).last();
  },

  getBlankHeader: function() {
    return element(by.tagName(tagName)).element(by.className('power-hours-table')).element(by.cssContainingText('td', ' '));
  },

  getRowHeaders: function() {
    return tableWidget.getFilteredList(tagName, 'hourRow in vm.hours | orderBy: \'index\' track by hourRow.index');
  },

  getTotalRow: function() {
    return element(by.tagName(tagName)).element(by.className('power-hours-table')).element(by.className('summary-row'));
  },

  getTotalRowHeader: function() {
    return element(by.tagName(tagName)).element(by.className('power-hours-table')).element(by.className('summary-row')).element(by.tagName('th'));
  },

  getTotalRowSum: function() {
    var totalRowSum = tableWidget.getRowByRepeater(tagName, 'summary-row', 'day in vm.days track by $index').getText();
    return totalRowSum.then(function(array){

      var formattedNumbers = [];
      array.forEach(function(traffic){
        var num = Number(traffic.replace( /\./g, '').replace( /,/g, '.').replace(/%/,'').replace(/\$/,''));
        formattedNumbers.push(num);
      });
      return getRowSum(formattedNumbers);
    });
  },

  getHourRowSum: function(rowNumber){
    var hourRowArray = element(by.tagName('power-hours-widget')).all(by.repeater('hourRow in vm.hours | orderBy: \'index\' track by hourRow.index').row(rowNumber)).all(by.repeater('day in vm.days track by $index')).getText();
    return hourRowArray.then(function(array) {
      var numberArray = [];
      array.forEach(function(number){
        //format string, change to number, and round to one decimal
        var num = Number(number.replace( /\./g, '').replace( /,/g, '.'));
        numberArray.push(num);
      });
      return getRowSum(numberArray);
    });
  },

  getTotalColumnArray: function() {
    var totalRowArray = tableWidget.getColumnByBinding(tagName, 'hourRow in vm.hours | orderBy: \'index\' track by hourRow.index', 'hourRow.total | formatNumber : vm.selectedOption.metric.precision : vm.numberFormatName').getText();
    return totalRowArray.then(function(array){
      var totalNumberArray = [];
      array.forEach(function(number){
        var num = Number(number.replace( /\./g, '').replace( /,/g, '.').replace(/%/,'').replace(/\$/,''));
        totalNumberArray.push(num);
      });
      return totalNumberArray;
    });
  },

  getTotalColumnSum: function() {
    return this.getTotalColumnArray().then(function(array){
      return getRowSum(array);
    });
  }
};


function getRowSum(elementIDText) {
  var row = elementIDText;
  var sum = row.reduce(function (a, b) {
    return a + b;
  });
  return sum;
}

