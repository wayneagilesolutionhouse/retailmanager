'use strict';

var dateSelector = require('../time-period-picker.js');

module.exports = {
  // used in currently-inactive entrance contribution widget?
  //getSiteList: function(widgetTag, columnRepeater, listElementTag) {
  //  var sites = element(by.tagName(widgetTag)).all(by.repeater(columnRepeater));
  //  var siteList = sites.map(function(elm) {
  //    return elm.getText();
  //  });
  //  return siteList;
  //},

  getColList: function(widgetTag, columnRepeater, columnBinding) {
    var siteList = element(by.tagName(widgetTag)).all(by.repeater(columnRepeater).column(columnBinding)).getText();
    return siteList;
  },

  percentFromListItem: function (entrance) {
    var entranceText = entrance.split(' ');

    var percentValue = (entranceText.length) - 1;
    var entrancePercent = entranceText[percentValue].replace('%', '').replace( /\./g, '').replace( /,/g, '.');

    return entrancePercent;
  },

  getListPercentSum: function (widgetTag, columnRepeater, columnBinding) {
    var itemList = this.getColList(widgetTag, columnRepeater, columnBinding);
    return itemList.then(function (list) {
      var percentList = [];

      list.forEach(function (item) {
        var percent = this.percentFromListItem(item);
        percentList.push(percent);
      }.bind(this));

      var percentSum = percentList.reduce(function (a, b) {
        var number = Number(b);
        return Number(a) + number;
      });

      return Math.round(percentSum);
    }.bind(this));
  },

  getPiePercentSum: function(widgetTag){
    var itemList = element(by.tagName(widgetTag)).element(by.css('g.highcharts-data-labels')).all(by.css('tspan.highcharts-text-outline')).getText();
    return itemList.then(function (list) {
      var percentList = [];

      list.forEach(function (item) {
        var percent = this.percentFromListItem(item);
        percentList.push(percent);
      }.bind(this));

      var percentSum = percentList.reduce(function (a, b) {
        var number = Number(b);
        return Number(a) + number;
      });

      return Math.round(percentSum);
    }.bind(this));
  },

  getSelectedPeriodDateRange: function(widgetTag, dateFormat) {
    return getFrameDateRange(widgetTag, '.visiting-frequency-detail-widget-current', dateFormat);
  },

  getPriorPeriodDateRange: function(widgetTag, dateFormat) {
    return getFrameDateRange(widgetTag, '.visiting-frequency-detail-widget-compare.prior-period', dateFormat);
  },

  getPriorYearDateRange: function(widgetTag, dateFormat) {
    return getFrameDateRange(widgetTag, '.visiting-frequency-detail-widget-compare.prior-year', dateFormat);
  },

  getPieChartLegendText: function(widgetTag) {
    return element(by.tagName(widgetTag)).all(by.className('chart-legend-label')).getText();
  }
};

function getFrameDateRange(widgetTag, cssSelector, dateFormat){
  var date = element(by.tagName(widgetTag)).element(by.css(cssSelector)).element(by.css('div.selected-period'));
  var dateText = date.getText();
  return dateText.then(function (dateTextString) {
    var formattedDate = dateSelector.makeDateArray(dateTextString, dateFormat);
    return formattedDate;
  });
}
