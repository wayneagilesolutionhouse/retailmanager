'use strict';

var widgetTag = 'organization-filter-widget';

module.exports = {

  getFilterMenu: function() {
    return element(by.tagName(widgetTag)).element(by.className('filter-header'));
  },

  openFilterMenu: function() {
    var caret = this.getFilterMenu().element(by.className('filter-caret'));
    caret.click();
  },

  getFilterMenuText: function() {
    return this.getFilterMenu().all(by.css('div.filter-title'));
  },

  getFilterMenuButtons: function(){
    return element(by.tagName(widgetTag)).all(by.css('button'));
  },

  getFilterMenuApplyBtn: function() {
    return element(by.tagName(widgetTag)).all(by.css('button')).first();
  },

  getFilterMenuClearBtn: function() {
    return element(by.tagName(widgetTag)).all(by.css('button')).last();
  },

  //todo: when required, add functionality for picking specific filter using filterName
  applyFilter: function(filterName) {
    var filterCategory = element(by.tagName(widgetTag)).all(by.css('div.filter-column-wrapper')).first();
    filterCategory.click();

    var filter = element(by.tagName(widgetTag)).element(by.css('ul.filter-tag')).all(by.repeater('tag in group.levels')).first();
    filter.click();

    this.getFilterMenuApplyBtn().click()
  },


  getSelectedFiltersLabelText: function() {
    return element(by.className('retail-organization-tags')).element(by.className('retail-organization-tag-label')).getText();
  }
};
