'use strict';

var widgetTag = 'retail-store-summary-widget';

module.exports = {

  widgetTitle: function () {
    return element(by.tagName(widgetTag)).element(by.className('widget-title')).element(by.css('span.ng-binding'));
  },

  openStoreCategoryDropdown: function() {
    var dropdown = element(by.tagName(widgetTag)).all(by.css('span.kpi-dropdown')).first();
    dropdown.click();
  },

  getStoreCategoryOptions: function() {
    this.openStoreCategoryDropdown();
    return element(by.tagName(widgetTag)).all(by.css('span.kpi-dropdown')).first().element(by.css('ul.dropdown-menu')).all(by.css('li')).getText();
  },

  openMetricSelectorDropdown: function() {
    var dropdown =  element(by.tagName(widgetTag)).all(by.css('span.kpi-dropdown')).last();
    dropdown.click();
  },

  getMetricOptions: function() {
    this.openMetricSelectorDropdown();
    return element(by.tagName(widgetTag)).all(by.css('span.kpi-dropdown')).last().element(by.css('ul.dropdown-menu')).all(by.css('li')).getText();
  },

  //works with setMetricOption functions below
  getMetricIndex: function(metric) {
    if (metric.toLowerCase() === 'conversion') {
      return 0;
    } else if (metric.toLowerCase() === 'sales') {
      return 1;
    }
  },

  setMetricOption: function(metric) {
    var metricIndex = this.getMetricIndex(metric);
    this.openMetricSelectorDropdown();
    var metric =  element(by.tagName(widgetTag)).all(by.css('span.kpi-dropdown')).last().element(by.css('ul.dropdown-menu')).all(by.css('li')).get(metricIndex).getText();
    metric.click();
  },

  getSearchBar: function() {
    return element(by.tagName(widgetTag)).element(by.css('span.filter-search')).element(by.css('input'));
  },

  getSearchBarPlaceholder: function() {
    return this.getSearchBar().getAttribute('placeholder');
  },

  getExtremeCheckboxLabel: function() {
    return element(by.tagName(widgetTag)).element(by.css('span.retail-extreme-checkbox')).getText();
  },

  getChartText: function() {
    return element(by.tagName(widgetTag)).element(by.id('chart1')).all(by.css('text')).getText();
  },

  getChartTooltipMetrics: function() {
    var dataPoint = element(by.tagName(widgetTag)).element(by.className('highcharts-series-group')).element(by.className('highcharts-markers')).all(by.css('path')).first();
    browser.actions().mouseMove(dataPoint).perform();
    var tooltips =  element(by.tagName(widgetTag)).all(by.css('div.retail-summary-tooltip'));
    return tooltips.filter(function(tooltip){
       return tooltip.isDisplayed();
     }).all(by.css('tr')).all(by.css('th')).getText();
  }
};
