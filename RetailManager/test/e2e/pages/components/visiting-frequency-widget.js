'use strict';

var pieChartWidget = require('./common/pie-chart-widget.js');

module.exports = {

  widgetTitle: function() {
    return element(by.tagName('visiting-frequency-detail-widget')).element(by.className('widget-title'));
  },

  clickExportButton: function() {
    var exportButton = element(by.tagName('visiting-frequency-detail-widget')).element(by.className('export-widget')).element(by.css('a'));
    exportButton.click();
  },

  getSelectedPeriodDateRange: function(dateFormat) {
    return pieChartWidget.getSelectedPeriodDateRange('visiting-frequency-detail-widget', dateFormat);
  },

  getPriorPeriodDateRange: function(dateFormat) {
    return pieChartWidget.getPriorPeriodDateRange('visiting-frequency-detail-widget', dateFormat);
  },

  getPriorYearDateRange: function(dateFormat) {
    return pieChartWidget.getPriorYearDateRange('visiting-frequency-detail-widget', dateFormat);
  },

  getPiePercentSum: function() {
    return pieChartWidget.getPiePercentSum('visiting-frequency-detail-widget');
  },

  getPieChartLegendText: function(){
    return pieChartWidget.getPieChartLegendText('visiting-frequency-detail-widget');
  },

  //used in translation checks
  getSummaryFrameSelectedPeriodLabel: function(){
    return element(by.tagName('visiting-frequency-detail-widget')).element(by.className('visiting-frequency-detail-widget-current')).element(by.className('period-label')).getText();
  },

  //used in translation checks
  getSummaryFrameCompare1Label: function(){
    return element(by.tagName('visiting-frequency-detail-widget')).element(by.css('.visiting-frequency-detail-widget-compare.prior-period')).element(by.className('period-label')).getText();
  },

  //used in translation checks
  getSummaryFrameCompare2Label: function(){
    return element(by.tagName('visiting-frequency-detail-widget')).element(by.css('.visiting-frequency-detail-widget-compare.prior-year')).element(by.className('period-label')).getText();
  },

  //used in translation checks
  getSummaryFrameMetricLabel: function(){
    return element(by.tagName('visiting-frequency-detail-widget')).element(by.className('visiting-frequency-detail-widget-current')).element(by.className('share-info')).element(by.css('small')).getText()
  }

};

