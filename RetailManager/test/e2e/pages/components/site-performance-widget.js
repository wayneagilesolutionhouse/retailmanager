'use strict';

var lineChartWidget = require('./common/line-chart-widget.js');
var widgetID = 'org-summary-site-performance-widget';

module.exports = {
  widgetTitle: function() {
    return element(by.id(widgetID)).element(by.className('widget-title')).all(by.css('span.ng-binding')).first();
  },

  openChartTypeDropdown: function() {
    var dropdown = element(by.id(widgetID)).element(by.className('widget-title')).element(by.css('button.dropdown-toggle'));
    dropdown.click();
  },

  getChartTypeDropdownOptions: function() {
    this.openChartTypeDropdown();
    return element(by.id(widgetID)).element(by.className('widget-title')).all(by.repeater('(key, view) in sitePerformanceWidget.views')).getText();
  },

  getChartDataRowNum: function (option) {
    if (option === 'contribution') {
      return 0;
    } else if (option === 'increase') {
      return 1;
    } else if (option === 'loss') {
      return 2;
    }
  },
//sets chart type option in widget header dropdown.  acceptable "option" parameters are 'contribution', 'increase' and 'loss'
  setChartDataOption: function(option) {
    this.openChartTypeDropdown();
    var optionRowNum = this.getChartDataRowNum(option);
    var chartSelector = element(by.id(widgetID)).element(by.className('widget-title')).all(by.repeater('(key, view) in sitePerformanceWidget.views').row(optionRowNum));
    chartSelector.click();
  },

  getHighestYAxisValue: function() {
    return lineChartWidget.getHighestYAxisValue(widgetID);
  },

  getXAxisSites: function() {
    return element(by.id(widgetID)).all(by.className('ct-labels')).all(by.className('ct-horizontal')).all(by.className('label-name')).getText();
  },

  getSelectedPeriodDataValues: function() {
    return lineChartWidget.getSelectedPeriodDataValues(widgetID);
  },

  getPriorPeriodDataValues: function() {
    return lineChartWidget.getPriorPeriodDataValues(widgetID);
  },

  getPriorYearDataValues: function() {
    return lineChartWidget.getPriorYearDataValues(widgetID);
  },

  getLegendText: function(){
    return lineChartWidget.getLegendText(widgetID);
  },

  //used in translation checks
  getTooltipTotalText: function() {
    return lineChartWidget.getBarChartTooltipTotalText(widgetID);
  }
};
