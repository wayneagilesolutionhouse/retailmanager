'use strict';

var tableWidget = require('./common/table-widget.js');

module.exports = {
  columnHeadings: ['Selected period traffic', 'Change', 'Prior period traffic', 'Change', 'Prior year traffic'],

  widgetTitle: function() {
    return element(by.tagName('tenant-traffic-table-widget')).element(by.className('widget-title'));
  },

  clickExportButton: function() {
    var exportButton = element(by.tagName('tenant-traffic-table-widget')).element(by.className('export-widget')).element(by.css('a'));
    exportButton.click();
  },

  clickExpandButtonIfPresent: function() {
    return tableWidget.clickExpandButton('tenant-traffic-table-widget');
  },

  getFilteredTenantList: function() {
    return tableWidget.getFilteredItemList('tenant-traffic-table-widget', 'item in (trafficTableWidget.filteredItems = (trafficTableWidget.items ');
  },

  getFilter: function() {
    return tableWidget.getFilter('tenant-traffic-table-widget', 'trafficTableWidget.filterItems');
  },

  getCurrentColumnHeader: function(){
    return element(by.tagName('tenant-traffic-table-widget')).element(by.css('[ng-click="trafficTableWidget.orderBy(\'traffic\')"]'));
  },

  getPeriodChangeColumnHeader: function() {
    return element(by.tagName('tenant-traffic-table-widget')).element(by.css('[ng-click="trafficTableWidget.orderBy(\'comparisons\',0,\'percentageChangeReal\')"]'));
  },

  getPeriodColumnHeader: function() {
    return element(by.tagName('tenant-traffic-table-widget')).element(by.css('[ng-click="trafficTableWidget.orderBy(\'totals\', 0)"]'));
  },
  getYearChangeColumnHeader: function() {
    return element(by.tagName('tenant-traffic-table-widget')).element(by.css('[ng-click="trafficTableWidget.orderBy(\'comparisons\',1,\'percentageChangeReal\')"]'));
  },

  getYearColumnHeader: function() {
    return element(by.tagName('tenant-traffic-table-widget')).element(by.css('[ng-click="trafficTableWidget.orderBy(\'totals\', 1)"]'));
  },

  getColumnText: function(columnBinding) {
    return tableWidget.getColumnByBinding('tenant-traffic-table-widget', 'item in (trafficTableWidget.filteredItems = (trafficTableWidget.items ', columnBinding).getText();
  },

  getCurrentPeriodColumn: function() {
    return tableWidget.formatNumberArray(this.getColumnText('::item.traffic | formatNumber:0:trafficTableWidget.numberFormatName '));
  },

  getPriorPeriodColumn: function() {
    return tableWidget.formatNumberArray(this.getColumnText('::item.totals[0] | formatNumber:0:trafficTableWidget.numberFormatName '));
  },

  getPriorYearColumn: function() {
    return tableWidget.formatNumberArray(this.getColumnText('::item.totals[1] | formatNumber:0:trafficTableWidget.numberFormatName '));
  },

  getPriorPeriodDeltaColumn: function() {
    return tableWidget.formatDeltaColumn(this.getColumnText('::item.comparisons[0].percentageChange | formatNumber : 1 : trafficTableWidget.numberFormatName '));
  },

  getPriorYearDeltaColumn: function() {
    return tableWidget.formatDeltaColumn(this.getColumnText('::item.comparisons[1].percentageChange | formatNumber : 1 : trafficTableWidget.numberFormatName '));
  },

  calculatePriorPeriodDeltaColumn: function() {
    return tableWidget.calculateDelta(this.getCurrentPeriodColumn(), this.getPriorPeriodColumn());
  },

  calculatePriorYearDeltaColumn: function() {
    return tableWidget.calculateDelta(this.getCurrentPeriodColumn(), this.getPriorYearColumn());
  },

  //used in translation checks
  getFilterBarText: function() {
    return tableWidget.getFilter('tenant-traffic-table-widget', 'trafficTableWidget.filterItems').getAttribute('placeholder');
  },

  //used in translation checks
  getExpandButtonText: function() {
    return tableWidget.getExpandButtonText('tenant-traffic-table-widget');
  },

  //used in translation checks
  getContractButtonText: function() {
    return tableWidget.getContractButtonText('tenant-traffic-table-widget');
  }
};


