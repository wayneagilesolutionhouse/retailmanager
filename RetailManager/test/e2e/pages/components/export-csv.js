'use strict';

module.exports = {

  scheduledReportFields: {
    sendToEmail: element(by.className('schedule-form-recipient-field')),
    reportName: element(by.className('schedule-form-report-name-field')),
    reportMessage: element(by.className('schedule-form-report-message')),
    saveButton: element(by.className('export-action-save-schedule')),
    cancelButton: element(by.className('export-action-cancel-schedule'))
  },

  getExportSiteName: function() {
    return element(by.className('organization-csv-site-name')).getText();
  },

  getExportPageTitle: function(){
    var pageTitles = element.all(by.className('organization-csv-subtitle'));
    return pageTitles.filter(function(elem) {
      return elem.isDisplayed();
    }).first().getText();
  },

  getDateTimeHeaderText: function() {
    return element(by.css('.organization-csv-section-title.date-time')).getText();
  },

  getSelectMetricsHeaderText: function() {
    return element(by.css('.organization-csv-section-title.metrics')).getText();
  },

  getMetricTypeButtonText: function() {
    return element(by.css('.organization-csv-section-title.metrics')).all(by.css('button')).getText();
  },

  setMetricType: function(type) {
    if (type.toLowerCase() === 'visitor behavior') {
      element(by.css('metric-switch-selector')).all(by.repeater('group in ::vm.groups')).last().click();
    } else {
      element(by.css('metric-switch-selector')).all(by.repeater('group in ::vm.groups')).first().click();
    }
  },

  getMetrics: function(){
    var metricArray = element.all(by.className('csv-export-metric-selector'));
    return metricArray;
  },

  getOpHoursVisibleText: function() {
    return element(by.css('.csv-export-block.op-hours')).getText();
  },

  getOpHoursMenuOptions: function() {
    this.openOpHoursMenu();
    return element(by.className('op-hours-dropdown-options')).all(by.css('a'));
  },

  openOpHoursMenu: function() {
    var opHoursMenu = element(by.css('div.op-hours-selection-dropdown'));
    opHoursMenu.click();
  },

  //helper for refactor (due to translation tests) of setting exported CSV report "group by" value.  Some calls to
  // setExportGroupBy pass in a string, but function now expects a row number.
  getGroupByRowNum: function(frequency) {
    if (frequency === 'Hour') {
      return 0;
    } else if (frequency === 'Day') {
      return 1;
    } else if (frequency === 'Week') {
      return 2;
    } else if (frequency === 'Month') {
      return 3;
    } else if (frequency === 'Aggregate') {
      return 4;
    } else {
      return frequency;
    }
  },

  getExportGroupByOption: function(frequency){
    var frequencyRowNum = this.getGroupByRowNum(frequency);
    var groupByOption = element(by.css('div.groupby-selection-dropdown')).all(by.repeater('groupByChoice in vm.groupByChoices').row(frequencyRowNum));
    return groupByOption;
  },

  setExportGroupBy: function(frequency) {
    this.openExportGroupByMenu();
    this.getExportGroupByOption(frequency).click();
  },

  openExportGroupByMenu: function() {
    var groupByMenu = element(by.css('div.groupby-selection-dropdown'));
    groupByMenu.click();
  },

  getExportGroupByVisibleText: function() {
    return element(by.css('.csv-export-block.groupby')).getText();
  },

  getExportGroupByMenuOptions: function() {
    this.openExportGroupByMenu();
    return element(by.className('group-by-dropdown-options')).all(by.css('a'));
  },

  getTimeSelectorVisibleText: function() {
    return element(by.css('.csv-export-block.csv-time-period-selector')).getText();
  },

  getExportButton: function() {
    return element(by.className('csv-export-actions')).element(by.className('csv-export-action-export-csv'));
  },

  getScheduleReportButton: function() {
    return element(by.className('csv-export-actions')).element(by.className('csv-export-action-schedule-report'));
  },

  getEditScheduleSection: function() {
    return element(by.id('schedule-form'));
  },

  getEditScheduleInputPlaceholders: function() {
    return element(by.id('schedule-form')).all(by.css('input')).getAttribute('placeholder');
  },

  getEditScheduleMsgPlaceholder: function() {
    return element(by.id('schedule-form')).element(by.css('textarea')).getAttribute('placeholder');
  },

  getAreaPicker: function() {
    return element(by.className('csv-export-select-sites-dropdown-wrapper'));
  },

  getAreaPickerHeaderText: function(locationType) {
    if(locationType === 'zone') {
      var picker = element(by.css('.organization-csv-section.export-zone-picker'));
    } else if (locationType === 'site') {
      var picker = element(by.css('.organization-csv-section.export-site-picker'));
    } else {//location type is area
      var picker = element(by.css('.organization-csv-section.export-area-picker'));
    }
    return picker.element(by.css('h2')).getText();
  },

  getPickerSearchBar: function(locationType) {
    if(locationType === 'zone') {
      return this.getAreaPicker().element(by.className('zone-selector-header')).element(by.model('vm.filter'));
    } else if(locationType === 'site') {
      return this.getAreaPicker().element(by.className('site-selector-header')).element(by.model('vm.keyword'));
    } else {//location type is area
      return this.getAreaPicker().element(by.className('location-selector-header')).element(by.model('vm.filter'));
    }
  },

  getSelectAllBtn: function(locationType) {
    if(locationType === 'zone') {
      var picker =  this.getAreaPicker().element(by.className('zone-selector-content'));
    } else if(locationType === 'site') {
      var picker =  this.getAreaPicker().element(by.className('site-selector-content'));
    } else {//location type is area
      var picker =  this.getAreaPicker().element(by.className('location-selector-content'));
    }

    return picker.element(by.className('select-all')).element(by.tagName('button'));
  },

  getAreaPickerList: function(locationType) {
    if(locationType === 'zone') {
      return this.getAreaPicker().element(by.className('zone-selector-content')).all(by.repeater('zone in vm.allZones')).getText();
    } else if (locationType === 'site') {
      return this.getAreaPicker().element(by.className('site-selector-content')).all(by.repeater('site in vm.sites ')).getText();
    } else {//location type is area
      //the repeater "vm.getRootNodes..." below returns an array of arrays.  In order to comfirm that listInDropdown matches the list of areas on the page, the array must be flattened
      var listInDropdown = this.getAreaPicker().element(by.className('location-selector-content')).all(by.repeater('location in vm.getRootNodesThatShouldBeShown()')).getText();
      return listInDropdown.then(function(multiAreaArray){
        var masterArray = [];
        multiAreaArray.forEach(function(multiAreaString){
          var individualAreas = multiAreaString.split('\n');
          masterArray = masterArray.concat(individualAreas);
        });
        return masterArray;
      });
    }
  },

  getSelectedAreaList: function(locationType) {
    var showAllButton = element(by.className('csv-export-site-selector-show-hide-button'));

    if(locationType === 'zone') {
      return showAllButton.isDisplayed().then(function (isListCollapsed) {
        if (isListCollapsed) {
          showAllButton.click();
          return element(by.className('csv-export-site-selector-wrapper')).all(by.repeater('zoneId in vm.selectedZones track by $index')).getText();
        } else {
          return element(by.className('csv-export-site-selector-wrapper')).all(by.repeater('zoneId in vm.selectedZones track by $index')).getText();
        }
      });
    } else if(locationType === 'site') {
      return showAllButton.isDisplayed().then(function (isListCollapsed) {
        if (isListCollapsed) {
          showAllButton.click();
          return element(by.className('csv-export-site-selector-wrapper')).all(by.repeater('siteId in vm.selectedSites track by $index')).getText();
        } else {
          return element(by.className('csv-export-site-selector-wrapper')).all(by.repeater('siteId in vm.selectedSites track by $index')).getText();
        }
      });
    } else {//location type is area
      return showAllButton.isDisplayed().then(function (isListCollapsed) {
        if (isListCollapsed) {
          showAllButton.click();
          return element(by.className('csv-export-site-selector-wrapper')).all(by.repeater('locationId in vm.selectedLocations track by $index')).getText();
        } else {
          return element(by.className('csv-export-site-selector-wrapper')).all(by.repeater('locationId in vm.selectedLocations track by $index')).getText();
        }
      });
    }
  },

  setAreaPickerLocation: function(locationType, areaName) {
    var areaPicker = this.getAreaPicker();
    if(locationType === 'area') {
      var testArea = element(by.className('location-selector-popover')).element(by.partialLinkText(areaName));
    } else if (locationType === 'site') {
      var testArea = element(by.className('site-selector-popover')).element(by.partialLinkText(areaName));
    } else {
      var testArea = element(by.className('zone-selector-popover')).element(by.partialLinkText(areaName));
    }
    areaPicker.click();
    browser.waitForAngular();
    testArea.click();
    browser.executeScript('window.scrollTo(0,0);').then(function () {
      areaPicker.click();
    });
  },

  getFilterMenuTitleText: function() {
    return element(by.className('csv-export-tag-filter-widget')).element(by.css('h2')).getText();
  },

//acceptable "frequency" parameters include (case sensitive strings): 'Day', 'Week', 'Month', or 'Year'.  Corresponds to options in "frequency" dropdown in "edit schedule" section on export CSV page.
  scheduleTestCSV: function(frequency, siteName) {
    this.scheduledReportFields.sendToEmail.clear();
    this.scheduledReportFields.reportName.clear();
    this.scheduledReportFields.reportMessage.clear();
    this.scheduledReportFields.sendToEmail.sendKeys('jzimmerman@retailmanager.com');
    this.scheduledReportFields.reportName.sendKeys('test CSV report - '+ siteName);
    this.scheduledReportFields.reportMessage.sendKeys('Beep Beep I am a test report message. I should be readable and not gibberish.');
    this.setScheduledFrequency(frequency);
    this.scheduledReportFields.saveButton.click();
  },

//helper for refactor (due to translation tests) of setting scheduled report frequency.  Some calls to setScheduledFrequency pass in a string,
//but function now expects a row number.
  getFrequencyRowNum: function(frequency) {
    if (frequency === 'Daily') {
      return 0;
    } else if (frequency === 'Weekly') {
      return 1;
    } else if (frequency === 'Monthly') {
      return 2;
    } else if (frequency === 'Yearly') {
      return 3;
    } else {
      return frequency;
    }
  },

  openScheduledFrequencyMenu: function() {
    var frequencyPicker = element(by.id('export-frequency')).element(by.css('button'));
    frequencyPicker.click();
  },

  setScheduledFrequency: function(frequency) {
    this.openScheduledFrequencyMenu();
    var frequencyRowNum = this.getFrequencyRowNum(frequency);
    var interval = element(by.id('export-frequency')).all(by.repeater('frequency in vm.frequencyChoices').row(frequencyRowNum));
    interval.click();
  },

//gets list items in "edit schedule" frequency dropdown (daily, weekly, etc.)
  getScheduledFrequencyOptions: function() {
    this.openScheduledFrequencyMenu();
    return element(by.id('export-frequency')).all(by.repeater('frequency in vm.frequencyChoices'));
  },

//returns label text for options displayed after selecting a frequency option in dropdown; works with getFrequencyOptions____Label functions
  getFrequencyOptionLabel: function(frequencyRowNum) {
    this.setScheduledFrequency(frequencyRowNum);
    return element(by.id('export-frequency-options')).element(by.css('label'));
  },

//returns text for secondary frequency options shown after selecting a frequency in "edit schedule" dropdown
  getFrequencyOptionSettings: function() {
    return element(by.id('export-frequency-options')).all(by.css('button'));
  },

  getFrequencyOptionsWeeklyLabel: function() {
    return this.getFrequencyOptionLabel(1);
  },

  getFrequencyOptionsMonthlyLabel: function() {
    return this.getFrequencyOptionLabel(2);
  },

  getFrequencyOptionsYearlyLabel: function() {
    return this.getFrequencyOptionLabel(3);
  },

  openScheduledActiveMenu: function() {
    var activePicker = element(by.id('export-active')).element(by.css('button'));
    activePicker.click();
  },

  getScheduledActiveOptions: function() {
    this.openScheduledActiveMenu();
    return element(by.id('export-active')).all(by.repeater('active in vm.activeChoices'));
  },

  getScheduledCSVs: function() {
    return element.all(by.repeater('schedule in vm.schedules'));
  },

  getScheduledReportsHeader: function() {
    return element(by.className('csv-export-schedules-title'));
  },

  getScheduledReportsEmptyListMsg: function() {
    return element(by.className('organization-csv-no-reports'));
  },

  getScheduledReportListHeaders: function() {
    return element(by.className('organization-csv-scheduled-reports-header'));
  },

  removeScheduledCSV: function() {
    var removeIcon = element(by.repeater('schedule in vm.schedules')).element(by.css('span.csv-export-remove-icon'));
    removeIcon.click();
  },

  exportCSV: function() {
    this.getExportButton().click();
  }
};
