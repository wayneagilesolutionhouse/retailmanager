'use strict';

var tableWidget = require('./common/table-widget.js');

module.exports = {

  columnHeadings: ['Selected period UPT', 'Change', 'Prior period UPT', 'Change', 'Prior year UPT'],


  widgetTitle: function() {
    return element(by.tagName('tenant-upt-table-widget')).element(by.className('widget-title'));
  },

  clickExportButton: function() {
    var exportButton = element(by.tagName('tenant-upt-table-widget')).element(by.className('export-widget')).element(by.css('a'));
    exportButton.click();
  },

  clickExpandButtonIfPresent: function() {
    return tableWidget.clickExpandButton('tenant-upt-table-widget');
  },

  getFilteredTenantList: function() {
    return tableWidget.getFilteredItemList('tenant-upt-table-widget', 'item in (salesSummaryTableWidget.filteredItems = (salesSummaryTableWidget.items | sortObjectBy: salesSummaryTableWidget.orderBy : salesSummaryTableWidget.sortColumnIndex : salesSummaryTableWidget.sortChildColumn : salesSummaryTableWidget.childProperty | filter: salesSummaryTableWidget.filterItems))');
  },

  getFilter: function() {
    return tableWidget.getFilter('tenant-upt-table-widget', 'salesSummaryTableWidget.filterItems');
  },

  getCurrentColumnHeader: function(){
    return element(by.tagName('tenant-upt-table-widget')).element(by.css('[ng-click="salesSummaryTableWidget.doOrderBy(0, \'value\')"]'));
  },

  getPeriodChangeColumnHeader: function() {
    return element(by.tagName('tenant-upt-table-widget')).all(by.css('[ng-click="salesSummaryTableWidget.doOrderBy($index + 1, \'comparison\', \'percentageChangeReal\')"]')).first();
  },

  getPeriodColumnHeader: function() {
    return element(by.tagName('tenant-upt-table-widget')).all(by.css('[ng-click="salesSummaryTableWidget.doOrderBy($index + 1, \'value\')"]')).first();
  },
  getYearChangeColumnHeader: function() {
    return element(by.tagName('tenant-upt-table-widget')).all(by.css('[ng-click="salesSummaryTableWidget.doOrderBy($index + 2, \'comparison\', \'percentageChangeReal\')"]')).last();
  },

  getYearColumnHeader: function() {
    return element(by.tagName('tenant-upt-table-widget')).all(by.css('[ng-click="salesSummaryTableWidget.doOrderBy($index + 2, \'value\')"]')).last();
  },

  getColumnText: function(columnBinding) {
    return tableWidget.getColumnByBinding('tenant-upt-table-widget', 'item in (salesSummaryTableWidget.filteredItems = (salesSummaryTableWidget.items | sortObjectBy: salesSummaryTableWidget.orderBy : salesSummaryTableWidget.sortColumnIndex : salesSummaryTableWidget.sortChildColumn : salesSummaryTableWidget.childProperty | filter: salesSummaryTableWidget.filterItems))', columnBinding).getText();
  },

  getCurrentPeriodColumn: function() {
    return tableWidget.formatNumberArray(this.getColumnText('::item.periodValues[0].value | formatNumber:salesSummaryTableWidget.returnDataPrecision:salesSummaryTableWidget.numberFormatName | dashIfNull '))
  },

  getPriorPeriodColumn: function() {
    return tableWidget.formatNumberArray(this.getColumnText('::item.periodValues[1].value | formatNumber:salesSummaryTableWidget.returnDataPrecision:salesSummaryTableWidget.numberFormatName | dashIfNull '))
  },

  getPriorYearColumn: function() {
    return tableWidget.formatNumberArray(this.getColumnText('::item.periodValues[2].value | formatNumber:salesSummaryTableWidget.returnDataPrecision:salesSummaryTableWidget.numberFormatName | dashIfNull '))
  },

  getPriorPeriodDeltaColumn: function() {
    return tableWidget.formatDeltaColumn(this.getColumnText('::item.periodValues[1].comparison.percentageChange | formatNumber : 1 : salesSummaryTableWidget.numberFormatName'));
  },

  getPriorYearDeltaColumn: function() {
    return tableWidget.formatDeltaColumn(this.getColumnText('::item.periodValues[2].comparison.percentageChange | formatNumber : 1 : salesSummaryTableWidget.numberFormatName'));
  },

  calculatePriorPeriodDeltaColumn: function() {
    return tableWidget.calculateDelta(this.getCurrentPeriodColumn(), this.getPriorPeriodColumn());
  },

  calculatePriorYearDeltaColumn: function() {
    return tableWidget.calculateDelta(this.getCurrentPeriodColumn(), this.getPriorYearColumn());
  },

  //used in translation checks
  getFilterBarText: function() {
    return tableWidget.getFilter('tenant-upt-table-widget', 'salesSummaryTableWidget.filterItems').getAttribute('placeholder');
  },

  //used in translation checks
  getExpandButtonText: function() {
    return tableWidget.getExpandButtonText('tenant-upt-table-widget');
  },

  //used in translation checks
  getContractButtonText: function() {
    return tableWidget.getContractButtonText('tenant-upt-table-widget')
  }
};


