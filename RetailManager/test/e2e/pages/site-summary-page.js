'use strict';

var orgData = require('../data/orgs.js');

module.exports = {

  //for "usage of areas" tab - these are pre-determined location types, NOT button text
  areaTypeFilterButtons: ['store', 'corridor', 'entrance', 'all'],

  siteTitle: function () {
    return element(by.className('title-specifier'));
  },

  getAreaName: function(){
    return element(by.className('title-container')).element(by.className('title')).getText();
  },

  //for "usage of areas" tab
  getAreaTypeFilter: function (selectorText) {
    var selector = 'selector-item-location-type-' + selectorText;
    return element(by.className('location-type-selector')).element(by.className(selector));
  },

  //for "usage of areas" tab
  clickAreaTypeFilter: function (selectorText) {
    browser.executeScript('window.scrollTo(0,0);').then(function () {
      this.getAreaTypeFilter(selectorText).click();
    }.bind(this));
  },

  navToTestArea: function () {
    navToSelectArea(orgData.SSOrg.testArea);
  },

  navToZone: function(){
    navToZone(orgData.MSOrgSite.testZone);
  },

  // navToOverall: function() {
  //   var picker = element(by.className('analytics-header')).element(by.css('button.ng-isolate-scope'));
  //   picker.click();
  //   var zone = element(by.className('analytics-header')).element(by.partialLinkText('Property overall'));
  //   zone.click();
  // },

  //used in translation checks
  getZonePickerTitleText: function(){
    //multiple instances of class "toolbar-label" - this should reference the first instance
    return element(by.className('analytics-header')).element(by.className('toolbar')).all(by.className('toolbar-label')).first().getText();
  },

  //used in translation checks
  getZonePickerSearchText: function() {
    var picker = element(by.className('analytics-header')).element(by.css('button.zone-selector'));
    picker.click();
    var searchText = element(by.className('analytics-header')).element(by.css('div.zone-selector-header')).element(by.css('input')).getAttribute('placeholder');
    picker.click();
    return searchText;
  },

  //used in translation checks
  getAreaPickerSearchText: function() {
    var picker = element(by.className('analytics-header')).element(by.css('button.area-selector'));
    picker.click();
    var searchText = element(by.className('analytics-header')).element(by.css('div.location-selector-header')).element(by.css('input')).getAttribute('placeholder');
    picker.click();
    return searchText;
  },

  //used in translation checks
  getHoursPickerTitleText: function() {
    return element(by.className('analytics-header')).element(by.className('date-range-detail-selector')).element(by.css('span.selector-label')).getText();
  },

  //used in translation checks
  getHoursPickerOptionText: function() {
    var dropdown = element(by.className('date-range-detail-selector')).element(by.css('button'));
    dropdown.click();

    return element(by.className('date-range-detail-selector')).element(by.className('dropdown-menu')).getText().then(function(text){
      dropdown.click();
      return text.split('\n');
    });
  },

  getCompStoresSelectorText: function() {
    return element(by.className('retail-organization-comp-stores')).getText();
  }
};

function navToSelectArea(areaName) {
  var picker = element(by.className('toolbar')).element(by.className('title'));
  picker.click();
  browser.waitForAngular();
  var loc = element(by.className('toolbar')).element(by.className('location-navigation-popover')).element(by.linkText(areaName));
  loc.click();
  browser.waitForAngular();
}

function navToZone(zoneName){
  var picker = element(by.className('analytics-header')).element(by.css('button.zone-selector'));
  picker.click();
  browser.waitForAngular();
  var zone = element(by.className('analytics-header')).element(by.css('div.zone-tree-container')).element(by.partialLinkText(zoneName));
  zone.click();
  browser.waitForAngular();
}
