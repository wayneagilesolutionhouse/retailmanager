'use strict';

module.exports = {
//shared tests for localization translations, using prior period/prior year date ranges

  exportCSVSharedLangTests: function (locale) {

    describe('Export CSV tab (shared translation tests)', function () {

      var translationsFilePath = '../../../src/l10n/languages/' + locale + '.json'; //sets up filepath for required translations file
      var translations = require(translationsFilePath);

      var moment = require('moment');
      var orgData = require('../data/orgs.js');
      var userData = require('../data/users.js');
      var login = require('../pages/login.js');
      var nav = require('../pages/components/nav-header.js');
      var sitePage = require('../pages/site-summary-page.js');
      var tabNav = require('../pages/components/tab-nav.js');
      var dateSelector = require('../pages/components/time-period-picker.js');
      var exportCSVPage = require('../pages/components/export-csv.js');
      var filterPicker = require('../pages/components/filter-picker.js');
      var fs = require('fs');

      describe('general export page text', function () {

        beforeAll(function () {
          nav.navToExportCSV();
        });

        it('should show correct site name', function () {
          var siteName = exportCSVPage.getExportSiteName();
          expect(siteName).toEqual(orgData.MSOrgSite.name);
        });

        it('Export PDF page title', function () {
          var pageTitle = exportCSVPage.getExportPageTitle();
          expect(pageTitle).toEqual(translations.csvExportView.EXPORTCSV.toUpperCase());
        });

        it('"date and time settings" text', function() {
          var header = exportCSVPage.getDateTimeHeaderText();
          expect(header).toEqual(translations.csvExportView.DATEANDTIMESETTINGS);
        });

        it('operating hours dropdown', function() {
          var opHoursHeader = exportCSVPage.getOpHoursVisibleText();
          var opHoursMenu = exportCSVPage.getOpHoursMenuOptions();

          expect(opHoursHeader).toMatch(translations.csvExportView.OPERATINGHOURS.toUpperCase());
          expect(opHoursMenu.getText()).toEqual([translations.csvExportView.BUSINESSHOURS, translations.csvExportView.BUSINESSDAYS]);
        });

        it('group by dropdown', function() {
          var groupByHeader = exportCSVPage.getExportGroupByVisibleText();
          var groupByMenu = exportCSVPage.getExportGroupByMenuOptions();

          expect(groupByHeader).toMatch(translations.csvExportView.GROUPBY.toUpperCase());
          expect(groupByMenu.getText()).toEqual([
            translations.common.HOUR,
            translations.common.DAY,
            translations.common.WEEK,
            translations.common.MONTH,
            translations.common.AGGREGATE
          ]);
        });

        it('time period selector header', function() {
          var pickerHeader = exportCSVPage.getTimeSelectorVisibleText();
          expect(pickerHeader).toMatch(translations.csvExportView.SELECTTIMEPERIOD.toUpperCase());
        });

        it('"select metrics" header', function(){
          var header = exportCSVPage.getSelectMetricsHeaderText();

          expect(header).toMatch(translations.csvExportView.SELECTMETRICS);
        });

        it('"export" and "schedule report" buttons', function() {
          var exportBtn = exportCSVPage.getExportButton();
          var scheduleReportBtn = exportCSVPage.getScheduleReportButton();

          expect(exportBtn.getText()).toEqual(translations.csvExportView.EXPORTCSV);
          expect(scheduleReportBtn.getText()).toEqual(translations.csvExportView.SCHEDULEREPORT);
        });

        //common date-picker text translation checks
        describe('time period selector text', function() {

          beforeAll(function() {
            dateSelector.toggleExportCSVDatePicker();
          });

          it('date field headers', function() {
            var fromField = dateSelector.getDateFieldHeaders('from');
            var toField = dateSelector.getDateFieldHeaders('to');

            expect(fromField.getText()).toEqual(translations.dateRangePicker.FROM.toUpperCase());
            expect(toField.getText()).toEqual(translations.dateRangePicker.TO.toUpperCase());
          });

          it('shortcuts dropdown', function() {
            var shortcutsHeader = dateSelector.getDateShortcutsVisibleText();
            var shortcutsMenu = dateSelector.getDateShortcutsMenuOptions();

            expect(shortcutsHeader).toMatch(translations.dateRangePicker.SHORTCUTS);
            expect(shortcutsMenu.getText()).toEqual([
              translations.dateRangePicker.WEEKTODATE,
              translations.dateRangePicker.MONTHTODATE,
              translations.dateRangePicker.QUARTERTODATE,
              translations.dateRangePicker.YEARTODATE

            ]);
          });

          it('apply and cancel buttons', function() {
            var applyButton = dateSelector.getApplyOrCancelButton('apply');
            var cancelButton = dateSelector.getApplyOrCancelButton('cancel');

            expect(applyButton.getText()).toEqual(translations.dateRangePicker.APPLY);
            expect(cancelButton.getText()).toEqual(translations.dateRangePicker.CANCEL);
          });

          afterAll(function(){
            dateSelector.toggleExportCSVDatePicker();
          });
        });
      });

      describe('"edit schedule" dialogue text', function() {

        beforeAll(function() {
          //setup for schedule report dialogue -
          // select at least one metric and zone to activate schedule button;
          var metrics = exportCSVPage.getMetrics();
          metrics.then(function (array) {
            return array[0].click();
          });

          exportCSVPage.setAreaPickerLocation('zone', 'New Yorker');
          exportCSVPage.getScheduleReportButton().click();
        });

        it('(fails until until SA-130 is fixed) "edit schedule" dialogue text', function() {
          var editScheduleText = exportCSVPage.getEditScheduleSection();

          //todo: "edit schedule" expectation below fails until SA-130 is fixed
          expect(editScheduleText.getText()).toMatch(translations.csvExportView.EDITSCHEDULE);
          expect(editScheduleText.getText()).toMatch(translations.scheduleReport.FROM);
          expect(editScheduleText.getText()).toMatch(translations.scheduleReport.TO);
          expect(editScheduleText.getText()).toMatch(translations.scheduleReport.RECIPIENT);
          expect(editScheduleText.getText()).toMatch(translations.scheduleReport.ADDRECIPIENT);
          expect(editScheduleText.getText()).toMatch(translations.scheduleReport.REPORTNAME);
          expect(editScheduleText.getText()).toMatch(translations.scheduleReport.FREQUENCY);
          expect(editScheduleText.getText()).toMatch(translations.common.WEEKLY);
          expect(editScheduleText.getText()).toMatch(translations.scheduleReport.MESSAGE);
          expect(editScheduleText.getText()).toMatch(translations.scheduleReport.ACTIVE);
          expect(editScheduleText.getText()).toMatch(translations.scheduleReport.MONTHS);
          expect(editScheduleText.getText()).toMatch(translations.scheduleReport.SAVESCHEDULE);
          expect(editScheduleText.getText()).toMatch(translations.scheduleReport.CANCEL);
        });

        it('input field placeholders', function () {
          var inputPlaceholders = exportCSVPage.getEditScheduleInputPlaceholders();
          var messagePlaceholder = exportCSVPage.getEditScheduleMsgPlaceholder();

          expect(inputPlaceholders).toEqual([
            'noreply@retailmanager.com',
            'jzimmerman@retailmanager.com',
            translations.scheduleReport.ADDONEEMAIL,
            '',
            ''
          ]);
          expect(messagePlaceholder).toEqual('');
        });

        it('frequency dropdown options', function () {
          var frequencyOptions = exportCSVPage.getScheduledFrequencyOptions();
          expect(frequencyOptions.getText()).toEqual([
            translations.common.DAILY,
            translations.common.WEEKLY,
            translations.common.MONTHLY,
            translations.common.YEARLY
          ]);

          exportCSVPage.openScheduledFrequencyMenu();
        });

        it('secondary frequency options - weekly', function () {
          var frequencyWeeklyHeader = exportCSVPage.getFrequencyOptionsWeeklyLabel();
          var frequencyWeeklySettings = exportCSVPage.getFrequencyOptionSettings();

          expect(frequencyWeeklyHeader.getText()).toEqual(translations.scheduleReport.DAYOFWEEK);
          expect(frequencyWeeklySettings.getText()).toEqual([
            translations.weekdaysLong.sun,
            translations.weekdaysLong.mon,
            translations.weekdaysLong.tue,
            translations.weekdaysLong.wed,
            translations.weekdaysLong.thu,
            translations.weekdaysLong.fri,
            translations.weekdaysLong.sat
          ]);
        });

        it('secondary frequency options - monthly', function () {
          var frequencyMonthlyHeader = exportCSVPage.getFrequencyOptionsMonthlyLabel();
          var frequencyMonthlySettings = exportCSVPage.getFrequencyOptionSettings();

          expect(frequencyMonthlyHeader.getText()).toEqual(translations.scheduleReport.DAYOFMONTH);
          expect(frequencyMonthlySettings.getText()).toEqual([
            translations.common.FIRST,
            translations.common['15TH']
          ]);
        });

        it('secondary frequency options - yearly', function () {
          var frequencyYearlyHeader = exportCSVPage.getFrequencyOptionsYearlyLabel();
          var frequencyYearlySettings = exportCSVPage.getFrequencyOptionSettings();

          expect(frequencyYearlyHeader.getText()).toEqual(translations.common.MONTH);
          expect(frequencyYearlySettings.getText()).toEqual([
            translations.monthsLong.january,
            translations.monthsLong.february,
            translations.monthsLong.march,
            translations.monthsLong.april,
            translations.monthsLong.may,
            translations.monthsLong.june,
            translations.monthsLong.july,
            translations.monthsLong.august,
            translations.monthsLong.september,
            translations.monthsLong.october,
            translations.monthsLong.november,
            translations.monthsLong.december
          ]);
        });

        it('active dropdown options', function () {
          var activeOptions = exportCSVPage.getScheduledActiveOptions();
          expect(activeOptions.getText()).toEqual([
            '6 ' + translations.scheduleReport.MONTHS,
            '12 ' + translations.scheduleReport.MONTHS,
            '18 ' + translations.scheduleReport.MONTHS
          ]);

          exportCSVPage.openScheduledActiveMenu();
        });

        it('"save schedule" and "cancel" buttons', function() {
          var saveButton = exportCSVPage.scheduledReportFields.saveButton;
          var cancelButton = exportCSVPage.scheduledReportFields.cancelButton;

          expect(saveButton.getText()).toEqual(translations.scheduleReport.SAVESCHEDULE);
          expect(cancelButton.getText()).toEqual(translations.scheduleReport.CANCEL);
        });

        it('"scheduled reports" list header', function () {
          var listHeader = exportCSVPage.getScheduledReportsHeader();
          expect(listHeader.getText()).toEqual(translations.csvExportView.SCHEDULEDCSVREPORTS);
        });

        it('"scheduled reports" list message, when list is empty', function () {
          //uncertain starting state - list may contain scheduled reports or may be empty
          var initialScheduledReports = exportCSVPage.getScheduledCSVs();

          initialScheduledReports.then(function (list) {
            //check that scheduled report list is empty, otherwise element in expectation will not be present
            if (list.length !== 0) {
              list.forEach(function () {
                exportCSVPage.removeScheduledCSV();
              });
            }
            var emptyListMsg = exportCSVPage.getScheduledReportsEmptyListMsg();
            expect(emptyListMsg.getText()).toEqual(translations.csvExportView.NOSAVEDREPORTS);
          });
        });

        it('"scheduled reports" list text, when list is populated', function () {
          //uncertain starting state - list may contain scheduled reports or may be empty

          var initialScheduledReports = exportCSVPage.getScheduledCSVs();

          initialScheduledReports.then(function (list) {

            if (list.length === 0) {//if list is empty, schedule a report
              exportCSVPage.scheduleTestCSV('Weekly', orgData.MSOrgSite.name);
            }

            var listHeaders = exportCSVPage.getScheduledReportListHeaders();
            expect(listHeaders.getText()).toMatch(translations.csvExportView.REPORTNAME);
            expect(listHeaders.getText()).toMatch(translations.csvExportView.FREQUENCY);
            expect(listHeaders.getText()).toMatch(translations.csvExportView.BY);

            exportCSVPage.removeScheduledCSV();
          });
        });

      });

      describe('dynamic metric/location text dependant on export source', function() {

        describe('site-level mall org with tenant zones', function() {

          beforeAll(function() {
            browser.executeScript('window.scrollTo(0,0);');
          });

          it('metric-type selector button', function() {
            var header = exportCSVPage.getMetricTypeButtonText();

            expect(header).toEqual([translations.csvExportView.PERIMETER.toUpperCase()]);
          });

          it('metrics for export', function() {
            var metrics = exportCSVPage.getMetrics();
            const expectedMetrics = orgData.MSOrgSite.metrics.csvTranslationKeys.map(key => {
              return orgData.getNestedProp(translations, key);
            });

            expect(metrics.getText()).toEqual(expectedMetrics);
          });

          it('location picker visible text', function() {
            var header = exportCSVPage.getAreaPickerHeaderText('zone');
            var pickerButton = exportCSVPage.getAreaPicker();

            expect(header.getText()).toEqual(translations.csvExportView.SELECTZONES);
            expect(pickerButton.getText()).toEqual(translations.csvExportView.SELECTZONES);
          });

          it('location picker dropdown text', function() {
            exportCSVPage.getAreaPicker().click();

            var searchBar = exportCSVPage.getPickerSearchBar('zone');
            var selectAllBtn = exportCSVPage.getSelectAllBtn('zone');

            expect(searchBar.getAttribute('placeholder')).toMatch(translations.locationSelector.SEARCH);
            expect(selectAllBtn.getText()).toEqual(translations.locationSelector.SELECTALL);
          });
        });

        describe('single site org with interior locations', function() {

          beforeAll(function() {
            // nav to: org Mandalay bay, site The Shoppes at Mandalay Place, traffic tab
            browser.get('#/'+orgData.SSOrg.id+'/'+orgData.SSOrg.ssOrgSiteId+'/traffic?dateRangeStart='+dateSelector.getURLDate('week', true)+'&dateRangeEnd='+dateSelector.getURLDate('week', false)+'&compareRange1Start='+dateSelector.getURLDate('week', true, 1)+'&compareRange1End='+dateSelector.getURLDate('week', false, 1)+'&compareRange2Start='+dateSelector.getURLDate('week', true, 2)+'&compareRange2End='+dateSelector.getURLDate('week', false, 2));
            nav.navToExportCSV();
          });

          it('metric-type selector button', function() {
            var header = exportCSVPage.getMetricTypeButtonText();

            expect(header).toEqual([translations.csvExportView.PERIMETER.toUpperCase(), translations.csvExportView.VISITORBEHAVIOR.toUpperCase()]);
          });

          it('metrics for export', function() {
            var metrics = exportCSVPage.getMetrics();
            const expectedPerimeterMetrics = orgData.SSOrg.metrics.csvTranslationKeys.perimeter.map(key => {
              return orgData.getNestedProp(translations, key);
            });
            const expectedVBMetrics = orgData.SSOrg.metrics.csvTranslationKeys.visitorBehavior.map(key => {
              return orgData.getNestedProp(translations, key);
            });

            expect(metrics.getText()).toEqual([
              translations.kpis.kpiTitle.traffic
            ]);

            exportCSVPage.setMetricType('visitor behavior');

            expect(metrics.getText()).toEqual(expectedVBMetrics);
          });

          it('location picker visible text', function() {
            var header = exportCSVPage.getAreaPickerHeaderText();
            var pickerButton = exportCSVPage.getAreaPicker();

            expect(header.getText()).toEqual(translations.csvExportView.SELECTAREAS);
            expect(pickerButton.getText()).toEqual(translations.csvExportView.SELECTAREAS);
          });

          it('location picker dropdown text', function() {
            exportCSVPage.getAreaPicker().click();

            var searchBar = exportCSVPage.getPickerSearchBar();
            var selectAllBtn = exportCSVPage.getSelectAllBtn();

            expect(searchBar.getAttribute('placeholder')).toEqual(translations.locationSelector.SEARCH);
            expect(selectAllBtn.getText()).toEqual(translations.locationSelector.SELECTALL);
          });
        });

        describe('retail org with retail sites', function() {

          beforeAll(function() {
            // nav to: org North Face, retail org summary page
            browser.get('#/' + orgData.MSRetailOrg.id + '/retail?dateRangeStart='+dateSelector.getURLDate('week', true)+'&dateRangeEnd='+dateSelector.getURLDate('week', false)+'&compareRange1Start='+dateSelector.getURLDate('week', true, 1)+'&compareRange1End='+dateSelector.getURLDate('week', false, 1)+'&compareRange2Start='+dateSelector.getURLDate('week', true, 2)+'&compareRange2End='+dateSelector.getURLDate('week', false, 2));
            nav.navToExportCSV();
          });

          //already checked metric button via site-level mall org test block above

          it('metrics for export', function() {
            var metrics = exportCSVPage.getMetrics();
            const expectedMetrics = orgData.MSRetailOrg.metrics.csvTranslationKeys.map(key => {
              return orgData.getNestedProp(translations, key);
            });

            expect(metrics.getText()).toEqual(expectedMetrics);
          });

          it('location picker visible text', function() {
            var header = exportCSVPage.getAreaPickerHeaderText('site');
            var pickerButton = exportCSVPage.getAreaPicker();

            expect(header.getText()).toEqual(translations.csvExportView.SELECTSITES);
            expect(pickerButton.getText()).toEqual(translations.csvExportView.SELECTSITES);
          });

          it('location picker dropdown text', function() {
            exportCSVPage.getAreaPicker().click();

            var searchBar = exportCSVPage.getPickerSearchBar('site');
            var selectAllBtn = exportCSVPage.getSelectAllBtn('site');

            expect(searchBar.getAttribute('placeholder')).toEqual(translations.locationSelector.SEARCH);
            expect(selectAllBtn.getText()).toEqual(translations.locationSelector.SELECTALL);

            exportCSVPage.getAreaPicker().click(); //revert state before testing filter picker
          });

          //filter picker is present only at org-level export from retail orgs
          it('filter picker', function() {
            var titleText = exportCSVPage.getFilterMenuTitleText();

            expect(titleText).toEqual(translations.csvExportView.SELECTFILTERS);

            var menuHeader = filterPicker.getFilterMenuText().getText();

            menuHeader.then(function(textArray) {
              expect(textArray[0]).toEqual(translations.common.FILTERS);
              expect(textArray[1]).toMatch(translations.common.FILTERSAPPLIED.toUpperCase());
              expect(textArray[2]).toMatch(translations.common.SHOWING.toUpperCase());
              expect(textArray[2]).toMatch(translations.common.SITES.toUpperCase());
            });
          });

          it('filter picker button text', function() {
            filterPicker.openFilterMenu();

            var applyBtn = filterPicker.getFilterMenuApplyBtn();
            var cancelBtn = filterPicker.getFilterMenuClearBtn();

            expect(applyBtn.getText()).toEqual(translations.common.APPLY);
            expect(cancelBtn.getText()).toEqual(translations.common.CLEAR);
          });
        });
      });
    });
  }
};
