// shared tests for localization translations, using custom compare periods

const sitePage = require('../pages/site-summary-page.js');
const dateSelector = require('../pages/components/time-period-picker.js');
const nav = require('../pages/components/nav-header.js');
const tabSidebar = require('../pages/components/tab-nav.js');
const orgData = require('../data/orgs.js');
const userData = require('../data/users.js');
const entranceSummaryWidget = require('../pages/components/entrance-summary-widget.js');
const tenantSummaryWidget = require('../pages/components/tenant-traffic-summary-widget.js');
const entranceContributionWidget = require('../pages/components/entrance-contribution-widget.js');
const commonAreasSummaryWidget = require('../pages/components/common-areas-summary-widget.js');
const powerHoursWidget = require('../pages/components/power-hours-widget.js');
const dailyMetricBarWidget = require('../pages/components/daily-metric-averages-widget.js');
const trafficWidget = require('../pages/components/site-traffic-widget.js');

module.exports = {

  trafficSharedLangCustomPdTests(locale) {
    // sets up filepath for required translations file
    const translationsFilePath = `../../../src/l10n/languages/${locale}.json`;
    const translations = require(translationsFilePath);

    describe('Traffic tab (shared translation tests, custom periods)', () => {
      it('should navigate to the correct site', () => {
        const title = nav.getSiteName();

        expect(title).toEqual(orgData.MSOrgSite.name);
      });

      describe('Traffic widget', () => {
        it('date range titles in chart legend', () => {
          // working around trafficWidget's refactor of getLegendText()
          const lowercasePeriodNames = [translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.CUSTOMCOMPARE1.toLowerCase(), translations.common.CUSTOMCOMPARE2.toLowerCase()];
          expect(trafficWidget.getLegendTextLowerCase()).toEqual(lowercasePeriodNames);
        });

        it('date range titles in chart summary frame', () => {
          const selectedPeriodLabel = trafficWidget.getSummaryFrameSelectedPeriodLabel();
          const priorPeriodLabel = trafficWidget.getSummaryFrameCompare1Label();
          const priorYearLabel = trafficWidget.getSummaryFrameCompare2Label();

          expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
          expect(priorPeriodLabel).toEqual(translations.common.CUSTOMCOMPARE1.toUpperCase());
          expect(priorYearLabel).toEqual(translations.common.CUSTOMCOMPARE2.toUpperCase());
        });
      });

      describe('Entrance summary widget', () => {
        it('header text on table', () => {
          expect(entranceSummaryWidget.getCurrentColumnHeader().getText()).toEqual(translations.entranceContributionWidget.CURRENTPERIOD);
          expect(entranceSummaryWidget.getPeriodChangeColumnHeader().getText()).toEqual(translations.entranceContributionWidget.CHANGE);
          expect(entranceSummaryWidget.getPeriodColumnHeader().getText()).toEqual(`${translations.common.CUSTOMCOMPARE1} ${translations.entranceContributionWidget.TRAFFIC}`);
          expect(entranceSummaryWidget.getYearChangeColumnHeader().getText()).toEqual(translations.entranceContributionWidget.CHANGE);
          expect(entranceSummaryWidget.getYearColumnHeader().getText()).toEqual(`${translations.common.CUSTOMCOMPARE2} ${translations.entranceContributionWidget.TRAFFIC}`);
        });
      });

      describe('Daily average metric widget', () => {
        it('date range titles in chart legend', () => {
          expect(dailyMetricBarWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD, translations.common.CUSTOMCOMPARE1, translations.common.CUSTOMCOMPARE2]);
        });

        it('date range titles in tooltip', () => {
          expect(dailyMetricBarWidget.getTooltipTotalText()).toEqual([translations.common.SELECTEDPERIOD, translations.common.CUSTOMCOMPARE1, translations.common.CUSTOMCOMPARE2]);
        });
      });

      describe('Tenant summary widget', () => {
        it('header text on table', () => {
          expect(tenantSummaryWidget.getCurrentColumnHeader().getText()).toEqual(`${translations.common.SELECTEDPERIOD} ${translations.trafficTableWidget.TRAFFIC}`);
          expect(tenantSummaryWidget.getPeriodChangeColumnHeader().getText()).toEqual(translations.trafficTableWidget.CHANGE);
          expect(tenantSummaryWidget.getPeriodColumnHeader().getText()).toEqual(`${translations.common.CUSTOMCOMPARE1} ${translations.trafficTableWidget.TRAFFIC}`);
          expect(tenantSummaryWidget.getYearChangeColumnHeader().getText()).toEqual(translations.trafficTableWidget.CHANGE);
          expect(tenantSummaryWidget.getYearColumnHeader().getText()).toEqual(`${translations.common.CUSTOMCOMPARE2} ${translations.trafficTableWidget.TRAFFIC}`);
        });
      });

      describe('Common areas widget', () => {
        it('header text on table', () => {
          expect(commonAreasSummaryWidget.getCurrentColumnHeader().getText()).toEqual(`${translations.common.SELECTEDPERIOD} ${translations.trafficTableWidget.TRAFFIC}`);
          expect(commonAreasSummaryWidget.getPeriodChangeColumnHeader().getText()).toEqual(translations.trafficTableWidget.CHANGE);
          expect(commonAreasSummaryWidget.getPeriodColumnHeader().getText()).toEqual(`${translations.common.CUSTOMCOMPARE1} ${translations.trafficTableWidget.TRAFFIC}`);
          expect(commonAreasSummaryWidget.getYearChangeColumnHeader().getText()).toEqual(translations.trafficTableWidget.CHANGE);
          expect(commonAreasSummaryWidget.getYearColumnHeader().getText()).toEqual(`${translations.common.CUSTOMCOMPARE2} ${translations.trafficTableWidget.TRAFFIC}`);
        });
      });
    });
  }
};
