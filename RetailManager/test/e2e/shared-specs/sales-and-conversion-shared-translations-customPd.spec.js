'use strict';

module.exports = {
//shared tests for localization translations, using custom compare periods

  salesAndConversionSharedLangCustomPdTests: function (locale) {

    describe('Sales and Conversion tab (shared translation tests, custom periods)', function () {

      var translationsFilePath = '../../../src/l10n/languages/' + locale + '.json'; //sets up filepath for required translations file
      var translations = require(translationsFilePath);


      var sitePage = require('../pages/site-summary-page.js');
      var dateSelector = require('../pages/components/time-period-picker.js');
      var nav = require('../pages/components/nav-header.js');
      var tabSidebar = require('../pages/components/tab-nav.js');
      var orgData = require('../data/orgs.js');
      var userData = require('../data/users.js');
      var tenantSalesWidget = require('../pages/components/tenant-sales-summary-widget.js');
      var tenantConversionSumWidget = require('../pages/components/tenant-conversion-summary-widget.js');
      var tenantATSSumWidget = require('../pages/components/tenant-ats-summary-widget.js');
      var tenantUPTSumWidget = require('../pages/components/tenant-upt-summary-widget.js');
      var zoneSalesWidget = require('../pages/components/zone-sales-widget.js');
      var zoneConversionWidget = require('../pages/components/zone-conversion-widget.js');
      var averagePurchaseWidget = require('../pages/components/average-purchase-widget.js');
      var zoneUPTWidget = require('../pages/components/zone-units-per-transaction-widget.js');

      describe('"Property overall"-level tests', function () {

        describe('Tenant sales summary widget', function () {

          it('header text on table', function () {
            expect(tenantSalesWidget.getCurrentColumnHeader().getText()).toEqual(translations.common.SELECTEDPERIOD + ' ' + translations.kpis.shortKpiTitles.tenant_sales);
            expect(tenantSalesWidget.getPeriodChangeColumnHeader().getText()).toEqual(translations.salesTableWidget.CHANGE);
            expect(tenantSalesWidget.getPeriodColumnHeader().getText()).toEqual(translations.common.CUSTOMCOMPARE1 + ' ' + translations.kpis.shortKpiTitles.tenant_sales);
            expect(tenantSalesWidget.getYearChangeColumnHeader().getText()).toEqual(translations.salesTableWidget.CHANGE);
            expect(tenantSalesWidget.getYearColumnHeader().getText()).toEqual(translations.common.CUSTOMCOMPARE2 + ' ' + translations.kpis.shortKpiTitles.tenant_sales);
          });
        });

        describe('Tenant conversion summary widget', function () {

          it('header text on table', function () {
            expect(tenantConversionSumWidget.getCurrentColumnHeader().getText()).toEqual(translations.common.SELECTEDPERIOD + ' ' + translations.kpis.shortKpiTitles.tenant_conversion);
            expect(tenantConversionSumWidget.getPeriodChangeColumnHeader().getText()).toEqual(translations.salesTableWidget.CHANGE);
            expect(tenantConversionSumWidget.getPeriodColumnHeader().getText()).toEqual(translations.common.CUSTOMCOMPARE1 + ' ' + translations.kpis.shortKpiTitles.tenant_conversion);
            expect(tenantConversionSumWidget.getYearChangeColumnHeader().getText()).toEqual(translations.salesTableWidget.CHANGE);
            expect(tenantConversionSumWidget.getYearColumnHeader().getText()).toEqual(translations.common.CUSTOMCOMPARE2 + ' ' + translations.kpis.shortKpiTitles.tenant_conversion);
          });
        });

        describe('Tenant ATS summary widget', function () {

          it('header text on table', function () {
            expect(tenantATSSumWidget.getCurrentColumnHeader().getText()).toEqual(translations.common.SELECTEDPERIOD + ' ' + translations.kpis.shortKpiTitles.tenant_ats);
            expect(tenantATSSumWidget.getPeriodChangeColumnHeader().getText()).toEqual(translations.salesTableWidget.CHANGE);
            expect(tenantATSSumWidget.getPeriodColumnHeader().getText()).toEqual(translations.common.CUSTOMCOMPARE1 + ' ' + translations.kpis.shortKpiTitles.tenant_ats);
            expect(tenantATSSumWidget.getYearChangeColumnHeader().getText()).toEqual(translations.salesTableWidget.CHANGE);
            expect(tenantATSSumWidget.getYearColumnHeader().getText()).toEqual(translations.common.CUSTOMCOMPARE2 + ' ' + translations.kpis.shortKpiTitles.tenant_ats);
          });
        });

        describe('Tenant UPT summary widget', function () {

          it('header text on table', function () {
            expect(tenantUPTSumWidget.getCurrentColumnHeader().getText()).toEqual(translations.common.SELECTEDPERIOD + ' ' + translations.kpis.shortKpiTitles.tenant_upt);
            expect(tenantUPTSumWidget.getPeriodChangeColumnHeader().getText()).toEqual(translations.salesTableWidget.CHANGE);
            expect(tenantUPTSumWidget.getPeriodColumnHeader().getText()).toEqual(translations.common.CUSTOMCOMPARE1 + ' ' + translations.kpis.shortKpiTitles.tenant_upt);
            expect(tenantUPTSumWidget.getYearChangeColumnHeader().getText()).toEqual(translations.salesTableWidget.CHANGE);
            expect(tenantUPTSumWidget.getYearColumnHeader().getText()).toEqual(translations.common.CUSTOMCOMPARE2 + ' ' + translations.kpis.shortKpiTitles.tenant_upt);
          });
        });
      });

      describe('Zone-level tests', function () {

        beforeAll(function () {
          browser.executeScript('window.scrollTo(0,0);').then(function () {
            sitePage.navToZone();
          });
        });

        describe('Zone-level sales widget', function () {

          it('date range titles in chart legend', function(){
            expect(zoneSalesWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.CUSTOMCOMPARE1.toLowerCase(), translations.common.CUSTOMCOMPARE2.toLowerCase()]);
          });

          it('date range titles in chart summary frame', function(){
            var selectedPeriodLabel = zoneSalesWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = zoneSalesWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = zoneSalesWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.CUSTOMCOMPARE1.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.CUSTOMCOMPARE2.toUpperCase());
          });
        });

        describe('Zone-level conversion widget', function () {

          it('date range titles in chart legend', function(){
            expect(zoneConversionWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.CUSTOMCOMPARE1.toLowerCase(), translations.common.CUSTOMCOMPARE2.toLowerCase()]);
          });

          it('(fails until LFR-358 is fixed) date range titles in chart summary frame', function(){
            var selectedPeriodLabel = zoneConversionWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = zoneConversionWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = zoneConversionWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.CUSTOMCOMPARE1.toUpperCase());
            //todo: expectation below fails until LFR-358 is fixed - text refers to custom period 1 instead of custom period 2
            expect(priorYearLabel).toEqual(translations.common.CUSTOMCOMPARE2.toUpperCase());
          });
        });

        describe('Zone-level ATS widget', function () {

          it('date range titles in chart legend', function(){
            expect(averagePurchaseWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.CUSTOMCOMPARE1.toLowerCase(), translations.common.CUSTOMCOMPARE2.toLowerCase()]);
          });

          it('date range titles in chart summary frame', function(){
            var selectedPeriodLabel = averagePurchaseWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = averagePurchaseWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = averagePurchaseWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.CUSTOMCOMPARE1.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.CUSTOMCOMPARE2.toUpperCase());
          });
        });

        describe('Zone-level UPT widget', function () {

          //chart legend expectation below is less complete than other checks of this type - this widget
          // does not have data for this org/site, so custom period series text is not shown in legend
          it('date range titles in chart legend', function(){
            expect(zoneUPTWidget.getLegendText()).toContain(translations.common.SELECTEDPERIOD.toLowerCase());
            // expect(zoneUPTWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD, translations.common.CUSTOMCOMPARE1, translations.common.CUSTOMCOMPARE2])
          });

          it('date range titles in chart summary frame', function(){
            var selectedPeriodLabel = zoneUPTWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = zoneUPTWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = zoneUPTWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.CUSTOMCOMPARE1.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.CUSTOMCOMPARE2.toUpperCase());
          });
        });
      });
    });
  }
};
