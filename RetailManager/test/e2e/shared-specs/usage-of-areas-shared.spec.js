'use strict';

module.exports = {


  usageOfAreasSharedTests: function (timePeriod) {

    describe('Usage of Areas tab (shared tests)', function () {

      var sitePage = require('../pages/site-summary-page.js');
      var dateSelector = require('../pages/components/time-period-picker.js');
      var nav = require('../pages/components/nav-header.js');
      var orgData = require('../data/orgs.js');
      var trafficHeatmapWidget = require('../pages/components/traffic-heatmap-widget.js');
      var firstVisitsHeatmapWidget = require('../pages/components/first-visits-heatmap-widget.js');
      var oneAndDoneHeatmapWidget = require('../pages/components/one-done-heatmap-widget.js');
      var correlationHeatmapWidget = require('../pages/components/correlation-heatmap-widget.js');
      var visitsAfterHeatmapWidget = require('../pages/components/visits-after-heatmap-widget.js');
      var visitsBeforeHeatmapWidget = require('../pages/components/visits-before-heatmap-widget.js');

      it('should navigate to the correct site', function () {
        var title = nav.getSingleSiteName();

        expect(title).toEqual(orgData.SSOrg.ssOrgSiteName);
      });

      it('should nav to "Site Usage of areas" tab when clicked', function () {
        var tabHeading = sitePage.siteTitle();

        expect(tabHeading.getText()).toMatch('SITE USAGE OF AREAS');
      });

      it('date picker should appear', function () {
        var datePicker = dateSelector.getDatePicker();

        expect(datePicker.isPresent()).toBe(true);
      });

      it('area-type filter buttons should appear', function () {
        var filterButtons = sitePage.areaTypeFilterButtons;

        filterButtons.forEach(function (button) {
          expect(sitePage.getAreaTypeFilter(button).isPresent()).toBe(true);
        });
      });

      describe('"Property overall"-level tests', function () {

        it('should have selected the correct date button', function () {
          var dateButton;

          if(timePeriod === 'week') {
            dateButton = dateSelector.getWeekButton();
          }
          else if(timePeriod === 'month') {
            dateButton = dateSelector.getMonthButton();
          }
          else if(timePeriod === 'year') {
            dateButton = dateSelector.getYearButton();
          }
          dateButton.then(function (button) {
            expect(button.getAttribute('class')).toMatch('active');
          });
        });

        it('"page title should show the correct location" area name', function () {
          var areaName = sitePage.getAreaName();

          expect(areaName.getText()).toEqual(orgData.SSOrg.ssOrgSiteName);
        });

        describe('Traffic widget', function () {

          it('should display the title "Traffic distribution"', function () {
            var widgetTitle = trafficHeatmapWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('Traffic distribution');
          });

          it('should show a heatmap chart', function () {
            var heatmap = trafficHeatmapWidget.getHeatmap();

            expect(heatmap.isPresent()).toBe(true);
          });

          it('should show all areas in widget list when "all" filter is active', function () {
            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[3]);
            var areaList = trafficHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.getAllAreas()).toContain(area);
              });
            });
          });

          it('should show all stores in widget list when "stores" filter is active', function () {
            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[0]);
            var areaList = trafficHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.stores).toContain(area);
              });
            });
          });

          it('should show all corridors in widget list when "corridors" filter is active', function () {
            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[1]);
            var areaList = trafficHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.corridors).toContain(area);
              });
            });
          });

          it('should show all entrances in widget list when "entrances" filter is active', function () {
            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[2]);
            var areaList = trafficHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.entrances).toContain(area);
              });
            });
          });
        });

        describe('First locations widget', function () {

          it('should display the title "First locations to visit"', function () {
            var widgetTitle = firstVisitsHeatmapWidget.widgetTitle();
            expect(widgetTitle.getText()).toEqual('First locations to visit');
          });

          it('should show a heatmap chart', function () {
            var heatmap = firstVisitsHeatmapWidget.getHeatmap();

            expect(heatmap.isPresent()).toBe(true);
          });

          it('should show all areas in widget list when "all" filter is active', function () {
            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[3]);
            var areaList = firstVisitsHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.getAllAreas()).toContain(area);
              });
            });
          });

          it('should show all stores in widget list when "stores" filter is active', function () {
            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[0]);
            var areaList = firstVisitsHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.stores).toContain(area);
              });
            });
          });

          it('should show the correct subset of corridors in widget list when "corridors" filter is active', function () {
            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[1]);
            var areaList = firstVisitsHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.corridors).toContain(area);
              });
            });
          });

          it('should show all entrances in widget list when "entrances" filter is active', function () {
            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[2]);
            var areaList = firstVisitsHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.entrances).toContain(area);
              });
            });
          });
        });

        describe('One-and-done widget', function () {

          it('should display the title "One and done"', function () {
            var widgetTitle = oneAndDoneHeatmapWidget.widgetTitle();
            expect(widgetTitle.getText()).toEqual('One and done');
          });

          it('should show a heatmap chart', function () {
            var heatmap = oneAndDoneHeatmapWidget.getHeatmap();

            expect(heatmap.isPresent()).toBe(true);
          });

          it('should show all areas in widget list when "all" filter is active', function () {
            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[3]);
            var areaList = oneAndDoneHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.getAllAreas()).toContain(area);
              });
            });
          });

          it('should show all stores in widget list when "stores" filter is active', function () {
            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[0]);
            var areaList = oneAndDoneHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.stores).toContain(area);
              });
            });
          });

          it('should show the correct subset of corridors in widget list when "corridors" filter is active', function () {
            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[1]);
            var areaList = oneAndDoneHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.corridors).toContain(area);
              });
            });
          });

          it('should show all entrances in widget list when "entrances" filter is active', function () {
            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[2]);
            var areaList = oneAndDoneHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.entrances).toContain(area);
              });
            });
          });
        });
      });

      describe('"Single area"-level tests', function () {

        beforeAll(function () {
          browser.executeScript('window.scrollTo(0,0);').then(function () {
            sitePage.navToTestArea();
          });
        });

        it('should show the correct area name', function () {
          var areaName = sitePage.getAreaName();

          expect(areaName.getText()).toEqual(orgData.SSOrg.testArea);
        });

        it('should have selected the correct date button', function () {
          var dateButton;

          if(timePeriod === 'week') {
            dateButton = dateSelector.getWeekButton();
          }
          else if(timePeriod === 'month') {
            dateButton = dateSelector.getMonthButton();
          }
          else if(timePeriod === 'year') {
            dateButton = dateSelector.getYearButton();
          }
          dateButton.then(function (button) {
            expect(button.getAttribute('class')).toMatch('active');
          });
        });

        describe('Area-level correlation heat map widget', function () {

          it('should display the title "Correlation heat map"', function () {

            var widgetTitle = correlationHeatmapWidget.widgetTitle();
            expect(widgetTitle.getText()).toEqual('Correlation heat map');
          });

          it('should show a heatmap chart', function () {
            var heatmap = correlationHeatmapWidget.getHeatmap();

            expect(heatmap.isPresent()).toBe(true);
          });

          it('(may fail until SA-668 is resolved) should show all areas in widget list when "all" filter is active', function () {
            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[3]);

            var areaList = correlationHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.getAllAreas()).toContain(area);
              });
            });
          });

          it('(may fail until SA-668 is resolved) should show all stores in widget list when "stores" filter is active', function () {

            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[0]);
            var areaList = correlationHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.stores).toContain(area);
              });
            });
          });
          //functionality of corridor or entrance filters is disabled when selected area is of type "store"
        });

        describe('Area-level locations-after widget', function () {

          it('should display the title "Locations visited after"', function () {
            var widgetTitle = visitsAfterHeatmapWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('Locations visited after');
          });

          it('should show a heatmap chart', function () {
            var heatmap = visitsAfterHeatmapWidget.getHeatmap();

            expect(heatmap.isPresent()).toBe(true);
          });

          it('(may fail until SA-668 is resolved) should show all areas in widget list when "all" filter is active', function () {
            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[3]);
            var areaList = visitsAfterHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.getAllAreas()).toContain(area);
              });
            });
          });

          it('(may fail until SA-668 is resolved) should show all stores in widget list when "stores" filter is active', function () {
            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[0]);
            var areaList = visitsAfterHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.stores).toContain(area);
              });
            });
          });
          //functionality of corridor or entrance filters is disabled when selected area is of type "store"
        });

        describe('Area-level locations-before widget', function () {

          it('should display the title "Locations visited before"', function () {
            var widgetTitle = visitsBeforeHeatmapWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('Locations visited before');
          });

          it('should show a heatmap chart', function () {
            var heatmap = visitsBeforeHeatmapWidget.getHeatmap();

            expect(heatmap.isPresent()).toBe(true);
          });

          it('(may fail until SA-668 is resolved) should show all areas in widget list when "all" filter is active', function () {
            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[3]);
            var areaList = visitsBeforeHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.getAllAreas()).toContain(area);
              });
            });
          });

          it('(may fail until SA-668 is resolved) should show all stores in widget list when "stores" filter is active', function () {
            sitePage.clickAreaTypeFilter(sitePage.areaTypeFilterButtons[0]);
            var areaList = visitsBeforeHeatmapWidget.getAreaList();

            expect(areaList).not.toEqual([]);

            areaList.then(function (areaListArray) {
              areaListArray.forEach(function (area) {
                expect(orgData.SSOrg.stores).toContain(area);
              });
            });
          });
          //functionality of corridor or entrance filters is disabled when selected area is of type "store"
        });
      });
    });
  }
};
