'use strict';

module.exports = {
//shared tests for localization translations, using prior period/prior year date ranges

  visitorBehaviorSharedLangTests: function (locale) {

    describe('Visitor behavior tab (shared translation tests)', function () {

      var translationsFilePath = '../../../src/l10n/languages/' + locale + '.json'; //sets up filepath for required translations file
      var translations = require(translationsFilePath);

      var sitePage = require('../pages/site-summary-page.js');
      var dateSelector = require('../pages/components/time-period-picker.js');
      var nav = require('../pages/components/nav-header.js');
      var tabSidebar = require('../pages/components/tab-nav.js');
      var orgData = require('../data/orgs.js');
      var userData = require('../data/users.js');
      var visitorTrafficWidget = require('../pages/components/visitor-behavior-traffic-widget.js');
      var visitingFrequencyWidget = require('../pages/components/visiting-frequency-widget.js');
      var gshWidget = require('../pages/components/gross-shopping-hours-widget.js');
      var dwellTimeWidget = require('../pages/components/dwell-time-widget.js');
      var opportunityWidget = require('../pages/components/opportunity-widget.js');
      var drawRateWidget = require('../pages/components/draw-rate-widget.js');
      var shoppersVsOthersWidget = require('../pages/components/shoppers-vs-others-widget.js');
      var abandonmentRateWidget = require('../pages/components/abandonment-rate-widget.js');

      it('should navigate to the correct site', function () {
        var title = nav.getSingleSiteName();

        expect(title).toEqual(orgData.SSOrg.ssOrgSiteName);
      });

      describe('"Property overall"-level tests', function () {

        describe('Page title and tab sidebar checks', function () {

          it('"Site Visitor behavior" tab title', function () {
            var tabHeading = sitePage.siteTitle();
            var tabText = tabSidebar.getVisitorBehaviorTabName();

            expect(tabHeading.getText()).toEqual(translations.common.SITE.toUpperCase() + ' ' + translations.views.VISITORBEHAVIOR.toUpperCase());
          });

          it('"page title should show the correct location" area name', function () {
            var areaName = sitePage.getAreaName();

            expect(areaName).toEqual(orgData.SSOrg.ssOrgSiteName);
          });

          it('"select area" picker title', function () {
            var pickerTitle = sitePage.getZonePickerTitleText();

            expect(pickerTitle).toEqual(translations.analyticsHeader.SELECTAREA.toUpperCase());
          });

          it('default value in area-picker search bar', function () {
            var pickerTitle = sitePage.getAreaPickerSearchText();

            expect(pickerTitle).toEqual(translations.locationSelector.SEARCH);
          });
        });

        describe('Visitor behavior traffic widget', function () {

          it('widget title', function () {
            expect(visitorTrafficWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.visitor_behaviour_traffic);
          });

          it('date range titles in chart legend', function () {
            expect(visitorTrafficWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.PRIORPERIOD.toLowerCase(), translations.common.PRIORYEAR.toLowerCase()]);
          });

          it('date range titles in chart summary frame', function () {
            var selectedPeriodLabel = visitorTrafficWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = visitorTrafficWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = visitorTrafficWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.PRIORPERIOD.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.PRIORYEAR.toUpperCase());
          });

          it('metric unit label in chart summary frame', function () {
            expect(visitorTrafficWidget.getSummaryFrameMetricLabel()).toEqual(translations.kpis.totalLabel.visitor_behaviour_traffic.toUpperCase());
            expect(visitorTrafficWidget.getSummaryFrameMetricLabel('unique')).toEqual(translations.lineChartWidget.UNIQUE.toUpperCase());
            expect(visitorTrafficWidget.getSummaryFrameMetricLabel('returning')).toEqual(translations.lineChartWidget.RETURNING.toUpperCase());
          });

          it('tooltip text', function () {
             const tooltipTotalText = visitorTrafficWidget.getTooltipTotalText('total');
            tooltipTotalText.then(textArray => {
              expect(textArray[0]).toEqual(translations.lineChartWidget.TOTAL);
            });
            const tooltipUniqueText = visitorTrafficWidget.getTooltipTotalText('unique');
            tooltipUniqueText.then(textArray => {
              expect(textArray[0]).toEqual(translations.lineChartWidget.UNIQUE);
            });
            const tooltipReturningText = visitorTrafficWidget.getTooltipTotalText('returning');
            tooltipReturningText.then(textArray => {
              expect(textArray[0]).toEqual(translations.lineChartWidget.RETURNING);
            });
          });
        });

        describe('Visiting frequency widget', function () {

          it('widget title', function () {
            expect(visitingFrequencyWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.visiting_frequency);
          });

          it('frequency text in chart legend', function () {
            var legendArray = visitingFrequencyWidget.getPieChartLegendText();

            //# of legend text entries is variable for this widget.  1st and last entries in legend should pass checks below:
            legendArray.then(function (legendText) {
              expect(legendText[0]).toEqual(translations.visitingFrequencyDetailWidget.ONEVISIT);
              expect(legendText[legendText.length - 1]).toContain(translations.visitingFrequencyDetailWidget.ORMOREVISITS);

              //checks for translations in "middle" legend entries
              if (legendText.length > 2) {
                for (var i = 1; i < legendText.length - 1; i++) {
                  expect(legendText[i]).toContain(translations.visitingFrequencyDetailWidget.VISITS);
                }
              }
            });
          });

          it('date range titles in chart summary frame', function () {
            var selectedPeriodLabel = visitingFrequencyWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = visitingFrequencyWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = visitingFrequencyWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.PRIORPERIOD.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.PRIORYEAR.toUpperCase());
          });

          it('metric unit label in chart summary frame', function () {
            expect(visitingFrequencyWidget.getSummaryFrameMetricLabel()).toEqual(translations.kpis.totalLabel.visiting_frequency.toUpperCase());
          });
        });

        describe('Gross shopping hours widget', function () {

          it('widget title', function () {
            expect(gshWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.gsh);
          });

          it('date range titles in chart legend', function () {
            expect(gshWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.PRIORPERIOD.toLowerCase(), translations.common.PRIORYEAR.toLowerCase()]);
          });

          it('date range titles in chart summary frame', function () {
            var selectedPeriodLabel = gshWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = gshWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = gshWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.PRIORPERIOD.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.PRIORYEAR.toUpperCase());
          });

          it('metric unit label in chart summary frame', function () {
            expect(gshWidget.getSummaryFrameMetricLabel()).toEqual(translations.kpis.totalLabel.gsh.toUpperCase());
          });

          it('tooltip text', () => {
            const tooltipTotalText = gshWidget.getTooltipTotalText();
            tooltipTotalText.then(textArray => {
              expect(textArray[0]).toEqual(translations.lineChartWidget.TOTAL);
            });
          });
        });

        describe('Dwell time widget', function() {

          it('widget title', function () {
            expect(dwellTimeWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.dwell_time);
          });

          it('date range titles in chart legend', function () {
            expect(dwellTimeWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.PRIORPERIOD.toLowerCase(), translations.common.PRIORYEAR.toLowerCase()]);
          });

          it('date range titles in chart summary frame', function () {
            var selectedPeriodLabel = dwellTimeWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = dwellTimeWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = dwellTimeWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.PRIORPERIOD.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.PRIORYEAR.toUpperCase());
          });

          it('metric unit label in chart summary frame', function () {
            expect(dwellTimeWidget.getSummaryFrameMetricLabel()).toEqual(translations.kpis.totalLabel.dwell_time.toUpperCase());
          });

          it('tooltip text', () => {
            const tooltipTotalText = dwellTimeWidget.getTooltipTotalText();
            tooltipTotalText.then(textArray => {
              expect(textArray[0]).toEqual(translations.lineChartWidget.TOTAL);
            });
          });
        });

        describe('Opportunity widget', function() {

          it('widget title', function () {
            expect(opportunityWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.opportunity);
          });

          it('date range titles in chart legend', function () {
            expect(opportunityWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.PRIORPERIOD.toLowerCase(), translations.common.PRIORYEAR.toLowerCase()]);
          });

          it('date range titles in chart summary frame', function () {
            var selectedPeriodLabel = opportunityWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = opportunityWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = opportunityWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.PRIORPERIOD.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.PRIORYEAR.toUpperCase());
          });

          it('metric unit label in chart summary frame', function () {
            expect(opportunityWidget.getSummaryFrameMetricLabel()).toEqual(translations.kpis.totalLabel.opportunity.toUpperCase());
          });

          it('tooltip text', () => {
            const tooltipTotalText = opportunityWidget.getTooltipTotalText();
            tooltipTotalText.then(textArray => {
              expect(textArray[0]).toEqual(translations.lineChartWidget.TOTAL);
            });
          });
        });

        describe('Draw rate widget', function() {

          it('widget title', function () {
            expect(drawRateWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.draw_rate);
          });

          it('date range titles in chart legend', function () {
            expect(drawRateWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.PRIORPERIOD.toLowerCase(), translations.common.PRIORYEAR.toLowerCase()]);
          });

          it('date range titles in chart summary frame', function () {
            var selectedPeriodLabel = drawRateWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = drawRateWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = drawRateWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.PRIORPERIOD.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.PRIORYEAR.toUpperCase());
          });

          it('metric unit label in chart summary frame', function () {
            expect(drawRateWidget.getSummaryFrameMetricLabel()).toEqual(translations.kpis.totalLabel.draw_rate.toUpperCase());
          });

          it('tooltip text', () => {
            const tooltipTotalText = drawRateWidget.getTooltipTotalText();
            tooltipTotalText.then(textArray => {
              expect(textArray[0]).toEqual(translations.lineChartWidget.TOTAL);
            });
          });
        });

        describe('Shoppers vs others widget', function(){

          it('widget title', function () {
            expect(shoppersVsOthersWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.shoppers_vs_others);
          });

          it('date range titles in chart legend', function () {
            expect(shoppersVsOthersWidget.getLegendText()).toEqual([translations.shoppersVsTravellersWidget.SHOPPERS, translations.shoppersVsTravellersWidget.OTHERS]);
          });

          it('date range titles in chart summary frame', function () {
            var selectedPeriodLabel = shoppersVsOthersWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = shoppersVsOthersWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = shoppersVsOthersWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.PRIORPERIOD.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.PRIORYEAR.toUpperCase());
          });

          it('metric unit label in chart summary frame', function () {
            expect(shoppersVsOthersWidget.getSummaryFrameMetricLabel('shoppers')).toEqual(translations.shoppersVsTravellersWidget.SHOPPERS.toUpperCase());
            expect(shoppersVsOthersWidget.getSummaryFrameMetricLabel('others')).toEqual(translations.shoppersVsTravellersWidget.OTHERS.toUpperCase());
          });

          it('tooltip text', function () {
            expect(shoppersVsOthersWidget.getTooltipTotalText('shoppers')).toEqual(translations.shoppersVsTravellersWidget.SHOPPERS);
            expect(shoppersVsOthersWidget.getTooltipTotalText('others')).toEqual(translations.shoppersVsTravellersWidget.OTHERS);
          });
        });
      });

      describe('Area-level tests', function() {

        beforeAll(function () {
          browser.executeScript('window.scrollTo(0,0);').then(function () {
            sitePage.navToTestArea();
          });
        });

        describe('Page title and tab sidebar checks', function () {

          it('"Visitor behavior" tab title', function () {
            var tabHeading = sitePage.siteTitle();
            var tabText = tabSidebar.getVisitorBehaviorTabName();

            expect(tabHeading.getText()).toEqual(translations.common.AREA.toUpperCase() + ' ' + translations.views.VISITORBEHAVIOR.toUpperCase());
          });

          it('test area name', function() {
              var areaName = sitePage.getAreaName();

              expect(areaName).toMatch(orgData.SSOrg.testArea);
          });

          describe('Zone-level abandonment rate widget', function() {

            it('widget title', function () {
              expect(abandonmentRateWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.abandonment_rate);
            });

            it('date range titles in chart legend', function () {
              expect(abandonmentRateWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.PRIORPERIOD.toLowerCase(), translations.common.PRIORYEAR.toLowerCase()]);
            });

            it('date range titles in chart summary frame', function () {
              var selectedPeriodLabel = abandonmentRateWidget.getSummaryFrameSelectedPeriodLabel();
              var priorPeriodLabel = abandonmentRateWidget.getSummaryFrameCompare1Label();
              var priorYearLabel = abandonmentRateWidget.getSummaryFrameCompare2Label();

              expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
              expect(priorPeriodLabel).toEqual(translations.common.PRIORPERIOD.toUpperCase());
              expect(priorYearLabel).toEqual(translations.common.PRIORYEAR.toUpperCase());
            });

            it('metric unit label in chart summary frame', function () {
              expect(abandonmentRateWidget.getSummaryFrameMetricLabel()).toEqual(translations.kpis.totalLabel.abandonment_rate.toUpperCase());
            });

            it('tooltip text', () => {
              const tooltipTotalText = abandonmentRateWidget.getTooltipTotalText();
              tooltipTotalText.then(textArray => {
                expect(textArray[0]).toEqual(translations.lineChartWidget.TOTAL);
              });
            });
          });
        });
      });
    });
  }
};
