'use strict';

module.exports = {
//shared tests for localization translations, using prior period/prior year date ranges

  mallOrgSummarySharedLangTests: function (locale) {

    describe('Mall org summary page (shared translation tests)', function () {

      var translationsFilePath = '../../../src/l10n/languages/' + locale + '.json'; //sets up filepath for required translations file
      var translations = require(translationsFilePath);


      var sitePage = require('../pages/site-summary-page.js');
      var dateSelector = require('../pages/components/time-period-picker.js');
      var nav = require('../pages/components/nav-header.js');
      var tabSidebar = require('../pages/components/tab-nav.js');
      var orgData = require('../data/orgs.js');
      var userData = require('../data/users.js');
      var organizationSummaryWidget = require('../pages/components/organization-summary-widget.js');
      var sitePerformanceWidget = require('../pages/components/site-performance-widget.js');

      it('should navigate to the correct org', function () {
        var title = nav.getOrgName();

        expect(title).toEqual(orgData.MSOrg.name);
      });

      it('page title', function () {
        var tabHeading = sitePage.siteTitle();

        expect(tabHeading.getText()).toEqual(translations.views.ORGANIZATIONSUMMARY.toUpperCase());
      });

      // todo: add translation spec for common date-picker text (LFR-286)

      describe('Org summary widget', function() {
        //no need to test table expander button - will change to scroll bar

        it('widget title', function () {
          expect(organizationSummaryWidget.widgetTitle().getText()).toEqual(translations.organizationSummaryWidget.HEADER );
        });

        it('prior period/year selector', function() {
          expect(organizationSummaryWidget.getDatePeriodButton().getText()).toEqual(
            [translations.common.PRIORPERIOD,
              translations.common.PRIORYEAR
            ]
          );
        });

        it('search bar placeholder', function(){
          expect(organizationSummaryWidget.getFilterDefaultValue()).toEqual(translations.organizationSummaryWidget.HEADER);
        });

        //todo: (fails against staging until Jira LFR-525 is fixed)
        it('(fails against staging until Jira LFR-525 is fixed) metric column headers', function() {
          expect(organizationSummaryWidget.getKpiColumns()).toEqual(
             [
              translations.common.SITE.toUpperCase(),
              translations.kpis.deltaLabel.traffic.toUpperCase(),
              translations.kpis.kpiTitle.traffic.toUpperCase(),
              translations.kpis.deltaLabel.gsh.toUpperCase(),
              translations.kpis.kpiTitle.gsh.toUpperCase(),
              translations.kpis.deltaLabel.loyalty.toUpperCase(),
              translations.kpis.kpiTitle.loyalty.toUpperCase()
            ]
          );
        });

        it('"no data" message', function() {
          //todo: add statement after refactor of org summary table
        });
      });

      describe('sites performance bar chart widget', function() {

        it('widget title', function () {
          expect(sitePerformanceWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.sites_performance);
        });

        it('chart data dropdown options', function() {
          expect(sitePerformanceWidget.getChartTypeDropdownOptions()).toEqual(
            [translations.kpis.kpiTitle.traffic_contribution,
              translations.kpis.kpiTitle.traffic_increase,
              translations.kpis.kpiTitle.traffic_loss
            ]
          );
        });

        it('chart legend labels', function() {
          expect(sitePerformanceWidget.getLegendText()).toEqual(
            [translations.common.SELECTEDPERIOD,
              translations.common.PRIORPERIOD,
              translations.common.PRIORYEAR
            ]
          );
        });

        it('(tooltip text', function() {
          expect(sitePerformanceWidget.getTooltipTotalText()).toEqual(
            [translations.common.SELECTEDPERIOD,
              translations.common.PRIORPERIOD,
              translations.common.PRIORYEAR
            ]
          );
        })
      });
      //tests for daily performance and daily avg metric bar chart widgets are in retail-org-summary-shared-translations.spec.js
    });
  }
};
