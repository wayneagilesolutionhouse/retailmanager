'use strict';

module.exports = {


  trafficSharedTestsMonday: function (timePeriod) {

    describe('Traffic tab (shared tests)', function () {

      var sitePage = require('../pages/site-summary-page.js');
      var dateSelector = require('../pages/components/time-period-picker.js');
      var nav = require('../pages/components/nav-header.js');
      var orgData = require('../data/orgs.js');
      var userData = require('../data/users.js');
      var trafficWidget = require('../pages/components/site-traffic-widget.js');
      var powerHoursWidget = require('../pages/components/power-hours-widget.js');
      var dailyMetricBarWidget = require('../pages/components/daily-metric-averages-widget.js');

      it('date picker should appear', function () {
        var datePicker = dateSelector.getDatePicker();

        expect(datePicker.isPresent()).toBe(true);
      });

      it('should have selected the correct date button', function () {
        var dateButton;

        if(timePeriod === 'week') {
          dateButton = dateSelector.getWeekButton();
        }
        else if(timePeriod === 'month') {
          dateButton = dateSelector.getMonthButton();
        }
        else if(timePeriod === 'year') {
          dateButton = dateSelector.getYearButton();
        }
        dateButton.then(function (button) {
          expect(button.getAttribute('class')).toMatch('active');
        });
      });

      describe('Traffic widget:', function () {

        //checks that x-axis values fit within selected date range
        it('widget chart should display correct date range on x-axis', function () {
          var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
          var allXAxisDates = trafficWidget.getXAxisDates(userData.mondayUser.dateFormat);

          protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
            var reportArray = promiseArray[0];
            var chartDateArray = promiseArray[1];
            //first point on x-axis should match start date of time period
            expect(chartDateArray[0]).toEqual(reportArray[0]);
            chartDateArray.forEach(function (dateItem) {
              expect(dateItem).not.toBeLessThan(reportArray[0]);
              expect(dateItem).not.toBeGreaterThan(reportArray[1]);
            });
          });
        });

        it('correct date range should appear in "overall visitors" frame', function () {
          var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
          var comparePeriod1 = dateSelector.getComparePeriod(userData.mondayUser.comparePd1WeeksBack, userData.mondayUser.dateFormat);

          var selectedPeriodDate = trafficWidget.getSelectedPeriodDateRange(userData.mondayUser.dateFormat);
          var priorPeriodDate = trafficWidget.getPriorPeriodDateRange(userData.mondayUser.dateFormat);

          var comparePeriod2 = dateSelector.getComparePeriod(userData.mondayUser.comparePd2WeeksBack, userData.mondayUser.dateFormat);
          var priorPeriod2Date = trafficWidget.getPriorYearDateRange(userData.mondayUser.dateFormat);


          expect(selectedPeriodDate).toEqual(reportPeriod);
          expect(priorPeriodDate).toEqual(comparePeriod1);
          expect(priorPeriod2Date).toEqual(comparePeriod2);

        });
      });

      describe('Power hours widget:', function () {

        it('should display 7 Monday-based day columns', function () {
          expect(powerHoursWidget.getDayHeaders()).toEqual(powerHoursWidget.mondayHeaders);
        });
      });

      describe('Daily average metric widget', function () {

        it('should display 7 days along the x-axis beginning with Monday', function () {
          expect(dailyMetricBarWidget.getXAxisLabels()).toEqual(dailyMetricBarWidget.getMondayHeaders());
        });

        it('should display 7 days beginning with Monday in the table', function () {
          dailyMetricBarWidget.getExpandTableButton().click();
          expect(dailyMetricBarWidget.getColumnData('day')).toEqual(dailyMetricBarWidget.getMondayHeaders());
        });

        it('should sort days in the correct inverse order in the table', function () {
          dailyMetricBarWidget.sortTableBy('day');
          expect(dailyMetricBarWidget.getColumnData('day')).toEqual(dailyMetricBarWidget.sortedMondayHeaders());
        });
      });
    });
  }
};

