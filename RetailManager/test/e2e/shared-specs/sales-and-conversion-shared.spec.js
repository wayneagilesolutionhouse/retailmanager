const sitePage = require('../pages/site-summary-page.js');
const dateSelector = require('../pages/components/time-period-picker.js');
const nav = require('../pages/components/nav-header.js');
const orgData = require('../data/orgs.js');
const userData = require('../data/users.js');
const tenantSalesWidget = require('../pages/components/tenant-sales-summary-widget.js');
const tenantConversionSumWidget = require('../pages/components/tenant-conversion-summary-widget.js');
const tenantATSSumWidget = require('../pages/components/tenant-ats-summary-widget.js');
const tenantUPTSumWidget = require('../pages/components/tenant-upt-summary-widget.js');
const zoneSalesWidget = require('../pages/components/zone-sales-widget.js');
const zoneConversionWidget = require('../pages/components/zone-conversion-widget.js');
const averagePurchaseWidget = require('../pages/components/average-purchase-widget.js');
const translations = require('../../../src/l10n/languages/en_US.json');

module.exports = {


  salesAndConversionSharedTests(timePeriod) {
    describe('Sales and Conversion tab (shared tests)', () => {
      it('should navigate to the correct site', () => {
        const title = nav.getSiteName();

        expect(title).toEqual(orgData.MSOrgSite.name);
      });

      it('should nav to "Site Sales and conversion" tab when clicked', () => {
        const tabHeading = sitePage.siteTitle();

        expect(tabHeading.getText()).toMatch('SITE SALES AND CONVERSION');
      });

      it('date picker should appear', () => {
        const datePicker = dateSelector.getDatePicker();

        expect(datePicker.isPresent()).toBe(true);
      });

      describe('"Property overall"-level tests', () => {
        it('should have selected the correct date button', () => {
          let dateButton;

          if (timePeriod === 'week') {
            dateButton = dateSelector.getWeekButton();
          } else if (timePeriod === 'month') {
            dateButton = dateSelector.getMonthButton();
          } else if (timePeriod === 'year') {
            dateButton = dateSelector.getYearButton();
          }
          dateButton.then(button => {
            expect(button.getAttribute('class')).toMatch('active');
          });
        });

        it('"page title should show the correct location" area name', () => {
          const areaName = sitePage.getAreaName();

          expect(areaName.getText()).toEqual(orgData.MSOrgSite.name);
        });

        describe('Tenant sales summary widget', () => {
          beforeAll(() => {
            tenantSalesWidget.clickExpandButtonIfPresent();
          });

          it('should display the title "Tenant sales summary"', () => {
            const widgetTitle = tenantSalesWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('Tenant sales summary');
          });

          it('should show all 5 columns', () => {
            const columnHeadings = tenantSalesWidget.columnHeadings;

            expect(tenantSalesWidget.getCurrentColumnHeader().getText()).toEqual(`${translations.common.SELECTEDPERIOD} ${translations.kpis.options.sales}`);
            expect(tenantSalesWidget.getPeriodChangeColumnHeader().getText()).toEqual(columnHeadings[1]);
            expect(tenantSalesWidget.getPeriodColumnHeader().getText()).toEqual(`${translations.common.PRIORPERIOD} ${translations.kpis.options.sales}`);
            expect(tenantSalesWidget.getYearChangeColumnHeader().getText()).toEqual(columnHeadings[3]);
            expect(tenantSalesWidget.getYearColumnHeader().getText()).toEqual(`${translations.common.PRIORYEAR} ${translations.kpis.options.sales}`);
          });

          it('should display all test site tenants in the table list', () => {
            const tenantList = tenantSalesWidget.getFilteredTenantList();

            orgData.MSOrgSite.tenants.forEach(site => {
              expect(tenantList).toContain(site);
            });
          });

          it('should change the area list when filtered', () => {
            const filter = tenantSalesWidget.getFilter();

            filter.clear();
            expect(tenantSalesWidget.getFilteredTenantList()).toContain('New Yorker 119905');

            filter.sendKeys('amusing test string');
            expect(tenantSalesWidget.getFilteredTenantList()).toEqual([]);

            filter.clear();
            filter.sendKeys('yorker');
            expect(tenantSalesWidget.getFilteredTenantList()).toContain('New Yorker 119905');
            expect(tenantSalesWidget.getFilteredTenantList()).not.toContain('Gap Ext 109522');
            filter.clear();
          });

          it('should have data in each column', () => {
            tenantSalesWidget.getFilteredTenantList().then(array => {
              expect(array.length).toBeGreaterThan(0);
            });
            tenantSalesWidget.getCurrentPeriodColumn().then(array => {
              expect(array.length).toBeGreaterThan(0);
              expect(array[0]).not.toBeNaN();
            });
            tenantSalesWidget.getPriorPeriodDeltaColumn().then(array => {
              expect(array.length).toBeGreaterThan(0);
              expect(array[0]).not.toBeNaN();
            });
            tenantSalesWidget.getPriorPeriodColumn().then(array => {
              expect(array.length).toBeGreaterThan(0);
              expect(array[0]).not.toBeNaN();
            });

            if (timePeriod !== 'year') {
              tenantSalesWidget.getPriorYearDeltaColumn().then(array => {
                expect(array.length).toBeGreaterThan(0);
                expect(array[0]).not.toBeNaN();
              });
              tenantSalesWidget.getPriorYearColumn().then(array => {
                expect(array.length).toBeGreaterThan(0);
                expect(array[0]).not.toBeNaN();
              });
            }
          });

          it('should display the correct percentage deltas in each row', () => {
            const priorPeriodDeltas = tenantSalesWidget.getPriorPeriodDeltaColumn();
            const calculatedPriorPeriodDeltas = tenantSalesWidget.calculatePriorPeriodDeltaColumn();
            let priorYearDeltas;
            let calculatedPriorYearDeltas;

            if (timePeriod !== 'year') {
              priorYearDeltas = tenantSalesWidget.getPriorYearDeltaColumn();
              calculatedPriorYearDeltas = tenantSalesWidget.calculatePriorYearDeltaColumn();
            }

            protractor.promise.all([calculatedPriorPeriodDeltas, priorPeriodDeltas]).then(promiseArray => {
              const calculatedArray = promiseArray[0];
              const pageDataArray = promiseArray[1];

              calculatedArray.forEach((delta, index) => {
                expect(delta).not.toBeNaN();
                expect(delta).toEqual(jasmine.any(Number));
                expect(pageDataArray[index]).not.toBeNaN();
                expect(pageDataArray[index]).toEqual(jasmine.any(Number));
                expect(delta).not.toBeLessThan(pageDataArray[index] - 0.2);
                expect(delta).not.toBeGreaterThan(pageDataArray[index] + 0.2);
              });
            });

            if (timePeriod !== 'year') {
              protractor.promise.all([calculatedPriorYearDeltas, priorYearDeltas]).then(promiseArray => {
                const calculatedArray = promiseArray[0];
                const pageDataArray = promiseArray[1];

                calculatedArray.forEach((delta, index) => {
                  expect(delta).not.toBeNaN();
                  expect(delta).toEqual(jasmine.any(Number));
                  expect(pageDataArray[index]).not.toBeNaN();
                  expect(pageDataArray[index]).toEqual(jasmine.any(Number));
                  expect(delta).not.toBeLessThan(pageDataArray[index] - 0.2);
                  expect(delta).not.toBeGreaterThan(pageDataArray[index] + 0.2);
                });
              });
            }
          });
        });

        describe('Tenant conversion summary widget', () => {
          beforeAll(() => {
            tenantConversionSumWidget.clickExpandButtonIfPresent();
          });

          it('should display the title "Tenant conversion summary"', () => {
            const widgetTitle = tenantConversionSumWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('Tenant conversion summary');
          });

          it('should show all 5 columns', () => {
            const columnHeadings = tenantConversionSumWidget.columnHeadings;

            expect(tenantConversionSumWidget.getCurrentColumnHeader().getText()).toEqual(`${translations.common.SELECTEDPERIOD} ${translations.kpis.options.conversion}`);
            expect(tenantConversionSumWidget.getPeriodChangeColumnHeader().getText()).toEqual(columnHeadings[1]);
            expect(tenantConversionSumWidget.getPeriodColumnHeader().getText()).toEqual(`${translations.common.PRIORPERIOD} ${translations.kpis.options.conversion}`);
            expect(tenantConversionSumWidget.getYearChangeColumnHeader().getText()).toEqual(columnHeadings[3]);
            expect(tenantConversionSumWidget.getYearColumnHeader().getText()).toEqual(`${translations.common.PRIORYEAR} ${translations.kpis.options.conversion}`);
          });

          it('should display all test site tenants in the table list', () => {
            const tenantList = tenantConversionSumWidget.getFilteredTenantList();

            orgData.MSOrgSite.tenants.forEach(site => {
              expect(tenantList).toContain(site);
            });
          });

          it('should change the area list when filtered', () => {
            const filter = tenantConversionSumWidget.getFilter();

            filter.clear();
            expect(tenantConversionSumWidget.getFilteredTenantList()).toContain('New Yorker 119905');

            filter.sendKeys('amusing test string');
            expect(tenantConversionSumWidget.getFilteredTenantList()).toEqual([]);

            filter.clear();
            filter.sendKeys('yorker');
            expect(tenantConversionSumWidget.getFilteredTenantList()).toContain('New Yorker 119905');
            expect(tenantConversionSumWidget.getFilteredTenantList()).not.toContain('Gap Ext 109522');
            filter.clear();
          });

          it('should have data in each column', () => {
            tenantConversionSumWidget.getFilteredTenantList().then(array => {
              expect(array.length).toBeGreaterThan(0);
              expect(array[0]).not.toBeNaN();
            });
            tenantConversionSumWidget.getCurrentPeriodColumn().then(array => {
              expect(array.length).toBeGreaterThan(0);
              expect(array[0]).not.toBeNaN();
            });
            tenantConversionSumWidget.getPriorPeriodDeltaColumn().then(array => {
              expect(array.length).toBeGreaterThan(0);
              expect(array[0]).not.toBeNaN();
            });
            tenantConversionSumWidget.getPriorPeriodColumn().then(array => {
              expect(array.length).toBeGreaterThan(0);
              expect(array[0]).not.toBeNaN();
            });

            if (timePeriod !== 'year') {
              tenantConversionSumWidget.getPriorYearDeltaColumn().then(array => {
                expect(array.length).toBeGreaterThan(0);
                expect(array[0]).not.toBeNaN();
              });
              tenantConversionSumWidget.getPriorYearColumn().then(array => {
                expect(array.length).toBeGreaterThan(0);
                expect(array[0]).not.toBeNaN();
              });
            }
          });

          it('percentage deltas in each row should have the correct sign', () => {
            const priorPeriodDeltas = tenantConversionSumWidget.getPriorPeriodDeltaColumn(true);
            const currentPeriodValues = tenantConversionSumWidget.getCurrentPeriodColumn();
            const priorPeriodValues = tenantConversionSumWidget.getPriorPeriodColumn();
            let priorYearDeltas;
            let priorYearValues;

            if (timePeriod !== 'year') {
              priorYearDeltas = tenantConversionSumWidget.getPriorYearDeltaColumn(true);
              priorYearValues = tenantConversionSumWidget.getPriorYearColumn();
            }

            protractor.promise.all([currentPeriodValues, priorPeriodValues, priorPeriodDeltas]).then(promiseArray => {
              const currentPeriodArray = promiseArray[0];
              const priorPeriodArray = promiseArray[1];
              const pageDataArray = promiseArray[2];

              pageDataArray.forEach((delta, index) => {
                expect(delta).not.toBeNaN();
                expect(delta).toEqual(jasmine.any(Number));

                if (priorPeriodArray[index] === 0) {
                  expect(delta).toEqual(0);
                } else if (currentPeriodArray[index] < priorPeriodArray[index]) {
                  expect(delta).toBeLessThan(0);
                } else if (currentPeriodArray[index] > priorPeriodArray[index]) {
                  expect(delta).toBeGreaterThan(0);
                }
              });
            });

            if (timePeriod !== 'year') {
              protractor.promise.all([currentPeriodValues, priorYearValues, priorYearDeltas]).then(promiseArray => {
                const currentPeriodArray = promiseArray[0];
                const priorYearArray = promiseArray[1];
                const pageDataArray = promiseArray[2];

                pageDataArray.forEach((delta, index) => {
                  expect(delta).not.toBeNaN();
                  expect(delta).toEqual(jasmine.any(Number));

                  if (priorYearArray[index] === 0) {
                    expect(delta).toEqual(0);
                  } else if (currentPeriodArray[index] < priorYearArray[index]) {
                    expect(delta).toBeLessThan(0);
                  } else if (currentPeriodArray[index] > priorYearArray[index]) {
                    expect(delta).toBeGreaterThan(0);
                  }
                });
              });
            }
          });
        });

        describe('Tenant ATS summary widget', () => {
          beforeAll(() => {
            tenantATSSumWidget.clickExpandButtonIfPresent();
          });

          it('should display the title "Tenant ATS summary"', () => {
            const widgetTitle = tenantATSSumWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('Tenant ATS summary');
          });

          it('should show all 5 columns', () => {
            const columnHeadings = tenantATSSumWidget.columnHeadings;

            expect(tenantATSSumWidget.getCurrentColumnHeader().getText()).toEqual(`${translations.common.SELECTEDPERIOD} ${translations.kpis.options.ats}`);
            expect(tenantATSSumWidget.getPeriodChangeColumnHeader().getText()).toEqual(columnHeadings[1]);
            expect(tenantATSSumWidget.getPeriodColumnHeader().getText()).toEqual(`${translations.common.PRIORPERIOD} ${translations.kpis.options.ats}`);
            expect(tenantATSSumWidget.getYearChangeColumnHeader().getText()).toEqual(columnHeadings[3]);
            expect(tenantATSSumWidget.getYearColumnHeader().getText()).toEqual(`${translations.common.PRIORYEAR} ${translations.kpis.options.ats}`);
          });

          it('should display all test site tenants in the table list', () => {
            const tenantList = tenantATSSumWidget.getFilteredTenantList();

            orgData.MSOrgSite.tenants.forEach(site => {
              expect(tenantList).toContain(site);
            });
          });

          it('should change the area list when filtered', () => {
            const filter = tenantATSSumWidget.getFilter();

            filter.clear();
            expect(tenantATSSumWidget.getFilteredTenantList()).toContain('New Yorker 119905');

            filter.sendKeys('amusing test string');
            expect(tenantATSSumWidget.getFilteredTenantList()).toEqual([]);

            filter.clear();
            filter.sendKeys('yorker');
            expect(tenantATSSumWidget.getFilteredTenantList()).toContain('New Yorker 119905');
            expect(tenantATSSumWidget.getFilteredTenantList()).not.toContain('Gap Ext 109522');
            filter.clear();
          });

          it('should have data in each column', () => {
            tenantATSSumWidget.getFilteredTenantList().then(array => {
              expect(array.length).toBeGreaterThan(0);
              expect(array[0]).not.toBeNaN();
            });
            tenantATSSumWidget.getCurrentPeriodColumn().then(array => {
              expect(array.length).toBeGreaterThan(0);
              expect(array[0]).not.toBeNaN();
            });
            tenantATSSumWidget.getPriorPeriodDeltaColumn().then(array => {
              expect(array.length).toBeGreaterThan(0);
              expect(array[0]).not.toBeNaN();
            });
            tenantATSSumWidget.getPriorPeriodColumn().then(array => {
              expect(array.length).toBeGreaterThan(0);
              expect(array[0]).not.toBeNaN();
            });

            if (timePeriod !== 'year') {
              tenantATSSumWidget.getPriorYearDeltaColumn().then(array => {
                expect(array.length).toBeGreaterThan(0);
                expect(array[0]).not.toBeNaN();
              });
              tenantATSSumWidget.getPriorYearColumn().then(array => {
                expect(array.length).toBeGreaterThan(0);
                expect(array[0]).not.toBeNaN();
              });
            }
          });

          it('percentage deltas in each row should have the correct sign', () => {
            const priorPeriodDeltas = tenantATSSumWidget.getPriorPeriodDeltaColumn(true);
            const currentPeriodValues = tenantATSSumWidget.getCurrentPeriodColumn();
            const priorPeriodValues = tenantATSSumWidget.getPriorPeriodColumn();
            let priorYearDeltas;
            let priorYearValues;

            if (timePeriod !== 'year') {
              priorYearDeltas = tenantATSSumWidget.getPriorYearDeltaColumn(true);
              priorYearValues = tenantATSSumWidget.getPriorYearColumn();
            }

            protractor.promise.all([currentPeriodValues, priorPeriodValues, priorPeriodDeltas]).then(promiseArray => {
              const currentPeriodArray = promiseArray[0];
              const priorPeriodArray = promiseArray[1];
              const pageDataArray = promiseArray[2];

              pageDataArray.forEach((delta, index) => {
                expect(delta).not.toBeNaN();
                expect(delta).toEqual(jasmine.any(Number));
                if (priorPeriodArray[index] === 0) {
                  expect(delta).toEqual(0);
                } else if (currentPeriodArray[index] < priorPeriodArray[index]) {
                  expect(delta).toBeLessThan(0);
                } else if (currentPeriodArray[index] > priorPeriodArray[index]) {
                  expect(delta).toBeGreaterThan(0);
                }
              });
            });

            if (timePeriod !== 'year') {
              protractor.promise.all([currentPeriodValues, priorYearValues, priorYearDeltas]).then(promiseArray => {
                const currentPeriodArray = promiseArray[0];
                const priorYearArray = promiseArray[1];
                const pageDataArray = promiseArray[2];

                pageDataArray.forEach((delta, index) => {
                  expect(delta).not.toBeNaN();
                  expect(delta).toEqual(jasmine.any(Number));
                  if (priorYearArray[index] === 0) {
                    expect(delta).toEqual(0);
                  } else if (currentPeriodArray[index] < priorYearArray[index]) {
                    expect(delta).toBeLessThan(0);
                  } else if (currentPeriodArray[index] > priorYearArray[index]) {
                    expect(delta).toBeGreaterThan(0);
                  }
                });
              });
            }
          });
        });

        // This widget does not contain data for this org/site - checking only for widget integrity, not data integrity
        describe('Tenant UPT summary widget', () => {
          beforeAll(() => {
            tenantUPTSumWidget.clickExpandButtonIfPresent();
          });

          it('should display the title "Tenant ATS summary"', () => {
            const widgetTitle = tenantUPTSumWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('Tenant UPT summary');
          });

          it('should show all 5 columns', () => {
            const columnHeadings = tenantUPTSumWidget.columnHeadings;

            expect(tenantUPTSumWidget.getCurrentColumnHeader().getText()).toEqual(columnHeadings[0]);
            expect(tenantUPTSumWidget.getPeriodChangeColumnHeader().getText()).toEqual(columnHeadings[1]);
            expect(tenantUPTSumWidget.getPeriodColumnHeader().getText()).toEqual(columnHeadings[2]);
            expect(tenantUPTSumWidget.getYearChangeColumnHeader().getText()).toEqual(columnHeadings[3]);
            expect(tenantUPTSumWidget.getYearColumnHeader().getText()).toEqual(columnHeadings[4]);
          });

          it('should display all test site tenants in the table list', () => {
            const tenantList = tenantUPTSumWidget.getFilteredTenantList();

            orgData.MSOrgSite.tenants.forEach(site => {
              expect(tenantList).toContain(site);
            });
          });

          it('should change the area list when filtered', () => {
            const filter = tenantUPTSumWidget.getFilter();

            filter.clear();
            expect(tenantUPTSumWidget.getFilteredTenantList()).toContain('New Yorker 119905');

            filter.sendKeys('amusing test string');
            expect(tenantUPTSumWidget.getFilteredTenantList()).toEqual([]);

            filter.clear();
            filter.sendKeys('yorker');
            expect(tenantUPTSumWidget.getFilteredTenantList()).toContain('New Yorker 119905');
            expect(tenantUPTSumWidget.getFilteredTenantList()).not.toContain('Gap Ext 109522');
            filter.clear();
          });
        });
      });

      describe('Zone-level tests', () => {
        beforeAll(() => {
          browser.executeScript('window.scrollTo(0,0);').then(() => {
            sitePage.navToZone();
          });
        });

        it('should have selected the correct date button', () => {
          let dateButton;

          if (timePeriod === 'week') {
            dateButton = dateSelector.getWeekButton();
          } else if (timePeriod === 'month') {
            dateButton = dateSelector.getMonthButton();
          } else if (timePeriod === 'year') {
            dateButton = dateSelector.getYearButton();
          }
          dateButton.then(button => {
            expect(button.getAttribute('class')).toMatch('active');
          });
        });

        it('should show the correct area name', () => {
          const areaName = sitePage.getAreaName();

          expect(areaName.getText()).toMatch(orgData.MSOrgSite.testZone);
        });

        describe('Zone-level sales widget', () => {
          it('should display the title "Sales"', () => {
            const widgetTitle = zoneSalesWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('Sales');
          });

          it('widget chart should display correct date range on x-axis', () => {
            const reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            const allXAxisDates = zoneSalesWidget.getXAxisDates(userData.superUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(promiseArray => {
              const reportArray = promiseArray[0];
              const chartDateArray = promiseArray[1];
              // first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(dateItem => {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });


          it('widget chart should display correct range on y-axis', () => {
            const selectedPeriodDataValues = zoneSalesWidget.getSelectedPeriodDataValues();
            const priorPeriodDataValues = zoneSalesWidget.getPriorPeriodDataValues();
            const highestYAxisValue = zoneSalesWidget.getHighestYAxisValue();
            let priorYearDataValues;

            if (timePeriod !== 'year') {
              priorYearDataValues = zoneSalesWidget.getPriorYearDataValues();
            }

            expect(highestYAxisValue).not.toBeNaN();
            expect(highestYAxisValue).toEqual(jasmine.any(Number));

            selectedPeriodDataValues.then(dataArray => {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(dataPoint => {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            priorPeriodDataValues.then(dataArray => {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(dataPoint => {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            if (timePeriod !== 'year') {
              priorYearDataValues.then(dataArray => {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(dataPoint => {
                  expect(dataPoint).not.toBeLessThan(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });
            }
          });

          it('sum of a line\'s data points should equal the corresponding total displayed in summary frame', () => {
            const selectedPeriodDataSum = zoneSalesWidget.getSelectedPeriodDataSum();
            const priorPeriodDataSum = zoneSalesWidget.getPriorPeriodDataSum();
            const selectedPeriodSales = zoneSalesWidget.getSelectedPeriodOverall();
            const priorPeriodSales = zoneSalesWidget.getPriorPeriodOverall();
            let priorYearDataSum;
            let priorYearSales;

            if (timePeriod !== 'year') {
              priorYearDataSum = zoneSalesWidget.getPriorYearDataSum();
              priorYearSales = zoneSalesWidget.getPriorYearOverall();
            }

            expect(selectedPeriodDataSum).toEqual(selectedPeriodSales);
            expect(priorPeriodDataSum).toEqual(priorPeriodSales);

            if (timePeriod !== 'year') {
              expect(priorYearDataSum).toEqual(priorYearSales);
            }
          });

          it('correct date range should appear in summary frame', () => {
            const reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            const priorReportPeriod = dateSelector.getPriorReportingPeriod(userData.superUser.dateFormat);
            const selectedPeriodDate = zoneSalesWidget.getSelectedPeriodDateRange(userData.superUser.dateFormat);
            const priorPeriodDate = zoneSalesWidget.getPriorPeriodDateRange(userData.superUser.dateFormat);
            let priorYearReportingPeriod;
            let priorYearDate;
            if (timePeriod !== 'year') {
              priorYearReportingPeriod = dateSelector.getPriorYearReportingPeriod(timePeriod, userData.superUser.dateFormat);
              priorYearDate = zoneSalesWidget.getPriorYearDateRange(userData.superUser.dateFormat);
            }

            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(priorReportPeriod);

            if (timePeriod !== 'year') {
              expect(priorYearDate).toEqual(priorYearReportingPeriod);
            }
          });

          it('correct percentage deltas should appear in summary frame', () => {
            const priorPeriodDelta = zoneSalesWidget.getPriorPeriodDelta();
            const calculatedPriorPeriodDelta = zoneSalesWidget.calculatePriorPeriodDelta();
            let priorYearDelta;
            let calculatedPriorYearDelta;
            if (timePeriod !== 'year') {
              priorYearDelta = zoneSalesWidget.getPriorYearDelta();
              calculatedPriorYearDelta = zoneSalesWidget.calculatePriorYearDelta();
            }

            expect(calculatedPriorPeriodDelta).toEqual(priorPeriodDelta);

            if (timePeriod !== 'year') {
              expect(calculatedPriorYearDelta).toEqual(priorYearDelta);
            }
          });
        });

        describe('Zone-level conversion widget', () => {
          it('should display the title "Conversion"', () => {
            const widgetTitle = zoneConversionWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('Conversion');
          });

          it('widget chart should display correct date range on x-axis', () => {
            const reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            const allXAxisDates = zoneConversionWidget.getXAxisDates(userData.superUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(promiseArray => {
              const reportArray = promiseArray[0];
              const chartDateArray = promiseArray[1];
              // first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(dateItem => {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('widget chart should display correct range on y-axis', () => {
            const selectedPeriodDataValues = zoneConversionWidget.getSelectedPeriodDataValues();
            const priorPeriodDataValues = zoneConversionWidget.getPriorPeriodDataValues();
            const highestYAxisValue = zoneConversionWidget.getHighestYAxisValue();
            let priorYearDataValues;
            if (timePeriod !== 'year') {
              priorYearDataValues = zoneConversionWidget.getPriorYearDataValues();
            }

            expect(highestYAxisValue).toEqual(jasmine.any(Number));
            expect(highestYAxisValue).not.toBeNaN();

            selectedPeriodDataValues.then(dataArray => {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(dataPoint => {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            priorPeriodDataValues.then(dataArray => {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(dataPoint => {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            if (timePeriod !== 'year') {
              priorYearDataValues.then(dataArray => {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(dataPoint => {
                  expect(dataPoint).not.toBeLessThan(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });
            }
          });

          // checking line values vs totals displayed is not possible - "average" totals derived from server make this challenging

          it('correct date range should appear in summary frame', () => {
            const reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            const priorReportPeriod = dateSelector.getPriorReportingPeriod(userData.superUser.dateFormat);
            const selectedPeriodDate = zoneConversionWidget.getSelectedPeriodDateRange(userData.superUser.dateFormat);
            const priorPeriodDate = zoneConversionWidget.getPriorPeriodDateRange(userData.superUser.dateFormat);
            let priorYearReportingPeriod;
            let priorYearDate;
            if (timePeriod !== 'year') {
              priorYearReportingPeriod = dateSelector.getPriorYearReportingPeriod(timePeriod, userData.superUser.dateFormat);
              priorYearDate = zoneConversionWidget.getPriorYearDateRange(userData.superUser.dateFormat);
            }

            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(priorReportPeriod);

            if (timePeriod !== 'year') {
              expect(priorYearDate).toEqual(priorYearReportingPeriod);
            }
          });

          // checking delta values vs deltas displayed is not implemented - "average" values derived from server make this challenging
        });

        describe('ATS widget', () => {
          it('should display the title "ATS"', () => {
            const widgetTitle = averagePurchaseWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('ATS');
          });

          it('widget chart should display correct date range on x-axis', () => {
            const reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            const allXAxisDates = averagePurchaseWidget.getXAxisDates(userData.superUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(promiseArray => {
              const reportArray = promiseArray[0];
              const chartDateArray = promiseArray[1];
              // first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(dateItem => {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('widget chart should display correct range on y-axis', () => {
            const selectedPeriodDataValues = averagePurchaseWidget.getSelectedPeriodDataValues();
            const priorPeriodDataValues = averagePurchaseWidget.getPriorPeriodDataValues();
            const highestYAxisValue = averagePurchaseWidget.getHighestYAxisValue();
            let priorYearDataValues;
            if (timePeriod !== 'year') {
              priorYearDataValues = averagePurchaseWidget.getPriorYearDataValues();
            }

            expect(highestYAxisValue).not.toBeNaN();
            expect(highestYAxisValue).toEqual(jasmine.any(Number));

            selectedPeriodDataValues.then(dataArray => {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(dataPoint => {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            priorPeriodDataValues.then(dataArray => {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(dataPoint => {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            if (timePeriod !== 'year') {
              priorYearDataValues.then(dataArray => {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(dataPoint => {
                  expect(dataPoint).not.toBeLessThan(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });
            }
          });

          // checking line values vs totals displayed is not implemented - "average" values make this challenging

          it('correct date range should appear in summary frame', () => {
            const reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            const priorReportPeriod = dateSelector.getPriorReportingPeriod(userData.superUser.dateFormat);
            const selectedPeriodDate = averagePurchaseWidget.getSelectedPeriodDateRange(userData.superUser.dateFormat);
            const priorPeriodDate = averagePurchaseWidget.getPriorPeriodDateRange(userData.superUser.dateFormat);
            let priorYearReportingPeriod;
            let priorYearDate;

            if (timePeriod !== 'year') {
              priorYearReportingPeriod = dateSelector.getPriorYearReportingPeriod(timePeriod, userData.superUser.dateFormat);
              priorYearDate = averagePurchaseWidget.getPriorYearDateRange(userData.superUser.dateFormat);
            }

            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(priorReportPeriod);

            if (timePeriod !== 'year') {
              expect(priorYearDate).toEqual(priorYearReportingPeriod);
            }
          });

          // checking delta values vs deltas displayed is not implemented - "average" values make this challenging
        });
// no UPT widget data for this org
      });
    });
  }
};
