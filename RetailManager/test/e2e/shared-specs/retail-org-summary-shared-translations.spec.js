const sitePage = require('../pages/site-summary-page.js');
const nav = require('../pages/components/nav-header.js');
const orgData = require('../data/orgs.js');
const filterPicker = require('../pages/components/filter-picker.js');
const orgPerformanceWidget = require('../pages/components/organization-performance-widget.js');
const dailyPerformanceWidget = require('../pages/components/daily-performance-widget.js');
const dailyMetricBarWidget = require('../pages/components/daily-metric-averages-widget.js');
const retailStoreChartWidget = require('../pages/components/retail-store-chart-widget.js');

module.exports = {
// shared tests for localization translations, using prior period/prior year date ranges

  retailOrgSummarySharedLangTests(locale) {
    const translationsFilePath = `../../../src/l10n/languages/${locale}.json`; // sets up filepath for required translations file
    const translations = require(translationsFilePath);

    describe('Retail org summary page (shared translation tests)', () => {
      it('should navigate to the correct org', () => {
        const title = nav.getOrgName();

        expect(title).toEqual(orgData.MSRetailOrg.name);
      });

      it('page title', () => {
        const tabHeading = sitePage.siteTitle();

        expect(tabHeading.getText()).toEqual(translations.views.RETAILORGANIZATIONSUMMARY.toUpperCase());
      });

      it('(may fail until SA-737 is fixed) comp/all stores selector text', () => {
        const selectorText = sitePage.getCompStoresSelectorText();

        expect(selectorText).toMatch(translations.retailOrganizationSummary.COMPAREMODE.toUpperCase());
        expect(selectorText).toMatch(translations.retailOrganizationSummary.COMPSTORES);
        expect(selectorText).toMatch(translations.retailOrganizationSummary.ALLSTORES);
      });

      describe('filter picker', () => {
        it('filter picker body text', () => {
          const menuHeader = filterPicker.getFilterMenuText().getText();

          menuHeader.then(textArray => {
            expect(textArray[0]).toEqual(translations.common.FILTERS);
            expect(textArray[1]).toMatch(translations.common.FILTERSAPPLIED.toUpperCase());
            expect(textArray[2]).toMatch(translations.common.SHOWING.toUpperCase());
            expect(textArray[2]).toMatch(translations.common.SITES.toUpperCase());
          });
        });

        it('filter picker button text', () => {
          filterPicker.openFilterMenu();

          const applyBtn = filterPicker.getFilterMenuApplyBtn();
          const cancelBtn = filterPicker.getFilterMenuClearBtn();

          expect(applyBtn.getText()).toEqual(translations.common.APPLY);
          expect(cancelBtn.getText()).toEqual(translations.common.CLEAR);
        });

        it('(fails until SA-810 is resolved) "selected filter" label', () => {
          filterPicker.applyFilter();
          const selectedFilterLabel = filterPicker.getSelectedFiltersLabelText();

          expect(selectedFilterLabel).toEqual(translations.retailOrganizationSummary.SELECTEDFILTERS.toUpperCase());

          filterPicker.getFilterMenuClearBtn().click();
        });
      });

      describe('Organization performance widget', () => {
        it('widget title', () => {
          expect(orgPerformanceWidget.widgetTitle().getText()).toEqual(translations.summaryKpiWidget.ORGPERFORMANCE);
        });

        it('KPI titles', () => {
          expect(orgPerformanceWidget.getKpiLabels()).toEqual(
            [translations.kpis.shortKpiTitles.tenant_sales.toUpperCase(),
              translations.kpis.shortKpiTitles.tenant_traffic.toUpperCase(),
              translations.kpis.shortKpiTitles.tenant_conversion.toUpperCase(),
              translations.kpis.shortKpiTitles.tenant_ats.toUpperCase(),
              translations.kpis.shortKpiTitles.tenant_star.toUpperCase()
            ]);
        });

        it('KPI total titles', () => {
          expect(orgPerformanceWidget.getKpiTotalLabels()).toEqual(
            [translations.kpis.totalLabel.sales.toUpperCase(),
              translations.kpis.totalLabel.traffic.toUpperCase(),
              translations.kpis.totalLabel.conversion.toUpperCase(),
              translations.kpis.totalLabel.ats.toUpperCase(),
              translations.kpis.totalLabel.star.toUpperCase()
            ]);
        });

        it('compare period 2 label', () => {
          expect(orgPerformanceWidget.getKpiVarianceLabels()).toEqual(
            [translations.common.FROMPRIORYEAR.toUpperCase(),
              translations.common.FROMPRIORYEAR.toUpperCase(),
              translations.common.FROMPRIORYEAR.toUpperCase(),
              translations.common.FROMPRIORYEAR.toUpperCase(),
              translations.common.FROMPRIORYEAR.toUpperCase()
            ]);
        });

        describe('upper tooltip text', () => {
          it('compare period labels', () => {
            expect(orgPerformanceWidget.getTooltipCompareLabel('sales', 'upper')).toEqual(translations.common.PRIORPERIOD.toUpperCase());
            expect(orgPerformanceWidget.getTooltipCompareLabel('traffic', 'upper')).toEqual(translations.common.PRIORPERIOD.toUpperCase());
            expect(orgPerformanceWidget.getTooltipCompareLabel('conversion', 'upper')).toEqual(translations.common.PRIORPERIOD.toUpperCase());
            expect(orgPerformanceWidget.getTooltipCompareLabel('ats', 'upper')).toEqual(translations.common.PRIORPERIOD.toUpperCase());
            expect(orgPerformanceWidget.getTooltipCompareLabel('star', 'upper')).toEqual(translations.common.PRIORPERIOD.toUpperCase());
          });

          it('KPI total titles', () => {
            expect(orgPerformanceWidget.getTooltipKpiTotalLabel('sales', 'upper')).toEqual(translations.kpis.totalLabel.sales.toUpperCase());
            expect(orgPerformanceWidget.getTooltipKpiTotalLabel('traffic', 'upper')).toEqual(translations.kpis.totalLabel.traffic.toUpperCase());
            expect(orgPerformanceWidget.getTooltipKpiTotalLabel('conversion', 'upper')).toEqual(translations.kpis.totalLabel.conversion.toUpperCase());
            expect(orgPerformanceWidget.getTooltipKpiTotalLabel('ats', 'upper')).toEqual(translations.kpis.totalLabel.ats.toUpperCase());
            expect(orgPerformanceWidget.getTooltipKpiTotalLabel('star', 'upper')).toEqual(translations.kpis.totalLabel.star.toUpperCase());
          });
        });

        describe('lower tooltip text', () => {
          it('compare period labels', () => {
            expect(orgPerformanceWidget.getTooltipCompareLabel('sales', 'lower')).toEqual(translations.common.PRIORYEAR.toUpperCase());
            expect(orgPerformanceWidget.getTooltipCompareLabel('traffic', 'lower')).toEqual(translations.common.PRIORYEAR.toUpperCase());
            expect(orgPerformanceWidget.getTooltipCompareLabel('conversion', 'lower')).toEqual(translations.common.PRIORYEAR.toUpperCase());
            expect(orgPerformanceWidget.getTooltipCompareLabel('ats', 'lower')).toEqual(translations.common.PRIORYEAR.toUpperCase());
            expect(orgPerformanceWidget.getTooltipCompareLabel('star', 'lower')).toEqual(translations.common.PRIORYEAR.toUpperCase());
          });

          it('KPI total titles', () => {
            expect(orgPerformanceWidget.getTooltipKpiTotalLabel('sales', 'lower')).toEqual(translations.kpis.totalLabel.sales.toUpperCase());
            expect(orgPerformanceWidget.getTooltipKpiTotalLabel('traffic', 'lower')).toEqual(translations.kpis.totalLabel.traffic.toUpperCase());
            expect(orgPerformanceWidget.getTooltipKpiTotalLabel('conversion', 'lower')).toEqual(translations.kpis.totalLabel.conversion.toUpperCase());
            expect(orgPerformanceWidget.getTooltipKpiTotalLabel('ats', 'lower')).toEqual(translations.kpis.totalLabel.ats.toUpperCase());
            expect(orgPerformanceWidget.getTooltipKpiTotalLabel('star', 'lower')).toEqual(translations.kpis.totalLabel.star.toUpperCase());
          });
        });
      });

      describe('Daily performance indicators widget', () => {
        it('widget title', () => {
          expect(dailyPerformanceWidget.widgetTitle().getText()).toEqual(translations.dailyPerformance.DAILYPERFORMANCEINDICATORS);
        });

        it('day selector dropdown options', () => {
          const options = dailyPerformanceWidget.getDaySelectorOptions();

          expect(options).toEqual(
            [translations.daySelector.ALLDAYS,
              translations.daySelector.WEEKENDS,
              translations.weekdaysLong.sun,
              translations.weekdaysLong.mon,
              translations.weekdaysLong.tue,
              translations.weekdaysLong.wed,
              translations.weekdaysLong.thu,
              translations.weekdaysLong.fri,
              translations.weekdaysLong.sat
            ]
          );
        });

        it('table expander button', () => {
          expect(dailyPerformanceWidget.getExpandTableButton().getText()).toEqual(translations.dailyPerformance.SHOWTABLE.toUpperCase());
          dailyPerformanceWidget.getExpandTableButton().click();
          expect(dailyPerformanceWidget.getExpandTableButton().getText()).toEqual(translations.dailyPerformance.HIDETABLE.toUpperCase());
        });

        describe('left-hand chart text', () => {
          const position = 'left';

          it('left chart titles', () => {
            const title = dailyPerformanceWidget.getChartTitle(position);
            expect(title).toMatch(translations.kpis.shortKpiTitles.tenant_sales.toUpperCase());
            expect(title).toMatch(translations.kpis.shortKpiTitles.tenant_traffic.toUpperCase());
            expect(title).toMatch(translations.kpis.shortKpiTitles.tenant_labor.toUpperCase());
            expect(title).toMatch(translations.kpis.shortKpiTitles.tenant_transactions.toUpperCase());
            expect(title).toMatch(translations.dailyPerformance.BYDAYOFWEEK.toUpperCase());
          });

          it('left chart y-axis labels', () => {
            expect(dailyPerformanceWidget.getChartYAxisLabels(position)).toEqual('%');
          });

          it('left chart x-axis labels', () => {
            expect(dailyPerformanceWidget.getChartXAxisLabels(position)).toEqual(
              [translations.weekdaysLong.sun,
                translations.weekdaysLong.mon,
                translations.weekdaysLong.tue,
                translations.weekdaysLong.wed,
                translations.weekdaysLong.thu,
                translations.weekdaysLong.fri,
                translations.weekdaysLong.sat
              ]
            );
          });

          it('left chart legend labels', () => {
            expect(dailyPerformanceWidget.getChartLegend(position)).toEqual(
              [translations.kpis.shortKpiTitles.tenant_sales,
                translations.kpis.shortKpiTitles.tenant_traffic,
                translations.kpis.shortKpiTitles.tenant_labor,
                translations.kpis.shortKpiTitles.tenant_transactions
              ]
            );
          });

          it('left chart tooltip title', () => {
            const tooltipTitle = dailyPerformanceWidget.getChartTooltip(position);

            tooltipTitle.then(text => {
              const tooltip = text.map(metric => {
                return metric.toUpperCase();
              });
              expect(tooltip).toMatch(translations.kpis.shortKpiTitles.tenant_sales.toUpperCase());
              expect(tooltip).toMatch(translations.kpis.shortKpiTitles.tenant_traffic.toUpperCase());
              expect(tooltip).toMatch(translations.kpis.shortKpiTitles.tenant_labor.toUpperCase());
              expect(tooltip).toMatch(translations.kpis.shortKpiTitles.tenant_transactions.toUpperCase());
            });
          });
        });

        describe('right-hand chart text', () => {
          const position = 'right';

          // todo: fails against staging until Jira LFR-526 is fixed
          it('(fails against staging until Jira LFR-526 is fixed) right chart titles', () => {
            const title = dailyPerformanceWidget.getChartTitle(position);
            expect(title).toMatch(translations.kpis.shortKpiTitles.tenant_conversion.toUpperCase());
            expect(title).toMatch(translations.kpis.shortKpiTitles.tenant_star.toUpperCase());
            expect(title).toMatch(translations.dailyPerformance.BYDAYOFWEEK.toUpperCase());
          });


          it('right chart y-axis labels', () => {
            const label = dailyPerformanceWidget.getChartYAxisLabels(position);
            expect(label).toMatch(translations.kpis.shortKpiTitles.tenant_conversion.toUpperCase());
            expect(label).toMatch(translations.kpis.shortKpiTitles.tenant_star.toUpperCase());
          });

          it('right chart x-axis labels', () => {
            expect(dailyPerformanceWidget.getChartXAxisLabels(position)).toEqual(
              [translations.weekdaysLong.sun,
                translations.weekdaysLong.mon,
                translations.weekdaysLong.tue,
                translations.weekdaysLong.wed,
                translations.weekdaysLong.thu,
                translations.weekdaysLong.fri,
                translations.weekdaysLong.sat
              ]
            );
          });

          it('right chart legend labels', () => {
            expect(dailyPerformanceWidget.getChartLegend(position)).toEqual(
              [translations.kpis.shortKpiTitles.tenant_conversion,
                translations.kpis.shortKpiTitles.tenant_star
              ]
            );
          });

          it('right chart tooltip title', () => {
            const tooltipTitle = dailyPerformanceWidget.getChartTooltip(position);

            tooltipTitle.then(array => {
              expect(array[0].toLowerCase()).toMatch(translations.kpis.shortKpiTitles.tenant_conversion.toLowerCase());
              expect(array[1].toLowerCase()).toMatch(translations.kpis.shortKpiTitles.tenant_star.toLowerCase());
            });
          });
        });

        describe('widget table', () => {
          it('table column header metrics', () => {
            expect(dailyPerformanceWidget.getTableHeaderMetricLabels()).toEqual(
              [translations.kpis.shortKpiTitles.tenant_sales.toUpperCase(),
                translations.kpis.shortKpiTitles.tenant_traffic.toUpperCase(),
                translations.kpis.shortKpiTitles.tenant_labor.toUpperCase(),
                translations.kpis.shortKpiTitles.tenant_transactions.toUpperCase(),
                translations.kpis.shortKpiTitles.tenant_conversion.toUpperCase(),
                translations.kpis.shortKpiTitles.tenant_star.toUpperCase()
              ]
            );
          });

          it('table row day labels', () => {
            expect(dailyPerformanceWidget.getTableDayLabels()).toEqual(
              [translations.weekdaysLong.sun,
                translations.weekdaysLong.mon,
                translations.weekdaysLong.tue,
                translations.weekdaysLong.wed,
                translations.weekdaysLong.thu,
                translations.weekdaysLong.fri,
                translations.weekdaysLong.sat
              ]
            );
          });

          it('table footer row label', () => {
            expect(dailyPerformanceWidget.getTableFooterLabel()).toEqual(translations.common.AVERAGE.toUpperCase());
          });
        });
      });

      describe('Daily averages widget', () => {
        it('widget title', () => {
          expect(dailyMetricBarWidget.widgetTitle().getText()).toEqual(`${translations.trafficPerWeekdayWidget.DAILYAVERAGES}: ${translations.kpis.kpiTitle.traffic}`);
        });

        it('day selector dropdown options', () => {
          const options = dailyMetricBarWidget.getDaySelectorOptions();

          expect(options).toEqual(
            [translations.daySelector.ALLDAYS,
              translations.daySelector.WEEKENDS,
              translations.weekdaysLong.sun,
              translations.weekdaysLong.mon,
              translations.weekdaysLong.tue,
              translations.weekdaysLong.wed,
              translations.weekdaysLong.thu,
              translations.weekdaysLong.fri,
              translations.weekdaysLong.sat
            ]
          );
        });

        it('metric selector dropdown options', () => {
          expect(dailyMetricBarWidget.getMetricSelectorOptions()).toEqual(
            [translations.kpis.kpiTitle.traffic,
              translations.kpis.kpiTitle.sales,
              translations.kpis.kpiTitle.conversion,
              translations.kpis.kpiTitle.ats,
              translations.kpis.kpiTitle.star
            ]
          );
        });

        it('table expander button', () => {
          expect(dailyMetricBarWidget.getExpandTableButton().getText()).toEqual(translations.trafficPerWeekdayWidget.SHOWTABLE.toUpperCase());
          dailyMetricBarWidget.getExpandTableButton().click();
          expect(dailyMetricBarWidget.getExpandTableButton().getText()).toEqual(translations.trafficPerWeekdayWidget.HIDETABLE.toUpperCase());
        });

        describe('widget chart', () => {
          it('day labels on chart x-axis', () => {
            expect(dailyMetricBarWidget.getXAxisLabels()).toEqual(
              [translations.weekdaysLong.sun,
                translations.weekdaysLong.mon,
                translations.weekdaysLong.tue,
                translations.weekdaysLong.wed,
                translations.weekdaysLong.thu,
                translations.weekdaysLong.fri,
                translations.weekdaysLong.sat
              ]
            );
          });

          it('chart legend', () => {
            expect(dailyMetricBarWidget.getLegendText()).toEqual(
              [translations.common.SELECTEDPERIOD,
                translations.common.PRIORPERIOD,
                translations.common.PRIORYEAR
              ]
            );
          });

          it('tooltip text', () => {
            expect(dailyMetricBarWidget.getTooltipTotalText()).toEqual(
              [translations.common.SELECTEDPERIOD,
                translations.common.PRIORPERIOD,
                translations.common.PRIORYEAR
              ]
            );
          });
        });

        describe('widget table', () => {
          it('table column header metrics', () => {
            expect(dailyMetricBarWidget.getTableHeaderMetricLabels()).toEqual(
              [translations.kpis.kpiTitle.traffic.toUpperCase(),
                translations.kpis.kpiTitle.sales.toUpperCase(),
                translations.kpis.kpiTitle.conversion.toUpperCase(),
                translations.kpis.kpiTitle.ats.toUpperCase(),
                translations.kpis.kpiTitle.star.toUpperCase()
              ]
            );
          });

          it('table row day labels', () => {
            expect(dailyMetricBarWidget.getTableDayLabels()).toEqual(
              [translations.weekdaysLong.sun,
                translations.weekdaysLong.mon,
                translations.weekdaysLong.tue,
                translations.weekdaysLong.wed,
                translations.weekdaysLong.thu,
                translations.weekdaysLong.fri,
                translations.weekdaysLong.sat
              ]
            );
          });

          it('table footer row label', () => {
            expect(dailyMetricBarWidget.getTableFooterLabel()).toEqual(translations.trafficPerWeekdayWidget.AVERAGE);
          });
        });
      });

      describe('Retail store summary table widget', () => {
        it('widget title', () => {
          expect(retailStoreChartWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.retail_store_summary);
        });

        it('store category dropdown options', () => {
          expect(retailStoreChartWidget.getStoreCategoryOptions()).toEqual(
            [translations.retailOrganization.ALLSTORES,
              translations.retailOrganization.OUTOFSTOREOPPORTUNITY,
              translations.retailOrganization.HIGHPERFORMERS,
              translations.retailOrganization.INSTOREOPPORUNITY,
              translations.retailOrganization.POORPERFORMERS
            ]
          );
          retailStoreChartWidget.openStoreCategoryDropdown();
        });

        it('metric dropdown options', () => {
          expect(retailStoreChartWidget.getMetricOptions()).toEqual(
            [translations.kpis.kpiTitle.conversion,
              translations.kpis.kpiTitle.sales
            ]
          );
          retailStoreChartWidget.openMetricSelectorDropdown();
        });

        it('default search string', () => {
          expect(retailStoreChartWidget.getSearchBarPlaceholder()).toEqual(translations.retailOrganization.FILTERSITES);
        });

        it('"extreme values" checkbox label', () => {
          expect(retailStoreChartWidget.getExtremeCheckboxLabel()).toEqual(translations.retailOrganization.EXTREMEVALUES);
        });

        it('chart quadrant and axis labels (conversion metric)', () => {
          expect(retailStoreChartWidget.getChartText()).toEqual(
            [translations.retailOrganization.POORPERFORMERS.toUpperCase(),
              translations.retailOrganization.INSTOREOPPORUNITY.toUpperCase(),
              translations.retailOrganization.OUTOFSTOREOPPORTUNITY.toUpperCase(),
              translations.retailOrganization.HIGHPERFORMERS.toUpperCase(),
              translations.retailOrganization.DAILYTRAFFIC.toUpperCase(),
              translations.kpis.kpiTitle.conversion.toUpperCase()
            ]
          );
        });

        it('chart quadrant and axis labels (sales metric)', () => {
          retailStoreChartWidget.setMetricOption('sales');
          expect(retailStoreChartWidget.getChartText()).toEqual(
            [translations.retailOrganization.POORPERFORMERS.toUpperCase(),
              translations.retailOrganization.INSTOREOPPORUNITY.toUpperCase(),
              translations.retailOrganization.OUTOFSTOREOPPORTUNITY.toUpperCase(),
              translations.retailOrganization.HIGHPERFORMERS.toUpperCase(),
              translations.retailOrganization.DAILYTRAFFIC.toUpperCase(),
              translations.kpis.kpiTitle.sales.toUpperCase()
            ]
          );
        });

        

        it('should translate chart tooltip text', () => {
          expect(retailStoreChartWidget.getChartTooltipMetrics()).toEqual(
            [`${translations.kpis.kpiTitle.traffic}:`,
              `${translations.kpis.kpiTitle.sales}:`,
              `${translations.kpis.kpiTitle.conversion}:`,
              `${translations.kpis.kpiTitle.transactions}:`,
              `${translations.kpis.kpiTitle.ats}:`,
              `${translations.kpis.shortKpiTitles.tenant_aur}:`,
              `${translations.kpis.shortKpiTitles.tenant_sps}:`,
              `${translations.kpis.kpiTitle.star}:`,
              `${translations.kpis.shortKpiTitles.tenant_splh}:`
            ]
          );
        });
      });
      // todo: add tests for org summary table when refactored
    });
  }
};
