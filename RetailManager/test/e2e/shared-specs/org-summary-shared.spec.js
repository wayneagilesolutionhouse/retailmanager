'use strict';

module.exports = {


  orgSummarySharedTests: function (timePeriod) {

    describe('Org navigation (shared tests)', function() {

      var orgData = require('../data/orgs.js');
      var login = require('../pages/login.js');
      var orgPage = require('../pages/org-summary-page.js');
      var nav = require('../pages/components/nav-header.js');
      var dateSelector = require('../pages/components/time-period-picker.js');
      var organizationSummaryWidget = require('../pages/components/organization-summary-widget.js');
      var sitePerformanceWidget = require('../pages/components/site-performance-widget.js');
      var dailyMetricBarWidget = require('../pages/components/daily-metric-averages-widget.js');

      describe('Multi-site org', function () {

        it('should navigate to an org and display the correct org name', function () {
          var title = orgPage.orgTitle();

          expect(title.getText()).toEqual(orgData.MSOrg.name);
          expect(browser.getCurrentUrl()).toMatch(orgData.MSOrg.id.toString());
        });

        it('date picker should appear', function () {
          var datePicker = dateSelector.getDatePicker();

          expect(datePicker.isPresent()).toBe(true);
        });

        it('should have selected the correct date button', function () {
          var dateButton;

          if(timePeriod === 'week') {
            dateButton = dateSelector.getWeekButton();
          }
          else if(timePeriod === 'month') {
            dateButton = dateSelector.getMonthButton();
          }
          else if(timePeriod === 'year') {
            dateButton = dateSelector.getYearButton();
          }
          dateButton.then(function (button) {
            expect(button.getAttribute('class')).toMatch('active');
          });
        });


        it('should show all 3 org summary widgets on org summary page', function () {
          var orgSummaryWidget = orgPage.orgSummaryWidget();
          var sitePerformanceWidget = orgPage.sitePerformanceWidget();
          var orgTrafficWeekdayDistWidget = orgPage.trafficWeekdayDistWidget();

          expect(orgSummaryWidget.isPresent()).toBe(true);
          expect(sitePerformanceWidget.isPresent()).toBe(true);
          expect(orgTrafficWeekdayDistWidget.isPresent()).toBe(true);
        });

        describe('Org summary widget', function () {

          beforeAll(function () {
            organizationSummaryWidget.clickExpandButtonIfPresent();
          });

          it('should display the title "Organization summary"', function () {
            expect(organizationSummaryWidget.widgetTitle().getText()).toEqual('Organization summary');
          });

          it('should show all 6 data columns', function () {
            expect(organizationSummaryWidget.getKpiColumns()).toEqual(organizationSummaryWidget.columns);
          });

          it('should change the site list when filtered', function () {
            var filter = organizationSummaryWidget.getFilter();

            filter.clear();
            expect(organizationSummaryWidget.getFilteredOrgList()).toContain('Mall of Arabia');

            filter.sendKeys('amusing text string');
            expect(organizationSummaryWidget.getFilteredOrgList()).toEqual([]);

            filter.clear();
            filter.sendKeys('Arabia');
            expect(organizationSummaryWidget.getFilteredOrgList()).not.toContain('Mall of Dhahran');
            expect(organizationSummaryWidget.getFilteredOrgList()).toContain('Mall of Arabia');
            filter.clear();
          });

          it('should sort sites by name, asc & desc.', function () {
            var defaultOrgOrder = organizationSummaryWidget.getFilteredOrgList();

            var sortedOrgList = organizationSummaryWidget.clickSiteHeaderAndGetOrgList();
            var reverseSortedOrgList = organizationSummaryWidget.clickSiteHeaderAndGetOrgList();
            var clickedSortAgain = organizationSummaryWidget.clickSiteHeaderAndGetOrgList();

            var sortPromises = [defaultOrgOrder, sortedOrgList, reverseSortedOrgList, clickedSortAgain];


            defaultOrgOrder.then(function(orgList) {
              orgList.forEach(function (site) {
                expect(orgData.MSOrg.sites).toContain(site);
              });
            });

            sortedOrgList.then(function(orgList) {
              orgList.forEach(function (site) {
                expect(orgData.MSOrg.sites).toContain(site);
              });
            });

            reverseSortedOrgList.then(function(orgList) {
              orgList.forEach(function (site) {
                expect(orgData.MSOrg.sites).toContain(site);
              });
            });

            Promise.all(sortPromises).then( function(data) {
              var widgetSitesDefault = data[0];
              var widgetSitesSort1 = data[1];
              var widgetSitesSort2 = data[2];
              var widgetSitesSort3 = data[3];

              expect(widgetSitesDefault).to.not.equal(widgetSitesSort1);
              expect(widgetSitesDefault).to.not.equal(widgetSitesSort2);
              expect(widgetSitesDefault).to.not.equal(widgetSitesSort3);

              expect(widgetSitesSort1).to.equal(widgetSitesSort3);
              expect(widgetSitesSort1).to.not.equal(widgetSitesSort2);

              expect(widgetSitesSort1.reverse()).to.equal(widgetSitesSort2);
              expect(widgetSitesSort2.reverse()).to.equal(widgetSitesSort1);
            });
          });

          it('should have data in each column', function () {
            organizationSummaryWidget.getFilteredOrgList().then(function (array) {
              expect(array.length).toBeGreaterThan(0);
            });
            organizationSummaryWidget.getTrafficDeltaColumn().then(function (array) {
              expect(array.length).toBeGreaterThan(0);
            });
            organizationSummaryWidget.getCurrentOverallColumn().then(function (array) {
              expect(array.length).toBeGreaterThan(0);
            });
            organizationSummaryWidget.getPreviousOverallColumn().then(function (array) {
              expect(array.length).toBeGreaterThan(0);
            });

          });

          it('(may fail until LFR-460 is resolved) should display the correct percentage deltas in each row', function () {
            var trafficDeltas = organizationSummaryWidget.getTrafficDeltaColumn();
            var gshDeltas = organizationSummaryWidget.getGSHDeltaColumn();
            var visitingDeltas = organizationSummaryWidget.getVisitingDeltaColumn();
            var calculatedTrafficDeltas = organizationSummaryWidget.calculateTrafficDeltaColumn();
            var calculatedGSHDeltas = organizationSummaryWidget.calculateGSHDeltaColumn();
            var calculatedVisitingDeltas = organizationSummaryWidget.calculateVisitingDeltaColumn();

            protractor.promise.all([calculatedTrafficDeltas, trafficDeltas]).then(function (promiseArray) {
              var calculatedArray = promiseArray[0];
              var pageDataArray = promiseArray[1];

              calculatedArray.forEach(function (delta, index) {
                expect(delta).not.toBeNaN();
                expect(delta).toEqual(jasmine.any(Number));
                expect(pageDataArray[index]).not.toBeNaN();
                expect(pageDataArray[index]).toEqual(jasmine.any(Number));
                expect(delta).not.toBeLessThan(pageDataArray[index] - 0.2);
                expect(delta).not.toBeGreaterThan(pageDataArray[index] + 0.2);
              });
            });

          });
        });

        describe('Site performance widget', function () {

          describe('when listing sites by highest percent traffic', function () {

            it('should display the title "Sites performance"', function () {

              expect(sitePerformanceWidget.widgetTitle().getText()).toEqual('Sites performance');
            });

            it('widget chart should display correct range on y-axis', function () {
              var highestYAxisValue = sitePerformanceWidget.getHighestYAxisValue();
              var selectedPeriodDataValues = sitePerformanceWidget.getSelectedPeriodDataValues();
              var priorPeriodDataValues = sitePerformanceWidget.getPriorPeriodDataValues();
              var priorYearDataValues = sitePerformanceWidget.getPriorYearDataValues();

              expect(highestYAxisValue).not.toBeNaN();
              expect(highestYAxisValue).toEqual(jasmine.any(Number));

              selectedPeriodDataValues.then(function (dataArray) {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(function (dataPoint) {
                  expect(dataPoint).not.toBeLessThan(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });

              priorPeriodDataValues.then(function (dataArray) {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(function (dataPoint) {
                  expect(dataPoint).not.toBeLessThan(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });

              priorYearDataValues.then(function (dataArray) {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(function (dataPoint) {
                  expect(dataPoint).not.toBeLessThan(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });
            });

            it('should display sites from the org along the x-axis', function () {
              var chartSites = sitePerformanceWidget.getXAxisSites();
              var uppercaseOrgList = [];

              orgData.MSOrg.sites.forEach(function (site) {
                uppercaseOrgList.push(site.toUpperCase());
              });

              return chartSites.then(function (siteArray) {
                siteArray.forEach(function (site) {
                  expect(uppercaseOrgList).toContain(site);
                });
              });
            });
          });

          describe('when listing sites by highest percent traffic increase', function () {

            beforeAll(function () {
              sitePerformanceWidget.setChartDataOption('increase');
            });

            it('should display the title "Sites performance"', function () {

              expect(sitePerformanceWidget.widgetTitle().getText()).toEqual('Sites performance');
            });

            it('widget chart should display correct range on y-axis', function () {
              var highestYAxisValue = sitePerformanceWidget.getHighestYAxisValue();
              var selectedPeriodDataValues = sitePerformanceWidget.getSelectedPeriodDataValues();
              var priorPeriodDataValues = sitePerformanceWidget.getPriorPeriodDataValues();
              var priorYearDataValues = sitePerformanceWidget.getPriorYearDataValues();

              expect(highestYAxisValue).not.toBeNaN();
              expect(highestYAxisValue).toEqual(jasmine.any(Number));

              selectedPeriodDataValues.then(function (dataArray) {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(function (dataPoint) {
                  expect(dataPoint).not.toBeLessThan(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });

              if (timePeriod !== 'year') { // some of these sites have sparse historical data
                priorPeriodDataValues.then(function (dataArray) {
                  expect(dataArray.length).toBeGreaterThan(0);
                  dataArray.forEach(function (dataPoint) {
                    expect(dataPoint).not.toBeLessThan(0);
                    expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                  });
                });

                priorYearDataValues.then(function (dataArray) {
                  expect(dataArray.length).toBeGreaterThan(0);
                  dataArray.forEach(function (dataPoint) {
                    expect(dataPoint).not.toBeLessThan(0);
                    expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                  });
                });
              };
            });

            it('should display sites from the org along the x-axis', function () {
              var chartSites = sitePerformanceWidget.getXAxisSites();
              var uppercaseOrgList = [];

              orgData.MSOrg.sites.forEach(function (site) {
                uppercaseOrgList.push(site.toUpperCase());
              });

              return chartSites.then(function (siteArray) {
                siteArray.forEach(function (site) {
                  expect(uppercaseOrgList).toContain(site);
                });
              });
            });
          });

          describe('when listing sites by highest percent traffic loss', function () {

            beforeAll(function () {
              sitePerformanceWidget.setChartDataOption('loss');
            });

            it('should display the title "Sites performance"', function () {

              expect(sitePerformanceWidget.widgetTitle().getText()).toEqual('Sites performance');
            });

            it('widget chart should display correct range on y-axis', function () {
              var highestYAxisValue = sitePerformanceWidget.getHighestYAxisValue();
              var selectedPeriodDataValues = sitePerformanceWidget.getSelectedPeriodDataValues();
              var priorPeriodDataValues = sitePerformanceWidget.getPriorPeriodDataValues();
              var priorYearDataValues = sitePerformanceWidget.getPriorYearDataValues();

              expect(highestYAxisValue).not.toBeNaN();
              expect(highestYAxisValue).toEqual(jasmine.any(Number));

              selectedPeriodDataValues.then(function (dataArray) {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(function (dataPoint) {
                  expect(dataPoint).not.toBeLessThan(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });

              priorPeriodDataValues.then(function (dataArray) {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(function (dataPoint) {
                  expect(dataPoint).not.toBeLessThan(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });

              priorYearDataValues.then(function (dataArray) {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(function (dataPoint) {
                  expect(dataPoint).not.toBeLessThan(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });
            });

            it('should display sites from the org along the x-axis', function () {
              var chartSites = sitePerformanceWidget.getXAxisSites();
              var uppercaseOrgList = [];

              orgData.MSOrg.sites.forEach(function (site) {
                uppercaseOrgList.push(site.toUpperCase());
              });

              return chartSites.then(function (siteArray) {
                siteArray.forEach(function (site) {
                  expect(uppercaseOrgList).toContain(site);
                });
              });
            });
          });
        });

        describe('Daily average metric widget', function () {

          it('should display the title "Daily averages: Traffic"', function () {

            expect(dailyMetricBarWidget.widgetTitle().getText()).toEqual('Daily averages: Traffic');
          });

          it('should display 7 days along the x-axis', function () {
            expect(dailyMetricBarWidget.getXAxisLabels()).toEqual(dailyMetricBarWidget.getDayHeaders());
          });

          it('widget chart should display correct range on y-axis', function () {
            var highestYAxisValue = dailyMetricBarWidget.getHighestYAxisValue();
            var selectedPeriodDataValues = dailyMetricBarWidget.getSelectedPeriodDataValues();
            var priorPeriodDataValues = dailyMetricBarWidget.getPriorPeriodDataValues();
            var priorYearDataValues = dailyMetricBarWidget.getPriorYearDataValues();

            expect(highestYAxisValue).not.toBeNaN();
            expect(highestYAxisValue).toEqual(jasmine.any(Number));

            selectedPeriodDataValues.then(function (dataArray) {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(function (dataPoint) {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            priorPeriodDataValues.then(function (dataArray) {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(function (dataPoint) {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            priorYearDataValues.then(function (dataArray) {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(function (dataPoint) {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });
          });

          it('table should show/hide when "show table" button is clicked', function() {
            expect(dailyMetricBarWidget.isTableDisplayed()).toEqual(false);
            dailyMetricBarWidget.getExpandTableButton().click();
            expect(dailyMetricBarWidget.isTableDisplayed()).toEqual(true);
          });

          it('each table column should be sortable, ascending and descending', function() {

            ['traffic-delta', 'traffic', 'day'].forEach(function(metric) {
              dailyMetricBarWidget.sortTableBy(metric);
              var sortedColumn = dailyMetricBarWidget.getColumnData(metric);
              dailyMetricBarWidget.sortTableBy(metric);
              var reverseSortedColumn = dailyMetricBarWidget.getColumnData(metric);
              expect(sortedColumn).not.toEqual(reverseSortedColumn);
            });
          });
        });
      });
    });
  }
};
