'use strict';

module.exports = {


  visitorBehaviorSharedTests: function (timePeriod) {

    describe('Visitor behavior tab (shared tests)', function () {

      var sitePage = require('../pages/site-summary-page.js');
      var dateSelector = require('../pages/components/time-period-picker.js');
      var nav = require('../pages/components/nav-header.js');
      var orgData = require('../data/orgs.js');
      var userData = require('../data/users.js');
      var lineChart = require('../pages/components/common/line-chart-widget.js');
      var visitorTrafficWidget = require('../pages/components/visitor-behavior-traffic-widget.js');
      var visitingFrequencyWidget = require('../pages/components/visiting-frequency-widget.js');
      var gshWidget = require('../pages/components/gross-shopping-hours-widget.js');
      var dwellTimeWidget = require('../pages/components/dwell-time-widget.js');
      var opportunityWidget = require('../pages/components/opportunity-widget.js');
      var drawRateWidget = require('../pages/components/draw-rate-widget.js');
      var shoppersVsOthersWidget = require('../pages/components/shoppers-vs-others-widget.js');
      var abandonmentRateWidget = require('../pages/components/abandonment-rate-widget.js');

      var dataWindow = lineChart.getSumDataWindow(timePeriod); //used for line chart widget sum checks

      it('should navigate to the correct site', function () {
        var title = nav.getSingleSiteName();

        expect(title).toEqual(orgData.SSOrg.ssOrgSiteName);
      });

      it('should nav to "Visitor behavior" tab when clicked', function () {
        var tabHeading = sitePage.siteTitle();

        expect(tabHeading.getText()).toMatch('SITE VISITOR BEHAVIOR');
      });

      it('date picker should appear', function () {
        var datePicker = dateSelector.getDatePicker();

        expect(datePicker.isPresent()).toBe(true);
      });

      it('should have selected the correct date button', function () {
        var dateButton;

        if(timePeriod === 'week') {
          dateButton = dateSelector.getWeekButton();
        }
        else if(timePeriod === 'month') {
          dateButton = dateSelector.getMonthButton();
        }
        else if(timePeriod === 'year') {
          dateButton = dateSelector.getYearButton();
        }
        dateButton.then(function (button) {
          expect(button.getAttribute('class')).toMatch('active');
        });
      });

      describe('"Property overall"-level tests', function () {

        describe('Visitor behavior traffic widget', function () {

          it('should display the title "Visitor behavior traffic"', function () {
            var widgetTitle = visitorTrafficWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('Visitor behavior traffic');
          });

          it('widget chart should display correct date range on x-axis', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            var allXAxisDates = visitorTrafficWidget.getXAxisDates(userData.superUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
              var reportArray = promiseArray[0];
              var chartDateArray = promiseArray[1];
              //first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(function (dateItem) {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('widget chart should display correct range on y-axis', function () {
            var selectedPeriodDataValues = visitorTrafficWidget.getSelectedPeriodDataValues();
            var priorPeriodDataValues = visitorTrafficWidget.getPriorPeriodDataValues();
            var highestYAxisValue = visitorTrafficWidget.getHighestYAxisValue();

            if (timePeriod !== 'year') {
              var priorYearDataValues = visitorTrafficWidget.getPriorYearDataValues();
            }

            expect(highestYAxisValue).not.toBeNaN();
            expect(highestYAxisValue).toEqual(jasmine.any(Number));

            selectedPeriodDataValues.then(function (dataArray) {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(function (dataPoint) {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });


            priorPeriodDataValues.then(function (dataArray) {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(function (dataPoint) {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            if (timePeriod !== 'year') {
              priorYearDataValues.then(function (dataArray) {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(function (dataPoint) {
                  expect(dataPoint).not.toBeLessThan(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });
            }
          });

          it('sum of a line\'s data points should equal the corresponding total displayed in summary frame', function () {
            var selectedPeriodDataSum = visitorTrafficWidget.getSelectedPeriodDataSum();
            var priorPeriodDataSum = visitorTrafficWidget.getPriorPeriodDataSum();

            var selectedPeriodVisitors = visitorTrafficWidget.getSelectedPeriodOverall();
            var priorPeriodVisitors = visitorTrafficWidget.getPriorPeriodOverall();

            if (timePeriod !== 'year') {
              var priorYearVisitors = visitorTrafficWidget.getPriorYearOverall();
              var priorYearDataSum = visitorTrafficWidget.getPriorYearDataSum();
            }

            expect(selectedPeriodDataSum).not.toBeLessThan(selectedPeriodVisitors - dataWindow);
            expect(selectedPeriodDataSum).not.toBeGreaterThan(selectedPeriodVisitors + dataWindow);

            expect(priorPeriodDataSum).not.toBeLessThan(priorPeriodVisitors - dataWindow);
            expect(priorPeriodDataSum).not.toBeGreaterThan(priorPeriodVisitors + dataWindow);

            if (timePeriod !== 'year') {
              expect(priorYearDataSum).not.toBeLessThan(priorYearVisitors - dataWindow);
              expect(priorYearDataSum).not.toBeGreaterThan(priorYearVisitors + dataWindow);
            }
          });

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            var priorReportPeriod = dateSelector.getPriorReportingPeriod(userData.superUser.dateFormat);
            var selectedPeriodDate = visitorTrafficWidget.getSelectedPeriodDateRange(userData.superUser.dateFormat);
            var priorPeriodDate = visitorTrafficWidget.getPriorPeriodDateRange(userData.superUser.dateFormat);

            if (timePeriod !== 'year') {
              var priorYearReportingPeriod = dateSelector.getPriorYearReportingPeriod(timePeriod, userData.superUser.dateFormat);
              var priorYearDate = visitorTrafficWidget.getPriorYearDateRange(userData.superUser.dateFormat);
            }

            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(priorReportPeriod);

            if (timePeriod !== 'year') {
              expect(priorYearDate).toEqual(priorYearReportingPeriod);
            }
          });

          it('correct percentage deltas should appear in summary frame', function () {
            var priorPeriodDelta = visitorTrafficWidget.getPriorPeriodDelta();
            var calculatedPriorPeriodDelta = visitorTrafficWidget.calculatePriorPeriodDelta();

            if (timePeriod !== 'year') {
              var priorYearDelta = visitorTrafficWidget.getPriorYearDelta();
              var calculatedPriorYearDelta = visitorTrafficWidget.calculatePriorYearDelta();
            }

            expect(calculatedPriorPeriodDelta).toEqual(priorPeriodDelta);

            if (timePeriod !== 'year') {
              expect(calculatedPriorYearDelta).toEqual(priorYearDelta);
            }
          });

          it('"unique" and "returning" data in summary frame should sum to "overall visitors" total', function () {
            var uniqueTraffic = visitorTrafficWidget.getUniqueVisitors();
            var returningTraffic = visitorTrafficWidget.getReturningVisitors();
            var selectedPeriodVisitors = visitorTrafficWidget.getSelectedPeriodOverall();

            protractor.promise.all([uniqueTraffic, returningTraffic, selectedPeriodVisitors]).then(function (promiseArray) {
              var unique = promiseArray[0];
              var returning = promiseArray[1];
              var visitors = promiseArray[2];

              expect(unique + returning).not.toBeLessThan(visitors - 1);
              expect(unique + returning).not.toBeGreaterThan(visitors + 1);
            });
          });
        });


        describe('Visiting frequency widget', function () {

          it('should display the title "Visiting frequency"', function () {
            var widgetTitle = visitingFrequencyWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('Visiting frequency');
          });

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            var priorReportPeriod = dateSelector.getPriorReportingPeriod(userData.superUser.dateFormat);
            var selectedPeriodDate = visitingFrequencyWidget.getSelectedPeriodDateRange(userData.superUser.dateFormat);
            var priorPeriodDate = visitingFrequencyWidget.getPriorPeriodDateRange(userData.superUser.dateFormat);

            if (timePeriod !== 'year') {
              var priorYearReportingPeriod = dateSelector.getPriorYearReportingPeriod(timePeriod, userData.superUser.dateFormat);
              var priorYearDate = visitingFrequencyWidget.getPriorYearDateRange(userData.superUser.dateFormat);
            }

            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(priorReportPeriod);

            if (timePeriod !== 'year') {
              expect(priorYearDate).toEqual(priorYearReportingPeriod);
            }
          });

          it('percentages around pie chart should sum to 100', function () {
            var percents = visitingFrequencyWidget.getPiePercentSum();

            expect(percents).toEqual(100);
          });
        });

        describe('Gross shopping hours widget', function () {

          it('should display the title "Gross shopping hours"', function () {
            var widgetTitle = gshWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('Gross shopping hours');
          });

          it('widget chart should display correct date range on x-axis', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            var allXAxisDates = gshWidget.getXAxisDates(userData.superUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
              var reportArray = promiseArray[0];
              var chartDateArray = promiseArray[1];
              //first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(function (dateItem) {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('widget chart should display correct range on y-axis', function () {
            var selectedPeriodDataValues = gshWidget.getSelectedPeriodDataValues();
            var priorPeriodDataValues = gshWidget.getPriorPeriodDataValues();
            var highestYAxisValue = gshWidget.getHighestYAxisValue();

            if (timePeriod !== 'year') {
              var priorYearDataValues = gshWidget.getPriorYearDataValues();
            }

            expect(highestYAxisValue).not.toBeNaN();
            expect(highestYAxisValue).toEqual(jasmine.any(Number));

            selectedPeriodDataValues.then(function (dataArray) {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(function (dataPoint) {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            priorPeriodDataValues.then(function (dataArray) {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(function (dataPoint) {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            if (timePeriod !== 'year') {
              priorYearDataValues.then(function (dataArray) {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(function (dataPoint) {
                  expect(dataPoint).not.toBeLessThan(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });
            }
          });

          it('sum of a line\'s data points should equal the corresponding total displayed in summary frame', function () {
            var selectedPeriodDataSum = gshWidget.getSelectedPeriodDataSum();
            var priorPeriodDataSum = gshWidget.getPriorPeriodDataSum();


            var selectedPeriodHours = gshWidget.getSelectedPeriodOverall();
            var priorPeriodHours = gshWidget.getPriorPeriodOverall();

            if (timePeriod !== 'year') {
              var priorYearDataSum = gshWidget.getPriorYearDataSum();
              var priorYearHours = gshWidget.getPriorYearOverall();
            }

            expect(selectedPeriodDataSum).not.toBeNaN();
            expect(priorPeriodDataSum).not.toBeNaN();
            expect(selectedPeriodHours).not.toBeNaN();
            expect(priorPeriodHours).not.toBeNaN();

            if (timePeriod !== 'year') {
              expect(priorYearDataSum).not.toBeNaN();
              expect(priorYearHours).not.toBeNaN();
            }

            expect(selectedPeriodDataSum).toEqual((jasmine.any(Number)));
            expect(priorPeriodDataSum).toEqual((jasmine.any(Number)));

            expect(selectedPeriodHours).toEqual((jasmine.any(Number)));
            expect(priorPeriodHours).toEqual((jasmine.any(Number)));

            if (timePeriod !== 'year') {
              expect(priorYearDataSum).toEqual((jasmine.any(Number)));
              expect(priorYearHours).toEqual((jasmine.any(Number)));
            }

            expect(selectedPeriodDataSum).not.toBeGreaterThan(selectedPeriodHours + dataWindow);
            expect(selectedPeriodDataSum).not.toBeLessThan(selectedPeriodHours - dataWindow);

            expect(priorPeriodDataSum).not.toBeGreaterThan(priorPeriodHours + dataWindow);
            expect(priorPeriodDataSum).not.toBeLessThan(priorPeriodHours - dataWindow);

            if (timePeriod !== 'year') {
              expect(priorYearDataSum).not.toBeGreaterThan(priorYearHours + dataWindow);
              expect(priorYearDataSum).not.toBeLessThan(priorYearHours - dataWindow);
            }
          });

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            var priorReportPeriod = dateSelector.getPriorReportingPeriod(userData.superUser.dateFormat);
            var selectedPeriodDate = gshWidget.getSelectedPeriodDateRange(userData.superUser.dateFormat);
            var priorPeriodDate = gshWidget.getPriorPeriodDateRange(userData.superUser.dateFormat);

            if (timePeriod !== 'year') {
              var priorYearReportingPeriod = dateSelector.getPriorYearReportingPeriod(timePeriod, userData.superUser.dateFormat);
              var priorYearDate = gshWidget.getPriorYearDateRange(userData.superUser.dateFormat);
            }

            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(priorReportPeriod);

            if (timePeriod !== 'year') {
              expect(priorYearDate).toEqual(priorYearReportingPeriod);
            }
          });

          it('correct percentage deltas should appear in summary frame', function () {
            var priorPeriodDelta = gshWidget.getPriorPeriodDelta();
            var calculatedPriorPeriodDelta = gshWidget.calculatePriorPeriodDelta();

            if (timePeriod !== 'year') {
              var priorYearDelta = gshWidget.getPriorYearDelta();
              var calculatedPriorYearDelta = gshWidget.calculatePriorYearDelta();
            }

            expect(calculatedPriorPeriodDelta).toEqual(priorPeriodDelta);

            if (timePeriod !== 'year') {
              expect(calculatedPriorYearDelta).toEqual(priorYearDelta);
            }
          });
        });

        describe('Dwell time widget', function () {

          it('should display the title "Dwell time"', function () {
            var widgetTitle = dwellTimeWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('Dwell time');
          });

          it('widget chart should display correct date range on x-axis', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            var allXAxisDates = dwellTimeWidget.getXAxisDates(userData.superUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
              var reportArray = promiseArray[0];
              var chartDateArray = promiseArray[1];
              //first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(function (dateItem) {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('widget chart should display correct range on y-axis', function () {
            var selectedPeriodDataValues = dwellTimeWidget.getSelectedPeriodDataValues();
            var priorPeriodDataValues = dwellTimeWidget.getPriorPeriodDataValues();
            var highestYAxisValue = dwellTimeWidget.getHighestYAxisValue();

            if (timePeriod !== 'year') {
              var priorYearDataValues = dwellTimeWidget.getPriorYearDataValues();
            }

            expect(highestYAxisValue).not.toBeNaN();
            expect(highestYAxisValue).toEqual(jasmine.any(Number));

            selectedPeriodDataValues.then(function (dataArray) {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(function (dataPoint) {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            priorPeriodDataValues.then(function (dataArray) {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(function (dataPoint) {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            if (timePeriod !== 'year') {
              priorYearDataValues.then(function (dataArray) {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(function (dataPoint) {
                  expect(dataPoint).not.toBeLessThan(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });
            }
          });

          //checking line values vs totals displayed is not implemented - "average" values make this challenging

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            var priorReportPeriod = dateSelector.getPriorReportingPeriod(userData.superUser.dateFormat);
            var selectedPeriodDate = dwellTimeWidget.getSelectedPeriodDateRange(userData.superUser.dateFormat);
            var priorPeriodDate = dwellTimeWidget.getPriorPeriodDateRange(userData.superUser.dateFormat);

            if (timePeriod !== 'year') {
              var priorYearReportingPeriod = dateSelector.getPriorYearReportingPeriod(timePeriod, userData.superUser.dateFormat);
              var priorYearDate = dwellTimeWidget.getPriorYearDateRange(userData.superUser.dateFormat);
            }

            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(priorReportPeriod);

            if (timePeriod !== 'year') {
              expect(priorYearDate).toEqual(priorYearReportingPeriod);
            }
          });

          //checking delta values vs deltas displayed is not implemented - "average" values make this challenging
        });

        describe('Opportunity widget', function () {

          it('should display the title "Opportunity"', function () {
            var widgetTitle = opportunityWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('Opportunity');
          });

          it('widget chart should display correct date range on x-axis', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            var allXAxisDates = opportunityWidget.getXAxisDates(userData.superUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
              var reportArray = promiseArray[0];
              var chartDateArray = promiseArray[1];
              //first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(function (dateItem) {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('widget chart should display correct range on y-axis', function () {
            var selectedPeriodDataValues = opportunityWidget.getSelectedPeriodDataValues();
            var priorPeriodDataValues = opportunityWidget.getPriorPeriodDataValues();
            var highestYAxisValue = opportunityWidget.getHighestYAxisValue();

            if (timePeriod !== 'year') {
              var priorYearDataValues = opportunityWidget.getPriorYearDataValues();
            }

            expect(highestYAxisValue).not.toBeNaN();
            expect(highestYAxisValue).toEqual(jasmine.any(Number));

            selectedPeriodDataValues.then(function (dataArray) {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(function (dataPoint) {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            priorPeriodDataValues.then(function (dataArray) {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(function (dataPoint) {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            if (timePeriod !== 'year') {
              priorYearDataValues.then(function (dataArray) {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(function (dataPoint) {
                  expect(dataPoint).not.toBeLessThan(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });
            }
          });

          it('sum of a line\'s data points should equal the corresponding total displayed in summary frame', function () {
            var selectedPeriodDataSum = opportunityWidget.getSelectedPeriodDataSum();
            var priorPeriodDataSum = opportunityWidget.getPriorPeriodDataSum();


            var selectedPeriodTraffic = opportunityWidget.getSelectedPeriodOverall();
            var priorPeriodTraffic = opportunityWidget.getPriorPeriodOverall();

            if (timePeriod !== 'year') {
              var priorYearDataSum = opportunityWidget.getPriorYearDataSum();
              var priorYearTraffic = opportunityWidget.getPriorYearOverall();
            }

            expect(selectedPeriodDataSum).not.toBeLessThan(selectedPeriodTraffic - dataWindow);
            expect(selectedPeriodDataSum).not.toBeGreaterThan(selectedPeriodTraffic + dataWindow);

            expect(priorPeriodDataSum).not.toBeLessThan(priorPeriodTraffic - dataWindow);
            expect(priorPeriodDataSum).not.toBeGreaterThan(priorPeriodTraffic + dataWindow);

            if (timePeriod !== 'year') {
              expect(priorYearDataSum).not.toBeLessThan(priorYearTraffic - dataWindow);
              expect(priorYearDataSum).not.toBeGreaterThan(priorYearTraffic + dataWindow);
            }
          });

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            var priorReportPeriod = dateSelector.getPriorReportingPeriod(userData.superUser.dateFormat);
            var selectedPeriodDate = opportunityWidget.getSelectedPeriodDateRange(userData.superUser.dateFormat);
            var priorPeriodDate = opportunityWidget.getPriorPeriodDateRange(userData.superUser.dateFormat);

            if (timePeriod !== 'year') {
              var priorYearReportingPeriod = dateSelector.getPriorYearReportingPeriod(timePeriod, userData.superUser.dateFormat);
              var priorYearDate = opportunityWidget.getPriorYearDateRange(userData.superUser.dateFormat);
            }

            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(priorReportPeriod);

            if (timePeriod !== 'year') {
              expect(priorYearDate).toEqual(priorYearReportingPeriod);
            }
          });

          it('correct percentage deltas should appear in summary frame', function () {
            var priorPeriodDelta = opportunityWidget.getPriorPeriodDelta();
            var calculatedPriorPeriodDelta = opportunityWidget.calculatePriorPeriodDelta();

            if (timePeriod !== 'year') {
              var priorYearDelta = opportunityWidget.getPriorYearDelta();
              var calculatedPriorYearDelta = opportunityWidget.calculatePriorYearDelta();
            }

            expect(calculatedPriorPeriodDelta).toEqual(priorPeriodDelta);

            if (timePeriod !== 'year') {
              expect(calculatedPriorYearDelta).toEqual(priorYearDelta);
            }
          });
        });

        describe('Draw rate widget', function () {

          it('should display the title "Draw Rate"', function () {
            var widgetTitle = drawRateWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('Draw rate');
          });

          it('widget chart should display correct date range on x-axis', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            var allXAxisDates = drawRateWidget.getXAxisDates(userData.superUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
              var reportArray = promiseArray[0];
              var chartDateArray = promiseArray[1];
              //first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(function (dateItem) {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('widget chart should display correct range on y-axis', function () {
            var selectedPeriodDataValues = drawRateWidget.getSelectedPeriodDataValues();
            var priorPeriodDataValues = drawRateWidget.getPriorPeriodDataValues();
            var highestYAxisValue = drawRateWidget.getHighestYAxisValue();

            if (timePeriod !== 'year') {
              var priorYearDataValues = drawRateWidget.getPriorYearDataValues();
            }

            expect(highestYAxisValue).not.toBeNaN();
            expect(highestYAxisValue).toEqual(jasmine.any(Number));

            selectedPeriodDataValues.then(function (dataArray) {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(function (dataPoint) {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            priorPeriodDataValues.then(function (dataArray) {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(function (dataPoint) {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            if (timePeriod !== 'year') {
              priorYearDataValues.then(function (dataArray) {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(function (dataPoint) {
                  expect(dataPoint).not.toBeLessThan(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });
            }
          });

          //checking line values vs totals displayed is not implemented - "average" values make this challenging

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            var priorReportPeriod = dateSelector.getPriorReportingPeriod(userData.superUser.dateFormat);
            var selectedPeriodDate = drawRateWidget.getSelectedPeriodDateRange(userData.superUser.dateFormat);
            var priorPeriodDate = drawRateWidget.getPriorPeriodDateRange(userData.superUser.dateFormat);

            if (timePeriod !== 'year') {
              var priorYearReportingPeriod = dateSelector.getPriorYearReportingPeriod(timePeriod, userData.superUser.dateFormat);
              var priorYearDate = drawRateWidget.getPriorYearDateRange(userData.superUser.dateFormat);
            }

            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(priorReportPeriod);

            if (timePeriod !== 'year') {
              expect(priorYearDate).toEqual(priorYearReportingPeriod);
            }
          });

          //checking delta values vs deltas displayed is not implemented - "average" values make this challenging
        });

        describe('Shoppers vs others widget', function () {

          it('should display the title "Shoppers vs Others"', function () {
            var widgetTitle = shoppersVsOthersWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('Shoppers vs others');
          });

          it('widget chart should display correct date range on x-axis', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            var allXAxisDates = shoppersVsOthersWidget.getXAxisDates(userData.superUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
              var reportArray = promiseArray[0];
              var chartDateArray = promiseArray[1];
              //first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(function (dateItem) {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('widget chart should display correct range on y-axis', function () {
            var selectedPeriodDataValues = shoppersVsOthersWidget.getSelectedPeriodDataValues();
            var highestYAxisValue = shoppersVsOthersWidget.getHighestYAxisValue();

            expect(highestYAxisValue).not.toBeNaN();
            expect(highestYAxisValue).toEqual(jasmine.any(Number));

            selectedPeriodDataValues.then(function (dataArray) {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(function (dataPoint) {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });
          });

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            var priorReportPeriod = dateSelector.getPriorReportingPeriod(userData.superUser.dateFormat);
            var selectedPeriodDate = shoppersVsOthersWidget.getSelectedPeriodDateRange(userData.superUser.dateFormat);
            var priorPeriodDate = shoppersVsOthersWidget.getPriorPeriodDateRange(userData.superUser.dateFormat);

            if (timePeriod !== 'year') {
              var priorYearReportingPeriod = dateSelector.getPriorYearReportingPeriod(timePeriod, userData.superUser.dateFormat);
              var priorYearDate = shoppersVsOthersWidget.getPriorYearDateRange(userData.superUser.dateFormat);
            }

            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(priorReportPeriod);

            if (timePeriod !== 'year') {
              expect(priorYearDate).toEqual(priorYearReportingPeriod);
            }
          });

          it('"shoppers" and "others" percentages in summary frame should sum to 100', function () {
            var selectedPeriodShoppers = shoppersVsOthersWidget.getSelectedPeriodShoppers();
            var priorPeriodShoppers = shoppersVsOthersWidget.getPriorPeriodShoppers();
            var selectedPeriodOthers = shoppersVsOthersWidget.getSelectedPeriodOthers();
            var priorPeriodOthers = shoppersVsOthersWidget.getPriorPeriodOthers();

            if (timePeriod !== 'year') {
              var priorYearShoppers = shoppersVsOthersWidget.getPriorYearShoppers();
              var priorYearOthers = shoppersVsOthersWidget.getPriorYearOthers();
            }

            selectedPeriodShoppers.then(function (shoppers) {
              selectedPeriodOthers.then(function (others) {
                expect(shoppers + others).toEqual(100);
              });
            });

            priorPeriodShoppers.then(function (shoppers) {
              priorPeriodOthers.then(function (others) {
                expect(shoppers + others).toEqual(100);
              });
            });

            if (timePeriod !== 'year') {
              priorYearShoppers.then(function (shoppers) {
                priorYearOthers.then(function (others) {
                  expect(shoppers + others).toEqual(100);
                });
              });
            }
          });
        });
      });

      describe('"Single area"-level tests', function () {

        beforeAll(function () {
          browser.executeScript('window.scrollTo(0,0);').then(function () {
            sitePage.navToTestArea();
          });
        });

        it('should show the correct area name', function () {
          var areaName = sitePage.getAreaName();

          expect(areaName.getText()).toEqual(orgData.SSOrg.testArea);
        });

        it('should have selected the correct date button', function () {
          var dateButton;

          if(timePeriod === 'week') {
            dateButton = dateSelector.getWeekButton();
          }
          else if(timePeriod === 'month') {
            dateButton = dateSelector.getMonthButton();
          }
          else if(timePeriod === 'year') {
            dateButton = dateSelector.getYearButton();
          }
          dateButton.then(function (button) {
            expect(button.getAttribute('class')).toMatch('active');
          });
        });

        it('widgets shared with "property-overall" level should appear', function () {

          expect(visitorTrafficWidget.widgetTitle().isDisplayed()).toBe(true);
          expect(visitingFrequencyWidget.widgetTitle().isDisplayed()).toBe(true);
          expect(gshWidget.widgetTitle().isDisplayed()).toBe(true);
          expect(dwellTimeWidget.widgetTitle().isDisplayed()).toBe(true);
          expect(opportunityWidget.widgetTitle().isDisplayed()).toBe(true);
          expect(drawRateWidget.widgetTitle().isDisplayed()).toBe(true);
        });

        describe('Abandonment rate widget', function () {

          it('should display the title "Abandonment Rate"', function () {
            var widgetTitle = abandonmentRateWidget.widgetTitle();

            expect(widgetTitle.getText()).toEqual('Abandonment rate');
          });

          it('widget chart should display correct date range on x-axis', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            var allXAxisDates = abandonmentRateWidget.getXAxisDates(userData.superUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
              var reportArray = promiseArray[0];
              var chartDateArray = promiseArray[1];
              //first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(function (dateItem) {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('widget chart should display correct range on y-axis', function () {
            var selectedPeriodDataValues = abandonmentRateWidget.getSelectedPeriodDataValues();
            var priorPeriodDataValues = abandonmentRateWidget.getPriorPeriodDataValues();
            var highestYAxisValue = abandonmentRateWidget.getHighestYAxisValue();

            if (timePeriod !== 'year') {
              var priorYearDataValues = abandonmentRateWidget.getPriorYearDataValues();
            }

            expect(highestYAxisValue).not.toBeNaN();
            expect(highestYAxisValue).toEqual(jasmine.any(Number));

            selectedPeriodDataValues.then(function (dataArray) {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(function (dataPoint) {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            priorPeriodDataValues.then(function (dataArray) {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(function (dataPoint) {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });

            if (timePeriod !== 'year') {
              priorYearDataValues.then(function (dataArray) {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(function (dataPoint) {
                  expect(dataPoint).not.toBeLessThan(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });
            }
          });

          //checking line values vs totals displayed is not implemented - "average" values make this challenging

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
            var priorReportPeriod = dateSelector.getPriorReportingPeriod(userData.superUser.dateFormat);
            var selectedPeriodDate = abandonmentRateWidget.getSelectedPeriodDateRange(userData.superUser.dateFormat);
            var priorPeriodDate = abandonmentRateWidget.getPriorPeriodDateRange(userData.superUser.dateFormat);

            if (timePeriod !== 'year') {
              var priorYearReportingPeriod = dateSelector.getPriorYearReportingPeriod(timePeriod, userData.superUser.dateFormat);
              var priorYearDate = abandonmentRateWidget.getPriorYearDateRange(userData.superUser.dateFormat);
            }

            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(priorReportPeriod);

            if (timePeriod !== 'year') {
              expect(priorYearDate).toEqual(priorYearReportingPeriod);
            }
          });

          //checking delta values vs deltas displayed is not implemented - "average" values make this challenging
        });
      });
    });
  }
};
