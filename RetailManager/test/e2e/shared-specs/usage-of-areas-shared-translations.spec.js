'use strict';

module.exports = {
//shared tests for localization translations, using prior period/prior year date ranges

  usageOfAreasSharedLangTests: function (locale) {

    describe('Usage of Areas tab (shared translation tests)', function () {

      var translationsFilePath = '../../../src/l10n/languages/' + locale + '.json'; //sets up filepath for required translations file
      var translations = require(translationsFilePath);

      var sitePage = require('../pages/site-summary-page.js');
      var dateSelector = require('../pages/components/time-period-picker.js');
      var nav = require('../pages/components/nav-header.js');
      var tabSidebar = require('../pages/components/tab-nav.js');
      var orgData = require('../data/orgs.js');
      var trafficHeatmapWidget = require('../pages/components/traffic-heatmap-widget.js');
      var firstVisitsHeatmapWidget = require('../pages/components/first-visits-heatmap-widget.js');
      var oneAndDoneHeatmapWidget = require('../pages/components/one-done-heatmap-widget.js');
      var correlationHeatmapWidget = require('../pages/components/correlation-heatmap-widget.js');
      var visitsAfterHeatmapWidget = require('../pages/components/visits-after-heatmap-widget.js');
      var visitsBeforeHeatmapWidget = require('../pages/components/visits-before-heatmap-widget.js');

      it('should navigate to the correct site', function () {
        var title = nav.getSingleSiteName();

        expect(title).toEqual(orgData.SSOrg.ssOrgSiteName);
      });

      describe('"Property overall"-level tests', function () {

        describe('Page title and tab sidebar checks', function () {

          it('"Site Usage of areas" tab title', function () {
            var tabHeading = sitePage.siteTitle();
            var tabText = tabSidebar.getUsageOfAreasTabName();

            expect(tabHeading.getText()).toEqual(translations.common.SITE.toUpperCase() + ' ' + translations.views.USAGEOFAREAS.toUpperCase());
          });

          it('"page title should show the correct location" area name', function () {
            var areaName = sitePage.getAreaName();

            expect(areaName.getText()).toEqual(orgData.SSOrg.ssOrgSiteName);
          });

          it('"select area" picker title', function () {
            var pickerTitle = sitePage.getZonePickerTitleText();

            expect(pickerTitle).toEqual(translations.analyticsHeader.SELECTAREA.toUpperCase())
          });

          it('default value in area-picker search bar', function () {
            var pickerTitle = sitePage.getAreaPickerSearchText();

            expect(pickerTitle).toEqual(translations.locationSelector.SEARCH)
          });

          it('area-type filter buttons', function() {
            var buttons = sitePage.areaTypeFilterButtons;

            expect(sitePage.getAreaTypeFilter(buttons[0]).getText()).toEqual(translations.usageOfAreasView.STORES.toUpperCase());
            expect(sitePage.getAreaTypeFilter(buttons[1]).getText()).toEqual(translations.usageOfAreasView.CORRIDORS.toUpperCase());
            expect(sitePage.getAreaTypeFilter(buttons[2]).getText()).toEqual(translations.usageOfAreasView.ENTRANCES.toUpperCase());
            expect(sitePage.getAreaTypeFilter(buttons[3]).getText()).toEqual(translations.usageOfAreasView.ALL.toUpperCase());
          })
        });

        describe('Traffic widget', function() {

          it('widget title', function () {
            expect(trafficHeatmapWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.traffic_percentage)
          });

          it('text in heatmap legend', function(){
            var textArray = trafficHeatmapWidget.getLegendTextArray();

            expect(textArray).toEqual([translations.heatmapWidget.VISITORS, translations.heatmapWidget.LOW, translations.heatmapWidget.HIGH]);
          });

          it('percentage breakdown frame title', function() {
            expect(trafficHeatmapWidget.getPercentageFrameTitle()).toEqual(translations.kpis.totalLabel.traffic_percentage);
          })
        });

        describe('First locations widget', function() {

          it('widget title', function () {
            expect(firstVisitsHeatmapWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.first_visits)
          });

          it('text in heatmap legend', function(){
            var textArray = firstVisitsHeatmapWidget.getLegendTextArray();

            expect(textArray).toEqual([translations.heatmapWidget.VISITORS, translations.heatmapWidget.LOW, translations.heatmapWidget.HIGH]);
          });

          it('percentage breakdown frame title', function() {
            expect(firstVisitsHeatmapWidget.getPercentageFrameTitle()).toEqual(translations.kpis.totalLabel.first_visits);
          })
        });

        describe('One-and-done widget', function() {

          it('widget title', function () {
            expect(oneAndDoneHeatmapWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.one_and_done)
          });

          it('text in heatmap legend', function(){
            var textArray = oneAndDoneHeatmapWidget.getLegendTextArray();

            expect(textArray).toEqual([translations.heatmapWidget.VISITORS, translations.heatmapWidget.LOW, translations.heatmapWidget.HIGH]);
          });

          it('percentage breakdown frame title', function() {
            expect(oneAndDoneHeatmapWidget.getPercentageFrameTitle()).toEqual(translations.kpis.totalLabel.one_and_done);
          })
        })
      });

      describe('Area-level tests', function () {

        beforeAll(function () {
          browser.executeScript('window.scrollTo(0,0);').then(function () {
            sitePage.navToTestArea();
          });
        });

        describe('Page title and tab sidebar checks', function () {

          it('"Usage of areas" tab title', function () {
            var tabHeading = sitePage.siteTitle();
            var tabText = tabSidebar.getUsageOfAreasTabName();

            expect(tabHeading.getText()).toEqual(translations.common.AREA.toUpperCase() + ' ' + translations.views.USAGEOFAREAS.toUpperCase());
          });

          it('test area name', function() {
            var areaName = sitePage.getAreaName();

            expect(areaName).toMatch(orgData.SSOrg.testArea)
          });
        });

        describe('Area-level correlation heat map widget', function () {

          it('widget title', function () {
            expect(correlationHeatmapWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.traffic_correlation)
          });

          it('text in heatmap legend', function(){
            var textArray = correlationHeatmapWidget.getLegendTextArray();

            expect(textArray).toEqual([translations.heatmapWidget.VISITORS, translations.heatmapWidget.LOW, translations.heatmapWidget.HIGH, translations.heatmapWidget.SELECTEDAREA]);
          });

          it('percentage breakdown frame text', function() {
            expect(correlationHeatmapWidget.getPercentageFrameHeaderText()).toEqual(translations.heatmapWidget.CUSTOMERSWHOVISITED);
            expect(correlationHeatmapWidget.getPercentageFrameTitle()).toEqual(translations.kpis.totalLabel.traffic_correlation);
          })
        });

        describe('Area-level locations-after widget', function () {

          it('widget title', function () {
            expect(visitsAfterHeatmapWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.locations_after)
          });

          it('text in heatmap legend', function(){
            var textArray = visitsAfterHeatmapWidget.getLegendTextArray();

            expect(textArray).toEqual([translations.heatmapWidget.VISITORS, translations.heatmapWidget.LOW, translations.heatmapWidget.HIGH, translations.heatmapWidget.SELECTEDAREA]);
          });

          it('percentage breakdown frame text', function() {
            expect(visitsAfterHeatmapWidget.getPercentageFrameHeaderText()).toEqual(translations.heatmapWidget.CUSTOMERSWHOVISITED);
            expect(visitsAfterHeatmapWidget.getPercentageFrameTitle()).toEqual(translations.kpis.totalLabel.locations_after);
          })
        });

        describe('Area-level locations-before widget', function () {

          it('widget title', function () {
            expect(visitsBeforeHeatmapWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.locations_before)
          });

          it('text in heatmap legend', function(){
            var textArray = visitsBeforeHeatmapWidget.getLegendTextArray();

            expect(textArray).toEqual([translations.heatmapWidget.VISITORS, translations.heatmapWidget.LOW, translations.heatmapWidget.HIGH, translations.heatmapWidget.SELECTEDAREA]);
          });

          it('percentage breakdown frame text', function() {
            expect(visitsBeforeHeatmapWidget.getPercentageFrameHeaderText()).toEqual(translations.heatmapWidget.CUSTOMERSWHOVISITED);
            expect(visitsBeforeHeatmapWidget.getPercentageFrameTitle()).toEqual(translations.kpis.totalLabel.locations_before);
          })
        })
      })
    })
  }
};
