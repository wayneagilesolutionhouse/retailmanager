const sitePage = require('../pages/site-summary-page.js');
const dateSelector = require('../pages/components/time-period-picker.js');
const nav = require('../pages/components/nav-header.js');
const orgData = require('../data/orgs.js');
const userData = require('../data/users.js');
const entranceSummaryWidget = require('../pages/components/entrance-summary-widget.js');
const tenantSummaryWidget = require('../pages/components/tenant-traffic-summary-widget.js');
const entranceContributionWidget = require('../pages/components/entrance-contribution-widget.js');
const commonAreasSummaryWidget = require('../pages/components/common-areas-summary-widget.js');
const powerHoursWidget = require('../pages/components/power-hours-widget.js');
const dailyMetricBarWidget = require('../pages/components/daily-metric-averages-widget.js');
const trafficWidget = require('../pages/components/site-traffic-widget.js');

module.exports = {


  trafficSharedTests(timePeriod) {
    describe('Traffic tab (shared tests)', () => {
      it('should navigate to the correct site', () => {
        const title = nav.getSiteName();

        expect(title).toEqual(orgData.MSOrgSite.name);
      });

      it('should nav to "Site Traffic" tab when clicked', () => {
        const tabHeading = sitePage.siteTitle();

        expect(tabHeading.getText()).toEqual('SITE TRAFFIC');
      });

      it('date picker should appear', () => {
        const datePicker = dateSelector.getDatePicker();

        expect(datePicker.isPresent()).toBe(true);
      });

      it('should have selected the correct date button', () => {
        let dateButton;

        if (timePeriod === 'week') {
          dateButton = dateSelector.getWeekButton();
        } else if (timePeriod === 'month') {
          dateButton = dateSelector.getMonthButton();
        } else if (timePeriod === 'year') {
          dateButton = dateSelector.getYearButton();
        }
        dateButton.then(button => {
          expect(button.getAttribute('class')).toMatch('active');
        });
      });

      describe('Traffic widget:', () => {
        it('should display the title "Traffic"', () => {
          const widgetTitle = trafficWidget.widgetTitle();

          expect(widgetTitle.getText()).toEqual('Traffic');
        });

        // checks that x-axis values fit within selected date range
        it('widget chart should display correct date range on x-axis', () => {
          const reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
          const allXAxisDates = trafficWidget.getXAxisDates(userData.superUser.dateFormat);

          protractor.promise.all([reportPeriod, allXAxisDates]).then(promiseArray => {
            const reportArray = promiseArray[0];
            const chartDateArray = promiseArray[1];
            // first point on x-axis should match start date of time period
            expect(chartDateArray[0]).toEqual(reportArray[0]);
            chartDateArray.forEach(dateItem => {
              expect(dateItem).not.toBeLessThan(reportArray[0]);
              expect(dateItem).not.toBeGreaterThan(reportArray[1]);
            });
          });
        });

        it('widget chart should display correct range on y-axis', () => {
          const selectedPeriodDataValues = trafficWidget.getSelectedPeriodDataValues();
          const priorPeriodDataValues = trafficWidget.getPriorPeriodDataValues();
          const highestYAxisValue = trafficWidget.getHighestYAxisValue();
          let priorYearDataValues;

          if (timePeriod !== 'year') {
            priorYearDataValues = trafficWidget.getPriorYearDataValues();
          }

          expect(highestYAxisValue).not.toBeNaN();
          expect(highestYAxisValue).toEqual(jasmine.any(Number));

          selectedPeriodDataValues.then(dataArray => {
            expect(dataArray.length).toBeGreaterThan(0);
            dataArray.forEach(dataPoint => {
              expect(dataPoint).not.toBeLessThan(0);
              expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
            });
          });

          priorPeriodDataValues.then(dataArray => {
            expect(dataArray.length).toBeGreaterThan(0);
            dataArray.forEach(dataPoint => {
              expect(dataPoint).not.toBeLessThan(0);
              expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
            });
          });

          if (timePeriod !== 'year') {
            priorYearDataValues.then(dataArray => {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(dataPoint => {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });
          }
        });

        it('sum of a line\'s data points should equal the corresponding total displayed in "overall visitors" frame', () => {
          const selectedPeriodDataSum = trafficWidget.getSelectedPeriodDataSum();
          const priorPeriodDataSum = trafficWidget.getPriorPeriodDataSum();

          const selectedPeriodVisitors = trafficWidget.getSelectedPeriodOverall();
          const priorPeriodVisitors = trafficWidget.getPriorPeriodOverall();

          let priorYearDataSum;
          let priorYearVisitors;

          if (timePeriod !== 'year') {
            priorYearDataSum = trafficWidget.getPriorYearDataSum();
            priorYearVisitors = trafficWidget.getPriorYearOverall();
          }

          expect(selectedPeriodDataSum).toEqual(selectedPeriodVisitors);
          expect(priorPeriodDataSum).toEqual(priorPeriodVisitors);

          if (timePeriod !== 'year') {
            expect(priorYearDataSum).toEqual(priorYearVisitors);
          }
        });

        it('correct date range should appear in "overall visitors" frame', () => {
          const reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
          const priorReportPeriod = dateSelector.getPriorReportingPeriod(userData.superUser.dateFormat);

          const selectedPeriodDate = trafficWidget.getSelectedPeriodDateRange(userData.superUser.dateFormat);
          const priorPeriodDate = trafficWidget.getPriorPeriodDateRange(userData.superUser.dateFormat);

          let priorYearReportingPeriod;
          let priorYearDate;

          if (timePeriod !== 'year') {
            priorYearReportingPeriod = dateSelector.getPriorYearReportingPeriod(timePeriod, userData.superUser.dateFormat);
            priorYearDate = trafficWidget.getPriorYearDateRange(userData.superUser.dateFormat);
          }

          expect(selectedPeriodDate).toEqual(reportPeriod);
          expect(priorPeriodDate).toEqual(priorReportPeriod);

          if (timePeriod !== 'year') {
            protractor.promise.all([priorYearDate, priorYearReportingPeriod]).then(function (promiseArray) {
              expect(promiseArray[0]).toEqual(promiseArray[1]);
            })

          }
        });

        it('correct percentage deltas should appear in "overall visitors" frame', () => {
          const priorPeriodDelta = trafficWidget.getPriorPeriodDelta();
          const calculatedPriorPeriodDelta = trafficWidget.calculatePriorPeriodDelta();

          let priorYearDelta;
          let calculatedPriorYearDelta;

          if (timePeriod !== 'year') {
            priorYearDelta = trafficWidget.getPriorYearDelta();
            calculatedPriorYearDelta = trafficWidget.calculatePriorYearDelta();
          }

          expect(calculatedPriorPeriodDelta).toEqual(priorPeriodDelta);

          if (timePeriod !== 'year') {
            expect(calculatedPriorYearDelta).toEqual(priorYearDelta);
          }
        });
      });

      describe('Entrance summary widget:', () => {
        beforeAll(() => {
          entranceSummaryWidget.clickExpandButtonIfPresent();
        });

        it('should display the title "Entrance summary"', () => {
          const widgetTitle = entranceSummaryWidget.widgetTitle();

          expect(widgetTitle.getText()).toEqual('Entrance summary');
        });

        it('should show all 5 data columns', () => {
          const columnHeadings = entranceSummaryWidget.columnHeadings;

          expect(entranceSummaryWidget.getCurrentColumnHeader().getText()).toEqual(columnHeadings[0]);
          expect(entranceSummaryWidget.getPeriodChangeColumnHeader().getText()).toEqual(columnHeadings[1]);
          expect(entranceSummaryWidget.getPeriodColumnHeader().getText()).toEqual(columnHeadings[2]);
          expect(entranceSummaryWidget.getYearChangeColumnHeader().getText()).toEqual(columnHeadings[3]);
          expect(entranceSummaryWidget.getYearColumnHeader().getText()).toEqual(columnHeadings[4]);
        });

        it('should display all test site entrances in the table list', () => {
          const entranceList = entranceSummaryWidget.getFilteredEntranceList();

          orgData.MSOrgSite.entrances.forEach(site => {
            expect(entranceList).toContain(site);
          });
        });

        it('should change the entrance list when filtered', () => {
          const filter = entranceSummaryWidget.getFilter();

          filter.clear();
          expect(entranceSummaryWidget.getFilteredEntranceList()).toContain('Entrance3-Outer-Gnd');

          filter.sendKeys('amusing test string');
          expect(entranceSummaryWidget.getFilteredEntranceList()).toEqual([]);

          filter.clear();
          filter.sendKeys('Entrance3-Outer-Gnd');
          expect(entranceSummaryWidget.getFilteredEntranceList()).toContain('Entrance3-Outer-Gnd');
          expect(entranceSummaryWidget.getFilteredEntranceList()).not.toContain('Entrance4-Outer-Gnd');
          filter.clear();
        });

        it('should have data in each column', () => {
          entranceSummaryWidget.getFilteredEntranceList().then(array => {
            expect(array.length).toBeGreaterThan(0);
            expect(array[0]).not.toBeNaN();
          });
          entranceSummaryWidget.getCurrentPeriodColumn().then(array => {
            expect(array.length).toBeGreaterThan(0);
            expect(array[0]).not.toBeNaN();
          });
          entranceSummaryWidget.getPriorPeriodDeltaColumn().then(array => {
            expect(array.length).toBeGreaterThan(0);
            expect(array[0]).not.toBeNaN();
          });
          entranceSummaryWidget.getPriorPeriodColumn().then(array => {
            expect(array.length).toBeGreaterThan(0);
            expect(array[0]).not.toBeNaN();
          });

          if (timePeriod !== 'year') {
            entranceSummaryWidget.getPriorYearDeltaColumn().then(array => {
              expect(array.length).toBeGreaterThan(0);
              expect(array[0]).not.toBeNaN();
            });
            entranceSummaryWidget.getPriorYearColumn().then(array => {
              expect(array.length).toBeGreaterThan(0);
              expect(array[0]).not.toBeNaN();
            });
          }
        });

        it('(may fail until SA-1262 is resolved) should display the correct percentage deltas in each row', () => {
          const priorPeriodDeltas = entranceSummaryWidget.getPriorPeriodDeltaColumn();
          const calculatedPriorPeriodDeltas = entranceSummaryWidget.calculatePriorPeriodDeltaColumn();

          let priorYearDeltas;
          let calculatedPriorYearDeltas;

          if (timePeriod !== 'year') {
            priorYearDeltas = entranceSummaryWidget.getPriorYearDeltaColumn();
            calculatedPriorYearDeltas = entranceSummaryWidget.calculatePriorYearDeltaColumn();
          }

          protractor.promise.all([calculatedPriorPeriodDeltas, priorPeriodDeltas]).then(promiseArray => {
            const calculatedPriorPeriodArray = promiseArray[0];
            const priorPeriodArray = promiseArray[1];

            calculatedPriorPeriodArray.forEach((delta, index) => {
              expect(delta).not.toBeNaN();
              expect(delta).toEqual(jasmine.any(Number));
              expect(priorPeriodArray[index]).not.toBeNaN();
              expect(priorPeriodArray[index]).toEqual(jasmine.any(Number));
              expect(delta).not.toBeLessThan(priorPeriodArray[index] - 0.2);
              expect(delta).not.toBeGreaterThan(priorPeriodArray[index] + 0.2);
            });
          });

          if (timePeriod !== 'year') {
            protractor.promise.all([calculatedPriorYearDeltas, priorYearDeltas]).then(promiseArray => {
              const calculatedPriorPeriodArray = promiseArray[0];
              const priorPeriodArray = promiseArray[1];

              calculatedPriorPeriodArray.forEach((delta, index) => {
                expect(delta).not.toBeNaN();
                expect(delta).toEqual(jasmine.any(Number));
                expect(priorPeriodArray[index]).not.toBeNaN();
                expect(priorPeriodArray[index]).toEqual(jasmine.any(Number));
                expect(delta).not.toBeLessThan(priorPeriodArray[index] - 0.2);
                expect(delta).not.toBeGreaterThan(priorPeriodArray[index] + 0.2);
              });
            });
          }
        });
      });

      describe('Entrance contribution widget:', () => {
        it('should display the title "Entrance contribution"', () => {
          const widgetTitle = entranceContributionWidget.widgetTitle();

          expect(widgetTitle.getText()).toEqual('Entrance contribution');
        });

        it('should show all entrances in site in list panel', () => {
          orgData.MSOrgSite.entrances.forEach(entrance => {
            expect(entranceContributionWidget.entranceListItem(entrance).getText()).toMatch(entrance);
            // RegEx matches number followed by % to confirm data is present
            expect(entranceContributionWidget.entranceListItem(entrance).getText()).toMatch(entranceContributionWidget.entranceRegEx(entrance));
          });
        });

        it('entrance percentages in list panel should sum to 100', () => {
          expect(entranceContributionWidget.getListPercentSum()).toEqual(100);
        });
      });

      describe('Power hours widget:', () => {
        it('should display the title "Power hours"', () => {
          const widgetTitle = powerHoursWidget.widgetTitle();

          expect(widgetTitle.getText()).toEqual('Power hours');
        });

        it('should display 7 day columns and right-most/left-most columns', () => {
          expect(powerHoursWidget.getDayHeaders()).toEqual(powerHoursWidget.dayHeaders);
          expect(powerHoursWidget.getTotalHeader().isPresent()).toBe(true);
          expect(powerHoursWidget.getBlankHeader().isPresent()).toBe(true);
        });

        it('should display 24 hour rows and 1 total row', () => {
          expect(powerHoursWidget.getRowHeaders()).toEqual(orgData.MSOrgSite.powerHoursRows);
          expect(powerHoursWidget.getTotalRow().isPresent()).toBe(true);
        });

        describe('widget tests using percentage data', () => {
          beforeAll(() => {
            powerHoursWidget.selectMetricDropdown('Traffic%');
          });

          it('should display percentage data', () => {
            expect(powerHoursWidget.getTotalRow().getText()).toMatch(/%/);
          });

          it('"Daily total" row should sum to ~100', () => {
            const rowSum = powerHoursWidget.getTotalRowSum();
            const weeklyTotalTraffic = powerHoursWidget.getWeeklyTotalTraffic();

            expect(rowSum).not.toBeNaN();
            expect(rowSum).toEqual((jasmine.any(Number)));
            expect(rowSum).not.toBeGreaterThan(weeklyTotalTraffic + 5);
            expect(rowSum).not.toBeLessThan(weeklyTotalTraffic - 5);
          });

          it('each row should sum to the total shown in the right-hand "Total" column', () => {
            function checkRow(totalColumn, index) {
              powerHoursWidget.getHourRowSum(index).then(rowSum => {
                expect(totalColumn[index]).not.toBeNaN();
                expect(totalColumn[index]).toEqual((jasmine.any(Number)));
                expect(totalColumn[index]).not.toBeGreaterThan(rowSum + 1);
                expect(totalColumn[index]).not.toBeLessThan(rowSum - 1);
              });
            }

            powerHoursWidget.getTotalColumnArray().then(totalColumn => {
              let i = 0;
              do {
                checkRow(totalColumn, i);
                i += 1;
              } while (i <= 23);
            });
          });

          it('"Total" column should sum to ~100', () => {
            const weeklyTotalTraffic = powerHoursWidget.getWeeklyTotalTraffic();
            const columnSum = powerHoursWidget.getTotalColumnSum();

            expect(columnSum).not.toBeNaN();
            expect(columnSum).not.toBeGreaterThan(weeklyTotalTraffic + 5);
            expect(columnSum).not.toBeLessThan(weeklyTotalTraffic - 5);
          });
        });

        describe('widget tests using traffic data', () => {
          beforeAll(() => {
            powerHoursWidget.selectMetricDropdown('Average Traffic');
          });

          it('should display traffic data', () => {
            expect(powerHoursWidget.getTotalRow().getText()).not.toMatch(/%/);
          });

          it('"Daily total" row should sum to the total shown in the right-hand "Total" column', () => {
            const rowSum = powerHoursWidget.getTotalRowSum();
            const weeklyTotalTraffic = powerHoursWidget.getWeeklyTotalTraffic();

            expect(rowSum).not.toBeNaN();
            expect(rowSum).toEqual((jasmine.any(Number)));
            expect(rowSum).not.toBeGreaterThan(weeklyTotalTraffic + 5);
            expect(rowSum).not.toBeLessThan(weeklyTotalTraffic - 5);
          });

          it('each row should sum to the total shown in the right-hand "Total" column', () => {
            function checkRow(totalColumn, index) {
              powerHoursWidget.getHourRowSum(index).then(rowSum => {
                expect(totalColumn[index]).not.toBeNaN();
                expect(totalColumn[index]).toEqual(jasmine.any(Number));
                expect(totalColumn[index]).not.toBeGreaterThan(rowSum + 3);
                expect(totalColumn[index]).not.toBeLessThan(rowSum - 3);
              });
            }

            powerHoursWidget.getTotalColumnArray().then(totalColumn => {
              let i = 0;
              do {
                checkRow(totalColumn, i);
                i += 1;
              } while (i <= 23);
            });
          });

          it('"Total" column should sum to correct value shown in "Daily total" row', () => {
            const weeklyTotalTraffic = powerHoursWidget.getWeeklyTotalTraffic();
            const columnSum = powerHoursWidget.getTotalColumnSum();

            expect(columnSum).not.toBeNaN();
            expect(columnSum).not.toBeGreaterThan(weeklyTotalTraffic + 3);
            expect(columnSum).not.toBeLessThan(weeklyTotalTraffic - 3);
          });
        });
      });

      describe('Daily average metric widget', () => {
        it('should display the title "Daily averages: Traffic"', () => {
          const widgetTitle = dailyMetricBarWidget.widgetTitle();

          expect(widgetTitle.getText()).toEqual('Daily averages: Traffic');
        });

        it('should display 7 days along the x-axis', () => {
          expect(dailyMetricBarWidget.getXAxisLabels()).toEqual(dailyMetricBarWidget.getDayHeaders());
        });

        it('widget chart should display correct range on y-axis', () => {
          const highestYAxisValue = dailyMetricBarWidget.getHighestYAxisValue();
          const selectedPeriodDataValues = dailyMetricBarWidget.getSelectedPeriodDataValues();
          const priorPeriodDataValues = dailyMetricBarWidget.getPriorPeriodDataValues();
          let priorYearDataValues;

          if (timePeriod !== 'year') {
            priorYearDataValues = dailyMetricBarWidget.getPriorYearDataValues();
          }

          expect(highestYAxisValue).not.toBeNaN();
          expect(highestYAxisValue).toEqual(jasmine.any(Number));

          selectedPeriodDataValues.then(dataArray => {
            expect(dataArray.length).toBeGreaterThan(0);
            dataArray.forEach(dataPoint => {
              expect(dataPoint).not.toBeLessThan(0);
              expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
            });
          });

          priorPeriodDataValues.then(dataArray => {
            expect(dataArray.length).toBeGreaterThan(0);
            dataArray.forEach(dataPoint => {
              expect(dataPoint).not.toBeLessThan(0);
              expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
            });
          });

          if (timePeriod !== 'year') {
            priorYearDataValues.then(dataArray => {
              expect(dataArray.length).toBeGreaterThan(0);
              dataArray.forEach(dataPoint => {
                expect(dataPoint).not.toBeLessThan(0);
                expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
              });
            });
          }
        });

        it('table should show/hide when "show table" button is clicked', () => {
          expect(dailyMetricBarWidget.isTableDisplayed()).toEqual(false);
          dailyMetricBarWidget.getExpandTableButton().click();
          expect(dailyMetricBarWidget.isTableDisplayed()).toEqual(true);
        });

        it('each table column should be sortable, ascending and descending', () => {
          ['traffic-delta', 'traffic', 'day'].forEach(metric => {
            dailyMetricBarWidget.sortTableBy(metric);
            const sortedColumn = dailyMetricBarWidget.getColumnData(metric);
            dailyMetricBarWidget.sortTableBy(metric);
            const reverseSortedColumn = dailyMetricBarWidget.getColumnData(metric);
            expect(sortedColumn).not.toEqual(reverseSortedColumn);
          });
        });
      });

      describe('Tenant summary widget:', () => {
        beforeAll(() => {
          tenantSummaryWidget.clickExpandButtonIfPresent();
        });

        it('should display the title "Tenant Summary"', () => {
          const widgetTitle = tenantSummaryWidget.widgetTitle();

          expect(widgetTitle.getText()).toEqual('Tenant summary');
        });

        it('should show all 5 columns', () => {
          const columnHeadings = tenantSummaryWidget.columnHeadings;
          expect(tenantSummaryWidget.getCurrentColumnHeader().getText()).toEqual(columnHeadings[0]);
          expect(tenantSummaryWidget.getPeriodChangeColumnHeader().getText()).toEqual(columnHeadings[1]);
          expect(tenantSummaryWidget.getPeriodColumnHeader().getText()).toEqual(columnHeadings[2]);
          expect(tenantSummaryWidget.getYearChangeColumnHeader().getText()).toEqual(columnHeadings[3]);
          expect(tenantSummaryWidget.getYearColumnHeader().getText()).toEqual(columnHeadings[4]);
        });

        it('should display all test site tenants in the table list', () => {
          const tenantList = tenantSummaryWidget.getFilteredTenantList();

          orgData.MSOrgSite.tenants.forEach(site => {
            expect(tenantList).toContain(site);
          });
        });

        it('should change the tenant list when filtered', () => {
          const filter = tenantSummaryWidget.getFilter();

          filter.clear();
          expect(tenantSummaryWidget.getFilteredTenantList()).toContain('New Yorker 119905');

          filter.sendKeys('amusing test string');
          expect(tenantSummaryWidget.getFilteredTenantList()).toEqual([]);

          filter.clear();
          filter.sendKeys('yorker');
          expect(tenantSummaryWidget.getFilteredTenantList()).toContain('New Yorker 119905');
          expect(tenantSummaryWidget.getFilteredTenantList()).not.toContain('Gap Ext 109522');
          filter.clear();
        });

        it('should have data in each column', () => {
          tenantSummaryWidget.getFilteredTenantList().then(array => {
            expect(array.length).toBeGreaterThan(0);
            expect(array[0]).not.toBeNaN();
          });
          tenantSummaryWidget.getCurrentPeriodColumn().then(array => {
            expect(array.length).toBeGreaterThan(0);
            expect(array[0]).not.toBeNaN();
          });
          tenantSummaryWidget.getPriorPeriodDeltaColumn().then(array => {
            expect(array.length).toBeGreaterThan(0);
            expect(array[0]).not.toBeNaN();
          });
          tenantSummaryWidget.getPriorPeriodColumn().then(array => {
            expect(array.length).toBeGreaterThan(0);
            expect(array[0]).not.toBeNaN();
          });

          if (timePeriod !== 'year') {
            tenantSummaryWidget.getPriorYearDeltaColumn().then(array => {
              expect(array.length).toBeGreaterThan(0);
              expect(array[0]).not.toBeNaN();
            });
            tenantSummaryWidget.getPriorYearColumn().then(array => {
              expect(array.length).toBeGreaterThan(0);
              expect(array[0]).not.toBeNaN();
            });
          }
        });

        it('should display the correct percentage deltas in each row', () => {
          const priorPeriodDeltas = tenantSummaryWidget.getPriorPeriodDeltaColumn();
          const calculatedPriorPeriodDeltas = tenantSummaryWidget.calculatePriorPeriodDeltaColumn();
          let priorYearDeltas;
          let calculatedPriorYearDeltas;

          if (timePeriod !== 'year') {
            priorYearDeltas = tenantSummaryWidget.getPriorYearDeltaColumn();
            calculatedPriorYearDeltas = tenantSummaryWidget.calculatePriorYearDeltaColumn();
          }

          protractor.promise.all([calculatedPriorPeriodDeltas, priorPeriodDeltas]).then(promiseArray => {
            const calculatedPriorPeriodArray = promiseArray[0];
            const priorPeriodArray = promiseArray[1];

            calculatedPriorPeriodArray.forEach((delta, index) => {
              expect(delta).not.toBeNaN();
              expect(delta).toEqual(jasmine.any(Number));
              expect(priorPeriodArray[index]).not.toBeNaN();
              expect(priorPeriodArray[index]).toEqual(jasmine.any(Number));
              expect(delta).not.toBeLessThan(priorPeriodArray[index] - 0.2);
              expect(delta).not.toBeGreaterThan(priorPeriodArray[index] + 0.2);
            });
          });

          if (timePeriod !== 'year') {
            protractor.promise.all([calculatedPriorYearDeltas, priorYearDeltas]).then(promiseArray => {
              const calculatedPriorPeriodArray = promiseArray[0];
              const priorPeriodArray = promiseArray[1];

              calculatedPriorPeriodArray.forEach((delta, index) => {
                expect(delta).not.toBeNaN();
                expect(delta).toEqual(jasmine.any(Number));
                expect(priorPeriodArray[index]).not.toBeNaN();
                expect(priorPeriodArray[index]).toEqual(jasmine.any(Number));
                expect(delta).not.toBeLessThan(priorPeriodArray[index] - 0.2);
                expect(delta).not.toBeGreaterThan(priorPeriodArray[index] + 0.2);
              });
            });
          }
        });
      });

      describe('Common areas summary widget:', () => {
        it('should display the title "Common areas summary"', () => {
          const widgetTitle = commonAreasSummaryWidget.widgetTitle();

          expect(widgetTitle.getText()).toEqual('Common areas summary');
        });

        it('should show all 5 columns', () => {
          const columnHeadings = commonAreasSummaryWidget.columnHeadings;
          expect(commonAreasSummaryWidget.getCurrentColumnHeader().getText()).toEqual(columnHeadings[0]);
          expect(commonAreasSummaryWidget.getPeriodChangeColumnHeader().getText()).toEqual(columnHeadings[1]);
          expect(commonAreasSummaryWidget.getPeriodColumnHeader().getText()).toEqual(columnHeadings[2]);
          expect(commonAreasSummaryWidget.getYearChangeColumnHeader().getText()).toEqual(columnHeadings[3]);
          expect(commonAreasSummaryWidget.getYearColumnHeader().getText()).toEqual(columnHeadings[4]);
        });

        it('should display all test site common areas in the table list', () => {
          const commonAreaList = commonAreasSummaryWidget.getFilteredAreaList();

          orgData.MSOrgSite.commonAreas.forEach(site => {
            expect(commonAreaList).toContain(site);
          });
        });

        it('should change the area list when filtered', () => {
          const filter = commonAreasSummaryWidget.getFilter();

          filter.clear();
          expect(commonAreasSummaryWidget.getFilteredAreaList()).toContain('Mall Outer');

          filter.sendKeys('amusing test string');
          expect(commonAreasSummaryWidget.getFilteredAreaList()).toEqual([]);

          filter.clear();
          filter.sendKeys('perimeter');
          expect(commonAreasSummaryWidget.getFilteredAreaList()).toContain('Mall Perimeter');
          expect(commonAreasSummaryWidget.getFilteredAreaList()).not.toContain('Mall Outer');
          filter.clear();
        });

        it('should have data in each column', () => {
          commonAreasSummaryWidget.getFilteredAreaList().then(array => {
            expect(array.length).toBeGreaterThan(0);
            expect(array[0]).not.toBeNaN();
          });
          commonAreasSummaryWidget.getCurrentPeriodColumn().then(array => {
            expect(array.length).toBeGreaterThan(0);
            expect(array[0]).not.toBeNaN();
          });
          commonAreasSummaryWidget.getPriorPeriodDeltaColumn().then(array => {
            expect(array.length).toBeGreaterThan(0);
            expect(array[0]).not.toBeNaN();
          });
          commonAreasSummaryWidget.getPriorPeriodColumn().then(array => {
            expect(array.length).toBeGreaterThan(0);
            expect(array[0]).not.toBeNaN();
          });

          if (timePeriod !== 'year') {
            commonAreasSummaryWidget.getPriorYearDeltaColumn().then(array => {
              expect(array.length).toBeGreaterThan(0);
              expect(array[0]).not.toBeNaN();
            });
            commonAreasSummaryWidget.getPriorYearColumn().then(array => {
              expect(array.length).toBeGreaterThan(0);
              expect(array[0]).not.toBeNaN();
            });
          }
        });

        it('should display the correct percentage deltas in each row', () => {
          const priorPeriodDeltas = commonAreasSummaryWidget.getPriorPeriodDeltaColumn();
          const calculatedPriorPeriodDeltas = commonAreasSummaryWidget.calculatePriorPeriodDeltaColumn();
          let priorYearDeltas;
          let calculatedPriorYearDeltas;

          if (timePeriod !== 'year') {
            priorYearDeltas = commonAreasSummaryWidget.getPriorYearDeltaColumn();
            calculatedPriorYearDeltas = commonAreasSummaryWidget.calculatePriorYearDeltaColumn();
          }

          protractor.promise.all([calculatedPriorPeriodDeltas, priorPeriodDeltas]).then(promiseArray => {
            const calculatedPriorPeriodArray = promiseArray[0];
            const priorPeriodArray = promiseArray[1];

            calculatedPriorPeriodArray.forEach((delta, index) => {
              expect(delta).not.toBeNaN();
              expect(delta).toEqual(jasmine.any(Number));
              expect(priorPeriodArray[index]).not.toBeNaN();
              expect(priorPeriodArray[index]).toEqual(jasmine.any(Number));
              expect(delta).not.toBeLessThan(priorPeriodArray[index] - 0.2);
              expect(delta).not.toBeGreaterThan(priorPeriodArray[index] + 0.2);
            });
          });

          if (timePeriod !== 'year') {
            protractor.promise.all([calculatedPriorYearDeltas, priorYearDeltas]).then(promiseArray => {
              const calculatedPriorPeriodArray = promiseArray[0];
              const priorPeriodArray = promiseArray[1];

              calculatedPriorPeriodArray.forEach((delta, index) => {
                expect(delta).not.toBeNaN();
                expect(delta).toEqual(jasmine.any(Number));
                expect(priorPeriodArray[index]).not.toBeNaN();
                expect(priorPeriodArray[index]).toEqual(jasmine.any(Number));
                expect(delta).not.toBeLessThan(priorPeriodArray[index] - 0.2);
                expect(delta).not.toBeGreaterThan(priorPeriodArray[index] + 0.2);
              });
            });
          }
        });
      });
    });
  }
};

