'use strict';

module.exports = {
//shared tests for localization translations, using custom compare periods

  visitorBehaviorSharedLangCustomPdTests: function (locale) {

    describe('Visitor behavior tab (shared translation tests, custom periods)', function () {

      var translationsFilePath = '../../../src/l10n/languages/' + locale + '.json'; //sets up filepath for required translations file
      var translations = require(translationsFilePath);

      var sitePage = require('../pages/site-summary-page.js');
      var dateSelector = require('../pages/components/time-period-picker.js');
      var nav = require('../pages/components/nav-header.js');
      var tabSidebar = require('../pages/components/tab-nav.js');
      var orgData = require('../data/orgs.js');
      var userData = require('../data/users.js');
      var visitorTrafficWidget = require('../pages/components/visitor-behavior-traffic-widget.js');
      var visitingFrequencyWidget = require('../pages/components/visiting-frequency-widget.js');
      var gshWidget = require('../pages/components/gross-shopping-hours-widget.js');
      var dwellTimeWidget = require('../pages/components/dwell-time-widget.js');
      var opportunityWidget = require('../pages/components/opportunity-widget.js');
      var drawRateWidget = require('../pages/components/draw-rate-widget.js');
      var shoppersVsOthersWidget = require('../pages/components/shoppers-vs-others-widget.js');
      var abandonmentRateWidget = require('../pages/components/abandonment-rate-widget.js');

      it('"Visitor behavior" tab title', function () {
        var tabHeading = sitePage.siteTitle();

        expect(tabHeading.getText()).toEqual(translations.common.SITE.toUpperCase() + ' ' + translations.views.VISITORBEHAVIOR.toUpperCase());
      });

      it('test site name', function () {
        var title = nav.getSingleSiteName();

        expect(title).toEqual(orgData.SSOrg.ssOrgSiteName);
      });

      describe('"Property overall"-level tests', function () {

        describe('Visitor behavior traffic widget', function () {

          it('date range titles in chart legend', function () {
            expect(visitorTrafficWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.CUSTOMCOMPARE1.toLowerCase(), translations.common.CUSTOMCOMPARE2.toLowerCase()]);
          });

          it('date range titles in chart summary frame', function () {
            var selectedPeriodLabel = visitorTrafficWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = visitorTrafficWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = visitorTrafficWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.CUSTOMCOMPARE1.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.CUSTOMCOMPARE2.toUpperCase());
          });
        });

        describe('Visiting frequency widget', function () {

          it('date range titles in chart summary frame', function () {
            var selectedPeriodLabel = visitingFrequencyWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = visitingFrequencyWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = visitingFrequencyWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.CUSTOMCOMPARE1.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.CUSTOMCOMPARE2.toUpperCase());
          });
        });

        describe('Gross shopping hours widget', function () {

          it('date range titles in chart legend', function () {
            expect(gshWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.CUSTOMCOMPARE1.toLowerCase(), translations.common.CUSTOMCOMPARE2.toLowerCase()]);
          });

          it('date range titles in chart summary frame', function () {
            var selectedPeriodLabel = gshWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = gshWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = gshWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.CUSTOMCOMPARE1.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.CUSTOMCOMPARE2.toUpperCase());
          });
        });

        describe('Dwell time widget', function() {

          it('date range titles in chart legend', function () {
            expect(dwellTimeWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.CUSTOMCOMPARE1.toLowerCase(), translations.common.CUSTOMCOMPARE2.toLowerCase()]);
          });

          it('date range titles in chart summary frame', function () {
            var selectedPeriodLabel = dwellTimeWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = dwellTimeWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = dwellTimeWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.CUSTOMCOMPARE1.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.CUSTOMCOMPARE2.toUpperCase());
          });
        });

        describe('Opportunity widget', function() {

          it('date range titles in chart legend', function () {
            expect(opportunityWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.CUSTOMCOMPARE1.toLowerCase(), translations.common.CUSTOMCOMPARE2.toLowerCase()]);
          });

          it('date range titles in chart summary frame', function () {
            var selectedPeriodLabel = opportunityWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = opportunityWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = opportunityWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.CUSTOMCOMPARE1.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.CUSTOMCOMPARE2.toUpperCase());
          });
        });

        describe('Draw rate widget', function() {

          it('date range titles in chart legend', function () {
            expect(drawRateWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.CUSTOMCOMPARE1.toLowerCase(), translations.common.CUSTOMCOMPARE2.toLowerCase()]);
          });

          it('date range titles in chart summary frame', function () {
            var selectedPeriodLabel = drawRateWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = drawRateWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = drawRateWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.CUSTOMCOMPARE1.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.CUSTOMCOMPARE2.toUpperCase());
          });
        });

        describe('Shoppers vs others widget', function(){

          it('date range titles in chart summary frame', function () {
            var selectedPeriodLabel = shoppersVsOthersWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = shoppersVsOthersWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = shoppersVsOthersWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.CUSTOMCOMPARE1.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.CUSTOMCOMPARE2.toUpperCase());
          });
        });
      });

      describe('Area-level tests', function() {

        beforeAll(function () {
          browser.executeScript('window.scrollTo(0,0);').then(function () {
            sitePage.navToTestArea();
          });
        });

        describe('Page title and tab sidebar checks', function () {

          it('"Visitor behavior" tab title', function () {
            var tabHeading = sitePage.siteTitle();

            expect(tabHeading.getText()).toEqual(translations.common.AREA.toUpperCase() + ' ' + translations.views.VISITORBEHAVIOR.toUpperCase());
          });

          it('test area name', function () {
            var areaName = sitePage.getAreaName();

            expect(areaName).toMatch(orgData.SSOrg.testArea);
          });
        });

        describe('Zone-level abandonment rate widget', function() {

          it('date range titles in chart legend', function () {
            expect(abandonmentRateWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.CUSTOMCOMPARE1.toLowerCase(), translations.common.CUSTOMCOMPARE2.toLowerCase()]);
          });

          it('date range titles in chart summary frame', function () {
            var selectedPeriodLabel = abandonmentRateWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = abandonmentRateWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = abandonmentRateWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.CUSTOMCOMPARE1.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.CUSTOMCOMPARE2.toUpperCase());
          });
        });
      });
    });
  }
};
