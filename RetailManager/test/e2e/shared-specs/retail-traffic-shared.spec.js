
const sitePage = require('../pages/site-summary-page.js');
const dateSelector = require('../pages/components/time-period-picker.js');
const nav = require('../pages/components/nav-header.js');
const orgData = require('../data/orgs.js');
const userData = require('../data/users.js');
const lineChart = require('../pages/components/common/line-chart-widget.js');
const lineHighChart = require('../pages/components/common/line-chart-widget.js');
const trafficWidget = require('../pages/components/site-traffic-widget.js');
const sitePerformanceWidget = require('../pages/components/organization-performance-widget.js');
const dailyPerformanceWidget = require('../pages/components/daily-performance-widget.js');
const powerHoursWidget = require('../pages/components/power-hours-widget.js');
const dailyMetricBarWidget = require('../pages/components/daily-metric-averages-widget.js');

module.exports = {

  retailTrafficSharedTests(timePeriod) {
    describe('Retail traffic tab (shared tests)', () => {
      const dataWindow = lineChart.getSumDataWindow(timePeriod); // used for line chart widget sum checks
      const dataWindow2 = lineHighChart.getSumDataWindow(timePeriod); // used for line chart widget sum checks

      let kpiSummaryValues = { // to be used for checking 5-kpi metric widget values vs metric line chart widget summary values
        traffic: {},
        sales: {},
        conversion: {},
        ats: {},
        star: {}
      };

      it('should navigate to the correct site', () => {
        const title = nav.getSiteName();
        expect(title).toEqual(orgData.MSRetailSite.testSiteName);
      });

      it('should nav to "Traffic" tab when clicked', () => {
        const tabHeading = sitePage.siteTitle();
        expect(tabHeading.getText()).toEqual('SITE TRAFFIC');
      });

      it('date picker should appear', () => {
        const datePicker = dateSelector.getDatePicker();
        expect(datePicker.isPresent()).toBe(true);
      });

      it('should have selected the correct date button', () => {
        let dateButton;

        if (timePeriod === 'week') {
          dateButton = dateSelector.getWeekButton();
        } else if (timePeriod === 'month') {
          dateButton = dateSelector.getMonthButton();
        } else if (timePeriod === 'year') {
          dateButton = dateSelector.getYearButton();
        }
        dateButton.then(button => {
          expect(button.getAttribute('class')).toMatch('active');
        });
      });

      describe('Metric line chart widget', () => {
        describe('weather feature tests', () => {
          const temp = trafficWidget.getWeatherMetrics()[0];
          const highTemp = trafficWidget.getWeatherMetrics()[1];
          const windSpeed = trafficWidget.getWeatherMetrics()[6];
          const humidity = trafficWidget.getWeatherMetrics()[4];
          const degreesF = trafficWidget.getWeatherUnits()[0];
          const degreesC = trafficWidget.getWeatherUnits()[1];
          const mph = trafficWidget.getWeatherUnits()[2];
          const kph = trafficWidget.getWeatherUnits()[3];

          it('chart should show weather metric dropdown', () => {
            expect(trafficWidget.getWeatherDropdown().isPresent()).toBe(true);
          });

          it('by default, temperature metric is selected and appears on chart/in legend', () => {
            if (trafficWidget.getWidgetWeatherPermission()) {
              expect(trafficWidget.getActiveWeatherMetrics()).toMatch(temp);
              expect(trafficWidget.getWeatherMetricLine(3).isPresent()).toBe(true);
              expect(trafficWidget.getLegendTextLowerCase()).toMatch(temp);
              if (timePeriod !== 'week') {
                // removing weekly tooltip checks until SA-2099 is complete
                const tooltipText = trafficWidget.getTooltipText();
                expect(tooltipText).toMatch(temp);
                expect(tooltipText).toMatch(degreesC);
              }
            }
          });

          it('should show all expected weather metric names in dropdown', () => {
            trafficWidget.getWeatherDropdown().click();
            const weatherMetricOptions = trafficWidget.getWeatherDropdown().getTextLowerCase();
            trafficWidget.getWeatherMetrics().forEach(metric => {
              expect(weatherMetricOptions).toMatch(metric);
            });
            trafficWidget.getWeatherDropdown().click();
          });

          it('should correctly add and remove other weather metrics from the chart', () => {
            trafficWidget.setWeatherMetric(temp);
            expect(trafficWidget.getActiveWeatherMetrics()).not.toMatch(temp);
            expect(trafficWidget.getWeatherMetricLine(3).isPresent()).not.toBe(true);
            expect(trafficWidget.getLegendTextLowerCase()).not.toMatch(temp);
            if (timePeriod !== 'week') {
              let tooltipText = trafficWidget.getTooltipText();
              expect(tooltipText).not.toMatch(temp);
              expect(tooltipText).not.toMatch(degreesC);
              expect(tooltipText).not.toMatch(kph);
            }

            trafficWidget.setWeatherMetric(highTemp);

            expect(trafficWidget.getActiveWeatherMetrics()).toMatch(highTemp);
            expect(trafficWidget.getWeatherMetricLine(3).isPresent()).toBe(true);
            expect(trafficWidget.getLegendTextLowerCase()).toMatch(highTemp);
            if (timePeriod !== 'week') {
              tooltipText = trafficWidget.getTooltipText();
              expect(tooltipText).toMatch(highTemp);
              expect(tooltipText).toMatch(degreesC);
              expect(tooltipText).not.toMatch(degreesF);
            }

            trafficWidget.setWeatherMetric(windSpeed);

            expect(trafficWidget.getActiveWeatherMetrics()).toMatch(windSpeed);
            expect(trafficWidget.getWeatherMetricLine(4).isPresent()).toBe(true);
            expect(trafficWidget.getLegendTextLowerCase()).toMatch(windSpeed);
            if (timePeriod !== 'week') {
              tooltipText = trafficWidget.getTooltipText();
              expect(tooltipText).toMatch(windSpeed);
              expect(tooltipText).toMatch(kph);
              expect(tooltipText).not.toMatch(mph);
            }

            trafficWidget.setWeatherMetric(humidity);

            expect(trafficWidget.getActiveWeatherMetrics()).toMatch(humidity);
            expect(trafficWidget.getWeatherMetricLine(5).isPresent()).toBe(true);
            expect(trafficWidget.getLegendTextLowerCase()).toMatch(humidity);
            if (timePeriod !== 'week') {
              tooltipText = trafficWidget.getTooltipText();
              expect(tooltipText).toMatch(humidity);
            }
          });

          it('should disallow selection of greater than three weather metrics', () => {
            trafficWidget.getWeatherDropdown().click();
            const allDisplayedOptions = trafficWidget.getAllDisplayedWeatherOptions();
            const disabledOptions = allDisplayedOptions.filter(option => {
              return option.getAttribute('disabled').then(disabledAttr => {
                return disabledAttr === 'true';
              });
            });
            expect(allDisplayedOptions.count()).toEqual(7);
            expect(disabledOptions.count()).toEqual(4);
          });
        });

        describe('chart data tests', () => {
          // loop through available metrics in line chart widget dropdown
          ['Traffic', 'Sales', 'Conversion', 'ATS', 'STAR'].forEach(metric => {
            const metricKey = metric.toLowerCase(); // key for kpiSummaryValues

            describe(`( ${metric} metric):`, () => {
              beforeAll(() => {
                trafficWidget.selectMetric(metric);
              });

              it(`should display the title "${metric}"`, () => {
                const widgetTitle = trafficWidget.widgetTitle();
                expect(widgetTitle.getText()).toEqual(metric);
              });

              // checks that x-axis values fit within selected date range
              it('widget chart should display correct date range on x-axis', () => {
                const reportPeriod = dateSelector.getReportingPeriod(userData.superUser.dateFormat);
                const allXAxisDates = trafficWidget.getXAxisDates(userData.superUser.dateFormat);

                protractor.promise.all([reportPeriod, allXAxisDates]).then(promiseArray => {
                  const reportArray = promiseArray[0];
                  const chartDateArray = promiseArray[1];
                  // first point on x-axis should match start date of time period
                  expect(chartDateArray[0]).toEqual(reportArray[0]);
                  chartDateArray.forEach(dateItem => {
                    expect(dateItem).not.toBeLessThan(reportArray[0]);
                    expect(dateItem).not.toBeGreaterThan(reportArray[1]);
                  });
                });
              });

              it('widget chart should display correct range on y-axis', () => {
                const selectedPeriodDataValues = trafficWidget.getSelectedPeriodDataValues();
                const priorPeriodDataValues = trafficWidget.getPriorPeriodDataValues();
                const highestYAxisValue = trafficWidget.getHighestYAxisValue();

                expect(highestYAxisValue).not.toBeNaN();
                expect(highestYAxisValue).toEqual(jasmine.any(Number));

                selectedPeriodDataValues.then(dataArray => {
                  expect(dataArray.length).toBeGreaterThan(0);
                  dataArray.forEach(dataPoint => {
                    expect(dataPoint).not.toBeLessThan(0);
                    expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                  });
                });

                priorPeriodDataValues.then(dataArray => {
                  expect(dataArray.length).toBeGreaterThan(0);
                  dataArray.forEach(dataPoint => {
                    expect(dataPoint).not.toBeLessThan(0);
                    expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                  });
                });

                if (timePeriod !== 'year') {
                  const priorYearDataValues = trafficWidget.getPriorYearDataValues();
                  priorYearDataValues.then(dataArray => {
                    expect(dataArray.length).toBeGreaterThan(0);
                    dataArray.forEach(dataPoint => {
                      expect(dataPoint).not.toBeLessThan(0);
                      expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                    });
                  });
                }
              });

              it('correct date range should appear in "overall visitors" frame', () => {
                const dateFormat = userData.superUser.dateFormat;

                const reportPeriod = dateSelector.getReportingPeriod(dateFormat);
                const priorReportPeriod = dateSelector.getPriorReportingPeriod(dateFormat);

                const selectedPeriodDate = trafficWidget.getSelectedPeriodDateRange(dateFormat);
                const priorPeriodDate = trafficWidget.getPriorPeriodDateRange(dateFormat);

                expect(selectedPeriodDate).toEqual(reportPeriod);
                expect(priorPeriodDate).toEqual(priorReportPeriod);

                if (timePeriod !== 'year') {
                  const priorYearReportingPeriod = dateSelector.getPriorYearReportingPeriod(timePeriod, dateFormat);
                  const priorYearDate = trafficWidget.getPriorYearDateRange(dateFormat);

                  expect(priorYearDate).toEqual(priorYearReportingPeriod);
                }
              });

              it('correct percentage deltas should appear in "overall visitors" frame', () => {
                const priorPeriodDelta = trafficWidget.getPriorPeriodDelta();
                const calculatedPriorPeriodDelta = trafficWidget.calculatePriorPeriodDelta();
                const deltaDataWindow = 0.2;

                kpiSummaryValues[metricKey].priorPeriodDelta = priorPeriodDelta;

                if (metric !== 'STAR') {
                  expect(calculatedPriorPeriodDelta).not.toBeLessThan(priorPeriodDelta - deltaDataWindow);
                  expect(calculatedPriorPeriodDelta).not.toBeGreaterThan(priorPeriodDelta + deltaDataWindow);
                }

                if (timePeriod !== 'year') {
                  const priorYearDelta = trafficWidget.getPriorYearDelta();
                  kpiSummaryValues[metricKey].priorYearDelta = priorYearDelta;
                  if (metric !== 'STAR') {
                    const calculatedPriorYearDelta = trafficWidget.calculatePriorYearDelta();
                    expect(calculatedPriorYearDelta).not.toBeLessThan(priorYearDelta - deltaDataWindow);
                    expect(calculatedPriorYearDelta).not.toBeGreaterThan(priorYearDelta + deltaDataWindow);
                  }
                }
              });

              it('sum of a line\'s data points should equal the corresponding total displayed in "overall visitors" frame', () => {
                const selectedPeriodDataSum = trafficWidget.getSelectedPeriodDataSum();
                const priorPeriodDataSum = trafficWidget.getPriorPeriodDataSum();

                const selectedPeriodVisitors = trafficWidget.getSelectedPeriodOverall();
                const priorPeriodVisitors = trafficWidget.getPriorPeriodOverall();

                kpiSummaryValues[metricKey].selectedPeriod = selectedPeriodVisitors;
                kpiSummaryValues[metricKey].priorPeriod = priorPeriodVisitors;

                if (timePeriod !== 'year') {
                  kpiSummaryValues[metricKey].priorYear = trafficWidget.getPriorYearOverall();
                }

                if (metric === 'Traffic' || metric === 'Sales') {
                  expect(selectedPeriodDataSum).not.toBeGreaterThan(selectedPeriodVisitors + dataWindow);
                  expect(selectedPeriodDataSum).not.toBeLessThan(selectedPeriodVisitors - dataWindow);

                  expect(priorPeriodDataSum).not.toBeGreaterThan(priorPeriodVisitors + dataWindow);
                  expect(priorPeriodDataSum).not.toBeLessThan(priorPeriodVisitors - dataWindow);

                  if (timePeriod !== 'year') {
                    const priorYearDataSum = trafficWidget.getPriorYearDataSum();
                    const priorYearVisitors = kpiSummaryValues[metricKey].priorYear;
                    expect(priorYearDataSum).not.toBeGreaterThan(priorYearVisitors + dataWindow);
                    expect(priorYearDataSum).not.toBeLessThan(priorYearVisitors - dataWindow);
                  }
                }
              });
            });
          });
        });
      });

      describe('5-kpi site performance widget', () => {
        it('should display the title "Site performance"', () => {
          const widgetTitle = sitePerformanceWidget.widgetTitle();
          expect(widgetTitle.getText()).toEqual('Site performance');
        });

        ['Traffic', 'Sales', 'Conversion', 'ATS', 'STAR'].forEach(metric => {
          const metricKey = metric.toLowerCase(); // used for pushing values to kpiSummaryValues

          it(`should show the same ${metric} data as the metric line chart widget`, () => {
            const selectedPeriod = sitePerformanceWidget.getSelectedPeriodValue(metric);
            const priorPeriod = sitePerformanceWidget.getPriorPeriodValue(metric);
            const priorPeriodDelta = sitePerformanceWidget.getPriorPeriodDelta(metric);

            expect(selectedPeriod).toEqual(kpiSummaryValues[metricKey].selectedPeriod);
            expect(priorPeriod).toEqual(kpiSummaryValues[metricKey].priorPeriod);
            expect(priorPeriodDelta).toEqual(kpiSummaryValues[metricKey].priorPeriodDelta);

            if (timePeriod !== 'year') {
              const priorYear = sitePerformanceWidget.getPriorYearValue(metric);
              const priorYearDelta = sitePerformanceWidget.getPriorYearDelta(metric);

              expect(priorYear).toEqual(kpiSummaryValues[metricKey].priorYear);
              expect(priorYearDelta).toEqual(kpiSummaryValues[metricKey].priorYearDelta);
            }
          });
        });
      });

      describe('Power hours widget:', () => {
        it('should display the title "Power hours"', () => {
          const widgetTitle = powerHoursWidget.widgetTitle();
          expect(widgetTitle.getText()).toEqual('Power hours');
        });

        ['Average Traffic', 'Traffic%', 'Average Sales', 'Conversion', 'ATS'].forEach(metric => {
          describe(`${metric} metric`, () => {
            beforeAll(() => {
              powerHoursWidget.selectMetricDropdown(metric);
            });

            it('should display 7 day columns and right-most/left-most columns', () => {
              expect(powerHoursWidget.getDayHeaders()).toEqual(powerHoursWidget.dayHeaders);
              expect(powerHoursWidget.getTotalHeader().isPresent()).toBe(true);
              expect(powerHoursWidget.getBlankHeader().isPresent()).toBe(true);
            });

            it('should display 24 hour rows and 1 total row', () => {
              expect(powerHoursWidget.getRowHeaders()).toEqual(orgData.MSRetailSite.powerHoursRows);
              expect(powerHoursWidget.getTotalRow().isPresent()).toBe(true);
            });
          });
        });

        describe('widget tests using traffic percentage data', () => {
          beforeAll(() => {
            powerHoursWidget.selectMetricDropdown('Traffic%');
          });

          it('should display percentage data', () => {
            expect(powerHoursWidget.getTotalRow().getText()).toMatch(/%/);
          });

          it('"Daily total" row should sum to ~100', () => {
            const rowSum = powerHoursWidget.getTotalRowSum();
            const weeklyTotalTraffic = powerHoursWidget.getWeeklyTotalTraffic();

            expect(rowSum).not.toBeNaN();
            expect(rowSum).toEqual((jasmine.any(Number)));
            expect(rowSum).not.toBeGreaterThan(weeklyTotalTraffic + 5);
            expect(rowSum).not.toBeLessThan(weeklyTotalTraffic - 5);
          });

          it('each row should sum to the total shown in the right-hand "Total" column', () => {
            function checkRow(powerHoursWidget, totalColumn, index) {
              powerHoursWidget.getHourRowSum(index).then(rowSum => {
                expect(totalColumn[index]).not.toBeNaN();
                expect(totalColumn[index]).toEqual((jasmine.any(Number)));
                expect(totalColumn[index]).not.toBeGreaterThan(rowSum + 1);
                expect(totalColumn[index]).not.toBeLessThan(rowSum - 1);
              });
            }

            powerHoursWidget.getTotalColumnArray().then(totalColumn => {
              let i = 0;
              do {
                checkRow(powerHoursWidget, totalColumn, i);
                i += 1;
              } while (i <= 23);
            });
          });

          it('"Total" column should sum to ~100', () => {
            const weeklyTotalTraffic = powerHoursWidget.getWeeklyTotalTraffic();
            const columnSum = powerHoursWidget.getTotalColumnSum();

            expect(columnSum).not.toBeNaN();
            expect(columnSum).not.toBeGreaterThan(weeklyTotalTraffic + 5);
            expect(columnSum).not.toBeLessThan(weeklyTotalTraffic - 5);
          });
        });

        ['Average Traffic', 'Average Sales'].forEach(metric => {
          describe(`widget tests using ${metric} data.`, () => {
            beforeAll(() => {
              powerHoursWidget.selectMetricDropdown(metric);
            });

            it(`should display ${metric} data`, () => {
              expect(powerHoursWidget.getTotalRow().getText()).not.toMatch(/%/);
            });

            it('"Daily total" row should sum to the total shown in the right-hand "Total" column', () => {
              const rowSum = powerHoursWidget.getTotalRowSum();
              const weeklyTotalTraffic = powerHoursWidget.getWeeklyTotalTraffic();

              expect(rowSum).not.toBeNaN();
              expect(rowSum).toEqual((jasmine.any(Number)));
              expect(rowSum).not.toBeGreaterThan(weeklyTotalTraffic + 5);
              expect(rowSum).not.toBeLessThan(weeklyTotalTraffic - 5);
            });

            it('each row should sum to the total shown in the right-hand "Total" column', () => {
              function checkRow(powerHoursWidget, totalColumn, index) {
                powerHoursWidget.getHourRowSum(index).then(rowSum => {
                  expect(totalColumn[index]).not.toBeNaN();
                  expect(totalColumn[index]).toEqual(jasmine.any(Number));
                  expect(totalColumn[index]).not.toBeGreaterThan(rowSum + 3);
                  expect(totalColumn[index]).not.toBeLessThan(rowSum - 3);
                });
              }

              powerHoursWidget.getTotalColumnArray().then(totalColumn => {
                let i = 0;
                do {
                  checkRow(powerHoursWidget, totalColumn, i);
                  i += 1;
                } while (i <= 23);
              });
            });

            it('"Total" column should sum to correct value shown in "Daily total" row', () => {
              const weeklyTotalTraffic = powerHoursWidget.getWeeklyTotalTraffic();
              const columnSum = powerHoursWidget.getTotalColumnSum();

              expect(columnSum).not.toBeNaN();
              expect(columnSum).not.toBeGreaterThan(weeklyTotalTraffic + 3);
              expect(columnSum).not.toBeLessThan(weeklyTotalTraffic - 3);
            });
          });
        });
      });

      describe('Daily performance indicators widget', () => {
        it('should display the title "Daily performance indicators"', () => {
          const widgetTitle = dailyPerformanceWidget.widgetTitle();
          expect(widgetTitle.getText()).toEqual('Daily performance indicators');
        });

        it('chart y-axes should be scaled correctly to the data', () => {
          dailyPerformanceWidget.getExpandTableButton().click();

          const leftChartHighestYAxisValue = dailyPerformanceWidget.getHighestYAxisValue('left');
          const rightChartHighestLeftYAxisValue = dailyPerformanceWidget.getHighestYAxisValue('right', 'left');
          const rightChartHighestRightYAxisValue = dailyPerformanceWidget.getHighestYAxisValue('right', 'right');

          const salesDeltaValues = dailyPerformanceWidget.getColumnDataAsNumber('sales-delta');
          const trafficDeltaValues = dailyPerformanceWidget.getColumnDataAsNumber('traffic-delta');
          const laborDeltaValues = dailyPerformanceWidget.getColumnDataAsNumber('labor-delta');
          const transactionsDeltaValues = dailyPerformanceWidget.getColumnDataAsNumber('transactions-delta');
          const conversionValues = dailyPerformanceWidget.getColumnDataAsNumber('conversion');
          const starValues = dailyPerformanceWidget.getColumnDataAsNumber('star');

          protractor.promise.all([salesDeltaValues, trafficDeltaValues, laborDeltaValues, transactionsDeltaValues])
            .then(columnValuesArrays => {
              expect(leftChartHighestYAxisValue).not.toBeNaN();
              expect(leftChartHighestYAxisValue).toEqual(jasmine.any(Number));

              columnValuesArrays.forEach(dataArray => {
                expect(dataArray.length).toBeGreaterThan(0);

                dataArray.forEach(dataPoint => {
                  expect(dataPoint).not.toBeLessThan(0);
                  expect(dataPoint).not.toBeGreaterThan(leftChartHighestYAxisValue);
                });
              });
            });

          conversionValues.then(dataArray => {
            expect(rightChartHighestLeftYAxisValue).not.toBeNaN();
            expect(rightChartHighestLeftYAxisValue).toEqual(jasmine.any(Number));
            expect(dataArray.length).toBeGreaterThan(0);

            dataArray.forEach(dataPoint => {
              expect(dataPoint).not.toBeLessThan(0);
              expect(dataPoint).not.toBeGreaterThan(rightChartHighestLeftYAxisValue);
            });
          });

          starValues.then(dataArray => {
            expect(rightChartHighestRightYAxisValue).not.toBeNaN();
            expect(rightChartHighestRightYAxisValue).toEqual(jasmine.any(Number));
            expect(dataArray.length).toBeGreaterThan(0);

            dataArray.forEach(dataPoint => {
              expect(dataPoint).not.toBeLessThan(0);
              expect(dataPoint).not.toBeGreaterThan(rightChartHighestRightYAxisValue);
            });
          });

          dailyPerformanceWidget.getExpandTableButton().click();
        });

        it('table should show/hide when "show table" button is clicked', () => {
          expect(dailyPerformanceWidget.isTableDisplayed()).toEqual(false);
          dailyPerformanceWidget.getExpandTableButton().click();
          expect(dailyPerformanceWidget.isTableDisplayed()).toEqual(true);
          dailyPerformanceWidget.getExpandTableButton().click();
          expect(dailyPerformanceWidget.isTableDisplayed()).toEqual(false);
        });

        it('each table column should be sortable, ascending and descending', () => {
          dailyPerformanceWidget.getExpandTableButton().click();
          let unsortedColumn;
          let sortedColumn;

          dailyPerformanceWidget.getTableHeaders().forEach(metric => {
            dailyPerformanceWidget.sortTableBy(metric);
            unsortedColumn = dailyPerformanceWidget.getColumnData(metric);
            dailyPerformanceWidget.sortTableBy(metric);
            sortedColumn = dailyPerformanceWidget.getColumnData(metric);
            expect(unsortedColumn).not.toEqual(sortedColumn);
          });

          dailyPerformanceWidget.sortTableBy('day');
        });

        describe('charts and table update after selecting days', () => {
          const leftChartMetrics = ['sales', 'traffic', 'labor', 'transactions'];
          const rightChartMetrics = ['conversion', 'star'];

          describe('all days (default selection)', () => {
            const dayLabels = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

            it('should show all chart series and each series should have 7 points', () => {
              leftChartMetrics.forEach(metric => {
                expect(dailyPerformanceWidget.getChartLineLength('left', metric)).toEqual(7);
              });

              rightChartMetrics.forEach(metric => {
                expect(dailyPerformanceWidget.getChartLineLength('right', metric)).toEqual(7);
              });
            });

            it('chart x-axes should show all 7 days', () => {
              expect(dailyPerformanceWidget.getChartXAxisLabels('left')).toEqual(dayLabels);
              expect(dailyPerformanceWidget.getChartXAxisLabels('right')).toEqual(dayLabels);
            });

            it('should show 7 days in the table', () => {
              expect(dailyPerformanceWidget.getTableDayLabels()).toEqual(dayLabels);
            });
          });

          describe('weekends', () => {
            const dayLabels = ['Sunday', 'Saturday'];

            beforeAll(() => {
              dailyPerformanceWidget.openDaySelectorDropdown();
              dailyPerformanceWidget.selectDay('weekend');
              dailyPerformanceWidget.openDaySelectorDropdown();
            });

            it('should show all chart series and each series should have 2 points', () => {
              leftChartMetrics.forEach(metric => {
                expect(dailyPerformanceWidget.getChartLineLength('left', metric)).toEqual(2);
              });

              rightChartMetrics.forEach(metric => {
                expect(dailyPerformanceWidget.getChartLineLength('right', metric)).toEqual(2);
              });
            });

            it('chart x-axes should show both days', () => {
              expect(dailyPerformanceWidget.getChartXAxisLabels('left')).toEqual(dayLabels);
              expect(dailyPerformanceWidget.getChartXAxisLabels('right')).toEqual(dayLabels);
            });

            it('should show 2 days in the table', () => {
              expect(dailyPerformanceWidget.getTableDayLabels()).toEqual(dayLabels);
            });
          });

          describe('Monday, Wednesday, and Friday', () => {
            const dayLabels = ['Monday', 'Wednesday', 'Friday'];

            beforeAll(() => {
              dailyPerformanceWidget.openDaySelectorDropdown();
              dailyPerformanceWidget.selectDay('monday');
              dailyPerformanceWidget.selectDay('wednesday');
              dailyPerformanceWidget.selectDay('friday');
              // deselecting previous option
              dailyPerformanceWidget.selectDay('weekend');
              dailyPerformanceWidget.openDaySelectorDropdown();
            });

            it('should show all chart series and each series should have 3 points', () => {
              leftChartMetrics.forEach(metric => {
                expect(dailyPerformanceWidget.getChartLineLength('left', metric)).toEqual(3);
              });

              rightChartMetrics.forEach(metric => {
                expect(dailyPerformanceWidget.getChartLineLength('right', metric)).toEqual(3);
              });
            });

            it('chart x-axes should show all 3 days', () => {
              expect(dailyPerformanceWidget.getChartXAxisLabels('left')).toEqual(dayLabels);
              expect(dailyPerformanceWidget.getChartXAxisLabels('right')).toEqual(dayLabels);
            });

            it('should show 3 days in the table', () => {
              expect(dailyPerformanceWidget.getTableDayLabels()).toEqual(dayLabels);
            });
          });
        });
      });

      describe('Daily average metric widget', () => {
        // loop through available metrics in bar chart widget dropdown
        ['Sales', 'Traffic', 'Conversion', 'ATS', 'STAR'].forEach(metric => {
          describe(`${metric} metric'`, () => {
            beforeAll(() => {
              dailyMetricBarWidget.selectMetric(metric);
            });

            it(`should display the title "Daily averages: ${metric}"`, () => {
              const widgetTitle = dailyMetricBarWidget.widgetTitle();

              expect(widgetTitle.getText()).toEqual(`Daily averages: ${metric}`);
            });

            it('widget chart should have data for each day and should display correct range on y-axis', () => {
              const highestYAxisValue = dailyMetricBarWidget.getHighestYAxisValue();
              const selectedPeriodDataValues = dailyMetricBarWidget.getSelectedPeriodDataValues();
              const priorPeriodDataValues = dailyMetricBarWidget.getPriorPeriodDataValues();

              expect(highestYAxisValue).not.toBeNaN();
              expect(highestYAxisValue).toEqual(jasmine.any(Number));

              selectedPeriodDataValues.then(dataArray => {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(dataPoint => {
                  expect(dataPoint).not.toBe(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });

              priorPeriodDataValues.then(dataArray => {
                expect(dataArray.length).toBeGreaterThan(0);
                dataArray.forEach(dataPoint => {
                  expect(dataPoint).not.toBe(0);
                  expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                });
              });

              if (timePeriod !== 'year') {
                const priorYearDataValues = dailyMetricBarWidget.getPriorYearDataValues();

                priorYearDataValues.then(dataArray => {
                  expect(dataArray.length).toBeGreaterThan(0);
                  dataArray.forEach(dataPoint => {
                    expect(dataPoint).not.toBe(0);
                    expect(dataPoint).not.toBeGreaterThan(highestYAxisValue);
                  });
                });
              }
            });
          });
        });

        describe('charts and table update after selecting days', () => {
          beforeAll(() => {
            dailyMetricBarWidget.getExpandTableButton().click();
          });

          ['Sales', 'Traffic', 'Conversion', 'ATS', 'STAR'].forEach(metric => {
            describe(`${metric} metric`, () => {
              beforeAll(() => {
                dailyMetricBarWidget.selectMetric(metric);
              });

              describe('all days (default selection)', () => {
                const dayLabels = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

                // 7 day data integrity checks handled above
                it('chart x-axis should show all 7 days', () => {
                  expect(dailyMetricBarWidget.getXAxisLabels()).toEqual(dayLabels);
                });

                it('should show 7 days in the table', () => {
                  expect(dailyMetricBarWidget.getTableDayLabels()).toEqual(dayLabels);
                });
              });

              describe('weekends', () => {
                const dayLabels = ['Sunday', 'Saturday'];

                beforeAll(() => {
                  dailyMetricBarWidget.openDaySelectorDropdown();
                  dailyMetricBarWidget.selectDay('weekend');
                  dailyMetricBarWidget.openDaySelectorDropdown();
                });

                it('should show 2 days of data', () => {
                  const selectedPeriodDataValues = dailyMetricBarWidget.getSelectedPeriodDataValues();
                  const priorPeriodDataValues = dailyMetricBarWidget.getPriorPeriodDataValues();

                  selectedPeriodDataValues.then(dataArray => {
                    expect(dataArray.length).toEqual(2);
                  });

                  priorPeriodDataValues.then(dataArray => {
                    expect(dataArray.length).toEqual(2);
                  });

                  if (timePeriod !== 'year') {
                    const priorYearDataValues = dailyMetricBarWidget.getPriorYearDataValues();

                    priorYearDataValues.then(dataArray => {
                      expect(dataArray.length).toEqual(2);
                    });
                  }
                });

                it('chart x-axis should show both days', () => {
                  expect(dailyMetricBarWidget.getXAxisLabels()).toEqual(dayLabels);
                });

                it('should show 2 days in the table', () => {
                  expect(dailyMetricBarWidget.getTableDayLabels()).toEqual(dayLabels);
                });
              });

              describe('Monday, Wednesday, and Friday', () => {
                const dayLabels = ['Monday', 'Wednesday', 'Friday'];

                // todo: modify teardown after sa-610 is fixed
                beforeAll(() => {
                  dailyMetricBarWidget.openDaySelectorDropdown();
                  dailyMetricBarWidget.selectDay('monday');
                  dailyMetricBarWidget.selectDay('wednesday');
                  dailyMetricBarWidget.selectDay('friday');
                  // deselecting previous option
                  dailyMetricBarWidget.selectDay('weekend');
                  dailyMetricBarWidget.openDaySelectorDropdown();
                });

                it('should show 3 days of data', () => {
                  const selectedPeriodDataValues = dailyMetricBarWidget.getSelectedPeriodDataValues();
                  const priorPeriodDataValues = dailyMetricBarWidget.getPriorPeriodDataValues();

                  selectedPeriodDataValues.then(dataArray => {
                    expect(dataArray.length).toEqual(3);
                  });

                  priorPeriodDataValues.then(dataArray => {
                    expect(dataArray.length).toEqual(3);
                  });

                  if (timePeriod !== 'year') {
                    const priorYearDataValues = dailyMetricBarWidget.getPriorYearDataValues();

                    priorYearDataValues.then(dataArray => {
                      expect(dataArray.length).toEqual(3);
                    });
                  }
                });

                it('chart x-axis should show 3 days', () => {;
                  expect(dailyMetricBarWidget.getXAxisLabels()).toEqual(dayLabels);
                });

                it('should show 3 days in the table', () => {
                  expect(dailyMetricBarWidget.getTableDayLabels()).toEqual(dayLabels);
                });

                // todo: modify teardown after sa-610 is fixed
                afterAll(() => {
                  dailyMetricBarWidget.openDaySelectorDropdown();
                  // SA-1888: Selecting the all option removes the other selections automatically
                  dailyMetricBarWidget.selectDay('all');
                  dailyMetricBarWidget.openDaySelectorDropdown();
                });
              });
            });
          });

          afterAll(() => {
            dailyMetricBarWidget.getExpandTableButton().click();
          });
        });

        it('table should show/hide when "show table" button is clicked', () => {
          expect(dailyMetricBarWidget.isTableDisplayed()).toEqual(false);
          dailyMetricBarWidget.getExpandTableButton().click();
          expect(dailyMetricBarWidget.isTableDisplayed()).toEqual(true);
        });

        it('each table column should be sortable, ascending and descending', () => {
          ['sales-delta', 'sales', 'traffic', 'traffic-delta', 'conversion', 'ATS', 'ATS-delta', 'STAR', 'day']
            .forEach(metric => {
              dailyMetricBarWidget.sortTableBy(metric);
              const sortedColumn = dailyMetricBarWidget.getColumnData(metric);
              dailyMetricBarWidget.sortTableBy(metric);
              const reverseSortedColumn = dailyMetricBarWidget.getColumnData(metric);
              expect(sortedColumn).not.toEqual(reverseSortedColumn);
            });
        });
      });
    });
  }
};
