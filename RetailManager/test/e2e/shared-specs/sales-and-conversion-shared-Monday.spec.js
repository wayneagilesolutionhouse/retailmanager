'use strict';

module.exports = {


  salesAndConversionSharedTestsMonday: function (timePeriod) {

    describe('Sales and Conversion tab (shared tests)', function () {

      var sitePage = require('../pages/site-summary-page.js');
      var dateSelector = require('../pages/components/time-period-picker.js');
      var nav = require('../pages/components/nav-header.js');
      var orgData = require('../data/orgs.js');
      var userData = require('../data/users.js');
      var zoneSalesWidget = require('../pages/components/zone-sales-widget.js');
      var zoneConversionWidget = require('../pages/components/zone-conversion-widget.js');
      var averagePurchaseWidget = require('../pages/components/average-purchase-widget.js');


      it('date picker should appear', function () {
        var datePicker = dateSelector.getDatePicker();

        expect(datePicker.isPresent()).toBe(true);
      });

      describe('"Property overall"-level tests', function () {

        it('should have selected the correct date button', function () {
          var dateButton;

          if(timePeriod === 'week') {
            dateButton = dateSelector.getWeekButton();
          }
          else if(timePeriod === 'month') {
            dateButton = dateSelector.getMonthButton();
          }
          else if(timePeriod === 'year') {
            dateButton = dateSelector.getYearButton();
          }
          dateButton.then(function (button) {
            expect(button.getAttribute('class')).toMatch('active')
          });
        });
      });

      describe('Zone-level tests', function () {

        beforeAll(function () {
          browser.executeScript('window.scrollTo(0,0);').then(function () {
            sitePage.navToZone()
          });
        });

        it('should have selected the correct date button', function () {
          var dateButton;

          if(timePeriod === 'week') {
            dateButton = dateSelector.getWeekButton();
          }
          else if(timePeriod === 'month') {
            dateButton = dateSelector.getMonthButton();
          }
          else if(timePeriod === 'year') {
            dateButton = dateSelector.getYearButton();
          }
          dateButton.then(function (button) {
            expect(button.getAttribute('class')).toMatch('active')
          });
        });

        describe('Zone-level sales widget', function () {

          it('widget chart should display correct date range on x-axis', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var allXAxisDates = zoneSalesWidget.getXAxisDates(userData.mondayUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
              var reportArray = promiseArray[0];
              var chartDateArray = promiseArray[1];
              //first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(function (dateItem) {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var comparePeriod1 = dateSelector.getComparePeriod(userData.mondayUser.comparePd1WeeksBack, userData.mondayUser.dateFormat);

            var selectedPeriodDate = zoneSalesWidget.getSelectedPeriodDateRange(userData.mondayUser.dateFormat);
            var priorPeriodDate = zoneSalesWidget.getPriorPeriodDateRange(userData.mondayUser.dateFormat);

            var comparePeriod2 = dateSelector.getComparePeriod(userData.mondayUser.comparePd2WeeksBack, userData.mondayUser.dateFormat);
            var priorPeriod2Date = zoneSalesWidget.getPriorYearDateRange(userData.mondayUser.dateFormat);


            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(comparePeriod1);
            expect(priorPeriod2Date).toEqual(comparePeriod2);
          });
        });


        describe('Zone-level conversion widget', function () {

          //todo: (may fail until SA-489 is resolved)
          it('(may fail until SA-489 is resolved) widget chart should display correct date range on x-axis', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var allXAxisDates = zoneConversionWidget.getXAxisDates(userData.mondayUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
              var reportArray = promiseArray[0];
              var chartDateArray = promiseArray[1];
              //first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(function (dateItem) {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          //todo: (may fail until SA-489 is resolved)
          it('(may fail until SA-489 is resolved) correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var comparePeriod1 = dateSelector.getComparePeriod(userData.mondayUser.comparePd1WeeksBack, userData.mondayUser.dateFormat);

            var selectedPeriodDate = zoneConversionWidget.getSelectedPeriodDateRange(userData.mondayUser.dateFormat);
            var priorPeriodDate = zoneConversionWidget.getPriorPeriodDateRange(userData.mondayUser.dateFormat);

            var comparePeriod2 = dateSelector.getComparePeriod(userData.mondayUser.comparePd2WeeksBack, userData.mondayUser.dateFormat);
            var priorPeriod2Date = zoneConversionWidget.getPriorYearDateRange(userData.mondayUser.dateFormat);


            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(comparePeriod1);
            expect(priorPeriod2Date).toEqual(comparePeriod2);
          });
        });

        describe('ATS widget', function () {

          it('widget chart should display correct date range on x-axis', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var allXAxisDates = averagePurchaseWidget.getXAxisDates(userData.mondayUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
              var reportArray = promiseArray[0];
              var chartDateArray = promiseArray[1];
              //first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(function (dateItem) {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var comparePeriod1 = dateSelector.getComparePeriod(userData.mondayUser.comparePd1WeeksBack, userData.mondayUser.dateFormat);
            var selectedPeriodDate = averagePurchaseWidget.getSelectedPeriodDateRange(userData.mondayUser.dateFormat);
            var priorPeriodDate = averagePurchaseWidget.getPriorPeriodDateRange(userData.mondayUser.dateFormat);

            var comparePeriod2 = dateSelector.getComparePeriod(userData.mondayUser.comparePd2WeeksBack, userData.mondayUser.dateFormat);
            var priorPeriod2Date = averagePurchaseWidget.getPriorYearDateRange(userData.mondayUser.dateFormat);


            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(comparePeriod1);
            expect(priorPeriod2Date).toEqual(comparePeriod2);
          });
        });
      });
    });
  }};
