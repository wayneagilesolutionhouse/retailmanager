'use strict';

module.exports = {
//shared tests for localization translations, using prior period/prior year date ranges

  salesAndConversionSharedLangTests: function (locale) {

    describe('Sales and Conversion tab (shared translation tests)', function () {

      var translationsFilePath = '../../../src/l10n/languages/' + locale + '.json'; //sets up filepath for required translations file
      var translations = require(translationsFilePath);


      var sitePage = require('../pages/site-summary-page.js');
      var dateSelector = require('../pages/components/time-period-picker.js');
      var nav = require('../pages/components/nav-header.js');
      var tabSidebar = require('../pages/components/tab-nav.js');
      var orgData = require('../data/orgs.js');
      var userData = require('../data/users.js');
      var tenantSalesWidget = require('../pages/components/tenant-sales-summary-widget.js');
      var tenantConversionSumWidget = require('../pages/components/tenant-conversion-summary-widget.js');
      var tenantATSSumWidget = require('../pages/components/tenant-ats-summary-widget.js');
      var tenantUPTSumWidget = require('../pages/components/tenant-upt-summary-widget.js');
      var zoneSalesWidget = require('../pages/components/zone-sales-widget.js');
      var zoneConversionWidget = require('../pages/components/zone-conversion-widget.js');
      var averagePurchaseWidget = require('../pages/components/average-purchase-widget.js');
      var zoneUPTWidget = require('../pages/components/zone-units-per-transaction-widget.js');

      it('should navigate to the correct site', function () {
        var title = nav.getSiteName();

        expect(title).toEqual(orgData.MSOrgSite.name);
      });

      describe('"Property overall"-level tests', function () {

        describe('Page title and tab sidebar checks', function () {

          it('"Site Sales and Conversion" tab title', function () {
            var tabHeading = sitePage.siteTitle();

            expect(tabHeading.getText()).toEqual(translations.common.SITE.toUpperCase() + ' ' + translations.views.SALES.toUpperCase());
          });

          it('"page title should show the correct location" area name', function () {
            var areaName = sitePage.getAreaName();

            expect(areaName.getText()).toEqual(orgData.MSOrgSite.name);
          });

          it('"select zone" picker title', function () {
            var pickerTitle = sitePage.getZonePickerTitleText();

            expect(pickerTitle).toEqual(translations.analyticsHeader.SELECTZONE.toUpperCase());
          });

          it('default value in zone-picker search bar', function () {
            var pickerTitle = sitePage.getZonePickerSearchText();

            expect(pickerTitle).toMatch(translations.locationSelector.SEARCH);
          });
        });

        describe('Tenant sales summary widget', function () {

          it('widget title', function () {
            expect(tenantSalesWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.tenant_sales);
          });

          it('default value in table search bar', function () {
            expect(tenantSalesWidget.getFilterBarText()).toEqual(translations.salesTableWidget.FILTER);
          });

          it('header text on table', function () {
            expect(tenantSalesWidget.getCurrentColumnHeader().getText()).toEqual(translations.common.SELECTEDPERIOD + ' ' + translations.kpis.shortKpiTitles.tenant_sales);
            expect(tenantSalesWidget.getPeriodChangeColumnHeader().getText()).toEqual(translations.salesTableWidget.CHANGE);
            expect(tenantSalesWidget.getPeriodColumnHeader().getText()).toEqual(translations.common.PRIORPERIOD + ' ' + translations.kpis.shortKpiTitles.tenant_sales);
            expect(tenantSalesWidget.getYearChangeColumnHeader().getText()).toEqual(translations.salesTableWidget.CHANGE);
            expect(tenantSalesWidget.getYearColumnHeader().getText()).toEqual(translations.common.PRIORYEAR + ' ' + translations.kpis.shortKpiTitles.tenant_sales);
          });

          it('text on table expander button', function () {
            var expandButton = tenantSalesWidget.getExpandButtonText();
            expect(expandButton).toEqual(translations.salesTableWidget.VIEWALL);
            tenantSalesWidget.clickExpandButtonIfPresent();
            var contractButton = tenantSalesWidget.getExpandButtonText();
            expect(contractButton).toEqual(translations.salesTableWidget.VIEWLESS);
          });
        });

        describe('Tenant conversion summary widget', function () {

          it('widget title', function () {
            expect(tenantConversionSumWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.tenant_conversion);
          });

          it('default value in table search bar', function () {
            expect(tenantConversionSumWidget.getFilterBarText()).toEqual(translations.salesTableWidget.FILTER);
          });

          it('header text on table', function () {
            expect(tenantConversionSumWidget.getCurrentColumnHeader().getText()).toEqual(translations.common.SELECTEDPERIOD + ' ' + translations.kpis.shortKpiTitles.tenant_conversion);
            expect(tenantConversionSumWidget.getPeriodChangeColumnHeader().getText()).toEqual(translations.salesTableWidget.CHANGE);
            expect(tenantConversionSumWidget.getPeriodColumnHeader().getText()).toEqual(translations.common.PRIORPERIOD + ' ' + translations.kpis.shortKpiTitles.tenant_conversion);
            expect(tenantConversionSumWidget.getYearChangeColumnHeader().getText()).toEqual(translations.salesTableWidget.CHANGE);
            expect(tenantConversionSumWidget.getYearColumnHeader().getText()).toEqual(translations.common.PRIORYEAR + ' ' + translations.kpis.shortKpiTitles.tenant_conversion);
          });

          it('text on table expander button', function () {
            var expandButton = tenantConversionSumWidget.getExpandButtonText();
            expect(expandButton).toEqual(translations.salesTableWidget.VIEWALL);
            tenantConversionSumWidget.clickExpandButtonIfPresent();
            var contractButton = tenantConversionSumWidget.getContractButtonText();
            expect(contractButton).toEqual(translations.salesTableWidget.VIEWLESS);
          });
        });

        describe('Tenant ATS summary widget', function () {

          it('widget title', function () {
            expect(tenantATSSumWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.tenant_ats);
          });

          it('default value in table search bar', function () {
            expect(tenantATSSumWidget.getFilterBarText()).toEqual(translations.salesTableWidget.FILTER);
          });

          it('header text on table', function () {
            expect(tenantATSSumWidget.getCurrentColumnHeader().getText()).toEqual(translations.common.SELECTEDPERIOD + ' ' + translations.kpis.shortKpiTitles.tenant_ats);
            expect(tenantATSSumWidget.getPeriodChangeColumnHeader().getText()).toEqual(translations.salesTableWidget.CHANGE);
            expect(tenantATSSumWidget.getPeriodColumnHeader().getText()).toEqual(translations.common.PRIORPERIOD + ' ' + translations.kpis.shortKpiTitles.tenant_ats);
            expect(tenantATSSumWidget.getYearChangeColumnHeader().getText()).toEqual(translations.salesTableWidget.CHANGE);
            expect(tenantATSSumWidget.getYearColumnHeader().getText()).toEqual(translations.common.PRIORYEAR + ' ' + translations.kpis.shortKpiTitles.tenant_ats);
          });

          it('text on table expander button', function () {
            var expandButton = tenantATSSumWidget.getExpandButtonText();
            expect(expandButton).toEqual(translations.salesTableWidget.VIEWALL);
            tenantATSSumWidget.clickExpandButtonIfPresent();
            var contractButton = tenantATSSumWidget.getContractButtonText();
            expect(contractButton).toEqual(translations.salesTableWidget.VIEWLESS);
          });
        });

        describe('Tenant UPT summary widget', function () {

          it('widget title', function () {
            expect(tenantUPTSumWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.tenant_upt);
          });

          it('default value in table search bar', function () {
            expect(tenantUPTSumWidget.getFilterBarText()).toEqual(translations.salesTableWidget.FILTER);
          });

          it('header text on table', function () {
            expect(tenantUPTSumWidget.getCurrentColumnHeader().getText()).toEqual(translations.common.SELECTEDPERIOD + ' ' + translations.kpis.shortKpiTitles.tenant_upt);
            expect(tenantUPTSumWidget.getPeriodChangeColumnHeader().getText()).toEqual(translations.salesTableWidget.CHANGE);
            expect(tenantUPTSumWidget.getPeriodColumnHeader().getText()).toEqual(translations.common.PRIORPERIOD + ' ' + translations.kpis.shortKpiTitles.tenant_upt);
            expect(tenantUPTSumWidget.getYearChangeColumnHeader().getText()).toEqual(translations.salesTableWidget.CHANGE);
            expect(tenantUPTSumWidget.getYearColumnHeader().getText()).toEqual(translations.common.PRIORYEAR + ' ' + translations.kpis.shortKpiTitles.tenant_upt);
          });

          it('text on table expander button', function () {
            var expandButton = tenantUPTSumWidget.getExpandButtonText();
            expect(expandButton).toEqual(translations.salesTableWidget.VIEWALL);
            tenantUPTSumWidget.clickExpandButtonIfPresent();
            var contractButton = tenantUPTSumWidget.getContractButtonText();
            expect(contractButton).toEqual(translations.salesTableWidget.VIEWLESS);
          });
        });
      });

      describe('Zone-level tests', function () {

        beforeAll(function () {
          browser.executeScript('window.scrollTo(0,0);').then(function () {
            sitePage.navToZone();
          });
        });

        describe('Page title and tab sidebar checks', function () {

          it('"Sales and Conversion" tab title', function () {
            var tabHeading = sitePage.siteTitle();

            expect(tabHeading.getText()).toEqual(translations.common.ZONE.toUpperCase() + ' ' + translations.views.SALES.toUpperCase());
          });

          it('test zone name', function () {
            var areaName = sitePage.getAreaName();

            expect(areaName).toMatch(orgData.MSOrgSite.testZone);
          });
        });

        describe('Zone-level sales widget', function () {

          it('widget title', function() {
            expect(zoneSalesWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.sales);
          });

          it('date range titles in chart legend', function(){
            expect(zoneSalesWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.PRIORPERIOD.toLowerCase(), translations.common.PRIORYEAR.toLowerCase()]);
          });

          it('date range titles in chart summary frame', function(){
            var selectedPeriodLabel = zoneSalesWidget.getSummaryFrameSelectedPeriodLabel();
            var priorPeriodLabel = zoneSalesWidget.getSummaryFrameCompare1Label();
            var priorYearLabel = zoneSalesWidget.getSummaryFrameCompare2Label();

            expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
            expect(priorPeriodLabel).toEqual(translations.common.PRIORPERIOD.toUpperCase());
            expect(priorYearLabel).toEqual(translations.common.PRIORYEAR.toUpperCase());
          });

          it('metric unit label in chart summary frame', function(){
            expect(zoneSalesWidget.getSummaryFrameMetricLabel()).toEqual(translations.kpis.totalLabel.sales.toUpperCase());
          });

          it('tooltip text', () => {
            const tooltipTotalText = zoneSalesWidget.getTooltipTotalText();
            tooltipTotalText.then(textArray => {
              expect(textArray[0]).toEqual(translations.lineChartWidget.TOTAL);
            });
          });
        });
      });

      describe('Zone-level conversion widget', function () {

        it('widget title', function() {
          expect(zoneConversionWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.conversion);
        });

        it('date range titles in chart legend', function(){
          expect(zoneConversionWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.PRIORPERIOD.toLowerCase(), translations.common.PRIORYEAR.toLowerCase()]);
        });

        it('date range titles in chart summary frame', function(){
          var selectedPeriodLabel = zoneConversionWidget.getSummaryFrameSelectedPeriodLabel();
          var priorPeriodLabel = zoneConversionWidget.getSummaryFrameCompare1Label();
          var priorYearLabel = zoneConversionWidget.getSummaryFrameCompare2Label();

          expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
          expect(priorPeriodLabel).toEqual(translations.common.PRIORPERIOD.toUpperCase());
          expect(priorYearLabel).toEqual(translations.common.PRIORYEAR.toUpperCase());
        });

        it('metric unit label in chart summary frame', function(){
          expect(zoneConversionWidget.getSummaryFrameMetricLabel()).toEqual(translations.kpis.totalLabel.conversion.toUpperCase());
        });

        it('tooltip text', () => {
          const tooltipTotalText = zoneConversionWidget.getTooltipTotalText();
          tooltipTotalText.then(textArray => {
            expect(textArray[0]).toEqual(translations.lineChartWidget.TOTAL);
          });
        });
      });

      describe('Zone-level ats widget', function () {

        it('widget title', function() {
          expect(averagePurchaseWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.ats);
        });

        it('date range titles in chart legend', function(){
          expect(averagePurchaseWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.PRIORPERIOD.toLowerCase(), translations.common.PRIORYEAR.toLowerCase()]);
        });

        it('date range titles in chart summary frame', function(){
          var selectedPeriodLabel = averagePurchaseWidget.getSummaryFrameSelectedPeriodLabel();
          var priorPeriodLabel = averagePurchaseWidget.getSummaryFrameCompare1Label();
          var priorYearLabel = averagePurchaseWidget.getSummaryFrameCompare2Label();

          expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
          expect(priorPeriodLabel).toEqual(translations.common.PRIORPERIOD.toUpperCase());
          expect(priorYearLabel).toEqual(translations.common.PRIORYEAR.toUpperCase());
        });

        it('metric unit label in chart summary frame', function(){
          expect(averagePurchaseWidget.getSummaryFrameMetricLabel()).toEqual(translations.kpis.totalLabel.ats.toUpperCase());
        });

        it('tooltip text', () => {
          const tooltipTotalText = averagePurchaseWidget.getTooltipTotalText();
          tooltipTotalText.then(textArray => {
            expect(textArray[0]).toEqual(translations.lineChartWidget.TOTAL);
          });
        });
      });

      describe('Zone-level UPT widget', function () {

        it('widget title', function() {
          expect(zoneUPTWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.upt);
        });

        //chart legend expectation below is less complete than other checks of this type - this widget
        // does not have data for this org/site, so prior series text is not shown in legend
        it('date range titles in chart legend', function(){
          expect(zoneUPTWidget.getLegendText()).toContain(translations.common.SELECTEDPERIOD.toLowerCase());
          // expect(zoneUPTWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD, translations.common.PRIORPERIOD, translations.common.PRIORYEAR])
        });

        it('date range titles in chart summary frame', function(){
          var selectedPeriodLabel = zoneUPTWidget.getSummaryFrameSelectedPeriodLabel();
          var priorPeriodLabel = zoneUPTWidget.getSummaryFrameCompare1Label();
          var priorYearLabel = zoneUPTWidget.getSummaryFrameCompare2Label();

          expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
          expect(priorPeriodLabel).toEqual(translations.common.PRIORPERIOD.toUpperCase());
          expect(priorYearLabel).toEqual(translations.common.PRIORYEAR.toUpperCase());
        });

        it('metric unit label in chart summary frame', function(){
          expect(zoneUPTWidget.getSummaryFrameMetricLabel()).toEqual(translations.kpis.totalLabel.upt.toUpperCase());
        });

        it('tooltip text', () => {
          const tooltipTotalText = zoneUPTWidget.getTooltipTotalText();
          tooltipTotalText.then(textArray => {
            expect(textArray[0]).toEqual(translations.lineChartWidget.TOTAL);
          });
        });
      });
    });
  }
};
