'use strict';

module.exports = {
//shared tests for localization translations, using prior period/prior year date ranges

  trafficSharedLangTests: function (locale) {

    describe('Traffic tab (shared translation tests)', function () {

      var translationsFilePath = '../../../src/l10n/languages/' + locale + '.json'; //sets up filepath for required translations file
      var translations = require(translationsFilePath);

      var sitePage = require('../pages/site-summary-page.js');
      var dateSelector = require('../pages/components/time-period-picker.js');
      var nav = require('../pages/components/nav-header.js');
      var tabSidebar = require('../pages/components/tab-nav.js');
      var orgData = require('../data/orgs.js');
      var userData = require('../data/users.js');
      var entranceSummaryWidget = require('../pages/components/entrance-summary-widget.js');
      var tenantSummaryWidget = require('../pages/components/tenant-traffic-summary-widget.js');
      var entranceContributionWidget = require('../pages/components/entrance-contribution-widget.js');
      var commonAreasSummaryWidget = require('../pages/components/common-areas-summary-widget.js');
      var powerHoursWidget = require('../pages/components/power-hours-widget.js');
      var dailyMetricBarWidget = require('../pages/components/daily-metric-averages-widget.js');
      var trafficWidget = require('../pages/components/site-traffic-widget.js');

      it('should navigate to the correct site', function () {
        var title = nav.getSiteName();

        expect(title).toEqual(orgData.MSOrgSite.name);
      });

      describe('Page title and tab sidebar checks', function() {

        it('"Site Traffic" tab title', function () {
          var tabHeading = sitePage.siteTitle();
          var tabText = tabSidebar.getTrafficTabName();

          expect(tabHeading.getText()).toEqual(translations.common.SITE.toUpperCase() + ' ' + translations.views.TRAFFIC.toUpperCase());
        });

        it('"page title should show the correct location" area name', function () {
          var areaName = sitePage.getAreaName();

          expect(areaName.getText()).toEqual(orgData.MSOrgSite.name);
        });

        it('"select zone" picker title', function() {
          var pickerTitle = sitePage.getZonePickerTitleText();

          expect(pickerTitle).toEqual(translations.analyticsHeader.SELECTZONE.toUpperCase());
        });

        it('default value in zone-picker search bar', function() {
          var pickerTitle = sitePage.getZonePickerSearchText();

          expect(pickerTitle).toMatch(translations.locationSelector.SEARCH);
        });

        it('"hours of operation" dropdown title', function() {
          var hoursText = sitePage.getHoursPickerTitleText();

          expect(hoursText).toEqual(translations.analyticsHeader.OPHOURS.toUpperCase());
        });

        it('options in "hours of operation" dropdown', function() {
          var hoursMenuOptions = sitePage.getHoursPickerOptionText();

          expect(hoursMenuOptions).toEqual([translations.analyticsHeader.BUSINESSDAYS,translations.analyticsHeader.BUSINESSHOURS]);
        });
      });

      describe('Traffic widget', function(){

        it('widget title', function() {
          expect(trafficWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.traffic);
        });

        it('date range titles in chart legend', function(){
          expect(trafficWidget.getLegendTextLowerCase()).toEqual([translations.common.SELECTEDPERIOD.toLowerCase(), translations.common.PRIORPERIOD.toLowerCase(), translations.common.PRIORYEAR.toLowerCase()]);
        });

        it('date range titles in chart summary frame', function(){
          var selectedPeriodLabel = trafficWidget.getSummaryFrameSelectedPeriodLabel();
          var priorPeriodLabel = trafficWidget.getSummaryFrameCompare1Label();
          var priorYearLabel = trafficWidget.getSummaryFrameCompare2Label();

          expect(selectedPeriodLabel).toEqual(translations.common.SELECTEDPERIOD);
          expect(priorPeriodLabel).toEqual(translations.common.PRIORPERIOD.toUpperCase());
          expect(priorYearLabel).toEqual(translations.common.PRIORYEAR.toUpperCase());
        });

        it('metric unit label in chart summary frame', function(){
          expect(trafficWidget.getSummaryFrameMetricLabel()).toEqual(translations.kpis.totalLabel.traffic.toUpperCase());
        });

        it('tooltip text', () => {
          const tooltipTotalText = trafficWidget.getTooltipTotalText();
          tooltipTotalText.then(textArray => {
            expect(textArray[0]).toEqual(translations.lineChartWidget.TOTAL);
          });
       });
      });

      describe('Entrance summary widget', function() {

        it('widget title', function() {
          expect(entranceSummaryWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.entrance_contribution);
        });

        it('default value in table search bar', function() {
          expect(entranceSummaryWidget.getFilterBarText()).toEqual(translations.entranceContributionWidget.FILTER);
        });

        it('header text on table', function() {
          expect(entranceSummaryWidget.getCurrentColumnHeader().getText()).toEqual(translations.entranceContributionWidget.CURRENTPERIOD);
          expect(entranceSummaryWidget.getPeriodChangeColumnHeader().getText()).toEqual(translations.entranceContributionWidget.CHANGE);
          expect(entranceSummaryWidget.getPeriodColumnHeader().getText()).toEqual(translations.entranceContributionWidget.PRIORPERIOD +
            ' ' + translations.entranceContributionWidget.TRAFFIC);
          expect(entranceSummaryWidget.getYearChangeColumnHeader().getText()).toEqual(translations.entranceContributionWidget.CHANGE);
          expect(entranceSummaryWidget.getYearColumnHeader().getText()).toEqual(translations.entranceContributionWidget.PRIORYEAR +
            ' ' + translations.entranceContributionWidget.TRAFFIC);
        });

      });

      describe('Entrance contribution widget', function(){

        it('widget title', function() {
          expect(entranceContributionWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.entrance_contribution_pie);
        });
      });

      describe('Power hours widget', function() {

        it('widget title', function() {
          expect(powerHoursWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.power_hours);
        });

        it('metric options', function() {
          expect(powerHoursWidget.getMetricOptions()).toEqual(
            [translations.kpis.options.average_traffic,
              translations.kpis.options['traffic (pct)'],
              translations.kpis.options.average_sales,
              translations.kpis.options.conversion,
              translations.kpis.options.ats
            ]
          );
        });

        it('column headers', function() {
          expect(powerHoursWidget.getDayHeaders()).toEqual(
            [translations.weekdaysShort.sun.toUpperCase(),
              translations.weekdaysShort.mon.toUpperCase(),
              translations.weekdaysShort.tue.toUpperCase(),
              translations.weekdaysShort.wed.toUpperCase(),
              translations.weekdaysShort.thu.toUpperCase(),
              translations.weekdaysShort.fri.toUpperCase(),
              translations.weekdaysShort.sat.toUpperCase()
            ]
          );

          expect(powerHoursWidget.getTotalHeader().getText()).toEqual(translations.powerHoursWidget.TOTAL.toUpperCase());
        });

        it('total row header', function() {
          expect(powerHoursWidget.getTotalRowHeader().getText()).toEqual(translations.powerHoursWidget.DAILYAVERAGE.toUpperCase());
        });

        it('text in chart legend', function() {
          expect(powerHoursWidget.getChartLegendText('medium')).toMatch(translations.powerHoursWidget.TRAFFIC);
          expect(powerHoursWidget.getChartLegendText('high')).toMatch(translations.powerHoursWidget.TRAFFIC);
        });
      });

      describe('Daily average metric widget', function () {

        it('widget title', function() {
          expect(dailyMetricBarWidget.widgetTitle().getText()).toEqual(translations.trafficPerWeekdayWidget.DAILYAVERAGES +
            ': ' + translations.kpis.kpiTitle.traffic);
        });

        it('days of week on x-axis', function() {
          expect(dailyMetricBarWidget.getXAxisLabels()).toEqual(
            [translations.weekdaysLong.sun,
              translations.weekdaysLong.mon,
              translations.weekdaysLong.tue,
              translations.weekdaysLong.wed,
              translations.weekdaysLong.thu,
              translations.weekdaysLong.fri,
              translations.weekdaysLong.sat
            ]
          );
        });

        it('date range titles in chart legend', function(){
          expect(dailyMetricBarWidget.getLegendText()).toEqual([translations.common.SELECTEDPERIOD, translations.common.PRIORPERIOD,
            translations.common.PRIORYEAR]);
        });

        it('tooltip text', function() {
          expect(dailyMetricBarWidget.getTooltipTotalText()).toEqual([translations.common.SELECTEDPERIOD, translations.common.PRIORPERIOD, translations.common.PRIORYEAR]);
        });
      });

      describe('Tenant summary widget', function() {

        it('widget title', function() {
          expect(tenantSummaryWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.table_tenant);
        });

        it('default value in table search bar', function() {
          expect(tenantSummaryWidget.getFilterBarText()).toEqual(translations.trafficTableWidget.FILTER);
        });

        it('header text on table', function() {
          expect(tenantSummaryWidget.getCurrentColumnHeader().getText()).toEqual(translations.common.SELECTEDPERIOD + ' ' + translations.trafficTableWidget.TRAFFIC);
          expect(tenantSummaryWidget.getPeriodChangeColumnHeader().getText()).toEqual(translations.trafficTableWidget.CHANGE);
          expect(tenantSummaryWidget.getPeriodColumnHeader().getText()).toEqual(translations.common.PRIORPERIOD + ' ' + translations.trafficTableWidget.TRAFFIC);
          expect(tenantSummaryWidget.getYearChangeColumnHeader().getText()).toEqual(translations.trafficTableWidget.CHANGE);
          expect(tenantSummaryWidget.getYearColumnHeader().getText()).toEqual(translations.common.PRIORYEAR + ' ' + translations.trafficTableWidget.TRAFFIC);
        });

        it('text on table expander button', function() {
          var expandButton = tenantSummaryWidget.getExpandButtonText();
          expect(expandButton).toEqual(translations.trafficTableWidget.VIEWALL);
          tenantSummaryWidget.clickExpandButtonIfPresent();
          browser.waitForAngular();
          var contractButton = tenantSummaryWidget.getContractButtonText();
          expect(contractButton).toEqual(translations.trafficTableWidget.VIEWLESS);
        });
      });

      describe('Common areas widget', function() {

        it('widget title', function() {
          expect(commonAreasSummaryWidget.widgetTitle().getText()).toEqual(translations.kpis.kpiTitle.table_other_areas);
        });

        it('default value in table search bar', function() {
          expect(commonAreasSummaryWidget.getFilterBarText()).toEqual(translations.trafficTableWidget.FILTER);
        });

        it('header text on table', function() {
          expect(commonAreasSummaryWidget.getCurrentColumnHeader().getText()).toEqual(translations.common.SELECTEDPERIOD + ' ' + translations.trafficTableWidget.TRAFFIC);
          expect(commonAreasSummaryWidget.getPeriodChangeColumnHeader().getText()).toEqual(translations.trafficTableWidget.CHANGE);
          expect(commonAreasSummaryWidget.getPeriodColumnHeader().getText()).toEqual(translations.common.PRIORPERIOD + ' ' + translations.trafficTableWidget.TRAFFIC);
          expect(commonAreasSummaryWidget.getYearChangeColumnHeader().getText()).toEqual(translations.trafficTableWidget.CHANGE);
          expect(commonAreasSummaryWidget.getYearColumnHeader().getText()).toEqual(translations.common.PRIORYEAR + ' ' + translations.trafficTableWidget.TRAFFIC);
        });
        //no table expander button for this widget in this org
      });
    });
  }
};
