'use strict';

module.exports = {


  visitorBehaviorSharedTestsMonday: function (timePeriod) {

    describe('Visitor behavior tab (shared tests)', function () {

      var sitePage = require('../pages/site-summary-page.js');
      var dateSelector = require('../pages/components/time-period-picker.js');
      var nav = require('../pages/components/nav-header.js');
      var orgData = require('../data/orgs.js');
      var userData = require('../data/users.js');
      var visitorTrafficWidget = require('../pages/components/visitor-behavior-traffic-widget.js');
      var visitingFrequencyWidget = require('../pages/components/visiting-frequency-widget.js');
      var gshWidget = require('../pages/components/gross-shopping-hours-widget.js');
      var dwellTimeWidget = require('../pages/components/dwell-time-widget.js');
      var opportunityWidget = require('../pages/components/opportunity-widget.js');
      var drawRateWidget = require('../pages/components/draw-rate-widget.js');
      var shoppersVsOthersWidget = require('../pages/components/shoppers-vs-others-widget.js');
      var abandonmentRateWidget = require('../pages/components/abandonment-rate-widget.js');

      it('date picker should appear', function () {
        var datePicker = dateSelector.getDatePicker();

        expect(datePicker.isPresent()).toBe(true);
      });

      it('should have selected the correct date button', function () {
        var dateButton;

        if(timePeriod === 'week') {
          dateButton = dateSelector.getWeekButton();
        }
        else if(timePeriod === 'month') {
          dateButton = dateSelector.getMonthButton();
        }
        else if(timePeriod === 'year') {
          dateButton = dateSelector.getYearButton();
        }
        dateButton.then(function (button) {
          expect(button.getAttribute('class')).toMatch('active')
        });
      });

      describe('"Property overall"-level tests', function () {

        describe('Visitor behavior traffic widget', function () {

          it('widget chart should display correct date range on x-axis', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var allXAxisDates = visitorTrafficWidget.getXAxisDates(userData.mondayUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
              var reportArray = promiseArray[0];
              var chartDateArray = promiseArray[1];
              //first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(function (dateItem) {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var comparePeriod1 = dateSelector.getComparePeriod(userData.mondayUser.comparePd1WeeksBack, userData.mondayUser.dateFormat);

            var selectedPeriodDate = visitorTrafficWidget.getSelectedPeriodDateRange(userData.mondayUser.dateFormat);
            var priorPeriodDate = visitorTrafficWidget.getPriorPeriodDateRange(userData.mondayUser.dateFormat);

            var comparePeriod2 = dateSelector.getComparePeriod(userData.mondayUser.comparePd2WeeksBack, userData.mondayUser.dateFormat);
            var priorPeriod2Date = visitorTrafficWidget.getPriorYearDateRange(userData.mondayUser.dateFormat);


            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(comparePeriod1);
            expect(priorPeriod2Date).toEqual(comparePeriod2);

          });
        });


        describe('Visiting frequency widget', function () {

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var comparePeriod1 = dateSelector.getComparePeriod(userData.mondayUser.comparePd1WeeksBack, userData.mondayUser.dateFormat);

            var selectedPeriodDate = visitingFrequencyWidget.getSelectedPeriodDateRange(userData.mondayUser.dateFormat);
            var priorPeriodDate = visitingFrequencyWidget.getPriorPeriodDateRange(userData.mondayUser.dateFormat);

            var comparePeriod2 = dateSelector.getComparePeriod(userData.mondayUser.comparePd2WeeksBack, userData.mondayUser.dateFormat);
            var priorPeriod2Date = visitingFrequencyWidget.getPriorYearDateRange(userData.mondayUser.dateFormat);


            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(comparePeriod1);
            expect(priorPeriod2Date).toEqual(comparePeriod2);

          });
        });

        describe('Gross shopping hours widget', function () {

          it('widget chart should display correct date range on x-axis', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var allXAxisDates = gshWidget.getXAxisDates(userData.mondayUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
              var reportArray = promiseArray[0];
              var chartDateArray = promiseArray[1];
              //first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(function (dateItem) {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var comparePeriod1 = dateSelector.getComparePeriod(userData.mondayUser.comparePd1WeeksBack, userData.mondayUser.dateFormat);

            var selectedPeriodDate = gshWidget.getSelectedPeriodDateRange(userData.mondayUser.dateFormat);
            var priorPeriodDate = gshWidget.getPriorPeriodDateRange(userData.mondayUser.dateFormat);

            var comparePeriod2 = dateSelector.getComparePeriod(userData.mondayUser.comparePd2WeeksBack, userData.mondayUser.dateFormat);
            var priorPeriod2Date = gshWidget.getPriorYearDateRange(userData.mondayUser.dateFormat);


            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(comparePeriod1);
            expect(priorPeriod2Date).toEqual(comparePeriod2);

          });
        });

        describe('Dwell time widget', function () {

          it('widget chart should display correct date range on x-axis', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var allXAxisDates = dwellTimeWidget.getXAxisDates(userData.mondayUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
              var reportArray = promiseArray[0];
              var chartDateArray = promiseArray[1];
              //first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(function (dateItem) {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var comparePeriod1 = dateSelector.getComparePeriod(userData.mondayUser.comparePd1WeeksBack, userData.mondayUser.dateFormat);

            var selectedPeriodDate = dwellTimeWidget.getSelectedPeriodDateRange(userData.mondayUser.dateFormat);
            var priorPeriodDate = dwellTimeWidget.getPriorPeriodDateRange(userData.mondayUser.dateFormat);

            var comparePeriod2 = dateSelector.getComparePeriod(userData.mondayUser.comparePd2WeeksBack, userData.mondayUser.dateFormat);
            var priorPeriod2Date = dwellTimeWidget.getPriorYearDateRange(userData.mondayUser.dateFormat);

            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(comparePeriod1);
            expect(priorPeriod2Date).toEqual(comparePeriod2);

          });
        });

        describe('Opportunity widget', function () {

          it('widget chart should display correct date range on x-axis', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var allXAxisDates = opportunityWidget.getXAxisDates(userData.mondayUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
              var reportArray = promiseArray[0];
              var chartDateArray = promiseArray[1];
              //first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(function (dateItem) {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var comparePeriod1 = dateSelector.getComparePeriod(userData.mondayUser.comparePd1WeeksBack, userData.mondayUser.dateFormat);

            var selectedPeriodDate = opportunityWidget.getSelectedPeriodDateRange(userData.mondayUser.dateFormat);
            var priorPeriodDate = opportunityWidget.getPriorPeriodDateRange(userData.mondayUser.dateFormat);

            var comparePeriod2 = dateSelector.getComparePeriod(userData.mondayUser.comparePd2WeeksBack, userData.mondayUser.dateFormat);
            var priorPeriod2Date = opportunityWidget.getPriorYearDateRange(userData.mondayUser.dateFormat);

            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(comparePeriod1);
            expect(priorPeriod2Date).toEqual(comparePeriod2);
          });
        });

        describe('Draw rate widget', function () {

          it('widget chart should display correct date range on x-axis', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var allXAxisDates = drawRateWidget.getXAxisDates(userData.mondayUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
              var reportArray = promiseArray[0];
              var chartDateArray = promiseArray[1];
              //first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(function (dateItem) {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var comparePeriod1 = dateSelector.getComparePeriod(userData.mondayUser.comparePd1WeeksBack, userData.mondayUser.dateFormat);

            var selectedPeriodDate = drawRateWidget.getSelectedPeriodDateRange(userData.mondayUser.dateFormat);
            var priorPeriodDate = drawRateWidget.getPriorPeriodDateRange(userData.mondayUser.dateFormat);

            var comparePeriod2 = dateSelector.getComparePeriod(userData.mondayUser.comparePd2WeeksBack, userData.mondayUser.dateFormat);
            var priorPeriod2Date = drawRateWidget.getPriorYearDateRange(userData.mondayUser.dateFormat);

            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(comparePeriod1);
            expect(priorPeriod2Date).toEqual(comparePeriod2);

          });
        });

        describe('Shoppers vs others widget', function () {

          it('widget chart should display correct date range on x-axis', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var allXAxisDates = shoppersVsOthersWidget.getXAxisDates(userData.mondayUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
              var reportArray = promiseArray[0];
              var chartDateArray = promiseArray[1];
              //first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(function (dateItem) {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var comparePeriod1 = dateSelector.getComparePeriod(userData.mondayUser.comparePd1WeeksBack, userData.mondayUser.dateFormat);

            var selectedPeriodDate = shoppersVsOthersWidget.getSelectedPeriodDateRange(userData.mondayUser.dateFormat);
            var priorPeriodDate = shoppersVsOthersWidget.getPriorPeriodDateRange(userData.mondayUser.dateFormat);

            var comparePeriod2 = dateSelector.getComparePeriod(userData.mondayUser.comparePd2WeeksBack, userData.mondayUser.dateFormat);
            var priorPeriod2Date = shoppersVsOthersWidget.getPriorYearDateRange(userData.mondayUser.dateFormat);

            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(comparePeriod1);
            expect(priorPeriod2Date).toEqual(comparePeriod2);
          });
        });
      });

      describe('"Single area"-level tests', function () {

        beforeAll(function () {
          browser.executeScript('window.scrollTo(0,0);').then(function () {
            sitePage.navToTestArea();
          });
        });

        it('should have selected the correct date button', function () {
          var dateButton;

          if(timePeriod === 'week') {
            dateButton = dateSelector.getWeekButton();
          }
          else if(timePeriod === 'month') {
            dateButton = dateSelector.getMonthButton();
          }
          else if(timePeriod === 'year') {
            dateButton = dateSelector.getYearButton();
          }
          dateButton.then(function (button) {
            expect(button.getAttribute('class')).toMatch('active')
          });
        });

        describe('Abandonment rate widget', function () {

          it('widget chart should display correct date range on x-axis', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var allXAxisDates = abandonmentRateWidget.getXAxisDates(userData.mondayUser.dateFormat);

            protractor.promise.all([reportPeriod, allXAxisDates]).then(function (promiseArray) {
              var reportArray = promiseArray[0];
              var chartDateArray = promiseArray[1];
              //first point on x-axis should match start date of time period
              expect(chartDateArray[0]).toEqual(reportArray[0]);
              chartDateArray.forEach(function (dateItem) {
                expect(dateItem).not.toBeLessThan(reportArray[0]);
                expect(dateItem).not.toBeGreaterThan(reportArray[1]);
              });
            });
          });

          it('correct date range should appear in summary frame', function () {
            var reportPeriod = dateSelector.getReportingPeriod(userData.mondayUser.dateFormat);
            var comparePeriod1 = dateSelector.getComparePeriod(userData.mondayUser.comparePd1WeeksBack, userData.mondayUser.dateFormat);

            var selectedPeriodDate = abandonmentRateWidget.getSelectedPeriodDateRange(userData.mondayUser.dateFormat);
            var priorPeriodDate = abandonmentRateWidget.getPriorPeriodDateRange(userData.mondayUser.dateFormat);

            var comparePeriod2 = dateSelector.getComparePeriod(userData.mondayUser.comparePd2WeeksBack, userData.mondayUser.dateFormat);
            var priorPeriod2Date = abandonmentRateWidget.getPriorYearDateRange(userData.mondayUser.dateFormat);


            expect(selectedPeriodDate).toEqual(reportPeriod);
            expect(priorPeriodDate).toEqual(comparePeriod1);
            expect(priorPeriod2Date).toEqual(comparePeriod2);
          });
        });
      });
    });
  }
};
