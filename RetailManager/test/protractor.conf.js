const utils = require('./e2e/pages/common-utils/protractor-utils');

// Local Chrome-based configuration file.
exports.config = {
  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    'browserName': 'chrome',
      // 'shardTestFiles': true,
      // 'maxInstances' : 3,
    'chromeOptions': {
      // Set download path and avoid prompting for download even though
      // this is already the default on Chrome but for completeness
      prefs: {
        'download': {
          'prompt_for_download': false,
          'default_directory': __dirname + '/e2e/downloads'
        },
      },
    },
  },

  // Spec patterns are relative to the current working directly when
  // protractor is called.
  specs: ['e2e/specs/**/*.js'],
  directConnect: true,
  baseUrl: 'http://localhost:3000/index.html',  //use localHost if batching
   // baseUrl: 'https://analytics-staging.retailmanager.com',
  //baseUrl: 'https://analytics.retailmanager.com',
  // Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 2400000,
    //isVerbose: true
    print: function() {}
  },
  framework: 'jasmine2',
  //directConnect:true,
  allScriptsTimeout: 240000,
  getPageTimeout: 50000,
  onPrepare: () => utils.onPrepareHandler(jasmine),
  params: {
    //indirectNav: true
    downloadPath:  __dirname + '/e2e/downloads',
    localBrowser: true
  },
  //debugInfoEnabled for $compileProvider
  debugInfoEnabled:true,

  //batchEnabled
  batchEnabled:true
};
