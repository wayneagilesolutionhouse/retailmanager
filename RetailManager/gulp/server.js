#!/usr/bin/env node
'use strict';

const gulp = require('gulp');
const util = require('util');
const browserSync = require('browser-sync');
const middleware = require('./proxy');

let bs;

function createBrowserSyncServer(baseDir, extraOptions) {
  let options = {
      startPath: '/index.html',
      server: {
          baseDir: baseDir,
          middleware: middleware,
          routes: baseDir.toString().indexOf('src') === -1 ? null : {
            '/bower_components': 'bower_components'
          }
      },
      browser: 'default',
      ghostMode: false
  };

  Object.assign(options, extraOptions);

  bs = browserSync.init(options);
}

gulp.task('serve', ['watch'], () => {
  createBrowserSyncServer(['src', '.tmp'],{
    files: [
      'src/assets/images/**/*',
      'src/*.html',
      'src/{app,components}/**/*.html',
      'src/{app,components}/**/*.js',
      '!src/{app,components}/**/*.spec.js',
      {
        match: [
          '.tmp/*.css',
          '.tmp/{app,components}/**/*.css'
        ],
        fn: function(event, file) {
          // Hot load CSS etc
          bs.reload(file);
        }
      },
    ]
  });
});

gulp.task('serve:dist', ['build'], () => {
  createBrowserSyncServer('dist');
});

gulp.task('serve:relative-paths', ['build'], () => {
  createBrowserSyncServer('dist',{
    startPath: '/mall-app/dist/index.html',
    server: {
      baseDir: '../',
      directory:true
    }
  });
});

gulp.task('serve:e2e', () => {
  createBrowserSyncServer(
    ['src', '.tmp'], {
    notify: false,
    browser: []
  });
});

gulp.task('serve:e2e-dist', ['build', 'watch'], () => {
  createBrowserSyncServer(
    'dist', {
    notify: false,
    browser: []
  });
});
