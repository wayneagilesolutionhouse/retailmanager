'use strict';

describe('AccountCtrl', function() {

  var $scope;
  var AccountCtrl;
  var authServiceMock;

  beforeEach(module('shopperTrak'));
  beforeEach(inject(function($rootScope, $controller, $q) {
    $scope = $rootScope.$new();

    authServiceMock = {
      _isAuthenticated: true,
      isAuthenticated: function() {
        return authServiceMock._isAuthenticated;
      },
      logout: function() {
        authServiceMock._isAuthenticated = false;
        $rootScope.$broadcast('auth-logout-success');
      },
      changePassword: function() {
        var deferred = $q.defer();
        deferred.resolve();
        return deferred.promise;
      }
    };

    AccountCtrl = $controller('AccountCtrl', {
      '$scope': $scope,
      'authService': authServiceMock
    });
  }));
/*
  describe('validateForm', function() {

      it('should return error when password password is too short', function() {
        $scope.password = 'pass';
        $scope.passwordConfirmation = 'pass';
        $scope.validateForm();
        expect($scope.errorMessages.length >= 1).toBeTruthy();
      });

      it('should return error when password confirmation is not given', function() {
        $scope.password = 'password1234';
        $scope.passwordConfirmation = '';
        $scope.validateForm();
        expect($scope.errorMessages.length >= 1).toBeTruthy();
      });

      it('should return error when passwords don\'t match)', function() {
        $scope.password = 'password1234';
        $scope.passwordConfirmation = 'password123';
        $scope.validateForm();
        expect($scope.errorMessages.length >= 1).toBeTruthy();
      });

      it('should not return errors when password requirements are met', function() {
        $scope.password = 'password1234';
        $scope.passwordConfirmation = 'password1234';
        $scope.validateForm();
        expect($scope.errorMessages.length).toBe(0);
      });

  });

  describe('submitForm', function() {

    it('should verify that the controller provides information to the view about successful password change', function() {
      expect($scope.formHasBeenSubmitted).toBe(false);

      $scope.password = 'password1234';
      $scope.passwordConfirmation = 'password1234';
      $scope.submitForm();
      $scope.$apply();

      expect($scope.formHasBeenSubmitted).toBe(true);
    });

  });
*/
});
