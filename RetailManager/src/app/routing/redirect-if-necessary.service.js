(function () {
  'use strict';

  angular.module('shopperTrak.routing')
    .factory('redirectIfNecessary', redirectIfNecessaryFactory);

  redirectIfNecessaryFactory.$inject = [
    '$state',
    '$stateParams',
    '$timeout',
    'fillDefaultDateRangeParams',
    'getOrganization',
    'authService',
    'LocalizationService',
    'localStorageService',
    'ObjectUtils'
  ];

  function redirectIfNecessaryFactory(
    $state,
    $stateParams,
    $timeout,
    fillDefaultDateRangeParams,
    getOrganization,
    authService,
    LocalizationService,
    localStorageService,
    ObjectUtils
  ) {
    return redirectIfNecessary;

    function redirectIfNecessary(event, toState, toParams, fromState) {
      var strSelectedDateRanges = localStorageService.get('selectedDateRanges');
      if (useRedirect(toParams, toState, strSelectedDateRanges)) {

        getOrganization($stateParams.orgId).then(function (organization) {

          authService.getCurrentUser().then(function (user) {
            LocalizationService.getAllCalendars().then(function (calendars) {

              calendars = calendars.result;
              var newParams;

              if(!ObjectUtils.isNullOrUndefined(strSelectedDateRanges)) {
                  newParams = getDateParams(strSelectedDateRanges);
              } else {
                  newParams = fillDefaultDateRangeParams(toParams, organization, user, calendars, toState.name, fromState.name);
              }

              if (!angular.equals(newParams, toParams)) {
                event.preventDefault();
                // Call $state.go inside a $timeout to work around
                // buggy behavior otherwise.
                $timeout(function () {
                  // Clear cached calendarId when it's not needed anymore
                  localStorageService.set('currentCalendarId', null);
                  $state.go(toState.name, newParams);
                }, 500);
              }
            });
          });
        });
      }
    }

    function useRedirect(toParams, toState, strSelectedDateRanges) {
      if (toParams.dateRangeStart === undefined || toParams.dateRangeEnd === undefined ||
      toParams.compareRange1Start === undefined || toParams.compareRange1End === undefined ||
      toParams.compareRange2Start === undefined || toParams.compareRange2End === undefined) {
        return true;
      }

      if(!ObjectUtils.isNullOrUndefined(strSelectedDateRanges) && toState.name !== 'analytics.organization.site.real-time-data') {
        return true;
      }

      return false;
    }

    function getDateParams(selectedDateRanges) {
      var dateParams = {
        dateRangeStart: selectedDateRanges.current.start,
        dateRangeEnd: selectedDateRanges.current.end
      };

      if(selectedDateRanges.compare1.start !== undefined &&
        selectedDateRanges.compare1.end !== undefined &&
        selectedDateRanges.compare2.start !== undefined &&
        selectedDateRanges.compare2.end !== undefined) {
        dateParams.compareRange1Start = selectedDateRanges.compare1.start;
        dateParams.compareRange1End = selectedDateRanges.compare1.end;
        dateParams.compareRange2Start = selectedDateRanges.compare2.start;
        dateParams.compareRange2End = selectedDateRanges.compare1.end;
      }

      localStorageService.remove('selectedDateRanges');
      return dateParams;
    }
  }
})();
