(function () {
  'use strict';

  angular.module('shopperTrak.routing')
    .factory('getDefaultComparisonDateRangeParams', getDefaultComparisonDateRangeParamsFactory);

  getDefaultComparisonDateRangeParamsFactory.$inject = [
    'utils',
    'comparisons',
    'comparisonsHelper',
    '$stateParams',
    'authService',
    'LocalizationService'
  ];

  function getDefaultComparisonDateRangeParamsFactory(
    utils,
    comparisons,
    comparisonsHelper,
    $stateParams,
    authService,
    LocalizationService
  ) {
    return getDefaultComparisonDateRangeParams;

    function getDefaultComparisonDateRangeParams(dateRangeParams, user, organization, orgCalendars, singleComparePeriod) {
      if(angular.isUndefined(singleComparePeriod)) {
        singleComparePeriod = false;
      }

      LocalizationService.setUser(user);
      LocalizationService.setAllCalendars(orgCalendars);

      var params = {};
      var selectedPeriod = angular.copy(dateRangeParams);
      var firstWeekdaySetting = LocalizationService.getCurrentCalendarFirstDayOfWeek();

      if(singleComparePeriod === true) {
        var priorYear = {
          period_type: 'prior_year'
        };

        getDateparams(params, priorYear, 1, selectedPeriod, user, organization, firstWeekdaySetting, true);
      } else {
        getDateparams(params, user.preferences.custom_period_1, 1, selectedPeriod, user, organization, firstWeekdaySetting);
        getDateparams(params, user.preferences.custom_period_2, 2, selectedPeriod, user, organization, firstWeekdaySetting);
      }

      // If default compare ranges are outside calendar definitions for current calendar,
      // load period of same length from the start of calendar

      var startOfCalendar = LocalizationService.getStartOfCurrentCalendar();
      var lengthOfSelectedPeriod = selectedPeriod.dateRangeEnd.diff(selectedPeriod.dateRangeStart,'days');

      if(params.compareRange1Start < startOfCalendar) {
        params.compareRange1Start = angular.copy(startOfCalendar);
        params.compareRange1End = angular.copy(startOfCalendar).add(lengthOfSelectedPeriod,'days');
      }

      if(!singleComparePeriod) {
        if(params.compareRange2Start < startOfCalendar) {
          params.compareRange2Start = angular.copy(startOfCalendar);
          params.compareRange2End = angular.copy(startOfCalendar).add(lengthOfSelectedPeriod,'days');
        }
      }

      return params;
    }

    function getDateparams (params, period, index, selectedPeriod, user, organization, firstWeekdaySetting, singleComparePeriod) {
        var definedPeriod = _.findWhere(comparisons, { value: period.period_type });
        var rangeIndex = index;
        var newStart = '';
        var newEnd = '';
        var paramStartKey = 'compareRange' + rangeIndex + 'Start';
        var paramEndKey = 'compareRange' + rangeIndex + 'End';
        var dateRange = {
          start: selectedPeriod.dateRangeStart,
          end: selectedPeriod.dateRangeEnd
        };
        var range = {};
        switch (definedPeriod.id) {
          case comparisonsHelper.periodTypes.priorPeriod:
            range = utils.getPreviousCalendarPeriodDateRange(dateRange, user, organization);
            newStart = range.start;
            newEnd = range.end;
            break;
          case comparisonsHelper.periodTypes.priorYear:
            range = utils.getEquivalentPriorYearDateRange(dateRange, firstWeekdaySetting, user, organization, undefined, singleComparePeriod);
            newStart = range.start;
            newEnd = range.end;
            break;
          default:
            var weeksAgo = period.num_weeks;
            newStart = angular.copy(selectedPeriod.dateRangeStart).subtract(weeksAgo, 'weeks');
            newEnd = angular.copy(selectedPeriod.dateRangeEnd).subtract(weeksAgo, 'weeks');
            break;
        }
        params[paramStartKey] = newStart;
        params[paramEndKey] = newEnd;
      }
  }
})();
