(function() {
  'use strict';

  angular.module('shopperTrak.routing', [
    'ui.router',
    'shopperTrak.utils',
    'shopperTrak.auth',
    'shopperTrak.resources'
  ]);
})();
