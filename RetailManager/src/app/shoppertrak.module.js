(function () {
  'use strict';

  angular.module('shopperTrak', [
    'ngAnimate',
    'ngTouch',
    'ngSanitize',
    'ui.router',
    'shopperTrak.config',
    'shopperTrak.routing',
    'shopperTrak.auth',
    'shopperTrak.resources',
    'shopperTrak.filters',
    'shopperTrak.obfuscation',
    'shopperTrak.widgets',
    'shopperTrak.locationSelector',
    'shopperTrak.zoneSelector',
    'shopperTrak.siteSelector',
    'shopperTrak.validators',
    'shopperTrak.constants',
    'shopperTrak.features',
    'shopperTrak.modals',
    //'shopperTrak.profiling',
    'shopperTrak.httpRequests',
    'angularMoment',
    'mgcrea.ngStrap',
    'offClick',
    'shopperTrak.utils',
    'angular-underscore',
    'LocalStorageModule',
    'ui.sortable',
    'jcs.angular-http-batch',
    'perfect_scrollbar',
    'pascalprecht.translate',
    'data-table',
    'ngFileUpload',
    'ui.bootstrap',
    window.angularDragula(angular),
    'slugifier'
  ])
  .config([
    'httpBatchConfigProvider',
    'apiUrl',
    '$compileProvider',
    '$qProvider',
    '$locationProvider',
    'debugInfoEnabled',
    'batchEnabled',
  function config(
    httpBatchConfigProvider,
    apiUrl,
    $compileProvider,
    $qProvider,
    $locationProvider,
    debugInfoEnabled,
    batchEnabled
  ) {

    $compileProvider.debugInfoEnabled(debugInfoEnabled);
    $compileProvider.preAssignBindingsEnabled(true);
    $qProvider.errorOnUnhandledRejections(false);
    $locationProvider.hashPrefix('');
    if(batchEnabled === true) configureBatching(httpBatchConfigProvider, apiUrl);
  }
  ]);

  function configureBatching(httpBatchConfigProvider, apiUrl) {
    httpBatchConfigProvider.setAllowedBatchEndpoint(
      apiUrl.substr(0, apiUrl.indexOf('/api/v1')),
      apiUrl + '/batch',
      {
        batchRequestCollectionDelay: 5,
        maxBatchedRequestPerCall: 6,
        ignoredVerbs: ['head'], // This array is pointless. The getBatchableRequests function overrides it
        canBatchRequest: getBatchableRequests,
        adapter: 'nodeJsMultiFetchAdapter'
      }
    );
  }

  function getBatchableRequests(url, method) {
    if(method !== 'GET') {
      return false;
    }

    return url.indexOf('source=pdfExport') < 0 &&
      url.indexOf('noBatch=true') < 0 &&
      url.indexOf('/batch') < 0 &&
      url.indexOf('/pdf') < 0 &&
      url.indexOf('/sites') < 0;
  }

})();
