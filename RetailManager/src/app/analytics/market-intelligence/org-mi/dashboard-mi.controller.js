(function () {
  'use strict';

  angular.module('shopperTrak').controller('DashboardMiController', DashboardMiController);

  DashboardMiController.$inject = [
    '$scope',
    '$q',
    '$rootScope',
    '$state',
    '$stateParams',
    'currentOrganization',
    'ObjectUtils',
    'authService',
    'utils',
    'LocalizationService',
    'SubscriptionsService',
    'miUserPreferences',
    'ExportService',
    'marketIntelligenceService',
    'MarketIntelligenceGeographyResource',
    'MarketIntelligenceCategoryResource',
    'MarketIntelligenceSubscriptionResource',
    'widgetConstants'
  ];

  function DashboardMiController(
    $scope,
    $q,
    $rootScope,
    $state,
    $stateParams,
    currentOrganization,
    ObjectUtils,
    authService,
    utils,
    LocalizationService,
    SubscriptionsService,
    miUserPreferences,
    ExportService,
    marketIntelligenceService,
    MarketIntelligenceGeographyResource,
    MarketIntelligenceCategoryResource,
    MarketIntelligenceSubscriptionResource,
    widgetConstants
  ) {
    var vm = this;

    activate();

    function activate() {
      vm.isLoading = true;

      getCoreData().then(function (res) {
        vm.miAvailable = true;
        vm.rules = [{name: 'Contains'}];
        vm.geography = res[0];
        vm.category = res[1];
        vm.subscription = res[2];
        vm.currentUser = res[3];

        if(ObjectUtils.isNullOrUndefinedOrEmpty(vm.subscription)) {
          showError('No Market Intelligence subscriptions found for orgId: ' + $stateParams.orgId.toString());
          return;
        }

        initScope();

        checkIfMiExists(vm.currentUser);
      })
      .catch(showError);
    }

    function checkIfMiExists(currentUser) {
      vm.miExists = miUserPreferences.segmentPreferencesAreConfigured(currentUser.preferences);

      if (!vm.miExists) {
        vm.firstTimeConfigure = true;
        defaultSelectedSegments();
        defaultSaveSegments();
      } else {
        loadConfiguredMi()
      }
    }

    function loadConfiguredMi() {
      try {
        vm.firstTimeConfigure = false;
        vm.numberFormatName = LocalizationService.getCurrentNumberFormatName(vm.currentUser, currentOrganization);
        vm.dateFormat = LocalizationService.getCurrentDateFormat(currentOrganization);
        getMiData();
        buildSubscriptionLists();
        setDefaultSegment();
        configureWatches();
        vm.showOrgIndex = SubscriptionsService.hasMiOrgIndex(vm.currentUser, currentOrganization);
        vm.isLoading = false;
      } catch(error) {
        showError(error);
      }
    }

    function showError(error) {
      console.error(error);
      vm.isLoading = false;
      vm.miAvailable = false;
    }

    function getCoreData() {
      var deferred = $q.defer();
      $q.all([
        MarketIntelligenceGeographyResource.query().$promise,
        MarketIntelligenceCategoryResource.query().$promise,
        MarketIntelligenceSubscriptionResource.query({
          orgId: $stateParams.orgId
        }).$promise,
        authService.getCurrentUser()
      ])
      .then(function (data) {
        deferred.resolve(data);
      })
      .catch(function(error) {
        deferred.reject(error);
      });
      return deferred.promise;
    }

    function createRegionsArray(passedArray) {
      var defaultArray = ['US', 'Northeast', 'Midwest', 'West', 'South'];
      var regionsArray = [];
      _.each(defaultArray,function (item) {
        regionsArray.push(_.findWhere(passedArray,{name:item}))
      });

      return regionsArray;
    }

    function defaultSelectedSegments () {
      var geoArray = [], catArray = [], countryArray = [], USuuid = '';
      vm.defaultSelectedSegmentsArray = [];
      _.each(vm.subscription, function (item) {
        geoArray.push(item.geography);
        catArray.push(item.category);
        if (item.geography.name === 'US') { // ToDo: Fix this for other countries
          USuuid = item.geography.uuid;
          countryArray.push(item.geography);
        }
      });

      var tempRegionsArray =  _.uniq(_.where(countryArray, {name:'US'}), 'name').concat(_.uniq(_.where(geoArray, {parentUuid: USuuid, geoType:'REGION'}), 'name'));

      var regionsArray = createRegionsArray(tempRegionsArray) ;

      var totalRetailCategory = _.findWhere(catArray, {name: 'Total Retail'});

      for (var i = 0; i < 5; i++) {
        var subs = {
          geography: {
            orgId: currentOrganization.organization_id,
            name: changeCase(regionsArray[i].geoType),
            rule: 'Contains',
            value: {
              name: regionsArray[i].name
            }
          },
          category: {
            orgId: currentOrganization.organization_id,
            name: 'Category',
            rule: 'Contains',
            value: {
              name: totalRetailCategory.name
            }
          }
        };

        subs.geography.value.src = regionsArray[i];
        subs.category.value.src = totalRetailCategory;
        vm.defaultSelectedSegmentsArray.push({subscription: subs});
      }
    }

    function defaultSaveSegments() {
      marketIntelligenceService.saveUserMarketIntelligence(vm.currentUser, vm.defaultSelectedSegmentsArray)
      .then(function () {
        vm.miExists = miUserPreferences.segmentPreferencesAreConfigured(vm.currentUser.preferences);
        vm.firstTimeConfigure = false;
        $scope.$emit('setMiExists');
        loadConfiguredMi();
      })
      .catch(showError);
    }

    function changeCase(string) {
      return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
    }

    function buildSubscriptionLists() {
      var createObj = {
        'Category': {
          values: {}
        }
      };

      vm.subscription.forEach(function (item) {
        createObj[item.geography.geoType] = createObj[item.geography.geoType] || {values: {}};
        createObj[item.geography.geoType].values[item.geography.name] = {
          uuid: item.geography.uuid,
          sourceObj: item.geography
        };

        createObj['Category'].values[item.category.name] = {uuid: item.category.uuid, sourceObj: item.category};
      });

      vm.flatSubscription = Object.keys(createObj).map(function (item) {
        return {
          name: changeCase(item),
          values: Object.keys(createObj[item].values).map(function (val) {
            return {
              name: val,
              src: createObj[item].values[val].sourceObj
            };
          })
        }
      });
    }

    function findSubscriptionObject(type, uuid) {
      var containerObject = _.find(vm.subscription, function(subscription) {
        return subscription[type].uuid === uuid;
      });

      return containerObject[type];
    }

    function setDefaultSegment() {
      var totalRetailUuid = 'da815dbc-f066-4807-a6ac-ae145e6b6242';
      var usUuid = '6d1175b8-eb06-45ee-9d11-9a63c072f728';

      var totalRetailCategory = findSubscriptionObject('category', totalRetailUuid);
      var usGeography = findSubscriptionObject('geography', usUuid);

      vm.defaultSegment = {
        subscription: {
          category: {
            value: {
              src: totalRetailCategory,
              name: 'Total Retail'
            },
            rule: 'Contains',
            name: 'Category'
          },
          geography: {
            value: {
              src: usGeography,
              name: 'US'
            },
            rule: 'Contains',
            name: 'Country'
          },
            orgId: currentOrganization.organization_id // ToDo: Fix this
        }

      };
    }

    function initScope() {
      vm.currentOrganization = currentOrganization;
      vm.dashboardMi = true;
      vm.metricsToShow = ['segment','market-intelligence'];
      vm.selectedAdvanceOption = [];
      vm.updatedOptions = {};
      vm.trendAnalysisPanelArray = [];
      vm.viewData = {};
      vm.isLoading = {};
      vm.exportWidget = exportWidget;
      vm.widgetIsExported = widgetIsExported;

      vm.periodSelected = {
        start: $stateParams.dateRangeStart,
        end: $stateParams.dateRangeEnd
      };

      vm.periodCompared = {
        start: $stateParams.compareRange1Start,
        end: $stateParams.compareRange1End
      };

      vm.isPriorYear = isPriorYear(vm.periodSelected, vm.periodCompared);
      vm.dateRangesLoaded = dateRangesLoaded();
    }

    function exportAllWidgets() {
      exportWidget('segment');
      $state.go('pdfexport', { orgId: vm.currentOrganization.organization_id, view: 'schedule' });
    }

    function exportWidget(widgetType) {
      initializeViewData();
      var params = buildWidgetConfig(widgetType);

      ExportService.createExportAndStore(params);
    }

    function buildWidgetConfig(widgetType) {
      var dateRange = { start: $stateParams.dateRangeStart, end: $stateParams.dateRangeEnd };
      var compareRange1 = { start: $stateParams.compareRange1Start, end: $stateParams.compareRange1End };

      var widgetConfig = {
        orgId: currentOrganization.organization_id,
        dateRange: dateRange,
        compare1Range: compareRange1,
        compare2Range: { },
        compare1Type: utils.getCompareType(compareRange1, dateRange, vm.currentUser, vm.currentOrganization),
        compare2Type: { },
        numberFormat: vm.numberFormatName,
        name: widgetType,
        hideCompare2Range: true,
        dateFormat: vm.dateFormat,
        showOrgIndex: vm.showOrgIndex
      };

      if(!ObjectUtils.isNullOrUndefined(vm.params)) {
        widgetConfig.segments =_.map(vm.params.subscriptions, function(segment) {
          return {
            positionIndex: segment.positionIndex,
            category: {
              uuid: segment.category.uuid
            },
            geography: {
              uuid: segment.geography.uuid
            }
          };
        });
      }

      return widgetConfig;
    }

    function widgetIsExported (widgetType, params) {

      var dateRangeKey =
        params.dateRange.start +
        ' - ' +
        params.dateRange.end +
        ' - ' +
        params.compare1Range.start +
        ' - ' +
        params.compare1Range.end +
        ' - undefined - undefined'; // MI doesn't use second compare range

      var paramsToCompare = widgetVariables()[widgetType];

      return ExportService.isInExportCartWithSettings(getAreaKey(), dateRangeKey, widgetType, params, paramsToCompare);
    }

    function getAreaKey() {
      return currentOrganization.organization_id+'_-1';
    }

    function configureWatches() {
      var unbinds = [];

      unbinds.push($scope.$watch('vm.selectedSegment', function(selectedSegment) {
        if(ObjectUtils.isNullOrUndefinedOrEmptyObject(selectedSegment)) {
          vm.updatedOptions.uuid = vm.defaultSegment.subscription.geography.value.src.uuid;
          vm.updatedOptions.lastUpdated = 0;
          vm.updatedOptions.orgId = vm.currentOrganization.organization_id;
          vm.updatedOptions.category = vm.defaultSegment.subscription.category.value.src;
        } else {
          vm.updatedOptions.uuid = selectedSegment.subscription.geography.value.src.uuid;
          vm.updatedOptions.lastUpdated = 0;
          vm.updatedOptions.orgId = vm.currentOrganization.organization_id;
          vm.updatedOptions.category = selectedSegment.subscription.category.value.src;
        }

        getUpdatedAdvanceOptions();
      }));

      unbinds.push($scope.$on('scheduleExportCurrentViewToPdf', exportAllWidgets));

      unbinds.push(
        $scope.metricsChange = $scope.$watchGroup([
          'vm.metricsToShow',
          'vm.dateRangesLoaded'
        ], function() {
          if(viewIsLoaded()) {
            vm.viewData = initializeViewData();
          }
        }));

      $scope.$on('$destroy', function() {
        _.each(unbinds, function(unbind) {
          if(angular.isFunction(unbind)) {
            unbind();
          }
        });
      });
    }

    function getUpdatedAdvanceOptions() {
      vm.selectedOptions = checkPriorYear(vm.isPriorYear, vm.updatedOptions);
    }

    function getMiData() {
      var configuredSegments = miUserPreferences.getConfiguredSegments(vm.currentUser.preferences);

      var subs = configuredSegments.map(function (item) {
        return {
          positionIndex: item.positionIndex,
          uuid: '54c454be-f729-48f4-8ffd-5285b9c4103d',
          lastUpdated: 0,
          orgId: vm.currentOrganization.organization_id,
          category: item.subscription.category.value.src,
          geography: item.subscription.geography.value.src
        };
      });

      vm.params = checkPriorYear(vm.isPriorYear, subs);
    }

    function firstDaySetting() {
      return LocalizationService.getCurrentCalendarFirstDayOfWeek();
    }

    function isPriorYear(startPeriod, comparePeriod) {
      return utils.dateRangeIsPriorYear(startPeriod, comparePeriod, firstDaySetting(), vm.currentUser, vm.currentOrganization);
    }

    function checkPriorYear(priorYear, subsObject) {
      if (LocalizationService.isCurrentCalendarGregorian() && priorYear) {
        return {
          dateStart: $stateParams.dateRangeStart,
          dateEnd: $stateParams.dateRangeEnd,
          compareStart: $stateParams.compareRange1Start,
          compareEnd: $stateParams.compareRange1End,
          subscriptions: subsObject
        };
      }
      if (!priorYear) {
        return {
          dateStart: $stateParams.dateRangeStart,
          dateEnd: $stateParams.dateRangeEnd,
          compareStart: $stateParams.compareRange1Start,
          compareEnd: $stateParams.compareRange1End,
          subscriptions: subsObject
        };
      }
      return {
        dateStart: $stateParams.dateRangeStart,
        dateEnd: $stateParams.dateRangeEnd,
        subscriptions: subsObject
      };
    }

    function initializeViewData() {
      var configuration = {};

      _.each(vm.metricsToShow, function(metricKey) {
        configuration[metricKey] = buildWidgetConfig(metricKey);
      });

      return configuration;
    }

    function widgetVariables() {
      return widgetConstants.exportProperties;
    }

    function viewIsLoaded() {
      return !ObjectUtils.isNullOrUndefinedOrEmpty(vm.metricsToShow) && vm.dateRangesLoaded;
    }

    function dateRangesLoaded() {
      return utils.urlDateParamsLoaded($stateParams, true);
    }

  }
})();
