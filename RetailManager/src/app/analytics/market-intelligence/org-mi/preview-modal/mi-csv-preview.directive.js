(function(){
  'use strict';

  angular.module('shopperTrak')
  .directive('miCsv', function miCsv() {
    return {
      restrict: 'E',
      templateUrl: 'app/analytics/market-intelligence/org-mi/preview-modal/mi-csv-preview.partial.html',
      bindToController: true,
      controller: 'miCsvController',
      controllerAs: 'vm',
      scope: {}
    };
  })
  
})();