(function () {
  'use strict';

  angular.module('shopperTrak').controller('miCsvController', miCsvController);

  miCsvController.$inject = [
    'ObjectUtils',
    'MarketIntelligenceSubscriptionResource',
    '$scope',
    '$q',
    '$stateParams',
    '$translate'
  ];

  function miCsvController(ObjectUtils,
                           MarketIntelligenceSubscriptionResource,
                           $scope,
                           $q,
                           $stateParams,
                           $translate) {
    var vm = this;

    activate();

    function activate() {
      vm.isLoading = true;
      initScope();
      getTranslations();

      loadSubscriptions()
        .then(subscriptionCategoriesArray)
        .catch(showError);
    }

    function initScope() {
      loadDates();
      vm.selectedUuid = null;
      vm.setSelected = setSelected;
    }

    function setSelected(selectedUuid) {
      vm.selectedUuid = selectedUuid;
    }

    function subscriptionCategoriesArray(passedSubsArray) {
      vm.subscribedCategories = [];
      _.each(passedSubsArray, function (item) {
        vm.subscribedCategories.push({uuid: item.category.uuid, name: item.category.name})
      });
      vm.subscribedCategories = _.uniq(vm.subscribedCategories, 'name');
      vm.isLoading = false;
      vm.hasError = false;
    }

    function loadSubscriptions() {
      return MarketIntelligenceSubscriptionResource.query({
          orgId: $stateParams.orgId
        }).$promise;
    }

    function getTranslations() {
      $translate('marketIntelligence.CSV.ENTERREPORTNAME')
        .then(function(translation) {
          vm.reportNamePlaceholder = translation
        });
    }

    function loadDates() {
      vm.periodSelected = {
        start: $stateParams.dateRangeStart,
        end: $stateParams.dateRangeEnd
      };
      vm.periodCompared = {
        start: $stateParams.compareRange1Start,
        end: $stateParams.compareRange1End
      };
    }

    function showError(error) {
      vm.isLoading = false;
      vm.showError = true;
      console.error(error);
    }

  }
})();
