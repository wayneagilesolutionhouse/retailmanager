(function () {
  'use strict';

  angular.module('shopperTrak')
  .factory('marketIntelligenceService', marketIntelligenceService);

  marketIntelligenceService.$inject = [
    'apiUrl',
    '$http',
    '$q',
    'authService',
    'LocalizationService',
    'ObjectUtils',
    'dateRangeHelper',
    'utils'
  ];

  function marketIntelligenceService(apiUrl, $http, $q, authService, LocalizationService, ObjectUtils, dateRangeHelper, utils) {

  /**  Saves the user's segment widget configuration
    * 
    *  @param {object} user the user to update
    *  @param {object} customSegments the customised segments to be saved in the preferences.market_intelligence prop
    *  @returns {object} an empty promise
    **/
    function saveUserMarketIntelligence(user, customSegments) {
      var deferred = $q.defer();

      $http.put(apiUrl + '/users/' + user._id, getUserParams(user, customSegments))
        .then(function (result) {
          user = result.data.result[0];
          authService.updateMarketIntelligence(user.preferences.market_intelligence);
          deferred.resolve(user.preferences.market_intelligence);
        })
        .catch(function(error) {
          deferred.reject(error);
        });
      return deferred.promise;
    }

  /**  Gets the index data for the market intelligence charts and 5 segment widget.=
    *  Hits the index and org index endpoint if it needs to.
    * 
    *  @param {object} paramsObject The params object to be sent to the MI API
    *  @param {boolean} includeOrgIndex if set to true, will attempt to fetch the org index
    *  @returns {object} a promise containing the data
    **/
    function getIndexData(paramsObject, includeOrgIndex) {
      var defer = $q.defer();

      var endPointsArray = ['mi/index'];

      if(includeOrgIndex === true) {
        endPointsArray.push('mi/index/org');
      }

      var indexList = _.map(endPointsArray,function (queryUri) {
        return $http({
          method: 'POST',
          url: apiUrl + '/' + queryUri,
          data: paramsObject
        });
      });

      $q.all(indexList)
        .then(function (results) {
          var resultObj = {};
          resultObj.index = results[0].data;

          if(includeOrgIndex) {
            resultObj.org = results[1].data;
          }

          if(datasetContainsNoData(results)) {
            defer.reject('No Data');
            return;
          }

          defer.resolve(resultObj)
        }, function (err) {
          defer.reject(err);
        });

      return defer.promise;
    }

  /**  Gets the mi subscription data for an org
    * 
    *  @param {number} orgId The org to get the MI subscription data for
    *  @returns {object} a promise containing the data
    **/
    function getSubscriptions(orgId) {
      var url = apiUrl + '/mi/subscription?orgId=' + orgId.toString();

      return $http.get(url);
    }

  /**  Gets the data ranges for the specified period
    *  E.g. will find the start and end dates for all weeks within a specified range.
    *  Gregorian calculations are done on the front end and are not requested from the API
    * 
    *  @param {number} calendarId The calendarId to check
    *  @param {string} selectedPeriod One of "day", "week", "month", "quarter", "year", or "periodToDate"
    *  @param {object} startDate a momentJs object representing the start date
    *  @param {endDate} startDate a momentJs object representing the end date
    *  @param {object} currentUser The current user. Optional. Only required for periodToDate checks within gregorian calendars
    *  @param {object} currentOrg The current org. Optional. Only required for periodToDate checks within gregorian calendars
    *  @returns {object} a promise containing the date ranges
    **/
    function getCalendarDateRanges(calendarId, selectedPeriod, startDate, endDate, type, currentUser, currentOrg) {
      var defer = $q.defer();

      var requestCalendarId = getRequestCalendarId(calendarId);

      if(requestCalendarId === LocalizationService.getStandardMonthlyCalendarId() && selectedPeriod === 'periodToDate') {
        return getPeriodToDateRanges(calendarId, endDate, type, currentUser, currentOrg);
      }

      var calendarEndPoint = getCalendarEndpoint(requestCalendarId, selectedPeriod, startDate, endDate);

      $http.get(calendarEndPoint).then(function (result) {
        if(LocalizationService.isCalendarIdGregorianMonday(calendarId)) {
          adjustRangesByOneDay(result.data);
        }

        defer.resolve(result);
      });

      return defer.promise;
    }

  /**  Returns the api endpoint to get the calendar dates and build the GET request params
    *  This will always return the groupByDateRanges, unless the selected period is 'periodToDate', 
    *  in which case it will return groupByPeriodToDate.
    *  Private function
    * 
    *  @param {number} calendarId the current CalendarId
    *  @param {string} selectedPeriod the selected period. Can be 'day', 'week', 'month', 'quarter', 'year' or 'periodToDate'
    *  @param {object} startDate the start date. A momentJs object
    *  @param {object} endDate the end date. A momentJs object
    *  @returns {string} The request URL
    **/
    function getCalendarEndpoint(calendarId, selectedPeriod, startDate, endDate) {
      var calendarEndPoint = apiUrl + '/calendars/' + calendarId + '/mi/';

      if(selectedPeriod === 'periodToDate') {
        calendarEndPoint += 'groupByPeriodToDate?';
      } else {
        calendarEndPoint += 'groupByDateRanges?groupBy=' + selectedPeriod + '&';
      }

      calendarEndPoint += 'dateStart=' + startDate.toISOString() + '&dateEnd=' +  endDate.toISOString();
      
      return calendarEndPoint;
    }

    function adjustRangesByOneDay(data) {
      _.each(data.result, function(grouping, index) {
        if(index !== 0) {
          grouping.start = addDay(grouping.start);
        }

        grouping.end = addDay(grouping.end);
      });
    }

    function addDay(isoDateString) {
      return moment(isoDateString).add(1, 'days').toISOString();
    }

  /**  Evaluates and returns the correct calendarId
    *  Prevents the calendarIds that the API can't understand from being sent to the API
    *  Private function
    * 
    *  @param {number} calendarId the current CalendarId
    *  @returns {number} The calendarId to send to the API
    **/
    function getRequestCalendarId(calendarId) {
      var requestCalendarId = calendarId;

      if(LocalizationService.isCalendarIdGregorianMonday(calendarId)) {
        requestCalendarId = LocalizationService.getStandardMonthlyCalendarId();
      }

      if(LocalizationService.isCalendarIdGregorianSunday(calendarId)) {
        requestCalendarId = LocalizationService.getStandardMonthlyCalendarId();
      }

      return requestCalendarId;
    }

  /**  Gets the ranges for period to date ranges for gregorian calendars
    *  Private function
    * 
    *  @param {number} calendarId the current CalendarId
    *  @param {object} endDate The end date. A momentJs object
    *  @param {string} type The type of comparison
    *  @param {object} currentUser The current user
    *  @param {object} currentOrg The current org
    *  @returns {object} A promise containing a date range 
    **/
    function getPeriodToDateRanges(calendarId, endDate, type, currentUser, currentOrg) {
      var data = {
        result : [{}]
      };

      data.result[0].year = dateRangeHelper.getYTD(endDate);
      data.result[0].quarter = dateRangeHelper.getQTD(endDate);
      data.result[0].month = dateRangeHelper.getMTD(endDate);
      data.result[0].week = dateRangeHelper.getWTD(endDate);

      if(type === 'compare') {
        setPriorYear(data.result[0].year, endDate, 'ytd', currentUser, currentOrg);
        setPriorYear(data.result[0].quarter, endDate, 'qtd', currentUser, currentOrg);
        setPriorYear(data.result[0].month, endDate, 'mtd', currentUser, currentOrg);
        setPriorYear(data.result[0].week, endDate, 'wtd', currentUser, currentOrg);
      } else {
        data.result[0].year.end = endDate;
        data.result[0].quarter.end = endDate;
        data.result[0].month.end = endDate;
        data.result[0].week.end = endDate;
      }

      setRangeToISODates(data.result[0].year);
      setRangeToISODates(data.result[0].quarter);
      setRangeToISODates(data.result[0].month);
      setRangeToISODates(data.result[0].week);

      var defer = $q.defer();

      defer.resolve({data: data});

      return defer.promise;
    }

  /**  Gets the equivalent prior year dates for a specified range. Works with Gregorian Calendars only.
    *  Does not return anything. Instead directly operates on the range object passed in
    *  Private function
    * 
    *  @param {object} range a range object. Contain two properties, both momentJs objects - start and end
    *  @param {object} endDate The end date. A momentJs object 
    **/
    function setPriorYear(range, endDate, shortcut, user, organization) {
      range.end = endDate;

      var firstDayOfWeekSetting = LocalizationService.getCurrentCalendarFirstDayOfWeek();

      var priorYear = utils.getEquivalentPriorYearDateRange(range, firstDayOfWeekSetting, user, organization, shortcut, true);

      range.start = moment(priorYear.start);
      range.end = moment(priorYear.end);
    }

  /**  Converts the date ranges to ISO date strings from momentJs objects
    *  Does not return anything. Instead directly operates on the range object passed in
    *  Private function.
    * 
    *  @param {object} range a range object. Contain two properties, both momentJs objects - start and end
    **/
    function setRangeToISODates(range) {
      range.start = range.start.toISOString();
      range.end = range.end.toISOString();
    }

  /**  Updates the user object with the specified market intelligence segments
    *  Private function
    * 
    *  @param {object} user The current user
    *  @param {object} segments The market intelligence segments to apply to the passed in user object
    **/
    function getUserParams(user, segments) {
      var params = {
        preferences: angular.copy(user.preferences)
      };
      if(_.has(params.preferences.market_intelligence,'segments')){
        params.preferences.market_intelligence.segments = segments;
      }
      return params;
    }

  /**  Checks to see if the returned data does not contain any useful values
    *  Private function
    * 
    *  @param {object[]} results array from the market intelligence API
    *  @returns {boolean} A boolean
    **/
    function datasetContainsNoData(results) {
      var resultsWithNoData = [];
      var totalDatasets = 0;

      _.each(results, function(result) {
        var dataset = result.data;

        _.each(dataset, function (item) {
          totalDatasets += 1;
          if (!ObjectUtils.isNullOrUndefined(item.errorMessage)) {
            resultsWithNoData.push(item);
          }
        });
      });

      return resultsWithNoData.length === totalDatasets;
    }

    return {
      saveUserMarketIntelligence: saveUserMarketIntelligence,
      getIndexData: getIndexData,
      getCalendarDateRanges: getCalendarDateRanges,
      getSubscriptions: getSubscriptions
    };
  }
})();
