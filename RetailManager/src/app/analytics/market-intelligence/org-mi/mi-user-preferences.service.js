(function() {
  'use strict';

  angular.module('shopperTrak')
  .service('miUserPreferences', [
    'ObjectUtils',
    function (
      ObjectUtils
    ) {

      function segmentPreferencesAreConfigured(userPreferences) {
        if(ObjectUtils.isNullOrUndefined(userPreferences.market_intelligence)) {
          return false;
        }

        if(ObjectUtils.isNullOrUndefinedOrEmpty(userPreferences.market_intelligence.segments)) {
          return false;
        }

        var configuredSegments = getConfiguredSegments(userPreferences);

        if(configuredSegments.length === 0) {
          return false;
        }

        return true;
      }

      function getConfiguredSegments(userPreferences) {
        var userSegments = angular.copy(userPreferences.market_intelligence.segments);

        var segmentsWithPositions = _.map(userSegments, function(segment, i) {
          if (!ObjectUtils.isNullOrUndefined(segment)) {
            segment.positionIndex = i;
          }

          return segment;
        });

        return _.filter(segmentsWithPositions, function(segment) {
          return !ObjectUtils.isNullOrUndefined(segment) &&
                  !ObjectUtils.isNullOrUndefinedOrEmptyObject(segment.subscription);
        });
      }

      return {
        segmentPreferencesAreConfigured: segmentPreferencesAreConfigured,
        getConfiguredSegments: getConfiguredSegments
      };
    }]);
})();
