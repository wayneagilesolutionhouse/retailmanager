'use strict';

describe('MarketIntelligenceService', function () {

  var marketIntelligenceService;
  var authService;
  var dateRangeHelper;
  var localizationService;
  var utils;

  var $httpBackend;
  var $timeout;

  var apiUrl = 'https://api.url';

  beforeEach(module('shopperTrak'));
  beforeEach(module('shopperTrak.auth'));

  beforeEach(module(function ($provide) {
    $provide.constant('apiUrl', apiUrl);
  }));

  beforeEach(inject(function (_$httpBackend_,  _$timeout_, _authService_, _dateRangeHelper_, _LocalizationService_, _utils_, _marketIntelligenceService_) {
    $httpBackend = _$httpBackend_;
    $timeout =  _$timeout_;
    authService = _authService_;
    dateRangeHelper = _dateRangeHelper_;
    localizationService = _LocalizationService_;
    utils = _utils_;
    marketIntelligenceService = _marketIntelligenceService_;
  }));

  describe('saveUserMarketIntelligence', function() {
    var user;

    beforeEach(function() {
      user = {
        _id: 101,
        preferences: {
          market_intelligence: {
            segments: []
          }
        }
      };
    });

    it('should be exposed', function() {
      expect(typeof marketIntelligenceService.saveUserMarketIntelligence).toBe('function');
    });

    it('should make a PUT request to update the user', function() {
      var expectedUrl = apiUrl + '/users/' + user._id;

      var expectedRequestBody = {
        preferences: user.preferences
      };

      $httpBackend.expectPUT(expectedUrl, expectedRequestBody).respond(200, {});

      marketIntelligenceService.saveUserMarketIntelligence(user, []);

      $httpBackend.flush();

      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should update the market intelligence segments on the user object', function() {
      var expectedUrl = apiUrl + '/users/' + user._id;

      var segments = [{
          name: 'segment 1'
        }, {
          name: 'segment 2'
        }
      ];

      var expectedRequestBody = {
        preferences: {
          market_intelligence: {
            segments: segments
          }
        }
      };

      $httpBackend.expectPUT(expectedUrl, expectedRequestBody).respond(200, {});

      marketIntelligenceService.saveUserMarketIntelligence(user, segments);

      $httpBackend.flush();

      $httpBackend.verifyNoOutstandingRequest();   
    });

    it('should update the auth service with the result of the request', function() {
      var expectedUrl = apiUrl + '/users/' + user._id;

      var segments = [{
          name: 'segment 1'
        }, {
          name: 'segment 2'
        }
      ];

      var expectedRequestBody = {
        preferences: {
          market_intelligence: {
            segments: segments
          }
        }
      };

      var mockResponse = {
        result: [{
            preferences: {
              market_intelligence: {
                segments: segments
              }
            }
          }]
      };

      $httpBackend.expectPUT(expectedUrl, expectedRequestBody).respond(200, mockResponse);
      
      spyOn(authService, 'updateMarketIntelligence');

      marketIntelligenceService.saveUserMarketIntelligence(user, segments);

      $httpBackend.flush();

      $httpBackend.verifyNoOutstandingRequest();

      expect(authService.updateMarketIntelligence).toHaveBeenCalledWith(mockResponse.result[0].preferences.market_intelligence);
    });
  });

  describe('getIndexData', function() {
    it('should be exposed', function() {
      expect(typeof marketIntelligenceService.getIndexData).toBe('function');
    });

    it('should hit the index endpoint', function() {
      var expectedUrl = apiUrl + '/mi/index';

      var paramsObj = {};

      $httpBackend.expectPOST(expectedUrl, paramsObj).respond(200, {});

      marketIntelligenceService.getIndexData(paramsObj);

      $httpBackend.flush();
    });

    it('should hit the org index endpoint', function() {
      var expectedUrl = apiUrl + '/mi/index/org';

      var paramsObj = {};

      $httpBackend.expectPOST(expectedUrl, paramsObj).respond(200, {});

      marketIntelligenceService.getIndexData(paramsObj, true);

      $httpBackend.flush();
    });

    it('should reject the promise if the index returns no data', function() {
      var expectedUrlIndex = apiUrl + '/mi/index';
      var expectedUrlOrg = apiUrl + '/mi/index/org';

      var paramsObj = {};

      var mockResponse = [
        { errorMessage: 'oops' }
      ];

      $httpBackend.expectPOST(expectedUrlIndex, paramsObj).respond(200, mockResponse);

      $httpBackend.expectPOST(expectedUrlOrg, paramsObj).respond(200, mockResponse);

      marketIntelligenceService.getIndexData(paramsObj, true)
        .then(function() {
          throw 'This shouldn\'t have been reached';
        })
        .catch(function(result) {
          expect(result).toBe('No Data');
        });

      $httpBackend.flush();
      $timeout.flush();
    });

    it('should return the index data', function() {
      var expectedUrlIndex = apiUrl + '/mi/index';
      var expectedUrlOrg = apiUrl + '/mi/index/org';

      var paramsObj = {};

      var mockResponseIndex = [
        { someValue: 1 }
      ];

      var mockResponseOrg = [
        { someValue: 2 }
      ];

      $httpBackend.expectPOST(expectedUrlIndex, paramsObj).respond(200, mockResponseIndex);

      $httpBackend.expectPOST(expectedUrlOrg, paramsObj).respond(200, mockResponseOrg);

      marketIntelligenceService.getIndexData(paramsObj, true)
        .then(function(result) {
          expect(result.index[0].someValue).toBe(1);
        })
        .catch(function() {
          throw 'This shouldn\'t have been reached';
        });

      $httpBackend.flush();
      $timeout.flush();
    });

    it('should return the org index data', function() {
      var expectedUrlIndex = apiUrl + '/mi/index';
      var expectedUrlOrg = apiUrl + '/mi/index/org';

      var paramsObj = {};

      var mockResponseIndex = [
        { someValue: 1 }
      ];

      var mockResponseOrg = [
        { someValue: 2 }
      ];

      $httpBackend.expectPOST(expectedUrlIndex, paramsObj).respond(200, mockResponseIndex);

      $httpBackend.expectPOST(expectedUrlOrg, paramsObj).respond(200, mockResponseOrg);

      marketIntelligenceService.getIndexData(paramsObj, true)
        .then(function(result) {
          expect(result.org[0].someValue).toBe(2);
        })
        .catch(function() {
          throw 'This shouldn\'t have been reached';
        });

      $httpBackend.flush();
      $timeout.flush();
    });
  });

  describe('getCalendarDateRanges', function() {
    it('should get date ranges from the API when a custom calendar is used', function() {

      var startDate = moment('2017-01-01', 'YYYY-MM-DD');
      var endDate = moment('2017-01-30', 'YYYY-MM-DD');

      var calendarEndPoint = apiUrl + '/calendars/1/mi/groupByDateRanges?groupBy=week&dateStart=' + startDate.toISOString() + '&dateEnd=' +  endDate.toISOString();

      var response = {
        result: [{
          start: '01-01-2017',
          end: '07-01-2017'
        }]
      };

      $httpBackend.expectGET(calendarEndPoint).respond(200, response);

      marketIntelligenceService.getCalendarDateRanges(1, 'week', startDate, endDate)
        .then(function(result) {
          expect(result.data.result[0].start).toBe(response.result[0].start);
          expect(result.data.result[0].end).toBe(response.result[0].end);
        })
        .catch(function() {
          throw 'This shouldn\'t have been reached';
        });

      $httpBackend.flush();
    });

    it('should get ranges from the API for period to date when the selected period is periodToDate', function() {
      var startDate = moment('2017-01-01', 'YYYY-MM-DD');
      var endDate = moment('2017-01-30', 'YYYY-MM-DD');

      var calendarEndPoint = apiUrl + '/calendars/1/mi/groupByPeriodToDate?dateStart=' + startDate.toISOString() + '&dateEnd=' +  endDate.toISOString();

      var response = {
        result: [{
          start: '01-01-2017',
          end: '07-01-2017'
        }]
      };

      $httpBackend.expectGET(calendarEndPoint).respond(200, response);

      marketIntelligenceService.getCalendarDateRanges(1, 'periodToDate', startDate, endDate)
        .then(function(result) {
          expect(result.data.result[0].start).toBe(response.result[0].start);
          expect(result.data.result[0].end).toBe(response.result[0].end);
        })
        .catch(function() {
          throw 'This shouldn\'t have been reached';
        });

      $httpBackend.flush();
    });

    describe('gregorian calendar', function() {
      var momentFormat = 'YYYY-MM-DD';

      beforeEach(function() {
        dateRangeHelper.getYTD = function() {
          return {
            start: moment.utc('2017-01-01', momentFormat),
            end: moment.utc('2017-11-30', momentFormat)
          }
        }

        dateRangeHelper.getQTD = function() {
          return {
            start: moment.utc('2017-09-01', momentFormat),
            end: moment.utc('2017-11-30', momentFormat)
          }
        }

        dateRangeHelper.getMTD = function() {
          return {
            start: moment.utc('2017-11-01', momentFormat),
            end: moment.utc('2017-11-30', momentFormat)
          }
        }

        dateRangeHelper.getWTD = function() {
          return {
            start: moment.utc('2017-11-23', momentFormat),
            end: moment.utc('2017-11-30', momentFormat)
          }
        }
      });

      it('should calculate the periodToDate periods when selected period is periodToDate', function() {
        var standardGregorianSundayCalendarId = -2;

        var startDate = moment.utc('2017-01-01', momentFormat);
        var endDate = moment.utc('2017-12-30', momentFormat);

        marketIntelligenceService.getCalendarDateRanges(standardGregorianSundayCalendarId, 'periodToDate', startDate, endDate)
          .then(function(result) {
            var ranges = result.data.result[0];
            
            expect(ranges.week.start).toBe('2017-11-23T00:00:00.000Z');
            expect(ranges.week.end).toBe('2017-12-30T00:00:00.000Z');

            expect(ranges.month.start).toBe('2017-11-01T00:00:00.000Z');
            expect(ranges.month.end).toBe('2017-12-30T00:00:00.000Z');

            expect(ranges.quarter.start).toBe('2017-09-01T00:00:00.000Z');
            expect(ranges.quarter.end).toBe('2017-12-30T00:00:00.000Z');

            expect(ranges.year.start).toBe('2017-01-01T00:00:00.000Z');
            expect(ranges.year.end).toBe('2017-12-30T00:00:00.000Z');
          })
          .catch(function() {
            throw 'This shouldn\'t have been reached';
          });

        $timeout.flush();
      });

      it('should adjust the ranges returned by the api by one day when the calendar is standard gregorian monday', function() {
        var standardGregorianSundayCalendarId = -1;

        var startDate = moment('2017-01-01', momentFormat);
        var endDate = moment('2017-12-30', momentFormat);

        var calendarEndPoint = apiUrl + '/calendars/65/mi/groupByDateRanges?groupBy=week&dateStart=' + startDate.toISOString() + '&dateEnd=' +  endDate.toISOString();

        var response = {
          result: [{
            start: '2017-01-01T00:00:00.000Z',
            end: '2017-01-07T00:00:00.000Z'
          }, {
            start: '2017-01-07T00:00:00.000Z',
            end: '2017-01-14T00:00:00.000Z'
          }]
        };

        $httpBackend.expectGET(calendarEndPoint).respond(200, response);

        marketIntelligenceService.getCalendarDateRanges(standardGregorianSundayCalendarId, 'week', startDate, endDate)
          .then(function(result) {
            var dates = result.data.result;
            expect(dates[0].start).toBe('2017-01-01T00:00:00.000Z'); // First date is always unchanged
            expect(dates[0].end).toBe('2017-01-08T00:00:00.000Z');   // All others are adjusted by 1

            expect(dates[1].start).toBe('2017-01-08T00:00:00.000Z'); // First date is always unchanged
            expect(dates[1].end).toBe('2017-01-15T00:00:00.000Z');   // All others are adjusted by 1
          })
          .catch(function() {
            throw 'This shouldn\'t have been reached';
          });

        $httpBackend.flush();

      });

      it('should calculate the periodToDate periods when selected period is periodToDate and the type is compare', function() {
        var standardGregorianSundayCalendarId = -2;

        // Let's fake the bits we need
        localizationService.getCurrentCalendarFirstDayOfWeek = function() {
          return 0;
        }

        utils.getEquivalentPriorYearDateRange = function(range) {
          return {
            start: moment(range.start).subtract(1, 'year'),
            end: moment(range.end).subtract(1, 'year')
          }
        }

        var startDate = moment.utc('2017-01-01', momentFormat);
        var endDate = moment.utc('2017-12-30', momentFormat);


        marketIntelligenceService.getCalendarDateRanges(standardGregorianSundayCalendarId, 'periodToDate', startDate, endDate, 'compare')
          .then(function(result) {
            var ranges = result.data.result[0];
            
            expect(ranges.week.start).toBe('2016-11-23T00:00:00.000Z');
            expect(ranges.week.end).toBe('2016-12-30T00:00:00.000Z');

            expect(ranges.month.start).toBe('2016-11-01T00:00:00.000Z');
            expect(ranges.month.end).toBe('2016-12-30T00:00:00.000Z');

            expect(ranges.quarter.start).toBe('2016-09-01T00:00:00.000Z');
            expect(ranges.quarter.end).toBe('2016-12-30T00:00:00.000Z');

            expect(ranges.year.start).toBe('2016-01-01T00:00:00.000Z');
            expect(ranges.year.end).toBe('2016-12-30T00:00:00.000Z');
          })
          .catch(function() {
            throw 'This shouldn\'t have been reached';
          });
          
        $timeout.flush();
      });
    });
  });

  describe('getSubscriptions', function() {
    it('should request the subscriptions from the API', function() {
      var orgId = 100;

      var url = apiUrl + '/mi/subscription?orgId=' + orgId.toString();

      var mockResponse = [{
        name: 'subscription 1'
      }];

      $httpBackend.expectGET(url).respond(200, mockResponse);

      marketIntelligenceService.getSubscriptions(orgId)
        .then(function(result) {
          expect(result.data[0].name).toBe('subscription 1');
        });
      
      $httpBackend.flush();
    });
  });
});