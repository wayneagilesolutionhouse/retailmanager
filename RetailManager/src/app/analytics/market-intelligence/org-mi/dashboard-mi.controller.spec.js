'use strict';

describe('DashboardMiController', function () {
  var $rootScope;
  var $scope; 
  var $q;
  var $timeout;
  var $controller;
  var $stateParamsMock = {};
  var $mockState;

  var exportServiceMock = {
    createExportAndStore: function(params) {
      angular.noop(params);
    },

    isInExportCartWithSettings: function(params) {
      angular.noop(params);
    },
  };

  var currentOrganizationMock = {
    organization_id: 1
  };

  var mockUtils;

  var currentUserMock;

  var mockMiGeo, mockMiCategory, mockMiSubscription, subscriptionList, mockViewData;

  var mockMiUserPreferences, segmentPreferencesAreConfigured;

  var mockMarketIntelligenceService;

  var authServiceMock = {
    getCurrentUser: function () {
      var defer = $q.defer();
      defer.resolve(currentUserMock);
      return defer.promise;
    }
  }

  beforeEach(module('shopperTrak', function ($translateProvider) {
    $translateProvider.translations('en_US', {});
  }));

  beforeEach(inject(function (_$rootScope_, _$controller_, _$q_, _$timeout_) {
    $scope = _$rootScope_.$new();
    $controller = _$controller_;
    $q = _$q_;
    $timeout = _$timeout_;
    $rootScope = _$rootScope_;
  }));

  beforeEach(function() {
    console.error = angular.noop;
  });

  describe('exportAllWidgets', function() {
    beforeEach(function() {
      resetResources();

      currentUserMock = {
        username: 'test1',
        preferences: {
          market_intelligence : {
            segments: getSingleSubscription()
          } 
        }
      };

      segmentPreferencesAreConfigured = true;
    });

    it('should redirect the user to the pdf view when the scheduleExportCurrentViewToPdf event is broadcast', function() {
      instantiateController();

      $timeout.flush();

      spyOn($mockState, 'go');

      $rootScope.$broadcast('scheduleExportCurrentViewToPdf');

      expect($mockState.go).toHaveBeenCalledWith('pdfexport', { orgId: 1, view: 'schedule'});
    });
  });

  describe('exportWidget', function() {
    beforeEach(function() {
      resetResources();

      currentUserMock = {
        username: 'test1',
        preferences: {
          market_intelligence : {
            segments: getSingleSubscription()
          } 
        }
      };

      segmentPreferencesAreConfigured = true;
    });

    it('should add the segment widget to the export cart', function() {
      spyOn(exportServiceMock, 'createExportAndStore');
      
      var vm = instantiateController();

      $timeout.flush();

      vm.exportWidget('segment');

      expect(exportServiceMock.createExportAndStore).toHaveBeenCalled();
    });
  });

  describe('widgetIsExported', function() {
    beforeEach(function() {
      resetResources();

      currentUserMock = {
        username: 'test1',
        preferences: {
          market_intelligence : {
            segments: getSingleSubscription()
          } 
        }
      };

      segmentPreferencesAreConfigured = true;
    });

    it('should call the export service to check if a widget is in the export cart', function() {
      spyOn(exportServiceMock, 'isInExportCartWithSettings');

      var vm = instantiateController();

      $timeout.flush();

      vm.widgetIsExported('segment', mockViewData.segment);

      expect(exportServiceMock.isInExportCartWithSettings).toHaveBeenCalled();
    });
  });


  describe('activate', function () {
    beforeEach(function() {
      resetResources();
    });

    it('should set isLoading to true', function() {
      var vm = instantiateController();
      expect(vm.isLoading).toBe(true);
    });

    it('should set market intelligence to unavailable if the geographies endpoint returns an error', function() {
      mockMiGeo.query = function() {
        var defer = $q.defer();
        defer.reject('some error');
        return {
          $promise: defer.promise
        };
      }

      var vm = instantiateController();

      $timeout.flush();

      expect(vm.isLoading).toBe(false);
      expect(vm.miAvailable).toBe(false)
    });

    it('should set market intelligence to unavailable if the categories endpoint returns an error', function() {
      mockMiCategory.query = function() {
        var defer = $q.defer();
        defer.reject('some error');
        return {
          $promise: defer.promise
        };
      }

      var vm = instantiateController();

      $timeout.flush();

      expect(vm.isLoading).toBe(false);
      expect(vm.miAvailable).toBe(false)
    });

    it('should set market intelligence to unavailable if the miSubscription endpoint returns an error', function() {
      mockMiSubscription.query = function() {
        var defer = $q.defer();
        defer.reject('some error');
        return {
          $promise: defer.promise
        };
      }

      var vm = instantiateController();

      $timeout.flush();

      expect(vm.isLoading).toBe(false);
      expect(vm.miAvailable).toBe(false)
    });

    it('should set market intelligence to unavailable if an empty list gets returned from the subscriptions service', function() {
      subscriptionList = [];

      var vm = instantiateController();

      $timeout.flush();

      expect(vm.isLoading).toBe(false);
      expect(vm.miAvailable).toBe(false)
    });

    it('should set market intelligence to unavailable if no subscriptions are returned for the org', function() {
      mockMiSubscription.query = function() {
        var defer = $q.defer();
        defer.resolve([]);
        return {
          $promise: defer.promise
        };
      };

      $stateParamsMock = {
        orgId: 100
      };

      spyOn(console, 'error');

      var vm = instantiateController();

      $timeout.flush();

      expect(vm.isLoading).toBe(false);
      expect(vm.miAvailable).toBe(false);
      expect(console.error).toHaveBeenCalledWith('No Market Intelligence subscriptions found for orgId: 100');
    });

    describe('market intelligence is available', function() {
      it('should assign the date ranges to scope variables from the state', function() {
        $stateParamsMock.dateRangeStart = moment.utc('2017-01-01');
        $stateParamsMock.dateRangeEnd = moment.utc('2017-01-30');

        $stateParamsMock.compareRange1Start = moment.utc('2016-01-01');
        $stateParamsMock.compareRange1End = moment.utc('2016-01-30');

        var vm = instantiateController();

        $timeout.flush();

        expect(vm.periodSelected.start.toISOString()).toBe($stateParamsMock.dateRangeStart.toISOString());
        expect(vm.periodSelected.end.toISOString()).toBe($stateParamsMock.dateRangeEnd.toISOString());

        expect(vm.periodCompared.start.toISOString()).toBe($stateParamsMock.compareRange1Start.toISOString());
        expect(vm.periodCompared.end.toISOString()).toBe($stateParamsMock.compareRange1End.toISOString());
      });

      it('should set the current user', function() {
        currentUserMock = {
          username: 'test1'
        };

        var vm = instantiateController();

        $timeout.flush();

        expect(vm.currentUser.username).toBe('test1');
      });

      describe('First time MI configuration', function() {
        it('should configure the default selected segments if the current user has no segments configured', function() {
          segmentPreferencesAreConfigured = false;

          currentUserMock = {
            username: 'test1'
          };

          var vm = instantiateController();

          $timeout.flush();

          expect(vm.defaultSelectedSegmentsArray.length).toBe(5);
          expect(vm.defaultSelectedSegmentsArray[0].subscription.geography.value.name).toBe('US');
          expect(vm.defaultSelectedSegmentsArray[1].subscription.geography.value.name).toBe('Northeast');
          expect(vm.defaultSelectedSegmentsArray[2].subscription.geography.value.name).toBe('Midwest');
          expect(vm.defaultSelectedSegmentsArray[3].subscription.geography.value.name).toBe('West');
          expect(vm.defaultSelectedSegmentsArray[4].subscription.geography.value.name).toBe('South');
        });

        it('should save the the default segments once set', function() {
          segmentPreferencesAreConfigured = false;

          currentUserMock = {
            username: 'test1',
            preferences: {
              market_intelligence : { } 
            }
          };

          spyOn(mockMarketIntelligenceService, 'saveUserMarketIntelligence').and.callThrough();

          var vm = instantiateController();

          $timeout.flush();

          segmentPreferencesAreConfigured = true;

          expect(mockMarketIntelligenceService.saveUserMarketIntelligence).toHaveBeenCalledWith(currentUserMock, vm.defaultSelectedSegmentsArray);
          expect(vm.firstTimeConfigure).toBe(false);
        });

        it('should emit setMiExists once market intelligence has been configured and saved for the first time', function() {
          segmentPreferencesAreConfigured = false;

          currentUserMock = {
            username: 'test1',
            preferences: {
              market_intelligence : { } 
            }
          };

          spyOn($scope, '$emit');

          instantiateController();

          $timeout.flush();

          expect($scope.$emit).toHaveBeenCalledWith('setMiExists');
        });
      });

      describe('the user has market intelligence configured', function() {
        beforeEach(function() {
          currentUserMock = {
            username: 'test1',
            preferences: {
              market_intelligence : {
                segments: getSingleSubscription()
              } 
            }
          };

          segmentPreferencesAreConfigured = true;

          $stateParamsMock.dateRangeStart = moment.utc('2017-01-01');
          $stateParamsMock.dateRangeEnd = moment.utc('2017-01-30');

          $stateParamsMock.compareRange1Start = moment.utc('2016-01-01');
          $stateParamsMock.compareRange1End = moment.utc('2016-01-30');
        });

        it('should setup the params object', function() {
          var vm = instantiateController();

          $timeout.flush();

          expect(vm.params.dateStart.toISOString()).toBe($stateParamsMock.dateRangeStart.toISOString());
          expect(vm.params.dateEnd.toISOString()).toBe($stateParamsMock.dateRangeEnd.toISOString());
          expect(vm.params.subscriptions).toBeDefined();
        });

        it('should set the default segment', function() {
          var vm = instantiateController();

          $timeout.flush();

          expect(vm.defaultSegment.subscription.category.value.name).toBe('Total Retail');
          expect(vm.defaultSegment.subscription.geography.value.name).toBe('US');
        });
      });
    });
  });

  function resetResources() {
    var totalRetailUuid = 'da815dbc-f066-4807-a6ac-ae145e6b6242';

    subscriptionList = [{
      geography: {
        name: 'US',
        uuid: '6d1175b8-eb06-45ee-9d11-9a63c072f728',
        geoType: 'REGION'
      },
      category: {
        name: 'Total Retail',
        uuid: totalRetailUuid
      }
    }, {
      geography: {
        name: 'Northeast',
        geoType: 'REGION',
        parentUuid: '6d1175b8-eb06-45ee-9d11-9a63c072f728',
        uuid: '2222'
      },
      category: {
        name: 'Total Retail',
        uuid: totalRetailUuid
      }
    }, {
      geography: {
        name: 'Midwest',
        geoType: 'REGION',
        parentUuid: '6d1175b8-eb06-45ee-9d11-9a63c072f728',
        uuid: '3333'
      },
      category: {
        name: 'Total Retail',
        uuid: totalRetailUuid
      }
    }, {
      geography: {
        name: 'West',
        geoType: 'REGION',
        parentUuid: '6d1175b8-eb06-45ee-9d11-9a63c072f728',
        uuid: '4444'
      },
      category: {
        name: 'Total Retail',
        uuid: totalRetailUuid
      }
    }, {
      geography: {
        name: 'South',
        geoType: 'REGION',
        parentUuid: '6d1175b8-eb06-45ee-9d11-9a63c072f728',
        uuid: '5555'
      },
      category: {
        name: 'Total Retail',
        uuid: totalRetailUuid
      }
    }];

    mockMiGeo = {
      query: function() {
        var defer = $q.defer();
        defer.resolve();
        return {
          $promise: defer.promise
        }
      }
    };

    mockMiCategory = {
      query: function() {
        var defer = $q.defer();
        defer.resolve();
        return defer.promise;
      }
    };

    mockMiSubscription = {
      query: function() {
        var defer = $q.defer();
        defer.resolve(subscriptionList);
        return {
          $promise: defer.promise
        };
      }
    };

    mockMiUserPreferences = {
      segmentPreferencesAreConfigured: function() {
        return segmentPreferencesAreConfigured;
      },
      getConfiguredSegments: function(userPreferences) {
        return userPreferences.market_intelligence.segments;
      }
    };

    mockMarketIntelligenceService = {
      saveUserMarketIntelligence: function(currentUser, segments) {
        angular.noop(currentUser, segments);
        var defer = $q.defer();
        defer.resolve();
        return defer.promise;
      }
    };

    mockViewData = {
      "segment": {
        "orgId": 1000003068,
        "dateRange": {
          "start": "2017-08-06T00:00:00.000Z",
          "end": "2017-08-12T23:59:59.999Z"
        },
        "compare1Range": {
          "start": "2016-08-07T00:00:00.000Z",
          "end": "2016-08-13T23:59:59.999Z"
        },
        "compare2Range": {},
        "compare1Type": "prior_year",
        "compare2Type": {},
        "numberFormat": "en-us",
        "name": "segment",
        "hideCompare2Range": true,
        "dateFormat": "D.M.YYYY",
        "showOrgIndex": true,
        "segments": [{
          "positionIndex": 0,
          "category": {
            "uuid": "da815dbc-f066-4807-a6ac-ae145e6b6242"
          },
          "geography": {
            "uuid": "6d1175b8-eb06-45ee-9d11-9a63c072f728"
          }
        }, {
          "positionIndex": 1,
          "category": {
            "uuid": "da815dbc-f066-4807-a6ac-ae145e6b6242"
          },
          "geography": {
            "uuid": "102cc0f3-e297-4ce3-bb1e-cb192c68eaee"
          }
        }]
      },
      "market-intelligence": {
        "orgId": 1000003068,
        "dateRange": {
          "start": "2017-08-06T00:00:00.000Z",
          "end": "2017-08-12T23:59:59.999Z"
        },
        "compare1Range": {
          "start": "2016-08-07T00:00:00.000Z",
          "end": "2016-08-13T23:59:59.999Z"
        },
        "compare2Range": {},
        "compare1Type": "prior_year",
        "compare2Type": {},
        "numberFormat": "en-us",
        "name": "market-intelligence",
        "hideCompare2Range": true,
        "dateFormat": "D.M.YYYY",
        "showOrgIndex": true,
        "segments": [{
          "positionIndex": 0,
          "category": {
            "uuid": "da815dbc-f066-4807-a6ac-ae145e6b6242"
          },
          "geography": {
            "uuid": "6d1175b8-eb06-45ee-9d11-9a63c072f728"
          }
        }, {
          "positionIndex": 1,
          "category": {
            "uuid": "da815dbc-f066-4807-a6ac-ae145e6b6242"
          },
          "geography": {
            "uuid": "102cc0f3-e297-4ce3-bb1e-cb192c68eaee"
          }
        }]
      }
    };

    $mockState = {
      go: function(stateName) {
        angular.noop(stateName)
      }
    }

    mockUtils = {
      dateRangeIsPriorYear: function() {
        return true;
      },
      getCompareType: function() {
        return 'compare-type';
      },
      urlDateParamsLoaded: function() {
        return true;
      }
    }
  }

  function getSingleSubscription() {
    return [
      {
        'subscription': {
          'geography': {
            'orgId': 1000003068,
            'name': 'Country',
            'rule': 'Contains',
            'value': {
              'name': 'US',
              'src': {
                'uuid': '6d1175b8-eb06-45ee-9d11-9a63c072f728',
                'lastUpdated': '2017-07-03T09:00:00.033Z',
                'name': 'US',
                'geoType': 'COUNTRY',
                'parentUuid': '46a3b5b5-76b6-4419-8038-0b622bc9f7ae',
                
              }
            }
          },
          'category': {
            'orgId': 1000003068,
            'name': 'Category',
            'rule': 'Contains',
            'value': {
              'name': 'Total Retail',
              'src': {
                'name': 'Total Retail',
                'uuid': 'da815dbc-f066-4807-a6ac-ae145e6b6242',
                'lastUpdated': '2017-05-17T16:46:27.009Z'
              }
            }
          }
        }
      }
    ]
  }

  function instantiateController() {
    return $controller('DashboardMiController', {
      '$scope': $scope,
      '$state': $mockState,
      '$stateParams': $stateParamsMock,
      'currentOrganization': currentOrganizationMock,
      'authService': authServiceMock,
      'miUserPreferences': mockMiUserPreferences,
      'MarketIntelligenceGeographyResource': mockMiGeo,
      'MarketIntelligenceCategoryResource': mockMiCategory,
      'MarketIntelligenceSubscriptionResource': mockMiSubscription,
      'marketIntelligenceService': mockMarketIntelligenceService,
      'ExportService': exportServiceMock,
      'utils': mockUtils
    });

    $timeout.flush();
  }
});
