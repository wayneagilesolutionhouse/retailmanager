'use strict';

describe('WidgetLibraryController', function () {
	var $scope;
	var $rootScope;
	var $controller;
	var features;

	var controller;

	beforeEach(module('shopperTrak'));
	beforeEach(inject(function (_$rootScope_, _$controller_, _features_) {
		$scope = _$rootScope_.$new();
		$rootScope = _$rootScope_
		$controller = _$controller_;
		features = _features_,
		controller = null;
	}));


		//TODO: Replace with real tests
		it('should return a test string', function () {
				//Place tests inside this if block
			if (features.isEnabled('widgetLibrary')) {
				var testString = 'Hello World!';

				instantiateController();
				console.log('Running widget library dummy test');
				expect(controller.test()).toBe(testString);
			}
		});

	function instantiateController() {

		controller = $controller('WidgetLibraryController', {
			'$scope': $scope
		});

		// Emulate the 'controllerAs' syntax:
		$scope.vm = controller;
	}

});