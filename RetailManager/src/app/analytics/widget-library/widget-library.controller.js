(function () {
  'use strict';

  angular
    .module('shopperTrak')
    .controller('WidgetLibraryController', WidgetLibraryController);

  WidgetLibraryController.$inject = [
		'$scope',
		'$state',
		'features'
  ];

  function WidgetLibraryController(
		$scope,
		$state,
		features
  ) {

		//TODO: remove block when feature ready
		if(!features.isEnabled('widgetLibrary')){
			$state.go('analytics.organization')
		}

		var vm = this;


		activate();

		function activate(){
			//test function assignment
			//TODO: remove when real tests addeed
			vm.test = test;
		}

		//test function
		//TODO: remove when real tests addeed
		function test(){
			return 'Hello World!';
		}
  }
})();