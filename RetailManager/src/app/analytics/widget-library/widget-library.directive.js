(function () {
  'use strict';

  angular.module('shopperTrak').directive('widgetLibrary', function widgetLibrary() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/analytics/widget-library/widget-library.partial.html',
      bindToController: true
    };
  });

})();