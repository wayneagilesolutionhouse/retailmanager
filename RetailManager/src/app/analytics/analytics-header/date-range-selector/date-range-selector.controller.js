'use strict';

angular.module('shopperTrak')
.controller('DateRangeSelectorCtrl', [
  '$scope',
  '$rootScope',
  '$state',
  '$stateParams',
  'utils',
  'LocalizationService',
  '$timeout',
  'getDefaultComparisonDateRangeParams',
  '$translate',
  'ObjectUtils',
  'SubscriptionsService',
  'localStorageService',
  'googleAnalytics',
function(
  $scope,
  $rootScope,
  $state,
  $stateParams,
  utils,
  LocalizationService,
  $timeout,
  getDefaultComparisonDateRangeParams,
  $translate,
  ObjectUtils,
  SubscriptionsService,
  localStorageService,
  googleAnalytics
) {
  var now = moment().utc();

  $scope.$state = $state;
  $scope.comparePeriodSelectorIsOpen = false;
  $scope.opHoursSelectorIsOpen = false;
  $scope.calendarsLoaded = false;
  $scope.showDateRangePicker = false;
  $scope.showRealTimeData = false;
  $scope.currentUser = $scope.$parent.currentUser;
  $scope.currentOrganization = $scope.$parent.currentOrganization;

  $scope.language = LocalizationService.getCurrentLocaleSetting();

  $state.rangeSelected = 'week';

  $scope.$watch('firstDay', function() {
    if(!ObjectUtils.isNullOrUndefined($scope.firstDay)) {
      getStandardShortcuts();
    }
  });

  $scope.$on('dateSelectionChanged', dateRangeChanged);

  $scope.$on('$stateChangeSuccess', function() {
    configureRealTime();
  });

  $scope.setCustomRangeActive = setCustomRangeActive;
  $scope.dateRangeIsSetToCustom = dateRangeIsSetToCustom;
  $scope.setDateRange = setDateRange;
  $scope.changeOperatingHoursType = changeOperatingHoursType;
  $scope.showRealTimaData = showRealTimaData;
  $scope.showBusinessHourOption = getShowBusinessHours();
  
  activate();

  function activate() {
    loadTranslations();
    configureRealTime();
    checkIfCurrentCalendarHasExpired();
    LocalizationService.setOrganization($scope.currentOrganization);

    if($state.current.name.indexOf('marketIntelligence') !== -1) {
      $scope.hasOnlyOneCompare = true;
    }

    if(ObjectUtils.isNullOrUndefined($scope.selectedDateRange)) {
      $scope.selectedDateRange = {
        start: $stateParams.dateRangeStart,
        end: $stateParams.dateRangeEnd
      };
    }

    if ($stateParams.compareRange1Start && $stateParams.compareRange1End) {
      $scope.compareRange1 = {
        start: $stateParams.compareRange1Start,
        end: $stateParams.compareRange1End
      };
    }

    if ($stateParams.compareRange2Start && $stateParams.compareRange2End) {
      $scope.compareRange2 = {
        start: $stateParams.compareRange2Start,
        end: $stateParams.compareRange2End
      };
    }

    if ($stateParams.businessDays === 'true') {
      $scope.operatingHoursType = 'BUSINESS_DAYS';
    } else {
      $scope.operatingHoursType = 'BUSINESS_HOURS';
    }
    loadCalendars();
    $scope.dateFormat = LocalizationService.getCurrentDateFormat($scope.currentOrganization);
  }

  function getStandardShortcuts() {
    $scope.dateRangeShortcuts = [
      {
        'label': $scope.translatedLabels.day,
        'type': 'day'
      },{
        'label': $scope.translatedLabels.week,
        'type': 'week'
      },{
        'label': $scope.translatedLabels.month,
        'type': 'month'
      },{
        'label': $scope.translatedLabels.year,
        'type': 'year'
      }
    ];
  }

  function checkIfCurrentCalendarHasExpired() {
    if(LocalizationService.isCurrentCalendarGregorian(true)) {
      return;
    }

    $scope.currentCalendarIsExpired = LocalizationService.currentCalendarHasExpired();

    if($scope.currentCalendarIsExpired) {
      $scope.calendarName = LocalizationService.getCurrentCalendarName();
    }
  }

  function loadCalendars() {
    LocalizationService.getAllCalendars().then( function(calendars) {
      $scope.allCalendars = LocalizationService.setAllCalendars(calendars.result);
      $scope.organizationCalendars = calendars.result;
      $scope.calendarsLoaded = true;

      setActiveDateRange();
      getOrganizationStartOfWeek();
    });
  }

  function setActiveDateRange() {
    if(dateRangeIsSetTo('week')) {
      $scope.activeDateRange = 'week';
      return;
    }

    if(dateRangeIsSetTo('day')) {
      $scope.activeDateRange = 'day';
      return;
    }

    if(dateRangeIsSetTo('month')) {
      $scope.activeDateRange = 'month';
      return;
    }

    if(dateRangeIsSetTo('year')) {
      $scope.activeDateRange = 'year';
      return;
    }

    if(dateRangeIsSetToCustom()){
      $state.rangeSelected = 'custom';
      $scope.activeDateRange = 'custom';
    }
  }

  function getOrganizationStartOfWeek() {
    $scope.firstDay = LocalizationService.getCurrentCalendarFirstDayOfWeek();
  }

  function dateRangeIsSetTo(type) {
    $state.rangeSelected = type;
    var compareDateRange;

    if($scope.calendarsLoaded && $scope.currentUser !== undefined) {

      LocalizationService.setUser($scope.currentUser);

      var calendarInfo = LocalizationService.getSystemYearForDate(moment());

      if(!LocalizationService.hasMonthDefinitions() ) {
        calendarInfo.month -= 1;
      }

      var currentMonth = calendarInfo.month;
      var currentYear = calendarInfo.year;

      if(type==='day') {
        compareDateRange = {
          start: moment(now).startOf('day').subtract(1, 'day'),
          end: moment(now).startOf('day').subtract(1, 'day').endOf('day')
        };
      } else if(type==='week') {
        compareDateRange = {
          start: LocalizationService.getFirstDayOfCurrentWeek().subtract(1, 'week'),
          end: LocalizationService.getLastDayOfCurrentWeek().subtract(1, 'week')
        };
      } else if(type==='month') {
        var previousYear;
        var previousMonth = currentMonth - 1;
        if(previousMonth < 0) {
          previousMonth = 11;
          previousYear = currentYear - 1;
        } else {
          previousYear = currentYear;
        }
        compareDateRange = {
          start: LocalizationService.getFirstDayOfMonth(previousMonth, previousYear),
          end: LocalizationService.getLastDayOfMonth(previousMonth, previousYear)
        };
      } else if(type==='year') {
        compareDateRange = {
          start: LocalizationService.getFirstDayOfYear(currentYear-1),
          end: LocalizationService.getLastDayOfYear(currentYear-1)
        };
      }

      return ($scope.selectedDateRange.start.format('DD.MM.YYYY') === compareDateRange.start.format('DD.MM.YYYY') &&
             $scope.selectedDateRange.end.format('DD.MM.YYYY') === compareDateRange.end.format('DD.MM.YYYY')) ||
             $scope.selectedPreset === type;

    } else {
      return false;
    }
  }

  function dateRangeIsSetToCustom() {
    for (var i in $scope.dateRangeShortcuts) {
      var shortcut = $scope.dateRangeShortcuts[i];
      if (dateRangeIsSetTo(shortcut.type)) {
        return false;
      }
    }
    return true;
  }

  function dateRangeChanged() {
    $scope.realTimeDataShown = false;

    $timeout(function() {
      $state.go(getStateName(), {
        dateRangeStart: $scope.selectedDateRange.start,
        dateRangeEnd: $scope.selectedDateRange.end,
        compareRange1Start: $scope.compareRange1.start,
        compareRange1End: $scope.compareRange1.end,
        compareRange2Start: $scope.compareRange2.start,
        compareRange2End: $scope.compareRange2.end
      });
    });
  }

  function getStateName() {
   if($scope.realTimeDataShown === true || !ObjectUtils.isNullOrUndefinedOrBlank($stateParams.returnState)) {
      return $stateParams.returnState;
    }
    return $state.$current.name;
  }

  function setDateRange(type) {
    googleAnalytics.trackUserEvent('date shortcut', type);
    $scope.activeDateRange = type;
    var newDateRange = {};
    var systemDate, year, previousMonth;

    if($scope.calendarsLoaded) {

      if(type==='day') {
        newDateRange = {
          start: moment(now).startOf('day').subtract(1, 'day'),
          end: moment(now).startOf('day').subtract(1, 'day').endOf('day')
        };
      } else if(type==='week') {
        newDateRange = {
          start: LocalizationService.getFirstDayOfCurrentWeek().subtract(1, 'week'),
          end: LocalizationService.getLastDayOfCurrentWeek().subtract(1, 'week')
        };
      } else if(type==='month') {
        systemDate = LocalizationService.getSystemYearForDate( moment() );

        if(LocalizationService.hasMonthDefinitions() ) {
          previousMonth = systemDate.month - 1;
        } else {
          previousMonth = systemDate.month - 2;
        }

        if(previousMonth < 0) {
          previousMonth = 11;
          year = systemDate.year - 1;
        } else {
          year = systemDate.year;
        }

        newDateRange = {
          start: LocalizationService.getFirstDayOfMonth(previousMonth, year),
          end: LocalizationService.getLastDayOfMonth(previousMonth, year)
        };

      } else if(type==='year') {
        systemDate = LocalizationService.getSystemYearForDate(moment());
        year = systemDate.year - 1;
        newDateRange = {
          start: LocalizationService.getFirstDayOfYear(year),
          end: LocalizationService.getLastDayOfYear(year)
        };
      }
      var ranges = getRanges(newDateRange);

      $timeout(function() {
        if( !ObjectUtils.isNullOrUndefined(newDateRange) ) {
          var stateName = getStateName();

          if( ObjectUtils.isNullOrUndefined(stateName) ) {
            stateName = 'analytics.organization.retail';
          }

          $scope.realTimeDataShown = false;

          $state.go(stateName, ranges);
        }
        $scope.selectedPreset = type;
      });

    }
  }

  function getRanges(newDateRange) {
    var singleComparePeriod = isCurrentStateMarketIntelligence();
    
    var compareRanges = getDefaultComparisonDateRangeParams(
      {dateRangeStart: newDateRange.start, dateRangeEnd: newDateRange.end},
      $scope.currentUser,
      $scope.currentOrganization,
      $scope.organizationCalendars,
      singleComparePeriod
    );

    var ranges = angular.extend(compareRanges,{
      dateRangeStart: newDateRange.start,
      dateRangeEnd: newDateRange.end
    });
    return ranges;
  }

  function showRealTimaData(){
    if($scope.realTimeDataShown === true || $stateParams.returnState === $state.$current.name) {
      return;
    }
    $scope.realTimeDataShown = true;
    var selectedDateRanges = {
      current : $scope.selectedDateRange,
      compare1 : $scope.compareRange1,
      compare2 : $scope.compareRange2
    };
    localStorageService.set('selectedDateRanges', JSON.stringify(selectedDateRanges));
    goToRealTimeData();
  }

  function goToRealTimeData() {
    var params = {
        realTimeDataShown: $scope.realTimeDataShown,
        returnState: $state.$current.name,
        dateRangeStart: moment(now).toISOString(),
        dateRangeEnd: moment(now).startOf('day').subtract(1, 'day').toISOString(),
        compareRange1Start: $scope.compareRange1.start,
        compareRange1End: $scope.compareRange1.end,
        compareRange2Start: $scope.compareRange2.start,
        compareRange2End: $scope.compareRange2.end,
        businessDays: $scope.operatingHoursType === 'BUSINESS_DAYS'
    };

    $state.go(getRealTimeStateName(), params);
  }

  function getRealTimeStateName() {
    if ($state.$current.name.indexOf('site')< 0) {
      return 'analytics.organization.real-time-data';
    }
    return 'analytics.organization.site.real-time-data';
  }

  function setCustomRangeActive() {
    $scope.showDateRangePicker = !$scope.showDateRangePicker;
    $scope.activeDateRange = 'custom';
  }

  function changeOperatingHoursType(newType) {
    var businessDays;
    $scope.operatingHoursType = newType;
    if(newType === 'BUSINESS_HOURS') {
      businessDays = false;
    } else {
      businessDays = true;
    }
    if($state.$current.name.indexOf('real-time') >= 0) {
      $rootScope.$broadcast('businessDayChanged', businessDays);
      return;
    }
    $state.go(getStateName(), {
      businessDays: businessDays
    });
  }

  function loadTranslations() {
    $translate.use($scope.language);

    var transKeys = [
      'common.DAY',
      'common.WEEK',
      'common.MONTH',
      'common.YEAR'
    ];

    $scope.translatedLabels = {};

    $translate(transKeys).then(function (translations) {
      $scope.translatedLabels.day = translations[transKeys[0]];
      $scope.translatedLabels.week = translations[transKeys[1]];
      $scope.translatedLabels.month = translations[transKeys[2]];
      $scope.translatedLabels.year = translations[transKeys[3]];
    });
  }

  function configureRealTime() {
    if(ObjectUtils.isNullOrUndefined($scope.realTimeDataShown)) {
      $scope.realTimeDataShown = $stateParams.realTimeDataShown;
    }

    if(ObjectUtils.isNullOrUndefined($scope.realTimeDataShown)) {
      $scope.realTimeDataShown = false;
    }

    $scope.showRealTimeData = SubscriptionsService.hasRealTime($scope.currentOrganization) && !isCurrentStateMarketIntelligence();

    //when current site selected if site's customer_site_id not defined site doesn't have real time data so api returns error so hide the button
    if(!ObjectUtils.isNullOrUndefined($scope.$parent.currentSite) &&
      ObjectUtils.isNullOrUndefined($scope.$parent.currentSite.customer_site_id)) {
      $scope.showRealTimeData = false;
    }
  }

  function isCurrentStateMarketIntelligence() {
    return $state.current.name.indexOf('marketIntelligence') > -1;
  }

  function getShowBusinessHours() {
    if($state.current.data.title === 'Traffic') {
      return true;
    }

    if($state.$current.name.indexOf('real-time') > -1) {
      return true;
    }
    
    if($state.current.data.title === 'Hourly') {
      return true;
    }

    return false;
  }
}]);
