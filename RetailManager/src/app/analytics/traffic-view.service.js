(function () {
  'use strict';

angular.module('shopperTrak')
  .factory('trafficViewService', [
    '$rootScope',
    '$q',
    '$state',
    '$stateParams',
    'utils',
    'ObjectUtils',
    'metricConstants',
    'widgetConstants',
    'ExportService',
    function trafficViewService (
    $rootScope,
    $q,
    $state,
    $stateParams,
    utils,
    ObjectUtils,
    metricConstants,
    widgetConstants,
    ExportService) {

    var self = this;
    var currentOrganization;
    var currentZone;
    var currentSite;
    var metricsToShow;

    function widgetVariables() {
      return widgetConstants.exportProperties;
    }

    function isShowMetrics() {
      var tenant = ['TenantCommon'];

      if (ObjectUtils.isNullOrUndefined(currentOrganization) && ObjectUtils.isNullOrUndefined(currentZone)) {
        return false;
      }

      var retailSite = currentOrganization.portal_settings.organization_type === 'Retail' ||
        (!ObjectUtils.isNullOrUndefined(currentSite) && currentSite.type === 'Retailer');

      var tenantZone = (!ObjectUtils.isNullOrUndefined(currentZone) && currentZone.type === tenant) ||
        (!ObjectUtils.isNullOrUndefined(self.zones) && hasZonesWithType(tenant));

      return retailSite || tenantZone;
    }

    function hasZonesWithType(widgetZoneTypes, exclude) {
      var zoneTypes = mapZoneType(self.zones);
      if (exclude === undefined) {
        exclude = false;
      }
      var filteredList = filterLists(zoneTypes, widgetZoneTypes, exclude);
      var showWidget = false;

      if (filteredList !== undefined && filteredList.length > 0) {
        showWidget = true;
      }
      return showWidget;
    }

    function mapZoneType(zones) {
      return zones.map(function (zone) {
        return zone.type;
      });
    }

    function filterLists(zoneTypes, widgetZoneTypes, exclude) {
      var matchedList = [];

      zoneTypes.forEach(function (type) {
        if (widgetZoneTypes.indexOf(type) === -1 && exclude === true) {
          matchedList.push(type);
        } else if (widgetZoneTypes.indexOf(type) !== -1 && exclude === false) {
          matchedList.push(type);
        }
      });

      return matchedList;
    }

    function initExportParam(metricKey, state) {
      var params;

      params = {
        orgId: currentSite.organization.id,
        siteId: currentSite.site_id,
        dateRange: { start: state.dateRange.start, end: state.dateRange.end },
        dateRangeType: utils.getDateRangeType(state.dateRange, state.currentUser, state.currentOrganization),
        compare1Range: { start: state.compareRange1.start, end: state.compareRange1.end },
        compare2Range: { start: state.compareRange2.start, end: state.compareRange2.end },
        compare1Type: state.compareRange1Type,
        compare2Type: state.compareRange2Type,
        dateFormat: state.dateFormat,
        dateRangeShortCut: $state.rangeSelected,
        customRange:  $state.customRange,
        name: metricKey,
        kpi: state.kpi.name,
        language: state.language,
        firstDayOfWeekSetting: state.firstDayOfWeek,
        operatingHours: state.operatingHours,
        siteHasLabor: state.siteHasLabor,
        siteHasSales: state.siteHasSales,
        currencySymbol: _.findWhere(metricConstants.metrics, { isCurrency: true }).prefixSymbol,
        userId: state.currentUser._id,
        selectedWeatherMetrics: state.selectedWeatherMetrics,
        showWeatherMetrics: state.showWeatherMetrics,
        sortType: '-traffic'
      };

      if (currentZone) {
        params.zoneId = currentZone.id;
      }

      return params;
    }

    function setCurrentOrganization(org) {
      currentOrganization = org;
    }

    function setCurrentZone (zone) {
      currentZone = zone;
    }

    function setCurrentSite(site) {
      currentSite = site;
      if (site && site.zones) {
        self.zones = site.zones;
      }
    }

    function setMetricsToShow(metrics) {
      metricsToShow = metrics;
    }

    function getAreaKey() {
      if (ObjectUtils.isNullOrUndefined(currentSite)) {
        return null;
      }

      var areaKey = currentSite.organization.id + '_' + currentSite.site_id;
      if (currentZone) {
        areaKey += '_zone_' + currentZone.id;
      }
      return areaKey;
    }

    function scheduleExportCurrentViewToPdf(scope) {
      _.each(metricsToShow, function (value) {
        if(value === 'daily_performance_widget') {
          if (scope.showPerfWidget) {
            scope.exportWidget(value);
          }
        } else {

          // Export entrance contribution if there is more than 1 entrance for a zone
          if (value === 'entrance_contribution_pie' && !ObjectUtils.isNullOrUndefined(currentZone)) {
            var numberOfEntrances = scope.vm.chartSegments.length;
            if (numberOfEntrances > 1) {
              scope.exportWidget(value);
            }
          } else {
            // Export entrance contribution pie if there is more than 1 entrance
            if (value === 'entrance_contribution_pie' && currentSite.zones.length > 0 && ObjectUtils.isNullOrUndefined(currentZone)) {
              var numberOfEntrances = scope.vm.chartSegments.length;
              if (numberOfEntrances > 1) {
                scope.exportWidget(value);
              }
            } else {
              scope.exportWidget(value);
            }

          }

        }
      });

      $state.go('pdfexport', { orgId: currentOrganization.organization_id, view: 'schedule' });
    }

      /**
       * this = $scope from controller - widgetIsExported.bind($scope) is used in traffic.controller and hourly.controller
       * @param metricKey
       * @param groupBy
       * @returns {*}
       */
    function widgetIsExported(metricKey, params) {
      // widgetIsExported.bind($scope) is used in traffic.controller and hourly.controller
      var dateRangeKey =
        this.dateRange.start +
        ' - ' +
        this.dateRange.end +
        ' - ' +
        this.compareRange1.start +
        ' - ' +
        this.compareRange1.end +
        ' - ' +
        this.compareRange2.start +
        ' - ' +
        this.compareRange2.end;

      var paramsToCompare = widgetVariables()[metricKey];

      return ExportService.isInExportCartWithSettings(getAreaKey(), dateRangeKey, metricKey, params, paramsToCompare)
    }


    return {
      isShowMetrics: isShowMetrics,
      hasZonesWithType: hasZonesWithType,
      mapZoneType: mapZoneType,
      filterLists: filterLists,
      initExportParam: initExportParam,
      setCurrentOrganization: setCurrentOrganization,
      setCurrentZone: setCurrentZone,
      setCurrentSite: setCurrentSite,
      setMetricsToShow: setMetricsToShow,
      getAreaKey: getAreaKey,
      scheduleExportCurrentViewToPdf: scheduleExportCurrentViewToPdf,
      widgetIsExported: widgetIsExported,
      widgetVariables: widgetVariables
    }
  }])
})();
