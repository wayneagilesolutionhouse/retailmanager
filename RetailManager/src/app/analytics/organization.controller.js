(function() {
  'use strict';

  angular.module('shopperTrak')
    .controller('OrganizationController', OrganizationController);

  OrganizationController.$inject = [
    '$scope',
    '$state',
    'currentOrganization',
    'sites',
    'SubscriptionsService'
  ];

  function OrganizationController(
    $scope,
    $state,
    currentOrganization,
    sites,
    SubscriptionsService
  ) {
    var vm = this;

    vm.sites = sites;

    activate();

    function activate() {
      $scope.$on('$stateChangeSuccess', redirectIfNecessary);
    }

    function redirectIfNecessary() {
      if ($state.current.name !== 'analytics.organization') {
        return;
      }

      if (SubscriptionsService.onlyMiSubscription(currentOrganization, sites)) {
        $state.go('analytics.organization.marketIntelligence.dashboard');
      } else if (sites.length > 1) {
        if (currentOrganization.portal_settings !== undefined &&
          currentOrganization.portal_settings.organization_type !== undefined &&
          currentOrganization.portal_settings.organization_type === 'Retail'
        ) {
          $state.go('analytics.organization.retail');
        } else {
          $state.go('analytics.organization.summary');
        }
      } else if (sites.length === 1) {
        $state.go('analytics.organization.site', {
          orgId: currentOrganization.organization_id,
          siteId: sites[0].site_id
        });
      }
    }
  }
})();
