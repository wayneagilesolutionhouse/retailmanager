'use strict';

angular.module('shopperTrak')
.controller('LaborCtrl', [
  '$scope',
  '$rootScope',
  '$q',
  '$state',
  '$stateParams',
  'currentOrganization',
  'currentZone',
  'currentSite',
  'currentUser',
  'ExportService',
  'LocalizationService',
  'utils',
  'customDashboardService',
  'ObjectUtils',
  'widgetConstants',
function (
  $scope,
  $rootScope,
  $q,
  $state,
  $stateParams,
  currentOrganization,
  currentZone,
  currentSite,
  currentUser,
  ExportService,
  LocalizationService,
  utils,
  customDashboardService,
  ObjectUtils,
  widgetConstants
) {

  $scope.currentOrganization = currentOrganization;
  $scope.currentSite = currentSite;
  $scope.zones = currentSite.zones;
  $scope.currentZone = currentZone;
  $scope.dateRangesLoaded = dateRangesLoaded();
  $scope.scheduleExportCurrentViewToPdf = scheduleExportCurrentViewToPdf;
  $scope.kpiValues ={};
  $scope.viewData = {};
  $scope.isLoading = {};

  $scope.exportWidget = exportWidget;
  $scope.setSelectedWidget = setSelectedWidget;

  /* Date range picker sets the first day of week setting
     according to current user, calendar and organisation. */
  $rootScope.$watch('firstDaySetting', function() {
    $scope.firstDayOfWeek = $rootScope.firstDaySetting;
  });

  $scope.dateRange = {
    start: $stateParams.dateRangeStart,
    end: $stateParams.dateRangeEnd
  };

  $scope.compareRange1 = {
    start: $stateParams.compareRange1Start,
    end: $stateParams.compareRange1End
  };

  $scope.compareRange2 = {
    start: $stateParams.compareRange2Start,
    end: $stateParams.compareRange2End
  };

  $scope.zoneChange = $scope.$watch('currentZone', function() {
    $rootScope.groupBy = {};
  });

  $scope.metricsChange = $scope.$watchGroup(['metricsToShow', 'dateRangesLoaded', 'selectedWeatherMetrics','firstDayOfWeek','isLoading'], function() {
    if(viewIsLoaded()) {
      $scope.viewData = initializeViewData();
    }
  });

  $scope.$on('$destroy', function () {
    if (_.isFunction($scope.zoneChange)) {
      $scope.zoneChange();
    }
    if (_.isFunction($scope.metricsChange)) {
      $scope.metricsChange();
    }
  });

  LocalizationService.setUser(currentUser);
  $scope.dateFormat = LocalizationService.getCurrentDateFormat(currentOrganization);

  LocalizationService.getAllCalendars().then(function (calendars) {
    LocalizationService.setAllCalendars(calendars.result);
    LocalizationService.setOrganization(currentOrganization);
    $scope.compareRange1Type = utils.getCompareType($scope.compareRange1, $scope.dateRange, currentUser, currentOrganization);
    $scope.compareRange2Type = utils.getCompareType($scope.compareRange2, $scope.dateRange, currentUser, currentOrganization);
  });

  if (currentSite.type === 'Mall' && currentZone === null) {
    $scope.metricsToShow = [
      'tenant_labor_hours_table_widget',
      'tenant_star_labor_table_widget'
    ];
  } else {
    $scope.metricsToShow = [
      'labor_hours_widget',
      'star_labor_widget'
    ];
  }

  getKpiValuesForKpi();

  function getKpiValuesForKpi(){
        $scope.kpiValues.labor_hours_widget ='labor_hours';
        $scope.kpiValues.star_labor_widget ='star';
      }

  $scope.$on('scheduleExportCurrentViewToPdf', scheduleExportCurrentViewToPdf);

  function getAreaKey() {
    var areaKey = currentSite.organization.id + '_' + currentSite.site_id;
    if (currentZone) {
      areaKey += '_zone_' + currentZone.id;
    }
    return areaKey;
  }

  function exportWidget(metricKey, toDashboard) {
    initializeViewData();
    var params = $scope.viewData[metricKey];

    if (toDashboard) {
      customDashboardService.setSelectedWidget(params);
    } else {
      ExportService.createExportAndStore(params);
    }
  };

  $scope.widgetIsExported = function(metricKey, params) {
    var dateRangeKey =
      $scope.dateRange.start +
      ' - ' +
      $scope.dateRange.end +
      ' - ' +
      $scope.compareRange1.start +
      ' - ' +
      $scope.compareRange1.end +
      ' - ' +
      $scope.compareRange2.start +
      ' - ' +
      $scope.compareRange2.end;

    var paramsToCompare = widgetVariables()[metricKey];

    return ExportService.isInExportCartWithSettings(getAreaKey(), dateRangeKey, metricKey, params, paramsToCompare)
  };

  function setSelectedWidget(title) {
    exportWidget(title, true);
  }

  function scheduleExportCurrentViewToPdf() {

    _.each($scope.metricsToShow, function (value) {
      $scope.exportWidget(value);
    });

    $state.go('pdfexport', { orgId: currentOrganization.organization_id, view: 'schedule' });
  }

  function dateRangesLoaded() {
    return utils.urlDateParamsLoaded($stateParams);
  }

  function initializeViewData() {

    var configuration = {};

    _.each($scope.metricsToShow, function(metricKey) {
      configuration[metricKey] = initExportParam(metricKey);
    });

    return configuration;
  }

  function initExportParam(metricKey) {
    var params;

    params = {
      orgId: currentSite.organization.id  ,
      siteId: currentSite.site_id,
      dateRange: { start: $scope.dateRange.start, end: $scope.dateRange.end },
      dateRangeType: utils.getDateRangeType($scope.dateRange, currentUser, currentOrganization),
      compare1Range: { start: $scope.compareRange1.start, end: $scope.compareRange1.end },
      compare2Range: { start: $scope.compareRange2.start, end: $scope.compareRange2.end },
      compare1Type: $scope.compareRange1Type,
      compare2Type: $scope.compareRange2Type,
      dateFormat: $scope.dateFormat,
      language: $scope.language,
      firstDayOfWeekSetting: $scope.firstDayOfWeek,
      name: metricKey,
      dateRangeShortCut: $state.rangeSelected,
      customRange:  $state.customRange,
      groupBy: 'day' //default
    };

    if(metricKey === 'tenant_star_labor_table_widget' || metricKey === 'tenant_labor_hours_table_widget') {
      params.filterQuery = $scope.viewData[metricKey].zoneFilterQuery;
      params.orderBy = $scope.viewData[metricKey].sortType;
    }

    if(params.dateRangeShortCut === 'custom' && params.customRange === null) {
      params.xDaysBack = moment().utc().diff(params.dateRange.start, 'days');
      params.xDaysDuration = params.dateRange.end.diff(params.dateRange.start, 'days');
    }

    if (currentZone) {
      params.zoneId = currentZone.id;
    }

    if(metricKey === 'labor_hours_widget' && !ObjectUtils.isNullOrUndefined($scope.viewData[metricKey])) {
      params.groupBy = $scope.viewData[metricKey].groupBy || 'day';
    }

    if(metricKey === 'star_labor_widget' && !ObjectUtils.isNullOrUndefined($scope.viewData[metricKey])) {
      params.groupBy = $scope.viewData[metricKey].groupBy || 'day';
    }

    if(metricKey === 'labor_hours_widget' || metricKey === 'star_labor_widget') {
      params.kpi = $scope.kpiValues[metricKey]
    }

    return params;
  }

  function viewIsLoaded() {
    return !ObjectUtils.isNullOrUndefinedOrEmpty($scope.metricsToShow) &&
           $scope.dateRangesLoaded;
  }

  function widgetVariables() {
    return widgetConstants.exportProperties;
  }

}]);
