'use strict';

describe('TrafficCtrl', function() {
  var $scope;
  var $controller;
  var LocalizationService;

  var currentUserMock = {
    localization: {date_format: 'MM/DD/YYYY'},
    preferences: {
      custom_dashboards:[],
      weather_reporting:false
    }
  };

  it('should add currentZone object to scope', function() {
    var orgId = 10;
    var siteId = 100;
    var zoneId = 1000;

    var currentOrganizationMock = {
      'organization_id': orgId,
      'subscriptions': {
        'interior': true,
        'perimeter': true
      },
      'portal_settings': {
        'organization_type': 'Mall'
      }
    };

    var currentSiteMock = {
      'site_id': siteId,
      'organization': {
        'id': orgId
      }
    };

    var currentZoneMock = {
      'id': zoneId
    };

    $controller('TrafficCtrl', {
      '$scope': $scope,
      '$stateParams': {
        'orgId': orgId,
        'siteId': siteId,
        'zoneId': zoneId
      },
      'currentOrganization': currentOrganizationMock,
      'currentSite': currentSiteMock,
      'currentZone': currentZoneMock,
      'currentUser': currentUserMock
    });

    expect($scope.currentZone).toBe(currentZoneMock);
  });

  it('should show the correct set of widgets for site level with sites that have perimeter', function() {
    var orgId = 10;
    var siteId = 100;

    var currentOrganizationMock = {
      'organization_id': orgId,
      'subscriptions': {
        'interior': true,
        'perimeter': true
      },
      'portal_settings': {
        'organization_type': 'Mall'
      }
    };

    var currentSiteMock = {
      'site_id': siteId,
      'organization': {
        'id': orgId
      },
      'zones': [
        {
          'name': 'Entrance 1',
          'type': 'Entrance'
        },
        {
          'name': 'Tenant 1',
          'type': 'TenantCommon'
        },
        {
          'name': 'Other zone',
          'type': 'MallPerim'
        }
      ]
    };

    $controller('TrafficCtrl', {
      '$scope': $scope,
      '$stateParams': {
        'orgId': orgId,
        'siteId': siteId
      },
      'currentOrganization': currentOrganizationMock,
      'currentSite': currentSiteMock,
      'currentZone': null,
      'currentUser': currentUserMock
    });

    expect($scope.metricsToShow).toEqual([
      'traffic',
      'entrance_contribution',
      'entrance_contribution_pie',
      'power_hours',
      'daily_performance_widget',
      'traffic_per_weekday',
      'tenant_traffic_table_widget',
      'other_areas_traffic_table_widget'
    ]);
  });

  it('should not show Tenant summary if site has no tenant zones', function() {
    var orgId = 10;
    var siteId = 100;

    var currentOrganizationMock = {
      'organization_id': orgId,
      'subscriptions': {
        'interior': true,
        'perimeter': true
      },
      'portal_settings': {
        'organization_type': 'Mall'
      }
    };

    var currentSiteMock = {
      'site_id': siteId,
      'organization': {
        'id': orgId
      },
      'zones': [
        {
          'name': 'Entrance 1',
          'type': 'Entrance'
        },
        {
          'name': 'Other zone',
          'type': 'MallPerim'
        }
      ]
    };

    $controller('TrafficCtrl', {
      '$scope': $scope,
      '$stateParams': {
        'orgId': orgId,
        'siteId': siteId
      },
      'currentOrganization': currentOrganizationMock,
      'currentSite': currentSiteMock,
      'currentZone': null,
      'currentUser': currentUserMock
    });

    expect($scope.metricsToShow).toEqual([
      'traffic',
      'entrance_contribution',
      'entrance_contribution_pie',
      'power_hours',
      'daily_performance_widget',
      'traffic_per_weekday',
      'other_areas_traffic_table_widget'
    ]);
  });

  it('should show the correct set of widgets for site level with sites that do not have perimeter', function() {
    var orgId = 10;
    var siteId = 100;

    var currentOrganizationMock = {
      'organization_id': orgId,
      'subscriptions': {
        'interior': true,
        'perimeter': false
      },
      'portal_settings': {
        'organization_type': 'Mall'
      }
    };

    var currentSiteMock = {
      'site_id': siteId,
      'organization': {
        'id': orgId
      }
    };

    $controller('TrafficCtrl', {
      '$scope': $scope,
      '$stateParams': {
        'orgId': orgId,
        'siteId': siteId
      },
      'currentOrganization': currentOrganizationMock,
      'currentSite': currentSiteMock,
      'currentZone': null,
      'currentUser': currentUserMock
    });

    expect($scope.metricsToShow).toEqual([
      'traffic',
      'traffic_per_weekday'
    ]);
  });


  it('should show the correct set of widgets for zone level', function() {
    var orgId = 10;
    var siteId = 100;
    var zoneId = 1000;

    var currentOrganizationMock = {
      'organization_id': orgId,
      'subscriptions': {
        'interior': true,
        'perimeter': true
      },
      'portal_settings': {
        'organization_type': 'Mall'
      }
    };

    var currentSiteMock = {
      'site_id': siteId,
      'organization': {
        'id': orgId
      }
    };

    var currentZoneMock = {
      'id': zoneId
    };

    $controller('TrafficCtrl', {
      '$scope': $scope,
      '$stateParams': {
        'orgId': orgId,
        'siteId': siteId,
        'zoneId': zoneId
      },
      'currentOrganization': currentOrganizationMock,
      'currentSite': currentSiteMock,
      'currentZone': currentZoneMock,
      'currentUser': currentUserMock
    });

    expect($scope.metricsToShow).toEqual([
      'traffic',
      'entrance_contribution',
      'entrance_contribution_pie',
      'power_hours',
      'daily_performance_widget',
      'traffic_per_weekday'
    ]);
  });

  beforeEach(module('shopperTrak'));
  beforeEach(inject(function($rootScope, _$controller_,_LocalizationService_) {
    $scope = $rootScope.$new();
    $controller = _$controller_;

    $scope.vm = {};
    $scope.vm.customDashboards = function() {};

    LocalizationService = _LocalizationService_;
    spyOn(LocalizationService, 'getCurrentDateFormat');
  }));
});
