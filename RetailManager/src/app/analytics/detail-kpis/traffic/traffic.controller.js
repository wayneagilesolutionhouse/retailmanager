'use strict';

angular.module('shopperTrak')
  .controller('TrafficCtrl', [
    '$scope',
    '$rootScope',
    '$q',
    '$state',
    '$stateParams',
    'currentOrganization',
    'currentZone',
    'currentSite',
    'currentUser',
    'ExportService',
    'SubscriptionsService',
    'MallCheckService',
    'LocalizationService',
    'utils',
    'ObjectUtils',
    'metricConstants',
    'customDashboardService',
    'trafficViewService',
    function (
      $scope,
      $rootScope,
      $q,
      $state,
      $stateParams,
      currentOrganization,
      currentZone,
      currentSite,
      currentUser,
      ExportService,
      SubscriptionsService,
      MallCheckService,
      LocalizationService,
      utils,
      ObjectUtils,
      metricConstants,
      customDashboardService,
      trafficViewService
    ) {
      $scope.currentOrganization = currentOrganization;
      $scope.currentSite = currentSite;
      $scope.zones = currentSite.zones;
      $scope.currentZone = currentZone;
      $scope.currentUser = currentUser;
      $scope.isLoading = {};
      var _currentMetric = 'traffic';
      activate();

      function activate() {
        trafficViewService.setCurrentOrganization(currentOrganization);
        trafficViewService.setCurrentZone(currentZone);
        trafficViewService.setCurrentSite(currentSite);
        $rootScope.customDashboards = false;
        $scope.updateSelectedWeatherMetrics = updateSelectedWeatherMetrics;
        $scope.dateRangesLoaded = dateRangesLoaded();
        $scope.language = LocalizationService.getCurrentLocaleSetting();
        $scope.exportWidget = exportWidget;
        $scope.setSelectedWidget = setSelectedWidget;

        $scope.vm.customDashboards = customDashboardService.getDashboards(currentUser);

        $scope.vm.chartSegments = null;

        $scope.showPerfWidget = MallCheckService.isNotMall(currentSite, currentZone);

        $scope.showWeatherMetrics = !ObjectUtils.isNullOrUndefined($scope.currentSite) &&
          $scope.currentUser.preferences.weather_reporting;

        $scope.showPerfWidget = MallCheckService.isNotMall(currentSite, currentZone);

        $scope.kpi = {
          name: 'traffic'
        };

        $scope.salesCategoriesTraffic = {
          selection: []
        };

        $scope.groupBy = 'day';

        $scope.salesCategoriesTraffic = {
          selection: []
        };

        $scope.salesCategoriesDailyPerf = {
          selection: []
        };

        $scope.salesCategoriesTrafficWeekday = {
          selection: []
        };

        $scope.trafficOption = {};

        setWatches();

        $scope.dateRange = {
          start: $stateParams.dateRangeStart,
          end: $stateParams.dateRangeEnd
        };

        $scope.compareRange1 = {
          start: $stateParams.compareRange1Start,
          end: $stateParams.compareRange1End
        };

        $scope.compareRange2 = {
          start: $stateParams.compareRange2Start,
          end: $stateParams.compareRange2End
        };

        LocalizationService.getAllCalendars().then(function (calendars) {
          LocalizationService.setAllCalendars(calendars.result);
          LocalizationService.setOrganization($scope.currentOrganization);
          LocalizationService.setUser($scope.currentUser);
          $scope.compareRange1Type = utils.getCompareType($scope.compareRange1, $scope.dateRange, $scope.currentUser, $scope.currentOrganization);
          $scope.compareRange2Type = utils.getCompareType($scope.compareRange2, $scope.dateRange, $scope.currentUser, $scope.currentOrganization);
        });

        if ($stateParams.businessDays === undefined) {
          $scope.operatingHours = true;
        } else {
          if ($stateParams.businessDays === 'false') {
            $scope.operatingHours = true;
          } else {
            $scope.operatingHours = false;
          }
        }

        LocalizationService.setUser(currentUser);

        $scope.dateFormat = LocalizationService.getCurrentDateFormat($scope.currentOrganization);

        $scope.isRetail = currentOrganization.portal_settings.organization_type === 'Retail';

        $scope.metricsToShow = [];

        if (currentZone) {
          $scope.metricsToShow = [
            'traffic',
            'entrance_contribution',
            'entrance_contribution_pie',
            'power_hours',
            'daily_performance_widget',
            'traffic_per_weekday'
          ];
          $scope.hasTenantZone = currentZone.type === 'TenantCommon';
        } else if (currentSite && SubscriptionsService.siteHasPerimeter(currentOrganization, currentSite)) {
          var tenant = ['TenantCommon'];
          var otherAreas = ['TenantCommon', 'Entrance', 'TotalProp'];

          $scope.metricsToShow = [
            'traffic',
            'entrance_contribution',
            'entrance_contribution_pie',
            'power_hours',
            'daily_performance_widget',
            'traffic_per_weekday'];

          if (trafficViewService.hasZonesWithType(tenant)) {
            $scope.metricsToShow.push('tenant_traffic_table_widget');
          }

          // Other areas widget should contain all types
          // except the ones listed in otherAreas variable
          if (trafficViewService.hasZonesWithType(otherAreas, true)) {
            $scope.metricsToShow.push('other_areas_traffic_table_widget');
          }
        } else if (currentSite && !SubscriptionsService.siteHasPerimeter(currentOrganization, currentSite)) {
          $scope.metricsToShow = ['traffic', 'traffic_per_weekday'];
        }

        if ($scope.isRetail || $scope.hasTenantZone) {
          $scope.metricsToShow.unshift('kpi_summary_widget_container');
        }

        trafficViewService.setMetricsToShow($scope.metricsToShow);

        $scope.showMetrics = trafficViewService.isShowMetrics();

        $scope.siteHasLabor = SubscriptionsService.siteHasLabor(currentOrganization, currentSite);

        $scope.siteHasSales = SubscriptionsService.siteHasSales(currentOrganization, currentSite);

      }

      function goToHourlyDrilldown(event, data) {
        $state.go('analytics.organization.site.hourly', {
          orgId: $stateParams.orgId,
          siteId: $stateParams.siteId,
          day: data.label,
          compareRange1Start: event.currentScope.compareRange1.start,
          compareRange1End: event.currentScope.compareRange1.end,
          compareRange2Start: event.currentScope.compareRange2.start,
          compareRange2End: event.currentScope.compareRange2.end,
          dateRangeStart: event.currentScope.dateRange.start,
          dateRangeEnd: event.currentScope.dateRange.end
        });
      }


      function setWatches() {
        /* Date range picker sets the first day of week setting
          according to current user, calendar and organisation. */
        $scope.currentSiteChange = $rootScope.$watch('currentSite', function () {
          if( ObjectUtils.isNullOrUndefined($scope.viewData)) {
            $scope.viewData = {};
          }
          if( ObjectUtils.isNullOrUndefined($scope.viewData.power_hours)) {
            $scope.viewData.power_hours = {};
          }
          $scope.viewData.power_hours.displayType = '';
          $scope.vm.savedTrafficClasses = [];
        });

        $scope.firstDaySettingChange = $rootScope.$watch('firstDaySetting', function () {
          $scope.firstDayOfWeek = $rootScope.firstDaySetting;
        });

        $scope.zoneChange = $scope.$watch('currentZone', function() {
          $rootScope.groupBy = {};
        });

        $scope.metricsChange = $scope.$watchGroup(['metricsToShow','dateRangesLoaded', 'selectedWeatherMetrics','firstDayOfWeek','isLoading'], function() {
          if(viewIsLoaded()) {
            $scope.viewData = initializeViewData();
          }
        });

        $scope.$on('$destroy', function () {
          if (_.isFunction($scope.currentSiteChange)) {
            $scope.currentSiteChange();
          }
          if (_.isFunction($scope.firstDaySettingChange)) {
            $scope.firstDaySettingChange();
          }
          if (_.isFunction($scope.zoneChange)) {
            $scope.zoneChange();
          }

          if (_.isFunction($scope.metricsChange)) {
            $scope.metricsChange();
          }

        });

        $scope.$on('scheduleExportCurrentViewToPdf', trafficViewService.scheduleExportCurrentViewToPdf.bind(null, $scope));
        $scope.$on('goToHourlyDrilldown', goToHourlyDrilldown);
      }

      $scope.onSelectOption = function(_option) {
        var itemIndex = $scope.metricsToShow.indexOf(_currentMetric);
        var metric = _currentMetric;

        if(itemIndex > -1) {
          $scope.metricsToShow[itemIndex] = _option.name;
          _currentMetric = _option.name;
          $scope.viewData[_option.name] = angular.copy($scope.viewData[metric]);
          $scope.viewData[_option.name].name = _option.name;
          $scope.viewData[_option.name].kpi = _option.name;
          $scope.viewData[_option.name].selectedOption = _option;
        }
      }

      function updateParamsForKpiSummary(params) {
        params.orgId = $scope.currentSite.organization.id;
        params.siteId = $scope.currentSite.site_id;
        params.zoneId = !ObjectUtils.isNullOrUndefined($scope.currentZone) ? $scope.currentZone.id : undefined;
        params.dateRange = $scope.dateRange;
        params.compareRange1 = { start: $scope.compareRange1.start, end: $scope.compareRange1.end };
        params.compareRange1Type = $scope.compareRange1Type;
        params.compareRange2 = { start: $scope.compareRange2.start, end: $scope.compareRange2.end };
        params.compareRange2Type = $scope.compareRange2Type;
        params.operatingHours = $scope.operatingHours;
        params.firstDayOfWeekSetting = $scope.firstDayOfWeek;
        params.siteHasLabor = $scope.siteHasLabor;
        params.siteHasSales = $scope.siteHasSales;
        return params;
      }

      function updateParamsForPerWeekDay(params) {
        params.selectedMetric = $scope.viewData[params.name].selectedMetric;
        params.orderTable = $scope.viewData[params.name].orderTable;
        params.orderReverse = $scope.viewData[params.name].orderReverse;
        params.showTable = $scope.viewData[params.name].showTable;
        params.salesCategories = $scope.salesCategoriesTrafficWeekday.selection;
        params.currentZone = $scope.currentZone;
        params.organizationId = '' + $scope.currentOrganization.organization_id;
        params.selectedDays = $scope.viewData[params.name].selectedDays;

        if(typeof ObjectUtils.getNestedProperty($scope, 'viewData[params.name].selectedDays') !== 'undefined' &&
           $scope.viewData[params.name].selectedDays.length < 7){
          //don't include unless needed load all from widget
          params.selectedDays = $scope.viewData[params.name].selectedDays;
          _.each(params.selectedDays, function(day){
            delete day.transkey;
          })
        }

        return params;
      }

      function updateParamsForPerformanceDaily(params) {
        if(typeof ObjectUtils.getNestedProperty($scope, 'viewData.daily_performance_widget.selectedDaysDailyPerformance') === 'undefined') {
          return params;
        }
        params.showTable = $scope.viewData[params.name].showTable;
        params.orderTable = $scope.viewData[params.name].orderTable;
        params.orderDirection = $scope.viewData[params.name].orderDirection;
        if($scope.viewData[params.name].selectedDays.length < 7){ //don't include unless needed load all from widget
          params.selectedDays = $scope.viewData[params.name].selectedDays;
          _.each(params.selectedDays, function(day){
            delete day.transkey;
          })
        }
        params.salesCategories = $scope.salesCategories;
        return params;
      }

      function updateParamsForEntranceZone(params, metricKey) {
        if(typeof ObjectUtils.getNestedProperty($scope, 'viewData['+metricKey+']') === 'undefined') {
          return params;
        }

        params.filterQuery = $scope.viewData[metricKey].zoneFilterQuery;
        params.orderBy = $scope.viewData[metricKey].sortType;
        params.comparisonColumnIndex = $scope.viewData[metricKey].comparisonColumnIndex;
        params.childProperty = $scope.viewData[metricKey].childProperty;

        return params;
      }

      function updateParamsForTraffic(params, metricKey) {
        if(metricKey === 'sales' || metricKey === 'conversion' || metricKey === 'ats') {
            params.salesCategories = $scope.salesCategoriesTraffic.selection;
        }
        if(ObjectUtils.isNullOrUndefined) {
          //make sure it gets pdf traffic partial
          params.partialPageName = 'traffic';
        }
        params.showMetrics = $scope.showMetrics;
        params.selectedOption = $scope.viewData[params.name].selectedOption;

        if(params.showWeatherMetrics){
          params.weather = [];
          _.each(params.selectedWeatherMetrics, function(weatherMetric){
            params.weather.push(weatherMetric.kpi);
          });
          delete params.selectedWeatherMetrics;
        }

        return params;
      }

      function exportWidget(metricKey, toDashboard) {
        initializeViewData();
        var params = $scope.viewData[metricKey];

        if (toDashboard) {
          customDashboardService.setSelectedWidget(params);
        } else {
          ExportService.createExportAndStore(params);
        }

      }

      function getGroupBy(metricKey) {
        var groupByKey = metricKey;
        if(metricKey === 'traffic' && metricKey !== $scope.kpi.name) {
          groupByKey = $scope.kpi.name;
        }
        return $rootScope.groupBy[$state.current.views.analyticsMain.controller.toString() + '-' + groupByKey] || $scope.viewData[metricKey].groupBy || 'day';
      }

      $scope.widgetIsExported = trafficViewService.widgetIsExported.bind($scope);
      $scope.widgetVariables = trafficViewService.widgetVariables.bind($scope);

      function initializeViewData() {

        // When loading the view, create object holding all widget configuration variables.
        // This object is used when storing export or when determinating if widget is already in the export cart
        // with current configuration.

        var configuration = {};

        _.each($scope.metricsToShow, function(metricKey) {

          var params = trafficViewService.initExportParam(metricKey, $scope);

          switch (metricKey) {
            case 'kpi_summary_widget_container':
              params = updateParamsForKpiSummary(params);
              break;
            case 'entrance_contribution':
            case 'zone_traffic_summary':
            case 'entrance_contribution_pie':
            case 'other_areas_traffic_table_widget':
              params = updateParamsForEntranceZone(params, metricKey);
              break;
            case 'tenant_traffic_table_widget':
              params = updateParamsForEntranceZone(params, metricKey);
              break;
            case 'power_hours':
              params.displayType = $scope.viewData[metricKey].displayType;
              params.savedTrafficClasses = $scope.viewData[metricKey].savedTrafficClasses;
              break;
            case 'traffic_per_weekday':
              params = updateParamsForPerWeekDay(params);
              break;
            case 'daily_performance_widget':
              params = updateParamsForPerformanceDaily(params);
              break;
            default:
              params = updateParamsForTraffic(params, metricKey);
          }

          if(!ObjectUtils.isNullOrUndefined($scope.viewData[metricKey])) {
            params.groupBy = getGroupBy(metricKey);
          }

          configuration[metricKey] = params;
        });

        return configuration;
      }

      function setSelectedWidget(title) {
        exportWidget(title, true);
      }

      function dateRangesLoaded() {
        return utils.urlDateParamsLoaded($stateParams);
      }

      function updateSelectedWeatherMetrics(metrics) {
        $scope.selectedWeatherMetrics = metrics;
      }

      function viewIsLoaded() {
        return !ObjectUtils.isNullOrUndefinedOrEmpty($scope.metricsToShow) &&
               !ObjectUtils.isNullOrUndefined($scope.selectedWeatherMetrics) &&
               $scope.dateRangesLoaded
      }

    }]);
