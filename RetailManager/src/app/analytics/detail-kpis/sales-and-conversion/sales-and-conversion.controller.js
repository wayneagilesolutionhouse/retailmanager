'use strict';

angular.module('shopperTrak')
.controller('SalesAndConversionCtrl', [
  '$scope',
  '$rootScope',
  '$state',
  '$stateParams',
  'currentSite',
  'ExportService',
  'currentOrganization',
  'currentZone',
  'currentUser',
  'LocalizationService',
  'utils',
  'metricConstants',
  'customDashboardService',
  'ObjectUtils',
  'widgetConstants',
function(
  $scope,
  $rootScope,
  $state,
  $stateParams,
  currentSite,
  ExportService,
  currentOrganization,
  currentZone,
  currentUser,
  LocalizationService,
  utils,
  metricConstants,
  customDashboardService,
  ObjectUtils,
  widgetConstants
) {
  $scope.currentOrganization = currentOrganization;
  $scope.currentSite = currentSite;
  $scope.currentUser = currentUser;
  $scope.dateRangesLoaded = dateRangesLoaded();
  $scope.scheduleExportCurrentViewToPdf = scheduleExportCurrentViewToPdf;
  $scope.kpiValues={};
  $scope.viewData = {};
  $scope.isLoading = {};

  /* Date range picker sets the first day of week setting
     according to current user, calendar and organisation. */
  $rootScope.$watch('firstDaySetting', function() {
    $scope.firstDayOfWeek = $rootScope.firstDaySetting;
  });

  $scope.dateRange = {
    start: $stateParams.dateRangeStart,
    end: $stateParams.dateRangeEnd
  };

  $scope.compareRange1 = {
    start: $stateParams.compareRange1Start,
    end: $stateParams.compareRange1End
  };

  $scope.compareRange2 = {
    start: $stateParams.compareRange2Start,
    end: $stateParams.compareRange2End
  };

  $scope.zoneChange = $scope.$watch('currentZone', function() {
    $rootScope.groupBy = {};
  });

  $scope.metricsChange = $scope.$watchGroup(['metricsToShow', 'dateRangesLoaded', 'firstDayOfWeek', 'isLoading'], function() {
    if(viewIsLoaded()) {
      $scope.viewData = initializeViewData();
    }
  });

  $scope.$on('$destroy', function () {
    if (_.isFunction($scope.zoneChange)) {
      $scope.zoneChange();
    }
    if (_.isFunction($scope.metricsChange)) {
      $scope.metricsChange();
    }
  });

  LocalizationService.setUser(currentUser);
  LocalizationService.setOrganization(currentOrganization);
  $scope.dateFormat = LocalizationService.getCurrentDateFormat(currentOrganization);
  $scope.numberFormat = {
    name: LocalizationService.getCurrentNumberFormatName(currentUser, currentOrganization)
  };
  $scope.language = LocalizationService.getCurrentLocaleSetting();

  LocalizationService.getAllCalendars().then(function (calendars) {
    LocalizationService.setAllCalendars(calendars.result);
    $scope.compareRange1Type = utils.getCompareType($scope.compareRange1, $scope.dateRange, $scope.currentUser, $scope.currentOrganization);
    $scope.compareRange2Type = utils.getCompareType($scope.compareRange2, $scope.dateRange, $scope.currentUser, $scope.currentOrganization);
  });

  if(currentZone !== null) {
    $scope.currentZone = currentZone;
  }

  $scope.$on('scheduleExportCurrentViewToPdf', scheduleExportCurrentViewToPdf);

  if (currentSite.type === 'Mall' && currentZone === null) {
    $scope.metricsToShow = [
      'tenant_sales_table_widget',
      'tenant_conversion_table_widget',
      'tenant_ats_table_widget',
      'tenant_upt_table_widget'
    ];
  } else {
    $scope.metricsToShow = [
      'sales_widget',
      'conversion_widget',
      'ats_sales_widget',
      'upt_sales_widget'
    ];
  }

  getKpiValuesForKpi();

  function getKpiValuesForKpi(){
        var kpiValues = {
          sales_widget : {name : 'sales'},
          conversion_widget  : {name : 'conversion'},
          ats_sales_widget : {name : 'ats'},
          upt_sales_widget : {name : 'upt'}
        };
        $scope.kpiValues=angular.copy(kpiValues);
  }


  function scheduleExportCurrentViewToPdf() {
    _.each($scope.metricsToShow, function (value) {
      $scope.exportWidget(value);
    });

    $state.go('pdfexport', { orgId: currentOrganization.organization_id, view: 'schedule' });
  }

  function getAreaKey() {
    var areaKey = currentSite.organization.id + '_' + currentSite.site_id;
    if (currentZone) {
      areaKey += '_zone_' + currentZone.id;
    }
    return areaKey;
  }

  $scope.exportWidget = function(metricKey, toDashboard) {
    initializeViewData();
    var params = $scope.viewData[metricKey];

    if(toDashboard === true) {
      params.currentView = 'analytics.organization.site.sales-and-conversion';
      customDashboardService.setSelectedWidget(params);
    } else {
      ExportService.createExportAndStore(params);
    }
  };

  $scope.widgetIsExported = function(metricKey, params) {
    var dateRangeKey =
      $scope.dateRange.start +
      ' - ' +
      $scope.dateRange.end +
      ' - ' +
      $scope.compareRange1.start +
      ' - ' +
      $scope.compareRange1.end +
      ' - ' +
      $scope.compareRange2.start +
      ' - ' +
      $scope.compareRange2.end;

    var paramsToCompare = widgetVariables()[metricKey];

    return ExportService.isInExportCartWithSettings(getAreaKey(), dateRangeKey, metricKey, params, paramsToCompare)
  };

  function dateRangesLoaded() {
    return utils.urlDateParamsLoaded($stateParams);
  }

  function initializeViewData() {
    var configuration = {};

    _.each($scope.metricsToShow, function(metricKey) {
      configuration[metricKey] = initExportParam(metricKey);
    });

    return configuration;
  }

  function initExportParam(metricKey) {
    var params;
    var tab = $state.current.views.analyticsMain.controller.toString();
    params = {
      orgId: currentSite.organization.id  ,
      siteId: currentSite.site_id,
      dateRange: { start: $scope.dateRange.start, end: $scope.dateRange.end },
      dateRangeType: utils.getDateRangeType($scope.dateRange, currentUser, currentOrganization),
      compare1Range: { start: $scope.compareRange1.start, end: $scope.compareRange1.end },
      compare2Range: { start: $scope.compareRange2.start, end: $scope.compareRange2.end },
      compare1Type: $scope.compareRange1Type,
      compare2Type: $scope.compareRange2Type,
      numberFormat: $scope.numberFormat,
      dateFormat: $scope.dateFormat,
      dateRangeShortCut: $state.rangeSelected,
      customRange:  $state.customRange,
      name: metricKey,
      language: $scope.language,
      firstDayOfWeekSetting: $scope.firstDayOfWeek,
      operatingHours: $scope.operatingHours,
      currencySymbol: _.findWhere(metricConstants.metrics, {isCurrency: true}).prefixSymbol,
      groupBy: 'day'
    };

    if(params.dateRangeShortCut === 'custom' && params.customRange === null) {
      params.xDaysBack = moment().utc().diff(params.dateRange.start, 'days');
      params.xDaysDuration = params.dateRange.end.diff(params.dateRange.start, 'days');
    }

    if (currentZone) {
      params.zoneId = currentZone.id;
    }

    if(typeof ObjectUtils.getNestedProperty($scope, 'viewData['+metricKey+'].zoneFilterQuery') !== 'undefined') {
      params.filterQuery = $scope.viewData[metricKey].zoneFilterQuery;
    }

    initializeSortTypeParams();

    params.orderBy = $scope.viewData[metricKey].sortType;

    initializeGroupByParams();

    if(metricKey === 'sales_widget') {
      params.groupBy = $rootScope.groupBy[tab + '-sales'] || $scope.viewData[metricKey].groupBy;
    }

    if(metricKey === 'conversion_widget') {
      params.groupBy = $rootScope.groupBy[tab + '-conversion'] || $scope.viewData[metricKey].groupBy;
    }

    if(metricKey === 'ats_sales_widget') {
      params.groupBy = $rootScope.groupBy[tab + '-ats'] || $scope.viewData[metricKey].groupBy;
    }

    if(metricKey === 'upt_sales_widget') {
      params.groupBy = $rootScope.groupBy[tab + '-upt'] || $scope.viewData[metricKey].groupBy;
    }

    return params;
  }

  function viewIsLoaded() {
    return !ObjectUtils.isNullOrUndefinedOrEmpty($scope.metricsToShow) &&
      $scope.dateRangesLoaded;
  }

  function widgetVariables() {
    return widgetConstants.exportProperties;
  }

  function initializeGroupByParams() {
    _.each($scope.metricsToShow, function(key) {
      if( ObjectUtils.isNullOrUndefined($scope.viewData[key])) {
        $scope.viewData[key] = {};
      }
      if( ObjectUtils.isNullOrUndefined($scope.viewData[key].groupBy)) {
        $scope.viewData[key].groupBy = 'day';
      }
    });
  }

  function initializeSortTypeParams() {
    _.each($scope.metricsToShow, function(key) {
      if( ObjectUtils.isNullOrUndefined($scope.viewData[key])) {
        $scope.viewData[key] = {};
      }
      if( ObjectUtils.isNullOrUndefined($scope.viewData[key].sortType)) {
        $scope.viewData[key].sortType = '-periodValues';
      }
    });
  }

}]);
