'use strict';

describe('SalesAndConversionCtrl', function() {
  var $scope;
  var $controller;
  var LocalizationService;
  var LocalizationServiceMock;

  it('should add currentZone object to scope', function() {
    var orgId = 10;
    var siteId = 100;
    var zoneId = 1000;

    var currentOrganizationMock = {
      'organization_id': orgId
    };

    var currentSiteMock = {
      'site_id': siteId,
      'organization': {
        'id': orgId
      }
    };

    var currentZoneMock = {
      'id': zoneId
    };

    var currentUserMock = {localization: {date_format: 'MM/DD/YYYY'}};

    $controller('SalesAndConversionCtrl', {
      '$scope': $scope,
      '$stateParams': {
        'orgId': orgId,
        'siteId': siteId,
        'zoneId': zoneId
      },
      'currentOrganization': currentOrganizationMock,
      'currentSite': currentSiteMock,
      'currentZone': currentZoneMock,
      'currentUser': currentUserMock
    });

    expect($scope.currentZone).toBe(currentZoneMock);
  });

  it('should show the correct set of widgets for site level with site with type of Mall', function() {
    var orgId = 10;
    var siteId = 100;

    var currentOrganizationMock = {
      'organization_id': orgId
    };

    var currentSiteMock = {
      'site_id': siteId,
      'organization': {
        'id': orgId
      },
      'type': 'Mall'
    };

    var currentUserMock = {localization: {date_format: 'MM/DD/YYYY'}};

    $controller('SalesAndConversionCtrl', {
      '$scope': $scope,
      '$stateParams': {
        'orgId': orgId,
        'siteId': siteId
      },
      'currentOrganization': currentOrganizationMock,
      'currentSite': currentSiteMock,
      'currentZone': null,
      'currentUser': currentUserMock
    });

    expect($scope.metricsToShow).toEqual([
      'tenant_sales_table_widget',
      'tenant_conversion_table_widget',
      'tenant_ats_table_widget',
      'tenant_upt_table_widget'
    ]);
  });

  it('should show the correct set of widgets for site level with sites with type of Retail', function() {
    var orgId = 10;
    var siteId = 100;

    var currentOrganizationMock = {
      'organization_id': orgId
    };

    var currentSiteMock = {
      'site_id': siteId,
      'organization': {
        'id': orgId
      },
      'type': 'Retail'
    };

    var currentUserMock = {localization: {date_format: 'MM/DD/YYYY'}};

    $controller('SalesAndConversionCtrl', {
      '$scope': $scope,
      '$stateParams': {
        'orgId': orgId,
        'siteId': siteId
      },
      'currentOrganization': currentOrganizationMock,
      'currentSite': currentSiteMock,
      'currentZone': null,
      'currentUser': currentUserMock
    });

    expect($scope.metricsToShow).toEqual([
      'sales_widget',
      'conversion_widget',
      'ats_sales_widget',
      'upt_sales_widget'
    ]);
  });


  it('should show the correct set of widgets for zone level', function() {
    var orgId = 10;
    var siteId = 100;
    var zoneId = 1000;

    var currentOrganizationMock = {
      'organization_id': orgId
    };

    var currentSiteMock = {
      'site_id': siteId,
      'organization': {
        'id': orgId
      },
      'type': 'Mall'
    };

    var currentZoneMock = {
      'id': zoneId
    };

    var currentUserMock = {localization: {date_format: {mask: 'MM/DD/YYYY'}}};

    $controller('SalesAndConversionCtrl', {
      '$scope': $scope,
      '$stateParams': {
        'orgId': orgId,
        'siteId': siteId,
        'zoneId': zoneId
      },
      'currentOrganization': currentOrganizationMock,
      'currentSite': currentSiteMock,
      'currentZone': currentZoneMock,
      'currentUser': currentUserMock
    });

    expect($scope.metricsToShow).toEqual([
      'sales_widget',
      'conversion_widget',
      'ats_sales_widget',
      'upt_sales_widget'
    ]);
  });

  beforeEach(module('shopperTrak'));
  beforeEach(module(function($provide) {
    LocalizationServiceMock = {
      getCurrentDateFormat: function() {
        return 'DD/MM/YYYY';
      },
      getAllCalendars: function() {
        return {};
      }
    };
  }));
  beforeEach(inject(function($rootScope, _$controller_, _LocalizationService_) {
    $scope = $rootScope.$new();
    $controller = _$controller_;
    LocalizationService = _LocalizationService_;
    spyOn(LocalizationService, 'getCurrentDateFormat');
  }));


});
