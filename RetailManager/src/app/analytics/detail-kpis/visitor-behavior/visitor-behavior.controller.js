(function() {
    'use strict';

    angular.module('shopperTrak')
      .controller('VisitorBehaviorCtrl', VisitorBehaviorCtrl);

    VisitorBehaviorCtrl.$inject = [
      '$scope',
      '$rootScope',
      '$state',
      '$stateParams',
      'currentLocation',
      'currentSite',
      'currentOrganization',
      'currentUser',
      'LocalizationService',
      '$translate',
      'ExportService',
      'utils',
      'customDashboardService',
      'ObjectUtils',
      'widgetConstants'
    ];

    function VisitorBehaviorCtrl(
      $scope,
      $rootScope,
      $state,
      $stateParams,
      currentLocation,
      currentSite,
      currentOrganization,
      currentUser,
      LocalizationService,
      $translate,
      ExportService,
      utils,
      customDashboardService,
      ObjectUtils,
      widgetConstants
    ) {
      var vm = this;
      vm.currentOrganization = currentOrganization;
      vm.currentSite = currentSite;
      vm.currentLocation = currentLocation;
      vm.currentUser = currentUser;
      vm.dateRangesLoaded = dateRangesLoaded();
      vm.kpiValues = {};
      vm.viewData = {};
      vm.isLoading = {};
      vm.language = LocalizationService.getCurrentLocaleSetting();
      loadTranslations();

      $scope.locationChange = $scope.$watch('vm.currentLocation', function() {
        $rootScope.groupBy = {};
      });

      $scope.metricsChange = $scope.$watchGroup(['vm.metricsToShow', 'vm.dateRangesLoaded', 'vm.selectedWeatherMetrics','vm.firstDayOfWeek','vm.isLoading'], function() {
        if(viewIsLoaded()) {
          vm.viewData = initializeViewData();
        }
      });

      $scope.$on('$destroy', function () {
        if (typeof $scope.locationChange === 'function') {
          $scope.locationChange();
        }
        if (_.isFunction($scope.metricsChange)) {
          $scope.metricsChange();
        }
      });

      /* Date range picker sets the first day of week setting
         according to current user, calendar and organisation. */
      $rootScope.$watch('firstDaySetting', function() {
        vm.firstDayOfWeek = $rootScope.firstDaySetting;
      });

      vm.dateRange = {
        start: $stateParams.dateRangeStart,
        end: $stateParams.dateRangeEnd
      };

      vm.compareRange1 = {
        start: $stateParams.compareRange1Start,
        end: $stateParams.compareRange1End
      };

      vm.compareRange2 = {
        start: $stateParams.compareRange2Start,
        end: $stateParams.compareRange2End
      };

      LocalizationService.setOrganization(currentOrganization);
      LocalizationService.setUser(currentUser);
      vm.dateFormat = LocalizationService.getCurrentDateFormat(currentOrganization);

      LocalizationService.getAllCalendars().then(function (calendars) {
        LocalizationService.setAllCalendars(calendars.result);
        vm.compareRange1Type = utils.getCompareType(vm.compareRange1, vm.dateRange, vm.currentUser, vm.currentOrganization);
        vm.compareRange2Type = utils.getCompareType(vm.compareRange2, vm.dateRange, vm.currentUser, vm.currentOrganization);
      });

      vm.metricsToShow = getMetricsToShow();
      getKpiValuesForKpi();
      if(currentLocation) {
        vm.currentLocationId = vm.currentLocation.location_id;
      } else if(vm.currentSite.location_id !== undefined) {
        vm.currentLocationId = vm.currentSite.location_id;
      }

      $scope.$on('scheduleExportCurrentViewToPdf', scheduleExportCurrentViewToPdf);

      function setDateGroup(_param, _widget) {
        var kpi = vm.kpiValues[_widget];
        var groupBy = $rootScope.groupBy[$state.current.views.analyticsMain.controller.toString() + '-' + kpi];

        if( !ObjectUtils.isNullOrUndefined(groupBy) ) {
          _param.groupBy = groupBy;
        }
      }

      function scheduleExportCurrentViewToPdf() {
        _.each(vm.metricsToShow, function(value) {
          var params = {
            name: value,
            dateFormat: vm.dateFormat,
            compare1Type: vm.compareRange1Type,
            compare2Type: vm.compareRange2Type
          };

          setDateGroup(params, value);
          ExportService.addToExportCartAndStore(getAreaKey(), vm.dateRange, vm.compareRange1, vm.compareRange2, params);
        });

        $state.go('pdfexport', {orgId: currentOrganization.organization_id, view: 'schedule'});
      }

      function getAreaKey() {
        var areaKey = currentSite.organization.id + '_' + currentSite.site_id;
        if (currentLocation) {
          areaKey += '_location_' + currentLocation.location_id;
        } else if(currentSite.location_id !== undefined) {
          areaKey += '_location_' + currentSite.location_id;
        }
        return areaKey;
      }

      vm.exportWidget = function(metricKey, toDashboard) {
        initializeViewData();
        var params = vm.viewData[metricKey];

        if (toDashboard) {
          customDashboardService.setSelectedWidget(params);
        } else {
          ExportService.createExportAndStore(params);
        }
      };

      function getGroupBy(metricKey) {
        return $rootScope.groupBy[$state.current.views.analyticsMain.controller.toString() + '-' + metricKey] || vm.viewData[metricKey].groupBy || 'day';
      }

      vm.widgetIsExported = function(metricKey, params) {
        var dateRangeKey =
          vm.dateRange.start +
          ' - ' +
          vm.dateRange.end +
          ' - ' +
          vm.compareRange1.start +
          ' - ' +
          vm.compareRange1.end +
          ' - ' +
          vm.compareRange2.start +
          ' - ' +
          vm.compareRange2.end;

        var paramsToCompare = widgetVariables()[metricKey];

        return ExportService.isInExportCartWithSettings(getAreaKey(), dateRangeKey, metricKey, params, paramsToCompare)
      };

      vm.setSelectedWidget = function(metricKey) {
        vm.exportWidget(metricKey, true);
      };

      function getMetricsToShow() {
        var widgets = [];

        if (currentLocation === null ) {
          widgets = [
            'visitor_behaviour_traffic',
            'loyalty',
            'gross_shopping_hours',
            'detail_dwell_time',
            'detail_opportunity',
            'detail_draw_rate'
          ];
          if(currentSite.type==='Retailer') {
            widgets.push('detail_abandonment_rate');
          } else {
            widgets.push('average_percent_shoppers');
          }
        } else if (currentLocation.location_type === 'Store') {
          widgets = [
            'visitor_behaviour_traffic',
            'loyalty',
            'gross_shopping_hours',
            'detail_dwell_time',
            'detail_opportunity',
            'detail_draw_rate',
            'detail_abandonment_rate'
          ];
        } else if (currentLocation.location_type === 'Floor') {
          widgets = [
            'visitor_behaviour_traffic',
            'detail_dwell_time',
            'gross_shopping_hours',
            'loyalty'
          ];
        } else if (currentLocation.location_type === 'Entrance' || currentLocation.location_type === 'Zone') {
          widgets = [
            'visitor_behaviour_traffic',
            'loyalty',
            'gross_shopping_hours',
            'detail_dwell_time',
            'detail_opportunity',
            'detail_draw_rate'
          ];
        } else {
          widgets = [
            'visitor_behaviour_traffic',
            'loyalty',
            'gross_shopping_hours',
            'detail_dwell_time'
          ];
        }
        return widgets;
      }

      function getKpiValuesForKpi(){
        vm.kpiValues.visitor_behaviour_traffic ='visitor_behaviour_traffic';
        vm.kpiValues.loyalty ='traffic';
        vm.kpiValues.gross_shopping_hours ='gsh';
        vm.kpiValues.detail_dwell_time ='dwell_time';
        vm.kpiValues.detail_opportunity ='opportunity';
        vm.kpiValues.detail_draw_rate ='draw_rate';
        vm.kpiValues.detail_abandonment_rate ='abandonment_rate';
        vm.kpiValues.average_percent_shoppers = 'average_percent_shoppers';
      }

      function dateRangesLoaded() {
        return utils.urlDateParamsLoaded($stateParams);
      }

      function loadTranslations() {
        $translate.use(vm.language);
      }

      function initializeViewData() {
        var configuration = {};
        initializeGroupByParams();

        _.each(vm.metricsToShow, function(metricKey) {
          configuration[metricKey] = initExportParam(metricKey);
        });

        return configuration;
      }

      function initExportParam(metricKey) {

        var params;

        params = {
          orgId: currentSite.organization.id,
          siteId: currentSite.site_id,
          dateRange: { start: vm.dateRange.start, end: vm.dateRange.end },
          dateRangeType: utils.getDateRangeType(vm.dateRange, currentUser, currentOrganization),
          compare1Range: { start: vm.compareRange1.start, end: vm.compareRange1.end },
          compare2Range: { start: vm.compareRange2.start, end: vm.compareRange2.end },
          compare1Type: vm.compareRange1Type,
          compare2Type: vm.compareRange2Type,
          dateFormat: vm.dateFormat,
          dateRangeShortCut: $state.rangeSelected,
          customRange:  $state.customRange,
          language: vm.language,
          firstDayOfWeekSetting: vm.firstDayOfWeek,
          name: metricKey,
          kpi: vm.kpiValues[metricKey]
        };

        setDateGroup(params, metricKey);

        if(params.dateRangeShortCut === 'custom' && params.customRange === null) {
          params.xDaysBack = moment().utc().diff(params.dateRange.start, 'days');
          params.xDaysDuration = params.dateRange.end.diff(params.dateRange.start, 'days');
        }

        if (currentLocation) {
          params.locationId = currentLocation.location_id;
        } else if(currentSite.location_id !== undefined) {
          params.locationId = currentSite.location_id;
        }

        if (metricKey === 'visitor_behaviour_traffic') {
          params.displayUniqueReturning = true;
        }

        params.groupBy = getGroupBy(metricKey);

        return params;
      }

      function viewIsLoaded() {
        return !ObjectUtils.isNullOrUndefinedOrEmpty(vm.metricsToShow) && vm.dateRangesLoaded;
      }

      function widgetVariables() {
        return widgetConstants.exportProperties;
      }

      function initializeGroupByParams() {
        _.each(vm.metricsToShow, function(key) {
          if( ObjectUtils.isNullOrUndefined(vm.viewData[key])) {
            vm.viewData[key] = {};
          }
          if( ObjectUtils.isNullOrUndefined(vm.viewData[key].groupBy)) {
            vm.viewData[key].groupBy = 'day';
          }
        });
      }

  }
})();
