'use strict';

describe('GraphCompareController', function() {
  var $scope;
  var $controller;
  var $stateParamsMock;

  var controllerConstants;
  var compareStateManagerMock;
  var currentSite;
  var currentOrganizationMock;
  var currentUserMock;
  var controller;

  beforeEach(module('shopperTrak', function($translateProvider) {
    $translateProvider.translations('en_US', {});
  }));

  beforeEach(inject(function($rootScope, _$controller_, _graphCompareControllerConstants_) {
    $scope = $rootScope.$new();
    $controller = _$controller_;
    controllerConstants = _graphCompareControllerConstants_;
    // Prevent accidentally using the same controller
    // instance in multiple it-blocks.
    controller = null;

    $stateParamsMock = {
      orgId: 1000,
      siteId: 10000
    };
    compareStateManagerMock = {
      loadWidgets: function() { return []; },
      saveWidgets: function() {}
    };

    currentOrganizationMock = {
      'organization_id': 123,
      'name': 'Foobar',
      'subscriptions': {
        'advanced': false,
        'campaigns': false,
        'labor': false,
        'sales': false,
        'market_intelligence': false,
        'qlik': false,
        'large_format_mall': true,
        'interior': false,
        'perimeter': true
      }
    };

    currentUserMock = {localization: {date_format: 'MM/DD/YYYY'}};

  }));

  it('should expose $stateParams', function() {
    instantiateController();
    expect(controller.stateParams).toBe($stateParamsMock);
  });

  it('should load widgets from compareStateWidgetManager service', function() {
    var savedWidgetsMock = [];
    spyOn(compareStateManagerMock, 'loadWidgets').and.callFake(function() {
      return savedWidgetsMock;
    });
    instantiateController();
    expect(compareStateManagerMock.loadWidgets).toHaveBeenCalledWith(
      $stateParamsMock.orgId,
      $stateParamsMock.siteId
    );
    expect(controller.widgets).toBe(savedWidgetsMock);
  });

  it('should save widgets whenever widgets array is changed', function() {
    spyOn(compareStateManagerMock, 'saveWidgets').and.callThrough();
    instantiateController();
    $scope.$digest();

    controller.addWidgetAndCloseDropdown('traffic-comparison-widget');
    $scope.$digest();

    controller.widgets[0].locationIds.push(123);
    $scope.$digest();

    expect(compareStateManagerMock.saveWidgets.calls.count()).toBe(2);
    expect(compareStateManagerMock.saveWidgets).toHaveBeenCalledWith(
      $stateParamsMock.orgId,
      $stateParamsMock.siteId,
      controller.widgets
    );
  });

  it('should not save widgets on initial $digest cycle', function() {
    spyOn(compareStateManagerMock, 'saveWidgets').and.callThrough();
    instantiateController();
    $scope.$digest();
    expect(compareStateManagerMock.saveWidgets).not.toHaveBeenCalled();
  });

  describe('addWidgetAndCloseDropdown', function() {
    it('should add a widget to the widgets array', function() {
      instantiateController();
      expect(controller.widgets.length).toBe(0);
      controller.addWidgetAndCloseDropdown('traffic-comparison-widget');
      expect(controller.widgets[0]).toEqual({
        type: 'traffic-comparison-widget',
        locationIds: []
      });
    });

    it('should close the dropdown', function() {
      instantiateController();
      controller.widgetDropdownIsOpen = true;
      controller.addWidgetAndCloseDropdown('traffic-comparison-widget');
      expect(controller.widgetDropdownIsOpen).toBe(false);
    });
  });

  describe('removeWidget', function() {
    it('should remove widget from the widgets array', function() {
      var widgets = [
        'traffic-comparison-widget',
        'dwell-time-comparison-widget',
        'gross-shopping-hours-comparison-widget'
      ];

      instantiateController();

      expect(controller.widgets.length).toBe(0);
      widgets.forEach(controller.addWidgetAndCloseDropdown);
      expect(controller.widgets.length).toBe(widgets.length);
      expect(_(controller.widgets).pluck('type')).toEqual(widgets);

      controller.removeWidget(controller.widgets[1]);
      expect(controller.widgets.length).toBe(widgets.length - 1);

      var newWidgets = widgets.slice();
      newWidgets.splice(1, 1);
      expect(_(controller.widgets).pluck('type')).toEqual(newWidgets);
    });
  });

  describe('changeWidgetType', function() {
    it('should change the widget type', function() {
      instantiateController();
      var widget = {
        'title': 'Foo Bar',
        'type': 'bogus-type'
      };
      controller.changeWidgetType(widget, 'different-type');
      expect(widget.type).toBe('different-type');
    });
  });

  describe('getAvailableWidgetByType', function() {
    it('should return the correct widget', function() {
      instantiateController();
      var firstWidget = _(controller.availableWidgets).first();
      var lastWidget = _(controller.availableWidgets).last();

      expect(controller.getAvailableWidgetByType(firstWidget.type))
        .toBe(firstWidget);
      expect(controller.getAvailableWidgetByType(lastWidget.type))
        .toBe(lastWidget);
    });
  });

  function instantiateController() {
    controller = $controller('GraphCompareController', {
      '$scope': $scope,
      '$stateParams': $stateParamsMock,
      'compareStateManager': compareStateManagerMock,
      'currentSite' : currentSite,
      'currentOrganization' : currentOrganizationMock,
      'currentUser' : currentUserMock
    });

    // Emulate the 'controllerAs' syntax:
    $scope.vm = controller;
  }
});
