(function() {
  'use strict';

  angular.module('shopperTrak').
    controller('SiteController', SiteController);

  SiteController.$inject = [
    '$scope',
    '$state',
    '$timeout',
    'currentOrganization',
    'currentSite',
    'locations',
    'SubscriptionsService'
  ];

  function SiteController($scope, $state, $timeout, currentOrganization, currentSite, locations, SubscriptionsService) {
    activate();

    function activate() {
      $scope.$on('$stateChangeSuccess', redirectIfNecessary);
    }

    function redirectIfNecessary() {

      if ($state.current.name !== 'analytics.organization.site') {
        return;
      }

      var siteHasPerimeter = SubscriptionsService.siteHasPerimeter(currentOrganization, currentSite);

      var stateName = siteHasPerimeter ?
                      'analytics.organization.site.traffic' :
                      'analytics.organization.site.visitor-behavior';

      if (currentSite.fullAccess) {
        $state.go(stateName, {
          orgId: currentOrganization.organization_id,
          siteId: currentSite.site_id
        });
      } else if (locations.length > 0) {
        $state.go(stateName, {
          orgId: currentOrganization.organization_id,
          siteId: currentSite.site_id,
          locationId: locations[0].location_id
        });
      } else {
        throw new Error('No routes available with current access rights.');
      }
    }
  }
})();
