'use strict';

describe('AnalyticsMenuController', function() {
  var $controller;
  var sitesMock = [{name: 'a site'}];
  var features;

  beforeEach(module('shopperTrak'));
  beforeEach(inject(function(_$controller_, _features_, _LocalizationService_) {

    $controller = _$controller_;
    features = _features_;
    _LocalizationService_.setUser({ preferences: { calendar_id: 1}});
  }));

  describe('when user has full access to site', function() {
    var siteUserHasFullAccessTo = {
      'fullAccess': true
    };

    var currentUserMock = {
      'username': 'foobar',
      'preferences': {
        'custom_dashboards': []
      }
    };

    it('should show the correct set of menu items for organizations with interior subscription', function() {
      var organizationWithInteriorSubscription = {
        'subscriptions': { 'interior': true, 'perimeter': true, 'market_intelligence': true }
      };

      var controller = $controller('AnalyticsMenuController', {
        'currentOrganization': organizationWithInteriorSubscription,
        'currentSite': siteUserHasFullAccessTo,
        'currentUser': currentUserMock,
        'sites': sitesMock
      });

      var menuItemStates = _(controller.menuItems).pluck('state');

      //fix from SA-920 only perimeter should enable traffic
      var states = [
        'analytics.organization.site.traffic',
        'analytics.organization.site.visitor-behavior',
        'analytics.organization.site.usageOfAreas',
        'analytics.organization.site.compare'
      ];

      //TODO: remove when feature ready
      if(features.isEnabled('widgetLibrary')){
        states.push('analytics.organization.site.widget-library');
      }

      expect(menuItemStates).toEqual(states);
    });

    it('should show the correct set of menu items for organizations with market intelligence subscription', function() {
      var organizationWithMarketIntelligenceSubscription = {
        'subscriptions': { 'perimeter': true, 'market_intelligence': true }
      };

      var controller = $controller('AnalyticsMenuController', {
        'currentOrganization': organizationWithMarketIntelligenceSubscription,
        'currentSite': siteUserHasFullAccessTo,
        'currentUser': currentUserMock,
        'sites': sitesMock
      });

      var menuItemStates = _(controller.menuItems).pluck('state');

      var states = [
        'analytics.organization.site.traffic',
        'analytics.organization.site.compare'
      ];

      //TODO: remove when feature ready
      if(features.isEnabled('widgetLibrary')){
        states.push('analytics.organization.site.widget-library');
      }

      expect(menuItemStates).toEqual(states);
    });

    it('should show only traffic menu item for organizations do not have interior subscription and a market intelligence subscription', function() {
      var organizationWithoutInteriorSubscription = {
        'subscriptions': { 'interior': false, 'perimeter': true }
      };

      var controller = $controller('AnalyticsMenuController', {
        'currentOrganization': organizationWithoutInteriorSubscription,
        'currentSite': siteUserHasFullAccessTo,
        'currentUser': currentUserMock,
        'sites': sitesMock
      });

      var menuItemStates = _(controller.menuItems).pluck('state');

      var states = [
        'analytics.organization.site.traffic',
        'analytics.organization.site.compare'
      ];

      //TODO: remove when feature ready
      if(features.isEnabled('widgetLibrary')){
        states.push('analytics.organization.site.widget-library');
      }

      expect(menuItemStates).toEqual(states);
    });
  });

  describe('when user does not have full access to site', function() {
    var siteUserDoesNotHaveFullAccessTo = {
      'fullAccess': false
    };

    var currentUserMock = {
      'username': 'foobar',
      'preferences': {
        'custom_dashboards': []
      }
    };

    it('should show all menu items except Usage of areas if site has interior subscription and market intelligence subscription', function() {
      var organizationWithInteriorSubscription = {
        'subscriptions': { 'perimeter': true, 'interior': true, 'market_intelligence': true }
      };

      var controller = $controller('AnalyticsMenuController', {
        'currentOrganization': organizationWithInteriorSubscription,
        'currentSite': siteUserDoesNotHaveFullAccessTo,
        'currentUser': currentUserMock,
        'sites': sitesMock
      });

      var menuItemStates = _(controller.menuItems).pluck('state');

      var states = [
        'analytics.organization.site.traffic',
        'analytics.organization.site.visitor-behavior',
        'analytics.organization.site.compare'
      ];

      //TODO: remove when feature ready
      if(features.isEnabled('widgetLibrary')){
        states.push('analytics.organization.site.widget-library');
      }

      expect(menuItemStates).toEqual(states);
    });

    it('should show no menu items if site does not have interior subscription', function() {
      var organizationWithInteriorSubscription = {
        'subscriptions': { 'interior': false }
      };

      var controller = $controller('AnalyticsMenuController', {
        'currentOrganization': organizationWithInteriorSubscription,
        'currentSite': siteUserDoesNotHaveFullAccessTo,
        'currentUser': currentUserMock,
        'sites': sitesMock
      });

      var menuItemStates = _(controller.menuItems).pluck('state');

      var states = ['analytics.organization.site.compare'];

      //TODO: remove when feature ready
      if(features.isEnabled('widgetLibrary')){
        states.push('analytics.organization.site.widget-library');
      }

      expect(menuItemStates).toEqual(states);
    });
  });

  describe('Menu hyperlinks', function() {
    var siteUserDoesNotHaveFullAccessTo = {
      'fullAccess': false
    };

    var currentUserMock = {
      'username': 'foobar',
      'preferences': {
        'custom_dashboards': []
      }
    };

    var mockState = {
      href: function(state, dateParams) {
        return 'http://app.url/' + state;
      }
    };

    var organizationWithInteriorSubscription = {
        'subscriptions': { 'perimeter': true, 'interior': true }
    };

    it('should be set for each menuItem', function() {

      var controller = $controller('AnalyticsMenuController', {
        'currentOrganization': organizationWithInteriorSubscription,
        'currentSite': siteUserDoesNotHaveFullAccessTo,
        'currentUser': currentUserMock,
        '$state': mockState,
        'sites': sitesMock
      });

      var menuItemHrefs = _(controller.menuItems).pluck('href');

      var urls = [
        'http://app.url/analytics.organization.site.traffic',
        'http://app.url/analytics.organization.site.visitor-behavior',
        'http://app.url/analytics.organization.site.compare'
      ];

      //TODO: remove when feature ready
      if(features.isEnabled('widgetLibrary')){
        urls.push('http://app.url/analytics.organization.site.widget-library');
      }

      expect(menuItemHrefs).toEqual(urls);
    });

    it('should be set with any date parameters', function() {

      var mockStateParams = {
        dateRangeStart: '01-01-2016',
        dateRangeEnd: '30-01-2016'
      };

      spyOn(mockState, 'href');

      var controller = $controller('AnalyticsMenuController', {
        'currentOrganization': organizationWithInteriorSubscription,
        'currentSite': siteUserDoesNotHaveFullAccessTo,
        'currentUser': currentUserMock,
        '$state': mockState,
        '$stateParams': mockStateParams,
        'sites': sitesMock
      });

      var firstCallArgs = mockState.href.calls.first().args;

      expect(firstCallArgs[1].dateRangeStart).toBe('01-01-2016');
      expect(firstCallArgs[1].dateRangeEnd).toBe('30-01-2016');
    });
  });

});
