(function() {
  'use strict';

  angular.module('shopperTrak')
    .constant('retailOrganizationSummaryDataConstants', {
      kpis: [
        'traffic',
        'dwelltime',
        'sales',
        'conversion',
        'ats',
        'star',
        'upt',
        'sps',
        'splh',
        'aur',
        'transactions',
      ],
      deltaLabels: {
        'traffic': 'Traffic change',
        'dwelltime': 'Dwell time',
        'sales': 'Sales',
        'conversion': 'Conversion',
        'ats': 'ATS',
        'star': 'STAR',
        'upt': 'UPT',
        'sps': 'SPS',
        'splh': 'SPLH',
        'aur': 'AUR'
      },
      totalLabels: {
        'traffic': 'Overall traffic',
        'dwelltime': 'Avg dwell time',
        'sales': 'Gross revenue',
        'conversion': 'Avg conversion',
        'ats': 'Avg transaction',
        'star': 'Shoppers to associate',
        'upt': 'Units per transaction',
        'sps': 'Sales per shopper',
        'splh': 'Sales per labor hour',
        'aur': 'Average unit retail'
      },
      fractionSizes: {
        'traffic': 0,
        'dwelltime': 2,
        'sales': 0,
        'conversion': 2,
        'ats': 2,
        'star': 2,
        'upt': 1,
        'sps': 2,
        'splh': 2,
        'aur': 2
      }
    })
    .factory('retailOrganizationSummaryData', retailOrganizationSummaryData);

  retailOrganizationSummaryData.$inject = [
    '$filter',
    'requestManager',
    'apiUrl',
    'retailOrganizationSummaryDataConstants',
    'ObjectUtils'
  ];

  function retailOrganizationSummaryData($filter, requestManager, apiUrl, constants, ObjectUtils) {
    var selectedTags = {};
    var selectedTagNames = {};

    /* Make /kpis/reports request and transform site data. */
    function fetchReportData(params, checkCache, callback) {

      if(ObjectUtils.isNullOrUndefined(params.dateRangeStart)) {
        return;
      }

      if(ObjectUtils.isNullOrUndefined(params.dateRangeEnd)) {
        return;
      }

      var dateRangeParams = {
        reportStartDate: params.dateRangeStart.toISOString(),
        reportEndDate: params.dateRangeEnd.toISOString()
      };

      var kpis = constants.kpis;
      if (params.kpi) {
        kpis = _.intersection(constants.kpis, params.kpi);
      }

      var requestParams = angular.extend({
        orgId: params.orgId,
        comp_site: params.comp_site,
        groupBy: 'aggregate',
        operatingHours: 'true',
        kpi: kpis,
        hierarchyTagId: params.selectedTags,
        sales_category_id: params.sales_category_id,
        page_size: 5000 //ToDo: Remove this hardcoding in LFR-187
      }, dateRangeParams);


      if(!ObjectUtils.isNullOrUndefined(params.customTagId)) {
        requestParams.customTagId = params.customTagId;
      }

      if(!ObjectUtils.isNullOrUndefined(requestParams.sales_category_id) && requestParams.sales_category_id.length > 1) {
        //set aggregate flag
        requestParams.aggregate_sales_categories = true;
      }

     requestManager.get(apiUrl + '/kpis/report', {
        params: requestParams
      }, checkCache)
      .then(function(data) {
        var transformedData = transformResponseData(data, constants.kpis);
        callback(transformedData);
      })
      .catch(function(error) {
        console.error(error);
      });

    }

    function transformResponseData(responseData, kpis) {
      if (ObjectUtils.isNullOrUndefinedOrEmptyObject(responseData) ||
      ObjectUtils.isNullOrUndefined(responseData.result)) {
        return;
      }

      var transformedData = [];
      var uniqueSites = $filter('unique')(responseData.result,'site_id');

      var uniqueSitesLength = uniqueSites.length;

      for(var i = 0; i !== uniqueSitesLength; ++i){
        var row = uniqueSites[i];

        var kpisLength = kpis.length;

        for(var j = 0; j !== kpisLength; ++j){
          var kpi = kpis[j];
          if(row[kpi] === 0 || typeof row[kpi] === 'undefined') {
            var siteRow = getSiteRow(responseData.result, row);
            if(typeof siteRow[0] !== 'undefined') {
              row[kpi] = siteRow[0][kpi];
            }
          }
        }

        transformedData.push(row);
      }

      function getSiteRow(data, row) {
        return data.filter(function(obj) {
          return obj.site_id === row.site_id && obj[kpi] !== 0 && !_.isUndefined(obj[kpi]);
        });
      }

      return transformedData;
    }

    /* Make request to individual kpi endpoint (eg. /sales) and return data. */

    function fetchKpiData(params, checkCache, callback, errorCallBack) {
      if(!ObjectUtils.isNullOrUndefined(params.dateRangeStart) && !ObjectUtils.isNullOrUndefined(params.dateRangeEnd)) {

        var dateRangeParams = {
          reportStartDate: getDateString(params.dateRangeStart),
          reportEndDate: getDateString(params.dateRangeEnd)
        };

        var requestParams = angular.extend({
          orgId: params.orgId,
          siteId: params.siteId,
          comp_site: params.comp_site,
          groupBy: 'aggregate',
          operatingHours: 'true',
          hierarchyTagId: params.selectedTags,
          kpi: params.kpi,
          sales_category_id: params.sales_category_id
        }, dateRangeParams);

        if(!ObjectUtils.isNullOrUndefined(params.customTagId)) {
          requestParams.customTagId = params.customTagId;
        }

        if(ObjectUtils.isNullOrUndefined(params.siteId)) {
          requestParams.org_level = true;
        }

        requestManager.get(apiUrl + '/' + params.apiEndpoint, {
          params: requestParams
        }, checkCache).then(function (data) {

          // Depending on the endpoint we call, the api returns traffic and sales with different prop name.
          // This code renames the properies into something more common

          // eslint-disable-next-line no-unused-vars
          _.each(data.result, function(row) {
            row = ObjectUtils.rename(row, 'sales_amount', 'sales');
            row = ObjectUtils.rename(row, 'total_sales', 'sales');
            row = ObjectUtils.rename(row, 'total_traffic', 'traffic');
          });

          callback(data);
        }, function (error, status){

          if(typeof errorCallBack === 'function') {
            errorCallBack(error, status);
          }

        fetchKpiData.error = { message: error, status: status};

        });
      }
    }

    function getDateString(dateRange) {
      if(typeof dateRange === 'string') {
        return dateRange;
      }

      return dateRange.toISOString();
    }

    function getDateRangeKey(dateRangeStart, dateRangeEnd) {
      return dateRangeStart + '_' + dateRangeEnd;
    }

    function setSelectedTags(tags) {
      selectedTags = tags;
    }

    function setSelectedTagNames(names) {
      selectedTagNames = names;
    }

    function getSelectedTagNames() {
      return selectedTagNames;
    }

    function getSelectedTags() {
      return selectedTags;
    }

    return {
      fetchReportData: fetchReportData,
      fetchKpiData: fetchKpiData,
      transformResponseData: transformResponseData,
      getDateRangeKey: getDateRangeKey,
      setSelectedTags: setSelectedTags,
      getSelectedTags: getSelectedTags,
      setSelectedTagNames: setSelectedTagNames,
      getSelectedTagNames: getSelectedTagNames
    };
  }
})();
