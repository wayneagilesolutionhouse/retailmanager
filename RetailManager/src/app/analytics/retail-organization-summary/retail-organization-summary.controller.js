(function () {
  'use strict';
  angular.module('shopperTrak').controller('RetailOrganizationSummaryController', RetailOrganizationSummaryController);

  RetailOrganizationSummaryController.$inject = [
    '$scope',
    '$rootScope',
    '$state',
    '$stateParams',
    '$q',
    '$translate',
    '$timeout',
    'currentOrganization',
    'LocalizationService',
    'retailOrganizationSummaryData',
    'utils',
    'SubscriptionsService',
    'currentUser',
    'sites',
    'ExportService',
    'metricConstants',
    'ObjectUtils',
    'customDashboardService',
    'maxSitesToCache',
    'widgetConstants'
  ];

  function RetailOrganizationSummaryController(
    $scope,
    $rootScope,
    $state,
    $stateParams,
    $q,
    $translate,
    $timeout,
    currentOrganization,
    LocalizationService,
    retailOrganizationSummaryData,
    utils,
    SubscriptionsService,
    currentUser,
    sites,
    ExportService,
    metricConstants,
    ObjectUtils,
    customDashboardService,
    maxSitesToCache,
    widgetConstants
  ) {

    var isLoading = {};
    var tempKpiData = {};
    var tempTableData = {};

    activate();

    function activate() {
      initScope();
      loadTranslations();
      configureWatches();

      if(orgHasSalesCategories()) {
        configureTableSalesCategoryWatch();
      }

      $scope.checkCache = sites.length < maxSitesToCache;
      $scope.dateRange = {
        start: $stateParams.dateRangeStart,
        end: $stateParams.dateRangeEnd
      };

      $scope.compareRange1 = {
        start: $stateParams.compareRange1Start,
        end: $stateParams.compareRange1End
      };

      $scope.compareRange2 = {
        start: $stateParams.compareRange2Start,
        end: $stateParams.compareRange2End
      };

      LocalizationService.setUser(currentUser);
      LocalizationService.setOrganization(currentOrganization);
      $scope.dateFormat = LocalizationService.getCurrentDateFormat(currentOrganization);

      LocalizationService.getAllCalendars().then(function (calendars) {
        LocalizationService.setAllCalendars(calendars.result);
        LocalizationService.setUser($scope.currentUser);
        $scope.compareRange1Type = utils.getCompareType($scope.compareRange1, $scope.dateRange, $scope.currentUser, $scope.currentOrganization);
        $scope.compareRange2Type = utils.getCompareType($scope.compareRange2, $scope.dateRange, $scope.currentUser, $scope.currentOrganization);
      });

      if (SubscriptionsService.siteHasSales(currentOrganization)) {
        $scope.orgHasSales = true;
      } else {
        $scope.orgHasSales = false;
      }

      if (SubscriptionsService.siteHasLabor(currentOrganization)) {
        $scope.orgHasLabor = true;
      } else {
        $scope.orgHasLabor = false;
      }

      if (SubscriptionsService.siteHasInterior(currentOrganization)) {
        $scope.orgHasInterior = true;
      } else {
        $scope.orgHasInterior = false;
      }
      getAllKpiData();
    }

    function initScope() {
      $scope.metricsToShow = ['kpi_summary_widget','daily_performance_widget','traffic_per_weekday','retail_store_summary','retail_organization_table'];
      $scope.retailStoreSummaryData = {};
      $scope.tableData = {};
      $scope.kpiData = {};
      $scope.viewData = {};
      $scope.isLoading = {};
      $scope.compareMode = {
        stores: false,
        all: true
      };
      $scope.currentCompare = 1;
      $scope.setCompareMode = setCompareMode;
      $scope.orgSites = sites;
      $scope.orgId = currentOrganization.organization_id;
      $scope.currentOrganization = currentOrganization;
      $scope.currentUser = currentUser;
      $scope.userOptions = {};
      $scope.numberFormat = getNumberFormat();
      $scope.dateRangesLoaded = dateRangesLoaded();
      $scope.isChartLoading = { status: true };
      $scope.kpiFlag = {
        DataLoading: true
      };
      $scope.exportWidget = exportWidget;
      $scope.widgetIsExported = widgetIsExported;
      $scope.setSelectedWidget = setSelectedWidget;
      $scope.$on('scheduleExportCurrentViewToPdf', scheduleExportCurrentViewToPdf);
      $scope.setSelectedFilters = setSelectedFilters;
      $scope.selectedTags = [];
      $scope.selectedTagNames = {};
      $scope.viewIsLoaded = viewIsLoaded();

      $scope.$on('goToHourlyDrilldown', goToHourlyDrilldown);
    }

    function getPermittedKpis() {
      var kpisPermitted = [];

      var permdMetrics = metricConstants.metrics.filter(function(metric) {
        return SubscriptionsService.hasSubscriptions(metric.requiredPermissions, $scope.currentOrganization);
      });

      kpisPermitted = _.pluck(permdMetrics, 'value');

      kpisPermitted.push('traffic');

      return kpisPermitted;
    }

    function configureWatches() {
      var unbindFirstDaySettingWatch = $rootScope.$watch('firstDaySetting', function () {
        $scope.firstDayOfWeek = $rootScope.firstDaySetting;
      });

      var unbindMetricsWatch = $scope.$watchGroup(['metricsToShow', 'dateRangesLoaded', 'selectedWeatherMetrics','firstDayOfWeek','isLoading','userOptions.filterText','userOptions.siteCategories'], function() {
        if(viewIsLoaded()) {
          $scope.viewData = initializeViewData();
        }
      });

      $scope.$on('$destroy', function() {
        if(typeof unbindFirstDaySettingWatch === 'function') {
          unbindFirstDaySettingWatch();
        }

        if(typeof unbindMetricsWatch === 'function') {
          unbindMetricsWatch();
        }
      });
    }

    function configureTableSalesCategoryWatch() {
      if(typeof unbindTableSalesCategoryWatch === 'function') {
        return;
      }

      var unbindSalesCatWatches = [];
      var widgetsToWatch = ['kpi_summary_widget','retail_store_summary','retail_organization_table'];

      _.each(widgetsToWatch, function (metric) {
        unbindSalesCatWatches.push($scope.$watchCollection('viewData.'+metric+'.salesCategories.selection', function(newValue, oldValue) {
          if(angular.equals(newValue, oldValue) || ObjectUtils.isNullOrUndefined(oldValue)) {
            return;
          }

          if(metric === 'kpi_summary_widget') {
            $scope.kpiFlag.DataLoading = true;
            getAllKpiData();
          } else {
            // Full load
            $scope.isChartLoading.status = true;
            loadAllOrganizationData();
            getAllKpiData();
          }
        }));
      });

      $scope.$on('$destroy', function() {
        _.each(unbindSalesCatWatches, function(watcher) {
          if(typeof watcher === 'function') {
            watcher();
          }
        });
      });
    }



    function setCompareMode(allStores) {
      if($scope.currentCompare === allStores) {
        return;
      }
      var isAll = allStores ? true : false;
      angular.extend($scope.compareMode, {
        all: isAll,
        stores: !isAll
      });

      $scope.currentCompare = allStores;
      refreshTags();
    }

    function loadAllOrganizationData() {
      var salesCategories;

      if(!ObjectUtils.isNullOrUndefined($scope.viewData.retail_organization_table.salesCategories.selection)) {
        salesCategories = _.pluck($scope.viewData.retail_organization_table.salesCategories.selection, 'id');
      }

      var promises = [];
      promises.push(getReportData($scope.orgId, $scope.dateRange.start, $scope.dateRange.end, $scope.selectedTags, salesCategories));
      promises.push(getReportData($scope.orgId, $scope.compareRange1.start, $scope.compareRange1.end, $scope.selectedTags, salesCategories));
      promises.push(getReportData($scope.orgId, $scope.compareRange2.start, $scope.compareRange2.end, $scope.selectedTags, salesCategories));

      $q.all(promises).then(function() {
        $scope.tableData = angular.copy(tempTableData);
        $scope.kpiFlag.DataLoading = false;
      });
    }

    function orgHasSalesCategories() {
      if(ObjectUtils.isNullOrUndefined(currentOrganization.portal_settings)) {
        return false;
      }

      if(ObjectUtils.isNullOrUndefinedOrEmpty(currentOrganization.portal_settings.sales_categories)) {
        return false;
      }

      return true;
    }

    function refreshTags() {
      isLoading = {};
      $scope.numTags = _.keys($scope.selectedTags).length + _.keys($scope.selectedCustomTags).length;
      tempKpiData = {};
      $scope.kpiFlag.DataLoading = true;

      $timeout(function () {
        getAllKpiData();
      });
    }

    function getChartData(orgId, dateRangeStart, dateRangeEnd, selectedTags) {
      $scope.isChartLoading.status = true;

      var dateRangeKey = retailOrganizationSummaryData.getDateRangeKey(dateRangeStart, dateRangeEnd);

      var compStores = $scope.compareMode.stores;

      $scope.retailStoreSummaryData[dateRangeKey] = [];

      var params = {
        orgId: orgId,
        comp_site: compStores,
        dateRangeStart: dateRangeStart,
        dateRangeEnd: dateRangeEnd,
        selectedTags: selectedTags,
        kpi: getPermittedKpis()
      };

      if( !ObjectUtils.isNullOrUndefinedOrEmpty($scope.selectedCustomTags) ) {
        params.customTagId = $scope.selectedCustomTags;
      }

      if (typeof $scope.salesCategoriesKpi !== 'undefined' && $scope.salesCategoriesKpi.length > 0 && _.min($scope.salesCategoriesKpi.id) > 0) {
        params.sales_category_id = $scope.salesCategoriesKpi.map(function(category) {
          return category.id;
        });
      }

      retailOrganizationSummaryData.fetchReportData(params, $scope.checkCache, function (data) {
        $scope.retailStoreSummaryData[dateRangeKey] = data;
      });
    }

    function getReportData(orgId, dateRangeStart, dateRangeEnd, selectedTags, salesCategoryId) {
      var dateRangeKey = retailOrganizationSummaryData.getDateRangeKey(dateRangeStart, dateRangeEnd);
      var compStores = $scope.compareMode.stores;
      $scope.kpiFlag.DataLoading = true;


      tempTableData[dateRangeKey] = [];

      if(ObjectUtils.isNullOrUndefined(isLoading['kpis/report'])) {
        isLoading['kpis/report'] = {};
      }

      isLoading['kpis/report'][dateRangeKey] = true;

      var params = {
        orgId: orgId,
        comp_site: compStores,
        dateRangeStart: dateRangeStart,
        dateRangeEnd: dateRangeEnd,
        selectedTags: selectedTags,
        kpi: getPermittedKpis(),
        sales_category_id: salesCategoryId
      };

      if( !ObjectUtils.isNullOrUndefinedOrEmpty($scope.selectedCustomTags) ) {
        params.customTagId = $scope.selectedCustomTags;
      }

      var deferred = $q.defer();

      retailOrganizationSummaryData.fetchReportData(params,  $scope.checkCache, function (data) {
        tempTableData[dateRangeKey] = data;
        deferred.resolve();
      });

      return deferred.promise;
    }

    /** Gets the endpoint based on the current org, current site, and current subscriptions
     *
     *  @returns {string} endpoint - The api endpoint to call
     **/
    function getEndpoint() {

      // Traffic only
      if($scope.orgHasSales === false) {
        return 'kpis/traffic';
      }

      // Org Level Traffic and Sales
      return 'kpis/report';
    }

    function getAllKpiData() {
      $scope.kpiFlag.DataLoading = true;

      var apiUrl = getEndpoint();

      var salesDateranges = {
        current: 1,
        previousPeriod: 2,
        previousYear: 3,
        range: {
          1: { value: '' },
          2: { value: '' },
          3: { value: '' }
        }
      };

      $scope.salesDateRanges = salesDateranges;

      var promises = [];

      var apiDateRanges = [$scope.dateRange, $scope.compareRange1, $scope.compareRange2];

      _.each(apiDateRanges, function (range, index) {
        var dRange = salesDateranges.range[index + 1];

        var promise = getKpiData(apiUrl, $scope.orgId, range.start, range.end, $scope.selectedTags, dRange);

        promises.push(promise);
      });

      $q.all(promises).then(function() {
        $scope.kpiFlag.DataLoading = false;
        $scope.kpiData = angular.copy(tempKpiData);

        /*
          Currently, requests for the chart data happen after the data for the 5KPI widget is loaded
          This is deliberate as the requests for the chart data and the table are slow
          We do not want these slow requests holding up the quicker to load 5 kpi widget.

          ToDo: Assess this to see if it is still needed, and hopefully remove.
        */
        getChartData($scope.orgId, $scope.dateRange.start, $scope.dateRange.end, $scope.selectedTags);
        loadAllOrganizationData();
      });
    }

    function getKpiData(apiEndpoint, orgId, dateRangeStart, dateRangeEnd, selectedTags, range) {

      var dateRangeKey = retailOrganizationSummaryData.getDateRangeKey(dateRangeStart, dateRangeEnd);

      range = (typeof range === 'undefined') ? null : range;

      if (range !== null) {
        range.value = dateRangeKey;
      }

      var compStores = $scope.compareMode.stores;

      if (typeof tempKpiData[apiEndpoint] === 'undefined') {
        tempKpiData[apiEndpoint] = {};
      }

      tempKpiData[apiEndpoint][dateRangeKey] = [];

      var params = {
        apiEndpoint: apiEndpoint,
        orgId: orgId,
        comp_site: compStores,
        dateRangeStart: dateRangeStart,
        dateRangeEnd: dateRangeEnd,
        selectedTags: selectedTags
      };

      if (typeof ObjectUtils.getNestedProperty($scope,'viewData.kpi_summary_widget.salesCategories') !== 'undefined') {
        params.sales_category_id = _.pluck($scope.viewData.kpi_summary_widget.salesCategories.selection, 'id')
      }

      //just so requests don't fire without custom tags when filters are cached - multiple requests were being made due to multiple instances
      if(!ObjectUtils.isNullOrUndefined($state.activeFilters) && $scope.selectedCustomTags === undefined) {
        return;
      }

      if( !ObjectUtils.isNullOrUndefinedOrEmpty($scope.selectedCustomTags) ) {
        params.customTagId = $scope.selectedCustomTags;
      }

      if($scope.orgHasSales) {
        var kpi = ['ats', 'conversion', 'traffic'];

        if($scope.orgHasLabor) {
          kpi.push('star');
        }

        params.kpi = kpi;
      }

      var deferred = $q.defer();

      retailOrganizationSummaryData.fetchKpiData(params, true, function (data) {
        tempKpiData[dateRangeKey] = data;
        deferred.resolve();
      }, function (error, status) {
        tempKpiData[dateRangeKey] = { kpiRequestFailed: true, message: error, status: status };
        deferred.reject();
      });

      return deferred.promise;
    }

    function loadTranslations() {
      $translate.use($scope.language);
    }

    function getNumberFormat() {
      return LocalizationService.getCurrentNumberFormatName(currentUser, currentOrganization);
    }

    function exportWidget(metricKey, toDashboard) {
      initializeViewData();
      var params = $scope.viewData[metricKey];

      if (toDashboard) {
        customDashboardService.setSelectedWidget(params);
      } else {
        ExportService.createExportAndStore(params);
      }
    }

    function widgetIsExported(metricKey, params) {
      var dateRangeKey =
        $scope.dateRange.start +
        ' - ' +
        $scope.dateRange.end +
        ' - ' +
        $scope.compareRange1.start +
        ' - ' +
        $scope.compareRange1.end +
        ' - ' +
        $scope.compareRange2.start +
        ' - ' +
        $scope.compareRange2.end;

      var paramsToCompare = widgetVariables()[metricKey];

      return ExportService.isInExportCartWithSettings(getAreaKey(), dateRangeKey, metricKey, params, paramsToCompare)
    }

    function setSelectedWidget(title) {
      exportWidget(title, true)
    }

    function scheduleExportCurrentViewToPdf() {
      exportWidget('kpi_summary_widget');
      exportWidget('daily_performance_widget');
      exportWidget('traffic_per_weekday');
      exportWidget('retail_store_summary');
      $state.go('pdfexport', {orgId: currentOrganization.organization_id, view: 'schedule'});
    }

    function goToHourlyDrilldown(event, data) {
      if (moment.utc($stateParams.compareRange1Start).format('YYYY-MM-DD') === moment.utc($stateParams.compareRange1End).format('YYYY-MM-DD')) {
        $state.go('analytics.organization.hourly', {
          day: data.label,
          compareRange1Start: event.currentScope.compareRange1.start,
          compareRange1End: event.currentScope.compareRange1.end,
          compareRange2Start: event.currentScope.compareRange2.start,
          compareRange2End: event.currentScope.compareRange2.end,
          dateRangeStart: event.currentScope.dateRange.start,
          dateRangeEnd: event.currentScope.dateRange.end
        });
      }
    }

    function getAreaKey() {
      var key = currentOrganization.organization_id+'_-1';
      if($scope.selectedTags.length > 0) {
        key += '_tags_';
        _.each($scope.selectedTags, function(tag) {
          key += tag;
        });
      }
      return key;
    }

    function setSelectedFilters(filters, customTags) {
      $scope.selectedTags = [];
      $scope.selectedCustomTags = [];
      $scope.selectedTagNames = filters[1];
      $scope.userOptions.filterText = ''; // clear search text when tags are applied

      if( !ObjectUtils.isNullOrUndefinedOrEmptyObject(customTags) ) {
        $scope.selectedCustomTags =  _.keys(_.pick(customTags, function(_selected) {
          return _selected === true;
        }));
      }

      // Selected Tags
      var selectedTags = filters[0];
      $scope.selectedTags = _.keys(_.pick(selectedTags, function(_selected) {
        return _selected === true;
      }));

      refreshTags();
    }

    function dateRangesLoaded() {
      return utils.urlDateParamsLoaded($stateParams);
    }

    function initializeViewData() {

      var configuration = {};

      _.each($scope.metricsToShow, function(metricKey) {
        configuration[metricKey] = initExportParam(metricKey);
      });

      return configuration;
    }

    function initExportParam(metricKey) {
      var params;

      params = {
        orgId: currentOrganization.organization_id,
        name: metricKey,
        currentUser: currentUser,
        dateRangeKeys: $scope.salesDateRanges,
        dateRange: { start: $scope.dateRange.start, end: $scope.dateRange.end },
        dateRangeType: utils.getDateRangeType($scope.dateRange, currentUser, currentOrganization),
        compare1Range: { start: $scope.compareRange1.start, end: $scope.compareRange1.end },
        compare2Range: { start: $scope.compareRange2.start, end: $scope.compareRange2.end },
        compare1Type: $scope.compareRange1Type,
        compare2Type: $scope.compareRange2Type,
        firstDayOfWeekSetting: $scope.firstDayOfWeek,
        dateRangeShortCut: $state.rangeSelected,
        customRange:  $state.customRange,
        compStores: $scope.compareMode.stores,
        tags: $scope.selectedTags,
        dateFormat: $scope.dateFormat,
        numberFormat: $scope.numberFormat,
        language: $scope.language,
        selectedTags: $scope.selectedTags,
        customTags: $scope.selectedCustomTags,
        orgHasSales: $scope.orgHasSales,
        orgHasLabor: $scope.orgHasLabor,
        selectedCategory: $scope.userOptions.categoryFilter,
        orgHasInterior: $scope.orgHasInterior,
        currencySymbol: _.findWhere(metricConstants.metrics, {isCurrency: true}).prefixSymbol
      };

      if(params.dateRangeShortCut === 'custom' && params.customRange === null) {
        params.xDaysBack = moment().utc().diff(params.dateRange.start, 'days');
        params.xDaysDuration = params.dateRange.end.diff(params.dateRange.start, 'days');
      }

      if(!ObjectUtils.isNullOrUndefined($scope.viewData[metricKey])) {

        params.siteFilter = $scope.userOptions.filterText;
        params.extremeValues = $scope.viewData[metricKey].extremeValues;
        params.activeKpi = $scope.viewData[metricKey].activeKpi;

        switch (metricKey) {
          case 'kpi_summary_widget':
            if (!ObjectUtils.isNullOrUndefined($scope.viewData[metricKey].salesCategories)) {
              params.salesCategories = $scope.viewData[metricKey].salesCategories;
            }
            break;
          case 'traffic_per_weekday':
            params.selectedMetric = $scope.viewData[metricKey].selectedMetric;
            params.orderTable = $scope.viewData[metricKey].orderTable;
            params.orderReverse = $scope.viewData[metricKey].orderReverse;
            params.showTableTrafficPerWeekday = $scope.viewData[metricKey].showTable;
            if (!ObjectUtils.isNullOrUndefined($scope.viewData[metricKey].salesCategories)) {
              params.salesCategories = $scope.viewData[metricKey].salesCategories;
            }
            if (!ObjectUtils.isNullOrUndefined($scope.viewData[metricKey].selectedDays) && $scope.viewData[metricKey].selectedDays.length < 7) { //don't include unless needed load all from widget
              params.selectedDays = $scope.viewData[metricKey].selectedDays;
              _.each(params.selectedDays, function (day) {
                delete day.transkey;
              })
            }
            break;
          case 'daily_performance_widget':
            if (!ObjectUtils.isNullOrUndefined($scope.viewData[metricKey].selectedDays) && $scope.viewData[metricKey].selectedDays.length < 7) { //don't include unless needed load all from widget
              params.selectedDays = $scope.viewData[metricKey].selectedDays;
              _.each(params.selectedDays, function (day) {
                delete day.transkey;
              })
            }
            params.showTable = $scope.viewData[metricKey].showTable;
            params.orderTable = $scope.viewData[metricKey].orderTable;
            params.orderDirection = $scope.viewData[metricKey].orderDirection;
            if (!ObjectUtils.isNullOrUndefined($scope.viewData[metricKey].salesCategories)) {
              params.salesCategories = $scope.viewData[metricKey].salesCategories;
            }
            break;
          case 'retail_organization_table':
            params.salesCategories = ObjectUtils.getNestedProperty($scope, 'currentOrganization.portal_settings.sales_categories') || [];
            params.siteCategories = $scope.userOptions.siteCategories;
            params.categories = ObjectUtils.getNestedProperty($scope, 'viewData[' + metricKey + '].salesCategories') || [];
            params.selectedMetrics = $scope.viewData[metricKey].selectedMetrics;
            params.comparisonIndex = $scope.viewData[metricKey].comparisonIndex;
            params.filterText = $scope.userOptions.filterText;
            params.sites = [];
            break;
          case 'retail_store_summary':
            params.salesCategories = ObjectUtils.getNestedProperty($scope, 'currentOrganization.portal_settings.sales_categories') || [];
            params.categories = ObjectUtils.getNestedProperty($scope, 'viewData[' + metricKey + '].salesCategories') || [];
            params.siteCategories = $scope.userOptions.siteCategories;
            params.filterText = $scope.userOptions.filterText;
            break;
        }

      }
      return params;
    }

    function viewIsLoaded() {
      return !ObjectUtils.isNullOrUndefinedOrEmpty($scope.metricsToShow) && $scope.dateRangesLoaded;
    }

    function widgetVariables() {
      return widgetConstants.exportProperties;
    }
  }
})();
