'use strict';

angular.module('shopperTrak')
.controller('PdfCtrl',
['$scope',
  '$rootScope',
  '$q',
  '$http',
  'resourceUtils',
  '$stateParams',
  'currentOrganization',
  'currentSite',
  'OrganizationResource',
  'SiteResource',
  'LocationResource',
  'ZoneResource',
  '$translate',
  'LocalizationService',
  'ObjectUtils',
  'obfuscation',
  'ZoneHelperService',
  'utils',
  'apiUrl',
  '$timeout',
  'currencyService',
function (
  $scope,
  $rootScope,
  $q,
  $http,
  resourceUtils,
  $stateParams,
  currentOrganization,
  currentSite,
  OrganizationResource,
  SiteResource,
  LocationResource,
  ZoneResource,
  $translate,
  LocalizationService,
  ObjectUtils,
  obfuscation,
  ZoneHelperService,
  utils,
  apiUrl,
  $timeout,
  currencyService
) {
  $scope.summaryPage = $stateParams.data.indexOf('summary-page') > -1;

  $scope.widgetIsLoading = {};
  $scope.organizations = [];
  $scope.sites = [];
  $scope.zones = [];
  $scope.locations = [];

  $scope.siteList = '';
  $scope.orgList = '';
  $scope.isLoadingLocations = true;

  $scope.hasOrganizationId = hasOrganizationId;
  $scope.hasSiteId = hasSiteId;
  $scope.hasLocationId = hasLocationId;
  $scope.hasZoneId = hasZoneId;
  $scope.getPartialPageKey = getPartialPageKey;

  $rootScope.pdf = true;
  $scope.language = LocalizationService.getCurrentLocaleSetting();
  $scope.orgTags = {};

  $scope.currentUserLoaded = false;

  $scope.getColumnCssClass = getColumnCssClass;
  $scope.getPageBreakCssClass = getPageBreakCssClass;

  var requestData = JSON.parse($stateParams.data);

  if(!ObjectUtils.isNullOrUndefined(requestData.userId)){
    $scope.hasUserId = true;
    loadCurrentUser(requestData.userId);
  } else {
    $scope.hasUserId = false;
    LocalizationService.getAllCalendars().then(function() {
      activate();
    });

    $scope.currentUserLoaded = true;
  }

  $scope.$watchCollection('widgetIsLoading',function() {
    var numLoading = _.filter($scope.widgetIsLoading, function(status){ return status; });
    if(Object.keys($scope.widgetIsLoading).length > 0 && numLoading.length === 0) {
      $timeout(setWindowStatus, 4000);
    }
  });

  // pdf stops trying to load widgets after 5 minutes (page-wide timeout)
  $timeout(setWindowStatus, 300000);

  function setWindowStatus() {
    window.status = 'allWidgetsRendered';
    window.renderable = 'allWidgetsRendered';
  }

  function getPartialPageKey(export2) {
    return ObjectUtils.isNullOrUndefinedOrBlank(export2.partialPageName)?
      export2.summaryKey : export2.partialPageName;
  }

  function loadCurrentUser(userId) {
    $http.get(apiUrl + '/users/' + userId).then(function (result) {
      $scope.currentUser = result.data.result[0];
      if ($scope.currentUser.username.match(/^demo/gi)) {
        obfuscation.enableForSession();
      } else {
        obfuscation.disableForSession();
      }
      LocalizationService.getAllCalendars().then(function() {
        activate();
      });
      $scope.currentUserLoaded = true;
    },
      function onError() {
        console.error('error')
      }
    );
  };

  function activate() {
    var widgetData = [];

    getKpiValuesForKpi();
    loadTranslations();

    if(angular.isDefined($scope.currentUser)) {
      LocalizationService.setUser($scope.currentUser);
    }

    if($scope.hasUserId){
      widgetData = requestData.widgets;
    } else {
      widgetData = requestData;
    }

    var orgIds = _.pluck(widgetData, 'organizationId');

    loadOrganizations(orgIds).then(function() {
      loadOrganizationTags();
      parseExports();
      mapWidgets(widgetData);
      loadSites();
      loadZones();
      loadLocations();
      setExportParams();
      createOrder();
    });
  }

  function mapWidgets(widgetData) {
    $scope.exports = widgetData.map(function (value) {
      value.dateRange.start = moment.utc(value.dateRange.start);
      value.dateRange.end = moment.utc(value.dateRange.end);
      value.compare1Range.start = moment.utc(value.compare1Range.start);
      value.compare1Range.end = moment.utc(value.compare1Range.end);
      value.compare2Range.start = moment.utc(value.compare2Range.start);
      value.compare2Range.end = moment.utc(value.compare2Range.end);
      value.currentUser = $scope.currentUser;

      //parse string ids passed through stateParams - string ids tend to return empty values for map in pdf export
      if(!ObjectUtils.isNullOrUndefined(value.locationId)) {
        value.locationId = Number(value.locationId);
      }

      if(!ObjectUtils.isNullOrUndefined(value.organizationId)) {
        value.organizationId = Number(value.organizationId);

        if(ObjectUtils.isNullOrUndefined(value.currentOrganization)) {
          value.currentOrganization = $scope.organizations[value.organizationId];
        }
      }


      if(_.isUndefined(value.siteId)){
        currencyService.getCurrencySymbol(value.organizationId).then(function(data){
          value.currencySymbol = data.currencySymbol;
        })
      } else {
        currencyService.getCurrencySymbol(value.organizationId, value.siteId).then(function(data){
          value.currencySymbol = data.currencySymbol;
        })
      }


      if(!ObjectUtils.isNullOrUndefined(value.siteId)) {
        value.siteId = Number(value.siteId);
      }

      if (typeof value.tags !== 'undefined' && typeof $scope.orgTags[value.organizationId] === 'undefined') {
        getOrganizationTags(value.organizationId);
      }

      // Slashes break down the export URLs
      if (value.dateFormat) {
        value.dateFormat = value.dateFormat.replace(/\|/g, '/');
      } else {
        value.dateFormat = LocalizationService.getCurrentDateFormat(value.currentOrganization);
      }
      if (typeof $scope.currentUser !== 'undefined') {
        if (
          typeof $scope.currentUser.localization !== 'undefined' &&
          typeof $scope.currentUser.localization.date_format !== 'undefined' &&
          !ObjectUtils.isNullOrUndefined( $scope.currentUser.localization.date_format.mask )
        ) {
          $scope.currentUser.localization.date_format.mask = $scope.currentUser.localization.date_format.mask.replace(/\|/g, '/');
        }
      }

      if (typeof value.currentOrganization !== 'undefined') {
        if (
          typeof value.currentOrganization.localization !== 'undefined' &&
          typeof value.currentOrganization.localization.date_format !== 'undefined' &&
          !ObjectUtils.isNullOrUndefined( value.currentOrganization.localization.date_format.mask )
        ) {
          value.currentOrganization.localization.date_format.mask = value.currentOrganization.localization.date_format.mask.replace(/\|/g, '/');
        }
      }

      if(!ObjectUtils.isNullOrUndefinedOrEmpty(value.compareId)){
        _.each(value.currentUser.preferences.custom_charts, function(customChart){
          var idToCheck = customChart.chart_name.replace(/[^a-zA-Z0-9]/g, '') + customChart.organization_id;
          if(value.compareId === idToCheck){
            value.compare = customChart;
            value.compare.activeSelectedComparePeriods = value.selectedComparePeriod;
            value.compare.activeSelectedMetrics = value.selectedMetrics;
            value.compare.showTable = value.table;
          }
        });
      }

      if(!ObjectUtils.isNullOrUndefined(value.dateRangeShortCut)) {
        //set date range then get compare ranges
        updateDateRanges(value);
        value.compare1Range = utils.getPreviousPeriodDateRange(value.dateRange);
        value.compare2Range = utils.getEquivalentPriorYearDateRange(value.dateRange, value.firstDayOfWeekSetting, value.currentUser, value.currentOrganization, value.dateRangeShortCut);
        if(value.dateRangeShortCut === 'year'){
          value.compare1Range.start = LocalizationService.getFirstDayOfYear(moment(value.compare1Range.end).year());
          value.compare2Range = utils.getEquivalentPriorYearDateRange(value.compare1Range, value.firstDayOfWeekSetting, value.currentUser, value.currentOrganization, value.dateRangeShortCut);
        } else if(value.dateRangeShortCut === 'month') {
            var currentCalendarSettings = LocalizationService.getActiveCalendarSettings();
            var month = LocalizationService.getFirstDayOfMonth(moment(value.dateRange.start).month(), moment(value.dateRange.start).year());
            if(LocalizationService.isGregorian(currentCalendarSettings)) {
              value.compare1Range.start = moment(month).subtract(1, 'months');
              value.compare1Range.end = LocalizationService.getLastDayOfMonth(moment(value.compare1Range.start).month(), moment(value.dateRange.start).year());
            }
            value.compare2Range = utils.getEquivalentPriorYearDateRange(value.dateRange, value.firstDayOfWeekSetting, value.currentUser, value.currentOrganization, value.dateRangeShortCut);
        } else if (value.dateRangeShortCut === 'custom' && value.customRange !== 'custom'){
            switch (value.customRange) {
              case 'wtd': value.compare1Range = utils.getDateRangeForPreviousWTD(value.dateRange); break;
              case 'mtd': value.compare1Range = utils.getDateRangeForPreviousMTD(value.dateRange); break;
              case 'qtd': value.compare1Range = utils.getDateRangeForPreviousQTD(value.dateRange); break;
              case 'ytd': value.compare1Range = utils.getDateRangeForPreviousYTD(value.dateRange); break;
            }
        }

        if(value.currentUser.preferences.custom_period_1.period_type === 'custom'){
          var weeks = value.currentUser.preferences.custom_period_1.num_weeks;
          var baseRange = angular.copy(value.dateRange);
          value.compare1Range.start = baseRange.start.subtract(weeks, 'week');
          value.compare1Range.end = baseRange.end.subtract(weeks, 'week');
        }

        if(value.currentUser.preferences.custom_period_2.period_type === 'custom'){
          var weeks = value.currentUser.preferences.custom_period_2.num_weeks;
          var baseRange = angular.copy(value.dateRange);
          value.compare2Range.start = baseRange.start.subtract(weeks, 'week');
          value.compare2Range.end = baseRange.end.subtract(weeks, 'week');
        }
      }

      if(value.currentUser.preferences.weather_reporting) {
        value.showWeatherMetrics = true;
        value.selectedWeatherMetrics = [];
        _.each(value.weather, function(selectedMetric){
          value.selectedWeatherMetrics.push({kpi : selectedMetric});
        });
      }

      return value;
    });
  }

  function createOrder(){
    var orgList = [];
    var siteOrder, zoneOrder, locationOrder;
    var unique = 0;

    _.each($scope.exports, function(exports) {
        var org = exports.organizationId;
        if(orgList.indexOf(org) < 0) {
          orgList.push(org);
        }
    });

    _.each(orgList, function(org){
      var currentOrg = org;

      _.each($scope.exports, function(exports){
        if(exports.organizationId === currentOrg) {

          siteOrder = !ObjectUtils.isNullOrUndefined(exports.siteId) ? exports.siteId : '0';

          zoneOrder = !ObjectUtils.isNullOrUndefined(exports.zoneId) ? exports.zoneId : '0';

          locationOrder = !ObjectUtils.isNullOrUndefined(exports.locationId) ? exports.locationId : '0';

          exports.order = org + '.' + siteOrder + '.' + zoneOrder + '.' + locationOrder + '.' + unique;
          unique++;
        }
      });
    });
  }

  function parseExports() {
    _.each($scope.exports, function(value) {
      var siteIds = [], orgIds = [];
      value = decodeExportCartSlashes(value);

        if (!ObjectUtils.isNullOrUndefined(value.dateRange) &&
            (ObjectUtils.isNullOrUndefined(value.dateRangeType) || value.dateRangeType === 'custom')) {
            value.dateRange.start = moment.utc(value.dateRange.start);
            value.dateRange.end = moment.utc(value.dateRange.end);
        }
        if (!ObjectUtils.isNullOrUndefined(value.compare1Range) &&
            (ObjectUtils.isNullOrUndefined(value.compare1Type) || value.compare1Type === 'custom')) {
            value.compare1Range.start = moment.utc(value.compare1Range.start);
            value.compare1Range.end = moment.utc(value.compare1Range.end);
        }
        if (!ObjectUtils.isNullOrUndefined(value.compare2Range) &&
            (ObjectUtils.isNullOrUndefined(value.compare2Type) || value.compare2Type === 'custom')) {
            value.compare2Range.start = moment.utc(value.compare2Range.start);
            value.compare2Range.end = moment.utc(value.compare2Range.end);
        }

        //parse string ids passed through stateParams - string ids tend to return empty values for map in pdf export
        if(!ObjectUtils.isNullOrUndefined(value.locationId)) {
            value.locationId = Number(value.locationId);
        }

        if(!ObjectUtils.isNullOrUndefined(value.organizationId)) {
            value.organizationId = Number(value.organizationId);
        }

        if(!ObjectUtils.isNullOrUndefined(value.siteId)) {
            value.siteId = Number(value.siteId);
        }

        if (siteIds.indexOf(value.siteId) === -1) {
            siteIds.push(value.siteId);
        }
        if (orgIds.indexOf(value.organizationId) === -1) {
            orgIds.push(value.organizationId);
        }

        if (ObjectUtils.isNullOrUndefined(value.dateFormat)) {
            value.dateFormat = 'MM/DD/YYYY';
        }

        if (value.dateRangeType !== 'custom' ||
            value.compare1Type !== 'custom' ||
            value.compare2Type !== 'custom'
        ) {
            var currentOrganization = $scope.organizations[value.organizationId];

            if (ObjectUtils.isNullOrUndefined(value.currentUser)) {
                value.currentUser = $scope.currentUser;
            }
        }
        if (value.dateRangeType !== 'custom') {
            var dateRange = utils.getDateRangeForType(
                value.dateRangeType,
                currentOrganization,
                value.currentUser
            );

            if (dateRange) {
                value.dateRange = {
                    start: dateRange.start,
                    end: dateRange.end
                };
            }
        }
        if (value.compare1Type !== 'custom') {
            var compare1Range = getCompareRange(
                value.compare1Type,
                value.dateRange,
                currentOrganization,
                value.currentUser
            );

            if(angular.isDefined(compare1Range)) {
              value.compare1Range = {
                  start: compare1Range.start,
                  end: compare1Range.end
              };
            }
        }
        if (value.compare2Type !== 'custom') {
            var compare2Range = getCompareRange(
                value.compare2Type,
                value.dateRange,
                currentOrganization,
                value.currentUser
            );

            if(angular.isDefined(compare2Range)) {
              value.compare2Range = {
                  start: compare2Range.start,
                  end: compare2Range.end
              };
            }
        }

      if (typeof value.currentOrganization !== 'undefined') {
        if (
          typeof value.currentOrganization.localization !== 'undefined' &&
          typeof value.currentOrganization.localization.date_format !== 'undefined' &&
          !ObjectUtils.isNullOrUndefined( value.currentOrganization.localization.date_format.mask )
          ) {
            value.currentOrganization.localization.date_format.mask = value.currentOrganization.localization.date_format.mask.replace(/\|/g, '/');
          }
      }
    });

    $scope.exportsParsed = true;
  }

  function decodeExportCartSlashes(value) {
    if (!ObjectUtils.isNullOrUndefined(value.dateFormat)) {
      value.dateFormat = value.dateFormat.replace(/\|/g, '/');
    }
    if (!ObjectUtils.isNullOrUndefined(value.currentUser)) {
      if (
        typeof value.currentUser.localization !== 'undefined' &&
        typeof value.currentUser.localization.date_format !== 'undefined' &&
        !ObjectUtils.isNullOrUndefined( value.currentUser.localization.date_format.mask )
      ) {
        value.currentUser.localization.date_format.mask = value.currentUser.localization.date_format.mask.replace(/\|/g, '/');
      }
    }
    if (!ObjectUtils.isNullOrUndefined(value.currentOrganization)) {
      if (
        typeof value.currentOrganization.localization !== 'undefined' &&
        typeof value.currentOrganization.localization.date_format !== 'undefined' &&
        !ObjectUtils.isNullOrUndefined( value.currentOrganization.localization.date_format.mask )
      ) {
        value.currentOrganization.localization.date_format.mask = value.currentOrganization.localization.date_format.mask.replace(/\|/g, '/');
      }
    }
    return value;
  }

  function getCompareRange(type, selectedRange, organization, user) {
    var compareRange, firstDay;
    var firstDayOfWeekSetting = LocalizationService.getCurrentCalendarFirstDayOfWeek();
    if(firstDayOfWeekSetting===1) {
      firstDay = 'Monday';
    } else {
      firstDay = 'Sunday';
    }
    if(type === 'prior_period') {
      compareRange = utils.getPreviousCalendarPeriodDateRange({
        start: selectedRange.start,
        end: selectedRange.end
      }, user, organization);
    } else if(type === 'prior_year') {
      compareRange = utils.getEquivalentPriorYearDateRange({
        start: selectedRange.start,
        end: selectedRange.end
      }, firstDay, user, organization);
    }
    return compareRange;
  }

  function getOrganizationById(orgId) {
    return  OrganizationResource.get({orgId: orgId}).$promise;
  }

  function getOrganizationTags(orgId) {
    $scope.orgTags[orgId] = {};
    var organization = getOrganizationById(orgId);
    organization.then(function(result) {
      if(!ObjectUtils.isNullOrUndefined(result.portal_settings) &&
         !ObjectUtils.isNullOrUndefined(result.portal_settings.group_structures)) {
        _.each(result.portal_settings.group_structures, function(tagGroup) {
          _.each(tagGroup.levels, function(tag) {
            _.each(tag.possible_values, function(tagValue) {
              $scope.orgTags[orgId][tagValue._id] = tagValue.name;
            });
          });
        });
      }
    });
  }

  function setExportParams() {
    _.each($scope.exports, function(item) {
      item.currentOrganization=currentOrganization;
      item.currentUser = $scope.currentUser;
    });
  }

  function loadOrganizations(orgIds) {
    var deferred = $q.defer();

    var promises = [];

    _.each(orgIds, function(orgIdStr) {
      var orgId = Number(orgIdStr);
      if(ObjectUtils.isNullOrUndefined($scope.organizations[orgId])) {
        promises.push(OrganizationResource.get({orgId: orgId}).$promise.then(function(data) {
          $scope.organizations[orgId] = data;
        }));
      }
    });

    $q.all(promises).then(function() {
      deferred.resolve();
    });

    return deferred.promise;
  }

  function loadOrganizationTags() {
    _.each($scope.exports, function(item) {
      if (ObjectUtils.isNullOrUndefined(item.tags) &&
          ObjectUtils.isNullOrUndefined($scope.orgTags[item.organizationId])) {
        getOrganizationTags(item.organizationId);
      }
    });
  }

  function loadSites() {
    _.each($scope.exports, function(item) {
      if(ObjectUtils.isNullOrUndefined($scope.sites[item.organizationId]) ||
         ObjectUtils.isNullOrUndefined($scope.sites[item.organizationId][item.siteId])) {
        if(typeof $scope.sites[item.organizationId] === 'undefined') {
          $scope.sites[item.organizationId] = [];
        }
        $scope.sites[item.organizationId][item.siteId] = getSite(item);
      }
    });
  }

  function loadZones() {
    _.each($scope.exports, function(item) {
      if(ObjectUtils.isNullOrUndefined($scope.zones[item.organizationId]) ||
         ObjectUtils.isNullOrUndefined($scope.zones[item.organizationId]) ||
         ObjectUtils.isNullOrUndefined($scope.zones[item.organizationId][item.siteId]) ||
         ObjectUtils.isNullOrUndefined($scope.zones[item.organizationId][item.siteId][item.zoneId])) {
        if(typeof $scope.zones[item.organizationId] === 'undefined') {
          $scope.zones[item.organizationId] = [];
        }
        if(typeof $scope.zones[item.organizationId][item.siteId] === 'undefined') {
          $scope.zones[item.organizationId][item.siteId] = [];
        }

        getZone(item).$promise.then(function(zoneInfo){
          var zoneName = zoneInfo.name;
          zoneInfo.name = ZoneHelperService.removeLeadingX(zoneName);
          $scope.zones[item.organizationId][item.siteId][item.zoneId] = zoneInfo;
        });

      }
    });
  }

  function loadLocations() {
    _.each($scope.exports, function(item) {
      if(!ObjectUtils.isNullOrUndefined(item.locationId) && !ObjectUtils.isNullOrUndefined(item.organizationId) && !ObjectUtils.isNullOrUndefined(item.siteId)) {
        if(ObjectUtils.isNullOrUndefined($scope.locations[item.organizationId]) ||
          ObjectUtils.isNullOrUndefined($scope.locations[item.organizationId]) ||
          ObjectUtils.isNullOrUndefined($scope.locations[item.organizationId][item.siteId]) ||
          ObjectUtils.isNullOrUndefined($scope.locations[item.organizationId][item.siteId][item.locationId])) {
          if(typeof $scope.locations[item.organizationId] === 'undefined') {
            $scope.locations[item.organizationId] = [];
          }
          if(typeof $scope.locations[item.organizationId][item.siteId] === 'undefined') {
            $scope.locations[item.organizationId][item.siteId] = [];
          }

          getLocation(item).$promise.then(function(location){
            $scope.locations[item.organizationId][item.siteId][item.locationId] = location;
          });
        }
      }
    });
  }


  function hasOrganizationId(exportCart) {
    return !ObjectUtils.isNullOrUndefined(exportCart.organizationId);
  }

  function getSite(exportCart) {
    if(!ObjectUtils.isNullOrUndefinedOrEmpty(exportCart.currentSite)) {
      return exportCart.currentSite;
    } else {
      return SiteResource.get({orgId: exportCart.organizationId, siteId: exportCart.siteId});
    }
  }

  function hasSiteId(exportCart) {
    return !ObjectUtils.isNullOrUndefined(exportCart.siteId) && exportCart.siteId !== 'tags';
  }

  function getZone(exportCart) {
    if(!ObjectUtils.isNullOrUndefinedOrEmpty(exportCart.currentZone)) {
      return exportCart.currentZone;
    } else {
      if(!ObjectUtils.isNullOrUndefined(exportCart.zoneId)) {
        exportCart.zoneId = Number(exportCart.zoneId);
      }
      return new ZoneResource().get({orgId: exportCart.organizationId, siteId: exportCart.siteId, zoneId: exportCart.zoneId});
    }
  }

  function hasLocationId(exportCart) {
    return !ObjectUtils.isNullOrUndefined(exportCart.locationId) && exportCart.locationId !== 'zone' && exportCart.siteId !== 'tags';
  }

  function hasZoneId(exportCart) {
    return !ObjectUtils.isNullOrUndefined(exportCart.zoneId);
  }

  function getLocation(exportCart) {
    if(!ObjectUtils.isNullOrUndefinedOrEmpty(exportCart.currentLocation)) {
      return exportCart.currentLocation;
    } else {
      if(!ObjectUtils.isNullOrUndefined(exportCart.locationId)) {
        exportCart.locationId = Number(exportCart.locationId);
      }
      return LocationResource.get({orgId: exportCart.organizationId, siteId: exportCart.siteId, locationId: exportCart.locationId});
    }
  }

  $scope.getExportTimePeriodTitle = function(exportElm) {
    var start = exportElm.dateRange.start.format(exportElm.dateFormat);

    if(exportElm.summaryKey.indexOf('real_time') >= 0) {
      return start;
    }
    var end = exportElm.dateRange.end.format(exportElm.dateFormat);
    var period = start + ' - ' + end;

    if(!ObjectUtils.isNullOrUndefined(exportElm.compare1Range)) {
      var start2 = exportElm.compare1Range.start.format(exportElm.dateFormat);
      var end2= exportElm.compare1Range.end.format(exportElm.dateFormat);

      if(start2 !== 'Invalid date') {
        period += ', ' + start2 + ' - ';
      }
      if(end2 !== 'Invalid date') {
        period += end2;
      }
    }

    if(!ObjectUtils.isNullOrUndefined(exportElm.compare2Range) &&
      exportElm.hideCompare2Range !== true) {
      var start3 =  exportElm.compare2Range.start.format(exportElm.dateFormat);
      var end3 = exportElm.compare2Range.end.format(exportElm.dateFormat);
      if(start3 !== 'Invalid date') {
        period += ' & ' + start3+ ' - ';
      }
      if(end3 !== 'Invalid date') {
        period += end3;
      }
    }

    return period;
  };

  function getColumnCssClass(key) {
    switch (key) {
      case 'traffic_percentage_location_table':
      case 'traffic_percentage_correlation_table':
      case 'locations_before_table':
      case 'locations_after_table':
      case 'first_visits_table':
      case 'one_and_done_table':
        return 'col-xs-6';
      default:
        return 'col-xs-12';
    }
  };

  function loadTranslations() {
    $translate.use($scope.language);
  }

  function getKpiValuesForKpi(){
    $scope.kpiValues ={};
    $scope.kpiValues.ats ='ats';
    $scope.kpiValues.conversion ='conversion';
    $scope.kpiValues.average_abandonment_rate ='abandonment_rate';
    $scope.kpiValues.draw_rate ='draw_rate';
    $scope.kpiValues.dwell_time ='dwell_time';
    $scope.kpiValues.opportunity ='opportunity';
    $scope.kpiValues.gsh ='gsh';
    $scope.kpiValues.labor_hours ='labor_hours';
    $scope.kpiValues.sales = 'sales';
    $scope.kpiValues.star = 'star';
    $scope.kpiValues.traffic = 'traffic';
    $scope.kpiValues.upt = 'upt';
  }

  /**
    * Replaces the date range and compare dates with up to date ranges based upon the widgets params. Private function.
    * This function returns nothing, but instead acts on the current object.
    * @param {widget} widget - A saved widget object
    */
    function updateDateRanges(widget) {
      if (widget.dateRangeShortCut === 'custom' && widget.customRange === null){
        var startDate = moment().subtract(widget.xDaysBack, 'days').startOf('day');
        var endDate = moment(startDate.add(widget.xDaysDuration, 'days').endOf('day'));
        widget.dateRange = {
          start: moment().subtract(widget.xDaysBack, 'days').startOf('day'),
          end: endDate
        }
      } else if (!!widget.dateRangeShortCut.match(/(day|week|month|year)/)) {
        widget.dateRange = setDateShortCut(widget);
      } else {
        switch (widget.customRange) {
          case 'wtd': setWTD(widget); break;
          case 'mtd': setMTD(widget); break;
          case 'qtd': setQTD(widget); break;
          case 'ytd': setYTD(widget); break;
        }
      }
    }

    /**
    * Sets the date range & compare periods based upon the shortcut type. Private function.
    * This function returns a new date range which is applied to the widget.
    * @param {type} type - a date range shortcut (string) that has been saved to the widget object
    */
      function setDateShortCut(widget) {
        var newDateRange;
        var type = widget.dateRangeShortCut;

        LocalizationService.setOrganization(widget.currentOrganization);

        var calendarInfo = LocalizationService.getSystemYearForDate(moment());
        if (!LocalizationService.hasMonthDefinitions()) calendarInfo.month -= 1;

        var currentMonth = calendarInfo.month;
        var currentYear = calendarInfo.year;

        switch (type) {
          case 'day':
            newDateRange = {
              start: moment.utc().startOf('day').subtract(1, 'day').startOf('day'),
              end: moment.utc().startOf('day').subtract(1, 'day').endOf('day')
            };
            break;
          case 'week':
            newDateRange = {
              start: LocalizationService.getFirstDayOfCurrentWeek().subtract(1, 'week'),
              end: LocalizationService.getLastDayOfCurrentWeek().subtract(1, 'week').endOf('day')
            };
            break;
          case 'month':
            var previousYear;
            var previousMonth = currentMonth - 1;
            if (previousMonth < 0) {
              previousMonth = 11;
              previousYear = currentYear - 1;
            } else {
              previousYear = currentYear;
            }
            newDateRange = {
              start: LocalizationService.getFirstDayOfMonth(previousMonth, previousYear),
              end: LocalizationService.getLastDayOfMonth(previousMonth, previousYear)
            };
            break;
          case 'year':
            newDateRange = {
              start: LocalizationService.getFirstDayOfYear(currentYear - 1),
              end: LocalizationService.getLastDayOfYear(currentYear - 1)
            };
            break;
        }

        return newDateRange;
      }

    /**
    * Sets the date range & compare periods to week to date. Private function.
    * This function returns nothing, but instead acts on the current object.
    * @param {widget} widget - A saved widget object
    */
    function setWTD(widget) {
      widget.dateRange.start = LocalizationService.getFirstDayOfCurrentWeek();
      widget.dateRange.end = moment.utc().subtract(1, 'day').endOf('day');
    }

    /**
    * Sets the date range & compare periods to month to date. Private function.
    * This function returns nothing, but instead acts on the current object.
    * @param {widget} widget - A saved widget object
    */
    function setMTD(widget) {
      var currentTime = moment.utc()
      var systemDate = LocalizationService.getSystemYearForDate(currentTime);
      var currentMonth = systemDate.month - 1;
      var currentYear = systemDate.year;

      widget.dateRange.start = LocalizationService.getFirstDayOfMonth(currentMonth, currentYear);
      widget.dateRange.end = moment.utc().subtract(1, 'day').endOf('day');
    }

    /**
    * Sets the date range & compare periods to quarter to date. Private function.
    * This function returns nothing, but instead acts on the current object.
    * @param {widget} widget - A saved widget object
    */
    function setQTD(widget) {
      widget.dateRange = utils.getDateRangeForType(widget.customRange, widget.currentOrganization, widget.currentUser);
    }

    /**
    * Sets the date range & compare periods to year to date. Private function.
    * This function returns nothing, but instead acts on the current object.
    * @param {widget} widget - A saved widget object
    */
    function setYTD(widget) {
      var currentTime = moment.utc();
      var systemDate = LocalizationService.getSystemYearForDate(currentTime);
      var currentYear = systemDate.year;

      widget.dateRange.start = LocalizationService.getFirstDayOfYear(currentYear);
      widget.dateRange.end = moment.utc().subtract(1, 'day').endOf('day');
    }

    function getPageBreakCssClass(exportKey, index, isHourly) {
      if(isHourly === true) {
        var maxIndex = $scope.exports.length -1;

        if(index === maxIndex) {
          return 'page-break-avoid';
        }

        return 'page-break-after';
      }
      
      switch (exportKey) {
        // Tenant table widgets:
        case 'tenant_sales_table_widget':
        case 'tenant_conversion_table_widget':
        case 'tenant_traffic_table_widget':
        case 'tenant_ats_table_widget':
        case 'tenant_upt_table_widget':
        case 'tenant_labor_hours_table_widget':
        case 'tenant_star_labor_table_widget':
        // Heatmap widgets:
        case 'traffic_percentage_location':
        case 'traffic_percentage_correlation':
        case 'first_visits':
        case 'one_and_done':
        case 'locations_after':
        case 'locations_before':
          if (index > 0) return 'page-break-before'; // Don't force break if it's the first widget in the export
        default:
          return 'page-break-avoid';
      }
    }

}]);
