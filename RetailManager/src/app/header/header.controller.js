(function () {
  'use strict';
  angular.module('shopperTrak').controller('HeaderCtrl', HeaderCtrl);

  HeaderCtrl.$inject = [
    '$scope',
    '$rootScope',
    '$state',
    '$location',
    'authService',
    'ExportService',
    'SubscriptionsService',
    'LocationResource',
    'currentUser',
    'OrganizationResource',
    'organizations',
    '$translate',
    'LocalizationService',
    'metricConstants',
    'currencyService',
    'ObjectUtils',
    'apiUrl',
    'sitesRequestsService',
    'googleAnalytics',
    'features'
  ];

  function HeaderCtrl(
    $scope,
    $rootScope,
    $state,
    $location,
    authService,
    ExportService,
    SubscriptionsService,
    LocationResource,
    currentUser,
    OrganizationResource,
    organizations,
    $translate,
    LocalizationService,
    metricConstants,
    currencyService,
    ObjectUtils,
    apiUrl,
    sitesRequestsService,
    googleAnalytics,
    features
  ) {
    $scope.orgSelect = {};
    $scope.siteSelect = {};
    $scope.siteSelect.sites = [];

    $scope.siteSelectionDropdownIsOpen = false;
    $scope.getExportCartItemCount = ExportService.getCartItemCount;
    $scope.logout = authService.logout;
    $scope.numPdfExportsInProgress = 0;
    $scope.$state = $state;
    $scope.currentUser = currentUser;
    $scope.scheduleExportCurrentViewToPdf = scheduleExportCurrentViewToPdf;
    $scope.organizations = organizations;
    $scope.organizationDropdownIsOpen = false;
    $scope.orgSelect.currentOrganization = null;
    $scope.appDropdownIsOpen = false;
    $scope.clearExportCart = clearExportCart;
    $scope.language = LocalizationService.getCurrentLocaleSetting();
    $scope.setCurrencySymbol = setCurrencySymbol;
    $scope.navigateToAnalytics = navigateToAnalytics;

    $scope.campaignAccess = false;
    $scope.adminToolAccess = $scope.currentUser.superuser;
    $scope.showHeaderInformation = true;

    var stateChangeSuccessDebounced = _.debounce(function(event, toState, toParams) {
      // The state change is debounced as we don't want the redirects skewing our tracking data
      trackStateChange(event, toState, toParams);
      if (!ObjectUtils.isNullOrUndefined(toState) && toState.name !== 'analytics.organization.custom-dashboard') {
        setCurrencySymbol();
      }
    }, 200);

    activate();
    configureDropDowns();

    function activate() {

      googleAnalytics.setUserId(currentUser._id);
      $scope.usedTypeAhead = false;
      var unWatchAddMore, cleanUpSearchTextChanged, cleanUpExportStart, cleanUpExportFinish;
      updateCurrentOrganization();
      $translate.use($scope.language).then(function () {
        loadTranslations();
      });

      $scope.$on('$stateChangeSuccess', updateCurrentOrganization);
      $scope.$on('$stateChangeSuccess', updateCurrentSite);
      $scope.$on('$stateChangeSuccess', stateChangeSuccessDebounced);

      $scope.$watch('orgSelect.currentOrganization', updateSites);
      $scope.$watchCollection('organizations', updateCurrentOrganization);
      $scope.$watchCollection('siteSelect.sites', updateCurrentSite);
      $scope.$watchCollection('orgSelect.currentOrganization', setSiteCurrentUiSrefPattern);
      $scope.sitesPageNumber = 1;

      cleanUpSearchTextChanged = $scope.$on('searchTextChanged', function (event, searchValue) {
        if (($scope.searchTerm === '' && searchValue === '') || ($scope.searchTerm !== searchValue)) {
          $scope.searchUsed = false;
          $scope.searchTerm = searchValue;
          $rootScope.savedSearchterm = searchValue;
          $scope.sitesPageNumber = 1;
          addSites(1);
        }
      });

      cleanUpExportStart = $scope.$on('pdfExportStart', function () {
        $scope.numPdfExportsInProgress++;
      });

      cleanUpExportFinish = $scope.$on('pdfExportFinish', function () {
        $scope.numPdfExportsInProgress--;
      });

      unWatchAddMore = $scope.$watch('siteSelect.addMoreSites', function (nv) {
        if (nv) {
          $scope.sitesPageNumber++;
          addSites($scope.sitesPageNumber);
        }
      });

      $scope.$on('$destroy', function () {
        if (typeof unWatchAddMore === 'function') {
          unWatchAddMore();
        }
        if (typeof cleanUpSearchTextChanged === 'function') {
          cleanUpSearchTextChanged();
        }
        if (typeof cleanUpExportStart === 'function') {
          cleanUpExportStart();
        }
        if (typeof cleanUpExportFinish === 'function') {
          cleanUpExportFinish();
        }
      });
      $scope.currentStateName = $state.current.name.split('.');

      if ($state.current.name.indexOf('admin.home') !== -1) {
        $scope.showHeaderInformation = false;
      }
    }

    function hasMiAccess(user, orgId) {
      //ToDo: Remove the feature check
      if(!features.isEnabled('indexReport')) {
        return false;
      }

      if($state.current.name === 'csv') {
        return false;
      }

      var currentOrganization = getOrganizationById(orgId);

      return (SubscriptionsService.hasMarketIntelligence(currentOrganization) && SubscriptionsService.userHasMarketIntelligence(currentUser, orgId));
    }

    function addSites(pageNumber) {
      var params = {
        page_number: pageNumber,
        page_size: 25
      };
      if (!ObjectUtils.isNullOrUndefined($scope.searchTerm) && $scope.searchTerm.length > 3) {
        $scope.usedTypeAhead = true;
        angular.extend(params, {
          site_search_term: $scope.searchTerm
        });
      }
      $scope.isSearchingItems = true;
      getSiteRequest(params).then(function (output) {
        if (pageNumber > 1) {
          var c = $scope.siteSelect.sites.concat(output.data.result);
          $scope.siteSelect.sites = c;
        } else {
          $scope.siteSelect.sites = output.data.result;
        }
        if (output.data.result.length > 0) {
          $scope.siteSelect.addMoreSites = false;
        }
        $scope.isSearchingItems = false;
      });
    }

    function scheduleExportCurrentViewToPdf() {
      $rootScope.$broadcast('scheduleExportCurrentViewToPdf');
    }

    function updateCurrentOrganization(nv, ov, newParams, nw, oldParams) {
      if (organizations.length === 1) {
        $scope.orgSelect.currentOrganization = organizations[0];
      } else if ($state.params.orgId) {
        if (resetSavedSearchterm(newParams, oldParams)) {
          $rootScope.savedSearchterm = '';
        }
        $scope.orgSelect.currentOrganization = getOrganizationById($state.params.orgId);
      } else {
        $scope.orgSelect.currentOrganization = null;
      }

      if(!ObjectUtils.isNullOrUndefined($scope.orgSelect.currentOrganization)) {
        $scope.campaignAccess = SubscriptionsService.hasCampaigns($scope.orgSelect.currentOrganization);
      }

      $scope.siteSelect.addMoreSites = false;
      $scope.searchTerm = '';
      $scope.sitesPageNumber = 1;
      setCurrencySymbol();
      $scope.orgMIReportAccess = hasMiAccess(currentUser, $state.params.orgId);
    }

    function resetSavedSearchterm(newParams, oldParams) {
      if (ObjectUtils.isNullOrUndefined(newParams) || ObjectUtils.isNullOrUndefined(oldParams)) {
        return true;
      }
      if (ObjectUtils.isNullOrUndefined(newParams.orgId) || ObjectUtils.isNullOrUndefined(oldParams.orgId)) {
        return true;
      }
      if (newParams.orgId !== oldParams.orgId) {
        return ObjectUtils.isNullOrUndefined(newParams.siteId);
      } else {
        return ObjectUtils.isNullOrUndefined(newParams.siteId) && !ObjectUtils.isNullOrUndefined(oldParams.siteId);
      }
    }

    function updateSites() {
      if ($scope.orgSelect.currentOrganization) {
        $scope.isFetchingFirst = true;
        var params = {
          page_number: 1,
          page_size: 25
        };

        getSiteRequest(params).then(function (output) {
          $scope.siteSelect.sites = output.data.result;
          $scope.isSearchingItems = false;
          $scope.isFetchingFirst = false;
        });
      } else {
        $scope.siteSelect.sites = [];
        $scope.isSearchingItems = false;
      }
    }

    function getSiteRequestUrl() {
      return apiUrl + '/organizations/' + $scope.orgSelect.currentOrganization.organization_id + '/sites/';
    }

    function getSiteRequest(params) {
      return sitesRequestsService.get(getSiteRequestUrl(), params);
    }

    function setSiteCurrentUiSrefPattern() {
      if (ObjectUtils.isNullOrUndefined($scope.orgSelect.currentOrganization) || $scope.orgSelect.currentOrganization.site_count < 2) {
        //single site so remove url from single site to stop refresh
        $scope.usedTypeAhead = false;
        $scope.siteSelect.siteCurrentUiSrefPattern = null;
      }
    }


    function updateCurrentSite() {
      if ($scope.siteSelect.sites.length === 1 && !ObjectUtils.isNullOrUndefined($scope.searchTerm) && $scope.searchTerm.length < 4) {
        $scope.siteSelect.currentSite = $scope.siteSelect.sites[0];
      } else if ($state.params.siteId) {
        $scope.searchTerm = $rootScope.savedSearchterm;
        $scope.siteSelect.currentSite = getSiteById($state.params.siteId);
      } else {
        $scope.siteSelect.currentSite = undefined;
      }
      setCurrencySymbol();
    }

    function getOrganizationById(orgId) {
      return _.findWhere($scope.organizations, { organization_id: orgId });
    }

    function getSiteById(siteId) {
      return _.findWhere($scope.siteSelect.sites, { site_id: siteId });
    }

    function loadTranslations() {
      $translate(['header.SELECTORGANIZATION', 'header.ORGANIZATIONSUMMARY', 'header.SELECTPROPERTY', 'header.SEARCHSITES']).then(function (results) {
        $scope.siteSelect.activeLinkHeader = results['header.ORGANIZATIONSUMMARY'];
        $scope.orgSelect.selectOrgHeader = results['header.SELECTORGANIZATION'];
        $scope.siteSelect.selectSiteHeader = results['header.SELECTPROPERTY'];
        $scope.siteSelect.promptText = results['header.SEARCHSITES'];
      });
    }

    function configureDropDowns() {
      $scope.orgSelect.orgCurrentUiSrefPattern = 'analytics.organization({ orgId: item.organization_id })';
      $scope.siteSelect.siteUiSrefPattern = 'analytics.organization.site({orgId: item.organization.id,siteId: item.site_id,locationId: null })';
      $scope.siteSelect.orgUiSrefPattern = 'analytics.organization({orgId: currentItem.organization.id,siteId: null,locationId: null })';
      $scope.siteSelect.siteCurrentUiSrefPattern = 'analytics.organization.site({orgId: currentItem.organization.id,siteId: currentItem.site_id,locationId: null })';
    }

    function setCurrencySymbol() {
      if (ObjectUtils.isNullOrUndefined($scope.orgSelect.currentOrganization)) {
        return;
      }
      var orgId = $scope.orgSelect.currentOrganization.organization_id;
      var siteId;

      if (!ObjectUtils.isNullOrUndefined($scope.siteSelect.currentSite)) {
        siteId = $scope.siteSelect.currentSite.site_id;
      }

      currencyService.getCurrencySymbol(orgId, siteId).then(function (currencyInfo) {

        if (currencyInfo.orgId !== $scope.orgSelect.currentOrganization.organization_id) {
          // This call back is late, go away
          return;
        }

        if (!ObjectUtils.isNullOrUndefined($scope.siteSelect.currentSite) && currencyInfo.siteId !== $scope.siteSelect.currentSite.site_id) {
          // This call back is late, go away
          return;
        }

        _.filter(metricConstants.metrics, function (metric) {
          if (metric.isCurrency === true) {
            metric.prefixSymbol = currencyInfo.currencySymbol;
          }
        });
      });
    }

    function clearExportCart() {
      ExportService.clearExportCart();
      var cleanUpExportCall = $rootScope.$broadcast('clearCurrentPDfExports');

      $scope.$on('$destroy', function () {
        if (typeof cleanUpExportCall === 'function') {
          cleanUpExportCall();
        }
      });
    }

    function navigateToAnalytics() {
      if(ObjectUtils.isNullOrUndefined($scope.orgSelect) ||
        ObjectUtils.isNullOrUndefined($scope.orgSelect.currentOrganization)) {
        $state.go('analytics');
      } else {
        $state.go('analytics.organization', { orgId: $scope.orgSelect.currentOrganization.organization_id });
      }
    }

    function trackStateChange(event, toState, toParams) {
      // We track only the location.path as we don't want the date params skewing tracking data
      var currentPath = $location.$$path;

      if(ObjectUtils.isNullOrUndefinedOrBlank(currentPath)) {
        return;
      }

      if(routeRequiresDateRanges(toParams) && !hasDateRanges(toParams)) {
        return;
      }

      googleAnalytics.sendPageView(currentPath);
    }

    function routeRequiresDateRanges(toParams) {
      var dateRangeProperties = getDateRangeProperties();

      return _.every(dateRangeProperties, function(dateRangeProperty) {
        return _.has(toParams, dateRangeProperty);
      });
    }

    function hasDateRanges(toParams) {
      var dateRangeProperties = getDateRangeProperties();

      return _.every(dateRangeProperties, function(dateRangeProperty) {
        return !ObjectUtils.isNullOrUndefined(toParams[dateRangeProperty]);
      });
    }

    function getDateRangeProperties() {
      return [
        'dateRangeEnd',
        'dateRangeStart',
        'compareRange1Start',
        'compareRange1End',
        'compareRange2Start',
        'compareRange2End'
      ]
    }
  }
})();
