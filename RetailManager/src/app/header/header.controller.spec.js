'use strict';

describe('HeaderController', function() {
  var $scope;
  var $controller;

  var organizations;

  var trackingMock = {
    sendPageView: function (page) {
      angular.noop(page);
    },
    setUserId: function (userId) {
      angular.noop(userId);
    }
  };

  beforeEach(module('shopperTrak'));
  beforeEach(inject(function($rootScope, _$controller_) {
    $scope = $rootScope.$new();
    $controller = _$controller_;

    spyOn(_, 'debounce').and.callFake(function(cb) { 
      return function(event, toState, fromState) { 
        cb(event, toState, fromState);
      }
    });

    organizations = [{
      'organization_id': 1000
    }, {
      'organization_id': 2000
    }];
  }));

  describe('activate', function() {

    var stateMock;
    var mockLocation;

    beforeEach(function() {
      stateMock = {
        current: {
          name: 'analytics.organization'
        },
        go: jasmine.createSpy('go')
      };

      mockLocation = {
        $$path: '/traffic/1234'
      };
    });

    it('should send the current userId to googleAnalytics', function() {
      spyOn(trackingMock, 'setUserId');

      getController(stateMock, mockLocation, organizations, trackingMock);

      expect(trackingMock.setUserId).toHaveBeenCalledWith('1234');
    });
  });

  it('should send the current route to the page tracking service with the base URL only', function() {
    var $stateMock = {
      current: {
        name: 'analytics.organization'
      },
      go: jasmine.createSpy('go')
    };

    var mockLocation = {
      $$path: '/traffic/1234'
    };

    getController($stateMock, mockLocation, organizations, trackingMock);

    spyOn(trackingMock, 'sendPageView');

    $scope.$broadcast('$stateChangeSuccess');

    expect(trackingMock.sendPageView).toHaveBeenCalledWith('/traffic/1234');
  });

  it('should not send the current route to the page tracking service if the base URL is blank', function() {
    var $stateMock = {
      current: {
        name: 'analytics.organization'
      },
      go: jasmine.createSpy('go')
    };

    var mockLocation = {
      $$path: ''
    };

    getController($stateMock, mockLocation, organizations, trackingMock);

    spyOn(trackingMock, 'sendPageView');

    $scope.$broadcast('$stateChangeSuccess');

    expect(trackingMock.sendPageView).not.toHaveBeenCalled();
  });

  it('should not send the current route to the page tracking service if the route requires date ranges but they have not been set', function() {
    var $stateMock = {
      current: {
        name: 'analytics.organization'
      },
      go: jasmine.createSpy('go')
    };

    var mockLocation = {
      $$path: '/traffic/1234'
    };

    getController($stateMock, mockLocation, organizations, trackingMock);

    spyOn(trackingMock, 'sendPageView');

    var fakeRouteParams = {
      dateRangeEnd: undefined,
      dateRangeStart: undefined,
      compareRange1Start: undefined,
      compareRange1End: undefined,
      compareRange2Start: undefined,
      compareRange2End: undefined
    };

    $scope.$broadcast('$stateChangeSuccess', null, fakeRouteParams);

    expect(trackingMock.sendPageView).not.toHaveBeenCalled();
  });

  it('should send the current route to the page tracking service if the route requires date ranges and they have been set', function() {
    var $stateMock = {
      current: {
        name: 'analytics.organization'
      },
      go: jasmine.createSpy('go')
    };

    var mockLocation = {
      $$path: '/traffic/1234'
    };

    getController($stateMock, mockLocation, organizations, trackingMock);

    spyOn(trackingMock, 'sendPageView');

    var fakeRouteParams = {
      dateRangeEnd: moment(),
      dateRangeStart: moment(),
      compareRange1Start: moment(),
      compareRange1End: moment(),
      compareRange2Start: moment(),
      compareRange2End: moment()
    };

    $scope.$broadcast('$stateChangeSuccess', null, fakeRouteParams);

    expect(trackingMock.sendPageView).toHaveBeenCalledWith('/traffic/1234');
  });

  function getController(stateMock, locationMock, organizations, trackingMock) {
    stateMock.params = { };
    return $controller('HeaderCtrl', {
      '$scope': $scope,
      '$state': stateMock,
      '$location' : locationMock,
      'organizations': organizations,
      'googleAnalytics': trackingMock,
      'currentUser': {
        _id: '1234'
      }
    });
  }
});