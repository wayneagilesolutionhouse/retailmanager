(function () {
  'use strict';

  angular.module('shopperTrak')
    .factory('adminOrganizationsData', adminOrganizationsData);

  adminOrganizationsData.$inject = [
    '$state',
    '$q',
    '$http',
    'ObjectUtils',
    'Upload',
    'apiUrl',
    'authService'
  ];

  function adminOrganizationsData($state, $q, $http, ObjectUtils, Upload, apiUrl, authService) {
    // Keep reference to data in memory
    var _organizations = null;

    function fetchOrganizations(callback, useCache) {
      authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {
          if (ObjectUtils.isNullOrUndefined(callback)) {
            callback = {
              success: function () {},
              failed: function () {}
            };
          }
          // set useCache to true by default
          if (ObjectUtils.isNullOrUndefined(useCache)) {
            useCache = true;
          }

          // Use cached data if available
          if (_organizations && useCache === true) {
            return callback.success(transformResponseData(_organizations));
          }

          // Request data from back-end
          var url = apiUrl + '/organizations?all_fields=true';
          return $http.get(url).then(function (result) {
            _organizations = result.data;

            var transformedData = transformResponseData(result.data);
            callback.success(transformedData);
          }, callback.failed);
        } else {
          $state.go('analytics');
        }
      });
    }

    function getOrganization(id, useCache) {
      return authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {
          var org = {};
          var deferred = $q.defer();

          if (!ObjectUtils.isNullOrUndefined(_organizations) && useCache === true) {
            org = _.find(_organizations.result, function (obj) {
              return obj.organization_id === id;
            });

            deferred.resolve(org);
          } else {
            var url = apiUrl + '/organizations/' + id;

            $http.get(url).then(function (result) {
              deferred.resolve(result.data.result[0]);
            }, function (error) {
              deferred.reject(error.data.message);
            });
          }

          return deferred.promise;
        } else {
          $state.go('analytics');
        }
      });
    }

    function getOrganizationCalendars(orgId) {
      return authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {
          var deferred = $q.defer();

          var url = apiUrl + '/organizations/' + orgId + '/calendars';

          $http.get(url).then(function (result) {
            deferred.resolve(result.data.result);
          }, function (error) {
            deferred.reject(error.data.message);
          });

          return deferred.promise;
        } else {
          $state.go('analytics');
        }
      });
    }

    function updateSettings(orgId, params, callback) {
      authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {
          var url = apiUrl + '/organizations/' + orgId;
          $http.put(url, params).then(function (result) {

            if (!ObjectUtils.isNullOrUndefined(_organizations)) {
              updateOrganizationsData(orgId, result.data);
            }

            callback.success(result);

          }, callback.failed);

        } else {
          $state.go('analytics');
        }
      });
    }

    function refreshOrganization(id, callback) {
      authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {
          var url = apiUrl + '/organizations/' + id + '?refresh=true';

          $http.put(url).then(function (result) {
            var updatedData = updateOrganizationsData(id, result.data);
            callback.success(updatedData);
          }, callback.failed);
        } else {
          $state.go('analytics');
        }
      });
    }

    function createOrganization(org, callback) {
      authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {
          var url = apiUrl + '/organizations';

          var data = {
            orgId: org.orgId,
            name: org.name,
            subscriptions: org.subscriptions,
            default_calendar_id: org.default_calendar_id,
            timeOfDay: org.timeOfDay
          };

          $http.post(url, data).then(function (result) {
            if (!ObjectUtils.isNullOrUndefined(_organizations)) {
              _organizations.result.push(result.data.result[0]);
            }

            callback.success(result);
          }, callback.failed);
        } else {
          $state.go('analytics');
        }
      });
    }

    function removeOrganization(id, callback) {
      authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {
          var url = apiUrl + '/organizations/' + id;

          $http.delete(url).then(function () {
            callback.success();
          }, function (error) {
            callback.failed(error);
          });
        } else {
          $state.go('analytics');
        }
      });
    }

    function postOSMFile(orgId, osm, fileIndex, callback) {
      authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {
          if (ObjectUtils.isNullOrUndefined(orgId)) {
            throw new Error('org id is required.');
          }

          if (ObjectUtils.isNullOrUndefined(osm.file)) {
            throw new Error('osm file is required');
          }

          var url = apiUrl + '/organizations/' + orgId + '/osm?noBatch=true';
          var data = {
            osm: osm.file
          };

          if (!ObjectUtils.isNullOrUndefined(osm.floor)) {
            data.floor = osm.floor;
          }

          return Upload.upload({
            'url': url,
            'data': data
          }).then(function () {
            callback.success(fileIndex);
          }, function (error) {
            callback.failed(error, fileIndex);
          });
        } else {
          $state.go('analytics');
        }
      });
    }

    function updateOrganizationsData(id, data) {
      if (_organizations) {
        _organizations.result = _.map(_organizations.result, function (obj) {
          if (obj.organization_id === id) {
            return data.result[0];
          } else {
            return obj;
          }
        });
      }
      // Return transformed data for easy consumption
      return transformResponseData(_organizations);
    }

    function transformResponseData(responseData) {
      var transformedData = [];
      if (responseData) {
        transformedData = responseData.result.map(function (obj) {
          return {
            id: obj.organization_id,
            name: obj.name,
            type: obj.portal_settings && obj.portal_settings.organization_type,
            subscriptions: obj.subscriptions,
            default_calendar_id: obj.default_calendar_id,
            updated: new Date(obj.updated).toLocaleDateString()
          };
        });
      }
      return transformedData;
    }

    return {
      fetchOrganizations: fetchOrganizations,
      refreshOrganization: refreshOrganization,
      updateSettings: updateSettings,
      createOrganization: createOrganization,
      removeOrganization: removeOrganization,
      postOSMFile: postOSMFile,
      getOrganization: getOrganization,
      getOrganizationCalendars: getOrganizationCalendars,
      transformResponseData: transformResponseData
    };
  }
})();
