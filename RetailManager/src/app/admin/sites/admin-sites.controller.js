'use strict';

angular.module('shopperTrak')
  .controller('AdminSitesController', AdminSitesController);

AdminSitesController.$inject = [
  '$scope',
  '$state'
];

function AdminSitesController($scope, $state) {
  var vm = this;

  function updateLocation(path) {
    var base = '#/admin/organizations/' + $state.params.orgId;
    location.hash = base + path;
  }

  function editSite(site_id) {
    $state.go('admin.organizations.edit.site', {
      siteId: site_id
    });
  }

  function addSite() {
    updateLocation('/sites/add');
  }

  vm.editSite = editSite;
  vm.addSite = addSite;

}
