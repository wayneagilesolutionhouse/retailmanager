(function() {
  'use strict';

  angular.module('shopperTrak')
    .controller('AdminMenuController', AdminMenuController);

  AdminMenuController.$inject = [
    '$scope',
    '$state',
    'localStorageService'
  ];

  function AdminMenuController(
    $scope,
    $state,
    localStorageService
  ) {
    var vm = this;

    // NOTE: Admin menu UI-behaviour/styles/markup has parity with analytics-menu
    // and ideally would be refactored so we don't have repeated code.

    var sidebarStateKey = 'has-open-sidebar';
    var sidebarTransitionClass = 'is-transitioning-analytics-menu';
    var htmlEl = document.documentElement;
    vm.isExpanded = vm.isExpanded || false;
    vm.toggleExpanded = toggleExpanded;
    vm.stateIsActive = stateIsActive;
    vm.menuItems = [];

    vm.menuItems.push({
      'name': 'Organization management',
      'state': 'admin.organizations',
      'href': '#/admin/organizations',
      'icon': 'company'
    });

    vm.menuItems.push({
      'name': 'User management',
      'state': 'admin.usermanagement',
      'href': '#/admin/usermanagement',
      'icon': 'avatar'
    });

    /* this will come later once we have SA-731 done
    vm.menuItems.push({
      'name': 'Tag management',
      'state': 'admin.tag.managment',
      'icon': 'tags',
    }); */

    activate();

    function activate() {
      if (localStorageService.get(sidebarStateKey) === 1) toggleExpanded(true);
    }

    function toggleExpanded(expandState) {
      // Update state:
      vm.isExpanded = _.isUndefined(expandState) ? !vm.isExpanded : expandState;
      htmlEl.classList.toggle('has-expanded-analytics-menu', expandState);
      htmlEl.classList.add(sidebarTransitionClass);
      localStorageService.set(sidebarStateKey, vm.isExpanded ? 1 : 0);
      // Post-transition behaviour:
      setTimeout(expandedTransitionDone, 241);
    }

    function expandedTransitionDone() {
      // Force re-render of charts:
      window.dispatchEvent(new CustomEvent('resize'));
      // Remove transition style tweaks:
      setTimeout(function(){
        htmlEl.classList.remove(sidebarTransitionClass);
      },100);
    }

    function stateIsActive(stateName) {
      return $state.current.name.indexOf(stateName) !== -1;
    }
  }
})();
