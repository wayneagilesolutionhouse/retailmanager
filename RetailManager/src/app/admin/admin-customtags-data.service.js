(function () {
  'use strict';

  angular.module('shopperTrak')
    .factory('adminCustomTagsData', adminCustomTagsData);

  adminCustomTagsData.$inject = [
    '$state',
    '$http',
    'ObjectUtils',
    'apiUrl',
    'Upload',
    'authService'
  ];

  function adminCustomTagsData($state, $http, ObjectUtils, apiUrl, Upload, authService) {

    var defaultState = 'analytics';

    //Returns all tags for given organisation.
    //get:/organizations/:orgId/custom-tags
    function getAllTags(orgId, callback) {

      if (ObjectUtils.isNullOrUndefined(orgId)) {
        throw new Error('getAllTags: orgId is undefined');
      }

      if (ObjectUtils.isNullOrUndefined(callback)) {
        throw new Error('getAllTags: callback is undefined');
      }

      authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {

          var getAllTagsURL = apiUrl + '/organizations/' + orgId + '/custom-tags';
          $http.get(getAllTagsURL).then(function (response) {

            callback.success(response.data.result);

          }, callback.failed);
        } else {
          $state.go(defaultState);
        }
      });
    }

    //Returns a single tag for a given organisation.
    function getTag(orgId, tagId, callback) {

      if (ObjectUtils.isNullOrUndefined(orgId)) {
        throw new Error('getTag: orgId is undefined');
      }

      if (ObjectUtils.isNullOrUndefined(tagId)) {
        throw new Error('getTag: tagId is undefined');
      }

      if (ObjectUtils.isNullOrUndefined(callback)) {
        throw new Error('getTag: callback is undefined');
      }

      authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {
          var getTagURL = apiUrl + '/organizations/' + orgId + '/custom-tags/' + tagId;
          $http.get(getTagURL).then(function (response) {
            callback.success(response.data);
          }, callback.failed);
        } else {
          $state.go(defaultState);
        }
      });
    }

    //Deletes a specfic tag for an organisation.
    //delete:/organizations/:orgId/custom-tags/:customTagTypeId/custom-tag
    function addTag(orgId, values, callback) {
      authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {

          if (ObjectUtils.isNullOrUndefined(orgId)) {
            throw new Error('updateTag: orgId is undefined');
          }

          if (ObjectUtils.isNullOrUndefined(values)) {
            throw new Error('updateTag: orgId is undefined');
          }

          if (ObjectUtils.isNullOrUndefined(callback)) {
            throw new Error('updateTag: callback is undefined');
          }

          var addTagURL = apiUrl + '/organizations/' + orgId + '/custom-tags';
          $http.post(addTagURL, values).then(
            function (response) {
              // This is quite a complex travese so checking we have something valid.
              if (ObjectUtils.isNullOrUndefined(response.data.result[0].custom_tags)) {
                throw new Error('A data api call failed to return expected data -: ' + addTagURL);
              }

              callback.success(response.data.result[0].custom_tags);
            },
            function (error) {
              callback.failed(error);
            }
          );

        } else {
          $state.go(defaultState);
        }
      });
    }

    //Deletes a specfic tag for an organisation.
    //delete:/organizations/:orgId/custom-tags/:customTagTypeId/custom-tag
    function deleteTag(orgId, tagType, callback) {

      if (ObjectUtils.isNullOrUndefined(orgId)) {
        throw new Error('deleteTag: orgId is undefined');
      }

      if (ObjectUtils.isNullOrUndefined(tagType)) {
        throw new Error('deleteTag: tag is undefined');
      }

      if (ObjectUtils.isNullOrUndefined(callback)) {
        throw new Error('deleteTag: callback is undefined');
      }

      authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {
          var deleteTagURL = apiUrl + '/organizations/' + orgId + '/custom-tags/' + tagType;

          $http.delete(deleteTagURL).then(
            function (response) {
              callback.success(response.data.result[0].custom_tags);
            },
            function (error) {
              callback.failed(error)
            });

        } else {
          $state.go(defaultState);
        }
      });
    }

    //Updates the tag specified in tagID. If tagID is not set we ADD the tag.
    function updateTag(orgId, tagValues, callback) {

      if (ObjectUtils.isNullOrUndefined(orgId)) {
        throw new Error('updateTag: orgId is undefined');
      }

      if (ObjectUtils.isNullOrUndefined(tagValues)) {
        throw new Error('updateTag: tagId is undefined');
      }

      if (ObjectUtils.isNullOrUndefined(callback)) {
        throw new Error('updateTag: callback is undefined');
      }

      authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {
          //As we are updating the custom Tag here - we need to check whether or not the user
          //renamed the custom tag_type. If they have we need to change the tag_type in the values
          //to the new name so that api can update the new name.

          //First we check to see if the names are different
          if (tagValues.orgTagType !== tagValues.originalTagType) {

            for (var i = 0; i < tagValues.orgTagValues.length; i++) {
              tagValues.orgTagValues[i].tag_type = tagValues.orgTagType;
            }
          }

          var updateTagURL = apiUrl + '/organizations/' + orgId + '/custom-tags/' + tagValues.originalTagType;
          $http.put(updateTagURL, tagValues).then(
            function (response) {
              callback.success(response.data.result);
            },
            function (error) {
              callback.failed(error);
            }
          );
        } else {
          $state.go(defaultState);
        }
      });
    }

    // Returns specific users that are related to a certain tag.
    // Users related to a tag :/organizations/:orgId/custom-tags/:customTagId/users
    function getUserForCustomTag(orgId, customTagId, callback) {

      if (ObjectUtils.isNullOrUndefined(orgId)) {
        throw new Error('getUserForCustomTag: orgId is undefined');
      }

      if (ObjectUtils.isNullOrUndefined(customTagId)) {
        throw new Error('getUserForCustomTag: tag is undefined');
      }

      if (ObjectUtils.isNullOrUndefined(callback)) {
        throw new Error('getUserForCustomTag: callback is undefined');
      }

      authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {

          var customTagUsersURL = apiUrl + '/organizations/' + orgId + '/custom-tags/' + customTagId + '/users';

          $http.get(customTagUsersURL).then(
            function (data) {
              //Returning the list of users we have found.
              callback.success(data.data.result);
            },
            function (error) {
              callback.failed(error)
            });

        } else {
          $state.go(defaultState);
        }
      });
    }
    
    function uploadCustomTags(orgId, file, callback) {

      if (ObjectUtils.isNullOrUndefined(orgId)) {
        throw new Error('uploadCustomTags - orgId is undefined');
      }

      if (ObjectUtils.isNullOrUndefined(file)) {
        throw new Error('uploadCustomTags - file is undefined');
      }

      if (ObjectUtils.isNullOrUndefined(callback)) {
        throw new Error('uploadCustomTags - callback is undefined');
      }

      authService.getCurrentUser().then(function (currentUser) {
          if (currentUser.superuser) {

            var url = apiUrl + '/organizations/' + orgId + '/custom-tags/import';

            var data = {
              customTagFile: file
            };

            return Upload.upload({
              'url': url,
              'data': data,
            }).then(function (result) {
              callback.success(result);
            }, function (error) {
              callback.failed(error);
            });

          } else {
            $state.go('analytics');
          }
        })
    }
  


    return {
      getTag: getTag,
      getAllTags: getAllTags,
      addTag: addTag,
      deleteTag: deleteTag,
      updateTag: updateTag,
      getUserForCustomTag: getUserForCustomTag,
      uploadCustomTags: uploadCustomTags,
    };
  }
})();
