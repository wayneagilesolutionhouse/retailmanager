(function() {
  'use strict';

  angular.module('shopperTrak')
    .factory('adminLocationsData', adminLocationsData);

  adminLocationsData.$inject = [
    '$state',
    '$q',
    '$http',
    'ObjectUtils',
    'apiUrl',
    'authService'
  ];

  function adminLocationsData($state, $q, $http, ObjectUtils, apiUrl, authService) {

    function getLocations(orgId, siteId, callback) {

      if (ObjectUtils.isNullOrUndefined(orgId)) {
        throw new Error('orgId is undefined');
      }

      if (ObjectUtils.isNullOrUndefined(siteId)) {
        throw new Error('siteId is undefined');
      }
      
      authService.getCurrentUser().then(function(currentUser) {
        if(currentUser.superuser) {
          var url = apiUrl + '/organizations/' + orgId + '/sites/' + siteId + '/locations';

          $http.get(url).then(function(response) {
            callback.success(transformLocationsData(response.data.result));
          }, callback.failed);
        } else {
          $state.go('analytics');
        }
      });
    }

    function getLocation(orgId, siteId, locationId) {
      authService.getCurrentUser().then(function(currentUser) {
        if(currentUser.superuser) {
          if (ObjectUtils.isNullOrUndefined(locationId)) {
            throw new Error('locationId is undefined');
          }

          var url = apiUrl + '/organizations/' + orgId + '/sites/' + siteId + '/locations/' + locationId;
          $http({method: 'GET', url: url});
        } else {
          $state.go('analytics');
        }
      });
    }

    function getAllLocations(orgId, callback, siteIds) {
      authService.getCurrentUser().then(function(currentUser) {
        if(currentUser.superuser) {
          if(ObjectUtils.isNullOrUndefined(orgId)) {
            throw new Error('orgId is undefined');
          }

          if(ObjectUtils.isNullOrUndefined(callback)) {
            throw new Error('callback is undefined');
          }

          var queryString = '';

          if(!ObjectUtils.isNullOrUndefined(siteIds)) {
            queryString += '?';
            _.each(siteIds, function(value, index) {
              if(index === 0) {
                queryString += 'siteId=' + value;
              } else {
                queryString += '&siteId=' + value;
              }
            });
          }

          var url = apiUrl + '/organizations/' + orgId + '/locations' + queryString;
          
          $http.get(url).then(function(result) {
            var locations = result.data.result[0];

            _.each(locations, function(location, key) {
              locations[key] = transformLocationsData(location);
            });

            callback.success(locations);
          }, callback.failed);
        } else {
          $state.go('analytics');
        }
      });
    }

    function saveLocation(orgId, siteId, locationId, data, action) {
      authService.getCurrentUser().then(function(currentUser) {
        if(currentUser.superuser) {
          var method;
          var url = apiUrl + '/organizations/' + orgId + '/sites/' + siteId+ '/locations';
          
          if (action === 'edit') {
            method = 'PUT';
            url += '/' + locationId;
          }
          else if (action === 'add') {
            method = 'POST';
          }
          else {
            throw new Error('Invalid action call');
          }

          $http({method: method, url: url, data: data});
        } else {
          $state.go('analytics');
        }
      });
    }

    function deleteLocation(orgId, siteId, locationId, callback) {
      authService.getCurrentUser().then(function(currentUser) {
        if(currentUser.superuser) {
          var url = apiUrl + '/organizations/' + orgId + '/sites/' + siteId + '/locations/' + locationId;

          $http.delete(url).then(function(result) {
            callback(result.data);
          });
        } else {
          $state.go('analytics');
        }
      });
    }

    function refresh(orgId, siteId, locationId, callback) {
      authService.getCurrentUser().then(function(currentUser) {
        if(currentUser.superuser) {
          var url = apiUrl + '/organizations/' + orgId + '/sites/' +  siteId + '/locations/' + locationId + '?refresh=true';
          $http.put(url).then(callback.success, callback.failed);
        } else {
          $state.go('analytics');
        }
      });
    }

    function transformLocationsData(responseData) {
      var transformedData = [];

      if (responseData) {
        transformedData = _.map(responseData, function (obj) {
          return {
            id: obj.location_id,
            name: obj.description,
            type: obj.location_type
          };
        });
      }

      return transformedData;
    }

    return {
      getLocations: getLocations,
      getLocation: getLocation,
      getAllLocations: getAllLocations,
      saveLocation: saveLocation,
      deleteLocation: deleteLocation,
      refresh: refresh,
      transformLocationsData: transformLocationsData
    };
  }
})();
