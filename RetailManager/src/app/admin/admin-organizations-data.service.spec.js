'use strict';

describe('adminOrganizationsData', function () {
  var $rootScope;
  var $http;
  var $httpBackend;
  var $q;
  var stateMock;
  var apiUrl;
  var adminOrganizationsData;
  var callback;
  var orgMock;
  var org;
  var orgId;
  var url;
  var errorMessage;
  var nonUpdatedOrg;
  var shouldBeSuperuser;

  beforeEach(module('shopperTrak', function ($translateProvider) {
    $translateProvider.translations('en_US', {});
  }));

  beforeEach(function () {
    apiUrl = 'https://api.url';

    callback = {
      success: function (result) {
        org = result;
      },
      failed: function (result) {
        errorMessage = result.data.statusText;
      }
    };

    orgId = 1234;

    orgMock = [{
        organization_id: 1234,
        name: 'Test Org 1',
        portal_settings: 'test',
        subscriptions: {}
      },
      {
        organization_id: 5678,
        name: 'Test Org 2',
        portal_settings: 'tester',
        subscriptions: {}
      }
    ];

    stateMock = {
      go: jasmine.createSpy('go')
    };

    module(function ($provide) {
      $provide.constant('apiUrl', apiUrl);

      $provide.factory('authService', function ($q) {
        var getCurrentUser = jasmine.createSpy('getCurrentUser').and.callFake(function () {
          if (shouldBeSuperuser) {
            return $q.when({
              superuser: true
            });
          } else {
            return $q.when({
              superuser: false
            });
          }
        });

        return {
          getCurrentUser: getCurrentUser
        };
      });

      $provide.value('$state', stateMock);
    });
  });

  beforeEach(inject(function (_$rootScope_,
    _$httpBackend_,
    _$q_,
    _adminOrganizationsData_) {

    $rootScope = _$rootScope_;
    $httpBackend = _$httpBackend_;
    $q = _$q_;

    adminOrganizationsData = _adminOrganizationsData_;
  }));

  describe('current user is superuser', function () {
    beforeEach(function () {
      shouldBeSuperuser = true;
    });

    describe('fetchOrganizations', function () {
      it('should get organizations on a successful http call', function () {
        $httpBackend.expectGET(apiUrl + '/organizations?all_fields=true').respond({
          result: orgMock
        });
        adminOrganizationsData.fetchOrganizations(callback);
        $httpBackend.flush();

        var transformedData = adminOrganizationsData.transformResponseData({
          result: orgMock
        });

        expect(JSON.stringify(org)).toEqual(JSON.stringify(transformedData));
      });

      it('should not create an http call when fetchOrganizations has already been called', function () {
        $httpBackend.expectGET(apiUrl + '/organizations?all_fields=true').respond({
          result: orgMock
        });
        adminOrganizationsData.fetchOrganizations(callback);
        $httpBackend.flush();

        adminOrganizationsData.fetchOrganizations(callback, true);
        var transformedData = adminOrganizationsData.transformResponseData({
          result: orgMock
        });
        expect(JSON.stringify(org)).toEqual(JSON.stringify(transformedData));
      });

      it('should fail on a failed http call', function () {
        $httpBackend.expectGET(apiUrl + '/organizations?all_fields=true').respond(500, {
          statusText: 'Failed to get organizations.'
        });
        adminOrganizationsData.fetchOrganizations(callback);
        $httpBackend.flush();

        expect(errorMessage).toEqual('Failed to get organizations.');
      });
    });

    describe('refreshOrganization', function () {
      beforeEach(function () {
        callback.success = function (result) {
          nonUpdatedOrg = result;
        };

        $httpBackend.expectGET(apiUrl + '/organizations?all_fields=true').respond({
          result: orgMock
        });
        adminOrganizationsData.fetchOrganizations(callback);
        $httpBackend.flush();
      });

      it('should update organization data on successful http call', function () {
        callback.success = function (result) {
          org = result;
        };

        var newOrgMock = {
          organization_id: 5678,
          name: 'New Org',
          portal_settings: 'not portal',
          subscriptions: {}
        };

        $httpBackend.expectPUT(apiUrl + '/organizations/' + 5678 + '?refresh=true').respond({
          result: [newOrgMock]
        });
        adminOrganizationsData.refreshOrganization(5678, callback);
        $httpBackend.flush();

        var nonUpdatedTransformedOrg = adminOrganizationsData.transformResponseData({
          result: nonUpdatedOrg
        });
        var updatedTransformedOrg = adminOrganizationsData.transformResponseData({
          result: [newOrgMock]
        });

        expect(JSON.stringify(org)).not.toEqual(JSON.stringify(nonUpdatedTransformedOrg));
        expect(JSON.stringify(org[1])).toEqual(JSON.stringify(updatedTransformedOrg[0]));
      });

      it('should fail on a failed http call', function () {
        $httpBackend.expectPUT(apiUrl + '/organizations/' + 5678 + '?refresh=true').respond(500, {
          statusText: 'Failed to refresh organization.'
        });
        adminOrganizationsData.refreshOrganization(5678, callback);
        $httpBackend.flush();

        expect(errorMessage).toEqual('Failed to refresh organization.');
      });
    });
  });
});
