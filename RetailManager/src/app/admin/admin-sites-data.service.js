(function () {
  'use strict';

  angular.module('shopperTrak').factory('adminSitesData', adminSitesData);

  adminSitesData.$inject = [
    '$state',
    '$q',
    '$http',
    'ObjectUtils',
    'apiUrl',
    'authService'
  ];

  function adminSitesData($state, $q, $http, ObjectUtils, apiUrl, authService) {

    function fetchSites(orgId, callback) {
      if (ObjectUtils.isNullOrUndefined(orgId)) {
        throw new Error('orgId is undefined');
      }

      authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {
          var url = apiUrl + '/organizations/' + orgId + '/sites';

          // todo : move all_fields to be in the query string isntead of params
          $http.get(url, {
            params: {
              all_fields: true
            }
          }).then(function (result) {
            callback.success(transformSitesData(result.data));
          }, callback.failed);
        } else {
          $state.go('analytics');
        }
      });
    }

    function fetchSite(orgId, siteId, callback) {
      if (ObjectUtils.isNullOrUndefined(siteId)) {
        throw new Error('siteId is undefined');
      }

      authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {
          var url = apiUrl + '/organizations/' + orgId + '/sites/' + siteId;
          $http.get(url).then(function (result) {
            callback.success(transformSitesData(result.data));
          }, callback.failed);
        } else {
          $state.go('analytics');
        }
      });
    }

    function saveSite(orgId, siteId, action) {
      // TODO : Should have $http for SA-725
      authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {
          // var url = apiUrl + '/organizations/' + orgId + '/sites';
          //
          // var method;
          if (action === 'edit') {
            //   method = 'PUT';
            //   url += '/' + siteId;
          }
          // else if (action === 'add') {
          //   method = 'POST';
          // }
          // else {
          //   throw new Error('action ' + action + ' not supported');
          // }
        } else {
          $state.go('analytics');
        }
      });
    }

    function refresh(orgId, siteId, callback) {
      // TODO : Should have $http call for when refresh site functionality
      authService.getCurrentUser().then(function (currentUser) {
        if (currentUser.superuser) {

          var url = apiUrl + '/organizations/' + orgId + '/sites/' + siteId;

          $http.get(url, {
            params: {
              all_fields: true
            }
          }).then(function (result) {
            callback.success(transformSitesData(result.data));
          }, callback.failed);

        } else {
          $state.go('analytics');
        }
      });
    }

    function transformSitesData(responseData) {
      var transformedData = [];

      if (!ObjectUtils.isNullOrUndefined(responseData)) {
        transformedData = responseData.result.map(function (obj) {
          return {
            id: obj.site_id,
            name: obj.name,
            created: new Date(obj.created).toLocaleDateString(),
            custom_tags: (typeof obj.custom_tags !== 'undefined') ? _.pluck(obj.custom_tags,'tag_id') : [],
            taggedSiteAccess : false,
            customer_site_id: obj.customer_site_id,
          };
        });
      }
      return transformedData;
    }

    return {
      fetchSites: fetchSites,
      fetchSite: fetchSite,
      saveSite: saveSite,
      refreshSite: refresh,
      transformSitesData: transformSitesData
    };
  }
})();
