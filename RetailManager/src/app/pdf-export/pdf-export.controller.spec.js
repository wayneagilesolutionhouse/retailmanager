'use strict';

describe('PdfExportCtrl', function() {
  var $rootScope;
  var $scope;
  var $controller;
  var $httpBackend;

  var apiUrl = 'https://api.url';

  var host = 'http://localhost:3000/index.html#/pdf/';
  var cart = '[{"organizationId":"8699","siteId":"80042762",' +
    '"dateRange":{"start":"2015-07-19T00:00:00.000Z","end":"2015-07-25T23:59:59.999Z"},' +
    '"comparisonDateRange":{"start":"2015-07-12T00:00:00.000Z","end":"2015-07-18T23:59:59.999Z"},' +
    '"summaryKey":"total_traffic","locationType":[],"$$hashKey":"object:980"}]';
  var doubleCart = '[{"organizationId":"8699","siteId":"80042762",' +
    '"dateRange":{"start":"2015-07-19T00:00:00.000Z","end":"2015-07-25T23:59:59.999Z"},' +
    '"comparisonDateRange":{"start":"2015-07-12T00:00:00.000Z","end":"2015-07-18T23:59:59.999Z"},' +
    '"summaryKey":"total_traffic","locationType":[],"$$hashKey":"object:980"},' +
    '{"organizationId":"8699","siteId":"80042762",' +
      '"dateRange":{"start":"2015-07-19T00:00:00.000Z","end":"2015-07-25T23:59:59.999Z"},' +
      '"comparisonDateRange":{"start":"2015-07-12T00:00:00.000Z","end":"2015-07-18T23:59:59.999Z"},' +
      '"summaryKey":"total_traffic","locationType":[],"$$hashKey":"object:980"}' +
    ']';

  beforeEach(module('shopperTrak'));
  beforeEach(function() {
    module(function($provide) {
      $provide.constant('apiUrl', apiUrl);
    });
  });
  beforeEach(inject(function(_$rootScope_, _$controller_, _$httpBackend_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $controller = _$controller_;
    $httpBackend = _$httpBackend_;
  }));
});
