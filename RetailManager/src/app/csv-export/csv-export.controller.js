(function () {
  'use strict';

  angular.module('shopperTrak')
    .controller('CSVExportCtrl', CSVExportCtrl);

  CSVExportCtrl.$inject = [
    '$scope',
    '$stateParams',
    'currentOrganization',
    'currentSite',
    'currentUser',
    '$translate',
    'LocalizationService',
    'csvExportConstants',
    '$filter',
    '$http',
    'apiUrl',
    '$anchorScroll',
    '$location',
    'SiteResource',
    'LocationResource',
    'ZoneResource',
    'utils',
    'localStorageService',
    '$q',
    '$timeout',
    'SubscriptionsService',
    'ObjectUtils',
    'internalDateFormat',
    'googleAnalytics'
  ];

  function CSVExportCtrl(
    $scope,
    $stateParams,
    currentOrganization,
    currentSite,
    currentUser,
    $translate,
    LocalizationService,
    csvExportConstants,
    $filter,
    $http,
    apiUrl,
    $anchorScroll,
    $location,
    SiteResource,
    LocationResource,
    ZoneResource,
    utils,
    localStorageService,
    $q,
    $timeout,
    SubscriptionsService,
    ObjectUtils,
    internalDateFormat,
    googleAnalytics
  ) {
    var vm = this;

    vm.currentOrganization = currentOrganization;
    vm.currentSite = currentSite;
    vm.currentUser = currentUser;
    vm.groups = csvExportConstants.groups;
    vm.groupByChoices = csvExportConstants.groupByChoices;
    vm.frequencyChoices = csvExportConstants.frequencyChoices;
    vm.activeChoices = csvExportConstants.activeChoices;

    vm.translationsLoaded = false;
    vm.activeGroup = 'perimeter';
    vm.selectedMetrics = {}; //used {} instead of [] to allow watch for changes in properties
    vm.businessHours = true;
    vm.selectedSites = [];
    vm.selectedLocations = [];
    vm.selectedZones = [];
    vm.tags = [];
    vm.customTags = [];
    vm.groupBySetting = 'day';
    vm.hoursDisabled = false;

    vm.numExportsInProgress = 0;
    vm.activeSetting = 6;
    vm.frequencySetting = vm.frequencyChoices[0];

    /** @todo: Scheduling could be extracted into own service to make this controller cleaner. */

    vm.setActiveGroup = setActiveGroup;
    vm.selectMetric = selectMetric;
    vm.setActive = setActive;
    vm.setWeekDay = setWeekDay;
    vm.setMonthDay = setMonthDay;
    vm.setMonth = setMonth;
    vm.weekDayIsSetTo = weekDayIsSetTo;
    vm.dayIsSetTo = dayIsSetTo;
    vm.monthIsSetTo = monthIsSetTo;
    vm.addEMail = addEMail;
    vm.removeEmail = removeEmail;
    vm.toggleSchedulingForm = toggleSchedulingForm;
    vm.deleteSchedule = deleteSchedule;
    vm.toggleSite = toggleSite;
    vm.toggleLocation = toggleLocation;
    vm.siteIsSelected = siteIsSelected;
    vm.locationIsSelected = locationIsSelected;
    vm.metricIsDisabled = metricIsDisabled;
    vm.selectAllSites = selectAllSites;
    vm.isAllSitesSelected = isAllSitesSelected;
    vm.selectAllZones = selectAllZones;
    vm.getSiteNameById = getSiteNameById;
    vm.removeAllSites = removeAllSites;
    vm.setSelectedFilters = setSelectedFilters;
    vm.setGroupBy = setGroupBy;
    vm.getSelectedGroupByName = getSelectedGroupByName;
    vm.doExport = doExport;
    vm.loadSchedules = loadSchedules;
    vm.organizationIsTypeRetail = organizationIsTypeRetail;
    vm.setExportSiteOverall = setExportSiteOverall;
    vm.selectAllLocations = selectAllLocations;
    vm.toggleZone = toggleZone;
    vm.zoneIsSelected = zoneIsSelected;
    vm.locationIsSelected = locationIsSelected;
    vm.getZoneNameById = getZoneNameById;
    vm.getLocationNameById = getLocationNameById;
    vm.selectAllLocations = selectAllLocations;
    vm.removeAllLocations = removeAllLocations;
    vm.removeAllZones = removeAllZones;
    vm.canBeExported = canBeExported;
    vm.removeAllTags = removeAllTags;
    vm.tagDropdownIsDisabled = tagDropdownIsDisabled;
    vm.siteDropdownIsDisabled = siteDropdownIsDisabled;
    vm.hasSubscription = hasSubscription;
    vm.toggleReport = toggleReport;
    vm.clearSitesAndFilters = clearSitesAndFilters;

    vm.mailCC = [{ email: null }];

    vm.weekDays = [
      $filter('translate')('weekdaysShort.sun'),
      $filter('translate')('weekdaysShort.mon'),
      $filter('translate')('weekdaysShort.tue'),
      $filter('translate')('weekdaysShort.wed'),
      $filter('translate')('weekdaysShort.thu'),
      $filter('translate')('weekdaysShort.fri'),
      $filter('translate')('weekdaysShort.sat')
    ];

    vm.monthParts = [
      $filter('translate')('common.FIRST'),
      $filter('translate')('common.15TH'),
      $filter('translate')('common.LAST')
    ];

    vm.months = [
      $filter('translate')('monthsShort.january'),
      $filter('translate')('monthsShort.february'),
      $filter('translate')('monthsShort.march'),
      $filter('translate')('monthsShort.april'),
      $filter('translate')('monthsShort.may'),
      $filter('translate')('monthsShort.june'),
      $filter('translate')('monthsShort.july'),
      $filter('translate')('monthsShort.august'),
      $filter('translate')('monthsShort.september'),
      $filter('translate')('monthsShort.october'),
      $filter('translate')('monthsShort.november'),
      $filter('translate')('monthsShort.december')
    ];

    var now = moment().utc();

    activate();

    var firstDayWatch = $scope.$watch('vm.firstDay', function () {
      if (vm.firstDay !== undefined && vm.dateRange === undefined) {
        if (vm.firstDay === 1) {
          vm.dateRange = {
            start: moment(now).startOf('week').subtract(1, 'week').add(1, 'days'),
            end: moment(now).startOf('week').subtract(1, 'week').endOf('week').add(1, 'days')
          };
        } else {
          vm.dateRange = {
            start: moment(now).startOf('week').subtract(1, 'week'),
            end: moment(now).startOf('week').subtract(1, 'week').endOf('week')
          };
        }
        vm.selectedDateRange = angular.copy(vm.dateRange);
      }
    });

    var activeGroupWatch = $scope.$watch('vm.activeGroup', function () {
      if (vm.activeGroup === 'interior') {
        vm.hoursDisabled = true;
        vm.businessHours = false;

        if (vm.groupBySetting === 'hour') {
          vm.groupBySetting = 'day';
        }
      } else {
        vm.hoursDisabled = false;
      }
    });

    var selectedMetricsWatch = $scope.$watchCollection('vm.selectedMetrics', function(newValue, oldValue) {
      if(newValue === oldValue) {
        return;
      }
      setSalesCategoryVisibility();
    });

    $scope.$on('$destroy', function() {
      selectedMetricsWatch();
      activeGroupWatch();
      firstDayWatch();
    });

    function activate() {
      getActiveSubscriptions();

      LocalizationService.setOrganization(vm.currentOrganization);
      LocalizationService.setUser(vm.currentUser);

      vm.firstDay = getOrganizationStartOfWeek();
      vm.language = LocalizationService.getCurrentLocaleSetting();
      vm.dateFormat = LocalizationService.getCurrentDateFormat(vm.currentOrganization);

      if (currentSite !== null) {
        vm.siteLevel = true;
        vm.currentSiteName = vm.currentSite.name;
        vm.locations = loadSiteLocations(vm.currentOrganization, vm.currentSite);
        vm.zones = loadSiteZones(vm.currentOrganization, vm.currentSite);
      } else {
        vm.siteLevel = false;
        vm.currentOrganizationName = vm.currentOrganization.name;
        vm.sites = loadOrganizationSites(vm.currentOrganization);
      }

      // If only one tag, site or kpi is provided, they will string instead of array
      if ($stateParams.tag !== undefined) {
        if (typeof $stateParams.tag === 'string') {
          $stateParams.tag = [$stateParams.tag];
        }
        vm.tags = $stateParams.tag;
      }
      if ($stateParams.site !== undefined) {
        if (typeof $stateParams.site === 'string') {
          $stateParams.site = [$stateParams.site];
        }
        vm.selectedSites = $stateParams.site;
      }
      if ($stateParams.kpi !== undefined) {
        if (typeof $stateParams.kpi === 'string') {
          $stateParams.kpi = [$stateParams.kpi];
        }
        var kpis = $stateParams.kpi;
        _.each(kpis, function (kpi) {

          var metric = _.findWhere(vm.metrics, {kpi: kpi});

          var group = _.findWhere(vm.groups, {name: metric.group});

          if(vm.hasSubscription(group.subscription) && vm.hasSubscription(metric.subscription)) {
            vm.selectedMetrics[kpi] = true;
          }
        });

        setSalesCategoryVisibility();
      }

      if ($stateParams.startDate !== undefined && $stateParams.endDate !== undefined) {
        vm.dateRange = {
          start: moment.utc($stateParams.startDate, internalDateFormat),
          end: moment.utc($stateParams.endDate, internalDateFormat)
        };
        vm.selectedDateRange = angular.copy(vm.dateRange);
      }

      loadTranslations();
      loadSchedules();
      loadCalendars();
      loadSalesCategories();
    }

    function getActiveSubscriptions(){
      if (!ObjectUtils.isNullOrUndefined(vm.currentSite)){
        if (!ObjectUtils.isNullOrUndefined(vm.currentSite.subscriptions)){
          vm.activeSubscriptions = SubscriptionsService.getSubscriptions(vm.currentSite);
        } else {
          //use org data
          vm.activeSubscriptions = SubscriptionsService.getSubscriptions(vm.currentOrganization);
        }
      } else {
        vm.activeSubscriptions = SubscriptionsService.getSubscriptions(vm.currentOrganization);
      }

      vm.metrics = filterSubscriptions(csvExportConstants.metrics);
    }

    function filterSubscriptions(obj) {
      return _.filter(obj, function (subClass) {
        return (subClass.subscription === 'any' || _.contains(vm.activeSubscriptions, subClass.subscription));
      });
    }

    function loadSalesCategories() {
      if(ObjectUtils.isNullOrUndefinedOrEmpty(vm.currentOrganization.portal_settings.sales_categories)) {
        vm.showSalesCategories = false;
        return;
      }

      if(vm.currentOrganization.portal_settings.sales_categories.length <= 1) {
        vm.showSalesCategories = false;
        return;
      }

      vm.salesCategories = vm.currentOrganization.portal_settings.sales_categories;

      var selectedSalesCategories = {};

      _.each(vm.salesCategories, function(salesCategory) {
        selectedSalesCategories[salesCategory.id] = false;
      });

      if(typeof $stateParams.salesCategories === 'string') {
        $stateParams.salesCategories = [$stateParams.salesCategories];
      }

      _.each($stateParams.salesCategories, function (salesCategory) {
        selectedSalesCategories[salesCategory] = true;
      });

      if(!ObjectUtils.isNullOrUndefinedOrEmptyObject(selectedSalesCategories)) {
        vm.selectedSalesCategories = selectedSalesCategories;
      }

      vm.showSalesCategories = true;
    }

    function loadCalendars() {
      LocalizationService.getAllCalendars().then(function (calendars) {
        vm.organizationCalendars = calendars.result;
        LocalizationService.setAllCalendars(vm.organizationCalendars);
        vm.calendarsLoaded = true;
      });
    }

    function hasInteriorSubscription() {
      if (vm.currentSite !== null) {
        return SubscriptionsService.siteHasInterior(vm.currentOrganization, vm.currentSite);
      } else {
        return SubscriptionsService.siteHasInterior(vm.currentOrganization);
      }
    }

    function hasPerimeterSubscription() {
      if (vm.currentSite !== null) {
        return SubscriptionsService.siteHasPerimeter(vm.currentOrganization, vm.currentSite);
      } else {
        return SubscriptionsService.siteHasPerimeter(vm.currentOrganization);
      }
    }

    function hasSalesSubscription() {
      if (vm.currentSite !== null) {
        return SubscriptionsService.siteHasSales(vm.currentOrganization, vm.currentSite);
      } else {
        return SubscriptionsService.siteHasSales(vm.currentOrganization);
      }
    }
    function hasLaborSubscription() {
      if (vm.currentSite !== null) {
        return SubscriptionsService.siteHasLabor(vm.currentOrganization, vm.currentSite);
      } else {
        return SubscriptionsService.siteHasLabor(vm.currentOrganization);
      }
    }

    function hasSubscription(subscription) {
      if (subscription === 'any') {
        return true;
      } else {
        if (subscription === 'sales') {
          return hasSalesSubscription();
        } else if (subscription === 'labor') {
          return hasLaborSubscription();
        } else if (subscription === 'interior') {
          return hasInteriorSubscription();
        } else if (subscription === 'perimeter') {
          return hasPerimeterSubscription();
        }
      }
    }

    function toggleSite(siteId) {
      if (!siteIsSelected(siteId)) {
        vm.selectedSites.push(siteId);
      } else {
        var index = vm.selectedSites.indexOf(siteId);
        vm.selectedSites.splice(index, 1);
      }
    }

    function toggleLocation(locationId) {
      if (!locationIsSelected(locationId)) {
        vm.selectedLocations.push(locationId);
      } else {
        var index = vm.selectedLocations.indexOf(locationId);
        vm.selectedLocations.splice(index, 1);
      }
    }

    function toggleZone(zoneId) {
      if (!zoneIsSelected(zoneId)) {
        vm.selectedZones.push(zoneId);
      } else {
        var index = vm.selectedZones.indexOf(zoneId);
        vm.selectedZones.splice(index, 1);
      }
    }

    function siteIsSelected(siteId) {
      return (vm.selectedSites.indexOf(siteId) > -1);
    }

    function locationIsSelected(locationId) {
      return (vm.selectedLocations.indexOf(locationId) > -1);
    }

    function zoneIsSelected(zoneId) {
      return (vm.selectedZones.indexOf(zoneId) > -1);
    }

    function metricIsDisabled(kpi) {
      var disabled = false;
      if (vm.groupBySetting === 'hour' && kpi !== 'traffic') {
        disabled = true;
      }
      return disabled;
    }

    function selectAllSites() {
      if (vm.selectedSites.length === vm.sites.length) {
        vm.selectedSites = [];
        vm.allSelected = false;
      } else {
        vm.selectedSites = [];
        _.each(vm.sites, function (site) {
          vm.selectedSites.push(site.site_id);
        });
        vm.allSelected = true;
      }
    }

    function isAllSitesSelected() {
      return (vm.selectedSites.length === vm.sites.length);
    }

    function selectAllZones() {
      if (vm.selectedZones.length === vm.zones.length) {
        vm.selectedZones = [];
      } else {
        vm.selectedZones = [];
        _.each(vm.zones, function (zone) {
          vm.selectedZones.push(zone.id);
        });
      }
    }

    function tagDropdownIsDisabled() {
      if (Object.keys(vm.selectedSites).length > 0) {
        return true;
      } else {
        return false;
      }
    }

    function siteDropdownIsDisabled() {
      if (vm.tags.length > 0) {
        return true;
      } else {
        return false;
      }
    }

    function pushToList(allLocationsList) {
      var childListLenght = null;

      angular.forEach(allLocationsList, function (selectedLocation) {
        var locationId = selectedLocation.location_id;
        var childList = selectedLocation.sublist;

        vm.toggleLocation(locationId);

        angular.forEach(childList, function (selectedChildLocation) {
          vm.toggleLocation(selectedChildLocation.location_id);
          childListLenght++;
        });

      });

      if (childListLenght === 0) {
        $scope.selectedItemsLength = childListLenght;
      }
    }

    function setExportSiteOverall(value) {
      vm.exportSiteOverall = value;
    }

    function loadTranslations() {
      $translate.use(vm.language);
      vm.translationsLoaded = true;
    }

    function selectMetric(metric) {
      if (vm.metricIsDisabled(metric)) {
        return false;
      }
      vm.selectedMetrics[metric] = !vm.selectedMetrics[metric];

      setSalesCategoryVisibility();
    }

    function setSalesCategoryVisibility() {
      var salesMetricIsSelected = false;

      _.each(vm.metrics, function(metric) {
        if(vm.selectedMetrics[metric.kpi] === true && metric.subscription === 'sales') {
          salesMetricIsSelected = true;
        }
      });

      if(vm.salesMeasureIsSelected !== salesMetricIsSelected) {
        vm.salesMeasureIsSelected = salesMetricIsSelected;
      }
    }

    function setActiveGroup(group) {
      vm.selectedMetrics = [];
      vm.activeGroup = group;
    }

    function setActive(active) {
      vm.activeSetting = active;
    }

    function setWeekDay(day) {
      vm.weekDaySetting = day;
    }

    function setMonthDay(day) {
      vm.monthDaySetting = day;
    }

    function setGroupBy(groupBy) {
      /* Temporary fix: if groupBy = hour, unselect sales metrics */
      var salesMetrics = ['sales', 'conversion', 'ats', 'upt', 'labor_hours', 'star'];

      salesMetrics.forEach(function (metric) {
        vm.selectedMetrics[metric] = false;
      });

      vm.groupBySetting = groupBy;
    }

    function setMonth(month) {
      vm.monthSetting = month;
    }

    function weekDayIsSetTo(day) {
      return vm.weekDaySetting === day;
    }

    function dayIsSetTo(day) {
      return vm.monthDaySetting === day;
    }

    function monthIsSetTo(month) {
      return vm.monthSetting === month;
    }

    function getSelectedGroupByName() {
      var setting = _.find(vm.groupByChoices, { name: vm.groupBySetting });
      return setting.translation_label;
    }

    function addEMail() {
      vm.mailCC.push({
        email: null
      });
    }

    function removeEmail(index) {
      vm.mailCC.splice(index, 1);
    }

    function setSelectedFilters(filters, customTags) {
      vm.tags = [];
      vm.customTags = [];
      vm.selectedTagNames = filters[1];

      // selected custom tags
      if( !ObjectUtils.isNullOrUndefinedOrEmptyObject(customTags) ) {
        vm.customTags =  _.keys(_.pick(customTags, function(_selected) {
          return _selected === true;
        }));
      }


      // Selected Tags
      var selectedTags = filters[0];
      vm.tags = _.keys(_.pick(selectedTags, function(_selected) {
        return _selected === true;
      }));
    }

    function toggleSchedulingForm() {
      vm.formIsVisible = !vm.formIsVisible;
      if (vm.formIsVisible) {
        $location.hash('schedule-form');
        $anchorScroll();
      } else {
        $location.hash('');
      }
    }

    function canBeExported() {
      if (hasSelectedMetric()) {
        if (!vm.siteLevel && (Object.keys(vm.selectedSites).length > 0 || vm.tags.length > 0 || vm.customTags.length > 0)) {
          return true;
        } else {
          if (vm.siteLevel && vm.hasSubscription('interior') && Object.keys(vm.selectedLocations).length > 0) {
            return true;
          } else if (vm.siteLevel && vm.hasSubscription('perimeter') && Object.keys(vm.selectedZones).length > 0) {
            return true;
          } else {
            return false;
          }
        }
      } else {
        return false;
      }
    }

    function hasSelectedMetric() {
      for (var metric in vm.selectedMetrics) {
        if (vm.selectedMetrics[metric] === true) {
          return true;
        }
      }
      return false;
    }

    function organizationIsTypeRetail() {
      return currentOrganization.portal_settings.organization_type === 'Retail';
    }

    function removeAllSites() {
      vm.selectedSites = [];
    }

    function removeAllTags() {
      vm.tags = [];
      vm.customTags = [];
      vm.selectedTagData = [];
    }

    function removeAllLocations() {
      vm.selectedLocations = [];
    }

    function removeAllZones() {
      vm.selectedZones = [];
    }

    function getSiteNameById(siteId) {
      var site = _.where(vm.sites, { site_id: siteId });
      return site[0].name;
    }

    function getZoneNameById(zoneId) {
      var zone = _.where(vm.zones, { id: zoneId });
      return zone[0].name;
    }

    function getLocationNameById(locationId) {
      var location = _.where(vm.locations, { location_id: locationId });
      return location[0].description;
    }

    function selectAllLocations(allLocationsList) {
      var selectedItems = vm.selectedLocations;
      var selectedChildItemsLength = vm.selectedItemsLength;
      var selectedItemsLength = selectedItems.length;
      var totalLength = selectedItemsLength + selectedChildItemsLength;

      if (selectedItems.length !== totalLength || selectedItems.length === 0) {
        pushToList(allLocationsList);
      } else {
        vm.selectedLocations = [];
      }
    }

    function getOrganizationStartOfWeek() {
      return LocalizationService.getCurrentCalendarFirstDayOfWeek();
    }

    function loadOrganizationSites(currentOrganization) {
      return SiteResource.query({
        orgId: currentOrganization.organization_id
      });
    }

    function loadSiteLocations(currentOrganization, currentSite) {
      return LocationResource.query({
        orgId: currentOrganization.organization_id,
        siteId: currentSite.site_id
      });
    }

    function loadSiteZones(currentOrganization, currentSite) {
      return new ZoneResource().query({
        orgId: currentOrganization.organization_id,
        siteId: currentSite.site_id
      });
    }

    function loadSchedules() {
      $http.get(apiUrl + '/organizations/' + $stateParams.orgId + '/scheduled-reports?exportType=csv')
        .then(function (response) {
          vm.schedules = response.data.result;
          vm.isOpenReport = null;

          if (typeof vm.schedules === 'object') {
            vm.schedules.map(function (schedule) {
              if (schedule.data && schedule.data.userId) {
                $http.get(apiUrl + '/users/' + schedule.data.userId).then(function (response) {
                  schedule.data.username = response.data.result[0].username;
                  return schedule;
                });
              }
            });
            vm.schedules = _.sortBy(vm.schedules, function(schedule) {
              if (schedule.data && schedule.data.scheduleEndDate) {
                return schedule.data.scheduleEndDate;
              } else {
                return schedule._id;
              }
            });
          }
        },
        function (error) {
          console.log(error);
          vm.loadingSchedulesFailed = true;
        });
    }

    function deleteSchedule(schedule) {
      $http.delete(apiUrl + '/organizations/' + schedule.data.orgId + '/scheduled-reports/' + schedule._id)
        .then(function () {
          vm.isOpenReport = null;
          vm.loadSchedules();
        },
        function (error) {
          console.log(error);
          vm.deletingScheduleFailed = true;
        });
    }

    function getSelectedMetricIds() {
      var metrics = [];
      _.each(Object.keys(vm.selectedMetrics), function (key) {
        if (vm.selectedMetrics[key]) {
          metrics.push(key);
        }
      });
      return metrics;
    }

    function getSelectedSalesCategoryIds() {
      if(ObjectUtils.isNullOrUndefined(vm.selectedSalesCategories)) {
        return undefined;
      }

      // This is to mitigate an API error
      if(vm.salesMeasureIsSelected === false) {
        return undefined;
      }

      var salesCategories = _.keys(_.pick(vm.selectedSalesCategories, function(_selected) {
        return _selected === true;
      }));

      return salesCategories;
    }

    function doExport() {
      delete vm.errorMessage;

      vm.numExportsInProgress++;

      var dateRange = {
        start: vm.dateRange.start,
        end: vm.dateRange.end
      };

      var params = {
        groupBy: vm.groupBySetting,
        orgId: vm.currentOrganization.organization_id,
        reportStartDate: moment(vm.dateRange.start).toISOString(),
        reportEndDate: moment(vm.dateRange.end).toISOString(),
        dateRangeType: utils.getDateRangeType(dateRange, currentUser, currentOrganization),
        kpi: getSelectedMetricIds()
      };

      // We'll use sales_category_id param only, if org has sales categories defined
      if(!ObjectUtils.isNullOrUndefined(vm.salesCategories) && isSalesMetricsSelected()) {
        params.sales_category_id = getSelectedSalesCategoryIds();
      }

      if (vm.activeGroup === 'perimeter') {
        // Only perimeter data uses operatingHours
        params.operatingHours = vm.businessHours;
      }

      if (vm.siteLevel) {
        params.siteId = vm.currentSite.site_id;
        params.includeSiteNames = true;
        if (vm.activeGroup === 'perimeter') {
          params.zoneId = vm.selectedZones;
          params.includeZoneNames = true;
        } else {
          params.locationId = vm.selectedLocations;
        }
      } else {
        params.hierarchyTagId = vm.tags;
        params.customTagId = vm.customTags;
        params.siteId = vm.selectedSites;
        params.includeSiteNames = true;
      }

      $http.get(apiUrl + '/kpis/report', {
        headers: {
          'Accept': 'text/csv'
        },
        params: params
      }).then(function (response) {
        googleAnalytics.trackUserEvent('csv', 'generate');

        if(vm.showSalesCategories) {
          response.data = applySalesCategoryNames(response.data);
        }

        utils.saveFileAs('report-site.csv', response.data, 'text/csv');
        vm.numExportsInProgress--;

      },function () {
        vm.errorMessage = 'There was an error requesting data.';
        vm.numExportsInProgress--;
      });
    }

    function isSalesMetricsSelected() {
      var hasSalesMetrics = false;
      var selectedMetrics = getSelectedMetricIds();
      var salesMetrics = [];

      _.each(vm.metrics, function(metric) {
        if(metric.subscription === 'sales') {
          salesMetrics.push(metric.kpi);
        }
      });

      _.each(selectedMetrics, function(item) {
        if(salesMetrics.indexOf(item) > -1) {
          hasSalesMetrics = true;
        }
      });

      return hasSalesMetrics;
    }

    function toggleReport(index) {
      if(!ObjectUtils.isNullOrUndefined(vm.isOpenReport) && vm.isOpenReport[index]) {
        vm.isOpenReport[index] = false;
      } else {
        vm.isOpenReport = [];
        vm.isOpenReport[index] = true;
      }
    }

    function clearSitesAndFilters() {
      vm.removeAllSites();
      vm.removeAllTags();
      vm.selectedTagData = [];
    }

    function applySalesCategoryNames(response) {
      var lines = response.split('\r\n');

      var outputLines = [];

      var salesCategoryIndex;

      var headings = csvToArray(lines[0]);

      _.each(headings, function(heading, index) {
        if(heading === 'sales_category_id') {
          salesCategoryIndex = index;
        }
      });

      if(ObjectUtils.isNullOrUndefined(salesCategoryIndex)) {
        return response;
      }

      outputLines.push(lines[0]);

      _.each(lines, function(line, index) {
        if(index !== 0 && line !== '') {

          var lineValues = csvToArray(line);

          lineValues[salesCategoryIndex] = getSalesCategoryName(lineValues[salesCategoryIndex]);

          var updatedLine = '"' + lineValues.join('","') + '"';

          outputLines.push(updatedLine);
        }
      });

      outputLines.push('');

      return outputLines.join('\r\n');
    }

    function getSalesCategoryName(id) {
      if(ObjectUtils.isNullOrUndefinedOrBlank(id)) {
        return '';
      }

      var idInt = parseInt(id.replace('"', ''));

      var salesCat = _.findWhere(vm.salesCategories, {id: idInt});

      if(ObjectUtils.isNullOrUndefined(salesCat)) {
        return id;
      }

      return salesCat.name;
    }

    /**
     * converts a csv string to an array of strings
     * lifted from: http://stackoverflow.com/a/8497474/228770
     */
    function csvToArray(text) {
      var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
      var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
      // Return NULL if input string is not well formed CSV string.
      if (!re_valid.test(text)) {
        return null;
      }

      var a = [];

      text.replace(re_value, // "Walk" the string using replace with callback.
        function(m0, m1, m2, m3) {
          // Remove backslash from \' in single quoted values.
          if(typeof m1 !== 'undefined') {
            a.push(m1.replace(/\\'/g, '\''));
          }
          // Remove backslash from \" in double quoted values.
          else if (typeof m2 !== 'undefined') {
            a.push(m2.replace(/\\"/g, '"'));
          }

          else if (typeof m3 !== 'undefined') {
            a.push(m3);
          }
          return ''; // Return empty string.
      });

      // Handle special case of empty last value.
      if (/,\s*$/.test(text)) {
        a.push('');
      }

      return a;
    }

  }
})();
