'use strict';

describe('CSVExportCtrl', function() {
  var $scope;
  var $controller;
  var $httpBackend;

  var apiUrl;
  var constants;
  var organizationMock;
  var viewController;
  var currentUserMock;
  var zoneResourceMock;
  var locationResourceMock;
  var calendarsMock;
  var localizationService;

  beforeEach(function() {
    apiUrl = 'https://api.url';
    constants = {
      subscriptions: [
        {
          name: 'perimeter',
          translation_label: 'Perimeter'
        },
        {
          name: 'interior',
          translation_label: 'Interior'
        }
      ],
      metrics: [
        {
          kpi: 'foo',
          icon: 'foo-icon',
          subscription: 'interior',
          translation_label: 'fooLabel'
        },
        {
          kpi: 'bar',
          icon: 'bar-icon',
          subscription: 'perimeter',
          translation_label: 'barLabel'
        }
      ],
      groupByChoices: [
        {
          name: 'hour'
        },
        {
          name: 'day'
        }
      ],
      frequencyChoices: [{
        value: 'day'
      },{
        value: 'week'
      }],
      activeChoices: [
        6, 12, 18
      ]
    };

    organizationMock = {
      organization_id: 1234,
      portal_settings: {
        organization_type: 'Retail'
      },
      subscriptions: {
        labor: true,
        sales: true,
        interior: false,
        perimeter: true
      }
    };

  });

  beforeEach(module('shopperTrak', function($translateProvider) {
    $translateProvider.translations('en_US', {});
  }));

  beforeEach(module(function($provide) {
    $provide.constant('apiUrl', apiUrl);
    $provide.constant('csvExportConstants', constants)
  }));

  beforeEach(inject(function($rootScope, _$controller_, _$httpBackend_, _LocalizationService_) {
    $scope = $rootScope.$new();
    $controller = _$controller_;
    $httpBackend = _$httpBackend_;
    localizationService = _LocalizationService_;

    localizationService.setAllCalendars(undefined);

    calendarsMock = [{
      '_id': '56fc5f721a76b5921e3df217',
      'calendar_id': 1,
      'name': 'NRF Calendar',
      '__v': 100,
      'organization_ids': [
        5798,
        6177,
        5947,
        5210,
        8695,
        5198,
        8882,
        1224,
        6240,
        6751,
        5349,
        8699,
        5178,
        6339
      ],
      'years': [
        {
          'year': 2001,
          'start_date': '2001-02-04T00:00:00.000Z',
          'start_month': 1,
          'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
        },
        {
          'year': 2002,
          'start_date': '2002-02-03T00:00:00.000Z',
          'start_month': 1,
          'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
        },
        {
          'year': 2003,
          'start_date':
            '2003-02-02T00:00:00.000Z',
          'start_month': 1,
          'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
        },
        {
          'year': 2004,
          'start_date': '2004-02-01T00:00:00.000Z',
          'start_month': 1,
          'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
        },
        {
          'year': 2005,
          'start_date': '2005-01-30T00:00:00.000Z',
          'start_month': 1,
          'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
        }, {
          'year': 2006,
          'start_date': '2006-01-29T00:00:00.000Z',
          'start_month': 1,
          'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 5]
        },
        {
          'year': 2007,
          'start_date': '2007-02-04T00:00:00.000Z',
          'start_month': 1,
          'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
        },
        {
          'year': 2008,
          'start_date': '2008-02-03T00:00:00.000Z',
          'start_month': 1,
          'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
        },
        {
          'year': 2009,
          'start_date': '2009-02-01T00:00:00.000Z',
          'start_month': 1,
          'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
        },
        {
          'year': 2010,
          'start_date': '2010-01-31T00:00:00.000Z',
          'start_month': 1,
          'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
        },
        {
          'year': 2011,
          'start_date': '2011-01-30T00:00:00.000Z',
          'start_month': 1,
          'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
        },
        {
          'year': 2012,
          'start_date': '2012-01-29T00:00:00.000Z',
          'start_month': 1,
          'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 5]
        },
        {
          'year': 2013,
          'start_date': '2013-02-03T00:00:00.000Z',
          'start_month': 1,
          'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
        },
        {
          'year': 2014,
          'start_date': '2014-02-02T00:00:00.000Z',
          'start_month': 1,
          'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
        },
        {
          'year': 2015,
          'start_date': '2015-02-01T00:00:00.000Z',
          'start_month': 1,
          'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
        },
        {
          'year': 2016,
          'start_date': '2016-01-31T00:00:00.000Z',
          'start_month': 1,
          'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
        }
      ],
      'global': true
    },
      {
        '_id': '56fe81f9be710b6025f897d5',
        'calendar_id': 2826,
        'name': 'Lucky Brand Calendar',
        '__v': 3,
        'organization_ids': [8925],
        'years': [
          {
            'year': 2012,
            'start_date': '2012-01-01T00:00:00.000Z',
            'start_month': 0,
            'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
          },
          {
            'year': 2013,
            'start_date': '2013-01-06T00:00:00.000Z',
            'start_month': 0,
            'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
          },
          {
            'year': 2014,
            'start_date': '2014-01-05T00:00:00.000Z',
            'start_month': 0,
            'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
          },
          {
            'year': 2015,
            'start_date': '2015-01-04T00:00:00.000Z',
            'start_month': 0,
            'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
          },
          {
            'year': 2016,
            'start_date': '2016-01-03T00:00:00.000Z',
            'start_month': 0,
            'month_mask': [4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4]
          }
        ],
        'global': false
      },
      {
        '_id': '570d418480dee428210d4e8e',
        'calendar_id': 2146,
        'name': 'Bare Escentuals NEW',
        '__v': 0,
        'organization_ids': [],
        'years': [
          {
            'year': 2011,
            'start_date': '2011-01-03T00:00:00.000Z',
            'start_month': 0,
            'month_mask': [4, 4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5]
          },
          {
            'year': 2012,
            'start_date': '2012-01-02T00:00:00.000Z',
            'start_month': 0,
            'month_mask': [4, 4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5]
          },
          {
            'year': 2013,
            'start_date': '2012-12-31T00:00:00.000Z',
            'start_month': 0,
            'month_mask': [4, 4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5]
          },
          {
            'year': 2014,
            'start_date': '2013-12-30T00:00:00.000Z',
            'start_month': 0,
            'month_mask': [4, 4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5]
          },
          {
            'year': 2015,
            'start_date': '2014-12-29T00:00:00.000Z',
            'start_month': 0,
            'month_mask': [4, 4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 6]
          },
          {
            'year': 2016,
            'start_date': '2016-01-04T00:00:00.000Z',
            'start_month': 0,
            'month_mask': [4, 4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5]
          }
        ],
        'global': false
      },
      {
        '_id': '570d41a680dee428210d4fae',
        'calendar_id': 3226,
        'name': 'Mall LFL 2015',
        '__v': 0,
        'organization_ids': [],
        'years': [
          {
            'year': 2015,
            'start_date': '2015-01-05T00:00:00.000Z',
            'start_month': 0,
            'month_mask': [4, 4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5]
          },
          {
            'year': 2016,
            'start_date': '2016-01-04T00:00:00.000Z',
            'start_month': 0,
            'month_mask': [4, 4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5]
          }
        ],
        'global': false
      }];

    zoneResourceMock = function() {
      function query() {
        return true;
      }
      return {
        query: query
      };
    };

    locationResourceMock = {
      'query': function() {
        return true;
      }
    };

    currentUserMock = {localization: {date_format: 'MM/DD/YYYY'}, preferences: {calendar_id: 1}};
  }));

  it('should place metrics from config to vm.metrics model', function() {
    viewController = createController(organizationMock);
    expect(viewController.metrics.length).toBe(1);
  });

  it('should load scheduled exports for user', function() {
    $httpBackend.expectGET(apiUrl+'/organizations/'+organizationMock.organization_id+'/scheduled-reports?exportType=csv').respond([{result: { }}]);
    $httpBackend.expectGET(apiUrl+'/calendars').respond([{result: calendarsMock }]);
    viewController = createController(organizationMock);
    $httpBackend.flush();
  });

  describe('doExport', function() {
    it('should create correct API request', function() {
      var now = moment();
      $httpBackend.expectGET(apiUrl+'/organizations/'+organizationMock.organization_id+'/scheduled-reports?exportType=csv').respond([{result: { }}]);
      $httpBackend.expectGET(apiUrl+'/calendars').respond([{result: calendarsMock }]);
      $httpBackend.expectGET(/.*\/kpis\/report\?.*/).respond([{}]);
      viewController = createController(organizationMock);
      viewController.siteLevel = true;
      viewController.dateRange = {
        start: now,
        end: now
      };
      viewController.doExport();
      $httpBackend.flush();
    });
    it('should have param to return site names', function() {
      var now = moment();
      $httpBackend.expectGET(apiUrl+'/organizations/'+organizationMock.organization_id+'/scheduled-reports?exportType=csv').respond([{result: { }}]);
      $httpBackend.expectGET(apiUrl+'/calendars').respond([{result: calendarsMock }]);
      $httpBackend.expectGET(/.*\/kpis\/report\?.*includeZoneNames=true.*/).respond([{}]);
      viewController = createController(organizationMock);
      viewController.siteLevel = true;
      viewController.dateRange = {
        start: now,
        end: now
      };
      viewController.doExport();
      $httpBackend.flush();
    });
  });

  function createController(orgMock) {
    return $controller('CSVExportCtrl', {
      '$scope': $scope,
      '$stateParams': {
        'orgId': orgMock.organization_id
      },
      'currentOrganization': orgMock,
      'currentSite' : {
        site_id: 1234,
        subscriptions: {
          labor: true,
          sales: true,
          interior: false,
          perimeter: true
        }
      },
      'currentUser': currentUserMock,
      'ZoneResource' : zoneResourceMock,
      'LocationResource' : locationResourceMock,
      'localStorageService': {
        'get': function() { return true; }
      }
    });
  }
});
