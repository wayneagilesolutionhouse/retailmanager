(function() {
  'use strict';

  angular.module('shopperTrak.siteSelector', [
    'mgcrea.ngStrap',
    'shopperTrak.constants',
    'shopperTrak.resources'
  ]);
})();
