(function() {
  'use strict';

  angular.module('shopperTrak')
    .directive('miList', miList);

  function miList() {
    return {
      restrict: 'E',
      templateUrl: 'components/mi-list/mi-list.partial.html',
      scope: {
        selectitem : '=',
        subscriptions:'=',
        rules:'=?',
        onMarketClick: '&',
        segments :'=',
        userSegments:'=',
        orgId: '=',
        selectedSegment: '=',
        onInitialized: '&?',
        dashboardMi:'=',
        geography: '=',
        countryName:'=',
        shouldResetOnCancel: '=?'
      },
      bindToController: true,
      controller: miListController,
      controllerAs: 'vm'
    };
  }

  miListController.$inject = ['$translate','$q', '$scope', 'ObjectUtils'];

  function miListController($translate, $q, $scope, ObjectUtils) {
    var vm = this;

    var translation = {};

    activate();

    function initScope() {
      vm.subscriptionChange = subscriptionChange;
      vm.addFilter = addFilter;
      vm.applyFilter = applyFilter;
      vm.cancelFilter = cancelFilter;
      vm.clearFilter = clearFilter;
      vm.showFilters = showFilters;
    }

    function activate() {
      initScope();

      if(angular.isUndefined(vm.shouldResetOnCancel)) {
        vm.shouldResetOnCancel = true;
      }

      getMItranslations().then(function (translations) {
        configureFilters(translations);
        configureWatches();
      });
    }

    function configureFilters(translations) {
      if (!ObjectUtils.isNullOrUndefinedOrEmptyObject(vm.userSegments)) {
        buildFiltersArray();

        vm.showSelected = [
          {
            name: vm.userSegments.subscription.geography.value.name.toUpperCase(),
            src: vm.userSegments.subscription.geography.value.src
          },
          {
            name: vm.userSegments.subscription.category.value.name.toUpperCase(),
            src: vm.userSegments.subscription.category.value.src
          }
        ];

        vm.selectedSegment = angular.copy(vm.userSegments);
      } else {
        vm.filtersArray = [
          {
            title: {
              name: translations.dimensions,
              values: [{name: translations.values}]
            },
            rule: {
              name: translations.rules
            },
            value: {
              name: translations.values
            }
          }
        ];
        vm.showSelected = [{
          name: translations.selectFilters
        }];
      }

      if(angular.isFunction(vm.onInitialized)) {
        vm.onInitialized()
      }
    }

    function loadListValues(arr, value) {
      var item = _.findWhere(arr, {name: value});

      if(ObjectUtils.isNullOrUndefined(item)) {
        return;
      }

      return _.sortBy(item.values, 'name');
    }

    function getMItranslations() {
      var deferred = $q.defer();

      var miTransKeys = [
        'marketIntelligence.DIMENSIONS',
        'marketIntelligence.RULES',
        'marketIntelligence.CONTAINS',
        'marketIntelligence.VALUES',
        'marketIntelligence.SELECTFILTERS'
      ];

      $translate(miTransKeys).then(function (translations) {
        translation.dimensions = translations['marketIntelligence.DIMENSIONS'];
        translation.rules = translations['marketIntelligence.CONTAINS'];
        translation.contains = translations['marketIntelligence.CONTAINS'];
        translation.values = translations['marketIntelligence.VALUES'];
        translation.selectFilters = translations['marketIntelligence.SELECTFILTERS'];
        deferred.resolve(translation);
      });

      return deferred.promise;
    }

    function subscriptionChange (filterItem) {
      filterItem.value = {name: translation.values}
    }

    function addFilter() {
      if (vm.filtersArray.length < 2) {
        vm.filtersArray.push({
          title: {name: translation.dimensions, values: [{name: translation.values}]},
          rule: {name: translation.rules},
          value: {name: translation.values}
        });
      }
    }

    function findGeography(geographies, geographyUuid) {
      return _.find(geographies, function (g) {
        return g.uuid === geographyUuid;
      });
    }
    
    function findCountry(geo,sub) {
      if (!ObjectUtils.isNullOrUndefinedOrEmpty(geo)) {
        if (sub.geography.name === 'Country') {
          vm.countryName = sub.geography.value.name;
          return;
        }
        var geoObject = findGeography(geo, sub.geography.value.src.parentUuid);
        if (geoObject.geoType !== 'COUNTRY') {
          var geoObjectTwo = findGeography(geo, geoObject.parentUuid);
          vm.countryName = geoObjectTwo.name;
          return;
        }
        vm.countryName = geoObject.name;
      }
    }
    function applyFilter() {
      vm.showSelected = [];

      var subscription = {};

      _.each(vm.filtersArray, function (item) {
        if(item.value.name !== 'Values'){
          vm.showSelected.push(item.value);
        }

        if (item.title.name !== 'Dimensions' || item.value.name !== 'Values') {
          vm.mandatorySelectError = false;
          vm.showFilterIsOpen = false;
          if (item.title.name === 'Category') {
            subscription.category = {
              orgId: vm.orgId,
              name: item.title.name,
              rule: item.rule.name,
              value: item.value
            }
          }
          else if (item.title.name !== 'Category') {
            subscription.geography = {
              orgId: vm.orgId,
              name: item.title.name,
              rule: item.rule.name,
              value: item.value
            }
          }
        }
      });

      if(vm.showSelected.length === 0) {
        vm.showSelected = [{
          name: translation.selectFilters
        }];
      }


      showSelectionErrors(subscription);
    }

    function showSelectionErrors(subscription) {
      if(_.isEmpty(subscription)) {
        vm.showFilterIsOpen = false;
        vm.selectedSegment = {};
        return;
      }

      if ( _.has(subscription, 'category') && _.has(subscription, 'geography')) {
        if (subscription.category.value.name !== 'Values' && subscription.geography.value.name !== 'Values') {
          vm.selectedSegment = {subscription: subscription};
          findCountry(vm.geography,subscription);
        }
        else {
          vm.mandatorySelectError = false;
          vm.showFilterIsOpen = true;

          vm.mandatoryValueSelectError = true;
        }
      }
      else if(!_.has(subscription, 'category')){

        vm.showFilterIsOpen = true;
        vm.mandatorySelectError = true;
      }
      else {
        vm.mandatorySelectError = true;
        vm.showFilterIsOpen = true;
      }
    }

    function cancelFilter() {

      vm.showFilterIsOpen = false;

      if(!ObjectUtils.isNullOrUndefinedOrEmptyObject(vm.userSegments) && vm.shouldResetOnCancel) {
        buildFiltersArray();
      }
    }

    function buildFiltersArray() {
      vm.filtersArray = [
        {
          title: {name: vm.userSegments.subscription.category.name, values: loadListValues(vm.subscriptions,vm.userSegments.subscription.category.name)},
          rule: {name: vm.userSegments.subscription.category.rule},
          value: {name: vm.userSegments.subscription.category.value.name, src: vm.userSegments.subscription.category.value.src}
        },
        {
          title: {name: vm.userSegments.subscription.geography.name, values: loadListValues(vm.subscriptions,vm.userSegments.subscription.geography.name)},
          rule: {name: vm.userSegments.subscription.geography.rule},
          value: {name: vm.userSegments.subscription.geography.value.name, src: vm.userSegments.subscription.geography.value.src}
        }
      ];
    }

    function clearFilter() {
      vm.filtersArray = [
        {
          title: {name: translation.dimensions, values: [{name: translation.values}]},
          rule: {name: translation.rules},
          value: {name: translation.values}
        }
      ];

      vm.mandatorySelectError = false;
      vm.mandatoryCategorySelectError = false;
      vm.mandatoryValueSelectError = false;
    }

    function showFilters() {
      vm.showFilterIsOpen = !vm.showFilterIsOpen;
      vm.mandatorySelectError = false;
      vm.mandatoryCategorySelectError = false;
      vm.mandatoryValueSelectError = false;
    }

    function configureWatches() {
      var unbindResetToDefaultListener = $scope.$on('resetToDefault', function(event) {
        clearFilter();
        applyFilter();
        event.preventDefault();
      });

      $scope.$on('$destroy', function() {
        if(angular.isFunction(unbindResetToDefaultListener)) {
          unbindResetToDefaultListener();
        }
      })
    }
  }
})();
