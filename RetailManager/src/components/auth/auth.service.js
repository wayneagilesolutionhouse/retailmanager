(function () {
  'use strict';

  angular.module('shopperTrak.auth')
    .factory('authService', authService);

  authService.$inject = [
    '$rootScope', 
    '$http', 
    '$q', 
    '$translate', 
    'session', 
    'apiUrl', 
    'obfuscation', 
    'LocalizationService', 
    'requestManager',
    'googleAnalytics'
  ];

  function authService(
    $rootScope, 
    $http, 
    $q, 
    $translate, 
    session, 
    apiUrl, 
    obfuscation, 
    LocalizationService, 
    requestManager,
    googleAnalytics
  ) {

    var cachedCurrentUser = null;

    function loginWithToken(authToken) {
      session.setToken(authToken);
      $rootScope.$broadcast('auth-login-success');
    }

    function login(username, password) {
      var deferred = $q.defer();

      if (username.match(/^demo/gi)) {
        obfuscation.enableForSession();
      } else {
        obfuscation.disableForSession();
      }
      var loginRequest = $http.post(apiUrl + '/auth', {
        username: username,
        password: password
      });

      loginRequest.then(function (response) {
        var authToken = response.data.result[0].token;
        session.setToken(authToken);
        cachedCurrentUser = response.data.result[0].user;
        LocalizationService.setUser(cachedCurrentUser);
        session.setUserId(cachedCurrentUser._id);
        requestManager.clearOtherUserCachedData();
        googleAnalytics.resetUserIdSentStatus();
        $rootScope.$broadcast('auth-login-success');
        deferred.resolve();
      }, function (response) {
        session.clear();
        requestManager.clearCache();
        deferred.reject(response);
      });

      return deferred.promise;
    }

    function updateUserPreferences(preferences) {
      if (cachedCurrentUser) {
        preferences.forEach(function (preference, index) {
          cachedCurrentUser['custom_period_' + (index + 1)] = preferences[index];
        });
      }
    }

    function updateUserPreferencesCustomCharts(charts) {
      if (cachedCurrentUser) {
        cachedCurrentUser.preferences.custom_charts = charts;
      }
    }

    function updateUserPreferencesCustomDashboards(dashboards) {
      if (cachedCurrentUser) {
        cachedCurrentUser.preferences.custom_dashboards = dashboards;
      }
    }

    function updateMarketIntelligence(segments){
      if (cachedCurrentUser) {
        cachedCurrentUser.preferences.market_intelligence = segments;
      }
    }
    function logout() {
      session.clear();
      cachedCurrentUser = null;
      $rootScope.$broadcast('auth-logout-success');
    }

    function isAuthenticated() {
      return session.getToken() !== null;
    }

    function getCurrentUser(disableCache, disableBatching) {
      var deferred = $q.defer();

      if (disableCache !== true && cachedCurrentUser) {
        deferred.resolve(cachedCurrentUser);
      } else {
        var endpoint = '/auth/currentUser';

        if (disableBatching === true) {
          endpoint += '?noBatch=true';
        }

        $http.get(apiUrl + endpoint).then(
          function onSuccess(result) {
            cachedCurrentUser = result.data.result[0].user;
            LocalizationService.setUser(result.data.result[0].user);
            deferred.resolve(cachedCurrentUser);
          },
          function onError() {
            deferred.reject();
          });
      }

      return deferred.promise;
    }

    function changePassword(newPassword) {
      var deferred = $q.defer();

      $http.put(apiUrl + '/auth/currentUser', {
        password: newPassword
      })
        .then(function (result) {
          session.setToken(result.data.result[0].token);
          cachedCurrentUser = null;
          deferred.resolve();
        }).catch(function () {
          deferred.reject();
        });

      return deferred.promise;
    }

    return {
      login: login,
      logout: logout,
      isAuthenticated: isAuthenticated,
      loginWithToken: loginWithToken,
      changePassword: changePassword,
      getCurrentUser: getCurrentUser,
      updateUserPreferences: updateUserPreferences,
      updateUserPreferencesCustomCharts: updateUserPreferencesCustomCharts,
      updateUserPreferencesCustomDashboards: updateUserPreferencesCustomDashboards,
      updateMarketIntelligence: updateMarketIntelligence
    };
  }
})();
