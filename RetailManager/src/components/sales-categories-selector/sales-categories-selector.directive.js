(function () {
  'use strict';

  angular.module('shopperTrak')
    .directive('salesCategoriesSelector', salesCategoriesSelector);


  function salesCategoriesSelector() {
    return {
      restrict: 'E',
      templateUrl: 'components/sales-categories-selector/sales-categories-selector.partial.html',
      scope: {
        selectedCategories: '=',
        categories: '=?',
        multipleSelect: '=?'
      },
      bindToController: true,
      controller: salesCategoriesSelectorController,
      controllerAs: 'vm'
    };
  }

  salesCategoriesSelectorController.$inject = [
    '$scope',
    'ObjectUtils'
  ];

  function salesCategoriesSelectorController($scope, ObjectUtils) {
    var vm = this;
    var defaultOptionId = 0;

    activate();

    function activate() {

      if(ObjectUtils.isNullOrUndefined(vm.multipleSelect)) {
        vm.multipleSelect = true;
      }

      if(ObjectUtils.isNullOrUndefinedOrEmpty(vm.categories) || vm.categories.length === 1) {
        return;
      }

      if(vm.multipleSelect !== true) {
        vm.maxLength = 1;
      } else {
        vm.maxLength = vm.categories.length;
      }

      if(ObjectUtils.isNullOrUndefined(vm.selectedCategories) || vm.selectedCategories.length === 0) {
        vm.selectedCategories = _.where(vm.categories, {id: defaultOptionId});
      }

    }
  }
})();
