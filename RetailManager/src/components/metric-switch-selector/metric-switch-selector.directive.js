(function () {
  'use strict';

  angular.module('shopperTrak')
    .directive('metricSwitchSelector', metricSwitchSelector);

  function metricSwitchSelector() {
    return {
      restrict: 'E',
      templateUrl: 'components/metric-switch-selector/metric-switch-selector.partial.html',
      scope: {
        selectedMetrics: '=?',
        activeGroup: '=?',
        maxLength: '=?',
        minLength: '=?',
        currentOrganization: '=',
        currentSite: '=?',
        metricMessage: '=?'
      },
      bindToController: true,
      controller: metricSwitchSelectorController,
      controllerAs: 'vm'
    };
  }

  metricSwitchSelectorController.$inject = [
    '$scope',
    '$translate',
    'LocalizationService',
    'SubscriptionsService',
    'ObjectUtils',
    'metricConstants'
  ];

  function metricSwitchSelectorController(
    $scope,
    $translate,
    LocalizationService,
    SubscriptionsService,
    ObjectUtils,
    metricConstants
  ) {


    var vm = this;
    vm.selectMetric = selectMetric;
    vm.setActiveGroup = setActiveGroup;
    vm.groupFilter = groupFilter;
    vm.metricIsDisabled = metricIsDisabled;

    activate();

    function activate() {
      getActiveSubscriptions();
      setGroupAndMetrics();
      initialise();
    }

    function filterSubscriptionsForGroup(obj) {
      return _.filter(obj, function (subClass) {
        return (subClass.subscription === 'any' ||
          _.contains(vm.activeSubscriptions, subClass.subscription));
      });
    }

    function filterSubscriptionsForMetrics(obj) {
      return _.filter(obj, function (subClass) {
        return ((subClass.subscription === 'any' ||
          _.contains(vm.activeSubscriptions, subClass.subscription))) &&
          hasRequiredPermissions (subClass.requiredPermissions);
      });
    }

    function hasRequiredPermissions(requiredPermissions) {
      if (ObjectUtils.isNullOrUndefinedOrEmpty(requiredPermissions)) {
        return true;
      }

      var hasRequiredPermission = true;
       _.each(requiredPermissions, function(permission) {
        if(!_.contains(vm.activeSubscriptions, permission)) {
          hasRequiredPermission = false;
          return false;
        }
      });
      return hasRequiredPermission;
    }

    function metricIsDisabled(metric) {
      if(ObjectUtils.isNullOrUndefined(vm.selectedMetrics) ||
        ObjectUtils.isNullOrUndefined(vm.maxLength)){
        return false;
      }

      var selected = _.keys(
        _.pick(vm.selectedMetrics, function(value) {
          return value === true;})
        );

      return selected.length >= vm.maxLength && vm.selectedMetrics[metric] !== true;
   }

    function getActiveSubscriptions(){
      if (!ObjectUtils.isNullOrUndefined(vm.currentSite) &&
        !ObjectUtils.isNullOrUndefined(vm.currentSite.subscriptions)){
          vm.activeSubscriptions = SubscriptionsService.getSubscriptions(vm.currentSite);
      }else {
        vm.activeSubscriptions = SubscriptionsService.getSubscriptions(vm.currentOrganization);
      }
    }

    function setGroupAndMetrics() {
      vm.groups = filterSubscriptionsForGroup(metricConstants.groups);
      vm.metrics = _.sortBy(filterSubscriptionsForMetrics(metricConstants.metrics), 'order');
    }

    function groupFilter(metric) {
      return (metric.group === 'any' || metric.group === vm.activeGroup);
    }

    function initialise(){
      if (ObjectUtils.isNullOrUndefined(vm.activeGroup)){
        //if active group isnt passed to directive
        vm.activeGroup = vm.groups[0].name;
      }
    }

    function selectMetric(metric) {
      if( ObjectUtils.isNullOrUndefined(vm.selectedMetrics)){
        vm.selectedMetrics = {};
        vm.selectedMetrics[metric] = true;
        return;
      }

      if (ObjectUtils.isNullOrUndefined(vm.maxLength)){
        //default to unlimited selection
        vm.selectedMetrics[metric] = !vm.selectedMetrics[metric];
      } else {

        var selected = _.keys(
          _.pick(vm.selectedMetrics, function(value) {
            return value === true;})
          );

        if(selected.length < vm.maxLength || vm.selectedMetrics[metric] === true){
            vm.selectedMetrics[metric] = !vm.selectedMetrics[metric];
        }
      }
    }

    function setActiveGroup(group) {
      vm.selectedMetrics = {};
      vm.activeGroup = group;
      if(group === 'interior') {
        _.each(vm.metrics, function(metric) {
          if(metric.kpi === 'traffic'){
            metric.shortTranslationLabel = 'kpis.kpiTitle.visitor_behaviour_traffic';
            metric.translationLabel = 'kpis.kpiTitle.visitor_behaviour_traffic';
          }
        });
      }
      if(group === 'perimeter') {
        _.each(vm.metrics, function(metric) {
          if(metric.kpi === 'traffic'){
            metric.shortTranslationLabel = 'kpis.kpiTitle.traffic';
            metric.translationLabel = 'kpis.kpiTitle.traffic';
          }
        });
      }
    }
  }
})();
