'use strict';

describe('metricSwitchSelector', function () {

  var $compile, $scope, LocalizationService, SubscriptionsService, ObjectUtils;
  var $httpBackend;

  beforeEach(module('shopperTrak'));

  beforeEach(inject(putTemplateToTemplateCache));

  beforeEach(inject(function ($rootScope,
    _$compile_,
    _ObjectUtils_,
    _LocalizationService_,
    _SubscriptionsService_,
    _$httpBackend_) {
    $compile = _$compile_;
    $scope = $rootScope.$new();
    ObjectUtils = _ObjectUtils_;
    LocalizationService = _LocalizationService_;
    SubscriptionsService = _SubscriptionsService_;
    $httpBackend = _$httpBackend_;
    $httpBackend.whenGET('l10n/languages/en_US.json').respond({});
  }));

  describe('getActiveSubscriptions', function() {
    it('should set activeSubscriptions to items marked as true in subscriptions', function() {

      var scope = $scope;
      scope.activeGroup = 'perimeter';
      scope.selectedMetric = {};
      scope.currentOrg = {
        subscriptions: {
          advanced: false,
          campaigns: false,
          consumer_behavior: false,
          interior: false,
          labor: false,
          large_format_mall: true,
          market_intelligence: false,
          perimeter: true,
          qlik: false,
          sales: true,
        }
      };

      var metricSwitch = renderDirectiveAndDigest(scope);

      var actualResult = JSON.stringify(metricSwitch.activeSubscriptions);
      var expectedResult = JSON.stringify(['large_format_mall', 'perimeter', 'sales']);

      expect(actualResult).toBe(expectedResult);
    });

    it('should set the active group to the firstgroup', function() {
      var metricSwitch = renderDirectiveAndDigest();

      expect(metricSwitch.activeGroup).toBe('perimeter');
    });

  });


  function renderDirectiveAndDigest(scope) {
    // if(typeof scope !== 'undefined') {
    //   $scope = scope;
    // }

    var element = createDirectiveElement();
    $compile(element)($scope);
    $scope.$digest();
    var vm = element.isolateScope().vm;
    return vm;
  }

  function createDirectiveElement() {
    return angular.element(
      '<metric-switch-selector ' +
      'current-organization="currentOrg" ' +
      'current-site="currentSite" ' +
      '</metric-switch-selector>'
    );
  }

  function putTemplateToTemplateCache($templateCache) {
    // Put an empty template to the template cache to prevent Angular from
    // trying to fetch it. We are only testing the controller here, so the
    // actual template is not needed.
    $templateCache.put(
      'components/metric-switch-selector/metric-switch-selector.partial.html',
      '<div></div>'
    );
  }

});
