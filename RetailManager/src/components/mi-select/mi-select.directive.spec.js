'use strict';

describe('miSelect', function () {  

  var $compile, $scope

  beforeEach(module('shopperTrak'));

  beforeEach(inject(putTemplateToTemplateCache));

  beforeEach(inject(function ($rootScope,
    _$compile_) {
    $compile = _$compile_;
    $scope = $rootScope.$new();
  }));

  describe('activate', function() {
    it('should default the dropdown to closed', function() {
      var scope = renderDirectiveAndDigest();
      expect(scope.dropdownIsOpen).toBe(false);
    });

    it('should set the select setting to empty', function() {
      var scope = renderDirectiveAndDigest();
      expect(JSON.stringify(scope.selectSetting)).toBe(JSON.stringify({}));
    });

    it('should set switch on scroll to false', function() {
      var scope = renderDirectiveAndDigest();
      expect(scope.switchOnScroll).toBe(false);
    });
  });

  describe('onClick', function() {
    it('should toggle the drop down open status', function() {
      var scope = renderDirectiveAndDigest();
      scope.onClick();
      expect(scope.dropdownIsOpen).toBe(true);
      scope.onClick();
      expect(scope.dropdownIsOpen).toBe(false);
    });
  });

  describe('offClick', function() {
    it('should close the dropdown', function() {
      var scope = renderDirectiveAndDigest();
      
      scope.dropdownIsOpen = true;

      scope.offClick();
      
      expect(scope.dropdownIsOpen).toBe(false);
    })
  })

  describe('onItemSelect', function() {
    it('should set the current item', function() {
      var scope = renderDirectiveAndDigest();
      
      var item = {
        name: 'someitem'
      };

      scope.onItemSelection = undefined;

      scope.onItemSelect(item)

      expect(scope.currentItem).toBe(item);
    });

    it('should call onItemSelection if it has been set', function() {
      var scope = renderDirectiveAndDigest();

      scope.onItemSelection = angular.noop;

      spyOn(scope, 'onItemSelection');

      var item = {
        name: 'someitem'
      };

      scope.onItemSelect(item);

      expect(scope.onItemSelection).toHaveBeenCalled();
    });
  });


  function renderDirectiveAndDigest() {
    var element = createDirectiveElement();
    $compile(element)($scope);
    $scope.$digest();
    return element.isolateScope();
  }

  function createDirectiveElement() {
    return angular.element(
      '<mi-select' +
      ' control-id="\'some-id\'"' +
      ' items="items"' +
      ' current-item="currentItem"' +
      ' show-parent-link="showParentLink"' +
      ' display-property="\'name\'"' +
      ' on-item-selection="onItemSelection"' +
      ' parent-drop-down-class="\'parentDropDownClass\'"' +
      ' selected-days="selectedDays"' +
      '></mi-select>'
    );
  }

  function putTemplateToTemplateCache($templateCache) {
    // Put an empty template to the template cache to prevent Angular from
    // trying to fetch it. We are only testing the controller here, so the
    // actual template is not needed.
    $templateCache.put(
      'components/mi-select/mi-select.partial.html',
      '<div></div>'
    );
  }

});