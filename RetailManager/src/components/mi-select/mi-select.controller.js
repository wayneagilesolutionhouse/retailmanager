(function () {
  'use strict';
  angular.module('shopperTrak')
    .controller('miSelectCtrl', ['$scope', function ($scope) {

      activate();

      $scope.onClick = function () {
        $scope.dropdownIsOpen = !$scope.dropdownIsOpen;
      };

      $scope.offClick = function () {
        $scope.dropdownIsOpen = false;
      };

      $scope.onItemSelect = function (item) {
        $scope.currentItem = item;
        if (typeof $scope.onItemSelection === 'function') {
          $scope.onItemSelection();
        }
      };

      function activate() {
        $scope.dropdownIsOpen = false;
        $scope.selectSetting = {};
        $scope.switchOnScroll = false;
      }
    }]);
})();
