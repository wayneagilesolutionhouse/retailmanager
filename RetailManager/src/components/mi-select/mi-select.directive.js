(function () {
  'use strict';

  angular.module('shopperTrak').directive('miSelect', function () {
    return {
      restrict: 'E',
      templateUrl: 'components/mi-select/mi-select.partial.html',
      controller: 'miSelectCtrl',
      scope: {
        controlId: '@',
        items: '=',
        currentItem: '=',
        showParentLink: '=',
        displayProperty: '@',
        onItemSelection: '&',
        parentDropDownClass: '@'
      }
    };
  });
})();
