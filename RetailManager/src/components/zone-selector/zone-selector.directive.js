(function() {
  'use strict';

  angular.module('shopperTrak.zoneSelector')
    .directive('zoneSelector', zoneSelectorDirective);

  function zoneSelectorDirective() {
    return {
      restrict: 'E',
      replace: true,
      transclude: true,
      templateUrl: 'components/zone-selector/zone-selector.partial.html',
      scope: {
        zones: '=',
        onZoneClick: '&',
        getZoneHref: '&zoneHref',
        zoneIsSelected: '&',
        collapsedZones: '=',
        selectedZonesOnClick: '&',
        showSelectAllButton: '=',
        language: '='
      },
      controller: ZoneSelectorController,
      controllerAs: 'vm',
      bindToController: true,
    };
  }

  ZoneSelectorController.$inject = [
    '$scope',
    '$element',
    'ZoneResource'
  ];

  function ZoneSelectorController() {
    var vm = this;

    vm.filter = '';

    vm.selectZone = selectZone;
    vm.selectAll = selectAll;

    function selectZone(zoneId) {
      vm.onZoneClick({
        zoneId: zoneId
      });
    }

    function selectAll(allZonesList){
      vm.selectedZonesOnClick({
        allZonesList : allZonesList
      });
    }
  }
})();
