(function() {
  'use strict';

  angular.module('shopperTrak.zoneSelector', [
    'mgcrea.ngStrap',
    'shopperTrak.constants',
    'shopperTrak.resources'
  ]);
})();
