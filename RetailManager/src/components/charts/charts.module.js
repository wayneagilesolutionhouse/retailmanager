(function() {
  'use strict';

  angular.module('shopperTrak.charts', [
    'shopperTrak.utils',
    'highcharts-ng'
  ]);
})();
