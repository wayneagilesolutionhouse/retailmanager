(function() {
  'use strict';

  angular.module('shopperTrak.charts')
    .factory('chartUtils', chartUtils);

  chartUtils.$inject = ['Chartist'];

  function chartUtils(Chartist) {
    return {
      getElementSpaceCoordinatesFromScreenSpaceCoordinates: getElementSpaceCoordinatesFromScreenSpaceCoordinates,
      getClosestSvgElementOnXAxis: getClosestSvgElementOnXAxis,
      getSeriesItemClassName: getSeriesItemClassName,
      removeClassFromAllSvgCollectionElements: removeClassFromAllSvgCollectionElements,
      highlightClosestChartSeriesItems: highlightClosestChartSeriesItems,
      unhighlightAllChartSeriesItems: unhighlightAllChartSeriesItems,
      getClosestSeriesItemIndex: getClosestSeriesItemIndex,
      getTooltipCoordinates: getTooltipCoordinates,
      elementsIntersect: elementsIntersect
    };

    function getElementSpaceCoordinatesFromScreenSpaceCoordinates(screenSpaceCoordinates, element) {
      var elementPosition = $(element).offset();
      return {
        x: screenSpaceCoordinates.x - elementPosition.left + document.body.scrollLeft + document.documentElement.scrollLeft,
        y: screenSpaceCoordinates.y - elementPosition.top + document.body.scrollTop + document.documentElement.scrollTop
      };
    }

    function getClosestSvgElementOnXAxis(containerSpaceX, selectors, containerElement) {
      var elements = containerElement.querySelectorAll(selectors);
      if(elements.length > 0) {
        var closestElement = elements.item(0);
        var firstElementBoundingBox = closestElement.getBBox();
        var smallestAbsoluteDeltaX = Math.abs(firstElementBoundingBox.x + firstElementBoundingBox.width / 2 - containerSpaceX);
        for (var i = 1; i < elements.length; i++) {
          var element = elements.item(i);
          var elementBoundingBox = element.getBBox();
          var absoluteDeltaX = Math.abs(elementBoundingBox.x - containerSpaceX);
          if (absoluteDeltaX < smallestAbsoluteDeltaX) {
            smallestAbsoluteDeltaX = absoluteDeltaX;
            closestElement = element;
          }
        }
        return closestElement;
      }
    }

    function getSeriesItemClassName(chartistInstance) {
      if (chartistInstance instanceof Chartist.Bar) {
        return 'ct-bar';
      } else if (chartistInstance instanceof Chartist.Line) {
        return 'ct-point';
      }
    }

    function removeClassFromAllSvgCollectionElements(className, svgElementCollection) {
      for (var i = 0; i < svgElementCollection.length; i++) {
        var element = svgElementCollection.item(i);
        var svgInstance = new Chartist.Svg(element);
        svgInstance.removeClass(className);
      }
    }

    function highlightClosestChartSeriesItems(relativeMouseCoordinates, chartistInstance) {
      unhighlightAllChartSeriesItems(chartistInstance);

      var seriesIndex = getClosestSeriesItemIndex(relativeMouseCoordinates.x, chartistInstance);
      var seriesItemContainers = chartistInstance.container.querySelectorAll('.ct-series');
      var seriesItemSelector = '.' + getSeriesItemClassName(chartistInstance);

      for (var i = 0; i < seriesItemContainers.length; i++) {
        var seriesItemContainer = seriesItemContainers.item(i);
        var seriesItems = seriesItemContainer.querySelectorAll(seriesItemSelector);

        for (var j = 0; j < seriesItems.length; j++) {
          var seriesItem = seriesItems.item(j);
          if (seriesItem.getAttribute('data-series-index') === seriesIndex) {
            new Chartist.Svg(seriesItems.item(j)).addClass('is-hovered');
          }
        }
      }
    }

    function unhighlightAllChartSeriesItems(chartistInstance) {
      var seriesItemSelector = '.' + getSeriesItemClassName(chartistInstance);
      var items = chartistInstance.container.querySelectorAll(seriesItemSelector);
      removeClassFromAllSvgCollectionElements('is-hovered', items);
    }

    // 'x' is the x coordinate in chart space
    function getClosestSeriesItemIndex(x, chartistInstance) {
      var seriesItemSelector = '.' + getSeriesItemClassName(chartistInstance);
      var closestSeriesItem = getClosestSvgElementOnXAxis(
        x, seriesItemSelector, chartistInstance.container
      );
      return closestSeriesItem.getAttribute('data-series-index');
    }

    function getTooltipCoordinates(relativeMouseCoordinates, chartistInstance, $tooltipElement) {
      var seriesItemSelector = '.' + getSeriesItemClassName(chartistInstance);

      var coordinates = {};

      var $chart = $(chartistInstance.container);
      var seriesItemContainers = chartistInstance.container.querySelectorAll('.ct-series');
      if (seriesItemContainers.length === 0) {
        throw 'No rendered chart series present';
      }
      var seriesItemContainer = findFirstChartSeriesWithData(seriesItemContainers, seriesItemSelector);
      var closestFirstSeriesItem = getClosestSvgElementOnXAxis(relativeMouseCoordinates.x, seriesItemSelector, seriesItemContainer);
      var closestFirstSeriesItemBoundingBox = closestFirstSeriesItem.getBBox();

      coordinates.x = Math.min(
        closestFirstSeriesItemBoundingBox.x + closestFirstSeriesItemBoundingBox.width / 2 - $tooltipElement.outerWidth() / 2,
        $chart.outerWidth() - $tooltipElement.outerWidth()
      );
      coordinates.y = Math.min(relativeMouseCoordinates.y, $chart.outerHeight() - $tooltipElement.outerHeight());
      return coordinates;
    }

    function elementsIntersect(element1, element2) {
      var r1 = element1.getBoundingClientRect();
      var r2 = element2.getBoundingClientRect();
      return !(r2.left > r1.right ||
             r2.right < r1.left ||
             r2.top > r1.bottom ||
             r2.bottom < r1.top);
    }

    function findFirstChartSeriesWithData(seriesItemContainers, seriesItemSelector) {
      /* Tooltips fail if series container doesn't have data. This function checks that data really exists.
       * This is not optimal solution, but it works for now. We should come up with better solution. */
      var i = 0;
      while( seriesItemContainers.item(i).querySelectorAll(seriesItemSelector).length === 0 && i < 20) {
        i++;
      }
      if( seriesItemContainers.item(i).querySelectorAll(seriesItemSelector).length > 0) {
        return seriesItemContainers.item(i);
      } else {
        return null;
      }
    }
  }
})();
