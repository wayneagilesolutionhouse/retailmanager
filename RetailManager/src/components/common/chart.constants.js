(function() {
  'use strict';

  angular.module('shopperTrak.constants')
  .constant('chartColors', [// Medium brightness
  '#9bc614',
  '#0aaaff',
  '#be64f0',
  '#ff5028',
  '#feac00',
  '#00abae',
  '#929090',

  // Darkest
  '#328214',
  '#003291',
  '#690ff6',
  '#960000',
  '#af6400',
  '#005669',
  '#303030',

  // Darker
  '#50af14',
  '#0665b5',
  '#9b37f0',
  '#d23200',
  '#dc8200',
  '#007a8d',
  '#565454',

  // Lighter
  '#b4eb14',
  '#50d2ff',
  '#d296f5',
  '#ff8259',
  '#ffdc00',
  '#00dbba',
  '#c6c3c3'
    ]);
})();
