'use strict';

angular.module('shopperTrak.utils', [])
  .factory('ObjectUtils', function () {
    function isUndefined(item) {
      return typeof item === 'undefined';
    }
    function isNullOrUndefined(item) {
      if (typeof item === 'undefined') {
        return true;
      }
      if (item === null) {
        return true;
      }

      return false;
    }

    function isEmptyObject(obj) {
      for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          return false;
        }
      }

      return true && JSON.stringify(obj) === JSON.stringify({});
    }

    function isNullOrUndefinedOrEmptyObject(obj) {
      if(isNullOrUndefined(obj)) {
        return true;
      }

      if(isEmptyObject(obj)) {
        return true;
      }

      return false;
    }

    function isNullOrUndefinedOrEmpty(item) {
      if (isNullOrUndefined(item) || isNullOrUndefined(item.length) || item.length === 0) {
        return true;
      }
      if (item === '' || isNullOrUndefined(item.length)) {
        return true;
      }

      return false;
    }

    function isNullOrUndefinedOrBlank(item) {
      if (isNullOrUndefined(item)) {
        return true;
      }
      if (item === '') {
        return true;
      }

      return false;
    }

    // From https://github.com/remy/undefsafe/blob/master/lib/undefsafe.js
    function getNestedProperty(obj, path, value) {
      var parts = path.split('.');
      var key = null;
      var type = typeof obj;
      var parent = obj;

      var star = parts.filter(function (_) { return _ === '*' }).length > 0;

      // we're dealing with a primitive
      if (type !== 'object' && type !== 'function') {
        return obj;
      } else if (path.trim() === '') {
        return obj;
      }

      key = parts[0];
      var i = 0;
      for (; i < parts.length; i++) {
        key = parts[i];
        parent = obj;

        if (key === '*') {
          // loop through each property
          var prop = '';

          for (prop in parent) {
            var shallowObj = getNestedProperty(obj[prop], parts.slice(i + 1).join('.'), value);
            if (shallowObj) {
              if ((value && shallowObj === value) || (!value)) {
                return shallowObj;
              }
            }
          }
          return undefined;
        }

        obj = obj[key];
        if (obj === undefined || obj === null) {
          break;
        }
      }

      // if we have a null object, make sure it's the one the user was after,
      // if it's not (i.e. parts has a length) then give undefined back.
      if (obj === null && i !== parts.length - 1) {
        obj = undefined;
      } else if (!star && value) {
        key = path.split('.').pop();
        parent[key] = value;
      }
      return obj;
    }

    /** Renames a property on an object
     *
     *  @param {object} obj The object to change
     *  @param {string} sourcePropName The property to rename
     *  @param {string} destinationPropName What the property should be renames to
     *  @returns {object} The updated object
     **/
    function rename(obj, sourcePropName, destinationPropName) {
      if(!_.isUndefined(obj) && _.isUndefined(obj[destinationPropName]) && !_.isUndefined(obj[sourcePropName])) {
        obj[destinationPropName] = obj[sourcePropName];
        delete obj[sourcePropName];
      }
      return obj;
    }

    return {
      isUndefined: isUndefined,
      isNullOrUndefined: isNullOrUndefined,
      isNullOrUndefinedOrEmpty: isNullOrUndefinedOrEmpty,
      isNullOrUndefinedOrBlank: isNullOrUndefinedOrBlank,
      isEmptyObject: isEmptyObject,
      isNullOrUndefinedOrEmptyObject: isNullOrUndefinedOrEmptyObject,
      getNestedProperty: getNestedProperty,
      rename: rename
    };
  });
