'use strict';

describe('SubscriptionsService', function() {

  var $rootScope;
  var SubscriptionsService;

  beforeEach(module('shopperTrak'));
  beforeEach(inject(function(_$rootScope_, _SubscriptionsService_) {
    $rootScope = _$rootScope_;
    SubscriptionsService = _SubscriptionsService_;
  }));

  describe('getSubscriptions', function() {
    it('should return [large_format_mall, interior, perimeter] only site subscription for large_format_mall, interior, perimeter  are true', function() {

      var organizationMock = {
        subscriptions: {
          perimeter: true,
          interior: false
        }
      };

      var siteMock = {
        subscriptions: {
          market_intelligence: false,
          qlik: false,
          large_format_mall: true,
          interior: true,
          perimeter: true
        }
      };

      var _activeSubscriptions = JSON.stringify(SubscriptionsService.getSubscriptions(siteMock));
      var expectedResult = JSON.stringify(["large_format_mall","interior","perimeter"]);


      expect(_activeSubscriptions).toEqual(expectedResult);
    });

    it('should return [perimeter] value when only organization perimeter subscription is true ', function() {
      var organizationMock = {
        subscriptions: {
          perimeter: true,
          interior: false
        }
      };

      var _activeSubscriptions = SubscriptionsService.getSubscriptions(organizationMock);

      expect(_activeSubscriptions).toEqual(['perimeter']);
    });
  });

  describe('getSiteSubscriptions', function() {
    it('should return "interior" value when site has interior subscription', function() {
      var organizationMock = {
        subscriptions: {
          perimeter: true,
          interior: false
        }
      };
      var siteMock = {
        subscriptions: {
          market_intelligence: false,
          qlik: false,
          large_format_mall: true,
          interior: true,
          perimeter: true
        }
      };
      expect(SubscriptionsService.getSiteSubscriptions(organizationMock,siteMock)).toEqual('interior');
    });

    it('should return "perimeter" value when organization has perimeter subscription', function() {
      var organizationMock = {
        subscriptions: {
          perimeter: true
        }
      };
      var siteMock = {};
      expect(SubscriptionsService.getSiteSubscriptions(organizationMock,siteMock)).toEqual('perimeter');
    });
  });

  describe('siteHasPerimeter', function() {
    it('should return true when site has perimeter subscription', function() {
      var organizationMock = {
        subscriptions: {
          perimeter: true,
          interior: false
        }
      };
      var siteMock = {
        subscriptions: {
          market_intelligence: false,
          qlik: false,
          large_format_mall: true,
          interior: true,
          perimeter: true
        }
      };
      expect(SubscriptionsService.siteHasPerimeter(organizationMock,siteMock)).toBe(true);
    });
  });

  describe('siteHasInterior', function() {
    it('should return true when organization has interior subscription', function() {
      var organizationMock = {
        subscriptions: {
          perimeter: true,
          interior: true
        }
      };
      var siteMock = {};
      expect(SubscriptionsService.siteHasInterior(organizationMock,siteMock)).toBe(true);
    });
  });

  describe('siteHasSales', function() {
    it('should return true when site has sales subscription', function() {
      var organizationMock = {
        subscriptions: {
          perimeter: true,
          interior: false
        }
      };
      var siteMock = {
        subscriptions: {
          market_intelligence: false,
          qlik: false,
          large_format_mall: true,
          interior: true,
          perimeter: true,
          sales: true
        }
      };
      expect(SubscriptionsService.siteHasPerimeter(organizationMock,siteMock)).toBe(true);
    });
  });

  describe('siteHasSales', function() {
    it('should return true when organization has sales subscription', function() {
      var organizationMock = {
        subscriptions: {
          perimeter: true,
          interior: false,
          sales: true
        }
      };
      var siteMock = {
        subscriptions: {
          market_intelligence: false,
          qlik: false,
          large_format_mall: true,
          interior: true,
          perimeter: true
        }
      };
      expect(SubscriptionsService.siteHasPerimeter(organizationMock,siteMock)).toBe(true);
    });
  });

  describe('siteHasLabor', function() {
    it('should return true when organization has labor subscription', function() {
      var organizationMock = {
        subscriptions: {
          perimeter: true,
          interior: false,
          labor: true
        }
      };
      var siteMock = {
        subscriptions: {
          market_intelligence: false,
          qlik: false,
          large_format_mall: true,
          interior: true,
          perimeter: true
        }
      };
      expect(SubscriptionsService.siteHasPerimeter(organizationMock,siteMock)).toBe(true);
    });
  });

  describe('hasMarketIntelligence', function() {
    it('should return false when the organization object has no subscription properties', function() {
      var organizationMock = {
      };

      var hasMi = SubscriptionsService.hasMarketIntelligence(organizationMock);
      //to do when we remove mocking in subscription this needs to be false
      expect(hasMi).toBe(false);
    });

    it('should return false when the organization object has no market intelligence property', function() {
      var organizationMock = {
        subscriptions: {
        }
      };

      var hasMi = SubscriptionsService.hasMarketIntelligence(organizationMock);
      //to do when we remove mocking in subscription this needs to be false
      expect(hasMi).toBe(false);
    });

    it('should return false when the market_intelligence property is set to false', function() {
      var organizationMock = {
        subscriptions: {
          market_intelligence: false
        }
      };

      var hasMi = SubscriptionsService.hasMarketIntelligence(organizationMock);
      //to do when we remove mocking in subscription this needs to be false
      expect(hasMi).toBe(false);
    });

    it('should return true when the market_intelligence property is set to true', function() {
      var organizationMock = {
        subscriptions: {
          market_intelligence: true
        }
      };

      var hasMi = SubscriptionsService.hasMarketIntelligence(organizationMock);
      //to do when we remove mocking in subscription this needs to be false
      expect(hasMi).toBe(true);
    });
  });

  describe('hasRealTime', function() {
    it('should return false when the organization object has no realtime subscription properties', function() {
      var organizationMock = {
        subscriptions: {
        }
      };

      var hasRealTime = SubscriptionsService.hasRealTime(organizationMock);
      //to do when we remove mocking in subscription this needs to be false
      expect(hasRealTime).toBe(false);
    });

    it('should return false when the organization object has all realtime subscription properties set to false', function() {
      var organizationMock = {
        subscriptions: {
          realtime_labor: false,
          realtime_sales: false,
          realtime_traffic: false
        }
      };

      var hasRealTime = SubscriptionsService.hasRealTime(organizationMock);
      //to do when we remove mocking in subscription this needs to be false
      expect(hasRealTime).toBe(false);
    });

    it('should return true when the organization object has realtime_labor set to true', function() {
      var organizationMock = {
        subscriptions: {
          realtime_labor: true,
          realtime_sales: false,
          realtime_traffic: false
        }
      };

      var hasRealTime = SubscriptionsService.hasRealTime(organizationMock);

      expect(hasRealTime).toBe(true);
    });

    it('should return true when the organization object has realtime_sales set to true', function() {
      var organizationMock = {
        subscriptions: {
          realtime_labor: false,
          realtime_sales: true,
          realtime_traffic: false
        }
      };

      var hasRealTime = SubscriptionsService.hasRealTime(organizationMock);

      expect(hasRealTime).toBe(true);
    });

    it('should return true when the organization object has realtime_traffic set to true', function() {
      var organizationMock = {
        subscriptions: {
          realtime_labor: false,
          realtime_sales: false,
          realtime_traffic: true
        }
      };

      var hasRealTime = SubscriptionsService.hasRealTime(organizationMock);

      expect(hasRealTime).toBe(true);
    });
  });

});
