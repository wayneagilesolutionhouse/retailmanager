(function() {
  'use strict';

  angular.module('shopperTrak')
  .factory('features', [
      'ObjectUtils',
      'featuresConfig',
      '$window',

    function (
      ObjectUtils,
      featuresConfig,
      $window
    ) {

      // Add feature CSS classes to <html>:
      _.each(Object.keys(featuresConfig), function featureNames_each(name){
        $window.document.documentElement.classList.add((isEnabled(name) ? '' : 'no-') + 'feature-' + name);
      });

      function isEnabled(featureName) {
        if(allFeaturesOn()) {
          return true;
        }

        if(ObjectUtils.isNullOrUndefinedOrEmpty(featureName)) {
          console.error('Feature name is not valid: ' + featureName);
          return false;
        }
        var featureValue = featuresConfig[featureName];

        if(ObjectUtils.isNullOrUndefined(featureValue)) {
          console.error('Feature name: ' + featureName + ' has invalid value: ' + featureValue);
          return false;
        }
        return featureValue === true;
      }

      function allFeaturesOn() {
        if(!featuresConfig) {
          return false;
        }

        return featuresConfig['allFeaturesOn'] === true;
      }

    return {
      isEnabled: isEnabled
    };
  }]);
})();
