(function(){
  'use strict';

  angular.module('shopperTrak').service('viewportService', ['$rootScope', viewportService]);

  function viewportService($rootScope) {

    var _breakpoints = {};
    var _viewportInfo = {
      width: 0,
      height: 0,
      breakpoint: {}
    };

    function getBreakpoints() {
      return _breakpoints;
    }

    function setBreakpoints(newBreakpoints) {
      if (_.isUndefined(newBreakpoints)) return false;
      _breakpoints = newBreakpoints;
      return true;
    }

    function getCurrentBreakpoint() {
      return _viewportInfo.breakpoint;
    }

    function setCurrentBreakpoint(newBreakpoint) {
      if (_.isUndefined(newBreakpoint.name) || newBreakpoint.name === _viewportInfo.breakpoint.name) return false;
      _viewportInfo.breakpoint = newBreakpoint;
      $rootScope.$broadcast('viewportBreakpointChanged', _viewportInfo.breakpoint);
      return true;
    }

    function getViewportInfo(){
      return _viewportInfo;
    }

    function setViewportInfo(info) {
      var newInfo = {};
      if (info.hasOwnProperty('width') && _.isNumber(info.width)) {
        newInfo.width = info.width;
        newInfo.widthChanged = info.width !== _viewportInfo.width;
      }
      if (info.hasOwnProperty('height') && _.isNumber(info.height)) {
        newInfo.height = info.height;
        newInfo.heightChanged = info.height !== _viewportInfo.height;
      }
      if (info.hasOwnProperty('breakpoint') && _.isString(info.breakpoint.name)) {
        var breakpointChanged = info.breakpoint.name !== _viewportInfo.breakpoint.name;
        newInfo.breakpoint = info.breakpoint;
        newInfo.breakpointChanged = breakpointChanged;
        if (breakpointChanged) setCurrentBreakpoint(newInfo.breakpoint);
      }
      _viewportInfo = _.extend(_viewportInfo, newInfo);
      $rootScope.$broadcast('viewportChanged', newInfo);
    }

    return {
      setCurrentBreakpoint: setCurrentBreakpoint,
      getCurrentBreakpoint: getCurrentBreakpoint,
      getBreakpoints: getBreakpoints,
      setBreakpoints: setBreakpoints,
      getViewportInfo: getViewportInfo,
      setViewportInfo: setViewportInfo
    };
  }

})();
