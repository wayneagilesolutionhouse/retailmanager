(function() {
  'use strict';

  angular.module('shopperTrak')
  .factory('currencyService', [
      'OrganizationResource',
      'SiteResource',
      '$q',
      'currencies',
      'ObjectUtils',
    function (
      OrganizationResource,
      SiteResource,
      $q,
      currencies,
      ObjectUtils
    ) {
    var defaultCurrency = 'USD';

    function getCurrencySymbol(orgId, siteId) {
      var deferred = $q.defer();

      var promises = [];

      promises.push(getOrganization(orgId));

      if(typeof siteId !== 'undefined') {
        promises.push(getSite(orgId, siteId));
      }

      $q.all(promises).then(function(params) {
        var organizationCurrency;
        var siteCurrency;

        _.each(params, function(param) {
          if(typeof param.orgCurrency === 'object') {
            organizationCurrency = param.orgCurrency;
          }

          if(typeof param.siteCurrency === 'object') {
            siteCurrency = param.siteCurrency;
          }
        });

        var currencyInfo = {
          orgId: orgId,
          currency: defaultCurrency
        };

        if(currencyIsValid(organizationCurrency)) {
          currencyInfo = organizationCurrency;
        }
        
        if(currencyIsValid(siteCurrency)) {
          currencyInfo = siteCurrency;
        }

        if(typeof siteCurrency !== 'undefined' && typeof siteCurrency.siteId !== 'undefined') {
          currencyInfo.siteId = siteCurrency.siteId;
        }

        currencyInfo.currencySymbol = currencies[currencyInfo.currency];

        deferred.resolve(currencyInfo);
      }).catch(function() {
        var currencyInfo = {
          orgId: orgId,
          currency: defaultCurrency,
          currencySymbol : currencies[defaultCurrency]
        };

        deferred.resolve(currencyInfo);
      });
      return deferred.promise;
    }

    function currencyIsValid(currencyInfo) {
      if(ObjectUtils.isNullOrUndefined(currencyInfo)) {
        return false;
      }

      if(ObjectUtils.isNullOrUndefined(currencyInfo.currency)) {
        return false;
      }

      if(currencyInfo.currency === 'NONE') {
        return false;
      }

      return true;
    }

    function getOrganization(orgId) {
      var deferred = $q.defer();

      OrganizationResource.get({
        orgId: orgId
      }).$promise.then(function(organization) {

        if(typeof organization !== 'undefined' &&
           typeof organization.portal_settings !== 'undefined' &&
           typeof organization.portal_settings.currency !== 'undefined') {

             var organizationCurrency = {
               orgId: organization.organization_id,
               currency: organization.portal_settings.currency
             };

             deferred.resolve({'orgCurrency': organizationCurrency });
        } else {
          deferred.reject();
        }
      });

      return deferred.promise;
    }

    function getSite(orgId, siteId) {
      var deferred = $q.defer();

      SiteResource.get({
        orgId: orgId,
        siteId: siteId
      }).$promise.then(function (site) {

        if(typeof site !== 'undefined' &&
           typeof site.currency !== 'undefined') {

            var siteCurrency = {
              orgId: site.organization.id,
              siteId: site.site_id,
              currency: site.currency
            };

            deferred.resolve({'siteCurrency': siteCurrency });
        } else {
          var fallbackSiteCurrency = {
            orgId: orgId,
            siteId: siteId,
            currency: 'NONE'
          };
          deferred.resolve({'siteCurrency': fallbackSiteCurrency });
        }

      });

      return deferred.promise;
    }

    return {
      getCurrencySymbol: getCurrencySymbol
    };

  }]);
})();
