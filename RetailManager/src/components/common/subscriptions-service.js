'use strict';

angular.module('shopperTrak')
  .factory('SubscriptionsService', ['ObjectUtils', function (ObjectUtils) {

    function getActiveSubscriptions(subscriptions) {
      return _.pick(subscriptions, function(value) {
        return (value === true);
      });
    }

    function getSubscriptions(siteOrOrg){
      if (!ObjectUtils.isNullOrUndefined(siteOrOrg) && hasSubscription(siteOrOrg.subscriptions)) {
        var subscriptions = getActiveSubscriptions(siteOrOrg.subscriptions);
        return _.keys(subscriptions);
      } else {
        return 'unknown';
      }
    }

    //TODO: Evaluate and compare with functions above
    function getSiteSubscriptions(currentOrganization, currentSite) {
      if (!ObjectUtils.isNullOrUndefined(currentSite) && hasSubscription(currentSite.subscriptions)) {
        if (currentSite.subscriptions.interior && currentSite.subscriptions.interior === true) {
          return 'interior';
        } else if (currentSite.subscriptions.perimeter && currentSite.subscriptions.perimeter === true) {
          return 'perimeter';
        } else {
          return 'unknown';
        }
      } else if (!ObjectUtils.isNullOrUndefined(currentOrganization.subscriptions)) {
        if (currentOrganization.subscriptions.interior) {
          return 'interior';
        } else if (currentOrganization.subscriptions.perimeter) {
          return 'perimeter';
        } else {
          return 'unknown';
        }
      } else {
        return 'unknown';
      }
    }

    function siteHasPerimeter(currentOrganization, currentSite) {
      if (!ObjectUtils.isNullOrUndefined(currentSite) && hasSubscription(currentSite.subscriptions)) {
        if (currentSite.subscriptions.perimeter && currentSite.subscriptions.perimeter === true) {
          return true;
        } else {
          return false;
        }
      } else if (!ObjectUtils.isNullOrUndefined(currentOrganization.subscriptions)) {
        if (currentOrganization.subscriptions.perimeter) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

    function hasSubscription(subscriptions) {
      return !ObjectUtils.isNullOrUndefined(subscriptions) && !ObjectUtils.isEmptyObject(subscriptions);
    }

    function siteHasInterior(currentOrganization, currentSite) {
      if (!ObjectUtils.isNullOrUndefined(currentSite) && hasSubscription(currentSite.subscriptions)) {
        if (currentSite.subscriptions.interior && currentSite.subscriptions.interior === true) {
          return true;
        } else {
          return false;
        }
      } else if (!ObjectUtils.isNullOrUndefined(currentOrganization.subscriptions)) {
        if (currentOrganization.subscriptions.interior) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

    function siteHasSales(currentOrganization, currentSite) {
      if (!ObjectUtils.isNullOrUndefined(currentSite) && hasSubscription(currentSite.subscriptions)) {
        if (currentSite.subscriptions.sales && currentSite.subscriptions.sales === true) {
          return true;
        } else {
          return false;
        }
      } else if (!ObjectUtils.isNullOrUndefined(currentOrganization.subscriptions)) {
        if (currentOrganization.subscriptions.sales) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

    function siteHasLabor(currentOrganization, currentSite) {
      if (!ObjectUtils.isNullOrUndefined(currentSite) && hasSubscription(currentSite.subscriptions)) {
        if (currentSite.subscriptions.labor && currentSite.subscriptions.labor === true) {
          return true;
        } else {
          return false;
        }
      } else if (!ObjectUtils.isNullOrUndefined(currentOrganization.subscriptions)) {
        if (currentOrganization.subscriptions.labor) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

  /** Determines if the current session has only an MI subscription
   *  If the user has access to sites, this function returns true if the org has only a market intelligence sub
   *  If the user does not have access to sites but the org has a market intelligence sub, this function returns true
   *
   *  @param {Object} currentOrganization - The current organization
   *  @param {Object[]} sites - The sites available to the current user. Optional.
   *  @returns {boolean}
   **/
    function onlyMiSubscription(currentOrganization, sites) {
      try {

        if(angular.isDefined(sites)) {
          if(sites.length === 0 && currentOrganization.subscriptions.market_intelligence === true) {
            return true;
          }
        }

        var orgSubscriptions = [];

        _.each(Object.keys(currentOrganization.subscriptions), function(key) {
          if(key !== 'large_format_mall' && currentOrganization.subscriptions[key] === true) {
            orgSubscriptions.push(key);
          }
        });

        return orgSubscriptions.length === 1 && orgSubscriptions[0] === 'market_intelligence';
      } catch (e) {
        return false;
      }
    }

    function hasMarketIntelligence(currentOrganization) {
      if(!_.has(currentOrganization, 'subscriptions')) {
        return false;
      }

      if(!_.has(currentOrganization.subscriptions, 'market_intelligence')) {
        return false;
      }

      return currentOrganization.subscriptions.market_intelligence;
    }

    function hasMiOrgIndex(currentUser, currentOrg) {
      if(onlyMiSubscription(currentOrg)) {
        // If the org only has an MI sub, they can't be giving us traffic data, and therefore cannot have an index for their org
        return false;
      }

      if(ObjectUtils.isNullOrUndefined(currentUser.subscriptions)) {
        return false;
      }

      if(ObjectUtils.isNullOrUndefinedOrEmpty(currentUser.subscriptions.mi_index)) {
        return false;
      }

      return _.contains(currentUser.subscriptions.mi_index, currentOrg.organization_id);
    }

    function hasRealTime(currentOrganization) {
      return !ObjectUtils.isNullOrUndefined(currentOrganization) && (
        currentOrganization.subscriptions.realtime_labor === true ||
        currentOrganization.subscriptions.realtime_sales === true ||
        currentOrganization.subscriptions.realtime_traffic === true);
    }

    function hasSubscriptions(requiredSubscriptions, currentOrganization) {
      var orgSubscriptions = [];
      _.each(currentOrganization.subscriptions, function(value, key) {
        if(value === true) {
          orgSubscriptions.push(key);
        }
      });

      var subscriptionsCount = 0;

      _.each(requiredSubscriptions, function(permission) {
        if(_.contains(orgSubscriptions, permission)) {
          subscriptionsCount ++;
        }
      });

      return subscriptionsCount === requiredSubscriptions.length;
    }

    function hasCampaigns(currentOrganization) {
      return !ObjectUtils.isNullOrUndefined(currentOrganization) &&
             !ObjectUtils.isNullOrUndefined(currentOrganization.subscriptions) &&
             currentOrganization.subscriptions.campaigns === true;
    }

    //Check the user object for access to MI.
    function userHasMarketIntelligence(userObject, orgId) {
      var hasAccess = false;
      
      try {
        if (userObject.accessMap.setup.mi_orgs) {
          var mi_orgs = userObject.accessMap.setup.mi_orgs;
          hasAccess =  (mi_orgs.indexOf(orgId) > -1); //Check to see if the user has the orgId in their mi_orgs accessmap.
        }
      } catch (e) { }
      return hasAccess;
    }

    return {
      getSubscriptions: getSubscriptions,
      getSiteSubscriptions: getSiteSubscriptions,
      siteHasPerimeter: siteHasPerimeter,
      siteHasInterior: siteHasInterior,
      siteHasSales: siteHasSales,
      siteHasLabor: siteHasLabor,
      hasRealTime: hasRealTime,
      hasMarketIntelligence: hasMarketIntelligence,
      userHasMarketIntelligence : userHasMarketIntelligence,
      onlyMiSubscription: onlyMiSubscription,
      hasSubscriptions: hasSubscriptions,
      hasCampaigns: hasCampaigns,
      hasMiOrgIndex:hasMiOrgIndex
    };
  }]);
