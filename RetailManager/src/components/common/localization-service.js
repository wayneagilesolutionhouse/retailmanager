'use strict';

angular.module('shopperTrak')
.factory('LocalizationService', [
  'localStorageService',
  '$http',
  '$q',
  'apiUrl',
  'ObjectUtils',
  'requestManager',
function (
  localStorageService,
  $http,
  $q,
  apiUrl,
  ObjectUtils,
  requestManager
) {

  var activeOrganization;
  var allCalendars = localStorageService.get('calendars') || undefined;
  var activeUser;
  var calendarFirstDayOfWeek;
  var weeksAgo = {};
  var customCompareSetting = {};


/**
 * Enum for calendar types.
 * @readonly
 * @enum {number}
 */
  var calendarType = {
    appDefault: 0,
    orgDefault: 1,
    userPreference: 2
  };

      /**
       * Enum for calendar types.
       * @readonly
       * @enum {number}§§§
       */
      var daysOfWeek = {
        sunday: 0,
        monday: 1,
        tuesday: 2,
        wednesday: 3,
        thursday: 4,
        friday: 5,
        saturday: 6
      };

      var standardGregorianSundayCalendarId = -2;
      var standardGregorianMondayCalendarId = -1; // Declared default calendar ID's for Gregorian Sunday & Monday
      var standardMonthlyCalendarId = 65; // Other Sunday Gregorian calendar

/**
 * Returns the legacy first day of week from an organization object.
 * This should only be called when the user is on a 'Standard' calendar - i.e. they do not have a calendar set
 * and the current organization does not have a calendar set
 * 
 * @param {object} currentOrganization - The current organization as returned by /organizations/:orgId
 * @returns {number} The day of the week. Sunday = 0, Saturday = 6
 */
  function getLegacyFirstDayOfWeek(currentOrganization) {
    if (!ObjectUtils.isNullOrUndefined(calendarFirstDayOfWeek)) {
      return calendarFirstDayOfWeek;
    } else {

      if (!ObjectUtils.isNullOrUndefined(currentOrganization) && !ObjectUtils.isNullOrUndefined(currentOrganization.portal_settings)) {
        var firstWeekDaySetting = currentOrganization.portal_settings.legacy_week_start_day;

        if (ObjectUtils.isNullOrUndefined(firstWeekDaySetting)) {
          firstWeekDaySetting = 'Sunday';
        }

        firstWeekDaySetting = firstWeekDaySetting.toLowerCase();

        return daysOfWeek[firstWeekDaySetting];

      } else {
        return daysOfWeek.sunday; // default to Sunday if setting not available
      }
    }
  }

/**
 * Gets the first day of the week for the current user
 * This takes into account whether the user is on a custom or standard calendar
 * 
 * @returns {number} The day of the week. Sunday = 0, Saturday = 6
 */
  function getCurrentCalendarFirstDayOfWeek() {
    if (!ObjectUtils.isNullOrUndefined(activeUser) && !ObjectUtils.isNullOrUndefined(allCalendars)) {
      var currentCalendarSettings = getActiveCalendarSettings(false);

      if (ObjectUtils.isNullOrUndefined(currentCalendarSettings) ||
        ObjectUtils.isNullOrUndefinedOrEmpty(currentCalendarSettings.years)) {

        var userCalendarId = activeUser.preferences.calendar_id;

        if (userCalendarId === standardGregorianSundayCalendarId) {
          return daysOfWeek.sunday;
        }

        if(userCalendarId === standardMonthlyCalendarId) {
          return daysOfWeek.sunday;
        }
        
        if (userCalendarId === standardGregorianMondayCalendarId) {
          return daysOfWeek.monday;
        }
         
        // 1. Check if the calendar has been set in the admin tool for the current org
        if(!ObjectUtils.isNullOrUndefined(activeOrganization) && !ObjectUtils.isNullOrUndefined(activeOrganization.default_calendar_id)) {
          return getFirstDayOfWeekInCalendar(activeOrganization.default_calendar_id);
        }

        // Fallback to the legacy portal setting on the activeOrg
        return getLegacyFirstDayOfWeek(activeOrganization);
      } else {
        var year = getCurrentYear();
        var firstDate = getFirstDayOfYear(year);

        return firstDate.utc().weekday();
      }
    }
  }

/**
 * Gets the first day of the week for the specified calendar
 * This takes into account whether the user is on a custom or standard calendar.
 * This function is identical to getCurrentCalendarFirstDayOfWeek, with the exception of
 * taking in the calendarId.
 * 
 * @param {number} calendarId the calendarId
 */
  function getFirstDayOfWeekInCalendar(calendarId) {
    if (ObjectUtils.isNullOrUndefined(allCalendars) && ObjectUtils.isNullOrUndefined(activeOrganization)) {
      return;
    }

    if(calendarId === standardGregorianSundayCalendarId) {
      return daysOfWeek.sunday;
    }

    if(calendarId === standardMonthlyCalendarId) {
      return daysOfWeek.sunday;
    }

    if(calendarId === standardGregorianMondayCalendarId) {
      return daysOfWeek.monday;
    }

    var calendarSettings = getCalendarSettings(calendarId);

    if (ObjectUtils.isNullOrUndefined(calendarSettings) || ObjectUtils.isNullOrUndefined(calendarSettings.years)) {
      return getLegacyFirstDayOfWeek(activeOrganization);
    } else {
      var year = getCurrentYear();
      var firstDate = getFirstDayOfYear(year, calendarSettings);
      return firstDate.utc().weekday();
    }
  }


/**
 * Gets all calendars.
 * This attempts to load calendars in the following order:
 * 1. In memory cache
 * 2. Local Storage  (ls.calendars key)
 * 3. A http request to /calendars
 * 
 * @returns {Object} A promise containing the calendars
 */
  function getAllCalendars() {
    // 1 - In Memory Cache
    if(!ObjectUtils.isNullOrUndefined(allCalendars)) {
      return promisify(allCalendars);
    }

    // 2 - Local Storage
    var lsCalendars = localStorageService.get('calendars');

    if(!ObjectUtils.isNullOrUndefined(allCalendars)) {
      return promisify(lsCalendars);
    }

    // 3 - The API
    var deferred = $q.defer();

    var req = requestManager.get(apiUrl + '/calendars', {});

    req.then(function( calendar ) {
      setAllCalendars(calendar.result);

      deferred.resolve(calendar);
    }, function() {
      deferred.reject();
    });

    return deferred.promise;
  }


/**
 * Returns the active calendar id with the calendarType enum to indicate how it was resolved.
 * Tries to resolve the calendarId in the following order:
 * 1. The user prefered calendar
 * 2. The current organization's default calendar
 * 3. The application default calendar
 * 
 * @returns {object} An object that contains the active calendarId and the calendarType
 */
  function getActiveCalendar() {
    var calendarInfo = { };
    var preferences = getUserPreferences();

    calendarInfo.id = preferences.calendar_id;

    if (ObjectUtils.isNullOrUndefined(calendarInfo.id)) {
      if (!ObjectUtils.isNullOrUndefined(activeOrganization)) {
        calendarInfo.id = activeOrganization.default_calendar_id;
        calendarInfo.type = calendarType.orgDefault;
      }

      if(calendarInfo.id === standardGregorianSundayCalendarId || 
          calendarInfo.id === standardGregorianMondayCalendarId ||
          calendarInfo.id === standardMonthlyCalendarId) {
            return calendarInfo; 
      }

      if (ObjectUtils.isNullOrUndefined(calendarInfo.id) || ObjectUtils.isNullOrUndefined(getCalendarSettings(calendarInfo.id))) {
        calendarInfo.id = getAppDefaultCalendarId();
        calendarInfo.type = calendarType.appDefault;
      }
    }
    return calendarInfo;
  }


/**
 * Returns the active calendar.
 * Tries to resolve the calendarId in the following order:
 * 1. The user prefered calendar
 * 2. The current organization's default calendar
 * 3. The application default calendar (this last fallback can be disabled)
 * 
 * @param {boolean} fallbackToAppDefaultCalendar - Optional. Defaults to true. Specifies if the Application default calendar is an acceptable result.
 * @returns {object} The active calendar's object
 */
  function getActiveCalendarSettings(fallbackToAppDefaultCalendar) {
    if (ObjectUtils.isNullOrUndefined(fallbackToAppDefaultCalendar)) {
      fallbackToAppDefaultCalendar = true;
    }
    // We do not need to do any default checking here as getActiveCalendar does it for us
    var calendarInfo = getActiveCalendar();

    if (calendarInfo.type === calendarType.appDefault && fallbackToAppDefaultCalendar === false) {
      return;
    }

    return getCalendarSettings(calendarInfo.id);
  }

/**
 * Gets the first day of the specified month for the current calendar.
 * 
 * @param {number} month the zero indexed month number, ranging from 0 to 11
 * @param {number} year the year
 * @returns {Object} A momentJs object that is the first day of the month
 */
  function getFirstDayOfMonth(month, year) {
    var currentCalendarSettings = getActiveCalendarSettings();

    if(isGregorian(currentCalendarSettings)) {
      month = month + 1;

      if(month < 10) {
        month = '0'+ month;
      }

      return moment.utc(year+'-'+month+'-01').startOf('month');
    } else {
      var currentYear = _.findWhere(currentCalendarSettings.years, {year: year});

      if(ObjectUtils.isNullOrUndefined(currentYear)) {
        return;
      }

      var weeksFromStart = 0;

      _.each(currentYear.month_mask, function(numberOfWeeksInMonth, index) {
        if(index < month){
          weeksFromStart += numberOfWeeksInMonth;
        }
      });

      var firstDayOfYear = moment.utc(currentYear.start_date);

      return firstDayOfYear.add(weeksFromStart, 'weeks');
    }
  }

/**
 * Gets the last day of the specified month for the current calendar.
 * 
 * @param {number} month the zero indexed month number, ranging from 0 to 11
 * @param {number} year the year
 * @returns {Object} A momentJs object that is the last day of the month
 */
  function getLastDayOfMonth(month, year) {
    var currentCalendarSettings = getActiveCalendarSettings();

     if(isGregorian(currentCalendarSettings)) {
       month = month + 1;

       if(month < 10) { 
         month = '0' + month; 
       }

       var dateStr = year.toString() + '-' + month.toString() + '-01';

       return moment.utc(dateStr, 'YYYY-MM-DD', true).endOf('month');
    } else {

       var monthWeeks = getWeekCountOfMonth(year, month);

       var monthStart = getFirstDayOfMonth(month, year);

       if (!ObjectUtils.isNullOrUndefined(monthStart)) {
         return monthStart.add(monthWeeks, 'weeks').subtract(1, 'day');
       }
    }
  }

/**
 * Gets the first day of the specified year for the calendar that is passed in, or the default calendar
 * 
 * @param {number} year the year
 * @param {object} calendar Optional. the calendar to check. If this is passed in as null or undefined, the current calendar is used instead.
 * @returns {Object} A momentJs object that is the first day of the year
 */
  function getFirstDayOfYear(year, _calendarSettings) {
    var calendarSettings = _calendarSettings;

    var dateStr;


    if(ObjectUtils.isNullOrUndefined(calendarSettings) ||
      ObjectUtils.isNullOrUndefined(calendarSettings.years)) { // gregorian
      calendarSettings = getActiveCalendarSettings();
    }

    if(isGregorian(calendarSettings)) {
      dateStr = year.toString() + '-01-01';
      return moment.utc(dateStr, 'YYYY-MM-DD', true);
    } else {

      var result = _.findWhere(calendarSettings.years, {year: year});

      if(ObjectUtils.isNullOrUndefined(result)) {;
        dateStr = year.toString() + '-01-01';
        return moment.utc(dateStr, 'YYYY-MM-DD', true);
      }

      return moment.utc(result.start_date);

    }
  }

/**
 * Gets the last day of the specified year for the current calendar with a time of 23:59:59
 * 
 * @param {number} year the year
 * @returns {Object} A momentJs object that is the first day of the year
 */
  function getLastDayOfYear(year) {
    var currentCalendarSettings = getActiveCalendarSettings();

    if(isGregorian(currentCalendarSettings)) {

      var dateStr = year.toString() + '-01-01';

      return moment.utc(dateStr, 'YYYY-MM-DD', true).endOf('year');
    } else {
       var yearStart = getFirstDayOfYear(year);

       var monthDefinitions = getMonthDefinitions(year);

       var yearWeeks = _.reduce(monthDefinitions, function(total, weeks) {
         if(!ObjectUtils.isNullOrUndefined(weeks)) {
           return total + weeks;
         } else {
           return total;
         }
       }, 0);

      return yearStart.add((yearWeeks-1),'week').add(6,'day').endOf('day');
    }
  }

/**
 * Gets the first calendar month of the specified year for the current calendar
 * 
 * @param {number} year the year
 * @returns {number} The first month of the year. Zero indexed.
 */
  function getFirstMonthOfYear(year) {
    var currentCalendarSettings = getActiveCalendarSettings();

     if(isGregorian(currentCalendarSettings)) {
      return 0;
    } else {
      var result = _.filter(currentCalendarSettings.years, function(yearData) {
        return Number(yearData.year) === Number(year);
      });

      if(ObjectUtils.isNullOrUndefinedOrEmpty(result)) {
        return 0;
      }

      return result[0].start_month;
    }
  }

/**
 * Gets the first date in the current week for the current calendar
 *
 * @returns {object} A momentJs Object
 */
  function getFirstDayOfCurrentWeek() {
    var currentCalendarSettings = getActiveCalendarSettings();
    
    if (isGregorian(currentCalendarSettings)) {
      if (isGregorianMonday() === true) {
        return moment.utc().startOf('week').add(1, 'days');
      }

      return moment.utc().startOf('week');

    } else {
      var date = moment();
      var year = getCurrentYear();
      var firstDayOfYear = getFirstDayOfYear(year);
      var week = Math.ceil(moment(date).diff(firstDayOfYear) / (24 * 3600 * 1000) / 7) - 1;
      if (week < 1) {
        // For the first week of year, use previous year to calculate current week
        firstDayOfYear = getFirstDayOfYear(year - 1);
        week = getNumberOfWeeksInYear(year - 1) - week;
      }
      return firstDayOfYear.add(week, 'weeks');
    }
  }

  /**
   * Helper function to check if the user has set a gregorian calendar to start on a Monday
   *
   * @returns {boolean} true/false
   */
   function isGregorianMonday() {

     var calendarInfo = getActiveCalendar();

     return isCalendarIdGregorianMonday(calendarInfo.id);
   }

  /**
   * Helper function to check if calendarId is the gregorian monday calendarId
   * 
   * @param {number} calendarId The calendarId
   * @returns {boolean} true/false
   */
   function isCalendarIdGregorianMonday(calendarId) {
     return calendarId === standardGregorianMondayCalendarId;
   }

  /**
   * Helper function to check if calendarId is the gregorian sunday calendarId
   * 
   * @param {number} calendarId The calendarId
   * @returns {boolean} true/false
   */
   function isCalendarIdGregorianSunday(calendarId) {
     return calendarId === standardGregorianSundayCalendarId;
   }

/**
 * Gets number of weeks in a year
 *
 * @returns {integer} Number of weeks
 */
  function getNumberOfWeeksInYear(year) {
    var currentCalendarSettings = getActiveCalendarSettings();
    if(isGregorian(currentCalendarSettings)) {
      return moment(year+'-12-31').week();
    } else {
      var result = _.filter(currentCalendarSettings.years, function(yearData) {
        return Number(yearData.year) === Number(year);
      });
      return  _.reduce(result[0].month_mask, function(memo, num){ return memo + num; }, 0);
    }
  }

/**
 * Gets the last date in the current week for the current calendar
 *
 * @returns {object} A momentJs Object
 */
  function getLastDayOfCurrentWeek() {
    var currentCalendarSettings = getActiveCalendarSettings();
    if(ObjectUtils.isNullOrUndefined(currentCalendarSettings) ||
       ObjectUtils.isNullOrUndefinedOrEmpty(currentCalendarSettings.years)) { // gregorian

       if (isGregorianMonday()) {
         return moment.utc().endOf('week').add(1, 'days');
       } else {
         return moment.utc().endOf('week');
       }
    } else {
      var start = getFirstDayOfCurrentWeek();
      return start.add(6, 'days').endOf('day');
    }
  }

/**
 * Gets the month definitions (array containing week numbers) for the current calendar
 * 
 * @param {number} year the year
 * @returns {array} An array of numbers, which is the number of weeks in each month. This is not zero indexed. The item at position zero is always undefined.
 * It is ordered by the month after the start month. E.g. if the start month is Feb, the first item in the array will be the number of weeks for March
 * 
 */
  function getMonthDefinitions(year) {
    var currentCalendarSettings = getActiveCalendarSettings();
    var numberOfWeeks = [];
    if(!isGregorian(currentCalendarSettings)) {

      var result = _.filter(currentCalendarSettings.years, function (yearData) {
        return parseInt(yearData.year) === parseInt(year);
      });
      var startMonth = getFirstMonthOfYear(year);

      if(ObjectUtils.isNullOrUndefinedOrEmpty(result)) {
        return null;
      }

      _.each(result[0].month_mask, function (month, key) {
        var actualKey = key + (startMonth);
        if (actualKey >= 12) {
          actualKey = parseInt(actualKey - result[0].month_mask.length);
        }
        numberOfWeeks[(actualKey + 1)] = month;
      });

      return numberOfWeeks;
    } else {
      return null;
    }
  }

 /** Gets the correct definition for a month according to calendar settings.
  *  For example definition for January can be found from earlier year's
  *  year definition.
  * 
  *  @param {number} year the year
  *  @param {number} month the month, zero indexed
  *  @returns {number} The number of weeks in the month
  **/
  function getWeekCountOfMonth(year, month) {
    var numWeeks, earlierMonth;

    month = month + 1;

    var firstDayOfSelectedMonth = getFirstDayOfMonth(month, year);
    var currentCalendarSettings = getActiveCalendarSettings();

    if(!isGregorian(currentCalendarSettings)) {
      _.each(currentCalendarSettings.years, function(y) {
        if(ObjectUtils.isNullOrUndefined(numWeeks)) {
          var dt = moment.utc(y.start_date);

          _.each(y.month_mask, function (weeks, monthIndex) {
            if (dt >= firstDayOfSelectedMonth) {
              numWeeks = earlierMonth;
            } else {
              earlierMonth = weeks;
              dt.add(weeks, 'weeks');

              if(isFinalMonthOfYear(monthIndex) && (dt >= firstDayOfSelectedMonth)) {
                numWeeks = earlierMonth;
              }
            }
          });
        }
      });

      return numWeeks;
    } else {
      return 0;
    }
  }
  
 /** Gets the week number based on the last valid date that is passed in
  * 
  *  @param {array} weekDays An Array of weekdays to check. The last valid date is used.
  *  @returns {number} The week that the last valid date is in
  **/
  function getWeekNumber(weekDays) {
    var currentCalendarSettings = getActiveCalendarSettings();

    var result = _.filter(weekDays, function(day) {
      return !ObjectUtils.isNullOrUndefined(day) && typeof day.isValid === 'function' && day.isValid();
    });
    var date = result.slice(-1)[0];

    if(isGregorian(currentCalendarSettings)) {
      if(!ObjectUtils.isNullOrUndefined(date) && typeof date.isValid === 'function' && date.isValid()) {
        var firstWeekdaySetting = getCurrentCalendarFirstDayOfWeek();
        if(firstWeekdaySetting===0) {
          return angular.copy(date).day(6).isoWeek();
        } else {
          return date.isoWeek();
        }
      }
    } else {
      var systemDate = getSystemYearForDate(date);
      var year = systemDate.year;

      var firstDayOfYear = getFirstDayOfYear(year);
      var week = Math.ceil(moment(date).diff(firstDayOfYear) / (24*3600*1000)/7);

      if(week < 1) {
        firstDayOfYear = getFirstDayOfYear(year-1);
        week = Math.ceil(moment(date).diff(firstDayOfYear) / (24*3600*1000)/7);
      }
      return week;
    }
  }

 /** Gets the current month based on the active calendar
  *  ToDo: Fix this so that it is zero indexed. Do not do this until it is unit tested
  *        and all code that calls it is unit tested
  *  @returns {number} The current month. Not zero indexed.
  **/
  function getCurrentMonth() {
    var currentMonth = -1;

    var currentCalendarSettings = getActiveCalendarSettings();

    if(isGregorian(currentCalendarSettings)) {
       return moment().format('M'); // This will return a string. Needs fixing.
    } else {
      var day = [];
      day[0] = moment(); // ToDo: Abstract this out into an angular service to make testing possible
      var week = getWeekNumber(day);

      var currentYear = getCurrentYear();
      var firstMonthOfYear = getFirstMonthOfYear(currentYear)+1;
      var monthDefinitions = getMonthDefinitions(currentYear);
      var counter = 0;

      _.each(monthDefinitions,function(weeks,monthKey) {
        if(!ObjectUtils.isNullOrUndefined(weeks) && monthKey >= firstMonthOfYear) {
          counter = counter + parseInt(weeks);
          if(week <= counter && currentMonth === -1) {
            currentMonth = monthKey;
          }
        }
      });

      if(currentMonth === -1) {
        monthDefinitions = getMonthDefinitions(currentYear - 1);
        firstMonthOfYear = getFirstMonthOfYear(currentYear - 1);

        _.each(monthDefinitions,function(weeks,monthKey) {
          if(!ObjectUtils.isNullOrUndefined(weeks) && monthKey >= firstMonthOfYear) {
            counter = counter + parseInt(weeks);
            if(week <= counter && currentMonth === -1) {
              currentMonth = monthKey;
            }
          }
        });
      }

      return currentMonth;
    }
  }

/** Gets the current year based on the active calendar
 *
 *  This function is a little faulty for certain calendars.
 *  Return the year from the active calendar. Call getSystemYearForDate instead
  *
  *  @returns {number} The current year
  **/
  function getCurrentYear() {
    var currentCalendarSettings = getActiveCalendarSettings();
    if(isGregorian(currentCalendarSettings)) {
       return Number(moment().format('YYYY'));
    } else {
      var systemDate = getSystemYearForDate(moment.utc());
      return systemDate.year;
    }
  }

/** Gets the year for the date passed in based on the active calendar
 *
 *  @param {Object} date - the date under question, as a momentJs object
 *  @returns {Object} The year and month for the date passed in. Month is zero indexed.
 **/
  function getSystemYearForDate(date) {
    var ret = {};
    var currentSettings = getActiveCalendarSettings();

    if(isGregorian(currentSettings)) {
      ret = {
        month: Number(angular.copy(date).format('M')), // Fix this and make it zero indexed
        year: Number(angular.copy(date).format('YYYY'))
      };

      if(ret.month < date.format('M')) { // How can this ever be true?
        ret.year = ret.year + 1;
      }
    } else {

      _.each(currentSettings.years, function(year) {
        if(ObjectUtils.isNullOrUndefined(ret.month) && ObjectUtils.isNullOrUndefined(ret.year)) {
          var dt = moment.utc(year.start_date);

          _.each(year.month_mask, function (weeks, monthIndex) {
            
            if ( dt.isAfter(date) ) {
              
              if(ObjectUtils.isNullOrUndefined(ret.month) && ObjectUtils.isNullOrUndefined(ret.year)) {

                ret = {
                  year: year.year,
                  month: monthIndex-1
                };
              }
            } else {
              dt.add(weeks, 'weeks');
              // This check fixes a bug where if your current period is in the last
              // defined month of the calendar, no value gets assigned to ret
              if(isFinalMonthOfYear(monthIndex) && dt.isAfter(date)) {
                ret = {
                  year: year.year,
                  month: monthIndex
                };
              }
            }

          });
        }
      });
    }
    
    return ret;
  }

/** Sets the current organization. Should be called before other functions are called
 *  ToDo: Call this only once when the current organization changes, and during app load.
 * 
 *  @param {object} currentOrganization - The current organization, as returned from the API
 **/
  function setOrganization(currentOrganization) {
    activeOrganization = currentOrganization;
  }

/** Sets all calendars that are available to the user. Stores the calendars into local Storage
 *  ToDo: Remove this function and request calendars from the API automatically
 * 
 *  @param {array} calendars - Collection of calendar objects, as returned from the API
 **/
  function setAllCalendars(calendars) {
    localStorageService.set('calendars', calendars);
    allCalendars = calendars;
  }

/** Sets the current user. Should be called before other functions are called
 *  ToDo: Call this function only once when the user logs in
 *        or automatically call the currentUser endpoint after login and when this service loads
 * 
 *  @returns {object} currentUser - The user object, as returned from the API
 **/
  function setUser(currentUser) {
    activeUser = currentUser;
  }

/** Retrieves the number formats available on the user preferences page.
 * 
 *  @returns {object} number preferences
 **/
  function getNumberFormats(){
    return [
      { name: null, description: 'Use organization default' },
      { name: 'en-us', description: '1,234.00' },
      { name: 'international', description: '1.234,00' }
    ];
  }

/** Retrieves the number format separators
 * 
 *  @returns {object} seperator options
 **/
  function getNumberFormatSeparatorsByName(name){
    var separators = {
      'en-us': { decimalSeparator: '.', thousandsSeparator: ',' },
      'international': { decimalSeparator: ',', thousandsSeparator: '.' },

    };
    return separators[name] || { decimalSeparator: null, thousandsSeparator: null };
  }

/** Infers the name of the number format based on the seperators
 * 
 *  @returns {string} The format key
 **/
  function getNumberFormatName(number_format) {
    if (typeof number_format === 'undefined' || number_format === null) {
      number_format = {};
    }
    var formatKey = null;
    if ( number_format.decimal_separator === ',' && number_format.thousands_separator === '.' ) {
      formatKey = 'international';
    } else if ( number_format.decimal_separator === '.' && number_format.thousands_separator === ',' ) {
      formatKey = 'en-us';
    }
    return formatKey;
  }

/** Gets the number format name
 * 
 *  @param {object} currentUser the user to check
 *  @param {object} currentOrganization the org to check
 *  @returns {string} The format key
 **/
  function getCurrentNumberFormatName(currentUser, currentOrganization) {
    // Try to get user settings first
    var numberFormat = getUserNumberFormat(currentUser);
    if (!ObjectUtils.isNullOrUndefined(numberFormat) && 
        !ObjectUtils.isNullOrUndefined(numberFormat.decimal_separator) &&
        !ObjectUtils.isNullOrUndefined(numberFormat.thousands_separator)) {
          return getNumberFormatName(numberFormat);
    }

    // Fall back to organization settings if available
    numberFormat = currentOrganization ?
      getOrganizationNumberFormat(currentOrganization) :
      null;
   return getNumberFormatName(numberFormat);
  }

/** Gets the current date format. 
 *  Attempts to lift the format from the current user first, before falling back to the currentOrg
 * 
 *  @param {object} currentOrganization the user to check
 *  @returns {string} The format key
 **/
  function getCurrentDateFormat(currentOrganization) {
    if(currentUserHasDateFormatSetting(activeUser)) {
      return activeUser.localization.date_format.mask;
    }
    if(currentOrganizationHasDateFormatSetting(currentOrganization))  {
      return currentOrganization.localization.date_format.mask;
    } else {
      return 'MM/DD/YYYY';
    }
  }

/** Safely retrieves the active user's locale settings.
 *  Defaults to en_US if no locale settings are found
 * 
 *  @returns {string} locale
 **/
  function getCurrentLocaleSetting () {
    /* @todo: add locale picker with validation */
    var locale = 'en_US';
    if(!ObjectUtils.isNullOrUndefined(activeUser) &&
      !ObjectUtils.isNullOrUndefined(activeUser.localization) &&
      !ObjectUtils.isNullOrUndefined(activeUser.localization.locale)) {
      locale = activeUser.localization.locale;
    }
    return locale;
  }

/** Gets the first day of the week from the current org.
 *  Defaults to Sunday if the data is not available
 * 
 *  @returns {number} The day of the week. Zero indexed. Sunday = 0, Saturday = 6.
 **/
  function getCurrentOrganizationDaysOfWeek() {
    var firstDayOfWeek = getCurrentCalendarFirstDayOfWeek() || 0;

    return getDaysOfWeek(firstDayOfWeek);
  }

/** Gets the days of the week, ordered based on the first day of the week passed in
 *  
 *  @param {number} firstDayOfWeek Zero indexed day of week to start on
 *  @returns {array} Array of weekday labels, ordered correctly
 **/
  function getDaysOfWeek(firstDayOfWeek) {
    var standardWeekDayLabels = ['sun','mon','tue','wed','thu','fri','sat'];

    var daysOfWeek = [];

    while(daysOfWeek.length < 7) {
      var weekDayLabel = standardWeekDayLabels[firstDayOfWeek];

      daysOfWeek.push(weekDayLabel);

      if(firstDayOfWeek === 6) {
        firstDayOfWeek = 0;
      } else {
        firstDayOfWeek ++;
      }
    }

    return daysOfWeek;
  }

/** Gets the weeks ago setting
 *  
 *  @returns {object} Weeks ago setting
 **/
  function getWeeksAgo(){
    return weeksAgo;
  }

/** sets the weeks ago setting
 *  
 *  @param {object} data to be stored as the weeks ago setting
 **/
  function setWeeksAgo(data){
    weeksAgo = data;
  }

/** Gets the customCompare setting
 *  ToDo: Move this. It should't be a reponsiblity of the localization service.
 *  
 *  @returns {object} Weeks ago setting
 **/
  function getCustomCompareSetting(){
    return customCompareSetting;
  }

/** Sets the customCompare setting
 *  ToDo: Move this. It should't be a reponsiblity of the localization service.
 *  
 *  @param {object} data to be stored as the customCompare setting
 **/
  function setCustomCompareSetting(data){
    customCompareSetting = data;
  }

/** Gets the start of the current calendar
 *  
 *  @returns {object} The first date of the first year of the active calendar.
 **/
  function getStartOfCurrentCalendar() {
    var currentCalendarSettings = getActiveCalendarSettings();
    var minYear = moment();

    if(isGregorian(currentCalendarSettings)) {
      minYear = 1970;
    } else{
      _.each(currentCalendarSettings.years, function(year) {
        if(minYear > moment(year.start_date)) {
          minYear = year.year;
        }
      });
    }
    return getFirstDayOfYear(minYear,currentCalendarSettings);
  }

/** Gets the end of the current calednar
 *  
 *  @returns {object} The last date of the last year of the active calendar.
 **/
  function getEndOfCurrentCalendar() {
    var previousYear, lastYear;
    var currentCalendarSettings = getActiveCalendarSettings();    

    // Default to the current year, which will remain as this value for gregorian cals
    lastYear = moment().year();

    if (!isGregorian(currentCalendarSettings)) {
      var years = currentCalendarSettings.years;
      _.each(years, function (year) {
        if (moment(year.start_date) > previousYear) {
          lastYear = year.year;
        }
        previousYear = moment(year.start_date);
      });
    }

    return getLastDayOfYear(lastYear);
  }

/** Checks if the current calendar has year definitions
 *  
 *  @returns {boolean}
 **/
  function hasMonthDefinitions() {
    var currentCalendarSettings = getActiveCalendarSettings();

    if(ObjectUtils.isNullOrUndefined(currentCalendarSettings)) {
      return false;
    }

    if(ObjectUtils.isNullOrUndefinedOrEmpty(currentCalendarSettings.years)) {
      return false;
    }

    if(currentCalendarHasExpired()) {
      return false;
    }

    return true;
  }

/**
 * Determines if the current calendar has expired.
 *
 * @returns {boolean}
 */
  function currentCalendarHasExpired() {
    var latestSupportedDate = getCurrentCalendarLatestDate();

    if(moment.utc().isAfter(latestSupportedDate)) {
      return true;
    }

    return false;
  }

/**
 * Gets the current, user selected calendar name. Ignores any isGregorian checks.
 *
 * @returns {boolean}
 */
  function getCurrentCalendarName() {
    var currentCalendarSettings = getActiveCalendarSettings();

    if(ObjectUtils.isNullOrUndefined(currentCalendarSettings)) {
      return;
    }

    return currentCalendarSettings.name;

  }

/**
 * Gets the Standard Monthly calendarId
 * 
 * @returns {number} The calendarId
 */
  function getStandardMonthlyCalendarId() {
    return standardMonthlyCalendarId;
  }

/**
 * Gets the Standard Gregorian Sunday calendarId
 * 
 * @returns {number} The calendarId
 */
  function getStandardGregorianSundayCalendarId() {
    return standardGregorianSundayCalendarId;
  }

  /**
 * Gets the Standard Gregorian Monday calendarId
 * 
 * @returns {number} The calendarId
 */
  function getStandardGregorianMondayCalendarId() {
    return standardGregorianMondayCalendarId;
  }

  /**
   * 
   * Private Functions 
   * 
   */

/**
 * Helper function to wrap something in a promise
 * Private function
 *
 * @returns {Object} item - anything you want
 */
  function promisify(item) {
    return $q(function(resolve) {
      resolve({result: item});
    });
  }

/**
 * Finds the calendar object that matches the calendarId param
 * Private function
 * 
 * @param {number} calendarId
 * @returns {object} The active calendar's object
 */
  function getCalendarSettings(calendarId) {
    var result = _.filter(allCalendars, function(calendar) {
      return Number(calendar.calendar_id) === Number(calendarId);
    });
    return result[0];
  }

  /**
   * Checks if calendar for active user is Gregorian and exposing existing private isGregorian method publicly
   *
   * @param {boolean} ignoreExpiredCheck optional. Defaults to false.
   * Public function
   * @returns {boolean}
   */
  function isCurrentCalendarGregorian(ignoreExpiredCheck) {
    if (!ObjectUtils.isNullOrUndefined(activeUser)) {
      var activeCalendarSetting = getActiveCalendarSettings();
      return isGregorian(activeCalendarSetting, ignoreExpiredCheck)
    }
    return false;
  }

/**
 * Determines if the passed in calendar is Gregorian or not
 * Private function
 * 
 * @param {object} calendar a calendar object to test
 * @param {boolean} ignoreExpiredCheck optional. Defaults to false.
 * @returns {boolean}
 */
  function isGregorian(calendar, ignoreExpiredCheck) {

    if(ObjectUtils.isNullOrUndefined(ignoreExpiredCheck)) {
      ignoreExpiredCheck = false;
    }

    if(ObjectUtils.isNullOrUndefined(calendar)) {
      return true;
    }

    if(ObjectUtils.isNullOrUndefinedOrEmpty(calendar.years)) {
      return true;
    }

    if(!ignoreExpiredCheck && currentCalendarHasExpired()) {
      return true;
    }

    return false;
  }

/**
 * Gets the last day of the last year of the current calendar
 * Private function
 *
 * @returns {object} a momentJs DateTime object
 */
  function getCurrentCalendarLatestDate() {
    var currentSettings = getActiveCalendarSettings();

    var latestYearDefinition = _.max(currentSettings.years, function(yearDefinition) {
      return yearDefinition.year;
    });

    var yearEnd = moment(latestYearDefinition.start_date);

    _.each(latestYearDefinition.month_mask, function(monthMask) {
      yearEnd.add(monthMask, 'weeks');
    });

    return yearEnd;
  }

/** Gets application wide default calendar. Should only be used as a last ditch.
 *  Private Function
 *  
 *  @returns {number} The app wide default calendar Id
 **/
  function getAppDefaultCalendarId() {
    return 1;
  }

/** Safely checks if the passed in user has date format settings
 *  Private Function
 * 
 *  @param {object} user the user to check
 *  @returns {boolean}
 **/
  function currentUserHasDateFormatSetting(user) {
    if(!ObjectUtils.isNullOrUndefined(user) &&
        !ObjectUtils.isNullOrUndefined(user.localization) &&
        !ObjectUtils.isNullOrUndefined(user.localization.date_format) &&
        !ObjectUtils.isNullOrUndefined(user.localization.date_format.mask)) {
      return true;
    } else {
      return false;
    }
  }

/** Safely retrieves the passed in user's date format settings
 *  Private Function
 * 
 *  @param {object} user the user to check
 *  @returns {boolean}
 **/
  function currentOrganizationHasDateFormatSetting(organization) {
    if(!ObjectUtils.isNullOrUndefined(organization) &&
        !ObjectUtils.isNullOrUndefined(organization.localization) &&
        !ObjectUtils.isNullOrUndefined(organization.localization.date_format) &&
        !ObjectUtils.isNullOrUndefined(organization.localization.date_format.mask)) {
      return true;
    } else {
      return false;
    }
  }

/** Gets the organizations number format
 *  Private Function
 * 
 *  @param {object} currentOrganization the org to check
 *  @returns {string} The format key
 **/
  function getOrganizationNumberFormat(currentOrganization){
    return currentOrganization &&
      currentOrganization.localization &&
      currentOrganization.localization.number_format;
  }

/** Gets the users number format
 *  Private Function
 * 
 *  @param {object} currentUser the user to check
 *  @returns {string} The format key
 **/
  function getUserNumberFormat(currentUser){
    return currentUser &&
      currentUser.localization &&
      currentUser.localization.number_format;
  }

/** Retrieves the user's preferences from the activeUser object.
 *  Private Function.
 * 
 *  @param {object} user preferences
 **/
  function getUserPreferences() {
    return activeUser.preferences;
  }

/** Checks if the month in question is the final month of the year
 *  Private function
 * 
 *  @param {monthIndex} number - the month number, zero indexed
 *  @returns {boolean} 
 **/
  function isFinalMonthOfYear(monthIndex) {
    return monthIndex === 11;
  }


  return {
    getLegacyFirstDayOfWeek: getLegacyFirstDayOfWeek,
    getActiveCalendarSettings: getActiveCalendarSettings,
    getActiveCalendar: getActiveCalendar,
    getMonthDefinitions: getMonthDefinitions,
    getFirstDayOfYear: getFirstDayOfYear,
    getFirstMonthOfYear: getFirstMonthOfYear,
    getFirstDayOfMonth: getFirstDayOfMonth,
    getWeekCountOfMonth: getWeekCountOfMonth,
    getWeekNumber: getWeekNumber,
    setOrganization: setOrganization,
    getAllCalendars: getAllCalendars,
    setAllCalendars: setAllCalendars,
    getLastDayOfMonth: getLastDayOfMonth,
    getLastDayOfYear: getLastDayOfYear,
    getCurrentMonth: getCurrentMonth,
    getCurrentYear : getCurrentYear,
    getNumberFormats: getNumberFormats,
    getNumberFormatName: getNumberFormatName,
    getNumberFormatSeparatorsByName: getNumberFormatSeparatorsByName,
    getCurrentNumberFormatName: getCurrentNumberFormatName,
    getFirstDayOfCurrentWeek: getFirstDayOfCurrentWeek,
    getLastDayOfCurrentWeek: getLastDayOfCurrentWeek,
    setUser : setUser,
    getCurrentCalendarFirstDayOfWeek : getCurrentCalendarFirstDayOfWeek,
    getSystemYearForDate : getSystemYearForDate,
    getFirstDayOfWeekInCalendar : getFirstDayOfWeekInCalendar,
    getCurrentDateFormat: getCurrentDateFormat,
    getCurrentLocaleSetting: getCurrentLocaleSetting,
    getWeeksAgo: getWeeksAgo,
    setWeeksAgo: setWeeksAgo,
    getCustomCompareSetting: getCustomCompareSetting,
    setCustomCompareSetting: setCustomCompareSetting,
    getCurrentOrganizationDaysOfWeek: getCurrentOrganizationDaysOfWeek,
    getDaysOfWeek: getDaysOfWeek,
    getStartOfCurrentCalendar: getStartOfCurrentCalendar,
    getEndOfCurrentCalendar: getEndOfCurrentCalendar,
    hasMonthDefinitions: hasMonthDefinitions,
    isGregorian: isGregorian,
    isCurrentCalendarGregorian: isCurrentCalendarGregorian,
    isCalendarIdGregorianSunday: isCalendarIdGregorianSunday,
    isCalendarIdGregorianMonday: isCalendarIdGregorianMonday,
    currentCalendarHasExpired: currentCalendarHasExpired,
    getCurrentCalendarName: getCurrentCalendarName,
    getStandardMonthlyCalendarId: getStandardMonthlyCalendarId,
    getStandardGregorianSundayCalendarId: getStandardGregorianSundayCalendarId,
    getStandardGregorianMondayCalendarId: getStandardGregorianMondayCalendarId
  };
}]);
