'use strict';

angular.module('shopperTrak')
  .factory('NumberUtils',['ObjectUtils', function (ObjectUtils) {
   function getNumberValue(value) {
      if(typeof value === 'string') {
        return Number(value);
      }

      return value;
    }

    function isValidNonZeroNumber(value) {
      return !ObjectUtils.isNullOrUndefinedOrBlank(value) &&
        !isNaN(value) && isFinite(value) && value !== 0;
    }

    function isValidNumber(value) {
      return !ObjectUtils.isNullOrUndefinedOrBlank(value) &&
        !isNaN(value) && isFinite(value);
    }

    return {
      getNumberValue: getNumberValue,
      isValidNonZeroNumber: isValidNonZeroNumber,
      isValidNumber: isValidNumber
    };
  }]);
