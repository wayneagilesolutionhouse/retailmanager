(function() {
  'use strict';

  angular
  .module('shopperTrak')
  .factory('ZoneHelperService', ZoneHelperService);

  ZoneHelperService.$inject = ['ObjectUtils'];

    function ZoneHelperService(ObjectUtils) {

      function removeLeadingX(targetName){
	      if(!ObjectUtils.isNullOrUndefinedOrEmptyObject(targetName)) {
	        var firstCharacter = targetName.charAt(0);
	        if (firstCharacter === 'x' && firstCharacter.toLowerCase()) {
	          targetName = targetName.substring(1);
	          return targetName;
	        } else {
	        	return targetName;
	        }
	      } else {
          return null;
        }
	    }
      
	    return {
	    	removeLeadingX : removeLeadingX 
	    };
    }
})();