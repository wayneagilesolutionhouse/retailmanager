(function() {
  'use strict';

  angular.module('shopperTrak')
  .factory('MallCheckService', [
      'ObjectUtils',
    function (
      ObjectUtils
    ) {

    var exclusionList = ['mall'];

    function isNotMall(orgOrSite, zone) {

      if (ObjectUtils.isNullOrUndefined(orgOrSite)) {
        return false;
      }

      if('portal_settings' in orgOrSite) {
        //organisation
        if(ObjectUtils.isNullOrUndefined(orgOrSite.portal_settings)) {
          return false;
         }

         var organization_type = orgOrSite.portal_settings.organization_type;

         if(ObjectUtils.isNullOrUndefined(organization_type)) {
           return false;
          } else
          {
            return !isOrgExcluded(organization_type);
          }

      } else if('type' in orgOrSite) {
        //site
        //assuming that sites have zones
        if(!ObjectUtils.isNullOrUndefined(zone) && !ObjectUtils.isNullOrUndefined(zone.type)) {
          var zoneType = zone.type.toLowerCase();
          if(zoneType === 'tenantcommon') {
            return true;
          }
        }

        if(ObjectUtils.isNullOrUndefined(orgOrSite.type)) {
          return false;
         } else {
           return !isOrgExcluded(orgOrSite.type);
         }

      } else {
        return false;
      }

    }

    function isOrgExcluded(type) {
      //view not allowed for mall so far...
      var orgType = type.toLowerCase();
      var index = exclusionList.indexOf(orgType);
      return index > -1;
    }

    return {
      isNotMall: isNotMall
    };

  }]);
})();
