'use strict';

angular.module('shopperTrak')
.factory('ExportService', [
  '$rootScope',
  'localStorageService',
  '$q',
  'apiUrl',
  '$http',
  '$window',
  '$location',
  '$filter',
  'session',
  'ObjectUtils',
  'authService',
  'generatePdf',
  'googleAnalytics',
  'widgetConstants',
function (
  $rootScope,
  localStorageService,
  $q,
  apiUrl,
  $http,
  $window,
  $location,
  $filter,
  session,
  ObjectUtils,
  authService,
  generatePdf,
  googleAnalytics,
  widgetConstants
) {

  var numExportsInProgress = 0;
  var joinLocationIds, addToExportCart, buildAreaKey, buildDateRangeKey, storeExportCart, sort;
  var exportedCarts = localStorageService.get('exportedCarts');
  var exportCart = localStorageService.get('exportCart');

  var widgetProperties = widgetConstants.exportProperties;

  var clearExportCart = function() {
    exportCart = { };
    localStorageService.set('exportCart', exportCart);
  };

  var deletePreviousExports = function() {
    exportedCarts = [];
    localStorageService.set('exportedCarts', exportedCarts);
  };

  if(!exportCart) {
    clearExportCart();
  }

  if(!exportedCarts) {
    deletePreviousExports();
  }

  var createExportAndStore = function(parameter) {
    var reducedParameter = {},
        dateRange =  parameter.dateRange,
        compare1Range = parameter.compare1Range,
        compare2Range = parameter.compare2Range,
        locationIds = parameter.locationIds,
        locationId = parameter.locationId,
        areaKey;

    Object.keys(parameter).filter(function(key) {
      return key !== 'orgId' &&
      key !== 'siteId' &&
      key !== 'locationId' &&
      key !== 'zoneId' &&
      key !== 'dateRange' &&
      key !== 'locationIds' &&
      key !== 'compare1Range' &&
      key !== 'compare2Range';
    }).map(function(key) {
      reducedParameter[key] = parameter[key];
    });

     if(!ObjectUtils.isNullOrUndefinedOrEmpty( locationIds) ) {
      var joinedLocationIds = joinLocationIds (locationIds);
      areaKey = buildAreaKey(parameter.orgId, parameter.siteId, joinedLocationIds, parameter.zoneId, parameter.selectedTags, parameter.areaKey);
      addToExportCart(exportCart, areaKey, dateRange, compare1Range, compare2Range, reducedParameter, parameter.dateRangeKey);
      storeExportCart(exportCart);
    } else {
      areaKey = buildAreaKey(parameter.orgId, parameter.siteId, locationId, parameter.zoneId, parameter.selectedTags, parameter.areaKey);
      addToExportCart(exportCart, areaKey, dateRange, compare1Range, compare2Range, reducedParameter, parameter.dateRangeKey);
      storeExportCart(exportCart);
    }
 };

  joinLocationIds = function (locationIds){
    var joinedLocations;

    if(locationIds !== undefined){
      joinedLocations = locationIds.join(',');
    }
    return joinedLocations;
   };


  var addToExportCartAndStore = function(areaKey, dateRange, compare1Range, compare2Range, metricKey) {
    addToExportCart(exportCart, areaKey, dateRange, compare1Range, compare2Range, metricKey);

    storeExportCart(exportCart);
 };

  addToExportCart = function(cart, areaKey, dateRange, compare1Range, compare2Range, parameter, dateRangeKey) {
    var name = parameter;
    if (!parameter.name) {
      parameter = {
        name: name
      };
    }
    if (!cart[areaKey]) {
      cart[areaKey] = {};
    }
    if(ObjectUtils.isNullOrUndefined(dateRangeKey)) {
      dateRangeKey = buildDateRangeKey(dateRange, compare1Range, compare2Range);
    }

    if (!cart[areaKey][dateRangeKey]) {
      cart[areaKey][dateRangeKey] = {
        start: dateRange.start,
        end: dateRange.end,
        compare1Start: compare1Range.start,
        compare1End: compare1Range.end,
        compare2Start: compare2Range.start,
        compare2End: compare2Range.end,
        metrics: [],
        groupBy: null
      };
    }

    var metricKey = parameter.name;
    if(!ObjectUtils.isNullOrUndefined(parameter.partialPageName)) {
      var changingWidgetSettings = widgetProperties[parameter.partialPageName];
    } else {
      var changingWidgetSettings = widgetProperties[metricKey];
    }
    var exists = isInExportCartWithSettings(areaKey, dateRangeKey, metricKey, parameter, changingWidgetSettings);

    if (!exists) {
      var length = cart[areaKey][dateRangeKey].metrics.length;
      parameter.index = length;

      cart[areaKey][dateRangeKey].metrics.push(parameter);
    }

    // Slashes break down the export URLs

    if (parameter.dateFormat) {
      parameter.dateFormat = parameter.dateFormat.replace(/\//g,'|');
    }
    if(!ObjectUtils.isNullOrUndefined(parameter.currentUser) &&
      !ObjectUtils.isNullOrUndefined(parameter.currentUser.localization) &&
      !ObjectUtils.isNullOrUndefined(parameter.currentUser.localization.date_format) &&
      !ObjectUtils.isNullOrUndefined(parameter.currentUser.localization.date_format.mask)
    ) {
      parameter.currentUser.localization.date_format.mask = parameter.currentUser.localization.date_format.mask.replace(/\//g,'|');
    }

    if(!ObjectUtils.isNullOrUndefined(parameter.currentOrganization) &&
      !ObjectUtils.isNullOrUndefined(parameter.currentOrganization.localization) &&
      !ObjectUtils.isNullOrUndefined(parameter.currentOrganization.localization.date_format) &&
      !ObjectUtils.isNullOrUndefined(parameter.currentOrganization.localization.date_format.mask)
    ) {
      parameter.currentOrganization.localization.date_format.mask = parameter.currentOrganization.localization.date_format.mask.replace(/\//g,'|');
    }

    return cart;
  };

  buildAreaKey = function buildAreaKey(orgId, siteId, locationId, zoneId, tags, key) {
    if(!ObjectUtils.isNullOrUndefinedOrBlank(key)){
      return key;
    }
    var areaKey = orgId;
    areaKey += '_' + getId(siteId);
    if (locationId) {
      areaKey += '_location_' + getId(locationId);
    } else if (zoneId) {
      areaKey += '_zone_' + getId(zoneId);
    } else if (tags && tags.length > 0) {
      areaKey += '_tags_';
      _.each(tags, function(tag) {
        areaKey += tag;
      });
    }
    return areaKey;
  };

  function getId(id){
    if(ObjectUtils.isNullOrUndefined(id)){
      return -1;
    }
    return id;
  }

  buildDateRangeKey = function(dateRange, compare1Range, compare2Range) {
    if(ObjectUtils.isNullOrUndefined(dateRange)){
      return;
    }
    return dateRange.start +
           ' - ' +
           dateRange.end +
           ' - ' +
           compare1Range.start +
           ' - ' +
           compare1Range.end +
          ' - ' +
          compare2Range.start +
          ' - ' +
          compare2Range.end;
  };

  storeExportCart = function(exportCart) {
    localStorageService.set('exportCart', exportCart);
  };

  var removeFromExportCart = function(areaKey, dateRangeKey, metricKey) {
    if (metricKey.name) {
      metricKey = metricKey.name;
    }

    var metricFound = exportCart[areaKey][dateRangeKey].metrics.some(function(metric) {
      return metric.name === metricKey;
    });

    if (exportCart[areaKey] && exportCart[areaKey][dateRangeKey] && metricFound) {

      var index = -1;
      exportCart[areaKey][dateRangeKey].metrics.some(function(metric, i) {
        if (metric.name === metricKey) {
          index = i;
        }
      });

      if (index > -1) {
        delete exportCart[areaKey][dateRangeKey].metrics[index];

        //For some reason delete in this case doesn't adjust array length
        //so that's the clue with newMetrics
        var newMetrics = [];
        angular.forEach(exportCart[areaKey][dateRangeKey].metrics, function(metric) {
          newMetrics.push(metric);
        });
        exportCart[areaKey][dateRangeKey].metrics = newMetrics;
      }

      if (exportCart[areaKey][dateRangeKey].metrics.length === 0) {
        delete exportCart[areaKey][dateRangeKey];
        if (Object.keys(exportCart[areaKey]).length === 0) {
          delete exportCart[areaKey];
        }
      }

      localStorageService.set('exportCart', exportCart);
    }
  }

  function checkExtraParametersInCart(extraParams, cartMetric) {
    if(ObjectUtils.isNullOrUndefined(extraParams)) {
      return true;
    }
    var matches = true;

    _.each(Object.keys(extraParams), function(key) {
      if (cartMetric[key] !== extraParams[key]) {
        matches = false;
      }
    });
    return matches;
  }

  function isInExportCart(areaKey, dateRangeKey, metricKey, groupBy, extraParams) {
    if (exportCart[areaKey] && exportCart[areaKey][dateRangeKey]) {
      var found;

      if (groupBy) {
        found = exportCart[areaKey][dateRangeKey].metrics.some(function(metric) {
          return metric.name === metricKey && metric.groupBy === groupBy;
        })
      } else {
        found = exportCart[areaKey][dateRangeKey].metrics.some(function(metric) {
          return metric.name === metricKey && checkExtraParametersInCart(extraParams, metric);
        });
      }

      return exportCart[areaKey] &&
        exportCart[areaKey][dateRangeKey] &&
        found;
    }
    return false;
  };

  function isInExportCartWithSettings(areaKey, dateRangeKey, metricKey, params, paramsToCompare) {
    var found, checkCartData;

    var checkParams = filterParams(angular.copy(params), paramsToCompare);

    if (exportCart[areaKey] && exportCart[areaKey][dateRangeKey]) {
      found = exportCart[areaKey][dateRangeKey].metrics.some(function(data) {
        checkCartData = filterParams(angular.copy(data), paramsToCompare);
        return metricKey === data.name && angular.equals(checkCartData, checkParams);
      });
      return exportCart[areaKey] && exportCart[areaKey][dateRangeKey] && found;
    }

    return false;
  }

  function filterParams(params, paramsToCompare) {
    if(ObjectUtils.isNullOrUndefined(params)) {
      return params;
    }
    var compareParams = [];

    _.each(paramsToCompare, function(param) {
      compareParams.push(params[param]);
    });

    return compareParams;
  }

  /** Checks to see if a widget is in the export cart. Public function
   *  Does the legwork of building the various keys needed to check the export
   *
   *  @param {object} widget - The widget to check for
   */
  function isInExportCartSimple(widget, extraParams) {

    if(typeof widget.locationIds === 'undefined' && widget.locationId) {
      widget.locationIds = [];
      widget.locationIds.push(widget.locationId);
    }

    var joinedLocationIds = joinLocationIds(widget.locationIds);

    var areaKey = buildAreaKey(widget.orgId, widget.siteId, joinedLocationIds, widget.zoneId, widget.selectedTags, widget.areaKey);

    if(typeof widget.compare1Range === 'undefined' && typeof widget.compareRange1 !== 'undefined') {
      widget.compare1Range = widget.compareRange1;
    }

    if(typeof widget.compare2Range === 'undefined' && typeof widget.compareRange2 !== 'undefined') {
      widget.compare2Range = widget.compareRange2;
    }

    var dateRangeKey = buildDateRangeKey(widget.dateRange, widget.compare1Range, widget.compare2Range);

    return isInExportCart(areaKey, dateRangeKey, widget.name, extraParams);
  }

  function getCart() {
    return exportCart;
  }

  function getExportedCarts() {
    return exportedCarts;
  }

  function cartHasBeenExported(cart) {
    return exportedCarts.indexOf(cart) === -1;
  }

  function dotSeparation(locationId) {
    var locationIdsArray = [];
    if (locationId !== undefined) {
      var separatedList = locationId.split(',');
      angular.forEach(separatedList, function(key) {
        locationIdsArray.push(key);
      });
    }
    return locationIdsArray;
  }

  function buildMetricListFromExportCart(cart, fromSchedule) {
    var widgets = [];
    var locationIdsArray = [];

      angular.forEach(cart, function(dateRangeGroups, areaKey) {
        var locationId;
        var zoneId;
        var splitAreaKey = areaKey.split('_');
        var orgId = splitAreaKey[0];
        var siteId = setId(splitAreaKey[1]);
        var idType  = splitAreaKey[2];

       if(idType === 'location'){
          locationId = setId(splitAreaKey[3]);
       } else if (idType === 'zone'){
          zoneId = setId(splitAreaKey[3]);
       }

        locationIdsArray = dotSeparation(locationId);

        if (locationIdsArray.length > 1){
          locationId = undefined;
        }

        angular.forEach(dateRangeGroups, function(dateRangeGroup) {

          angular.forEach(dateRangeGroup.metrics, function(metric) {
            var locationTypes = [];
            angular.forEach(metric.location_type, function(type) {
              locationTypes.push(type);
            });
            var parameter = {
              'partialPageName': metric.partialPageName,
              'pageName': metric.pageName,
              'organizationId': orgId,
              'siteId': siteId,
              'locationId': locationId,
              'zoneId': zoneId,
              'dateRange': {'start': dateRangeGroup.start, 'end': dateRangeGroup.end},
              'compare1Range': {'start': dateRangeGroup.compare1Start, 'end': dateRangeGroup.compare1End},
              'compare2Range': {'start': dateRangeGroup.compare2Start, 'end': dateRangeGroup.compare2End},
              'summaryKey': getSummaryKey(metric),
              'locationType': locationTypes
            };

            if (locationIdsArray.length > 1 ){
                parameter.locationIds = locationIdsArray;
            }


            if (fromSchedule !== true) {
              angular.forEach(Object.keys(metric), function(key) {
                if (key !== 'name'
                  && key !== 'location_type'
                  && key !== 'index'
                  && key !== 'currentUser'
                  && key !== 'compare'
                  && key !== 'dateRangeShortCut'
                  && key !== 'customRange'
                  && key !== 'xDaysBack'
                  && key !== 'xDaysDuration'
                  && key !== '$$hashKey'
                  && key !== 'dateFormat'
                  && key !== 'firstDayOfWeekSetting'
                  && key !== 'language'
                  && key !== 'currencySymbol'
                  && key !== 'showWeatherMetrics'
                  && key !== 'selectedWeatherMetrics'
                  ) {
                  parameter[key] = metric[key];
                }
              });
            } else {
              angular.forEach(Object.keys(metric), function(key) {
                if (key !== 'name'
                  && key !== 'location_type'
                  && key !== 'index'
                  && key !== 'currentUser'
                  && key !== '$$hashKey'
                  && key !== 'dateFormat'
                  && key !== 'firstDayOfWeekSetting'
                  && key !== 'language'
                  && key !== 'currencySymbol'
                  ) {
                  parameter[key] = metric[key];
                }
              });
            }

            widgets.push(parameter);
          });
        });
      });

      return widgets;
  };

  function getSummaryKey(metric) {
     if(!ObjectUtils.isNullOrUndefinedOrBlank(metric.summaryKey)){
       return metric.summaryKey;
     }

     return  metric.name;
  }

  function setId(key){
    if(!ObjectUtils.isNullOrUndefined(key) && key < 0){
      return undefined;
    }
    return key;
  }

  var buildPdfUrl = function (widgets, fromSchedule) {
    var deferred = $q.defer();

    authService
    .getCurrentUser()
    .then(function (data) {
      var widgetData = {
        userId: data._id,
        widgets: widgets
      };

      _.each(widgetData.widgets, function(widget){
        if(fromSchedule && !ObjectUtils.isNullOrUndefined(widget.compare)){
          widget.compare.chart_name = $filter('replaceSpecialChars')(widget.compare.chart_name);
        }
        if (widget.compareSites) delete widget.compareSites;
      });

      var url = $location.absUrl().split('#')[0] + '#/pdf/';

      var encodedWidgetData = encodeURIComponent(JSON.stringify(widgetData));

      var retVal = {
        fullPdfUrl: encodeURIComponent(url) + encodedWidgetData,
        basePdfUrl: url,
        encodedWidgetData: encodedWidgetData
      };

      deferred.resolve(retVal);
    }, function () {
      console.log('error');
      deferred.reject();
    });

    return deferred.promise;
  }

  function exportCartToPdf(cart) {
    googleAnalytics.trackUserEvent('pdf', 'generate');
    var widgets = buildMetricListFromExportCart(cart);

    numExportsInProgress++;

    buildPdfUrl(widgets).then(function (pdfInfo) {

      sort(widgets);

      $rootScope.$broadcast('pdfExportStart', cart);

      if(generatePdf === true) {
        var request = buildPdfRequest(pdfInfo.fullPdfUrl);

        $http(request)
        .then(function (response) {
          turnResponseIntoPdf(response);

          numExportsInProgress--;

          onExportFinish(cart, true);
        }, function () {
          numExportsInProgress--;

          onExportFinish(cart, false);
        });
      } else {
        var url = pdfInfo.basePdfUrl + pdfInfo.encodedWidgetData;

        $window.open(url);

        numExportsInProgress--;

        onExportFinish(cart, true);
      }

      if (cartHasBeenExported(cart)) {
        exportedCarts.unshift(cart);
        localStorageService.set('exportedCarts', exportedCarts);
      }
    });
  }

  function buildPdfRequest(builtPdfUrl) {
    return {
      method: 'GET',
      url: apiUrl + '/pdf?url=' + builtPdfUrl,
      headers: {
        'Accept': 'application/pdf',
        'Authorization': session.getToken()
      },
      responseType: 'arraybuffer'
    };
  }

  function onExportFinish(cart, successful) {
    var exportDetails = {
      cart: cart,
      success: successful
    };

    $rootScope.$broadcast('pdfExportFinish', exportDetails);
  }

  function turnResponseIntoPdf(response) {
    var file = new Blob([response.data], { type: 'application/pdf' });
    var fileURL = URL.createObjectURL(file);

    var downloadAttrSupported = ('download' in document.createElement('a'));

    // Unique and human-readable (more or less) filename
    var filename = moment().format('YYYY-MM-DD_HH-mm-ss') + '_retailmanager.pdf';

    if (downloadAttrSupported) {
      var a = document.createElement('a');
      document.body.appendChild(a);
      a.style.display = 'none';
      a.href = fileURL;
      a.download = filename;
      a.click();
    } else {
      saveAs(file, filename);
    }
  }

  function exportCurrentCartToPdf() {
    exportCartToPdf(exportCart);
  }

  function sort(widgets) {
    widgets.sort(function(a, b) {
      return a.index - b.index;
    });
  }

  function getCartItemCount() {
    var cartItemCount = 0;
    for (var areaKey in exportCart) {
      for (var dateRangeKey in exportCart[areaKey]) {
        cartItemCount += exportCart[areaKey][dateRangeKey].metrics.length;
      }
    }
    return cartItemCount;
  }

  function getNumExportsInProgress() {
    return numExportsInProgress;
  }

  function getOrgIdFrom(cart) {
    var areaKeys = Object.keys(cart);
    var orgIds = areaKeys.map(function(areaKey) {
      return areaKey.split('_')[0];
    });
    //TODO: Currently returns first orgId; can there be multiple?
    return orgIds[0];
  }

  return {
    addToExportCart: addToExportCart,
    addToExportCartAndStore: addToExportCartAndStore,
    removeFromExportCart: removeFromExportCart,
    isInExportCart: isInExportCart,
    isInExportCartWithSettings: isInExportCartWithSettings,
    clearExportCart: clearExportCart,
    getCart: getCart,
    getExportedCarts: getExportedCarts,
    exportCartToPdf: exportCartToPdf,
    exportCurrentCartToPdf: exportCurrentCartToPdf,
    deletePreviousExports: deletePreviousExports,
    getCartItemCount: getCartItemCount,
    getNumExportsInProgress: getNumExportsInProgress,
    buildDateRangeKey: buildDateRangeKey,
    buildAreaKey: buildAreaKey,
    sort: sort,
    buildMetricListFromExportCart: buildMetricListFromExportCart,
    createExportAndStore: createExportAndStore,
    buildPdfUrl: buildPdfUrl,
    getOrgIdFrom: getOrgIdFrom,
    isInExportCartSimple: isInExportCartSimple
  };
}]);
