(function() {
  'use strict';

  angular.module('shopperTrak')
  .factory('googleAnalytics', [
    '$window',
    'ObjectUtils',
    function (
      $window,
      ObjectUtils
  ) {

    var googleAnalyticsNotLoadedErrorMsg = 'Google Analytics is not loaded';
    var hasSentUserId = false;

  /** Checks if the google analytics object is available.
   *  Private function.
   **/
    function googleAnalyticsIsLoaded() {
      return !ObjectUtils.isNullOrUndefined($window.ga);
    }

  /** Sends a page view to google analytics. Should be called when a state change occurs
   *  Public function.
   *
   *  @param {string} route - An identifier for the new route
   **/
    function sendPageView(route) {
      if(!googleAnalyticsIsLoaded()) {
        throw new Error(googleAnalyticsNotLoadedErrorMsg);
      }

      if(ObjectUtils.isNullOrUndefinedOrBlank(route)) {
        throw new Error('Route name is required');
      }

      $window.ga('set', 'page', route);
      $window.ga('send', 'pageview');
    }


      /***
       * Send a user event to Google Analytics
       * @param {string} category
       * @param {string} action
       */
    function trackUserEvent(category, action) {
      if (!googleAnalyticsIsLoaded()) {
        throw new Error(googleAnalyticsNotLoadedErrorMsg);
      }

      if (ObjectUtils.isNullOrUndefined(category)) {
        throw new Error('Category is required');
      }

      if (ObjectUtils.isNullOrUndefined(action)) {
        throw new Error('Action is required');
      }

      $window.ga('send', {
        hitType: 'event',
        eventCategory: category,
        eventAction: action,
        eventLabel: 'User Action',
        eventValue: 1
      });
    }


  /** Sends the time an API request took to respond to google analytics
   *  Public function.
   *
   *  @param {string} apiUri - A resource identifier for the route (e.g. /kpis/traffic)
   *  @param {string} apiAddress - The full address, including query string params.
   *  @param {number} requestTime - The time, in milliseconds the request took. Must be an integer (no decimals)
   **/
    function sendRequestTime(apiUri, apiAddress, requestTime) {
      if(!googleAnalyticsIsLoaded()) {
        throw new Error(googleAnalyticsNotLoadedErrorMsg);
      }

      if (ObjectUtils.isNullOrUndefinedOrBlank(apiUri)) {
        throw new Error('apiUri is required');
      }

      if (ObjectUtils.isNullOrUndefinedOrBlank(apiAddress)) {
        throw new Error('apiAddress is required');
      }

      if (!angular.isNumber(requestTime)) {
        throw new Error('requestTime must be a number');
      }

      $window.ga('send', {
        hitType: 'event',
        eventCategory: apiUri,
        eventAction: apiAddress,
        eventLabel: 'API Request Time',
        eventValue: requestTime
      });
    }

  /** Sends the specified userId to Google Analytics
   *  Public function.
   *
   *  @param {string} userId - An identifier for the current user
   **/
    function setUserId(userId) {
      if(hasSentUserId) {
        return;
      }

      if(!googleAnalyticsIsLoaded()) {
        throw new Error(googleAnalyticsNotLoadedErrorMsg);
      }

      if(!angular.isString(userId)) {
        throw new Error('UserId must be a string');
      }

      if(ObjectUtils.isNullOrUndefinedOrBlank(userId)) {
        throw new Error('UserId is required')
      }

      $window.ga('set', 'userId', userId);

      hasSentUserId = true;
    }

    function resetUserIdSentStatus() {
      hasSentUserId = false;
    }

    return {
      trackUserEvent: trackUserEvent,
      sendPageView: sendPageView,
      setUserId: setUserId,
      resetUserIdSentStatus: resetUserIdSentStatus,
      sendRequestTime: sendRequestTime
    };

  }]);
})();
