(function() {
  'use strict';

  angular.module('shopperTrak').factory('metricsHelper', ['metricConstants', 'ObjectUtils','NumberUtils', metricsHelper]);

  function metricsHelper(
    metricConstants,
    ObjectUtils,
    NumberUtils
  ) {

    var defaultMetricLookup = [{   
      key: 'traffic',
      apiProperty: 'traffic',
      apiReturnkey: 'traffic',
      transkey: 'kpis.shortKpiTitles.tenant_traffic',
      }, {
      key: 'sales',
      apiProperty: 'sales',
      apiReturnkey: 'sales',
      transkey: 'kpis.shortKpiTitles.tenant_sales',
      }, {
      key: 'conversion',
      apiProperty: 'conversion',
      apiReturnkey: 'conversion',
      transkey: 'kpis.shortKpiTitles.tenant_conversion',
      calculatedMetric: true,
      dependencies: [
        'traffic',
        'transactions'
      ]}, {
      key: 'star',
      apiProperty: 'star',
      apiReturnkey: 'star',
      transke: 'kpis.shortKpiTitles.tenant_star',
      calculatedMetric: true,
      dependencies: [
        'traffic',
        'labor_hours'
      ]},{
        key: 'ats',
        apiProperty: 'ats',
        apiReturnkey: 'ats',
        transkey: 'kpis.kpiTitle.ats',
        calculatedMetric: true,
        dependencies: [
          'sales',
          'transactions'
        ]}, {
      key: 'labor',
      apiProperty: 'labor_hours',
      apiReturnkey: 'labor_hours',
      transkey: 'kpis.shortKpiTitles.tenant_labor',
     }, {
      key: 'transaction',
      apiProperty: 'transactions',
      apiReturnkey: 'transactions',
      transkey: 'kpis.shortKpiTitles.tenant_transactions'
    }];

    function getTotalForMetric(metric, tableData, enterExits) {
      var total;
      if(ObjectUtils.isNullOrUndefinedOrEmpty(tableData)) {
        return total;
      }
      var count = 0 ;
      _.each(tableData, function(row) {
        if(isCalculatedMetricValueValid(row, metric, enterExits)) {
          if (ObjectUtils.isNullOrUndefined(total)) {
            total = 0;
          }
          count += 1;
          total += getCalculatedMetricValue(row, metric, enterExits);
        }
        
      });

      if ( metric.calculatedMetric === true && count > 0 ) {
        total = total/count;
      }

      return total;
    }

    function filterSubscriptions(obj, activeSubscriptions, realTime) {
      if(realTime === true) {
        return filterRealTimeSubscriptions(obj, activeSubscriptions);
      }

      return _.filter(obj, function(subClass) {
        return (subClass.subscription === 'any' ||
          _.contains(activeSubscriptions, subClass.subscription))&&
          hasRequiredPermissions (subClass.requiredPermissions, activeSubscriptions);
      });
    }

    function filterRealTimeSubscriptions(obj, activeSubscriptions) {
      return _.filter(obj, function(subClass) {
        return !ObjectUtils.isNullOrUndefinedOrBlank(subClass.realTimeSubscription) &&
          _.contains(activeSubscriptions, subClass.realTimeSubscription)&&
          hasRequiredPermissions (subClass.realTimeRequiredPermissions, activeSubscriptions);
      });
    }

    function hasRequiredPermissions (requiredPermissions, activeSubscriptions) {
      if(ObjectUtils.isNullOrUndefinedOrEmpty(requiredPermissions)) {
        return true;
      }

      var hasRequiredPermission = true;
       _.each(requiredPermissions, function(permission) {
        if(!_.contains(activeSubscriptions, permission)) {
          hasRequiredPermission = false;
          return false;
        }
      });
      return hasRequiredPermission;
    }

    function getMetricDisplayInformation(activeSubscriptions, metricLookup, realTime) {
      if (ObjectUtils.isNullOrUndefinedOrEmpty(metricLookup)) {
        metricLookup = defaultMetricLookup;
      }
      var metricDisplayInfo = [];

      var metricsWithSupscriptios = filterSubscriptions(metricConstants.metrics, activeSubscriptions, realTime);
      _.each(metricsWithSupscriptios, function(metric) {
        addMetricDisplayInfo(metricDisplayInfo, metric, metricLookup);
      });

      return metricDisplayInfo;
    }

    function addMetricDisplayInfo(metricDisplayInfo, metric, metricLookup) {
      var info = getUpdatedMetric(metric, metricLookup);
      if (!ObjectUtils.isNullOrUndefined(info)) {
        metricDisplayInfo.push(info);
      }
    }

    function getUpdatedMetric(metricInfo, metricLookup) {
      var metricProperty = getMetricProperty(metricInfo, metricLookup);
      if (!ObjectUtils.isNullOrUndefined(metricProperty)) {
        metricInfo.apiPropertyName = metricProperty.apiProperty;
        metricInfo.apiReturnkey = metricProperty.apiReturnkey;
        metricInfo.calculatedMetric = metricProperty.calculatedMetric;
        metricInfo.dependencies = metricProperty.dependencies;
        metricInfo.key = metricProperty.key;
      } else {
        metricInfo.apiPropertyName = metricInfo.value;
        metricInfo.apiReturnkey = metricInfo.value;
        metricInfo.calculatedMetric = false;
        metricInfo.dependencies = [];
        metricInfo.key = metricInfo.value;
      }

      return metricInfo;
    }

    function getApiParameterKey(value, enterExits) {
      if (!ObjectUtils.isNullOrUndefinedOrBlank(value)) {
        var val = value.toLowerCase();
        var enters = val.indexOf('enter') > -1;
        var exits = val.indexOf('exit') > -1;
        if (enters === true || exits === true) {
          return enterExits.toLowerCase();
        }
      }

      return value;
    }

    function getCalculatedMetricValue(item, metric, enterExits) {
      if (metric.calculatedMetric !== true) {
        return getMetricValue(item, getApiParameterKey(metric.apiReturnkey, enterExits));
      }

      switch (metric.key) {
        case 'conversion':
          return getConversion(getMetricValue(item, getApiParameterKey(metric.dependencies[0], enterExits)), getMetricValue(item, getApiParameterKey(metric.dependencies[1], enterExits)));
        case 'star':
          return getStar(getMetricValue(item, getApiParameterKey(metric.dependencies[0], enterExits)), getMetricValue(item, getApiParameterKey(metric.dependencies[1], enterExits)));
        case 'ats':
          return getAts(getMetricValue(item, getApiParameterKey(metric.dependencies[0], enterExits)), getMetricValue(item, getApiParameterKey(metric.dependencies[1], enterExits)));
        default:
          return getMetricValue(item, getApiParameterKey(metric.apiReturnkey, enterExits));
      }
    }

    function getRoundedNumber(value, decimalPlaces) {
      decimalPlaces = decimalPlaces || 0;
      return Number(Math.round(value+'e'+decimalPlaces)+'e-'+decimalPlaces);
    }

    function getDecimalPlacesLength(value) {
      var match = (''+value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/); // https://regex101.com/r/cSOSzK/1
      if (!match) { return 0; }
      return Math.max(
        0,
        // Number of digits right of decimal point.
        (match[1] ? match[1].length : 0) -
        // Adjust for scientific notation.
        (match[2] ? +match[2] : 0));
    }

    function getMetricTranskey(apiProperty, metricApiLookup) {
      return _.findWhere(metricApiLookup, {
        apiProperty: apiProperty
      }).transkey;
    }

    function getMetricApiProperty(shortTranslationLabel, metricApiLookup) {
      return _.find(metricApiLookup, function(item){
        return item.transkey === shortTranslationLabel || item.shortTranslationLabel === shortTranslationLabel;
      }).apiProperty;
    }

    function getMetricChartLocation(shortTranslationLabel, metricApiLookup) {
      return _.find(metricApiLookup, function(item){
        return item.transkey === shortTranslationLabel || item.shortTranslationLabel === shortTranslationLabel;
      }).chartLocation;
    }

    function getValidNumber(value) {
      if(!ObjectUtils.isNullOrUndefinedOrBlank(value) &&
        _.isString(value)) {
        value = Number(value);
        return _.isNumber(value) && !_.isNaN(value) && isFinite(value) ? value : 0;
      }
      return _.isNumber(value) && !_.isNaN(value) && isFinite(value) ? value : 0;
    }

    function isCalculatedMetricValueValid(item, metric, enterExits) {
      if(ObjectUtils.isNullOrUndefined(item)) {
        return false;
      }
      
      if (metric.calculatedMetric !== true) {
        return isMetricValueValidNumber(item, metric.apiReturnkey, enterExits);
      }

      switch (metric.key) {
        case 'conversion':
          return isMetricValueValidNumber(item, metric.dependencies[0], enterExits) &&
                 isMetricValueValidNonZeroNumber(item, metric.dependencies[1], enterExits);
        case 'star':
          return isMetricValueValidNumber(item, metric.dependencies[0], enterExits) &&
                 isMetricValueValidNonZeroNumber(item, metric.dependencies[1], enterExits);
        case 'ats':
          return isMetricValueValidNumber(item, metric.dependencies[0], enterExits) &&
                 isMetricValueValidNonZeroNumber(item, metric.dependencies[1], enterExits);
        default:
          return isMetricValueValidNumber(item, metric.apiReturnkey, enterExits);
      }
    }

    function isMetricValueValidNumber(item, apiPropertyKey, enterExits) {
      var apiParameterKey = getApiParameterKey(apiPropertyKey, enterExits);
      return NumberUtils.isValidNumber(item[apiParameterKey]);
    }

    function isMetricValueValidNonZeroNumber(item, apiPropertyKey, enterExits) {
      var apiParameterKey = getApiParameterKey(apiPropertyKey, enterExits);
      return NumberUtils.isValidNonZeroNumber(item[apiParameterKey]);
    }

    function getStar(traffic, labor) {
      return getValidNumber(Math.round(traffic / labor));
    }

    function getAts(sales, transaction) {
      return getValidNumber(sales / transaction);
    }

    function getConversion(traffic, transaction) {
      return getValidNumber((transaction / traffic) * 100);
    }

    function getSps(sales, traffic) {
      return getValidNumber(sales / traffic);
    }

    function getSplh(sales, labor) {
      return getValidNumber(sales / labor);
    }

    function getMetricValue(item, apiPropertyName) {
      if (ObjectUtils.isNullOrUndefined(item) || 
        ObjectUtils.isNullOrUndefinedOrBlank(item[apiPropertyName])) {
        return 0;
      }
      return NumberUtils.getNumberValue(item[apiPropertyName]);

    }

    function getMetricInfo(value, metricLookup) {
      var metricInfo = angular.copy(_.find(metricConstants.metrics, function(item){
        return item.value === value || item.key === value || item.kpi === value;
      }));
      var metricProperty = getMetricProperty(metricInfo, metricLookup);
      if (!ObjectUtils.isNullOrUndefined(metricProperty)) {
        metricInfo.apiPropertyName = metricProperty.apiProperty;
        metricInfo.apiReturnkey = metricProperty.apiReturnkey;
        metricInfo.calculatedMetric = metricProperty.calculatedMetric;
        metricInfo.dependencies = metricProperty.dependencies;
        metricInfo.key = metricProperty.key;
      }
      return metricInfo;
    }

    function getMetricProperty(metric, metricLookup) {
      if (ObjectUtils.isNullOrUndefined(metric)) {
        return;
      }

      return _.find(metricLookup, function(item) {
        return item.value === metric.value || item.key === metric.value || item.kpi === metric.value;
      });
    }

    return {
      getMetricDisplayInformation: getMetricDisplayInformation,
      getConversion: getConversion,
      getAts: getAts,
      getStar: getStar,
      getSplh: getSplh,
      getSps: getSps,
      getCalculatedMetricValue: getCalculatedMetricValue,
      getTotalForMetric: getTotalForMetric,
      getMetricInfo: getMetricInfo,
      getMetricTranskey:getMetricTranskey,
      getMetricApiProperty:getMetricApiProperty,
      getMetricChartLocation:getMetricChartLocation,
      getRoundedNumber: getRoundedNumber,
      getDecimalPlacesLength: getDecimalPlacesLength,
      getValidNumber: getValidNumber,
      getMetricValue: getMetricValue,
      isCalculatedMetricValueValid: isCalculatedMetricValueValid,
      isMetricValueValidNumber: isMetricValueValidNumber
    };
  }
})();
