'use strict';
describe('realTimeIntervalDataWidgetDirective', function(){
  var $scope;
  var $compile;
  var $httpBackend;
  var apiUrl = 'https://api.url';
  var currentUserMock = { localization: { date_format: 'MM/DD/YYYY' } };
  var currentSiteMock;

  beforeEach(module('shopperTrak', function($translateProvider) {
    $translateProvider.translations('en_US', {});
  }));

  beforeEach(module(function ($provide) {
    $provide.constant('apiUrl', apiUrl);

    var googleAnalytics = {
      sendRequestTime: function () { 
        angular.noop()
      }
    };

    $provide.value('googleAnalytics', googleAnalytics);
  }));

  beforeEach(inject(function($rootScope, $templateCache, _$compile_,
    _$httpBackend_) {
    $scope = $rootScope.$new();
    $compile = _$compile_;
    $scope.currentUser = currentUserMock;
    $httpBackend = _$httpBackend_;
    var authUser = { result: [currentUserMock] };
    initCurrentOrg();
    $scope.$apply();
    setInitialBackup();
    cacheTemplates($templateCache);
    currentSiteMock = { organization_id: 5777 };

    $httpBackend.whenGET('l10n/languages/en_US.json').respond({});
    $httpBackend.whenGET('https://api.url/auth/currentUser').respond(authUser);
    $httpBackend.whenGET('https://api.url/organizations').respond([$scope.currentOrganization]);
    $httpBackend.whenGET('https://api.url/organizations/10').respond($scope.currentOrganization);
    $httpBackend.whenGET('https://api.url/organizations/5777/sites/80070123?all_fields=true').respond(currentSiteMock);

  }));

  describe('populateOptions', function() {
    it('should have options', function() {
      var selector = renderDirectiveAndDigest();
      expect(selector.hasSales).toBe(true);
      expect(selector.totalTranskey).toBe('common.TOTAL');
      expect(selector.currentOrganization.organization_id).toBe(5777);
    });
  });

  describe('populateData', function() {
    it('should have data for org level', function() {
      setCurrentOrgWithRealTimeSubscription();
      var params = {
        orgId: 5777,
      };
      setupBackend(params, false);
      var selector = renderDirectiveAndDigest();
      $httpBackend.flush();
      expect(selector.hasData).toBe(true);
      expect(selector.hasTableData).toBe(true);
    });

    // This test is flaky. To be fixed in SA-1347
    xit('should have data for single site', function() {
      $scope.siteId = 80070123;
      $scope.singleSite = true;
      setCurrentOrgWithRealTimeSubscription();

      $scope.$apply();
      var params = {
        orgId:  5777,
        siteId: $scope.siteId,
      };
      setupBackend(params, true);
      var selector = renderSingleSiteDirectiveAndDigest();
      $httpBackend.flush();
      $scope.$apply();
      expect(selector.hasData).toBe(true);
      expect(selector.chartData.labels.length).toBe(1);
    });
  });

  function renderSingleSiteDirectiveAndDigest() {
    var orgId = 5777;
    var siteId = 80070123;
    $scope.selectedSites = [];
    $scope.selectedSites.push(siteId);
    var sitesMock = [{
      site_id: siteId,
      timezone:'Europe/Rome',
      customer_site_id: '0067',
      name: 'site1',
      OrganizationResource: {
        id: orgId
    }}];
    $scope.selectedSitesInfo = sitesMock;

    var element = angular.element(
      '  <real-time-interval-data-widget' +
      '  org-id="orgId"'+
      '  current-user="currentUser"' +
      '  current-organization="currentOrganization"' +
      '  site-id="siteId"'+
      '  sites="sites"'+
      '  selected-sites="selectedSites"' +
      '  selected-sites-info="selectedSitesInfo"' +
      '  single-site="true"'+
      '  has-labor="true"'+
      '  has-sales="true"'+
      '  </real-time-interval-data-widget>'
    );

    $compile(element)($scope);
    $scope.$digest();

    return element.controller('realTimeIntervalDataWidget');
  }

  function renderDirectiveAndDigest() {
    var element = angular.element(
      '  <real-time-interval-data-widget' +
      '  org-id="orgId"'+
      '  current-user="currentUser"' +
      '  current-organization="currentOrganization"' +
      '  site-id=""'+
      '  sites="sites"'+
      '  has-labor="true"'+
      '  has-sales="true"'+
      '  </real-time-interval-data-widget>'
    );

    $compile(element)($scope);
    $scope.$digest();

    return element.controller('realTimeIntervalDataWidget');
  }

  function cacheTemplates($templateCache) {
    // Put an empty template to cache to prevent Angular from fetching it
    $templateCache.put(
      'app/header/header.html',
      '<div></div>'
    );

    $templateCache.put(
      'app/analytics/analytics.partial.html',
      '<div></div>'
    );

    $templateCache.put(
      'components/widgets/real-time-data-widget/real-time-interval-data-widget.partial.html',
      '<div></div>'
    );
  }

  function setupBackend(params, singleSite) {
    var endpoint = apiUrl + '/realtime';
    if (!singleSite) {
      endpoint += '/organization';
    }
    var url = buildUrl(endpoint, params);

    $httpBackend.whenGET(url).respond({ result: getMockData() });
  }

  function buildUrl(endpoint, params) {
    return endpoint + '?' +
      Object.keys(params)
        .sort()
        .map(function (key) {

          if (Array.isArray(params[key])) {
            var param = '';

            _.each(params[key], function (item) {
              if (param.length > 0) {
                param += '&';
              }
              param += (key + '=' + item);
            });

            return param;
          }

          return key + '=' + params[key];
        })
        .join('&');
  }

  function setInitialBackup() {
   $scope.siteId = 80070123;
    var params = {
      orgId: $scope.currentOrganization.organization_id,
      siteId: $scope.siteId,
    };
    setupBackend(params, true);
  }

  function getMockData() {
    var date = moment();
    var businessStart = angular.copy(date.hours(date.hours()-2));
    var operartingHourStart =  angular.copy(date.hours(date.hours()-1));
    var operartingHourEnd =  angular.copy(date.hours(date.hours()+1));
    var result = [{ realtimeData:
      [{ sites:
        [{
          id: '0067',
          currency: '',
          data: [
            { 'time': businessStart.format('YYYYMMDDHHmm'), 'enters': '', 'exits': '', 'trafficCode': '01', 'sales': '0.00', 'transactions': 0, 'items': 0, 'laborHours': 0 },
            { 'time': operartingHourStart.format('YYYYMMDDHHmm'), 'enters': '', 'exits': '', 'trafficCode': '01', 'sales': '0.00', 'transactions': 0, 'items': 0, 'laborHours': 0 }
          ]
        }]
      }],
      businessDayData:{ site_id: '0067', start: businessStart},
      operatingHoursData: { start: operartingHourStart, end: operartingHourEnd },
      enterExit: 'Exits'
    }];
    return result;
  }

  function initCurrentOrg() {
    var orgId = 5777;
    $scope.orgId = orgId;
    var siteId = 80070123;

    var sitesMock = [{
      site_id: siteId,
      customer_site_id: '0067',
      name: 'site1',
      OrganizationResource: {
        id: orgId
    }}];

    var org = {
      'organization_id': orgId,
      'subscriptions': {
        'interior': true,
        'perimeter': true
      },
      'portal_settings': {
        'organization_type': 'Mall'
      }
    };
    $scope.currentOrganization = org;
    $scope.sites = sitesMock;
  }

  function setCurrentOrgWithRealTimeSubscription() {
    var orgId = 5777;
    var siteId = 80070123;

    var sitesMock = [{
      site_id: siteId,
      customer_site_id: '0067',
      timezone:'Europe/Rome',
      name: 'site1',
      OrganizationResource: {
        id: orgId
    }}];

    var org = {
      'organization_id': orgId,
      'subscriptions': {
        'interior': true,
        'perimeter': true,
        'realtime_sales':true,
        'realtime_traffic':true
      },
      'portal_settings': {
        'organization_type': 'Mall'
      }
    };
    $scope.sites = sitesMock;
    $scope.currentOrganization = org;
  }
});
