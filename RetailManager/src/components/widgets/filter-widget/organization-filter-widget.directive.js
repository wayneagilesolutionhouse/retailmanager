'use strict';

/** Organization Filter Widget
  * Isolated Scope:
  * - currentOrganization: We will need this data to display any filter data.
  * - updateSelectedFilters: Pass in a function that will be called when the user updates the filters they selected.
  *   This is mainly to create a two way connection with the parent controller so that we don't have to use factories (Ex: CSV Export).
  * - openMain: You can pass in a true/false on whether you want the main filter content to be open when the user loads the page.
  * - preselectedTags: If you want to have a preconfigured set of filter tags already selected then use this.
  *   It is an array of values containing the selectedTags, selectedTagNames and selectedTagsInGroup (in that order).
  *   Those three values do need to be sent for the functionality to work correctly.
  */


angular.module('shopperTrak.widgets').directive('organizationFilterWidget', OrganizationFilterWidget);

function OrganizationFilterWidget() {
  return {
    templateUrl: 'components/widgets/filter-widget/organization-filter-widget.partial.html',
    controller: OrganizationFilterWidgetCtrl,
    controllerAs: 'vm',
    bindToController: true,
    scope: {
      currentOrganization: '=?',
      updateSelectedFilters: '&',
      openMain: '=?',
      preselectedTags: '=?',
      selectedTagData: '=?',
      selectedTagIds: '=?',
      setSelectedTagsSites: '=?',
      clearFilter: '=clearFilter',
    }
  };
}

OrganizationFilterWidgetCtrl.$inject = [
  'retailOrganizationSummaryData',
  'adminSitesData',
  '$rootScope',
  '$http',
  '$translate',
  'LocalizationService',
  'requestManager',
  'ObjectUtils',
  'apiUrl',
  '$scope',
  '$timeout',
  '$state'
];

function OrganizationFilterWidgetCtrl(
  retailOrganizationSummaryData,
  adminSitesData,
  $rootScope,
  $http,
  $translate,
  LocalizationService,
  requestManager,
  ObjectUtils,
  apiUrl,
  $scope,
  $timeout,
  $state
) {
  var vm = this;
  var unbindClearFilterWatch;
  var unbindSelectedTagDataWatch;
  var stateWatch;

  vm.organizationTags = getOrganizationTags();
  vm.organizationTagsIsNull = false;
  vm.isOpenGroup = [];
  vm.isOpenLevel = [];
  vm.totalTagsSelected = 0;

  vm.language = LocalizationService.getCurrentLocaleSetting();

  vm.toggleGroup = toggleGroup;
  vm.toggleLevel = toggleLevel;
  vm.toggleTag = toggleTag;

  vm.clearFilters = clearFilters;
  vm.applyFilters = applyFilters;
  vm.toggleFilters = toggleFilters;
  vm.filterHasChanged = false;
  vm.ranApplyFilters = false;

  $scope.$watch('vm.selectedTagData', function() {
    if(typeof vm.selectedTagData !== 'undefined' && vm.selectedTagData.length === 0) {
      clearFilters();
    }
  });

  if(vm.openMain) {
    vm.openFilters = true;
  } else {
    vm.openFilters = false;
  }

  if(!ObjectUtils.isNullOrUndefined(vm.preselectedTags)) {
    vm.selectedTags = vm.preselectedTags[0];
    vm.selectedTagNames = vm.preselectedTags[1];
    vm.selectedTagsInGroup = vm.preselectedTags[2];
    updateAppliedSiteCount(); // if we pass in preselected tags then we should update site showing count
  } else {
    vm.selectedTags = [];
    vm.customTagsSelected = {};
    vm.selectedTagNames = {};
    vm.selectedTagsInGroup = {};
  }

  activate();

  function getOrganizationTags() {
    if(!ObjectUtils.isNullOrUndefined(vm.currentOrganization) &&
    !ObjectUtils.isNullOrUndefined(vm.currentOrganization.portal_settings)) {
      return vm.currentOrganization.portal_settings.group_structures;
    }
    return null;
  }

  function activate() {
    if(ObjectUtils.isNullOrUndefinedOrEmpty(vm.organizationTags)) {
      vm.organizationTagsIsNull = true;
      return;
    }

    getTotalFiltersApplied();

    getCustomTags();

    calculateUserFilterCount();

    parseHierarchyTags(vm.organizationTags);
    loadTranslations();
    configureWatches();

    if( ObjectUtils.isNullOrUndefined(vm.cachedFilters) ) {
      vm.cachedFilters = [];
    }

    if(!ObjectUtils.isNullOrUndefined($state.activeFilters) && $state.activeFilterId === vm.currentOrganization.organization_id ) {
      setPrevSelectedFilters();
    } else {
      delete $state.activeFilters;
      delete $state.activeFilterId;
    }
  }

  function setPrevSelectedFilters() {
    _.each($state.activeFilters, function(_filter) {
      var tag = _filter.tag;
      var group = _filter.group;

      toggleTag(tag, group);
    });

    applyFilters();
  }

  function configureWatches() {
    unbindSelectedTagDataWatch = $scope.$watch('vm.selectedTagData', function() {
      if(typeof vm.selectedTagData !== 'undefined' && (vm.selectedTagData === null || vm.selectedTagData.length === 0) ) {
        clearFilters();
      }
    });

    unbindClearFilterWatch = $scope.$on('clearFilter', function(event ,data){
      if(data) {
        clearFilters();
      }
    });

    stateWatch = $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams) {
      if( !ObjectUtils.isNullOrUndefined(toParams.siteId) || vm.currentOrganization.organization_id !== toParams.orgId){
        delete $state.activeFilters;
        delete $state.activeFilterId;
      }
    });

    $scope.$on('$destroy', onDestroy);
  }

  function onDestroy() {
    if (typeof unbindSelectedTagDataWatch === 'function') {
      unbindSelectedTagDataWatch();
    }

    if (typeof unbindClearFilterWatch === 'function') {
      unbindClearFilterWatch();
    }

    if (typeof stateWatch === 'function') {
      stateWatch();
    }
  }

  function loadTranslations() {
    $translate.use(vm.language);
  }

  function isTagSelected(tag) {
    if(ObjectUtils.isNullOrUndefinedOrEmpty(vm.selectedTagIds)) {
      return false;
    }
    return (vm.selectedTagIds.indexOf(tag) > -1);
  }

  function parseHierarchyTags(tags) {
    // Creating an object of unique level descriptions and it's possible values
    var availableTags = {};
    _.each(tags, function(group) {
      _.each(group.levels, function(level) {
        availableTags[level.description] = [];
        _.each(level.possible_values, function(tag) {
          availableTags[level.description].push(tag);
          if(isTagSelected(tag._id)) {
            toggleTag(tag, group);
          }
        });
      });
    });

    // Making the tags into an easier structure and removing any tags that don't have any values in them
    var newTags = [];

    _.each(availableTags, function(tag, key) {
      if(tag.length > 0) {
        var sortedTags = alphabeticalSort(tag);
        newTags.push({ name: key, levels: sortedTags });
      }
    });

    separateTagsIntoColumns(newTags);

    if(!ObjectUtils.isNullOrUndefinedOrEmpty(vm.selectedTagIds)) {
       applyFilters();
    }
  }

  function alphabeticalSort(obj) {

    var sortedItems = _.sortBy(obj, function(item) {
      return item.name;
    });

    return sortedItems;

  }

  function separateTagsIntoColumns(tags) {
    var column1 = [],
        column2 = [],
        column3 = [],
        column4 = [],
        column5 = [],
        column6 = [];

    for(var index = 0; index < tags.length; index++) {
      var modulusEvaluation = index % 6;
      if(index === 0) {
        column1.push(tags[index]);
      } else {
        if(modulusEvaluation === 1) {
          column2.push(tags[index]);
        } else if(modulusEvaluation === 2) {
          column3.push(tags[index]);
        } else if(modulusEvaluation === 3) {
          column4.push(tags[index]);
        } else if(modulusEvaluation === 4) {
          column5.push(tags[index]);
        } else if(modulusEvaluation === 5) {
          column6.push(tags[index]);
        } else {
          column1.push(tags[index]);
        }
      }
    }

    vm.separatedTags = [ column1, column2, column3, column4, column5, column6 ];

  }

  function toggleLevel(group, level) {
    if(!ObjectUtils.isNullOrUndefined(vm.isOpenLevel) && vm.isOpenLevel[group] && vm.isOpenLevel[group][level]) {
      vm.isOpenLevel[group][level] = false;
    } else {
      vm.isOpenLevel = [];
      vm.isOpenLevel[group] = [];
      vm.isOpenLevel[group][level] = true;
    }
  }

  function toggleGroup(number) {
    if(!ObjectUtils.isNullOrUndefined(vm.isOpenGroup) && vm.isOpenGroup[number]) {
      vm.isOpenGroup[number] = false;
    } else {
      vm.isOpenGroup = [];
      vm.isOpenGroup[number] = true;
    }
  }

  function toggleTag(_tag, group) {
    var tag = _tag._id;
    var name = _tag.name;

    vm.filterHasChanged = true;

    if( !ObjectUtils.isNullOrUndefined(_tag.filter_type) ) {

      vm.customTagsSelected[tag] = !vm.customTagsSelected[tag];

    } else {

      vm.selectedTags[tag] = !vm.selectedTags[tag];

    }

    if( vm.selectedTags[tag] || vm.customTagsSelected[tag] ) {
      if(ObjectUtils.isNullOrUndefined(vm.selectedTagsInGroup[group])) {
        vm.selectedTagsInGroup[group] = 0;
      }
      vm.selectedTagsInGroup[group]++;
    } else {
      vm.selectedTagsInGroup[group]--;
    }

    vm.selectedTagNames[tag] = name;
    tagIsActive(tag, name);

    var tagCache = {tag: _tag, group:group};

    if(vm.customTagsSelected[tag] === true || vm.selectedTags[tag] === true) {
      vm.cachedFilters.push(tagCache);
    } else
    if (vm.customTagsSelected[tag] === false || vm.selectedTags[tag] === false) {
      vm.cachedFilters = _.reject(vm.cachedFilters, function(_tagCache) {
        return _tagCache.tag._id === _tag._id;
      });
    }

  }

  function tagIsActive(tag, name) {
    if(!ObjectUtils.isNullOrUndefined(vm.selectedTagNames) && vm.selectedTagNames[tag] === name) {
      return true;
    } else {
      return false;
    }
  }

  function getTotalFiltersApplied() {
    vm.totalTagsSelected = 0;
    _.each(vm.selectedTagsInGroup, function(group) {
      vm.totalTagsSelected += group;
    });
  }

  function applyFilters() {
    // Only apply filters if the filter themselves have changed
    if(vm.filterHasChanged) {
      vm.filterHasChanged = false;
      vm.ranApplyFilters = true;

      var tags = [ vm.selectedTags, vm.selectedTagNames, vm.selectedTagsInGroup ];

      getTotalFiltersApplied();
      updateAppliedSiteCount();

      vm.selectedTagData = tags;

      vm.updateSelectedFilters({ filterTags: tags, customTags: vm.customTagsSelected });

      //save new tag selections
      $state.activeFilters = vm.cachedFilters;
      $state.activeFilterId = vm.currentOrganization.organization_id;
    }
  }

  function clearFilters() {
    vm.selectedTags = [];
    vm.customTagsSelected = {};
    vm.selectedTagNames = {};
    vm.selectedTagsInGroup = {};
    vm.totalTagsSelected = 0;

    // We only need to run this whenever we have already applied filters
    if(vm.ranApplyFilters) {
      vm.ranApplyFilters = false;
      var tags = [ vm.selectedTags, vm.selectedTagNames ];

      vm.selectedTagData = tags;

      updateAppliedSiteCount();
      vm.updateSelectedFilters({ filterTags: tags });
      if( _.isFunction(vm.setSelectedTagsSites)) {
        vm.setSelectedTagsSites([]);
      }
    }

    if( !ObjectUtils.isNullOrUndefined($state.activeFilters) ) {
      vm.cachedFilters = [];
      delete $state.activeFilters;
      delete $state.activeFilterId;
    }
  }

  function toggleFilters() {
    if(vm.openFilters) {
      vm.openFilters = false;
    } else {
      vm.openFilters = true;
    }
  }

  function getCount(data) {
     if(!ObjectUtils.isNullOrUndefined(data) &&
       !ObjectUtils.isNullOrUndefinedOrEmpty(data.result)) {
       if(!ObjectUtils.isNullOrUndefined(data.result[0].count)) {
         return data.result[0].count;
       }

       return data.result.length;
     }

     return 0;
  }

  function calculateUserFilterCount() {

    requestManager.get(apiUrl + '/organizations/' + vm.currentOrganization.organization_id + '/sites', {},false)
      .then(function (data) {

        if (!ObjectUtils.isNullOrUndefined(data) && !ObjectUtils.isNullOrUndefined(data.result)) {
          if (!ObjectUtils.isNullOrUndefined(vm.currentOrganization.site_count)) {

            var siteCount = data.result.length;

            vm.currentOrganization.site_count = siteCount;
            vm.totalSiteCount = siteCount;
            vm.sitesShowingCount = siteCount;

          }
        }
      });
  }



  function getCustomTags() {
    requestManager.get(apiUrl + '/organizations/' + vm.currentOrganization.organization_id + '/custom-tags', {})
    .then(function(data) {
      if( !ObjectUtils.isNullOrUndefined(data) && !ObjectUtils.isNullOrUndefined(data.result) ) {

        var customTags = data.result;
        var tagTypes = _.uniq( _.pluck(customTags, 'tag_type') );
        var transFormedTags = [];

        _.each(tagTypes, function(tagType) {
          var group = _.filter(customTags, function(_tag) {
            _tag.filter_type = 'custom';
            return _tag.tag_type === tagType;
          });

          var tagObj = [ {levels:group, name:tagType} ];
          transFormedTags.push(tagObj);
        });

        var filterTags = _.union(transFormedTags, vm.separatedTags);
        vm.separatedTags = [];

        $timeout(function () {
          vm.separatedTags = _.filter(filterTags, function (_tagGroup) {
            return _tagGroup.length > 0;
          });

        });


      }
    });
  }

  function updateAppliedSiteCount() {
    if(vm.totalTagsSelected === 0) {
      vm.sitesShowingCount = 0;
    } else {

      var customTags = [];
      var tagIds = _.keys(_.pick(vm.selectedTags, function(_selected) {
        return _selected === true;
      }));

      if( !ObjectUtils.isNullOrUndefined(vm.customTagsSelected) ) {
        customTags = _.keys(_.pick(vm.customTagsSelected, function(_selected) {
          return _selected === true;
        }));
      }

      var heirarchyTags = '';
      var customTagIds = '';

      _.each(tagIds, function(id) {
        heirarchyTags += heirarchyTags === ''? '?hierarchyTagId=' + id: '&hierarchyTagId=' + id;
      });

      _.each(customTags, function(id) {
        customTagIds += '&customTagId=' + id;
      });

      heirarchyTags += heirarchyTags === ''? '?all_fields=true': '&all_fields=true';

      var url = apiUrl + '/organizations/' + vm.currentOrganization.organization_id + '/sites' + heirarchyTags;

      if(customTagIds !== '') {
        url += customTagIds;
      }

      $http.get(url).then(function(response) {
        vm.sitesShowingCount = getCount(response.data);

        if( _.isFunction(vm.setSelectedTagsSites)) {
          vm.setSelectedTagsSites(response.data.result);
        }
      });
    }
  }
}

