(function () {
  'use strict';

  angular.module('shopperTrak').controller('KpiSummaryWidgetController', [
    '$scope',
    '$q',
    '$translate',
    '$http',
    'apiUrl',
    'retailOrganizationSummaryData',
    'comparisonsHelper',
    'kpiSummaryWidgetConstants',
    'metricConstants',
    'ObjectUtils',
    '$rootScope',
    'currencyService',
    function (
      $scope,
      $q,
      $translate,
      $http,
      apiUrl,
      retailOrganizationSummaryData,
      comparisonsHelper,
      constants,
      metricConstants,
      ObjectUtils,
      $rootScope,
      currencyService
    ) {
      var vm = this;
      var unbindLoadingWatch;
      var tagWatch;

      vm.allKpisData = [];
      vm.allowedPdfKpis = [];

      if ($rootScope.pdf) vm.isLoading = true; // Workaround for PDF rendering issue

      activate();

      function activate() {
        setCurrencySymbol();
        configureKpis();
        configureDateRanges();
        $scope.$on('$destroy', onDestroy);

        tagWatch = $scope.$watchCollection('vm.selectedTags', function () {
          configureDateRanges();
        });
      }

    /** Loads data for the widget if none has been passed in.
     *  Is currently used by custom dashboards and the PDF controller
     *
     **/
      function loadIfNoDataProvided() {
        if (!ObjectUtils.isNullOrUndefined(vm.widgetData)) {
          return;
        }

        $http({
          method: 'GET',
          url: apiUrl + '/organizations/' + vm.orgId
        }).then(function (response) {
          vm.currentOrganization = response.data.result[0];

          vm.widgetDataIsLoading = {};
          vm.widgetData = {};

          getWidgetData(vm.orgId, vm.siteId, vm.dateRange.start, vm.dateRange.end, vm.selectedTags, 1);
          getWidgetData(vm.orgId, vm.siteId, vm.compareRange1.start, vm.compareRange1.end, vm.selectedTags, 2);
          getWidgetData(vm.orgId, vm.siteId, vm.compareRange2.start, vm.compareRange2.end, vm.selectedTags, 3);
        }, function (err) {
          console.error(err);
        });
      }

      function setCurrencySymbol() {
        if (!ObjectUtils.isNullOrUndefinedOrBlank(vm.orgId) && !ObjectUtils.isNullOrUndefined(vm.siteId) && ObjectUtils.isNullOrUndefinedOrBlank(vm.currencySymbol)) {
          currencyService.getCurrencySymbol(vm.orgId, vm.siteId).then(function (data) {
            vm.currencySymbol = data.currencySymbol;
          });
        }
      }

      function configureKpis() {
        var overallDeferred = $q.defer();

        var promises = [];

        vm.allKpisData = [];

        constants.kpis.forEach(function (kpi, key) {
          var deferred = $q.defer();

          promises.push(deferred.promise);

          var metric = _.findWhere(metricConstants.metrics, { value: kpi.id });
          var permissionInfo = getPermissionInfo(kpi);
          var hasPermission = _.where(permissionInfo, { hasPermission: true }).length === permissionInfo.length;

          var prefixSymbol = metric.prefixSymbol;

          if (metric.isCurrency && !ObjectUtils.isNullOrUndefinedOrBlank(vm.currencySymbol)) {
            prefixSymbol = vm.currencySymbol;
          }

          vm.allowedPdfKpis.push(kpi.id);

          getPermissionMessage(permissionInfo).then(function (permissionMessage) {

            var kpiData = {
              id: key,
              isLoading: false,
              requestFailed: false,
              hasData: true,
              title: kpi.title,
              api: kpi.apiReturnkey,
              hasPermission: hasPermission,
              permissionName: permissionMessage,
              precision: metric.precision,
              prefixSymbol: prefixSymbol,
              suffixSymbol: metric.suffixSymbol,
              labels: {
                1: {
                  totalsLabel: kpi.totalsLabel,
                  fromLabel: ''
                },
                2: {
                  totalsLabel: kpi.totalsLabel,
                  fromLabel: getFromPreviousPeriodTranskey(vm.compareRange1Type),
                  periodLabel: getPreviousPeriodTranskey(vm.compareRange1Type)
                },
                3: {
                  totalsLabel: kpi.totalsLabel,
                  fromLabel: getFromPreviousYearTranskey(vm.compareRange2Type),
                  periodLabel: getPreviousYearTranskey(vm.compareRange2Type)
                }
              },
              comparisons: {
                current: 1,
                previousPeriod: 2,
                previousYear: 3,
                data: {}
              }
            };

            var kpiDataIndex =  _.findLastIndex(vm.allKpisData, {
              id: kpiData.id
            });

            if(ObjectUtils.isNullOrUndefined(kpiDataIndex) || kpiDataIndex < 0) {
              vm.allKpisData.push(kpiData);
            } else {
              vm.allKpisData[kpiDataIndex] = kpiData;
            }          deferred.resolve();
          });
        });

        $q.all(promises).then(function() {
          overallDeferred.resolve();
          loadIfNoDataProvided();
        });

        return overallDeferred.promise;
      }

      function configureDateRanges() {
        var previousPeriod = vm.dateRangeKeys.range[vm.dateRangeKeys.previousPeriod];
        var previousYear = vm.dateRangeKeys.range[vm.dateRangeKeys.previousYear];

        var dateRanges = {
          1: { range: vm.dateRange, key: vm.dateRangeKeys.range[vm.dateRangeKeys.current].value },
          2: { range: vm.compareRange1, key: previousPeriod.value },
          3: { range: vm.compareRange2, key: previousYear.value }
        };

        unbindLoadingWatch = $scope.$watch('vm.isLoading', function (isLoading) {
          if (isLoading === true) {
            return;
          }

          if (ObjectUtils.isNullOrUndefinedOrEmptyObject(vm.widgetData)) {
            return;
          }

          if (ObjectUtils.isNullOrUndefinedOrEmptyObject(vm.widgetData[dateRanges[1].key]) ||
            ObjectUtils.isNullOrUndefinedOrEmptyObject(vm.widgetData[dateRanges[1].key].result)) {
              return;
          }

          if (vm.widgetData[dateRanges[1].key].result.length === 0) {
            vm.hasData = false;
          } else {
            vm.hasData = true;
            configureKpis().then(function() {
              for (var i = 1; i < 4; i++) {
                var valueData = vm.widgetData[dateRanges[i].key].result[0];
                if (typeof valueData !== 'undefined') {
                  comparisonsHelper.getValuesForRange(i, vm.widgetData[dateRanges[i].key].result[0], vm.allKpisData, dateRanges[i].range);
                }
              }
            });
          }
        });
      }

      function onDestroy() {
        if (typeof unbindLoadingWatch === 'function') {
          unbindLoadingWatch();
        }

        if (typeof tagWatch === 'function') {
          tagWatch();
        }
      }

      function getPermissionMessage(permissionInfo) {
        var deferred = $q.defer();

        var result = '';

        var missingPermissions = _.where(permissionInfo, { hasPermission: false });

        var transkeys = missingPermissions.map(function (missingPermission) {
          return missingPermission.transkey;
        });

        if (transkeys.length === 0) {
          deferred.resolve(result);
        }

        $translate(transkeys).then(function (translations) {

          transkeys.forEach(function (transkey, index) {
            if (index > 0) {
              result += ', ';
            }
            result += translations[transkey];
          }, function () {
            deferred.reject();
          });

          deferred.resolve(result);
        });

        return deferred.promise;
      }

      function getPermissionInfo(kpi) {
        var metric = _.findWhere(metricConstants.metrics, { value: kpi.id });

        var permissionResults = [];

        metric.requiredPermissions.forEach(function (permission) {

          var permissionResult = {
            name: permission
          };

          if (permission === 'sales') {
            permissionResult.hasPermission = vm.hasSales;
            permissionResult.transkey = 'kpis.shortKpiTitles.tenant_sales';
          }

          if (permission === 'labor') {
            permissionResult.hasPermission = vm.hasLabor;
            permissionResult.transkey = 'kpis.shortKpiTitles.tenant_labor';
          }

          permissionResults.push(permissionResult);
        });

        return permissionResults;
      }

      function getFromPreviousPeriodTranskey(compareRangeType) {
        var transkey = getPreviousPeriodTranskey(compareRangeType);

        return makeTranskeyFrom(transkey);
      }

      function getFromPreviousYearTranskey(compareRangeType) {
        var transkey = getPreviousYearTranskey(compareRangeType);

        return makeTranskeyFrom(transkey);
      }

      function makeTranskeyFrom(transkey) {
        return transkey.replace('common.', 'common.FROM');
      }

      function getPreviousPeriodTranskey(compareRangeType) {
        var transkey = getPeriodTranskey(compareRangeType);

        if (transkey !== '') {
          return transkey;
        }

        return 'common.CUSTOMCOMPARE1';
      }

      function getPreviousYearTranskey(compareRangeType) {
        var transkey = getPeriodTranskey(compareRangeType);

        if (transkey !== '') {
          return transkey;
        }

        return 'common.CUSTOMCOMPARE2';
      }

      function getPeriodTranskey(compareRangeType) {
        if (compareRangeIsPriorPeriod(compareRangeType)) {
          return 'common.PRIORPERIOD';
        }

        if (compareRangeIsPriorYear(compareRangeType)) {
          return 'common.PRIORYEAR';
        }

        return '';
      }

      function compareRangeIsPriorPeriod(comparePeriodType) {
        return comparePeriodType === 'prior_period';
      }

      function compareRangeIsPriorYear(comparePeriodType) {
        return comparePeriodType === 'prior_year';
      }

      function getEndpoint(siteId) {
        // Traffic only
        if(!vm.currentOrganization.subscriptions.sales) {
          return 'kpis/traffic';
        }

        // Org level
        if(ObjectUtils.isNullOrUndefined(siteId)) {
          return 'kpis/report';
        }

        // Site level
        return 'kpis/report';
      }


      function getWidgetData(orgId, siteId, dateRangeStart, dateRangeEnd, selectedTags, periodKey) {
        var dateRangeKey = _.isUndefined(ObjectUtils.getNestedProperty($scope,'dateRangeKeys.range'))
          ? retailOrganizationSummaryData.getDateRangeKey(dateRangeStart, dateRangeEnd)
          : vm.dateRangeKeys.range[periodKey].value;


        var apiUrl = getEndpoint();
        var toSplice = 'traffic';
        if (vm.currentOrganization.subscriptions.sales) {
          toSplice = 'sales';
        }

        vm.widgetData[dateRangeKey] = [];
        vm.widgetDataIsLoading[dateRangeKey] = true;

       _.each(vm.allowedPdfKpis, function(kpi, index){
          if(kpi === toSplice){
            vm.allowedPdfKpis.splice(index, 1);
          }
        });

        var params = {
          apiEndpoint: apiUrl,
          orgId: orgId,
          siteId: siteId,
          comp_site: vm.compStores,
          dateRangeStart: dateRangeStart,
          dateRangeEnd: dateRangeEnd,
          selectedTags: selectedTags
        };

        if (typeof vm.salesCategories !== 'undefined' && vm.salesCategories.length > 0 && _.min(vm.salesCategories.id) > 0) {
          params.sales_category_id = vm.salesCategories.map(function(category) {
            return category.id;
          });
        }

        if( !ObjectUtils.isNullOrUndefinedOrEmpty(vm.customTags) ) {
          params.customTagId = vm.customTags;
        }

        if(apiUrl !== 'kpis/traffic') {
          params.kpi = vm.allowedPdfKpis;
        }

        retailOrganizationSummaryData.fetchKpiData(params, true, function (data) {
          vm.widgetData[dateRangeKey] = data;
          vm.widgetDataIsLoading[dateRangeKey] = false;
          vm.isLoading = checkWidgetDataRequests();
        }, function (error, status) {
          vm.widgetDataIsLoading[dateRangeKey] = { kpiRequestFailed: true, message: error, status: status };
          vm.isLoading = false;
          vm.isLoading = checkWidgetDataRequests();
        });
      }

      function checkWidgetDataRequests() {
        var numLoading = _.filter(vm.widgetDataIsLoading, function (item) {
          return item === true;
        });
        return numLoading.length > 0;
      }

    }]);
})();
