'use strict';

angular.module('shopperTrak').controller('KpiSummaryWidgetContainerController',
    [
    '$scope',
    '$q',
    'requestManager',
    'apiUrl',
    'retailOrganizationSummaryData',
    'ObjectUtils',
    function ($scope, $q, requestManager, apiUrl, retailOrganizationSummaryData, ObjectUtils) {
        var vm = this;
        var url = getEndpoint();
        var salesDateranges = {
            current: 1,
            previousPeriod: 2,
            previousYear: 3,
            range: {
                1: { value: '' },
                2: { value: '' },
                3: { value: '' }
            }
        };

        var tempKpiData = [];
        var loadingStatus = [];
        vm.orgHasLabor = true;
        vm.orgHasSales = true;
        vm.isLoading = true;

        $scope.$watch('vm.salesCategories', function () {
          vm.isLoading = true;
          getAllKpiData();
        });

        getAllKpiData();

        function getEndpoint() {
            if(!vm.siteHasSales) {
                return 'kpis/traffic';
            }

            return 'kpis/report';
        }

        function getAllKpiData() {
            var promises = [];

            vm.salesDateRanges = salesDateranges;
            var apiDateRanges = [vm.dateRange, vm.compareRange1, vm.compareRange2];

            apiDateRanges.forEach(function (range, index) {
                var promise = getKpiData(url, vm.orgId, vm.siteId, vm.zoneId, vm.operatingHours, range, salesDateranges.range[index + 1]);
                promises.push(promise);
            });

            $q.all(promises).then(function() {
                vm.isLoading = false;
                vm.widgetData = tempKpiData;
            });
        }

        function getKpiData(url, orgId, siteId, zoneId, operatingHours, range, salesDaterange) {
            var deferred = $q.defer();
            salesDaterange.value = retailOrganizationSummaryData.getDateRangeKey(range.start, range.end);
            var params = {
                orgId: orgId,
                groupBy: 'aggregate',
                siteId: siteId,
                operatingHours: true
            };

            if (typeof zoneId !== 'undefined') {
                params.zoneId = vm.zoneId;
            }
            if (typeof operatingHours !== 'undefined') {
                params.operatingHours = vm.operatingHours;
            }

            if(vm.siteHasSales) {
                var kpi = ['ats', 'conversion', 'traffic'];

                if(vm.siteHasLabor) {
                  kpi.push('star');
                }

                params.kpi = kpi;
            }

            if (typeof vm.salesCategories !== 'undefined' && vm.salesCategories.length > 0 && _.min(vm.salesCategories.id) > 0) {
              params.sales_category_id = vm.salesCategories.map(function(category) {
                return category.id;
              });
            }

            params.reportStartDate = typeof range.start === 'string' ? range.start : range.start.toISOString();
            params.reportEndDate = typeof range.end === 'string' ? range.end : range.end.toISOString();

            requestManager.get(apiUrl + '/' + url, {
                params: params
            }).then(function (response) {
                ObjectUtils.rename(response.result[0], 'sales_amount', 'sales');
                ObjectUtils.rename(response.result[0], 'total_sales', 'sales');
                ObjectUtils.rename(response.result[0], 'total_traffic', 'traffic');

                tempKpiData[salesDaterange.value] = response;
                loadingStatus[salesDaterange.value] = false;
                deferred.resolve();
            }, function(e) {
                console.log('rejected', e);
                deferred.reject();
            }).catch(function(e) {
              console.log('rejected', e)
            });

            return deferred.promise;
        }
    }]);
