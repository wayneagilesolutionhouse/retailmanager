(function () {
  'use strict';
  angular.module('shopperTrak')
  .directive('segmentWidget', segmentWidget);

  function segmentWidget() {
    return {
      restrict: 'E',
      templateUrl: 'components/widgets/segment-widget/segment-widget.partial.html',
      scope: {
        params: '=?',
        numberFormatName: '=',
        org: '=?',
        onExportClick: '&',
        exportIsDisabled: '=?',

        // The following props are used by the PDF export
        segments: '=?',
        startDate: '=?',
        endDate: '=?',
        compareDateStart: '=?',
        compareDateEnd: '=?',
        orgId: '=?',
        isLoading: '=?',
        firstTimeConfigure:'=?',
        showOrgIndex:'='
      },
      controller: segmentWidgetController,
      controllerAs: 'vm',
      bindToController: true
    };
  }

  segmentWidgetController.$inject = [
    '$state',
    '$rootScope',
    'marketIntelligenceService',
    'ObjectUtils',
    'OrganizationResource'
  ];

  function segmentWidgetController($state, $rootScope, marketIntelligenceService, ObjectUtils, OrganizationResource) {
    var vm = this;

    var mappedSubscriptions;

    activate();

    function activate() {
      initScope();

      if($rootScope.pdf) {
        setupParamsForPdf();
      } else {
        loadData();
      }
    }

    function initScope() {
      vm.isLoading = true;
      vm.kpiSegmentsArray = [];
      vm.segmentSettings = segmentSettings;
    }

    function setupParamsForPdf() {

      marketIntelligenceService.getSubscriptions(vm.orgId).then(function(result) {
        var subscriptionsForOrg = result.data;

        vm.params = {
          dateStart: vm.startDate,
          dateEnd: vm.endDate,
          compareStart: vm.compareDateStart,
          compareEnd: vm.compareDateEnd,
          subscriptions: _.map(vm.segments, function(segment) {
            segment.orgId = vm.orgId;
            segment.category = findSubscription(subscriptionsForOrg, segment.category.uuid);
            segment.geography = findSubscription(subscriptionsForOrg, segment.geography.uuid);
            return segment;
          })
        };

        OrganizationResource.get({orgId: vm.orgId}).$promise.then(function(organization) {
          vm.org = organization;
          loadData();  
        });

        
      });
    }

    function findSubscription(subscriptions, uuid) {
      if(angular.isUndefined(mappedSubscriptions)) {
        mapSubscriptions(subscriptions);
      }
      
      return _.findWhere(mappedSubscriptions, { uuid: uuid });
    }

    function mapSubscriptions(subscriptions) {
      _.each(subscriptions, function(subscription) {
        subscriptions.push(subscription.category);
        subscriptions.push(subscription.geography);
      });

      mappedSubscriptions = subscriptions;
    }

    function segmentSettings() {
      $state.go('analytics.organization.marketIntelligence.edit');
    }

    function loadData() {
      var requestParams = getRequestParams(vm.params);

      marketIntelligenceService.getIndexData(requestParams, vm.showOrgIndex).then(function (res) {

        vm.hasData = true;
        vm.requestFailed = false;

        var tempSegments = [];

        for (var i = 0; i < 5; i++) {
          var indexDataObj = {};

          var itemAtPosition = _.findWhere(vm.params.subscriptions, { positionIndex: i });

          if(_.isUndefined(itemAtPosition)) {
            indexDataObj.geographyName = 'Empty';
            tempSegments.push(indexDataObj);
            continue;
          }

          var indexMatch = getSubscriptionMatch(res.index, itemAtPosition);

          if(vm.showOrgIndex === true) {
            var orgMatch = getSubscriptionMatch(res.org, itemAtPosition);
            
            indexDataObj.orgIndexChange = orgMatch.value * 100;
            
            if (orgMatch.valid === false && !ObjectUtils.isNullOrUndefined(orgMatch.errorMessage)) {
              indexDataObj.orgErrorMessage = true;
            } else {
              indexDataObj.orgErrorMessage = false;
            }

            getIndexColor(orgMatch, indexDataObj, 'orgIndexColor');
            getIndexIcon(orgMatch, indexDataObj, 'orgIconClass');
          }
          
          if(ObjectUtils.isNullOrUndefined(indexMatch)) {
            indexDataObj.geographyName = 'Empty';
            tempSegments.push(indexDataObj);
            continue;
          }

          indexDataObj.geographyName = getSegmentName(indexMatch.subscription);
          indexDataObj.orgName = getOrgSegmentName(indexMatch.subscription);
          indexDataObj.marketIndexChange = indexMatch.value * 100;
          
          if (indexMatch.valid === false && !ObjectUtils.isNullOrUndefined(indexMatch.errorMessage)) {
            indexDataObj.indexErrorMessage = true;
          } else {
            indexDataObj.indexErrorMessage = false;
          }

          getIndexColor(indexMatch, indexDataObj, 'marketIndexColor');
          getIndexIcon(indexMatch, indexDataObj, 'marketIconClass');
          tempSegments.push(indexDataObj);
        }

        vm.kpiSegmentsArray = tempSegments;

        vm.isLoading = false;
      }).catch(function(error) {
         console.error(error);
        vm.isLoading = false;

        if(error === 'No Data') {
          vm.requestFailed = false;
          vm.hasData = false;
        } else {
          vm.requestFailed = true;
          vm.hasData = true;
        }
      });
    }

    function getIndexIcon(indexMatch, indexDataObj, propName) {
      if(indexMatch.value === 0) {
        indexDataObj[propName] = 'sticon-no-change-large';
      }

      if(indexMatch.value < 0) {
        indexDataObj[propName] = 'sticon-triangle-down';
      }

      if(indexMatch.value > 0) {
        indexDataObj[propName] = 'sticon-triangle-up';
      }
    }

    function getIndexColor(indexMatch, indexDataObj, propName) {
      if(indexMatch.value === 0) {
        indexDataObj[propName] = 'no-change';
      }

      if(indexMatch.value < 0) {
        indexDataObj[propName] = 'negative';
      }

      if(indexMatch.value > 0) {
        indexDataObj[propName] = 'positive';
      }
    }

    function getSubscriptionMatch(indexes, itemAtPosition) {
      var indexMatches = _.filter(indexes, function(miIndex) {
        return miIndex.subscription.category.uuid === itemAtPosition.category.uuid &&
          miIndex.subscription.geography.uuid === itemAtPosition.geography.uuid
      });

      return indexMatches[0];
    }

    function getSegmentName(subscription) {
      var segmentName = subscription.geography.name;

      segmentName += ' ';

      segmentName += subscription.category.name;

      return segmentName;
    }

    function getOrgSegmentName(subscription) {
      // The org section does not filter by category.
      // Instead, the orgId and geography is used
      return vm.org.name + ' ' + subscription.geography.name;
    }

    function getRequestParams(params) {
      var requestParams = angular.copy(params);

      _.each(requestParams.subscriptions, function(subscription) {
        delete subscription.positionIndex
      });

      return requestParams;
    }
  }
})();
