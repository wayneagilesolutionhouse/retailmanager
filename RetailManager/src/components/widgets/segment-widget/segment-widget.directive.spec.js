'use strict';

describe('segmentWidget', function() {
  var $scope;
  var controller;

  beforeEach(module('shopperTrak', function($translateProvider) {
    $translateProvider.translations('en_US', {});
  }));

  beforeEach(inject(function($rootScope, $templateCache, $compile) {
    $scope = $rootScope.$new();

    // Put an empty template to cache to prevent Angular from fetching it
    $templateCache.put(
      'components/widgets/segment-widget/segment-widget.partial.html',
      '<div></div>'
    );

    var element = angular.element(
      '<segment-widget'>'</segment-widget>'
    );

    $compile(element)($scope);
    $scope.$digest();
    controller = element.controller('segmentWidget');
  }));

});
