(function () {
  'use strict';

  angular
    .module('shopperTrak.widgets')
    .directive('powerHoursWidget', powerHoursWidget)
    .controller('powerHoursWidgetController', powerHoursWidgetController);

  function powerHoursWidget() {
    return {
      templateUrl: 'components/widgets/power-hours-widget/power-hours-widget.partial.html',
      scope: {
        zoneId: '=',
        dateRangeStart: '=',
        dateRangeEnd: '=',
        activeOption: '=?',
        currentUser: '=',
        orgId: '=?',
        currentOrganization: '=?',
        siteId: '=?',
        currentSite: '=?',
        operatingHours: '=',
        language: '=',
        hideExportIcon: '=',
        onExportClick: '&',
        exportIsDisabled: '=?',
        summaryKey: '@',
        currencySymbol: '=?',
        isExport: '=?',
        setSelectedWidget: '&',
        isLoading: '=?',
        salesCategories: '=?'
      },
      controller: powerHoursWidgetController,
      controllerAs: 'vm',
      bindToController: true
    };
  }

  powerHoursWidgetController.$inject = [
    '$scope',
    '$rootScope',
    'requestManager',
    'apiUrl',
    'LocalizationService',
    'SubscriptionsService',
    '$translate',
    '$filter',
    'metricConstants',
    'ObjectUtils',
    'currencyService',
    'SiteResource',
    'OrganizationResource',
  ];

  function powerHoursWidgetController($scope,
    $rootScope,
    requestManager,
    apiUrl,
    LocalizationService,
    SubscriptionsService,
    $translate,
    $filter,
    metricConstants,
    ObjectUtils,
    currencyService,
    SiteResource,
    OrganizationResource) {
    var vm = this; // 'vm' stands for 'view model'
    var weekDayTransKey = 'weekdaysShort.';

    var fetchDataDebounced = _.debounce(function() {
      fetchData();
    }, 200);

    activate();

    function activate() {
      vm.showNodata = false;

      vm.groupChange = $scope.$watchGroup([
        'vm.currentOrganization',
        'vm.currentSite',
        'vm.dateRangeStart',
        'vm.dateRangeEnd',
        'vm.siteId',
        'vm.orgId',
      ], getOrganizationParameters);

      vm.salesChange = $scope.$watchGroup([
        'vm.salesCategories'
      ], fetchData);

      vm.loadingCHange = $scope.$watchGroup(['vm.totalsAreLoading','vm.dataIsLoading', 'vm.isColourLoading'], function() {
        if(vm.totalsAreLoading === false && vm.dataIsLoading === false && vm.isColourLoading === false) {
          vm.isLoading = false;
        } else {
          vm.isLoading = true;
        }
        setNodata();
      });

      setTotalLabel();

      $scope.$on('$destroy', function () {
        if (typeof vm.groupChange === 'function') {
          vm.groupChange();
        }

        if (typeof vm.salesChange === 'function') {
          vm.salesChange();
        }
      });

      getCurrentSite();

      getCurrentOrganization();

      loadTranslations();

      if (ObjectUtils.isNullOrUndefined($scope.getRequestParams)) {
        $scope.getRequestParams = getRequestParams;
      }

      vm.isLoading = true;

      vm.requestFailed = false;

      setDays();

      vm.numberFormatName = LocalizationService.getCurrentNumberFormatName(vm.currentUser, vm.currentOrganization);
      setCurrencySymbol();
    }

    function getCurrentSite() {
      if (!ObjectUtils.isNullOrUndefined(vm.currentSite) || ObjectUtils.isNullOrUndefined(vm.siteId )) {
        return;
      }

      SiteResource.get({
        orgId: vm.orgId,
        siteId: vm.siteId
      }).$promise.then(function (result) {
        vm.currentSite = result;
      });
    }

    function getCurrentOrganization() {
      if (!ObjectUtils.isNullOrUndefined(vm.currentOrganization) || ObjectUtils.isNullOrUndefined(vm.orgId )) {
        return;
      }

      OrganizationResource.get({
        orgId: vm.orgId
      }).$promise.then(function (result) {
        vm.currentOrganization = result;
        LocalizationService.setOrganization(vm.currentOrganization);
        vm.numberFormatName = LocalizationService.getCurrentNumberFormatName(vm.currentUser, vm.currentOrganization);
      });
    }

    function setDays() {
      var days = [];

      for (var i = 0; i < 7; i++) {
        days.push({
          day: i,
          key: getDayKey(i),
          hasData: false,
          dailyTotal: 0
        });
      }

      if (vm.firstDay === 1) {
        var last = days.shift();

        days.push(last);
      }

      vm.days = days;
    }

    vm.getDayData = function (hour, day) {
      if (!ObjectUtils.isNullOrUndefinedOrEmpty(vm.hours)) {
        var hourData = _.find(vm.hours, function (hourItem) {
          return hourItem.hour === hour.hour;
        });

        if (ObjectUtils.isNullOrUndefined(hourData)) {
          return '';
        }

        var dayData = _.find(hourData.dayValues, function (dayItem) {
          return weekDayTransKey + dayItem.dayName.toLowerCase() === day.key;
        });

        if (!ObjectUtils.isNullOrUndefined(dayData)) {
          return dayData.total;
        }
      }

      return '';
    };

    function addDayTotalValue(day, value) {
      if (ObjectUtils.isNullOrUndefined(value)) {
        return;
      }

      var daysItem = _.find(vm.days, function (dayItem) {
        return dayItem.key === weekDayTransKey + day.toLowerCase();
      });

      if (!ObjectUtils.isNullOrUndefined(daysItem)) {
        daysItem.dailyTotal += value;

        daysItem.hasData = true;
      }
    }

    function setNumberOfDaysHasData() {
      if (isTraffic()) {
        var days = _.filter(vm.days, function (dayItem) {
          return dayItem.hasData === true;
        });

        if (!ObjectUtils.isNullOrUndefined(days)) {
          vm.numOfWeekDaysThatHaveData = days.length;
        }
      }
    }

    function setNumberOfDaysHasDataForTrafficColor() {
      var days = _.filter(vm.days, function (dayItem) {
        return dayItem.hasData === true;
      });

      if (!ObjectUtils.isNullOrUndefined(days)) {
        vm.numOfWeekDaysThatHaveData = days.length;
      }
    }

    function getOrganizationParameters() {
      if (ObjectUtils.isNullOrUndefined(vm.currentSite) || ObjectUtils.isNullOrUndefined(vm.currentOrganization)) {
        return;
      }

      setOptions();

      var thresholds = getThresholds(vm.currentOrganization);
      vm.lowThreshold = thresholds.lower;
      vm.highThreshold = thresholds.upper;

      vm.firstDay = LocalizationService.getCurrentCalendarFirstDayOfWeek();

      setDays();

      fetchData();

      fetchTrafficDataForColourSetting();
    }

    vm.setOption = function (option) {
      if (ObjectUtils.isNullOrUndefined(option) ||
        (!ObjectUtils.isNullOrUndefined(vm.selectedOption) &&
          vm.selectedOption.name === option.name)) {
        return;
      }

      vm.selectedOption = option;
      vm.activeOption = option.metric.shortTranslationLabel;

      fetchDataDebounced();
    };

    function getMetric(id) {
      var metric = angular.copy(_.findWhere(metricConstants.metrics, { value: id }));

      if (metric.isCurrency && !ObjectUtils.isNullOrUndefinedOrBlank(vm.currencySymbol)) {
        metric.prefixSymbol = vm.currencySymbol;
      }
      return metric;
    }

    function getOptionFromTranslationLabel(shortTranslatioinLabel) {
      if (typeof vm.options !== 'object') {
        return;
      }

      return _.find(vm.options, function (option) {
        return option.metric.shortTranslationLabel === shortTranslatioinLabel;
      });
    }

    function setOptions() {
      vm.options = [
        {
          name: 'average_traffic',
          displayType: 'traffic',
          propertyName: 'total_traffic',
          metric: getMetric('traffic'),
          calculateAverage: true
        },
        {
          name: 'traffic (pct)',
          displayType: 'traffic',
          propertyName: 'period_traffic_percent',
          metric: getMetric('traffic (pct)'),
          calculateAverage: false // This one appears to be ok.
        }
      ];

      var saleSubscription = SubscriptionsService.siteHasSales(vm.currentOrganization, vm.currentSite);

      if (saleSubscription) {
        vm.options.push({
            name: 'average_sales',
            displayType: 'sales',
            propertyName: 'hourly_sales',
            metric: getMetric('sales'),
            calculateAverage: true
          },{
            name: 'conversion',
            displayType: 'conversion',
            propertyName: 'hourly_conversion',
            metric: getMetric('conversion'),
            calculateAverage: true
          },{
            name: 'ats',
            displayType: 'ats',
            propertyName: 'hourly_ats',
            metric: getMetric('ats'),
            calculateAverage: true
        });

        var laborSubscription = SubscriptionsService.siteHasLabor(vm.currentOrganization, vm.currentSite);

        if (laborSubscription) {
          vm.options.push({
            name: 'star',
            displayType: 'star',
            propertyName: 'hourly_star',
            metric: getMetric('star'),
            calculateAverage: true
          });
        }
      }

      var option = !ObjectUtils.isNullOrUndefinedOrBlank(vm.activeOption) ? getOptionFromTranslationLabel(vm.activeOption) : vm.options[0];

      vm.setOption(option);
    }

    function setNodata() {
      vm.showNoData = vm.isLoading === false &&
        vm.requestForColorFailed === false &&
        vm.requestFailed === false &&
        vm.periodHasData === false;
    }

    function getRequestParamsForTraffic() {
      var params = getRequestParams();
      params.kpi = _.findWhere(vm.options, { displayType: 'traffic' }).displayType;
      params.sales_category_id = undefined;
      return params;
    }

    function setSalesCategoryIdParam(params) {
        if (!_.isUndefined(vm.salesCategories) && !_.isEmpty(vm.salesCategories) && _.min(vm.salesCategories.id) > 0) {
          params.sales_category_id = _.pluck(vm.salesCategories, 'id');
        }
    }

    function getRequestParams() {
      var params = {
        kpi: vm.selectedOption.displayType,
        countType: 'enters',
        reportStartDate: vm.dateRangeStart.toISOString(),
        reportEndDate: vm.dateRangeEnd.toISOString(),
        orgId: vm.currentOrganization.organization_id,
        siteId: vm.currentSite.site_id,
        // Hard-code basePercentage.
        // It is required by API
        // because this widget doesn't use power hour ind fields in the response
        basePercentage: 0.15
      };

      if (!ObjectUtils.isNullOrUndefined(vm.zoneId)) {
        params.zoneId = vm.zoneId;
      }

      if (!ObjectUtils.isNullOrUndefined(vm.operatingHours)) {
        params.operatingHours = vm.operatingHours;
      }

      if (typeof vm.salesCategories !== 'undefined' && vm.salesCategories.length > 0 && _.min(vm.salesCategories.id) > 0) {
        params.sales_category_id = vm.salesCategories.map(function(category) {
          return category.id;
        });
      }

      return params;
    }

    function getRequestParamsForTotals() {
      var params = {
        reportStartDate: vm.dateRangeStart.toISOString(),
        reportEndDate: vm.dateRangeEnd.toISOString(),
        orgId: vm.currentOrganization.organization_id,
        siteId: vm.currentSite.site_id,
        groupBy: 'day_of_week',
        kpi: vm.selectedOption.displayType
      };

      setSalesCategoryIdParam(params);
      return params;
    }

  /**
   * Returns true if the current selected metric is calculated.
   *
   * @returns {Boolean} The result
   */
    function selectedMetricIsCalculated() {
      var isTotalRequired = _.contains(['ats', 'star', 'conversion'], vm.selectedOption.displayType);

      return !ObjectUtils.isNullOrUndefinedOrEmpty(vm.hours) && isTotalRequired;
    }

  /**
   * Gets the totals from the API rather than calculating them in this controller.
   * Should be called for calculated metrics, like ATS, Conversion and STAR
   *
   * @returns {Object} A momentJs object that is the first day of the year
   */
    function getTotals() {
      vm.totalsAreLoading = true;

      var requestParams = getRequestParamsForTotals();

      // As this function is only called for ATS, STAR and Conversion, we can hit the sales endpoint
      requestManager.get(apiUrl + '/kpis/sales', {
        params: requestParams
    })
        .then(updateDataModelTotals, handleRequestError);
    }

    function getItemValue(item) {
      var value = item[vm.selectedOption.name];

      return parseFloat(isValid(value) ? value : 0);
    }

    function setDayTotalValue(key, value) {
      if (ObjectUtils.isNullOrUndefined(value)) {
        return;
      }

      var daysItem = _.find(vm.days, function (dayItem) {
        return weekDayTransKey + key.toLowerCase() === dayItem.key;
      });

      if (!ObjectUtils.isNullOrUndefined(daysItem)) {
        daysItem.dailyTotal = value;
      }
    }

    function updateDataModelTotals(response) {
      var items = response.result;

      _.each(items, function (item) {
        setDayTotalValue(item.period_start_date, getItemValue(item));
      });
      vm.totalsAreLoading = false;

      vm.requestFailed = false;
    }

    function fetchData() {
      if (ObjectUtils.isNullOrUndefined(vm.selectedOption)) {
        return;
      }

      vm.dataIsLoading = true;

      var requestParams = $scope.getRequestParams();

      return requestManager.get(apiUrl + '/kpis/traffic/powerHours', {
        params: requestParams
      }).then(updateDataModel, handleRequestError);
    }

    function fetchTrafficDataForColourSetting() {
      vm.isColourLoading = true;

      var requestParams = getRequestParamsForTraffic();

      requestManager.get(apiUrl + '/kpis/traffic/powerHours', {
        params: requestParams
      }).then(updateDataModelWithTrafficColour, handleColorRequestError);
    }

    function handleColorRequestError() {
      vm.isColourLoading = false;

      vm.requestForColorFailed = true;
    }

    function updateDataModelWithTrafficColour(response) {
      var totalTraffic = 0;

      var items = response.result;

      var hours = [];

      if (!ObjectUtils.isNullOrUndefinedOrEmpty(items)) {
        var groupedHourItems = getGroupedHourItems(items);
        _.each(groupedHourItems, function (hourItem) {
          hours.push(getHourData(hourItem, vm.options[0].propertyName, true, false));
        });
        _.each(hours, function (hourItem) {
          _.each(hourItem.dayValues, function(hourDayItem) {
            totalTraffic += hourDayItem.summedData;
          });
        });

        hours = orderHours(hours);
      }

      setNumberOfDaysHasDataForTrafficColor();

      setVmHoursTrafficClass(hours, totalTraffic);

      clearColourFlagsAndSetNoData();
    }

    function clearColourFlagsAndSetNoData() {
      vm.isColourLoading = false;

      vm.requestForColorFailed = false;

      setNodata();
    }

    function setVmHoursTrafficClass(hours, totalTraffic) {
      if (ObjectUtils.isNullOrUndefined(vm.hours)) {
        vm.cachedTrafficClassHours = hours;
        vm.cachedTotalTraffic = totalTraffic;
        return;
      }
      _.each(hours, function (hourItem) {
        var vmHour = _.findWhere(vm.hours, { hour: hourItem.hour });
        if (!ObjectUtils.isNullOrUndefined(vmHour) && !ObjectUtils.isNullOrUndefined(vmHour.dayValues)) {
          _.each(hourItem.dayValues, function (dayItem) {
            var colorClass = getTrafficClass(dayItem, totalTraffic);
            var hourDayItem = _.findWhere(vmHour.dayValues, { dayName: dayItem.dayName });
            hourDayItem.colorClass = colorClass;
          });
        }
      });
    }

    function getItemData(item, propertyName) {
      var value = item[propertyName];

      return parseFloat(isValid(value) ? value : 0);
    }

    function isValid(value) {
      return !ObjectUtils.isNullOrUndefinedOrBlank(value);
    }

    function getGroupedHourItems(items) {
      return _.groupBy(items, function (item) {
        return moment.utc(item.period_start_date).hour();
      });
    }

    function getGroupedHourDayItems(items) {
      return _.groupBy(items, function (item) {
        return item.dow_name;
      });
    }

    vm.getColorClass = function (hour, day) {
      if (!ObjectUtils.isNullOrUndefined(vm.hours)) {
        var hourData = _.find(vm.hours, function (hourItem) {
          return hourItem.hour === hour.hour;
        });

        if (ObjectUtils.isNullOrUndefined(hourData)) {
          return '';
        }

        var dayData = _.find(hourData.dayValues, function (dayItem) {
          return weekDayTransKey + dayItem.dayName.toLowerCase() === day.key;
        });

        if (!ObjectUtils.isNullOrUndefined(dayData)) {
          if (ObjectUtils.isNullOrUndefinedOrBlank(dayData.colorClass)) {
            return 'traffic-indicator has-low-traffic';
          }
          return 'traffic-indicator ' + dayData.colorClass;
        }
      }

      return '';
    };

    function getDayDataForHourFromDataList(hourDayItems, propertyName) {
      var data = {
        dayName: hourDayItems[0].dow_name,
        summedData: 0,
        frequency: hourDayItems.length,
        total: 0,
        colorClass: ''
      };

      _.each(hourDayItems, function (hourDayItem) {
        data.summedData += getItemData(hourDayItem, propertyName);
      });

      if (vm.selectedOption.calculateAverage) {
          data.total = data.summedData / data.frequency;
      } else {
          data.total = data.summedData;
      }

      return data;
    }

    function isTraffic() {
      return vm.selectedOption.displayType === 'traffic';
    }

    function getDayDataForHour(hourDayItem, propertyName) {
      if(!ObjectUtils.isNullOrUndefined(hourDayItem.length)) {
        return getDayDataForHourFromDataList(hourDayItem, propertyName);
      }

      var value = getItemData(hourDayItem, propertyName);

      return {
        dayName: hourDayItem.dow_name,
        summedData: value,
        frequency: 1,
        total: value,
        colorClass: ''
      };
    }

    function isPeriodHasData(items) {
      vm.periodHasData = !ObjectUtils.isNullOrUndefined(items) && items.length > 0;

      return vm.periodHasData;
    }

    /**
    * Calculate daily averages and totals for each hour
    * for display in the Total and Daiy Average
    *
    */
    function calculateDailyAveragesAndTotals(hours) {
      vm.summedAverage = _.chain(hours).pluck('total').reduce(function (mem, val){ return mem + val; }, 0).value();
      if (!vm.selectedOption.calculateAverage) {
        return;
      }

      // which metrics to calculate the average of for Daily Average row
      if (selectedMetricIsCalculated() || vm.selectedOption.propertyName === 'period_traffic_percent') {
        _.each(vm.days, function (dayItem) {
          dayItem.dailyTotal = dayItem.dailyTotal / hours.length;
        });
      }
    }

    /**
    * Decide whether of not to display the Total column in chart
    *
    */
    function setTotalColumnDisplay() {
        vm.showTotalColumn = _.contains(['average_sales', 'average_traffic', 'traffic (pct)'], vm.selectedOption.name);
    }

    function initValues() {
      vm.summedAverage = 0;

      setTotalColumnDisplay()

      _.each(vm.days, function (daysItem) {
        daysItem.dailyTotal = 0;

        if (isTraffic()) {
          daysItem.hasData = false;
        }
      });
    }

    function updateDataModel(response) {
      var items = response.result;

      vm.testData = _.pluck(items, 'period_start_date');

      initValues();

      var hours = [];

      if (isPeriodHasData(items)) {

        var groupedHourItems = getGroupedHourItems(items);

        var index = 0;

        // We need to add an index here to stop the ordering screwing up in the PDF export
        // wkthmltopdf---

        _.each(groupedHourItems, function (hourItem) {
          var hour = getHourData(hourItem, vm.selectedOption.propertyName);
          hour.index = index;
          hours.push(hour);
          index++;
        });

        setTotals(hours);

        hours = orderHours(hours);

        calculateDailyAveragesAndTotals(hours);
      }

      copyHoursToVmPreservingColor(hours);

      setNumberOfDaysHasData();

      // if vm.cachedTrafficClassHours is defined, traffic class data was loaded, but vm.hours was not created yet
      //   now that vm.hours is created, apply traffic class data to vm.hours
      if (vm.cachedTrafficClassHours) {
        setVmHoursTrafficClass(vm.cachedTrafficClassHours, vm.cachedTotalTraffic);
        vm.cachedTrafficClassHours = null;
        vm.cachedTotalTraffic = null;
      }

      vm.dataIsLoading = false;
      if (selectedMetricIsCalculated()) {
        return getTotals();
      } else {
        vm.totalsAreLoading = false;
      }

      clearLoadingFlagsAndSetNoData();
    }

    function clearLoadingFlagsAndSetNoData() {
      vm.dataIsLoading = false;

      vm.requestFailed = false;

      setNodata();
    }

    function copyHoursToVmPreservingColor(sourceHours) {
      if (ObjectUtils.isNullOrUndefined(vm.hours)) {
        vm.hours = sourceHours;
        return;
      }
      vm.hours = _.map(vm.hours, function (destHourItem) {
        var sourceHour = _.findWhere(sourceHours, { hour: destHourItem.hour });
        if (!ObjectUtils.isNullOrUndefined(sourceHour) && !ObjectUtils.isNullOrUndefinedOrEmpty(sourceHour.dayValues)) {
          var destDayValues = [];
          _.each(destHourItem.dayValues, function (destDayItem) {
            var sourceHourDayItem = _.findWhere(sourceHour.dayValues, { dayName: destDayItem.dayName });
            if (!ObjectUtils.isNullOrUndefined(sourceHourDayItem)) {
              var preservedColorClass = destDayItem.colorClass;
              var newDestDayItem = sourceHourDayItem;
              newDestDayItem.colorClass = preservedColorClass;
              destDayValues.push(newDestDayItem);
            }
          });
          var destHour = sourceHour;
          destHour.dayValues = destDayValues;
          return destHour;
        }
      });
    }

    function getTrafficClass(dayData, totalTraffic) {
      // Traffic class is based on value down to 1/10th of a percent
      var precision = 3;
      var value = dayData.summedData / totalTraffic;
      value = value.toFixed(precision);

      var low = vm.lowThreshold / vm.numOfWeekDaysThatHaveData * 7;
      var high = vm.highThreshold / vm.numOfWeekDaysThatHaveData * 7;

      if (value < low) {
        return 'has-low-traffic';
      }

      if (value < high && value >= low) {
        return 'has-medium-traffic';
      }

      return 'has-high-traffic';
    }

    function setTotals(hours) {
      _.each(hours, function (hourItem) {
        _.each(hourItem.dayValues, function(hourDayItem) {
          addDayTotalValue(hourDayItem.dayName, hourDayItem.total);
        });
      });
    }

    function getHourData(hourItem, propertyName) {
      var groupedHourDayItems = getGroupedHourDayItems(hourItem);

      var utc_hour = moment.utc(hourItem[0].period_start_date).hour();

      var hour = {
        yAxisLabel: utc_hour + '-' + (utc_hour + 1),
        dayValues: [],
        hour: utc_hour,
        total: 0
      };

      hour.dayValues = getHourDayValues(groupedHourDayItems, propertyName);

      hour.total = getHourTotal(hour);

      return hour;
    }

    function getHourDayValues(groupedHourDayItems, propertyName) {
      var dayValues = [];
      _.each(groupedHourDayItems, function (hourDayItems) {
        dayValues.push(getDayDataForHour(hourDayItems, propertyName));
      });

      return dayValues;
    }

    function getHourTotal(hour) {
      var total = 0;

      _.each(hour.dayValues, function (value) {
        total += value.total;
      });

      return total;
    }

    function orderHours(hours) {
      var startHour = vm.currentSite.business_day_start_hour || 0;

      var startHours = hours.slice(0, startHour);
      var endHours = hours.slice(startHour);

      var newHours = endHours.concat(startHours);

      hours = newHours;

      for (var i = 0; i < hours.length; i++) {
        hours[i].index = i;
        hours[i].hour = startHour + i;
      }

      return hours;
    }

    function handleRequestError() {
      vm.isLoading = false;

      vm.requestFailed = true;
    }

    function getDayKey(weekDay) {
      var weekday = moment().day(weekDay).format('ddd').toLowerCase();

      return weekDayTransKey + weekday;
    }

    function loadTranslations() {
      vm.kpiTitle = 'kpis.totalLabel.power_hours';
      vm.widgetTitle = 'kpis.kpiTitle.power_hours';
    }

    // All metrics should show Daily Average as label
    function setTotalLabel() {
      vm.averageMetricSelected = true;
    }

    function setCurrencySymbol() {
      if (!ObjectUtils.isNullOrUndefinedOrBlank(vm.orgId) && !ObjectUtils.isNullOrUndefined(vm.siteId) && ObjectUtils.isNullOrUndefinedOrBlank(vm.currencySymbol)) {
        currencyService.getCurrencySymbol(vm.orgId, vm.siteId).then(function (data) {
          vm.currencySymbol = data.currencySymbol;
        });
      }
    }

    function getThresholds(organization) {
      if(typeof ObjectUtils.getNestedProperty(organization,'power_hours_thresholds') === 'undefined') {
        return { lower: 0.005, upper: 0.015 };
      }

      var lower = organization.power_hours_thresholds.lower;
      var upper = organization.power_hours_thresholds.upper;

      return { lower: lower, upper: upper };
    }
  }
})();
