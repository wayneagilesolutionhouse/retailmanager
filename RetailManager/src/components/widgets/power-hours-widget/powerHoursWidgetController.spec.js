'use strict';

describe('powerHoursWidgetCtrl', function () {
  var $scope;
  var $controller;
  var LocalizationService;
  var OrganizationResourceMock;
  var TranslateMock;
  var FilterMock;
  var http;
  var timeout;
  var apiUrl = 'https://api.url';
  var q;
  var httpBackend;
  var constants;

  beforeEach(module('shopperTrak', function ($translateProvider) {
    $translateProvider.translations('en_US', {});
  }));

  beforeEach(module('shopperTrak.widgets'));
  beforeEach(module('shopperTrak.widgets.mock'));

  beforeEach(module(function ($provide) {
    $provide.constant('apiUrl', apiUrl);

    var localStorageService = {
      set: function(key, value) { },
      get: function(key) {
        return undefined;
      },
      keys: function() {
        return [];
      }
    };

    $provide.value('localStorageService', localStorageService);

    var googleAnalytics = {
      sendRequestTime: function () {
        angular.noop()
      }
    };

    $provide.value('googleAnalytics', googleAnalytics);
  }));

  beforeEach(inject(function ($rootScope, _$controller_, _LocalizationService_,
    $translate, $filter, $http, $q, $httpBackend, $timeout) {
    $scope = $rootScope.$new();
    http = $http;
    $controller = _$controller_;
    timeout = $timeout;
    LocalizationService = _LocalizationService_;
    q = $q;
    httpBackend = $httpBackend;
    TranslateMock = $translate;
    FilterMock = $filter;
    spyOn(LocalizationService, 'getCurrentDateFormat');

    _.debounce = function(func) {
      return function() {
        func();
      }
    }
  }));

  it('should create controller and init values', function () {

    var controller = createController();

    expect(controller.isLoading).toBe(true);

    expect(controller.requestFailed).toBe(false);

    expect(controller.numberFormatName).toBe(null);
  });

  it('should get organization parameters when group watch fire and selected option is traffic it should set hour data', function () {
    var data = {
      "result": [{ "organization_id": 1145, "site_id": 80064248, "zone_id": 433621, "period_start_date": "2016-06-05T00:00:00.000Z", "dow_name": "Sun", "total_traffic": "39", "period_traffic_percent": 0.0567181978156222, "power_hour_ind": false }, { "organization_id": 1145, "site_id": 80064248, "zone_id": 433621, "period_start_date": "2016-06-05T01:00:00.000Z", "dow_name": "Sun", "total_traffic": "24", "period_traffic_percent": 0.0349035063480752, "power_hour_ind": false }]
    };

    var url = 'https://api.url/kpis/traffic/powerHours?countType=enters&kpi=traffic&orgId=6240&percentage=0.15&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=80029997';

    httpBackend.whenGET(url).respond(data);

    var url1 = 'https://api.url/kpis/traffic/powerHours?basePercentage=0.15&countType=enters&kpi=traffic&operatingHours=true&orgId=1154&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=56976';

    httpBackend.whenGET(url1).respond(data);
    httpBackend.whenGET('https://api.url/organizations').respond({});

    setRequestParams('traffic', 6240, 80029997);

    var controller = createController(1154, 56976, false, false, true, true);

    $scope.$apply();

    httpBackend.flush();

    expect(controller.requestFailed).toBe(false);
    expect(controller.summedAverage).toBe(63);
    expect(controller.hours[0].dayValues[0].summedData).toBe(39);
  });

  it('should data from api and set hours for vm when setoption called', function () {
    var saleData = { "result": [{ "organization_id": 6240, "site_id": 80064248, "zone_id": 433621, "period_start_date": "2016-06-05T00:00:00.000Z", "dow_name": "Sun", "hourly_sales": "39", "period_sales_percent": 0.0567181978156222, "power_hour_ind": false }, { "organization_id": 6240, "site_id": 80064248, "zone_id": 433621, "period_start_date": "2016-06-05T01:00:00.000Z", "dow_name": "Sun", "hourly_sales": "24", "period_sales_percent": 0.0349035063480752, "power_hour_ind": false }] };

    var url = 'https://api.url/kpis/traffic/powerHours?basePercentage=0.15&countType=enters&kpi=traffic&operatingHours=true&orgId=6240&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=56976';

    httpBackend.whenGET(url).respond(saleData);

    var url1 = 'https://api.url/kpis/traffic/powerHours?countType=enters&kpi=sales&orgId=6240&percentage=0.15&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=80029997';

    httpBackend.whenGET(url1).respond(saleData);
    httpBackend.whenGET('https://api.url/organizations').respond({});

    setRequestParams('sales', 6240, 80029997);

    var controller = createController(6240, 56976, false, false, true, true);

    $scope.$digest();

    $scope.$apply();

    httpBackend.flush();

    controller.setOption(getOption('sales'));

    $scope.$digest();

    $scope.$apply();

    httpBackend.flush();

    expect(controller.requestFailed).toBe(false);

    expect(controller.summedAverage).toBe(63);

    expect(controller.hours[0].dayValues[0].summedData).toBe(39);

  });

  it('should data from api and set hours for vm when setoption called if active calendar setting first day is mon so it should order days with starting day and if each day has different starting hour it should find the smallest hour and order with it', function() {
    var data = {
      result :[{
        dow_name: 'Sun',
        organization_id:5687,
        period_start_date:'2016-08-07T10:00:00.000Z',
        period_traffic_percent:0.0731930466605673,
        power_hour_ind:false,
        site_id:10097278,
        total_traffic:'4',
        zone_id:373769
      },{
        dow_name:'Sun',
        organization_id:5687,
        period_start_date:'2016-08-07T11:00:00.000Z',
        period_traffic_percent:0.0831930466605673,
        power_hour_ind:false,
        site_id:10097278,
        total_traffic:'4',
        zone_id:373769
      },{
        dow_name: 'Mon',
        organization_id: 5687,
        period_start_date: '2016-08-08T09:00:00.000Z',
        period_traffic_percent: 0.0731930466605673,
        power_hour_ind: false,
        site_id: 10097278,
        total_traffic: '4',
        zone_id: 373769
      },{
        dow_name:'Mon',
        organization_id:5687,
        period_start_date:'2016-08-08T10:00:00.000Z',
        period_traffic_percent:0.0831930466605673,
        power_hour_ind:false,
        site_id:10097278,
        total_traffic:'4',
        zone_id:373769
      },{
        dow_name:'Mon',
        organization_id: 5687,
        period_start_date: '2016-08-08T11:00:00.000Z',
        period_traffic_percent: 0.0631930466605673,
        power_hour_ind: false,
        site_id: 10097278,
        total_traffic: '4',
        zone_id: 373769
      }]
    };

    var url='https://api.url/kpis/traffic/powerHours?basePercentage=0.15&countType=enters&kpi=traffic&operatingHours=true&orgId=1154&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=56976';

    var url1 = 'https://api.url/kpis/traffic/powerHours?countType=enters&kpi=traffic&orgId=6240&percentage=0.15&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=80029997'

    httpBackend.whenGET(url).respond(data);

    httpBackend.whenGET(url1).respond(data);

    setRequestParams('traffic', 6240, 80029997);

    LocalizationService.getCurrentCalendarFirstDayOfWeek= function(){
      return 1;
    };

    var controller = createController(1154, 56976, false, false, true, true, 9);

    $scope.$apply();

    httpBackend.flush();
    expect(controller.days[0].day).toBe(1);
    expect(controller.hours[0].hour).toBe(9);
    expect(controller.requestFailed).toBe(false);
    expect(controller.summedAverage).toBe(20);
    expect(controller.hours[0].dayValues[0].summedData).toBe(4);

  });

  it('should set color class same as traffic for non traffic data', function() {
    var data = {
      "result":
      [{ "organization_id": 1145, "site_id": 80064248, "zone_id": 433621, "period_start_date": "2016-06-05T00:00:00.000Z", "dow_name": "Sun", "total_traffic": "39", "period_traffic_percent": 0.0567181978156222, "power_hour_ind": false }, { "organization_id": 1145, "site_id": 80064248, "zone_id":      433621, "period_start_date": "2016-06-05T01:00:00.000Z", "dow_name": "Sun", "total_traffic": "24", "period_traffic_percent": 0.0349035063480752, "power_hour_ind": false
      }]
    };

    var url = 'https://api.url/kpis/traffic/powerHours?countType=enters&kpi=sales&orgId=6240&percentage=0.15&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=80029997';
    var url1 = 'https://api.url/kpis/traffic/powerHours?basePercentage=0.15&countType=enters&kpi=traffic&operatingHours=true&orgId=1154&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=56976';
    var url2 = 'https://api.url/kpis/traffic/powerHours?countType=enters&kpi=traffic&orgId=6240&percentage=0.15&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=80029997';
    httpBackend.whenGET(url).respond(data);
    httpBackend.whenGET(url1).respond(data);
    httpBackend.whenGET(url2).respond(data);
    httpBackend.whenGET('https://api.url/organizations').respond({});

    setRequestParams('traffic', 6240, 80029997);

    var controller = createController(1154, 56976, false, false, true, true);

    $scope.$apply();

    httpBackend.flush();

    expect(controller.requestFailed).toBe(false);
    expect(controller.summedAverage).toBe(63);
    expect(controller.hours[0].dayValues[0].summedData).toBe(39);

    var d1 = angular.copy(controller.hours[0].dayValues[0]);

    setRequestParams('sales', 6240, 80029997);;

    $scope.$digest();

    $scope.$apply();

    var saleData = { "result": [{ "organization_id": 6240, "site_id": 80064248, "zone_id": 433621, "period_start_date": "2016-06-05T00:00:00.000Z", "dow_name": "Sun", "hourly_sales": "0", "period_sales_percent": 0.0567181978156222, "power_hour_ind": false }, { "organization_id": 6240, "site_id": 80064248, "zone_id": 433621, "period_start_date": "2016-06-05T01:00:00.000Z", "dow_name": "Sun", "hourly_sales": "0", "period_sales_percent": 0.0349035063480752, "power_hour_ind": false }] };

    var url = 'https://api.url/kpis/traffic/powerHours?countType=enters&kpi=sales&operatingHours=true&orgId=6240&percentage=0.15&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=80029997';

    httpBackend.whenGET(url).respond(saleData);

    controller.setOption(getOption('sales'));

    $scope.$digest();

    $scope.$apply();

    httpBackend.flush();

    expect(controller.requestFailed).toBe(false);
    expect(controller.summedAverage).toBe(0);
    expect(controller.hours[0].dayValues[0].summedData).toBe(0);

    var d2 = controller.hours[0].dayValues[0];
    expect(d1.colorClass).toBe(d2.colorClass);
  });

  it('should order hour data for org has 24 hours active time and starting time is  day start', function () {
    var data = {
      "result": [{ "organization_id": 1145, "site_id": 80064248, "zone_id": 433621, "period_start_date": "2016-06-05T00:00:00.000Z", "dow_name": "Sun", "total_traffic": "39", "period_traffic_percent": 0.0567181978156222, "power_hour_ind": false }, { "organization_id": 1145, "site_id": 80064248, "zone_id": 433621, "period_start_date": "2016-06-05T01:00:00.000Z", "dow_name": "Sun", "total_traffic": "24", "period_traffic_percent": 0.0349035063480752, "power_hour_ind": false }]
    };

    var url1 = 'https://api.url/kpis/traffic/powerHours?countType=enters&kpi=traffic&orgId=6240&percentage=0.15&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=80029997';
    var url2 = 'https://api.url/kpis/traffic/powerHours?basePercentage=0.15&countType=enters&kpi=traffic&operatingHours=true&orgId=1154&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=56976';
    var url3 = 'https://api.url/kpis/traffic/powerHours?countType=enters&kpi=sales&orgId=6240&percentage=0.15&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=80029997';

    httpBackend.whenGET(url1).respond(data);
    httpBackend.whenGET(url2).respond(getMockData());
    httpBackend.whenGET(url3).respond(getMockData());
    httpBackend.whenGET('https://api.url/organizations').respond({});

    setRequestParams('traffic', 6240, 80029997);;

    var controller = createController(1154, 56976, false, false, true, true);

    $scope.$apply();

    httpBackend.flush();

    expect(controller.requestFailed).toBe(false);
    expect(controller.summedAverage).toBe(63);
    expect(controller.hours[0].dayValues[0].summedData).toBe(39);

    var d1 = angular.copy(controller.hours[0].dayValues[0]);

    setRequestParams('sales', 6240, 80029997);

    $scope.$digest();

    $scope.$apply();

    controller.setOption(getOption('sales'));

    $scope.$digest();

    $scope.$apply();

    httpBackend.flush();

    expect(controller.requestFailed).toBe(false);
    expect(controller.summedAverage).toBe(9072);
    expect(controller.hours[0].dayValues[0].summedData).toBe(54);
    expect(controller.hours[0].hour).toBe(0);

  });

  it('should order hour data for org has 24 hours active time and starting time is not  day start', function () {
    var data = {
      "result": [{ "organization_id": 1145, "site_id": 80064248, "zone_id": 433621, "period_start_date": "2016-06-05T00:00:00.000Z", "dow_name": "Sun", "total_traffic": "39", "period_traffic_percent": 0.0567181978156222, "power_hour_ind": false }, { "organization_id": 1145, "site_id": 80064248, "zone_id": 433621, "period_start_date": "2016-06-05T01:00:00.000Z", "dow_name": "Sun", "total_traffic": "24", "period_traffic_percent": 0.0349035063480752, "power_hour_ind": false }]
    };

    var url1 = 'https://api.url/kpis/traffic/powerHours?countType=enters&kpi=traffic&orgId=6240&percentage=0.15&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=80029997';
    var url2 = 'https://api.url/kpis/traffic/powerHours?basePercentage=0.15&countType=enters&kpi=traffic&operatingHours=true&orgId=1154&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=56976';
    var url3 = 'https://api.url/kpis/traffic/powerHours?countType=enters&kpi=sales&orgId=6240&percentage=0.15&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=80029997';
    httpBackend.whenGET(url1).respond(data);
    httpBackend.whenGET(url2).respond(data);
    httpBackend.whenGET(url3).respond(getMockData2());
    httpBackend.whenGET('https://api.url/organizations').respond({});

    setRequestParams('traffic', 6240, 80029997);

    var controller = createController(1154, 56976, false, false, true, true, 3);

    $scope.$apply();

    httpBackend.flush();

    expect(controller.requestFailed).toBe(false);
    expect(controller.summedAverage).toBe(63);
    expect(controller.hours[0].dayValues[0].summedData).toBe(39);

    var d1 = angular.copy(controller.hours[0].dayValues[0]);

    setRequestParams('sales', 6240, 80029997);

    $scope.$digest();

    $scope.$apply();

    controller.setOption(getOption('sales'));

    $scope.$digest();

    $scope.$apply();

    httpBackend.flush();

    expect(controller.requestFailed).toBe(false);
    expect(controller.summedAverage).toBe(9072);
    expect(controller.hours[0].dayValues[0].summedData).toBe(54);
    expect(controller.hours[0].hour).toBe(3);
  });

  it('should order hour data for org has activity hours with starting time', function () {
    var data = {
      "result": [{ "organization_id": 1145, "site_id": 80064248, "zone_id": 433621, "period_start_date": "2016-06-05T00:00:00.000Z", "dow_name": "Sun", "total_traffic": "39", "period_traffic_percent": 0.0567181978156222, "power_hour_ind": false }, { "organization_id": 1145, "site_id": 80064248, "zone_id": 433621, "period_start_date": "2016-06-05T01:00:00.000Z", "dow_name": "Sun", "total_traffic": "24", "period_traffic_percent": 0.0349035063480752, "power_hour_ind": false }]
    };

    var url1 = 'https://api.url/kpis/traffic/powerHours?countType=enters&kpi=traffic&orgId=6240&percentage=0.15&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=80029997';
    var url2 = 'https://api.url/kpis/traffic/powerHours?basePercentage=0.15&countType=enters&kpi=traffic&operatingHours=true&orgId=1154&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=56976';
    var url3 = 'https://api.url/kpis/traffic/powerHours?countType=enters&kpi=sales&operatingHours=true&orgId=6240&percentage=0.15&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=80029997';
    var url4 = 'https://api.url/kpis/traffic/powerHours?countType=enters&kpi=sales&orgId=6240&percentage=0.15&reportEndDate=2015-01-31T23:59:59.999Z&reportStartDate=2015-01-01T00:00:00.000Z&siteId=80029997';

    httpBackend.whenGET(url1).respond(data);
    httpBackend.whenGET(url2).respond(data);
    httpBackend.whenGET(url3).respond(getMockData3());
    httpBackend.whenGET(url4).respond(getMockData3());

    setRequestParams('traffic', 6240, 80029997);

    var controller = createController(1154, 56976, false, false, true, true, 8);

    httpBackend.whenGET('https://api.url/organizations').respond({});

    $scope.$apply();

    httpBackend.flush();

    expect(controller.requestFailed).toBe(false);
    expect(controller.summedAverage).toBe(63);
    expect(controller.hours[0].dayValues[0].summedData).toBe(39);

    var d1 = angular.copy(controller.hours[0].dayValues[0]);

    setRequestParams('sales', 6240, 80029997);;

    $scope.$digest();

    $scope.$apply();

    controller.setOption(getOption('sales'));

    $scope.$digest();

    $scope.$apply();

    httpBackend.flush();

    expect(controller.requestFailed).toBe(false);
    expect(controller.summedAverage).toBe(4698);
    expect(controller.hours[0].dayValues[0].summedData).toBe(54);
    expect(controller.hours[0].hour).toBe(8);
  });

  it('should  destroy watches on destroy', function () {
    var controller = createController();

    spyOn(controller, 'groupChange');

    $scope.$destroy();

    expect(controller.groupChange).toHaveBeenCalled();
  });

  function getOption(name) {
    return {
      name: 'average_' + name,
      displayType: name,
      propertyName: 'hourly_' + name,
      metric: {
        value: name,
        label: name,
        precision: 0,
        requiredPermissions: [name],
        prefixSymbol: name === 'sales' ? '$' : '',
        suffixSymbol: ''
      }
    };

  }

  var dow_names = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

  var getMockDayData = function (day, time) {
    return {
      dow_name : day,
      organization_id: 1154,
      period_start_date: time,//"2016-08-14T10:00:00.000Z"
      period_traffic_percent: 1.05112766608838,
      power_hour_ind: true,
      site_id: 80029997,
      total_sales: "103",
      hourly_sales: 54,
      zone_id: 420971
    };
  };

  var getMockData = function () {
    var data = [];
    var dateStart = "2016-08-";
    var day = 12;
    _.each(dow_names, function (dayName) {
      day += 1;
      for (var hour = 0; hour < 24; hour++) {
        var time = dateStart + day + "T";
        if (hour < 10) {
          time += '0';
        }
        time += hour + ":00:00.000Z";
        data.push(getMockDayData(dayName, time))
      }
    });
    return { "result": data };
  };

  var getMockData2 = function () {
    var data = [];
    var dateStart = "2016-08-";
    var day = 12;
    _.each(dow_names, function (dayName) {
      day += 1;
      for (var hour = 3; hour < 24; hour++) {
        var time = dateStart + day + "T";
        if (hour < 10) {
          time += '0';
        }
        time +=   hour + ":00:00.000Z";
        data.push(getMockDayData(dayName, time))
      }

      for (var hour = 0; hour < 3; hour++) {
        var time = dateStart + day + "T";
        if (hour < 10) {
          time += '0';
        }
        time += hour + ":00:00.000Z";
        data.push(getMockDayData(dayName, time))
      }
    });

    return { "result": data };
  };

  var getMockData3 = function () {
    var data = [];
    var dateStart = "2016-08-";
    var day = 12;
    _.each(dow_names, function (dayName) {
      day += 1;
      var startHour = 10;
      switch (dayName) {
        case 'Mon':
          startHour = 9
          break;
        case 'Wed':
          startHour = 8
          break;
        default:
          break;
      }
      for (var hour = startHour; hour < 22; hour++) {
        var time = dateStart + day + "T";
        if (hour < 10) {
          time += '0';
        }
        time +=   hour + ":00:00.000Z";
        data.push(getMockDayData(dayName, time))
      }
    });
    return { "result": data };
  };

  function setRequestParams(kpi, orgId, siteId) {
    $scope.getRequestParams = function () {
      return getParams(kpi, orgId, siteId);
    };

    $scope.$digest();

    $scope.$apply();
  }

  function getParams(kpi, orgId, siteId) {
    var dateRangeStart = moment.utc('2015-01-01');

    var dateRangeEnd = moment.utc('2015-01-31').endOf('day')

    return {
      kpi: kpi,
      countType: 'enters',
      reportStartDate: dateRangeStart.toISOString(),
      reportEndDate: dateRangeEnd.toISOString(),
      orgId: 6240,
      siteId: 80029997,
      // Hard-code basePercentage. It is required by the API, but the
      // exact value does not matter, because this widget does not use
      // the power_hour_ind fields in the response whose values
      // the parameter affects.
      percentage: 0.15
    };
  }

  function createController(orgId, siteId, interior, labor, sales, operatingHours, businessDayStartHour) {
    var controller = $controller('powerHoursWidgetController', {
      '$scope': $scope,
      '$http': http,
      'apiUrl': apiUrl,
      'LocalizationService': LocalizationService,
      '$translate': TranslateMock,
      '$filter': FilterMock
    });

     var organization= {
      _id: "56bd07cf 938c0a2323f7e8dd",
      organization_id: orgId,
      name: "Delta Downs Racetrack Casino and Hotel",
      portal_settings: {currency: '$'},
      subscriptions: {
        interior: interior,
        labor: labor,
        sales: sales
      }
    };

    var site = {
      site_id: siteId,
      business_day_start_hour: businessDayStartHour
    };

    controller.dateRangeStart = moment.utc('2015-01-01');
    controller.dateRangeEnd = moment.utc('2015-01-31').endOf('day');
    controller.currentOrganization = organization;
    controller.operatingHours = true;
    controller.currentSite = site;
    controller.lowThreshold = 0.005;

    return controller;
  }
});

