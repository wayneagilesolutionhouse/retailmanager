(function () {
  'use strict';

  angular
    .module('shopperTrak.widgets')
    .directive('retailOrganizationTableWidget', retailOrganizationTableWidget);

  function retailOrganizationTableWidget() {
    return {
      templateUrl: 'components/widgets/table-widget/retail-organization-table-widget/retail-organization-table-widget.partial.html',
      scope: {
        orgId:                  '=',
        orgSites:               '=',
        dateRangeStart:         '=',
        dateRangeEnd:           '=',
        compareStores:          '=',
        compareRange1Start:     '=',
        compareRange1End:       '=',
        compareRange2Start:     '=',
        compareRange2End:       '=',
        currentOrganization:    '=',
        currentUser:            '=',
        firstDayOfWeekSetting:  '=',
        hideExportIcon:         '=',
        onExportClick:          '&',
        exportIsDisabled:       '=?',
        summaryKey:             '@',
        widgetTitle:            '@',
        widgetIcon:             '@',
        orderBy:                '=?',
        activeSortType:         '=',
        widgetData:             '=',
        isLoading:              '=',
        selectedTags:           '=',
        siteCategories:         '=',
        selectedCategory:       '=',
        filterText:             '=',
        orgHasInterior:         '=',
        orgHasSales:            '=',
        orgHasLabor:            '=',
        language:               '=',
        salesCategories:        '=?',
        setSelectedWidget:      '&', // Custom dashboard,
        selectedMetrics:        '=?',
        comparisonIndex:        '=?',
        compStores:             '=?',
        widgetIsLoading:        '=?isLoading',
        currencySymbol:         '=?'
      },
      controller: retailOrganizationTableWidgetController,
      controllerAs: 'vm',
      bindToController: true
    };
  }

  retailOrganizationTableWidgetController.$inject = [
    '$scope',
    '$rootScope',
    '$filter',
    'metricConstants',
    'retailOrganizationSummaryData',
    '$translate',
    '$timeout',
    '$state',
    '$q',
    'utils',
    'LocalizationService',
    'ObjectUtils',
    'comparisonsHelper',
    'SubscriptionsService'
  ];

  function retailOrganizationTableWidgetController(
    $scope,
    $rootScope,
    $filter,
    metricConstants,
    retailOrganizationSummaryData,
    $translate,
    $timeout,
    $state,
    $q,
    utils,
    LocalizationService,
    ObjectUtils,
    comparisonsHelper,
    SubscriptionsService
  ) {
    var vm = this;
    vm.items = [];
    vm.itemIds = [];
    vm.itemTypes = [];
    vm.requestFailed = false;
    vm.showAllItems = false;
    vm.filterText = vm.filterText || ''; // default
    vm.orderByOrder = '-';
    vm.orderByColumn = 'selectedPeriod';
    vm.orderByKpi = 'traffic';
    vm.trueVal = true;
    vm.isLoading = true;
    if (_.isUndefined(vm.comparisonIndex)) vm.comparisonIndex = 1; // default

    var tempTableData = {};

    var watchUnbinds = [];

    var allPermittedMetricsColumns = {};

    var labelToMetricDict = {};
    var lastSortedMetric;

    vm.allData = [];
    vm.tableData = [];

    vm.options = {
      emptyMessage: '',
      loadingIndicator: true,
      rowHeight: 26,
      sortType: 'single',
      reorderable: false,
      columnMode: 'force',
      headerHeight: 50,
      scrollbarH: false,
      selectable: true,
      columns: [],
      paging: {
        externalPaging: false
      },
      internal: {
        bodyHeight: 500 // needed to fill widget body
      }
    };

    vm.hasFilterText = false;
    var filterText$timeout;

    var unbindSiteCategoriesWatch = $scope.$watch('vm.siteCategories', function () {
      renderWidget();
    });

    var unbindFilterTextWatch = $scope.$watch('vm.filterText', function (filterText) {
      if (filterText$timeout) {
        $timeout.cancel(filterText$timeout);
      }

      filterText$timeout = $timeout(function () {
        if (filterText.length > 2) {
          vm.hasFilterText = true;
          filterSites();
          clearSortIndicatorLabel();
        } else {
          if (vm.hasFilterText !== false) {
            vm.hasFilterText = false;
            filterSites();
          }
        }
      }, 200);
    });

    watchUnbinds.push(unbindSiteCategoriesWatch);
    watchUnbinds.push(unbindFilterTextWatch);

    vm.filterKeyDown = function (ev) {
      // Prevent Backspace in search input from refreshing the whole page
      if (ev.keyCode === 8) ev.stopPropagation();
    };

    vm.getSiteNameById = getSiteNameById;
    vm.getMetricPrecision = getMetricPrecision;
    vm.filterCategory = filterCategory;
    vm.changeCompare = changeCompare;
    vm.exportToCSV = exportToCSV;
    vm.setLastSortedKpi = setLastSortedKpi;

    vm.compareRangeIsPriorPeriod = compareRangeIsPriorPeriod;
    vm.compareRangeIsPriorYear = compareRangeIsPriorYear;


    vm.compare1Period = {
      start: vm.compareRange1Start,
      end: vm.compareRange1End
    };

    vm.compare2Period = {
      start: vm.compareRange2Start,
      end: vm.compareRange2End
    };

    var siteColumn = {
      name: 'site',
      prop: 'site_name',
      width: 200,
      sortable: false,
      canAutoResize: false
    };

    vm.options.columns.unshift(siteColumn);

    var unbindWidgetDataWatch = $scope.$watchCollection('vm.widgetData', function(oldValue, newValue) {
      if(ObjectUtils.isNullOrUndefinedOrEmptyObject(oldValue) && ObjectUtils.isNullOrUndefinedOrEmptyObject(newValue)) {
        return;
      } else {
        vm.tableData = [];
        renderWidget();
      }

    });

    var unbindSelectedCategoryWatch = $scope.$watch('vm.selectedCategory', filterSites);
    var unbindSelectedMetricsWatch = $scope.$watch('vm.selectedMetrics', loadMetricsColumns);

    activate();

    var unbindComparisonIndexWatch = $scope.$watch('vm.comparisonIndex', function() {

      renderWidget();
      clearSortIndicatorLabel();
    });

    watchUnbinds.push(unbindWidgetDataWatch);
    watchUnbinds.push(unbindSelectedCategoryWatch);
    watchUnbinds.push(unbindSelectedMetricsWatch);
    watchUnbinds.push(unbindComparisonIndexWatch);

    function reloadWidgetData() {
      if(!ObjectUtils.isNullOrUndefined(vm.salesCategories)) {
        var salesCategory = _.pluck(vm.salesCategories, 'id');
      }
      var firstSalesCat = !_.isUndefined(salesCategory) && !_.isUndefined(salesCategory[0]) ? salesCategory[0]: undefined;
      var initialSalesCategory = !_.isUndefined(vm.initialSalesCategory) && !_.isUndefined(vm.initialSalesCategory[0]) ? vm.initialSalesCategory[0]: undefined;
      if(initialSalesCategory !== firstSalesCat && !vm.isLoading) {
        vm.widgetData = null;
        vm.isLoading = true;

        loadData().then(function () {
          getCompareLabels();

          if (ObjectUtils.isNullOrUndefinedOrEmpty(vm.selectedMetrics)) {
            loadDefaultMetrics(); // creates columns for dtable usage
          }

          loadTranslations();
          loadMetricsColumns(); // pushes 'selectedMetrics' columns to be displayed

          if (ObjectUtils.isNullOrUndefined(vm.selectedCategory)) {
            vm.selectedCategory = '';
          }
          vm.numberFormatName = getNumberFormat();
        });
      }
    }

    function setLastSortedKpi(translationKeyLabel) {
      if(translationKeyLabel && labelToMetricDict) {
        lastSortedMetric = labelToMetricDict[translationKeyLabel.$header];
      }
    }

    function clearSortIndicatorLabel() {
      angular.element(document).find('.dt-header-cell .sort-btn').removeClass('sort-asc sort-desc');
    }

    function activate() {
      configureWatches();
      getSubscribedMetrics();

      if (ObjectUtils.isNullOrUndefinedOrEmpty(vm.selectedMetrics)) {
        loadDefaultMetrics(); // creates columns for dtable usage
      }

      loadData().then(function () {
        getCompareLabels();

        loadTranslations();
        loadMetricsColumns(); // pushes 'selectedMetrics' columns to be displayed

        if (ObjectUtils.isNullOrUndefined(vm.selectedCategory)) {
          vm.selectedCategory = '';
        }
        vm.numberFormatName = getNumberFormat();
      });

    }

    function configureWatches() {
      $scope.$on('$destroy', function() {
        _.each(watchUnbinds, function(unbind) {
          if(typeof unbind === 'function') {
            unbind();
          }
        });
      });
    }

    function loadData() {
      var defer = $q.defer();
      var salesCategories;

      if(!ObjectUtils.isNullOrUndefined(vm.salesCategories)) {
        salesCategories = _.pluck(vm.salesCategories, 'id');
      }

      // Used for Custom Dashboards as data is not passed in
      if ($rootScope.customDashboards || ObjectUtils.isNullOrUndefined(vm.widgetData)) {
        vm.isLoading = true;
        var promises = [];
        promises.push(getReportData(vm.orgId, vm.dateRangeStart, vm.dateRangeEnd, vm.selectedTags, salesCategories));
        promises.push(getReportData(vm.orgId, vm.compareRange1Start, vm.compareRange1End, vm.selectedTags, salesCategories));
        promises.push(getReportData(vm.orgId, vm.compareRange2Start, vm.compareRange2End, vm.selectedTags, salesCategories));

        $q.all(promises).then(function() {
          vm.widgetData = angular.copy(tempTableData);

          vm.initialSalesCategory = salesCategories;
          var unbindSalesCategoriesWatch = $scope.$watch('vm.salesCategories', reloadWidgetData);
          watchUnbinds.push(unbindSalesCategoriesWatch);

          defer.resolve();
          vm.isLoading = false;;
        }).catch(function (e) {
          defer.reject(e);
        });
      } else  {
        defer.resolve();
      }
      return defer.promise;
    }

    function getPermittedKpis() {
      var kpisPermitted = [];

      var permdMetrics = metricConstants.metrics.filter(function(metric) {
        return SubscriptionsService.hasSubscriptions(metric.requiredPermissions, vm.currentOrganization);
      });

      kpisPermitted = _.pluck(permdMetrics, 'value');

      kpisPermitted.push('traffic');

      return kpisPermitted;
    }

    function getReportData(orgId, dateRangeStart, dateRangeEnd, selectedTags, salesCategoryId) {
      var dateRangeKey = retailOrganizationSummaryData.getDateRangeKey(dateRangeStart, dateRangeEnd);
      var compStores = !_.isUndefined(vm.compStores) && !_.isUndefined(vm.compStores.stores) ? vm.compStores.stores : false;

      tempTableData[dateRangeKey] = [];

      var params = {
        orgId: orgId,
        comp_site: compStores,
        dateRangeStart: dateRangeStart,
        dateRangeEnd: dateRangeEnd,
        selectedTags: selectedTags,
        kpi: getPermittedKpis()
      };

      if($rootScope.customDashboards) {
        params.sales_category_id = undefined;
      } else {
        params.sales_category_id = salesCategoryId[0];
      }

      var deferred = $q.defer();

      retailOrganizationSummaryData.fetchReportData(params, false, function (data) {
        tempTableData[dateRangeKey] = data;
        deferred.resolve();
      });

      return deferred.promise;
    }

    function hasSalesPermission(selectedMetrics) {
      var salesPermissionExists = false;
      _.find(selectedMetrics, function(metric) {
        salesPermissionExists = _.contains(metric.requiredPermissions, 'sales') || metric.subscription === 'sales';
        return salesPermissionExists === true;
      });

      return salesPermissionExists;
    }

    function createColumnForMetric(metric) {
      var metricColumns = {};

      // Default widget to sort by traffic desc.
      var sort = metric.value === 'traffic' ? 'desc' : undefined;
      $translate(metric.shortTranslationLabel).then(function (label) {
        var kpiCol = {
          prop: metric.value + '.selectedPeriod',
          kpi: metric.value,
          precision: metric.precision,
          suffixSymbol: metric.suffixSymbol,
          maxWidth: 250,
          sort: sort,
          width: 130,
          minWidth: 120,
          name: label
        };
        labelToMetricDict[metric.shortTranslationLabel] = metric.value;

        metricColumns.kpi = kpiCol;

        var kpiChangeCol = {
          prop: metric.value + '.currentComparison.percentageChangeReal',
          kpi: metric.value,
          name: '%',
          maxWidth: 120,
          width: 75,
          minWidth: 70
        };
        metricColumns.change = kpiChangeCol;
        allPermittedMetricsColumns[metric.value] = metricColumns;
      });
    }


    function getSubscribedMetrics() {
      var metricsShownOnWidget = ['traffic', 'dwelltime', 'star', 'sales', 'conversion', 'ats', 'sps', 'splh', 'aur', 'transactions'];

      var sortedMetrics = _.sortBy(metricConstants.metrics, 'order');

      vm.metrics = _.filter(sortedMetrics, function(metric) {
        var hasSubscription = SubscriptionsService.hasSubscriptions(metric.requiredPermissions, vm.currentOrganization);
        return _.contains(metricsShownOnWidget, metric.value) && hasSubscription;
      });

      vm.metrics[0].selected = true;

      lastSortedMetric = 'traffic'; // widget orders by traffic on default

      _.each(vm.metrics, function (metric) {
        createColumnForMetric(metric);
      });
    }

    function loadDefaultMetrics() {
      vm.selectedMetrics = vm.metrics.slice(0, 5);
    }

    function getNumberFormat() {
      var currentUser = vm.currentUser;
      var currentOrganization = vm.currentOrganization;
      return LocalizationService.getCurrentNumberFormatName(currentUser, currentOrganization);
    }

    function renderWidget() {
      if(ObjectUtils.isNullOrUndefinedOrEmptyObject(vm.widgetData)) {
        return;
      }

      var dateRangeKeys = getDateRangeKeys();

      vm.allData = getTableWidgetData(dateRangeKeys, vm.widgetData);

      if(ObjectUtils.isNullOrUndefined(vm.allData)) {
        return;
      }

      var data = [];
      _.each(vm.allData, function (site) {
        if (!ObjectUtils.isNullOrUndefined(site)) {
          data.push(site);
        }
      });

      vm.tableData = angular.copy(data);
      filterSites();
      vm.options.paging.count = vm.tableData.length;
      vm.options.paging.size = 100;
      if(!$rootScope.customDashboards){
        vm.isLoading = false;
      }
    }

    // Applies filter based on selectedCategory and filter text
    function filterSites() {
      if (vm.selectedCategory === '') {
        vm.tableData = $filter('collectionFilter')(vm.allData, 'site_name', vm.filterText);
      } else {
        var filteredData = _.filter(vm.allData, function (site) {
          return site.site_category === vm.selectedCategory;
        });
        filteredData = $filter('collectionFilter')(filteredData, 'site_name', vm.filterText);
        vm.tableData = filteredData;
      }
      clearSortIndicatorLabel();
    }

    function getDateRangeKeys() {
      var dateRangeKey = retailOrganizationSummaryData.getDateRangeKey(vm.dateRangeStart, vm.dateRangeEnd);
      var com1RangeKey = retailOrganizationSummaryData.getDateRangeKey(vm.compareRange1Start, vm.compareRange1End);
      var com2RangeKey = retailOrganizationSummaryData.getDateRangeKey(vm.compareRange2Start, vm.compareRange2End);
      return [dateRangeKey, com1RangeKey, com2RangeKey];
    }

    function getTableWidgetData(dateRangeKeys, widgetData) {
      var baseData = widgetData[dateRangeKeys[0]];

      if(ObjectUtils.isNullOrUndefined(baseData)) {
        return;
      }

      var allData = _.extend({}, widgetData);
      var compare = [null, null];
      var comparisonIndices = [1, 2];
      vm.comparisonIndices = comparisonIndices;
      // Load compare data
      _.each(Object.keys(allData), function (key) {
        if (key === dateRangeKeys[0]) {
          return;
        }

        var list = _.sortBy(allData[key], 'site_id');
        var result = {};

        _.each(list,function (obj) {
          result[obj.site_id] = {
            site_id: obj.site_id,
            traffic: obj.traffic,
            dwelltime: obj.dwelltime,
            star: obj.star,
            sales: obj.sales,
            conversion: obj.conversion,
            ats: obj.ats,
            upt: obj.upt,
            transactions: obj.transactions
          };
        });

        if (key === dateRangeKeys[1]) {
          compare[0] = result;
        }
        if (key === dateRangeKeys[2]) {
          compare[1] = result;
        }

      });

      baseData = baseData.map(function (obj) {
        var site_id = obj.site_id;
        var siteName = getSiteNameById(obj.site_id);
        var result = {
          site_id: site_id,
          site_name: siteName,
          traffic: {},
          dwelltime: {},
          star: {},
          sales: {},
          conversion: {},
          ats: {},
          upt: {}
        };

        if (!ObjectUtils.isNullOrUndefined(vm.siteCategories)) {
          result.site_category = vm.siteCategories[obj.site_id];
        } else {
          result.site_category = null;
        }

        result.siteClass = getSiteClass(result.site_category);

        _.each(vm.metrics, function (metric) {

          result[metric.value] = {
            prefixSymbol: metric.prefixSymbol,
            suffixSymbol: metric.suffixSymbol,
            selectedPeriod: obj[metric.value], // This is needed for sorting
            displayValue: getDisplayValue(obj[metric.value], metric),
            comparisons: [],
            currentComparison: {}
          };

          for (var i = 0; i < compare.length ; i++) {
            var period = compare[i];
            var metricCompare;
            if (period[site_id]) {
              metricCompare = period[site_id][metric.value];
            } else {
              metricCompare = null;
            }
            var metricComparison = comparisonsHelper.getComparisonData(obj[metric.value], metricCompare, true);
            result[metric.value].comparisons.push(metricComparison);
            if (vm.comparisonIndex - 1 === i) {

              if(!ObjectUtils.isNullOrUndefined(metricComparison)) {
                var percentChangeMetricInfo = {
                  prefixSymbol: metricComparison.prefixSymbol,
                  precision: 1
                };

                metricComparison.displayValue = getDisplayValue(metricComparison.percentageChange, percentChangeMetricInfo);
              }

              result[metric.value].currentComparison = metricComparison;
            }
          }

        });
        return result;
      });
      return baseData;
    }

    /** Gets the CSS class for a site based on it's category.
     *  This function saves a heavy ng-class expression.
     *
     *  @param {number} siteCategory the value that represents the site's category
     *  @returns {string} The CSS class to be used in the UI
     **/
    function getSiteClass(siteCategory) {
      var siteClass = 'site-name-label--';

      switch(siteCategory) {
        case 0:
          siteClass += 'yellow';
          break;
        case 1:
          siteClass += 'orange';
          break;
        case 2:
          siteClass += 'purple';
          break;
        case 3:
          siteClass += 'cyan';
          break;
        default:
          siteClass = '';
          break;
      }

      return siteClass;
    }

    /** Gets the display value for the UI.
     *  Does the heavy lifting of applying prefix and suffix values, as well as precision formatting
     *
     *  @param {number} value the value that needs to be formatted
     *  @param {object} metric the metric object. Should contain the prefix symbol, suffix symbol, etc
     *  @returns {string} The formatted value that can be shown on the UI
     **/
    function getDisplayValue(value, metric) {
      if(ObjectUtils.isNullOrUndefined(value)) {
        return '-';
      }

      var displayValue = '';

      if(!ObjectUtils.isNullOrUndefined(metric.prefixSymbol)) {
        displayValue += metric.prefixSymbol;
      }

      displayValue += $filter('formatNumber')(value, metric.precision, vm.numberFormatName);

      if(!ObjectUtils.isNullOrUndefined(metric.suffixSymbol)) {
        displayValue += metric.suffixSymbol;
      }

      return displayValue;
    }

    function getSiteNameById(siteId) {
      var site = vm.orgSites.filter(function (obj) {
        return Number(obj.site_id) === Number(siteId);
      });

      if (!ObjectUtils.isNullOrUndefined(site[0])) {
        return site[0].name;
      }
    }

    function getMetricPrecision(metric) {
      var result = _.where(vm.metrics, { value: metric });
      return result[0].precision;
    }


    function filterCategory(cat) {
      vm.selectedCategory = cat;
    }

    function changeCompare(comparisonIndex) {
      vm.comparisonIndex = comparisonIndex;
    }

    function compareRangeIsPriorPeriod(comparePeriod) {
      if (!ObjectUtils.isNullOrUndefined(vm.dateRangeStart) && !ObjectUtils.isNullOrUndefined(vm.dateRangeEnd)) {
        var range = {
          start: vm.dateRangeStart,
          end: vm.dateRangeEnd
        };

        if (utils.dateRangeIsPriorPeriod(range, comparePeriod)) {
          return true;
        }
      }
      return false;
    }

    function compareRangeIsPriorYear(comparePeriod) {
      if (vm.dateRangeStart !== undefined && vm.dateRangeEnd !== undefined) {
        var range = {
          start: vm.dateRangeStart,
          end: vm.dateRangeEnd
        };

        if (utils.dateRangeIsPriorYear(range, comparePeriod, vm.firstDayOfWeekSetting)) {
          return true;
        }
      }
      return false;
    }


    function loadMetricsColumns() {
      if (!_.contains(selectedKpiList(), lastSortedMetric)) {
        clearSortIndicatorLabel();
      }

      vm.hideSalesCategories = !hasSalesPermission(vm.selectedMetrics);

      if(typeof vm.salesCategories !== 'undefined' && vm.hideSalesCategories){
        //re-init handle for when salesCategories dropdown is back in view...
        vm.salesCategories[0] = vm.currentOrganization.portal_settings.sales_categories[0];
      }

      $timeout(function () {
        vm.options.columns = [siteColumn];
        _.each(vm.selectedMetrics, function (kpi) {
          var columnDef = allPermittedMetricsColumns[kpi.value];

          if (columnDef) {
            var kpiString = columnDef.kpi.kpi;
            if (lastSortedMetric !== kpiString) {
              columnDef.kpi.sort = undefined;
              columnDef.change.sort = undefined;
            }
            vm.options.columns.push(columnDef.kpi);
            vm.options.columns.push(columnDef.change);
          }
        });

      }, 100);
    }

    function getCompareLabels() {
      if (compareRangeIsPriorPeriod(vm.compare1Period)) {
        vm.compareLabel1 = 'common.PRIORPERIOD';
      } else if (compareRangeIsPriorYear(vm.compare1Period)) {
        vm.compareLabel1 = 'common.PRIORYEAR';
      } else {
        vm.compareLabel1 = 'common.CUSTOMCOMPARE1';
      }

      if (compareRangeIsPriorPeriod(vm.compare2Period)) {
        vm.compareLabel2 = 'common.PRIORPERIOD';
      } else if (compareRangeIsPriorYear(vm.compare2Period)) {
        vm.compareLabel2 = 'common.PRIORYEAR';
      } else {
        vm.compareLabel2 = 'common.CUSTOMCOMPARE2';
      }
    }

    function loadTranslations() {
      $translate.use(vm.language);
    }


    function exportToCSV() {
      $state.go('csv', {
        orgId: vm.orgId,
        startDate: moment(vm.dateRangeStart).format('YYYY-MM-DD'),
        endDate: moment(vm.dateRangeEnd).format('YYYY-MM-DD'),
        kpi: selectedKpiList(),
        tag: vm.selectedTags,
        salesCategories: selectedSalesCategoriesList()
      });
    }

    function selectedKpiList() {
      return _.pluck(vm.selectedMetrics, 'value');
    }

    function selectedSalesCategoriesList() {
      return _.pluck(vm.salesCategories, 'id');
    }
  }
})();
