(function () {
  'use strict';

  angular
    .module('shopperTrak.widgets')
    .directive('sitePerformanceWidget', sitePerformanceWidget);

  function sitePerformanceWidget() {
    return {
      templateUrl: 'components/widgets/site-performance-widget/site-performance-widget.partial.html',
      scope: {
        orgId: '=',
        sites: '=?',
        widgetData: '=?',
        selectedPeriodStart: '=',
        selectedPeriodEnd: '=',
        compareRange1Start: '=',
        compareRange1End: '=',
        compareRange1Type: '=',
        compareRange2Start: '=',
        compareRange2End: '=',
        compareRange2Type: '=',
        dateFormatMask: '=',
        numberFormatName: '=',
        firstDayOfWeekSetting: '=',
        onExportClick: '&',
        exportIsDisabled: '=?',
        hideExportIcon: '=?',
        language: '=',
        isLoading: '=?',
        currentView: '=?',
        setSelectedWidget: '&'
      },
      controller: sitePerformanceWidgetController,
      controllerAs: 'sitePerformanceWidget',
      bindToController: true
    };
  }

  sitePerformanceWidgetController.$inject = [
    '$scope',
    '$filter',
    '$translate',
    '$timeout',
    '$rootScope',
    'sitePerformanceWidgetConstants',
    'organizationSummaryData',
    'comparisonsHelper',
    'utils',
    'SiteResource',
    'ObjectUtils'
  ];

  function sitePerformanceWidgetController(
    $scope,
    $filter,
    $translate,
    $timeout,
    $rootScope,
    constants,
    organizationSummaryData,
    comparisonsHelper,
    utils,
    SiteResource,
    ObjectUtils
  ) {

    var sitePerformanceWidget = this;

    // how many sites to show per page
    sitePerformanceWidget.numberOfSites = 10;

    sitePerformanceWidget.kpis = ['traffic', 'gsh', 'loyalty'];
    sitePerformanceWidget.defaultKpi = 'traffic';

    sitePerformanceWidget.orgSiteData = {};
    sitePerformanceWidget.reloadChart = reloadChart;
    sitePerformanceWidget.hasMoreSites = hasMoreSites;
    sitePerformanceWidget.getPreviousSitesLabel = getPreviousSitesLabel;
    sitePerformanceWidget.getNextSitesLabel = getNextSitesLabel;
    sitePerformanceWidget.loadOrgData = loadOrgData;
    sitePerformanceWidget.compareRangeIsPriorPeriod = compareRangeIsPriorPeriod;
    sitePerformanceWidget.compareRangeIsPriorYear = compareRangeIsPriorYear;

    activate();

    $scope.$watchCollection('sitePerformanceWidget.widgetData', function () {
      loadOrgData(sitePerformanceWidget.sites, sitePerformanceWidget.widgetData);
    });

    $scope.$watchCollection('sitePerformanceWidget.sites', function () {
      loadOrgData(sitePerformanceWidget.sites, sitePerformanceWidget.widgetData);
    });

    function activate() {

      sitePerformanceWidget.requestFailed = false;
      sitePerformanceWidget.isLoading = true;
      sitePerformanceWidget.views = constants.views;
      sitePerformanceWidget.chartOptions = {
        seriesBarDistance: 13,
        axisX: {
          showGrid: false
        },
      };

      if($rootScope.pdf) {
        sitePerformanceWidget.chartOptions.axisX.offset = 60;
      }

      loadTranslations();

      sitePerformanceWidget.compareRanges = [];
      sitePerformanceWidget.compareRanges.push({
        start: sitePerformanceWidget.compareRange1Start,
        end: sitePerformanceWidget.compareRange1End,
        type: sitePerformanceWidget.compareRange1Type
      });

      sitePerformanceWidget.compareRanges.push({
        start: sitePerformanceWidget.compareRange2Start,
        end: sitePerformanceWidget.compareRange2End,
        type: sitePerformanceWidget.compareRange2Type
      });

      // If widgetData or sites are not passed for directive from the view,
      // we need to make own data requests.
      if (sitePerformanceWidget.widgetData === undefined) {
        fetchKpiData(
          sitePerformanceWidget.orgId,
          sitePerformanceWidget.selectedPeriodStart,
          sitePerformanceWidget.selectedPeriodEnd
        );

        _.each(sitePerformanceWidget.compareRanges, function(range) {
          fetchKpiData(sitePerformanceWidget.orgId, range.start, range.end);
        });

        sitePerformanceWidget.externalDataRequest = true;
      }
      else {
        setNoData();
      }

      if (sitePerformanceWidget.sites === undefined) {
        getOrganizationSites();
      }

    }

    function fetchKpiData(orgId, dateRangeStart, dateRangeEnd) {
      if (ObjectUtils.isNullOrUndefined(sitePerformanceWidget.widgetData)) {
        sitePerformanceWidget.widgetData = {};
      }

      var dateRangeKey = organizationSummaryData.getDateRangeKey(dateRangeStart, dateRangeEnd);
      organizationSummaryData.setParams({ 'orgId': orgId, 'dateRangeStart': dateRangeStart, 'dateRangeEnd': dateRangeEnd });

      if (ObjectUtils.isNullOrUndefined(sitePerformanceWidget.widgetData[dateRangeKey])) {
        organizationSummaryData.fetchKPIData(function (data) {
          sitePerformanceWidget.widgetData[dateRangeKey] = data;
          setNoData();
        });
      }

    }

    function setNoData() {
      if (sitePerformanceWidget.isLoading || sitePerformanceWidget.isRefreshingChart) {
        sitePerformanceWidget.noData = false;

        return;
      }

      sitePerformanceWidget.noData = true;

      _.each(sitePerformanceWidget.sites, function (site) {
        _.each(sitePerformanceWidget.kpis, function(kpi) {
          if (reportDataIsAvailable(site.site_id, kpi)) {
            sitePerformanceWidget.noData = false;
          }
        });
      });
    }

    function getOrganizationSites() {
      sitePerformanceWidget.sites = SiteResource.query({ orgId: sitePerformanceWidget.orgId });
    }

    function initializeChart() {
      sitePerformanceWidget.chartData = {
        labels: [],
        series: [
          [],
          [],
          []
        ]
      };
      sitePerformanceWidget.tooltipData = {};
    }

    function reloadChart(view, page, comparisonIndex) {
      var typeChanged;
      sitePerformanceWidget.isRefreshingChart = true;
      sitePerformanceWidget.currentView = view.view;
      sitePerformanceWidget.currentKpi = view.kpi;
      typeChanged = (sitePerformanceWidget.currentType !== view.type || sitePerformanceWidget.comparisonIndex !== comparisonIndex - 1);
      sitePerformanceWidget.currentType = view.type;
      sitePerformanceWidget.comparisonIndex = comparisonIndex - 1;
      sitePerformanceWidget.currentOrder = view.order;
      sitePerformanceWidget.currentPage = page;

      var desc;

      initializeChart();

      if (page === undefined) {
        page = 1;
      }

      if (view.order === 'asc' && !typeChanged) {
        desc = false;
      } else {
        desc = true;
      }

      if (!ObjectUtils.isNullOrUndefinedOrEmpty(sitePerformanceWidget.orgSiteData)) {
        if (view.type === 'selectedPeriod') {
          sitePerformanceWidget.orgSiteData = $filter('orderOrgdataObjectBy')(sitePerformanceWidget.orgSiteData, view.kpi, view.type, desc);
        } else {
          var orderedKpi = desc ? '-' + view.kpi : view.kpi;
          sitePerformanceWidget.orgSiteData = $filter('sortObjectWithKpiBy')(sitePerformanceWidget.orgSiteData, orderedKpi, 'comparisons', comparisonIndex - 1, 'compareTotal', true);
        }

        var start = (page - 1) * sitePerformanceWidget.numberOfSites;
        var i = 0; var j = 0;

        _.each(sitePerformanceWidget.orgSiteData, function (site) {

          if (i >= start && i < (sitePerformanceWidget.numberOfSites * page)) {
            if (sitePerformanceWidget.externalDataRequest === true) {
              sitePerformanceWidget.chartData.labels.push((i + 1) + '. ' + site.name);

            } else {
              sitePerformanceWidget.chartData.labels.push('<div class="badge-container"><div class="badge">' + (i + 1) + '</div></div><div class="label-name">' + site.name + '</div>');
            }

            if (site.data[view.kpi][view.type] === null) {
              sitePerformanceWidget.chartData.series[0].push(0);
              _.each(sitePerformanceWidget.compareRanges, function (range, index) {
                sitePerformanceWidget.chartData.series[index + 1].push(0);
              });
            } else {
              sitePerformanceWidget.chartData.series[0].push(site.data[sitePerformanceWidget.defaultKpi].selectedPeriod);
              _.each(sitePerformanceWidget.compareRanges, function (range, index) {
                sitePerformanceWidget.chartData.series[index + 1].push(site.data[sitePerformanceWidget.defaultKpi].comparisons[index].compareTotal);
              });
            }
            sitePerformanceWidget.tooltipData[j] = {
              siteName: site.name,
              data: site.data[view.kpi]
            };
            j++;
          }
          i++;
        });

        sitePerformanceWidget.isRefreshingChart = false;
      }

    }

    function loadOrgData(sites, orgData) {

      if (typeof orgData !== 'object' || typeof sites !== 'object' ||
        Object.keys(orgData).length < 2 || Object.keys(sites) === 0) {
        return;
      }

      var selectedPeriodKey;
      var comparisonsPeriodKeys;
      var data;
      var kpiData;

      selectedPeriodKey = sitePerformanceWidget.selectedPeriodStart + '_' + sitePerformanceWidget.selectedPeriodEnd;
      comparisonsPeriodKeys = [];

      _.each(sitePerformanceWidget.compareRanges, function (range) {
        comparisonsPeriodKeys.push(range.start + '_' + range.end);
      });

      sitePerformanceWidget.data = orgData[selectedPeriodKey];
      sitePerformanceWidget.comparisonsData = [];

      _.each(comparisonsPeriodKeys, function (periodKey) {
        sitePerformanceWidget.comparisonsData.push(orgData[periodKey]);
      });

      sitePerformanceWidget.orgSiteData = [];

      _.each(sites, function (site) {
        kpiData = {};

        _.each(sitePerformanceWidget.kpis, function (kpi) {
          var currentTotal = null;
          var compareTotal = null;
          if (reportDataIsAvailable(site.site_id, kpi)) {
            currentTotal = sitePerformanceWidget.data[site.site_id][kpi];
          }

          kpiData[kpi] = { selectedPeriod: currentTotal };
          kpiData[kpi].comparisons = [];

          _.each(comparisonsPeriodKeys, function (periodKey, index) {

            if( !ObjectUtils.isNullOrUndefined (sitePerformanceWidget.comparisonsData[index]) && !ObjectUtils.isNullOrUndefined (sitePerformanceWidget.comparisonsData[index][site.site_id]) ) {
              compareTotal = sitePerformanceWidget.comparisonsData[index][site.site_id][kpi];
            } else {
              compareTotal = '-';
            }

            kpiData[kpi].comparisons.push({
              compareTotal: compareTotal,
              comparison: comparisonsHelper.getComparisonData(currentTotal, compareTotal, false)
            });
          });

        });

        data = { id: site.site_id, name: site.name, data: kpiData };
        sitePerformanceWidget.orgSiteData.push(data);
      });
      

      var currentView = sitePerformanceWidget.views[Object.keys(sitePerformanceWidget.views)[0]];

      var viewIndex = 1;

      // Initial sort
      sitePerformanceWidget.orgSiteData = $filter('orderOrgdataObjectBy')(sitePerformanceWidget.orgSiteData, currentView.kpi, currentView.type, true);

      if(!ObjectUtils.isNullOrUndefined(sitePerformanceWidget.currentView)) {
        var matchedView;

        // We use an each loop because we need the index
        _.each(sitePerformanceWidget.views, function(view, index) {
          if(view.view === sitePerformanceWidget.currentView) {
            matchedView = view;
            viewIndex = index;
          }
        });

        if(!ObjectUtils.isNullOrUndefined(matchedView)) {
          currentView = matchedView;
        }
      }

      reloadChart(currentView, 1, viewIndex);      

      setNoData();
      
      $timeout(function() {
        sitePerformanceWidget.isLoading = false;
        sitePerformanceWidget.allRequestsSucceeded = true;
      });
    }

    function reportDataIsAvailable(siteId, kpi) {
      return !ObjectUtils.isNullOrUndefined (sitePerformanceWidget.data) &&
        !ObjectUtils.isNullOrUndefined (sitePerformanceWidget.data[siteId]) &&
        !ObjectUtils.isNullOrUndefined (sitePerformanceWidget.data[siteId][kpi]);
    }

    function hasMoreSites() {
      var orgSites = sitePerformanceWidget.sites.length;
      if ((sitePerformanceWidget.currentPage * sitePerformanceWidget.numberOfSites) < orgSites) {
        return true;
      } else {
        return false;
      }
    }

    function getPreviousSitesLabel() {
      if (sitePerformanceWidget.currentPage > 2) {
        var startPrevious = getPreviousPage() * sitePerformanceWidget.numberOfSites - 1;
        return startPrevious + '-' + (startPrevious + sitePerformanceWidget.numberOfSites - 1);
      } else {
        return '1-' + sitePerformanceWidget.numberOfSites;
      }
    }

    function getNextSitesLabel() {
      var orgSites = sitePerformanceWidget.sites.length;
      var startNext = getNextPage() * sitePerformanceWidget.numberOfSites - 1;
      if (getNextPage() * sitePerformanceWidget.numberOfSites < orgSites) {
        return startNext + '-' + (startNext + sitePerformanceWidget.numberOfSites - 1);
      } else if (sitePerformanceWidget.currentPage * sitePerformanceWidget.numberOfSites + 1 === orgSites) {
        return orgSites;
      } else {
        return sitePerformanceWidget.currentPage * sitePerformanceWidget.numberOfSites + '-' + orgSites;
      }
    }

    function getNextPage() {
      return sitePerformanceWidget.currentPage + 1;
    }

    function getPreviousPage() {
      return sitePerformanceWidget.currentPage - 1;
    }

    function loadTranslations() {
      $translate.use(sitePerformanceWidget.language);

      angular.forEach(sitePerformanceWidget.views, function (view, key) {
        $translate('kpis.kpiTitle.' + view.view).then(function (title) {
          sitePerformanceWidget.views[key].title = title;
        });
      });

    }

    function compareRangeIsPriorPeriod(comparePeriodType) {
      if (comparePeriodType === 'prior_period') {
        return true;
      } else {
        return false;
      }
    }

    function compareRangeIsPriorYear(comparePeriodType) {
      if (comparePeriodType === 'prior_year') {
        return true;
      } else {
        return false;
      }
    }
  }
})();
