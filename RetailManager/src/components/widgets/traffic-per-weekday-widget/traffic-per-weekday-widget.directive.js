(function () {

  'use strict';

  angular
    .module('shopperTrak.widgets')
    .directive('trafficPerWeekdayWidget', trafficPerWeekdayWidget);

  function trafficPerWeekdayWidget() {
    return {
      restrict: 'EA',
      templateUrl: 'components/widgets/traffic-per-weekday-widget/traffic-per-weekday-widget.partial.html',
      transclude: true,
      scope: {
        // API/data settings
        isLoading: '=?',
        exists: '=?',
        requestFailed: '=?',
        data: '=?',
        kpi: '@',
        // Organization, site
        orgId: '=',
        siteId: '=',
        zoneId: '=?',
        // Date settings
        dateRange: '=',
        compareRange1: '=',
        compareRange1Type: '=?',
        compareRange2: '=',
        compareRange2Type: '=?',
        dateFormatMask: '=',
        operatingHours: '=',
        // For localization
        currentUser: '=',
        currentOrganization: '=?',
        currentSite: '=?',
        currentZone: '=?',
        firstDayOfWeekSetting: '=',
        language: '=',
        // Widget output settings
        widgetIcon: '@',
        onExportClick: '&',
        hideExportIcon: '=?',
        exportIsDisabled: '=?',
        isShown: '=?',
        selectedTags: '=?',
        customTags: '=?',
        selectedMetric: '=?',
        selectedDays: '=?',
        orderBy: '=?',
        orderReverse: '=?',
        currencySymbol: '=?',
        salesCategories: '=?',
        setSelectedWidget: '&',
        chartType: '=?',
        businessDayStartHour: '=?',
      },
      controller: trafficPerWeekdayWidgetController,
      controllerAs: 'vm',
      bindToController: true
    };
  }

  trafficPerWeekdayWidgetController.$inject = [
    '$scope',
    '$rootScope',
    '$filter',
    '$q',
    'LocalizationService',
    'dayOfWeekDataService',
    'trafficPerWeekdayWidgetMetrics',
    'metricConstants',
    'comparisonsHelper',
    'OrganizationResource',
    'ObjectUtils',
    'SiteResource',
    'ZoneResource',
    'MallCheckService',
    'currencyService'
  ];

  function trafficPerWeekdayWidgetController(
    $scope,
    $rootScope,
    $filter,
    $q,
    LocalizationService,
    dayOfWeekDataService,
    trafficPerWeekdayWidgetMetrics,
    metricConstants,
    comparisonsHelper,
    OrganizationResource,
    ObjectUtils,
    SiteResource,
    ZoneResource,
    MallCheckService,
    currencyService
  ) {
    var vm = this;
    var dayOfWeekPrefix = 'weekdaysLong.';

    vm.isLoading = true;
    vm.exists = false;
    vm.requestFailed = false;

    if (typeof vm.selectedMetric === 'undefined') {
      vm.selectedMetric = 'traffic';
    }
    if (typeof vm.isShown === 'undefined') {
      vm.isShown = false;
    }

    vm.availableMetrics = [];
    vm.returnKeys = {};
    vm.metrics = trafficPerWeekdayWidgetMetrics.metrics;
    vm.tableData = [];

    vm.compareRangeIsPriorPeriod = compareRangeIsPriorPeriod;
    vm.compareRangeIsPriorYear = compareRangeIsPriorYear;
    vm.orderTableBy = orderTableBy;
    vm.loadChartData = loadChartData;
    vm.chartType = vm.chartType || 'column';
    vm.chartOptions = {
      seriesBarDistance: 13,
      axisX: {
        showGrid: false
      },
      chartPadding: 20
    };

    $scope.$watchGroup([
      'vm.currentOrganization',
      'vm.orgId',
      'vm.currentSite',
      'vm.currentZone',
      'vm.siteId',
      'vm.dateRange',
      'vm.compareRange1',
      'vm.compareRange2',
      'vm.selectedDays',
      'vm.selectedTags',
      'vm.salesCategories',
      'vm.chartType'
      ], loadData);
    $scope.$watchCollection('vm.currentOrganization', loadOrganizationSettings);

    vm.isPdf = $rootScope.pdf;

    activate();

    function activate() {
      // User settings are required for localization settings
      LocalizationService.setUser(vm.currentUser);

      // If currentOrganization is not provided, fetch organization settings using vm.orgId
      if (typeof vm.currentOrganization === 'undefined' && typeof vm.orgId !== 'undefined') {
        var currentOrganization;
        currentOrganization = OrganizationResource.get({
          orgId: vm.orgId
        }).$promise;
        currentOrganization.then(function (result) {
          vm.currentOrganization = result;
          LocalizationService.setOrganization(vm.currentOrganization);
          getNumberFormatInfo();
        });
      }

      if (typeof vm.currentSite === 'undefined' && typeof vm.siteId !== 'undefined') {
        var currentSite;
        currentSite = SiteResource.get({
          orgId: vm.orgId,
          siteId: vm.siteId
        }).$promise;
        currentSite.then(function (result) {
          vm.currentSite = result;
        });
      }

      if (typeof vm.currentZone === 'undefined' && typeof vm.zoneId !== 'undefined') {
        var currentZone;
        currentZone = ZoneResource().get({
          orgId: vm.orgId,
          siteId: vm.siteId,
          zoneId: vm.zoneId
        }).$promise;
        currentZone.then(function (result) {
          vm.currentZone = result;
        });
      }

      getNumberFormatInfo();
      setCurrencySymbol();
    }

    function getNumberFormatInfo() {
      vm.numberFormatName = LocalizationService.getCurrentNumberFormatName(vm.currentUser, vm.currentOrganization);
    }

    function setCurrencySymbol() {
      if (!ObjectUtils.isNullOrUndefinedOrBlank(vm.orgId) && !ObjectUtils.isNullOrUndefined(vm.siteId) && ObjectUtils.isNullOrUndefinedOrBlank(vm.currencySymbol)) {
        currencyService.getCurrencySymbol(vm.orgId, vm.siteId).then(function (data) {
          vm.currencySymbol = data.currencySymbol;
        });
      }
    }

    function loadOrganizationSettings() {
      if (typeof vm.currentOrganization !== 'undefined') {
        if (vm.numberFormatName === null) {
          vm.numberFormatName = LocalizationService.getCurrentNumberFormatName(
            vm.currentUser,
            vm.currentOrganization
          );
        }

        loadDefaultMetrics();
        loadMetricsConfiguration();

        _.each(vm.metrics, function (metricData) {
          vm.returnKeys[metricData.value] = metricData.returnKey;
        });
      }
    }

    function loadDefaultMetrics() {
      var activeSubscriptions;
      var requiredSubscription;

      if (!ObjectUtils.isNullOrUndefined(vm.currentSite)) {
        //site level
        activeSubscriptions = vm.currentSite.subscriptions;
      }

      if (ObjectUtils.isNullOrUndefined(activeSubscriptions)) {
        //org level access
        activeSubscriptions = vm.currentOrganization.subscriptions;
      }

      vm.availableMetrics = [];

      if(isMall()) {
        //add only traffic data
        vm.availableMetrics.push('traffic');
        return;
      }

      vm.metrics.forEach(function (metric) {
        requiredSubscription = getMetricSubscriptions(metric.value);
        if (orgHasSubscription(activeSubscriptions, requiredSubscription)) {
          if (vm.availableMetrics.length < 5) {
            vm.availableMetrics.push(metric.returnKey);
          }
        }
      });
    }

    function isMall() {
      if (!ObjectUtils.isNullOrUndefined(vm.currentZone)) {
        //preference for tenantcommon zonetype
        return !MallCheckService.isNotMall(vm.currentSite, vm.currentZone);
      }

      return !MallCheckService.isNotMall(vm.currentOrganization)
    }

    function orgHasSubscription(orgSubscriptions, requiredSubscriptions) {
      if (requiredSubscriptions.length === 0) {
        return true;
      } else {
        var hasRequiredSubscriptions = true;
        _.each(requiredSubscriptions, function (subscription) {
          if (orgSubscriptions[subscription] === false || typeof orgSubscriptions[subscription] === 'undefined') {
            hasRequiredSubscriptions = false;
          }
        });
        return hasRequiredSubscriptions;
      }
    }

    function getMetricSubscriptions(metric) {
      return _.findWhere(metricConstants.metrics, { value: metric }).requiredPermissions;
    }

    function getMetricPrefix(metric) {
      return _.findWhere(metricConstants.metrics, { value: metric }).prefixSymbol;
    }

    function getMetricSuffix(metric) {
      return _.findWhere(metricConstants.metrics, { value: metric }).suffixSymbol;
    }

    function loadMetricsConfiguration() {
      vm.metricsConfiguration = {};
      _.each(vm.metrics, function (metric) {

        var prefixSymbol = getMetricPrefix(metric.value);

        if (isMetricCurrency(metric.value) && !ObjectUtils.isNullOrUndefinedOrBlank(vm.currencySymbol)) {
          prefixSymbol = vm.currencySymbol;
        }

        vm.metricsConfiguration[metric.returnKey] = {
          precision: getMetricPrecision(metric.value),
          translationLabel: metric.value,
          prefixSymbol: prefixSymbol,
          suffixSymbol: getMetricSuffix(metric.value)
        };
      });
    }

    function isMetricCurrency(metric) {
      var found = _.findWhere(metricConstants.metrics, { value: metric });

      if (typeof found !== 'undefined') {
        return found.isCurrency;
      } else {
        return false;
      }
    }

    function getMetricPrecision(metric) {
      var found = _.findWhere(metricConstants.metrics, { value: metric });
      if (typeof found !== 'undefined') {
        return found.precision;
      } else {
        return 0;
      }
    }

    function zoneNotLoadedIfNecessary() {
      return ObjectUtils.isNullOrUndefined(vm.currentZone) && !ObjectUtils.isNullOrUndefined(vm.zoneId);
    }

    function getOperatingHours(labels) {
      return labels.slice(Number(vm.businessDayStartHour));
    }

    function loadData() {
      vm.isLoading = true;

      if(ObjectUtils.isNullOrUndefinedOrEmpty(vm.selectedDays)) {
        return true;
      }

      vm.isHourly = (vm.selectedDays[0].key === 'hours');

      if(ObjectUtils.isNullOrUndefined(vm.orderBy)) {
        if(vm.isHourly) {
          vm.orderBy = 'hourOfDayIndex';
        } else {
          vm.orderBy = 'dayOfWeekIndex';
        }
        vm.orderReverse = true;
      }

      if (ObjectUtils.isNullOrUndefined(vm.currentOrganization)) {
        return true;
      }

      if(zoneNotLoadedIfNecessary()) {
        return true;
      }

      loadDefaultMetrics();
      loadMetricsConfiguration();

      var weekdays = moment.weekdays();
      _.each(weekdays, function (weekday, key) {
        weekdays[key] = $filter('translate')('weekdaysLong.' + weekday.toLowerCase());
      });

      if (vm.firstDay === 1) {
        var last = weekdays.shift();
        weekdays.push(last);
      }

      var promises = [];

      // Load chart data

      var widgetDataDefered = $q.defer();
      promises.push(widgetDataDefered.promise);
      getChartData(vm.dateRange)
        .then(function (chartData) {
          chartData.labels = vm.operatingHours ? getOperatingHours(chartData.labels) : chartData.labels;
          chartData.series[0] = vm.operatingHours ? getOperatingHours(chartData.series[0]) : chartData.series[0];
          vm.widgetData = chartData;
          widgetDataDefered.resolve();
        });

      var compare1DataDefered = $q.defer();
      promises.push(compare1DataDefered.promise);
      getChartData(vm.compareRange1)
        .then(function (chartData) {
          vm.compareRange1data = chartData;
          compare1DataDefered.resolve();
        });

      var compare2DataDefered = $q.defer();
      promises.push(compare2DataDefered.promise);
      getChartData(vm.compareRange2)
        .then(function (chartData) {
          vm.compareRange2data = chartData;
          compare2DataDefered.resolve();
        });

      // Load table data

      var tableDataDefered = $q.defer();
      promises.push(tableDataDefered.promise);
      getTableData(vm.dateRange)
        .then(function (tableData) {
          vm.tableData = tableData.tableData;
          vm.averageTableData = tableData.averages;
          vm.hasData = hasTableData(vm.tableData);
          vm.orderTableBy(vm.orderBy);
          tableDataDefered.resolve();
        });

      var tableCompareDataDefered = $q.defer();
      promises.push(tableCompareDataDefered.promise);
      getTableData(vm.compareRange1)
        .then(function (tableData) {
          vm.tableCompareData = tableData.tableData;
          vm.averageCompareTableData = tableData.averages;
          tableCompareDataDefered.resolve();
        });

      $q.all(promises).then(function () {
        vm.isLoading = false;
        vm.exists = true;
        combineChartData();
        combineTableData();
      });
    }

    function hasTableData(data) {
      var fullMetrics = vm.availableMetrics;
      var hasData = false;

      for(var i = 0; i < data.length; i++) {
        var row = data[i];
        for(var j = 0; j < fullMetrics.length; j++) {
          var metric = fullMetrics[j];
          if(!ObjectUtils.isNullOrUndefined(row[metric])) {
            hasData = true;
          }
        }
      }

      return hasData;
    }

    function loadChartData(metric) {
      var promises = [];
      vm.isLoading = true;
      vm.selectedMetric = metric;

      var widgetDataDefered = $q.defer();
      promises.push(widgetDataDefered.promise);
      getChartData(vm.dateRange)
        .then(function (chartData) {
          vm.widgetData = chartData;
          widgetDataDefered.resolve();
        });

      var compare1DataDefered = $q.defer();
      promises.push(compare1DataDefered.promise);
      getChartData(vm.compareRange1)
        .then(function (chartData) {
          vm.compareRange1data = chartData;
          compare1DataDefered.resolve();
        });

      var compare2DataDefered = $q.defer();
      promises.push(compare2DataDefered.promise);
      getChartData(vm.compareRange2)
        .then(function (chartData) {
          vm.compareRange2data = chartData;
          compare2DataDefered.resolve();
        });

      $q.all(promises).then(function () {
        vm.isLoading = false;
        combineChartData();
      });
    }

    function combineChartData() {
      if(vm.isHourly) {
        if(vm.widgetData.labels.length > 16) {
          vm.thinnerBars = true;
          if($rootScope.pdf ) {
            vm.chartOptions.seriesBarDistance = 8;
          }
        }
        vm.widgetData.series[1] = buildCompareSeriesForHourly(vm.compareRange1data);
        vm.widgetData.series[2] = buildCompareSeriesForHourly(vm.compareRange2data);
      } else {
        // Combine chart data returned by the data service
        vm.widgetData.series[1] = vm.compareRange1data.series[0];
        vm.widgetData.series[2] = vm.compareRange2data.series[0];
        vm.thinnerBars = false;
        vm.chartOptions.seriesBarDistance = 13;
      }
    }

  /** Builds chart data for hourly data.
   *  This is necessary because different days can have different operating hours
   *  This function makes sure that the hours tie up
   *
   *  @param {object} user preferences
   **/
    function buildCompareSeriesForHourly(compareDataSeries) {
      var compareData = [];

      _.each(vm.widgetData.labels, function(hour) {
        var index = _.indexOf(compareDataSeries.labels, hour);

        if(index === -1) {
          compareData.push(undefined);
        } else {
          var dataForHour = compareDataSeries.series[0][index];
          compareData.push(dataForHour);
        }
      });

      return compareData;
    }

    function combineTableData() {
      // Calculate change percentages of the table metrics using vm.tableData and vm.tableCompareData
      _.each(vm.tableData, function (row) {
        var compareDay;
        if (!ObjectUtils.isNullOrUndefined(row.hourOfDay)) {
          compareDay = _.findWhere(vm.tableCompareData, { hourOfDay: row.hourOfDay });
        } else {
          compareDay = _.findWhere(vm.tableCompareData, { dayOfWeek: row.dayOfWeek });
        }
        _.each(vm.availableMetrics, function (metric) {
          // calculate change
          row[metric + '_change'] = comparisonsHelper.getComparisonData(row[metric], compareDay[metric]).percentageChange;
          row[metric + '_change_type'] = comparisonsHelper.getComparisonData(row[metric], compareDay[metric]).deltaColoringPeriod;
          row[metric + '_change_numeric'] = parseFloat(comparisonsHelper.getComparisonData(row[metric], compareDay[metric]).percentageChange);

          if(ObjectUtils.isNullOrUndefined(row[metric])) {
            row[metric] = null;
          }

        });
      });

      _.each(vm.availableMetrics, function (metric) {
        vm.averageTableData[metric + '_change'] = comparisonsHelper.getComparisonData(vm.averageTableData[metric], vm.averageCompareTableData[metric]).percentageChange;
        vm.averageTableData[metric + '_change_type'] = comparisonsHelper.getComparisonData(vm.averageTableData[metric], vm.averageCompareTableData[metric]).deltaColoringPeriod;
        vm.averageTableData[metric + '_change_numeric'] = parseFloat(comparisonsHelper.getComparisonData(vm.averageTableData[metric], vm.averageCompareTableData[metric]).percentageChange);
      });
    }

    function getChartData(dates) {
      var dataRequest = buildRequestParams(dates);

      return dayOfWeekDataService.getDataForChart(dataRequest, vm.isHourly);
    }

    function getTableData(dates) {
     var dataRequest = buildRequestParams(dates, vm.isHourly);

      return dayOfWeekDataService.getMetricContributionDataForTable(dataRequest);
    }

    function buildRequestParams(dates, isHourly) {

      var requestParams = {
        metrics: vm.availableMetrics,
        startDate: dates.start,
        endDate: dates.end,
        organizationId: vm.orgId || vm.currentOrganization.organization_id,
        selectedTags: vm.selectedTags,
        siteId: getSiteId(),
        zoneId: vm.zoneId,
        daysOfWeek: getChartLabels(),
        salesCategories: vm.salesCategories,
        operatingHours: vm.operatingHours,
        trafficOnly: isMall(),
        selectedMetric: vm.selectedMetric
      };

      if( !ObjectUtils.isNullOrUndefinedOrEmpty(vm.customTags) ) {
        requestParams.customTagId = vm.customTags;
      }

      if (isHourly) {
        var hours = [];
        var hourCount = 0;

        while (hourCount < 24) {
          hours.push(hourCount);
          hourCount++;
        }

        requestParams.hoursOfDay = hours;
        delete requestParams.daysOfWeek;
      }

      return requestParams;
    }

    function getChartLabels() {
      if (vm.selectedDays && !vm.isHourly) {
        var orderedDays = vm.selectedDays.map(function (day) {
          return dayOfWeekPrefix + day.key;
        });

        return orderedDays;
      }
    }

    function compareRangeIsPriorPeriod(comparePeriodType) {
      return comparePeriodType === 'prior_period';
    }

    function compareRangeIsPriorYear(comparePeriodType) {
      return comparePeriodType === 'prior_year';
    }

    function getSiteId() {
      if (ObjectUtils.isNullOrUndefined(vm.currentSite) && typeof vm.siteId === 'undefined') {
        return undefined;
      }

      if (typeof vm.siteId === 'undefined') {
        return vm.currentSite.site_id;
      } else {
        return vm.siteId;
      }
    }

    function orderTableBy(metric) {
      if (vm.orderBy === metric && vm.orderReverse) {
        vm.orderBy = metric;
        vm.orderReverse = false;
      } else {
        vm.orderBy = metric;
        vm.orderReverse = true;
      }
      vm.tableData = $filter('orderBy')(vm.tableData, vm.orderBy, vm.orderReverse);
    }
  }

})();
