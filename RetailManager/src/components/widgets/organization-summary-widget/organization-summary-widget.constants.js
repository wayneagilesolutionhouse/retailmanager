(function() {
  'use strict';

  angular
    .module('shopperTrak.widgets')
    .constant('organizationSummaryWidgetConstants', {
      fractionSizes: {
        'traffic': 0,
        'gsh': 0,
        'loyalty': 2
      }
    });
})();
