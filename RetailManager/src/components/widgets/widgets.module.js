(function() {
  'use strict';

  angular.module('shopperTrak.widgets', [
    'offClick',
    'shopperTrak.charts',
    'shopperTrak.config',
    'shopperTrak.leaflet',
    'shopperTrak.resources',
    'shopperTrak.utils',
    'shopperTrak.constants'
  ]);
})();
