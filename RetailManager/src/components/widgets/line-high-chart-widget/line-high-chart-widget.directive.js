(function () {
  'use strict';

  angular
    .module('shopperTrak.widgets')
    .directive('lineHighChartWidget', lineHighChartWidget);

  function lineHighChartWidget() {
    return {
      templateUrl: 'components/widgets/line-high-chart-widget/line-high-chart-widget.partial.html',
      scope: {
        orgId: '=',
        siteId: '=',
        locationId: '=',
        zoneId: '=',
        // Date settings
        dateRangeStart: '=',
        dateRangeEnd: '=',
        compareRange1Start: '=',
        compareRange1End: '=',
        compareRange1Type: '=?',
        compareRange2Start: '=',
        compareRange2End: '=',
        compareRange2Type: '=?',
        // For localization
        currentUser: '=',
        currentOrganization: '=',
        //requires for weather
        currentSite: '=?',
        // API/data settings
        operatingHours: '=',
        firstDayOfWeekSetting: '=',
        getUniqueReturning: '=',
        summaryAverages: '=',
        apiEndpoint: '@',
        apiReturnKey: '@',
        separateSummaryRequests: '=?',
        multiplier: '=?',
        groupBy: '=?',
        // Widget output settings
        widgetIcon: '@',
        kpi: '=?',
        kpiLabel: '@',
        valueLabel: '@',
        onExportClick: '&',
        dateFormatMask: '=',
        returnDataPrecision: '=?',
        hideExportIcon: '=?',
        exportIsDisabled: '=?',
        language: '=',
        exporting: '=?',
        showMetrics: '=?',
        currencySymbol: '=?', // used only by the PDF exports
        salesCategories: '=?',
        isLoading: '=?',
        setSelectedWidget: '&',
        selectedOption: '=?',
        onSelectOption: '&',
        showWeatherMetrics: '=?',
        selectedWeatherMetrics: '=?',
        updateSelectedWeatherMetrics: '=?',
        chartType: '=?',
        isHourly: '=?',
        businessDayStartHour: '=?',
        showTable: '=?',
        showTableToggleButton: '=?',
        sortInfo: '=?',
        currentSortDirection: '=?'
      },
      controller: lineHighChartWidgetController,
      controllerAs: 'lineHighChartWidget',
      bindToController: true
    };
  }


  lineHighChartWidgetController.$inject = [
    '$scope',
    '$rootScope',
    '$timeout',
    'requestManager',
    '$q',
    '$filter',
    '$state',
    '$translate',
    'LocalizationService',
    'SubscriptionsService',
    'SiteResource',
    'apiUrl',
    'utils',
    'NumberUtils',
    'metricConstants',
    'comparisonsHelper',
    'ObjectUtils',
    'googleAnalytics',
    'getDefaultComparisonDateRangeParams',
    'currencyService'
  ];

  function lineHighChartWidgetController(
    $scope,
    $rootScope,
    $timeout,
    requestManager,
    $q,
    $filter,
    $state,
    $translate,
    LocalizationService,
    SubscriptionsService,
    SiteResource,
    apiUrl,
    utils,
    NumberUtils,
    metricConstants,
    comparisonsHelper,
    ObjectUtils,
    googleAnalytics,
    getDefaultComparisonDateRangeParams,
    currencyService) {

    var lineHighChartWidget = this;
    var periodLabelsRepresentSingleDay;

    // Current, Prior, Previous
    var dateLabels = [[], [], []];

    // Let's smooth out those bumpy signals...
    var updateWidgetDebounced = _.debounce(function() {
      updateWidget();
    }, 200);

    activate();

    function setWatches() {
      lineHighChartWidget.orgChange = $scope.$watchGroup([
        'lineHighChartWidget.orgId',
        'lineHighChartWidget.siteId',
        'lineHighChartWidget.zoneId',
      ], updateWithOptions);

      lineHighChartWidget.groupChange = $scope.$watchGroup([
        'lineHighChartWidget.locationId',
        'lineHighChartWidget.dateRangeStart',
        'lineHighChartWidget.dateRangeEnd',
        'lineHighChartWidget.groupBy',
        'lineHighChartWidget.salesCategories',
        'lineHighChartWidget.selectedWeatherMetrics',
        'lineHighChartWidget.chartType'
      ], updateWidgetDebounced);

      lineHighChartWidget.requestQueueChange = $scope.$watchCollection('lineHighChartWidget.requestQueue', loadDetails);

      lineHighChartWidget.prefixSymbolChange = $scope.$watch('lineHighChartWidget.selectedOption.metric.prefixSymbol', function (newValue, oldValue) {
        if (newValue !== oldValue) {
          lineHighChartWidget.selectedOption.metric.prefixSymbol = lineHighChartWidget.currencySymbol;
        }
      });

      $scope.$on('$destroy', function () {
        cancelAllOutstandingRequests();
        if (typeof lineHighChartWidget.requestQueueChange === 'function') {
          lineHighChartWidget.requestQueueChange();
        }

        if (typeof lineHighChartWidget.groupChange === 'function') {
          lineHighChartWidget.groupChange();
        }

        if (typeof lineHighChartWidget.orgChange === 'function') {
          lineHighChartWidget.orgChange();
        }

        if (typeof lineHighChartWidget.prefixSymbolChange === 'function') {
          lineHighChartWidget.prefixSymbolChange();
        }
      });
    }

    function activate() {
      lineHighChartWidget.isPdf = $rootScope.pdf;
      lineHighChartWidget.coloring = 'positive';
      lineHighChartWidget.isLoading = true;
      lineHighChartWidget.requestFailed = false;
      lineHighChartWidget.requestsLoading = 0;
      lineHighChartWidget.numberFormatName = LocalizationService.getCurrentNumberFormatName(lineHighChartWidget.currentUser, lineHighChartWidget.currentOrganization);
      lineHighChartWidget.trueVal = true;
      lineHighChartWidget.selectedDate = null;
      lineHighChartWidget.toggleTableVisibility = toggleTableVisibility;
      lineHighChartWidget.chartType = lineHighChartWidget.chartType || 'line';
      lineHighChartWidget.sortTable = sortTable;
      lineHighChartWidget.currentSort = 'hourSort';

      if(_.isUndefined(lineHighChartWidget.currentSortDirection)) {
        lineHighChartWidget.currentSortDirection = 'asc';
      }

      if(lineHighChartWidget.isHourly && angular.isUndefined(lineHighChartWidget.showTable)) {
        lineHighChartWidget.showTable = {
          selection: true
        };
      }

      /* Tooltip requests are fired when hovering. This can generate a lot of concurrent requests.
        This is the maximum number of concurrent request. Note! 1 request here means 3 requests,
        for each date period.
      */
      lineHighChartWidget.maxConcurrentTooltips = 2;

      lineHighChartWidget.loadTooltipData = loadTooltipData;
      lineHighChartWidget.loadWidgetDefaults = loadWidgetDefaults;
      lineHighChartWidget.getCommonRequestParams = getCommonRequestParams;
      lineHighChartWidget.formatDate = formatDate;
      lineHighChartWidget.setGroupBy = setGroupBy;
      lineHighChartWidget.hasData = hasData;
      lineHighChartWidget.setLegendLabel = setLegendLabel;
      lineHighChartWidget.compareRangeIsPriorPeriod = compareRangeIsPriorPeriod;
      lineHighChartWidget.compareRangeIsPriorYear = compareRangeIsPriorYear;
      lineHighChartWidget.calculateDelta = calculateDelta;

      lineHighChartWidget.isDailyBreakdown = (moment.utc(lineHighChartWidget.compareRange1Start).format('YYYY-MM-DD') === moment.utc(lineHighChartWidget.compareRange1End).format('YYYY-MM-DD')
                                                && !lineHighChartWidget.isHourly);

      if(!$rootScope.pdf){
        lineHighChartWidget.tabWidget = getGroupKey();
      }

      initGroupBy();

      lineHighChartWidget.dateRanges = [];
      lineHighChartWidget.requestQueue = [];

      lineHighChartWidget.isCaching = true;

      if(ObjectUtils.isNullOrUndefined(lineHighChartWidget.valueLabel)) {
        lineHighChartWidget.valueLabel= '';
      }

      if(lineHighChartWidget.isPdf || $rootScope.customDashboards) {
        setOptionsForPdf();
      }

      if (ObjectUtils.isNullOrUndefined(lineHighChartWidget.currentSite) &&
        !ObjectUtils.isNullOrUndefined(lineHighChartWidget.siteId)) {
        setCurrentSite();
        return;
      }

      lineHighChartWidget.weatherEnabled = hasUserEnabledWeather();

      init();
    }

    function initGroupBy() {
      if(ObjectUtils.isNullOrUndefined($rootScope.groupBy)){
        $rootScope.groupBy = {};
        if(!ObjectUtils.isNullOrUndefinedOrBlank(lineHighChartWidget.groupBy) ) {
          setRootScopeGroupBy(lineHighChartWidget.groupBy);
        }
        return;
      }

      if(ObjectUtils.isNullOrUndefinedOrBlank(lineHighChartWidget.groupBy) ) {
        lineHighChartWidget.groupBy = getRootScopeGroupBy();
      }

      if (lineHighChartWidget.isHourly) {
        lineHighChartWidget.groupBy = 'hour';
      }
    }

    function init() {
      lineHighChartWidget.setOption = setOption;

      loadTranslations();

      loadWidgetDefaults();

      if (!ObjectUtils.isNullOrUndefined(lineHighChartWidget.showMetrics)) {
        setOptions();
      }

      setCurrencySymbol()
        .then(setDataFormats)
        .catch(setDataFormats);

      setWeatherMetrics();

      setWeatherSplitOption();

      setWatches();
    }

    function hasUserEnabledWeather() {

      if(ObjectUtils.isNullOrUndefined(lineHighChartWidget.currentUser)) {
        return false;
      }

      if(ObjectUtils.isNullOrUndefined(lineHighChartWidget.currentUser.preferences)) {
        return false;
      }

      if(!lineHighChartWidget.showWeatherMetrics) {
        return false;
      }

      return lineHighChartWidget.currentUser.preferences.weather_reporting === true;
    }

    function setWeatherSplitOption() {
      if(!lineHighChartWidget.showWeatherMetrics) {
        return;
      }
      var start = moment(lineHighChartWidget.dateRangeStart);
      var end = moment(lineHighChartWidget.dateRangeEnd);
      lineHighChartWidget.splitWeatherRequests = moment.duration(end.diff(start)).asMonths() > 1;
    }

    function setCurrentSite() {
      var currentSite;

      currentSite = SiteResource.get({
        orgId: lineHighChartWidget.orgId,
        siteId: lineHighChartWidget.siteId
      }).$promise;

      currentSite.then(function (result) {
        lineHighChartWidget.currentSite = result;
        init();
      });
    }

    function setOption (option) {
      if (lineHighChartWidget.selectedOption.name === option.name) {
        return;
      }

      lineHighChartWidget.isLoading = true;

      lineHighChartWidget.selectedOption = option;

      lineHighChartWidget.kpi = lineHighChartWidget.selectedOption.name;

      lineHighChartWidget.apiReturnKey = lineHighChartWidget.selectedOption.propertyName;

      if (lineHighChartWidget.selectedOption.metric.subscription !== 'sales') {
        delete lineHighChartWidget.salesCategories;
        lineHighChartWidget.showSalesCategoriesSelector = false;
      } else {
        lineHighChartWidget.showSalesCategoriesSelector = true;
      }

      setRootScopeGroupBy(lineHighChartWidget.groupBy);

      setDataFormats();

      lineHighChartWidget.onSelectOption({option: option});
      updateWidgetDebounced();
    }

    function updateMetricUnits(displayInfo) {
      _.each(displayInfo, function (metric) {
        if (metric.unitName === 'temperature' && _.has(lineHighChartWidget.currentUser.localization, 'temperature_format')) {

          var weatherSuffixesToTranslate = ['common.SHOWWEATHERTEMPCENTSUFFIX', 'common.SHOWWEATHERTEMPFAHRSUFFIX'];

          $translate(weatherSuffixesToTranslate).then(function(translations) {

            if (lineHighChartWidget.currentUser.localization.temperature_format === 'C') {
              metric.suffixSymbol = '\u00B0' + translations['common.SHOWWEATHERTEMPCENTSUFFIX'];
              metric.unit = 'c';
            }
            else if (lineHighChartWidget.currentUser.localization.temperature_format === 'F') {
              metric.suffixSymbol = '\u00B0' + translations['common.SHOWWEATHERTEMPFAHRSUFFIX'];
              metric.unit = 'f';
            }
          })
        } else if (metric.unitName === 'weatherSpeed' && _.has(lineHighChartWidget.currentUser.localization,'velocity_format')) {

          var suffixesToTranslate = ['common.SHOWWEATHERWINDKILOSUFFIX', 'common.SHOWWEATHERWINDMILESUFFIX'];

          $translate(suffixesToTranslate).then(function(translations) {
            if (lineHighChartWidget.currentUser.localization.velocity_format === 'KPH') {
              metric.suffixSymbol = translations['common.SHOWWEATHERWINDKILOSUFFIX'];
              metric.unit = 'kmph';
            }
            else if (lineHighChartWidget.currentUser.localization.velocity_format === 'MPH') {
              metric.suffixSymbol = translations['common.SHOWWEATHERWINDMILESUFFIX'];
              metric.unit = 'mph';
            }
          });
        }
      });
      return displayInfo;
    }

    function setWeatherMetrics() {
      if(!lineHighChartWidget.showWeatherMetrics || lineHighChartWidget.isHourly || lineHighChartWidget.groupBy === 'hour') {
        return;
      }

      lineHighChartWidget.weatherMetricsDisplayInfo = updateMetricUnits(angular.copy(metricConstants.weatherMetrics));

      if($rootScope.pdf || $rootScope.customDashboards) {
        if(lineHighChartWidget.selectedWeatherMetrics){
          var selectedMetrics = [];
          _.each(lineHighChartWidget.selectedWeatherMetrics, function(item){
            var detailMetric = _.where(lineHighChartWidget.weatherMetricsDisplayInfo, item);
            selectedMetrics.push(detailMetric[0]);
          });
          lineHighChartWidget.selectedWeatherMetrics = selectedMetrics;
        }
        return;
      }

      if(ObjectUtils.isNullOrUndefined(lineHighChartWidget.selectedWeatherMetrics)) {
        lineHighChartWidget.selectedWeatherMetrics = [];
        lineHighChartWidget.selectedWeatherMetrics.push(lineHighChartWidget.weatherMetricsDisplayInfo[0]);
      }

      lineHighChartWidget.minWeatherMetricsLength = 0;
      lineHighChartWidget.maxWeatherMetricsLength = 3;
    }

    function setDataFormats() {
      var formatting = getMetricsFormatting(lineHighChartWidget.kpi);

      if( !ObjectUtils.isNullOrUndefined(formatting) ) {
        lineHighChartWidget.dataPrecision = formatting.precision;
        lineHighChartWidget.dataPrefix = formatting.prefixSymbol;
        lineHighChartWidget.dataSuffix = formatting.suffixSymbol;

        if(formatting.isCurrency && !ObjectUtils.isNullOrUndefined(lineHighChartWidget.currencySymbol)) {
          lineHighChartWidget.dataPrefix = lineHighChartWidget.currencySymbol;
        }
      } else {
        lineHighChartWidget.dataPrecision = lineHighChartWidget.returnDataPrecision;
        lineHighChartWidget.dataPrefix = '';
        lineHighChartWidget.dataSuffix = '';
      }
    }

    function loadWidgetDefaults() {
      if (typeof lineHighChartWidget.groupBy !== 'string' ||
        ObjectUtils.isNullOrUndefinedOrEmpty(lineHighChartWidget.groupBy) &&
        !$rootScope.pdf && !$rootScope.customDashboards){
        lineHighChartWidget.groupBy = 'day';
        if(!ObjectUtils.isNullOrUndefinedOrEmpty($state.current.views)){
          setRootScopeGroupBy('day')
        }
      }

      if( ObjectUtils.isNullOrUndefined(lineHighChartWidget.returnDataPrecision) ) {
        lineHighChartWidget.returnDataPrecision = 0;
      }

      if( ObjectUtils.isNullOrUndefined(lineHighChartWidget.multiplier) ) {
        lineHighChartWidget.multiplier = 1;
      }

      lineHighChartWidget.compare1Period = {
        start: lineHighChartWidget.compareRange1Start,
        end: lineHighChartWidget.compareRange1End
      };

      lineHighChartWidget.compare2Period = {
        start: lineHighChartWidget.compareRange2Start,
        end: lineHighChartWidget.compareRange2End
      };
    }

    function getMetricsFormatting(value) {
      var metric = _.find(metricConstants.metrics, function(_metric) {
        return _metric.value === value;
      });

      if(!ObjectUtils.isNullOrUndefined(metric)) {
        var formatting = {
          precision: metric.precision,
          prefixSymbol: metric.prefixSymbol,
          suffixSymbol: metric.suffixSymbol,
          isCurrency: metric.isCurrency
        };
        return formatting;
      }

      return;
    }

    function cancelAllOutstandingRequests() {
      if(!ObjectUtils.isNullOrUndefinedOrEmpty(lineHighChartWidget.outStandingRequests)) {
        var requests = angular.copy(lineHighChartWidget.outStandingRequests);
        requestManager.cancelRequests(requests, 'line highchart user cancelling');
        lineHighChartWidget.outstandingRequests = _.without(lineHighChartWidget.outstandingRequests, requests);
      }

    };

    /** Updates the widget. Should be called after a load, and should not be called directly.
     *  Instead, please call the debounced version of this function (updateWidgetDebounced)
     *
     **/
    function updateWidget() {
      setDateRangeIndicators();
      lineHighChartWidget.isLoading = true;
      lineHighChartWidget.dataReady = false;
      lineHighChartWidget.requestFailed = false;
      lineHighChartWidget.isChartLoading = true;
      lineHighChartWidget.chartRequestFailed = false;
      lineHighChartWidget.noData = null;
      if (typeof lineHighChartWidget.updateSelectedWeatherMetrics === 'function') {
        lineHighChartWidget.updateSelectedWeatherMetrics(lineHighChartWidget.selectedWeatherMetrics);
      }

      lineHighChartWidget.chartData = {
        labels: [],
        series: [[], [], []]
      };

      lineHighChartWidget.summaryData = [null, null, null];
      lineHighChartWidget.delta = [{}, {}, {}];
      lineHighChartWidget.returnData = [];
      lineHighChartWidget.uniqueData = [];
      lineHighChartWidget.totalData = [];
      lineHighChartWidget.separateSummaryRequests = true;

      if (ObjectUtils.isNullOrUndefined(lineHighChartWidget.apiEndpoint)) {
        lineHighChartWidget.isLoading = false;
        lineHighChartWidget.requestFailed = true;
        return true;
      }

      lineHighChartWidget.tooltipData = [];

      fetchChartData()
        .then(transformChartData)
        .catch(function (err) {
          if(err !== 'line highchart user cancelling') {
            lineHighChartWidget.isChartLoading = false;
            lineHighChartWidget.chartRequestFailed = true;
            lineHighChartWidget.isLoading = false;
            lineHighChartWidget.requestFailed = true;
          }
        });
    }

    /** Sets boolean values indicating if the current date range:
     *  1. Spans over two calendar weeks
     *  2. Spans over two calendar months
     *  This function is called during the update cycle.
     *
     **/
    function setDateRangeIndicators() {
      lineHighChartWidget.dateRangeSpansOverTwoCalendarWeeks = utils.dateRangeSpansOverTwoCalendarWeeks(lineHighChartWidget.dateRangeStart, lineHighChartWidget.dateRangeEnd);
      lineHighChartWidget.dateRangeSpansOverTwoCalendarMonths = utils.dateRangeSpansOverTwoCalendarMonths(lineHighChartWidget.dateRangeStart, lineHighChartWidget.dateRangeEnd);
    }

    function updateWithOptions() {
      cancelAllOutstandingRequests();
      if (!ObjectUtils.isNullOrUndefined(lineHighChartWidget.showMetrics)) {
        setOptions();
        return;
      }
      updateWidgetDebounced();
    }

    function setDefaultOption() {
      switch (lineHighChartWidget.kpi) {
        case 'sales':
          lineHighChartWidget.options = [getSalesOption()];
          break;
        case 'conversion':
          lineHighChartWidget.options = [getConversionOption()];
          break;
        case 'ats':
          lineHighChartWidget.options = [getAtsOption()];
          break;
        case 'star':
          lineHighChartWidget.options = [getStarOption()];
          break;
        case 'upt':
          lineHighChartWidget.options = [getUptOption()];
          break;
        case 'labor_hours':
          lineHighChartWidget.options = [getLaborOption()];
          break;
        case 'visitor_behaviour_traffic':
          lineHighChartWidget.options = [getVisitorBehaviourOption()];
          break;
        case 'gsh':
          lineHighChartWidget.options = [getGrossShoppingOption()];
          break;
        case 'draw_rate':
          lineHighChartWidget.options = [getDrawRateOption()];
          break;
        case 'opportunity':
          lineHighChartWidget.options = [getOpportunityOption()];
          break;
        case 'dwell_time':
          lineHighChartWidget.options = [getDwellTimeOption()];
          break;
        case 'abandonment_rate':
          lineHighChartWidget.options = [getAbandonmentRateOption()];
          break;
        default:
          lineHighChartWidget.options = [getTrafficOption()];
          break;
      }
    }

    function setOptionsForPdf() {
      switch (lineHighChartWidget.kpi) {
        case 'sales':
          lineHighChartWidget.selectedOption = getSalesOption();
          break;
        case 'conversion':
          lineHighChartWidget.selectedOption = getConversionOption();
          break;
        case 'ats':
          lineHighChartWidget.selectedOption = getAtsOption();
          break;
        case 'star':
          lineHighChartWidget.selectedOption = getStarOption();
          break;
        case 'upt':
          lineHighChartWidget.selectedOption = getUptOption();
          break;
        case 'labor_hours':
          lineHighChartWidget.selectedOption = getLaborOption();
          break;
        case 'visitor_behaviour_traffic':
          lineHighChartWidget.selectedOption = getVisitorBehaviourOption();
          break;
        case 'gsh':
          lineHighChartWidget.selectedOption = getGrossShoppingOption();
          break;
        case 'draw_rate':
          lineHighChartWidget.selectedOption = getDrawRateOption();
          break;
        case 'opportunity':
          lineHighChartWidget.selectedOption = getOpportunityOption();
          break;
        case 'dwell_time':
          lineHighChartWidget.selectedOption = getDwellTimeOption();
          break;
        case 'abandonment_rate':
          lineHighChartWidget.selectedOption = getAbandonmentRateOption();
          break;
        default:
          lineHighChartWidget.selectedOption = getTrafficOption();
          break;
      }

      if(!lineHighChartWidget.kpi.match(/(sales|ats)/)) {
        lineHighChartWidget.currencySymbol = '';
      } else {
        lineHighChartWidget.selectedOption.metric.prefixSymbol = lineHighChartWidget.currencySymbol;
        lineHighChartWidget.dataPrefix = lineHighChartWidget.currencySymbol;
      }
    }

    function getTrafficOption() {
      return {
        name: 'traffic',
        displayType: 'traffic',
        propertyName: 'total_traffic',
        metric: getMetric('traffic')
      };
    }

    function getSalesOption() {
      return {
        name: 'sales',
        displayType: 'sales',
        propertyName: 'sales_amount',
        metric: getMetric('sales')
      };
    }

    function getConversionOption() {
      return {
        name: 'conversion',
        displayType: 'conversion',
        propertyName: 'conversion',
        metric: getMetric('conversion')
      };
    }

    function getAtsOption() {
      return {
        name: 'ats',
        displayType: 'ats',
        propertyName: 'ats',
        metric: getMetric('ats')
      };
    }

    function getStarOption() {
      return {
        name: 'star',
        displayType: 'star',
        propertyName: 'star',
        metric: getMetric('star')
      };
    }

    function getUptOption() {
      return {
        name: 'upt',
        displayType: 'upt',
        propertyName: 'upt',
        metric: getMetric('upt')
      };
    }

    function getLaborOption() {
      return {
        name: 'labor_hours',
        displayType: 'labor_hours',
        propertyName: 'labor_hours',
        metric: getMetric('labor')
      };
    }

    function getVisitorBehaviourOption() {
      return {
        name: 'visitor_behaviour_traffic',
        displayType: 'visitor_behaviour_traffic',
        propertyName: 'total_traffic',
        metric: getMetric('traffic')
      };
    }

    function getGrossShoppingOption() {
      return {
        name: 'gsh',
        displayType: 'gsh',
        propertyName: 'gross_shopping_hours',
        metric: getMetric('gsh')
      };
    }

    function getDrawRateOption() {
      return {
        name: 'draw_rate',
        displayType: 'draw_rate',
        propertyName: 'average_draw_rate',
        metric: getMetric('draw_rate')
      };
    }

    function getOpportunityOption() {
      return {
        name: 'opportunity',
        displayType: 'opportunity',
        propertyName: 'total_opportunity',
        metric: getMetric('opportunity')
      };
    }

    function getDwellTimeOption() {
      return {
        name: 'dwell_time',
        displayType: 'dwelltime',
        propertyName: 'average_dwelltime',
        metric: getMetric('dwelltime')
      };
    }

    function getAbandonmentRateOption(){
      return {
        name: 'abandonment_rate',
        displayType: 'abandonment_rate',
        propertyName: 'average_abandonment_rate',
        metric: getMetric('abandonment_rate')
      }
    };

    function setOptions() {
      if (lineHighChartWidget.showMetrics !== true) {
        setDefaultOption();
      }
      else {
        lineHighChartWidget.options = [getTrafficOption()];
        var saleSubscription = SubscriptionsService.siteHasSales(lineHighChartWidget.currentOrganization, lineHighChartWidget.currentSite);

        if (saleSubscription) {
          lineHighChartWidget.options.push(getSalesOption(), getConversionOption(), getAtsOption());
        }

        var laborSubscription = SubscriptionsService.siteHasLabor(lineHighChartWidget.currentOrganization, lineHighChartWidget.currentSite);

        if (laborSubscription) {
          lineHighChartWidget.options.push(getStarOption());
        }
      }

      if(!$rootScope.customDashboards) {
        lineHighChartWidget.selectedOption = lineHighChartWidget.options[0];
      } else {
        var search = { name: lineHighChartWidget.kpi };
        lineHighChartWidget.selectedOption = _.findWhere(lineHighChartWidget.options, search);
      }

      if (lineHighChartWidget.selectedOption.metric.isCurrency && !ObjectUtils.isNullOrUndefinedOrBlank(lineHighChartWidget.currencySymbol)) {
        lineHighChartWidget.selectedOption.metric.prefixSymbol = lineHighChartWidget.currencySymbol;
      }
      if(lineHighChartWidget.kpi !== 'upt') {
        lineHighChartWidget.kpi = lineHighChartWidget.selectedOption.name;
      }
    }

    function getMetric(id) {
      return _.findWhere(metricConstants.metrics, { value: id });
    }

    function getApiUrl() {
      if
      ((ObjectUtils.isNullOrUndefined(lineHighChartWidget.selectedOption) &&
        ObjectUtils.isNullOrUndefinedOrBlank(lineHighChartWidget.kpi)) ||
        lineHighChartWidget.kpi.match(/(traffic|visitor_behaviour_traffic|gsh|draw_rate|opportunity|dwell_time|abandonment_rate)/) ||
        (!ObjectUtils.isNullOrUndefined(lineHighChartWidget.selectedOption) &&
        lineHighChartWidget.selectedOption.name === 'traffic')) {

        return lineHighChartWidget.apiEndpoint;
      }

      return 'kpis/sales';
    }

    function fetchChartData() {
     return $q.all(getChartRequests());
    }

    function getChartRequests() {
      var commonRequestParams = getCommonRequestParams();
      var _apiUrl = apiUrl + '/' + getApiUrl();

      if(getApiUrl() === 'kpis/sales' && lineHighChartWidget.kpi !== 'sales') {
        commonRequestParams.kpi = [ lineHighChartWidget.kpi ];
      }

      var requests = [
        getSelectedPeriodSummaryPromise(_apiUrl, commonRequestParams, lineHighChartWidget.isCaching),
        getRequestPromise(_apiUrl, commonRequestParams, lineHighChartWidget.isCaching, lineHighChartWidget.compare1Period),
        getRequestPromise(_apiUrl, commonRequestParams, lineHighChartWidget.isCaching, lineHighChartWidget.compare2Period),
      ];
      return requests;
    }

    function canGetWeatherData() {
      return lineHighChartWidget.showWeatherMetrics &&
        !ObjectUtils.isNullOrUndefined(lineHighChartWidget.selectedWeatherMetrics) &&
        !ObjectUtils.isNullOrUndefined(lineHighChartWidget.currentSite) &&
        !ObjectUtils.isNullOrUndefined(lineHighChartWidget.currentSite.geometry ) &&
        !ObjectUtils.isNullOrUndefined(lineHighChartWidget.currentSite.geometry.coordinates);
    }

    function getWeatherRequestParam(location, start, end) {
      return {
        location: location,
        reportStartDate:start.format('YYYY-MM-DDTHH'),
        reportEndDate: end.format('YYYY-MM-DDTHH')
      }
    }

    function getWeatherRequest (location, start, end, weatherUrl) {
      var weatherPrams = {params:getWeatherRequestParam(location, start, end)};
      var weatherRequest = requestManager.getRequest(weatherUrl,
      weatherPrams, lineHighChartWidget.isCaching, lineHighChartWidget.outStandingRequests);
      addOutstandingRequest(weatherRequest, weatherPrams, weatherUrl);
      return !ObjectUtils.isNullOrUndefined(weatherRequest.deferred) &&
        !ObjectUtils.isNullOrUndefined(weatherRequest.deferred.promise)? weatherRequest.deferred.promise:
        weatherRequest.promise;
    }

    function getWeatherRequests() {
      var requests = [];
      var weatherUrl = apiUrl + '/weather';
      var coord = angular.copy(lineHighChartWidget.currentSite.geometry.coordinates);
      var location = coord.reverse().join(',');
      var start = moment(lineHighChartWidget.dateRangeStart);
      var end =  moment(lineHighChartWidget.dateRangeEnd);

      if(!lineHighChartWidget.splitWeatherRequests) {
        requests.push(getWeatherRequest (location, start, end, weatherUrl));
        return requests;
      }
      var startDate = angular.copy(start);
      var endDate =  getEndDate(startDate, end);

      while(startDate <= end) {
        requests.push(getWeatherRequest (location, startDate, endDate, weatherUrl));
        startDate =  angular.copy(endDate).add(1, 'days');
        endDate =  getEndDate(startDate, end);
      }

     return requests;
    }

    function getEndDate(startDate, end) {
      var endDate = angular.copy(startDate).add(30, 'days');

      if(endDate > end) {
        endDate = angular.copy(end);
      }
      return endDate;
    }

    function getWeatherData() {
      lineHighChartWidget.isWeatherLoading = true;
      lineHighChartWidget.weatherRequestFailed = false;

      $q.all(getWeatherRequests()).then(transformWeatherData)
        .catch(function (err) {
          if(err !== 'line highchart user cancelling') {
            lineHighChartWidget.isWeatherLoading = false;
            lineHighChartWidget.weatherRequestFailed = true;
            console.error('weather request failed:', err);
            finalizeLoad(false);
          }
        });
    }

    function getCommonRequestParams() {
      var commonRequestParams = {
        orgId: lineHighChartWidget.orgId,
        groupBy: lineHighChartWidget.groupBy
      };

      if (angular.isDefined(lineHighChartWidget.zoneId)) {
        commonRequestParams.zoneId = lineHighChartWidget.zoneId;
      }

      if (angular.isDefined(lineHighChartWidget.siteId)) {
        commonRequestParams.siteId = lineHighChartWidget.siteId;
      }

      if (angular.isDefined(lineHighChartWidget.locationId) && !ObjectUtils.isNullOrUndefined(lineHighChartWidget.locationId)) {
        commonRequestParams.locationId = lineHighChartWidget.locationId;
      }

      if (angular.isDefined(lineHighChartWidget.operatingHours)) {
        commonRequestParams.operatingHours = lineHighChartWidget.operatingHours;
      }

      if(!lineHighChartWidget.getUniqueReturning){
        commonRequestParams.includeUnique = false;
        commonRequestParams.includeReturning = false;
      } else {
        commonRequestParams.includeUnique = true;
        commonRequestParams.includeReturning = true;
      }
      commonRequestParams.sales_category_id = selectedSalesCategoriesList();

      if(!ObjectUtils.isNullOrUndefined(commonRequestParams.sales_category_id) && commonRequestParams.sales_category_id.length > 1) {
        //set aggregate flag
        commonRequestParams.aggregate_sales_categories = true;
      }

      return commonRequestParams;
    }

    function selectedSalesCategoriesList() {
      return _.pluck(lineHighChartWidget.salesCategories, 'id');
    }

    function getYAxisData(graphColor) {
      var yAxisData = [];
      // y-axis
      yAxisData.push({
        labels: {
          formatter: function() {
            return Math.round(this.value);
          }
        },
        title: {
          text: ''
        },
        allowDecimals: false,
        gridLineDashStyle: 'Dot',
        gridLineColor: '#b0b0b0'
      });

      yAxisData.push({
        labels: {
          style: {
            color: graphColor[0]
          },
          formatter: function() {
            return Math.round(this.value);
          }
        },
        title: {
          text: '',
          style: {
            color: graphColor[0],
          }
        },
        allowDecimals: false,
        opposite: false,
        gridLineDashStyle: 'Dot',
        gridLineColor: '#b0b0b0'
      });

      //for percentage based kpis allow decimals to be used then x by 100 so it looks like % rather than decimal otherwise y-axis shos 0 - 1 an not values between.
      if(lineHighChartWidget.kpi === 'draw_rate' ||
        lineHighChartWidget.kpi === 'abandonment_rate'
        ){
        _.each(yAxisData, function(axisData){
          axisData.allowDecimals = true;
          axisData.labels.formatter = function(){
            return (this.value * 100);
          }
        })
      }

      return yAxisData;
    }

    function getOperatingHours(chartData) {
      return chartData.labels.slice(Number(lineHighChartWidget.businessDayStartHour));
    }

    function getDataWithOperatingHours(data) {
      return data.slice(Number(lineHighChartWidget.businessDayStartHour));
    }

    function buildHighchartConfig() {
      var chartData = lineHighChartWidget.chartData;
      if(lineHighChartWidget.noData || ObjectUtils.isNullOrUndefined(chartData)) {
        return;
      }
      var yAxisType = 'single';
      var graphColor = ['#9bc614', '#0aaaff', '#be64f0', '#ff5028', '#feac00', '#00abae', '#929090'];

      var seriesData = [];

        // series data
      _.each(chartData.series, function(data, seriesIndex) {
        if (lineHighChartWidget.operatingHours) {
          data = getDataWithOperatingHours(data);
        }
        seriesData.push(createSeriesData(seriesIndex, yAxisType, data, graphColor, chartData.labels));
      });

      // We have to set the xAxis labels since they change depending on user selected filters
      // this is for the formatter bug on the tooltips
      if (lineHighChartWidget.operatingHours) {
        var operatingHours = getOperatingHours(chartData);
        setxAxisChartLabels(operatingHours);
      } else {
        setxAxisChartLabels(chartData.labels);
      }

      return getChartConfig(graphColor, seriesData);
    }

    function createSeriesData(index, yAxisType, data, graphColor, labels) {
      //Array is indexed make sure each index exist otherwise
      for(var i = 0; i < data.length; i++) {
        if(!NumberUtils.isValidNumber(data[i])) {
          //highchart represent points if value is not valid. For those values should be null otherwise highchart drows a line without a point on the time label
          data[i] = null;
        }
      }
      return {
        name: getSeriesName(index),
        yAxis: getYAxisValue(yAxisType, index),
        data: data,
        labels: labels,
        color: graphColor[index],
        marker: {
          symbol: 'circle',
          radius: 3
        },
        states: {
          hover: {
            lineWidth: 2
          }
        }
      }
    }

    function getYAxisValue(yAxisType, index) {
      if (yAxisType === 'single') {
        if(lineHighChartWidget.showWeatherMetrics && index > 2) {
          return 1;
        }
        return 0;
      } else {
        return index;
      }
    }

    function getChartConfig(graphColor, seriesData) {
      var chartConfig = {
          options: {
            chart: {
              type: lineHighChartWidget.chartType, // 'line'|'column'
              height: 320,
              style: {
                fontFamily: '"Source Sans Pro", sans-serif'
              },
              events: {
                load: function(event) {
                  event.target.chartWidth -= 20;
                  event.target.redraw();
                }
              }
            },
            plotOptions: {
              series: {
                dataLabels: {
                  allowOverlap: true,
                  defer: false,
                  enabled: true,
                  padding: 0,
                  style: {
                    opacity: 0.001
                  }
                }
              }
            },
            tooltip: {
              shared: true,
              useHTML: true,
              backgroundColor: 'rgba(255, 255, 255, 0.8)',
              borderColor: '#e5e5e5',
              shadow: false,
              formatter: toolTipFormatter
            },
            exporting: {
              enabled: false
            },
            legend: {
              enabled: false
            },
          },
          title: {
            text: ''
          },
          xAxis: {
            crosshair: false,
            tickLength: 0,
            showLastLabel: true,
            endOnTick: true,
            labels: {
              align: lineHighChartWidget.chartType === 'line' ? 'left' : 'center',
              style: {
                color: '#929090'
              },
              formatter: function() {
                return lineHighChartWidget.xAxisChartLabels[this.value];
              }
            }
          },
          yAxis: getYAxisData(graphColor),
          series: seriesData
        };

        if($rootScope.pdf) {
          chartConfig.options.plotOptions.line = {
            animation: false,
            enableMouseTracking: false
          };
          chartConfig.options.plotOptions.series.dataLabels = false;
          chartConfig.options.tooltip.enabled = false;
          chartConfig.options.chart.events = null;
        }

        if(!$rootScope.pdf && lineHighChartWidget.kpi === 'traffic' && !lineHighChartWidget.isHourly) {
          addClickDrillDownEvents(chartConfig);
        }

        return chartConfig;
    }

  /**
   * Adds the click events to the chart config to enable drilling through to the hourly paage
   * Works directly on the supplied object
   * The first event that is applied is a hack to deal with some weirdness in high charts where some click events don't fire
   *
   * @param {object} chartConfig - The chart configuration to add the events to
   */
    function addClickDrillDownEvents(chartConfig) {
      if(ObjectUtils.isNullOrUndefined(chartConfig.options.chart.events)) {
        return;
      }

      chartConfig.options.chart.events.click = function(event) {
        if(!periodLabelsRepresentSingleDay) {
          return;
        }

        var nearestIndex = Math.round(event.xAxis[0].value) -1;

        var currentDate = dateLabels[0][nearestIndex];

        redirectToHourlyPage(currentDate);
      }

      chartConfig.options.plotOptions.series.point = {
        events: {
          click: function() {
            if(!periodLabelsRepresentSingleDay) {
              return;
            }

            var labelIndex = this.x;

            if(labelIndex > (dateLabels[0].length - 1)) {
              return;
            }

            var currentDate = dateLabels[0][labelIndex];

            redirectToHourlyPage(currentDate);
          }
        }
      }

      if(periodLabelsRepresentSingleDay) {
        chartConfig.options.plotOptions.series.cursor = 'pointer';
      }
    }

    function setPeriodLabelsRepresentSingleDay() {
      periodLabelsRepresentSingleDay = true;

      var currentPeriodDates = dateLabels[0];

      var previousDate = currentPeriodDates[0];

      _.each(currentPeriodDates, function(currentDate) {
        var daysBetween = currentDate.diff(previousDate, 'days');

        if(daysBetween > 1) {
          periodLabelsRepresentSingleDay = false;
        }

        previousDate = currentDate;
      });
    }

    function getWeatherName(index) {
      return $filter('translate')(lineHighChartWidget.selectedWeatherMetrics[index-3].shortTranslationLabel);
    }

    function getPeriod1Comparison() {
      switch (lineHighChartWidget.compareRange1Type) {
        case 'prior_period':
          return 'common.PRIORPERIOD';
        case 'prior_year':
          return 'common.PRIORYEAR';
        case 'custom':
          return 'common.CUSTOMCOMPARE1';
      }
    }

    function getPeriod2Comparison() {
      switch (lineHighChartWidget.compareRange2Type) {
        case 'prior_period':
          return 'common.PRIORPERIOD';
        case 'prior_year':
          return 'common.PRIORYEAR';
         case 'custom':
           return 'common.CUSTOMCOMPARE2';
       }
    }

    function getSeriesName(index) {
      switch(index){
        case 0:
          return $filter('translate')('common.SELECTEDPERIOD');
        case 1:
          return $filter('translate')(getPeriod1Comparison());
        case 2:
          return $filter('translate')(getPeriod2Comparison());
        default:
          if(lineHighChartWidget.showWeatherMetrics) {
            return getWeatherName(index);
          }
          return '';
      }
    }

    function getPointValue(index, point) {
      if(!lineHighChartWidget.getUniqueReturning){
        if(ObjectUtils.isNullOrUndefinedOrEmpty(point)) {
          return ' <label class="tooltip-value"> - </label>' ;
        }
        return index > 2 ?  getWeatherToolTipValue(point, index):  getToolTipValues(point[0].key, index);
      } else {
        if(ObjectUtils.isNullOrUndefinedOrEmpty(point)) {
          return ' <div class="col-xs-3 tooltip-value text-right"> - </div>' ;
        }
        return index > 2 ?  getWeatherToolTipValue(point, index):  getToolTipValues(point[0].key, index);
      }

    }

    function getPointColor(point) {
      if(ObjectUtils.isNullOrUndefinedOrEmpty(point)) {
        return '#929090';
      }
      return  point[0].color;
    }

    function setSelectedDate(point) {
      if (!lineHighChartWidget.isHourly && lineHighChartWidget.groupBy !== 'hour') {
        if(_.isUndefined(point[0])) {
          return;
        }
        var key = point[0].key;
        var selectedDate = lineHighChartWidget.chartData.labels[key];
        var momentDate = moment.utc(selectedDate, lineHighChartWidget.dateFormatMask);
        var formattedDate = moment.utc(momentDate).format('YYYY-MM-DD');
        if (formattedDate !== 'Invalid date') {
          lineHighChartWidget.selectedDate = formattedDate;
        }
      }
    }

    function getToolTipName(point, index, name) {
      setSelectedDate(point, index, name);
      if(index > 2) {
        return name;
      }

      if(ObjectUtils.isNullOrUndefinedOrEmpty(point)) {
        return moment().format(lineHighChartWidget.dateFormatMask);
      }

      return lineHighChartWidget.tooltipData[index][point[0].key].startDate.format(lineHighChartWidget.dateFormatMask);
    }

    function getToolTipTime(labels, index) {
      if (lineHighChartWidget.operatingHours) {
        labels = labels.slice(lineHighChartWidget.businessDayStartHour);
      }

      if (index.length > 0) {
        return labels[index[0].key] || '';
      }

      return '';
    }

    function populateToolTipBody(point, seriesItem, index) {

      var body = '';

      if(index <= 2 && ObjectUtils.isNullOrUndefined(lineHighChartWidget.tooltipData[index])) {
       return body;
      }

      var labelClass =  'label_' + index ;

      if(!lineHighChartWidget.getUniqueReturning){
        body += ' <div class="col-xs-12">';

        var toolTipName = getToolTipName(point, index, seriesItem.name);

        if(lineHighChartWidget.isHourly) {
          toolTipName += (' ' + getToolTipTime(seriesItem.labels, point));
        }

        body +=
        '<span style="color:' +
        getPointColor(point) + '">●</span>' + ' <label class="' + labelClass + ' tooltip-name" >'  +
        toolTipName + '</label>';

        body += getPointValue(index, point);

        body +=' </div>';
      } else {
        body = '<div class="row">' +
          '<div class="col-xs-3"> <span style="color:' +
            getPointColor(point) + '">●</span>' +
            ' <label class="' + labelClass + ' tooltip-name" style="font-size:12px;" >'  +
            getToolTipName(point, index, seriesItem.name) + ' </label> </div>' +
            getPointValue(index, point) +
        '</div>'
      }

      return body;
    }

    function getWeatherToolTipValue(point, index) {
      var valueLabel = ' <label class="tooltip-value">' ;


      var value = ObjectUtils.isNullOrUndefinedOrEmpty(point)? '-' :
        $filter('formatNumber')(point[0].y, 1) + lineHighChartWidget.selectedWeatherMetrics[index-3].suffixSymbol;

      valueLabel += value;

      valueLabel += '</label>';

      return  valueLabel;
    }

    function getToolTipValues(seriesItemIndex, index) {
      var row = '';

      var td =lineHighChartWidget.tooltipData[index];
      if(!lineHighChartWidget.getUniqueReturning ) {
        row += ' <label class="text-center tooltip-value">' + getValueString (td[seriesItemIndex]) + ' </label>';
      } else {
        row +=
        '<div class="col-xs-3 text-right"> <label class="tooltip-unique-value">' + getValueString(td[seriesItemIndex]) + ' </label></div>' +
        '<div class="col-xs-3 text-right"> <label class="tooltip-unique">' + getUniqueValueString(td[seriesItemIndex]) + ' </label></div>' +
        '<div class="col-xs-3 text-right"> <label  class="tooltip-return">' + getReturnValueString(td[seriesItemIndex])  + ' </label></div>';
      }

      return row ;
    }

  function toolTipFormatter() {
      var translatedTitle = $filter('translate')('kpis.kpiTitle.' + lineHighChartWidget.kpi);

      var title = '<div class="tooltip-header">' + translatedTitle + '</div>' + populateToolTipTotalHeader();

      var body = '';

      var points = this.points;

      _.each(lineHighChartWidget.chartConfig.series, function(seriesItem, index) {

        var point1 = _.filter(points, function(pointItem) {
          return pointItem.series.name === seriesItem.name;
        });

        if(lineHighChartWidget.showWeatherMetrics && index === 3) {
          body += populateToolTipWeatherHeader();
        }

        body += populateToolTipBody(point1, seriesItem, index);
      });

      return title + body;
    }

    function populateToolTipWeatherHeader() {
      var header = '  <div class= "col-xs-12 tooltip-header">' +
         $filter('translate')('common.WEATHER') + ' </div>';

      return header ;
    }

    function populateToolTipTotalHeader() {
      var header = ' <div class= "row">';

      if(!lineHighChartWidget.getUniqueReturning) {
        header += ' <div class= "tooltip-total-title col-xs-12">' +  $filter('translate')('common.TOTAL') + ' </div>';
      } else {
        header +=
          '<div class="col-xs-3"></div>' +
          ' <div class="tooltip-total-title-unique col-xs-3 text-right">' +  $filter('translate')('common.TOTAL') + ' </div>' +
          ' <div class="tooltip-unique-title col-xs-3 text-right">' + $filter('translate')('summaryWidget.UNIQUE') + ' </div>' +
          ' <div class="tooltip-returning-title col-xs-3 text-right">' + $filter('translate')('summaryWidget.RETURNING') + ' </div>';
      }

      return header + ' </div>';
    }

    function getPrefixForTooltip(metric) {
      if (metric.isCurrency) {
        return lineHighChartWidget.currencySymbol;
      }

      return metric.prefixSymbol;
    }

    function getMultipliedFormattedValue(value) {
      return  getPrefixForTooltip(lineHighChartWidget.selectedOption.metric) +
        $filter('formatNumber')($filter('multiplyNumber')(value,lineHighChartWidget.multiplier),
        lineHighChartWidget.selectedOption.metric.precision, lineHighChartWidget.numberFormatName) +
        lineHighChartWidget.valueLabel +
        lineHighChartWidget.selectedOption.metric.suffixSymbol;
    }

    function validData(data) {
      return !ObjectUtils.isNullOrUndefined(data) && !ObjectUtils.isNullOrUndefinedOrBlank(data.value);
    }

    function getValueString (data) {
      return validData(data) ? getMultipliedFormattedValue(data.value): '-';
    }

    function validUniqueData(data) {
      return !ObjectUtils.isNullOrUndefined(data) && !ObjectUtils.isNullOrUndefinedOrBlank(data.uniqueTraffic);
    }

    function getUniqueValueString (data) {
      return validUniqueData(data) ? getMultipliedFormattedValue(data.uniqueTraffic): '-';
    }

     function validReturnData(data) {
      return !ObjectUtils.isNullOrUndefined(data) && !ObjectUtils.isNullOrUndefinedOrBlank(data.returnTraffic);
    }

    function getReturnValueString (data) {
      return validReturnData(data) ? getMultipliedFormattedValue(data.returnTraffic): '-';
    }

    function setxAxisChartLabels(chartLabels) {
      lineHighChartWidget.xAxisChartLabels = chartLabels;
    }

    function transformChartData(responses) {
      lineHighChartWidget.chartConfig = null;
      lineHighChartWidget.isChartLoading = false;
      lineHighChartWidget.chartRequestFailed = false;
      var result = angular.copy(responses);

      //handle sort issues with chart date on x-axis
      _.map(result, function(responseObj) {
        responseObj.result = _.sortBy(responseObj.result, 'period_end_date');
      });

      var reportData;
      var compareData;
      var yearCompareData;

      if (lineHighChartWidget.isHourly || lineHighChartWidget.groupBy === 'hour') {

        reportData = result[0].result;
        compareData = result[1].result;
        yearCompareData = result[2].result;

        if(!ObjectUtils.isNullOrUndefinedOrEmpty(result[0].result)) {
          var hourRangeToDisplay = getHourRangeToDisplay(reportData);

          reportData = insertValuesForHourlyChart(reportData, hourRangeToDisplay);
          compareData = insertValuesForHourlyChart(compareData, hourRangeToDisplay);
          yearCompareData = insertValuesForHourlyChart(yearCompareData, hourRangeToDisplay);
        }

      } else {
        reportData = result[0].result;
        compareData = result[1].result;
        yearCompareData = result[2].result;
      }

      transformPeriodData('report', reportData);

      transformPeriodData('compare_period', compareData);

      transformPeriodData('compare_year', yearCompareData);

      if(lineHighChartWidget.isHourly) {
        transformDataForTable(reportData, compareData, yearCompareData);
      }

      loadSummary();
    }

    function getHour(dateString) {
      return Number(moment.utc(dateString).format('HH'));
    }

    function insertValuesForHourlyChart(data, hourRangeToDisplay) {
      var results = [];

      var currentHour = hourRangeToDisplay.start;

      while(currentHour <= hourRangeToDisplay.end) {
        var hourData = getDataForHour(data, currentHour);

        if(ObjectUtils.isNullOrUndefined(hourData)) {
          hourData = buildDefaultHourData(currentHour, data[0].period_start_date);
        }

        results.push(hourData);
        currentHour++;
      }

      return results;
    }

    function buildDefaultHourData(hour, firstDateRange) {
      var momentHour = moment.utc(firstDateRange).startOf('day').add('hour', hour);

      return {
        period_start_date: momentHour.format('YYYY-MM-DDTHH:mm:ss'),
        period_end_date: momentHour.format('YYYY-MM-DDTHH:mm:ss')
      };
    }

    function loadSummary() {
      /* If unique and returning values (traffic widget) are used, we need to make separate
        API requests for summary data to fetch unique/returning data.
      */

      var commonRequestParams = getCommonRequestParams();
      var averageRequestParams;

      if (lineHighChartWidget.separateSummaryRequests || lineHighChartWidget.getUniqueReturning) {
        averageRequestParams = angular.copy(commonRequestParams);
        averageRequestParams.groupBy = 'aggregate';

        if(getApiUrl() === 'kpis/sales' && lineHighChartWidget.kpi !== 'sales') {
          averageRequestParams.kpi = [ lineHighChartWidget.kpi ];
        }
        lineHighChartWidget.summaryLoading = true;

        fetchSummaryData(averageRequestParams)
          .then(function(responses) {
            transformSummaryData(responses);
          })
          .catch(function (err) {
            if(err !== 'line highchart user cancelling') {
              lineHighChartWidget.summaryLoading = false;
              finalizeLoad(true);
            }
        });
        return;
      } else {

        if(canGetWeatherData()) {
          getWeatherData();
        } else{
          lineHighChartWidget.summaryLoading = false;

          finalizeLoad(false);
        }
      }
    }

    function finalizeLoad(requestFailed) {
      if(lineHighChartWidget.isWeatherLoading || lineHighChartWidget.isChartLoading || lineHighChartWidget.summaryLoading) {
        return;
      }

      lineHighChartWidget.outStandingRequests = [];

      lineHighChartWidget.requestFailed = requestFailed;

      lineHighChartWidget.isLoading = false;

      setNoData();

      $timeout(function() {
        var chartConfig = buildHighchartConfig();
        lineHighChartWidget.dataReady = (!lineHighChartWidget.noData && !ObjectUtils.isNullOrUndefined(chartConfig));

        if(lineHighChartWidget.dataReady) {
          lineHighChartWidget.chartConfig = chartConfig;
        }
      });
    }

    function noWeatherData(responses) {
      return ObjectUtils.isNullOrUndefined(lineHighChartWidget.chartData) ||
        ObjectUtils.isNullOrUndefinedOrEmpty(lineHighChartWidget.chartData.labels) ||
        ObjectUtils.isNullOrUndefinedOrEmpty(responses);
    }

    function noTranformWeatherData(responses) {
      return !canGetWeatherData() ||
        noWeatherData(responses);
    }

    function transformWeatherData(responses) {
      lineHighChartWidget.isWeatherLoading = false;
      lineHighChartWidget.weatherRequestFailed = false;
      //check if there is weather data
      if(noTranformWeatherData(responses)) {
        finalizeLoad(false);

        return;
      }

      var chartStartDate = moment.utc(lineHighChartWidget.dateRangeStart);
      var data = _.map(responses, function(data){return data.result});
      lineHighChartWidget.weatherData = lineHighChartWidget.splitWeatherRequests || _.isArray(data) ?  _.flatten(data): data[0];

      populateWeatherData(chartStartDate, lineHighChartWidget.weatherData);

      finalizeLoad(false);
    }

    function populateWeatherData(chartStartDate, weatherData) {
       _.each(lineHighChartWidget.chartData.labels, function(label, index) {
         setWeatherData(chartStartDate, label, weatherData, index-1);
       });
    }

    function filterWeatherDataWithDate(dataList, date1, date2) {
      var maxDate = ObjectUtils.isNullOrUndefinedOrBlank(date1)?
        date1:
        moment.utc(date1, lineHighChartWidget.dateFormatMask).format('YYYY-MM-DD');

      var minDate = ObjectUtils.isNullOrUndefinedOrBlank(date2)?
        date2:
        moment.utc(date2, lineHighChartWidget.dateFormatMask).format('YYYY-MM-DD');
      return _.filter(dataList, function(data) {
        return (!ObjectUtils.isNullOrUndefined(data) && data.date <= maxDate)  &&
          (ObjectUtils.isNullOrUndefinedOrBlank(minDate) ||
          data.date >= minDate);
      });
    }

    function getDateFromIndex(index) {
      var date2;
      if(index > 0) {
        return lineHighChartWidget.chartData.labels[index]
      }
      return date2;
    }

    function getAgregatedMetricValue(filteredData, metric) {


      if(!_.isArray(filteredData)) {
        return getWeatherMetricValue(metric, filteredData);
      }

      var metricVal = null;
      var count = 0;

      _.each(filteredData, function(data) {
        var val = getWeatherMetricValue(metric, data);

        if(!ObjectUtils.isNullOrUndefined(val)) {
          if(ObjectUtils.isNullOrUndefined(metricVal)) {
              metricVal = 0;
          }

          count += 1;
          metricVal += val;
        }
      });

      if(count > 0) {
        metricVal = metricVal / count;
      }

      return metricVal;
    }

    function setWeatherData(chartStartDate, date, weatherDataList, index1) {
      var index = 3;

      var filteredData =_.isArray(weatherDataList)?  filterWeatherDataWithDate(weatherDataList, date, getDateFromIndex(index1)):weatherDataList;
      var seriesItemIndex = convertDateToChartSeriesIndex(
        moment.utc(date, lineHighChartWidget.dateFormatMask),
        chartStartDate,
        lineHighChartWidget.groupBy,
        index1 + 1
      );
      if($rootScope.customDashboards){
        updateMetricUnits(lineHighChartWidget.selectedWeatherMetrics);
      }
      _.each(lineHighChartWidget.selectedWeatherMetrics, function(metric, i) {
        var metricVal = getAgregatedMetricValue(filteredData, metric);
        if(ObjectUtils.isNullOrUndefined(lineHighChartWidget.chartData.series[index + i])) {
          var seriesData = [];
          seriesData[0] = metricVal;


          lineHighChartWidget.chartData.series.push(seriesData);
        }
        else {
          lineHighChartWidget.chartData.series[index + i][seriesItemIndex] = metricVal;
        }
      });
    }

    function getWeatherAvg(metric, data) {
      var val;
      var count = 0 ;
      _.each(metric.avgKeys , function(key) {
        if(!ObjectUtils.isNullOrUndefined(data[key])) {
          if(ObjectUtils.isNullOrUndefined(val)) {
            val = 0;
          }
          val += Number(data[key][metric.unit]);
          count += 1;
        }
      });
      if(count > 0 ) {
        return val/count;
      }
      return val;
    }

    function getTotal(data, metric, total) {
       if(ObjectUtils.isNullOrUndefined(data[metric.apiReturnkey])) {
         return total;
       }
       if(ObjectUtils.isNullOrUndefined(total)) {
         total = 0;
       }
       return total + getMetricNumberValue(data, metric);
    }

    function getMetricNumberValue(data, metric) {
      return ObjectUtils.isNullOrUndefinedOrBlank(metric.unit) ||
        ObjectUtils.isNullOrUndefinedOrBlank(data[metric.apiReturnkey][metric.unit]) ?
        Number(data[metric.apiReturnkey]):
        Number(data[metric.apiReturnkey][metric.unit])
    }

    function getWeatherMetricAverageFromHourlyData(metric, data) {
      if(ObjectUtils.isNullOrUndefined(data) ||
        ObjectUtils.isNullOrUndefinedOrEmpty(data.hourly)) {
        return null;
      }

      var total = null;

      _.each(data.hourly, function(hourlyData) {
        total = getTotal(hourlyData, metric, total);
      });

      return total / data.hourly.length;
    }

    function getWeatherMetricValue(metric, data) {

      if(!ObjectUtils.isNullOrUndefinedOrEmpty(metric.avgKeys)) {
        return getWeatherAvg(metric, data);
      }
      if(metric.isHourly){
        return getWeatherMetricAverageFromHourlyData(metric, data);
      }

      return ObjectUtils.isNullOrUndefined(data[metric.apiReturnkey])?
        data[metric.apiReturnkey]:
        getMetricNumberValue(data, metric);
    }

    function setNoData() {
      lineHighChartWidget.noData = !hasData(0);
      if(lineHighChartWidget.noData) {
        lineHighChartWidget.isLoading = false;
      }
    }

    function transformPeriodData(type, data) {
      var index, i, entry, chartStartDate, chartEndDate;
      if(ObjectUtils.isNullOrUndefinedOrEmpty(data)) {
        return;
      }

      if (type === 'report') {
        index = 0;
        chartStartDate = moment(lineHighChartWidget.dateRangeStart);
        chartEndDate = moment(lineHighChartWidget.dateRangeEnd);
        lineHighChartWidget.chartData.labels = [];
      } else if (type === 'compare_period') {
        index = 1;
        chartStartDate = moment(lineHighChartWidget.compare1Period.start);
        chartEndDate = moment(lineHighChartWidget.compare1Period.end);
      } else if (type === 'compare_year') {
        index = 2;
        chartStartDate = moment(lineHighChartWidget.compare2Period.start);
        chartEndDate = moment(lineHighChartWidget.compare2Period.end);
      } else {
        return;
      }

      lineHighChartWidget.dateRanges[index] = {
        start: formatShortDate(chartStartDate),
        end: formatShortDate(chartEndDate)
      };

      if (ObjectUtils.isNullOrUndefined(lineHighChartWidget.tooltipData[index])) {
        lineHighChartWidget.tooltipData[index] = {};
      }

      // Current, Prior1, Prior2
      dateLabels[index] = [];

      for (i = 0; i < data.length; i++) {
        /* Populate the data series. */
        entry = data[i];

        var date = moment.utc(entry.period_start_date);

        dateLabels[index].push(date);

        /* Create chart labels. */
        if (index === 0) {
          var label;
          if (lineHighChartWidget.isHourly || lineHighChartWidget.groupBy === 'hour') {
            label = getTimeFromDate(date);
          } else {
            label = formatShortDate(date);
          }
          lineHighChartWidget.chartData.labels[i] = label;
        }

        updateSeriesAndTooltip(entry, index, i);
      }

      if(!lineHighChartWidget.isHourly) {
        setPeriodLabelsRepresentSingleDay(dateLabels);
      }

      if(lineHighChartWidget.kpi === 'visitor_behaviour_traffic'){
        lineHighChartWidget.chartData.labels = _.sortBy(lineHighChartWidget.chartData.labels, function(label){
          return label;
        });
      }

      // If we don't do separate summary requests, calculate totals here
      if (!lineHighChartWidget.separateSummaryRequests && !lineHighChartWidget.getUniqueReturning) {
        lineHighChartWidget.totalData[index] = data;
        calculateTotalAggregate(index, data);
      }
    }

    function updateSeriesAndTooltip(entry, index, seriesItemIndex) {
      if (!ObjectUtils.isNullOrUndefined(lineHighChartWidget.selectedOption)) {
        lineHighChartWidget.chartData.series[index][seriesItemIndex] = entry[lineHighChartWidget.selectedOption.propertyName];
        if (ObjectUtils.isNullOrUndefined(lineHighChartWidget.tooltipData[index][seriesItemIndex])) {
          lineHighChartWidget.tooltipData[index][seriesItemIndex] = {};
        }
        if (lineHighChartWidget.getUniqueReturning) {
          lineHighChartWidget.tooltipData[index][seriesItemIndex].returnTraffic = entry.return_traffic;
          lineHighChartWidget.tooltipData[index][seriesItemIndex].uniqueTraffic = entry.unique_traffic;
        }
        lineHighChartWidget.tooltipData[index][seriesItemIndex].value = entry[lineHighChartWidget.selectedOption.propertyName];
        lineHighChartWidget.tooltipData[index][seriesItemIndex].startDate = moment.utc(entry.period_start_date);
        lineHighChartWidget.tooltipData[index][seriesItemIndex].endDate = moment.utc(entry.period_end_date);
      }
    }

    function convertDateToChartSeriesIndex(date, startDate, groupBy, realIndex) {
      switch (groupBy) {
        case 'day':
          return Math.floor((date.unix() - startDate.unix()) / (24 * 3600));
        case 'week':
          return Math.floor((date.unix() - startDate.unix()) / (7 * 24 * 3600));
        case 'month':
          return realIndex;
      }
    }

    function loadTooltipData(seriesItemIndex) {
      if (lineHighChartWidget.getUniqueReturning === true && tooltipDataInitialized(seriesItemIndex) && !tooltipDataIsLoaded(seriesItemIndex)) {
        var params = [];

        _.each(lineHighChartWidget.tooltipData, function(dateRange) {
          params.push({
            reportStartDate: dateRange[seriesItemIndex].startDate,
            reportEndDate: dateRange[seriesItemIndex].endDate
          });

          dateRange[seriesItemIndex].details = {};
        });

        lineHighChartWidget.requestQueue.push({
          seriesItemIndex: seriesItemIndex,
          params: params
        });
      }
    }

    function tooltipDataInitialized(seriesItemIndex) {
      var initialized = _.every(lineHighChartWidget.tooltipData, function(dateRange) {
        return !ObjectUtils.isNullOrUndefined(dateRange) && !ObjectUtils.isNullOrUndefined(dateRange[seriesItemIndex]);
      });

      return initialized;
    }

    function tooltipDataIsLoaded(seriesItemIndex) {
      var loaded = _.every(lineHighChartWidget.tooltipData, function(dateRange) {
        return !ObjectUtils.isNullOrUndefined(dateRange[seriesItemIndex].details);
      });

      return loaded;
    }

    function loadDetails() {
      if (Object.keys(lineHighChartWidget.requestQueue).length > 0 && lineHighChartWidget.requestsLoading < lineHighChartWidget.maxConcurrentTooltips) {
        var item = lineHighChartWidget.requestQueue.shift();
        lineHighChartWidget.requestsLoading++;

        fetchDetailData(item)
          .then(transformDetailData)
          .then(function () {
            lineHighChartWidget.requestsLoading--;
            loadDetails();
          })
          .catch(function () {
            lineHighChartWidget.requestsLoading--;
            loadDetails();
          });
      }
    }

    function fetchDetailData(item) {
      var commonRequestParams = getCommonRequestParams();
      commonRequestParams.includeUnique = true;
      commonRequestParams.includeReturning = true;
      commonRequestParams.groupBy = 'aggregate';
      var _apiUrl = apiUrl + '/' + getApiUrl();
      var promises = [];

      _.each(item.params, function(dateRange) {
        promises.push(requestManager.get(_apiUrl, {
          params: angular.extend({}, dateRange, commonRequestParams)
        }));
      });

      return $q.all(promises);
    }

    function transformDetailData(responses) {
      var result = angular.copy(responses);
      var seriesItemIndex;
      var chartStartDate = {
        0: moment.utc(lineHighChartWidget.dateRangeStart),
        1: moment.utc(lineHighChartWidget.compare1Period.start),
        2: moment.utc(lineHighChartWidget.compare2Period.start)
      };
      _.each(result, function (response, index) {
        seriesItemIndex = convertDateToChartSeriesIndex(
          moment.utc(response.result[0].period_start_date),
          chartStartDate[index],
          lineHighChartWidget.groupBy
        );
        lineHighChartWidget.tooltipData[index][seriesItemIndex].details = {
          return: response.result[0].return_traffic,
          unique: response.result[0].unique_traffic
        };
      });
    }

    function addOutstandingRequest(request, params, url) {
      if(ObjectUtils.isNullOrUndefined(lineHighChartWidget.outStandingRequests)) {
        lineHighChartWidget.outStandingRequests = [];
      }

      var outstandingRequest = requestManager.findOutstandingRequest(url,
        {params: params}, lineHighChartWidget.outStandingRequests);

      if(ObjectUtils.isNullOrUndefined(outstandingRequest)) {
        lineHighChartWidget.outStandingRequests.push(request);
      }
    }

    function getSelectedPeriodSummaryPromise(url, requestParams, cache) {
      var dateRange = {
        start: lineHighChartWidget.dateRangeStart,
        end: lineHighChartWidget.dateRangeEnd
      };

      return getRequestPromise(url, requestParams, cache, dateRange);
    }

    function getRequestPromise(_apiUrl, requestParams, cache, dateRange) {
      var params =  {
        params: angular.extend({}, {
          reportStartDate: moment.utc(dateRange.start).toISOString(),
          reportEndDate: moment.utc(dateRange.end).toISOString(),
        }, requestParams)
      };
      var request = requestManager.getRequest(_apiUrl, params, cache, lineHighChartWidget.outStandingRequests);
      addOutstandingRequest(request, params, _apiUrl);
      return !ObjectUtils.isNullOrUndefined(request.deferred) &&
        !ObjectUtils.isNullOrUndefined(request.deferred.promise)? request.deferred.promise:
        request.promise;
    }

    function fetchSummaryData(averageRequestParams) {
      var _apiUrl = apiUrl + '/' + getApiUrl();

      return $q.all([
        getSelectedPeriodSummaryPromise(_apiUrl, averageRequestParams, lineHighChartWidget.isCaching),
        getRequestPromise(_apiUrl, averageRequestParams, lineHighChartWidget.isCaching, lineHighChartWidget.compare1Period),
        getRequestPromise(_apiUrl, averageRequestParams, lineHighChartWidget.isCaching, lineHighChartWidget.compare2Period)
     ]);
    }

    function transformSummaryData(responses) {
      lineHighChartWidget.totalData = [];
      _.each(responses, function(response, index) {
        lineHighChartWidget.totalData.push(response.result);
        if(lineHighChartWidget.summaryAverages) {
          calculateAverageData(index, response.result);
        } else {
          calculateTotalAggregate(index, response.result);
        }
      });

      lineHighChartWidget.summaryLoading = false;
      if(canGetWeatherData()) {
        getWeatherData();
      } else {
        finalizeLoad(false);
      }
    }

    function calculateAverageData(index, data) {
      var currentValue, compareValue;
      if (index > 0 && !ObjectUtils.isNullOrUndefined(data[0])) {
        compareValue = data[0][lineHighChartWidget.selectedOption.propertyName];
        currentValue = lineHighChartWidget.summaryData[0];

        lineHighChartWidget.delta[index] = comparisonsHelper.getComparisonData(currentValue, compareValue, true);
        lineHighChartWidget.summaryData[index] = compareValue;
      } else if (!ObjectUtils.isNullOrUndefined(data[0])) {
        compareValue = data[0][lineHighChartWidget.selectedOption.propertyName];
        lineHighChartWidget.summaryData[0] = compareValue;
      }
    }

    function calculateTotalAggregate(index, data) {
      var currentValue, compareValue;

      if (!ObjectUtils.isNullOrUndefined(data[0])) {
        if (data.length > 0) {
          lineHighChartWidget.returnData[index] = 0;
          lineHighChartWidget.uniqueData[index] = 0;
          compareValue = null;

          _.each(data, function (item) {
            if( !ObjectUtils.isNullOrUndefined(lineHighChartWidget.selectedOption) && !ObjectUtils.isNullOrUndefined(item[lineHighChartWidget.selectedOption.propertyName] ) ) {
              if( compareValue === null) {
                //init
                compareValue = 0;
              }

              compareValue += item[lineHighChartWidget.selectedOption.propertyName] * lineHighChartWidget.multiplier;
            }

            if (lineHighChartWidget.getUniqueReturning) {
              lineHighChartWidget.uniqueData[index] += item.unique_traffic * lineHighChartWidget.multiplier;
              lineHighChartWidget.returnData[index] += item.return_traffic * lineHighChartWidget.multiplier;
            }
          });
        } else {
          compareValue = data[0][lineHighChartWidget.selectedOption.propertyName] * lineHighChartWidget.multiplier;
          if (lineHighChartWidget.getUniqueReturning) {
            lineHighChartWidget.uniqueData[index] = data[0].unique_traffic;
            lineHighChartWidget.returnData[index] = data[0].return_traffic;
          }
        }

        lineHighChartWidget.summaryData[index] = compareValue;
      }

      if (index > 0 && !ObjectUtils.isNullOrUndefined(data[0])) {
        currentValue = 0;
        if (!ObjectUtils.isNullOrUndefinedOrEmpty(lineHighChartWidget.totalData[0])) {
          _.each(lineHighChartWidget.totalData[0], function (item) {
            currentValue += item[lineHighChartWidget.selectedOption.propertyName] * lineHighChartWidget.multiplier;
          });
          lineHighChartWidget.delta[index] = comparisonsHelper.getComparisonData(currentValue, compareValue, true);
        }
      }
    }

    function formatDate(dateString) {
      return moment.utc(dateString).format(lineHighChartWidget.dateFormatMask);
    }

    function formatShortDate(dateString) {
      var momentDate;

      if(moment.isMoment(dateString)) {
        momentDate = dateString;
      } else {
        momentDate = moment.utc(dateString);
      }

      return momentDate.format(lineHighChartWidget.dateFormatMask);
    }

    function getTimeFromDate(momentDate) {
      return momentDate.format('LT');
    }

    function getGroupKey() {
      if(!ObjectUtils.isNullOrUndefined($state.current.views) &&
        !ObjectUtils.isNullOrUndefined($state.current.views.analyticsMain) &&
        !ObjectUtils.isNullOrUndefined($state.current.views.analyticsMain.controller)){
        return $state.current.views.analyticsMain.controller.toString() + '-' + lineHighChartWidget.kpi.toString();
      }
      return '';
    }

    function setRootScopeGroupBy(groupBy) {
      $rootScope.groupBy[getGroupKey()] = groupBy;
    }

    function getRootScopeGroupBy() {
      return $rootScope.groupBy[getGroupKey()];
    }

    function setGroupBy(groupBy) {
      googleAnalytics.trackUserEvent('group by', groupBy);
      lineHighChartWidget.groupBy = groupBy;
      setRootScopeGroupBy(groupBy);
    }

    function hasData(index) {
      return !ObjectUtils.isNullOrUndefined(lineHighChartWidget.chartData) &&
        !ObjectUtils.isNullOrUndefinedOrEmpty(lineHighChartWidget.chartData.series) &&
        !ObjectUtils.isNullOrUndefinedOrEmpty(lineHighChartWidget.chartData.series[index] ) &&
        !ObjectUtils.isNullOrUndefinedOrEmpty(lineHighChartWidget.summaryData ) &&
        !ObjectUtils.isNullOrUndefined( lineHighChartWidget.summaryData[index] ) ;
    }

    function compareRangeIsPriorPeriod(comparePeriodType) {
      if (comparePeriodType === 'prior_period') {
        return true;
      } else {
        return false;
      }
    }

    function compareRangeIsPriorYear(comparePeriodType) {
      if (comparePeriodType === 'prior_year') {
        return true;
      } else {
        return false;
      }
    }

    function calculateDelta(current, compare) {
      return ((compare - current) / compare) * 100 * -1;
    }

    function loadTranslations() {
      $translate.use(lineHighChartWidget.language);
    }

    function setLegendLabel(comparisonIndex, comparisonType) {
      var compareRangeType = 'compareRange' + comparisonIndex + 'Type';
      switch (comparisonType) {
        case 'priorPeriod':
          return hasData(comparisonIndex) && compareRangeIsPriorPeriod(lineHighChartWidget[compareRangeType]);
        case 'priorYear':
          return hasData(comparisonIndex) && compareRangeIsPriorYear(lineHighChartWidget[compareRangeType]);
        case 'custom':
          return hasData(comparisonIndex) && !compareRangeIsPriorPeriod(lineHighChartWidget[compareRangeType]) && !compareRangeIsPriorYear(lineHighChartWidget[compareRangeType]);
        default:
          return hasData(comparisonIndex) && compareRangeIsPriorPeriod(lineHighChartWidget[compareRangeType]);
      }
    }

    function toggleTableVisibility(){
      lineHighChartWidget.showTable.selection = !lineHighChartWidget.showTable.selection;
    }

    function transformDataForTable(currentPeriodData, comparePeriod1Data, comparePeriod2Data) {
      var tableData = [];
      var totalRow = {
        currentPeriod: 0,
        compare1: 0,
        compare2: 0
      };

      _.each(lineHighChartWidget.chartData.labels, function(label, index) {
        var row = {
          hourSort: index,
          hour: label,
          currentPeriod: getPeriod(currentPeriodData, label),
          compare1: getPeriod(comparePeriod1Data, label),
          compare2: getPeriod(comparePeriod2Data, label)
        };

        if(_.isNumber(row.currentPeriod)) {
          totalRow.currentPeriod += row.currentPeriod;
        }

        if(_.isNumber(row.compare1)) {
          totalRow.compare1 += row.compare1;
        }

        if(_.isNumber(row.compare2)) {
          totalRow.compare2 += row.compare2;
        }

        tableData.push(row);
      });

      lineHighChartWidget.tableData = tableData;
      lineHighChartWidget.totalRow = totalRow;

      if(!ObjectUtils.isNullOrUndefined(lineHighChartWidget.sortInfo) && !ObjectUtils.isNullOrUndefinedOrBlank(lineHighChartWidget.sortInfo.currentSort)) {
        setInitialSort(lineHighChartWidget.sortInfo.currentSort, lineHighChartWidget.sortInfo.currentSortDirection);
      }
    }

    // ToDo: Remove this and use the getDataForHour instead
    function getPeriod(data, hour) {
      var dataForHour = _.find(data, function(row) {
        return moment.utc(row.period_start_date).format('h:mm A') === hour;
      })

      if(ObjectUtils.isNullOrUndefined(dataForHour)) {
        return 0;
      }

      return dataForHour.total_traffic;
    }

  /**
   * Gets the entry for the specified hour.
   *
   * @param {object} data - A response array from the API
   * @param {number} hour - The hour of data to retrieve. Can be between 0 and 23
   * @returns {object} The data for the hour requested. Returns undefined if the hour cannot be found
   */
    function getDataForHour(data, hour) {
      var dataForHour = _.filter(data, function(row) {
        return Number(moment.utc(row.period_start_date).format('HH')) === hour;
      });

      if(ObjectUtils.isNullOrUndefined(dataForHour[0])) {
        return;
      }

      var format = 'YYYY-MM-DDTHH:mm:ss';
      var momentDate = moment.utc(dataForHour[0].period_start_date);

      var hourObject = {
        period_start_date: momentDate.format(format),
        period_end_date: momentDate.format(format),
        total_traffic: 0
      };

      _.each(dataForHour, function(result) {
        hourObject.total_traffic += result.total_traffic;
      });

      return hourObject;
    }

  /**
   * Sorts the hourly table
   *
   * @param {string} sortBy - The property name to sort by
   */
    function sortTable(sortBy) {
      lineHighChartWidget.tableData = _.sortBy(lineHighChartWidget.tableData, sortBy);

      if(lineHighChartWidget.currentSort === sortBy && lineHighChartWidget.currentSortDirection === 'asc') {
        lineHighChartWidget.tableData.reverse();
        lineHighChartWidget.currentSortDirection = 'desc';
      } else {
        lineHighChartWidget.currentSortDirection = 'asc';
      }
      lineHighChartWidget.currentSort = sortBy;

      if(ObjectUtils.isNullOrUndefined(lineHighChartWidget.sortInfo)) {
        lineHighChartWidget.sortInfo = {}
      }

      lineHighChartWidget.sortInfo.currentSort = lineHighChartWidget.currentSort;
      lineHighChartWidget.sortInfo.currentSortDirection = lineHighChartWidget.currentSortDirection;
    }

  /**
   * Sorts the hourly table during the initialisation sequence for the PDF
   *
   * @param {string} sortBy - The property name to sort by
   * @param {string} sortBy - The sortDirection
   */
    function setInitialSort(sortBy, sortDirection) {
      lineHighChartWidget.tableData = _.sortBy(lineHighChartWidget.tableData, sortBy);

      if(sortDirection === 'desc') {
        lineHighChartWidget.tableData.reverse();
      }

      lineHighChartWidget.currentSort = sortBy;
      lineHighChartWidget.currentSortDirection = sortDirection;

    }

  /**
   * Works out what the hour range shown to the users should be
   * Loops through all days in the current range to find the earliest hour and latest hour
   *
   * @param {object} currentPeriodData - A response array from the API
   * @returns {object} A object containing the min hour and max hour to display
   */
    function getHourRangeToDisplay(currentPeriodData) {
      var hourRangeToDisplay = { };

      var day = moment(lineHighChartWidget.dateRangeStart);
      var endDay = moment(lineHighChartWidget.dateRangeEnd);
      var dateFormatStringForComparison = 'DD-MM-YYYY';

      while(utils.getDaysBetweenDates(day, endDay) >= 0) {
        var dayString = day.format(dateFormatStringForComparison);

        var dataForDay = _.filter(currentPeriodData, function(hourData) {
          var date = moment.utc(hourData.period_start_date);

          return date.format(dateFormatStringForComparison) === dayString;
        });

        if(ObjectUtils.isNullOrUndefinedOrEmpty(dataForDay)) {
          break;
        }

        var startHourForDay = getHour(dataForDay[0].period_start_date);
        var endHourForDay = getHour(dataForDay[dataForDay.length - 1].period_start_date);

        if(_.isUndefined(hourRangeToDisplay.start) || hourRangeToDisplay.start > startHourForDay) {
          hourRangeToDisplay.start = startHourForDay;
        }

        if(_.isUndefined(hourRangeToDisplay.end) || hourRangeToDisplay.end < endHourForDay) {
          hourRangeToDisplay.end = endHourForDay;
        }

        if(dataForDay.length === 24) {
            // We just covered a whole day, so no point continuing
          break;
        }

        day.add(1, 'days');
      }

      return hourRangeToDisplay;
    }

  /**
   * Redirects the user to the hourly page for the specified date.
   * Works out comparison dates based on the day view before redirecting.
   * This prevents unnecessary redirects and keeps the back button helpful
   *
   * @param {object} currentPeriodData - A response array from the API
   */
    function redirectToHourlyPage(currentDate) {
      var params = {
        dateRangeStart: currentDate,
        dateRangeEnd: currentDate
      };

      LocalizationService.getAllCalendars().then(function(calendars) {
        calendars = calendars.result;

        var comparisonRanges = getDefaultComparisonDateRangeParams(params, lineHighChartWidget.currentUser, lineHighChartWidget.currentOrganization, calendars);

        angular.extend(params, comparisonRanges);

        $state.go('analytics.organization.site.hourly', params);
      });
    }

  /**
   * Sets the currency symbol
   * Requires the currentOrganization and the currentSite to be set. If not, the promise will be rejected
   * Only sets the currency symbol if it hasn't been passed into the directive
   * @returns {object} A promise
   */
    function setCurrencySymbol() {
      var deferred = $q.defer();
      try{
        if (!ObjectUtils.isNullOrUndefinedOrBlank(lineHighChartWidget.currentOrganization.organization_id) &&
            !ObjectUtils.isNullOrUndefined(lineHighChartWidget.currentSite.site_id) &&
            ObjectUtils.isNullOrUndefinedOrBlank(lineHighChartWidget.currencySymbol)) {
              currencyService.getCurrencySymbol(lineHighChartWidget.currentOrganization.organization_id, lineHighChartWidget.currentSite.site_id).then(function (data) {
              lineHighChartWidget.currencySymbol = data.currencySymbol;
              deferred.resolve();
          });
        }
      } catch(error) {
        deferred.reject(error);
      }
      return deferred.promise;
    }
  }
})();
