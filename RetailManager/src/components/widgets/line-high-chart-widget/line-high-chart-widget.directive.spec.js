'use strict';

describe('lineHighChartWidgetDirective', function () {
  var $compile;
  var $httpBackend;
  var $q;

  var $scope, $rootScope;
  var apiUrl = 'https://api.url';
  var lineHighChartWidget;
  var currentOrganizationMock;
  var mockSiteResource = {
    get: function(params) {
      angular.noop(params);
      var deferred = $q.defer();
      deferred.resolve({name: 'site 1'});

      return {
        $promise: deferred.promise
      };
    }
  };

  var mockState = {
    current: {
      views: {
        analyticsMain: {
          controller: 'someController'
        }
      }
    }
  };

  beforeEach(module('shopperTrak', function($translateProvider) {
    $translateProvider.translations('en_US', {});
  }));

  beforeEach(module('shopperTrak.widgets'));
  beforeEach(module('shopperTrak.widgets.mock'));

  beforeEach(module(function ($provide) {
    $provide.constant('apiUrl', apiUrl);
    $provide.value('SiteResource', mockSiteResource);
    $provide.value('$state', mockState);
  }));

  beforeEach(inject(putTemplateToTemplateCache));

  beforeEach(inject(function (_$rootScope_,
    _$compile_,
    _$httpBackend_,
    _$q_) {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $httpBackend = _$httpBackend_;
    $scope = $rootScope.$new();
    $q = _$q_;
    currentOrganizationMock = {
      organization_id: 1,
      portal_settings: { currency: '$' }
    };

    $httpBackend.whenGET('https://api.url/organizations').respond(currentOrganizationMock);
    //$httpBackend.whenGET('https://api.url/organizations').respond(currentOrganizationMock);
    $scope.currentSortDirection = undefined;
    $scope.kpi = 'traffic';
  }));

  describe('activate', function () {
    it('should not set the currentSortDirection if it has already been set on the directive', function() {
      $scope.currentSortDirection = 'desc';
      lineHighChartWidget = renderDirectiveAndDigest();
      expect(lineHighChartWidget.currentSortDirection).toBe('desc');
    });

    it('should set the currentSortDirection to a default if it has not been set', function() {
      lineHighChartWidget = renderDirectiveAndDigest();
      expect(lineHighChartWidget.currentSortDirection).toBe('asc');
    });

    it('should default the widget status to loading', function() {
      lineHighChartWidget = renderDirectiveAndDigest();
      expect(lineHighChartWidget.isLoading).toBe(true);
    });

    it('should set the tabWidget if not on the PDF export', function() {
      $rootScope.pdf = false;
      lineHighChartWidget = renderDirectiveAndDigest();
      expect(lineHighChartWidget.tabWidget).toBeDefined();
    });

    it('should set the tabWidget if not on the PDF export', function() {
      $rootScope.pdf = true;
      lineHighChartWidget = renderDirectiveAndDigest();
      expect(lineHighChartWidget.tabWidget).toBeUndefined();
    });

    it('should not change the valueLabel if it has been set', function() {
      $scope.valueLabel = 'value label';
      lineHighChartWidget = renderDirectiveAndDigest();
      expect(lineHighChartWidget.valueLabel).toBe('value label');
    });

    it('should set the valueLabel to an empty string if it has not been set', function() {
      var element = angular.element(
        '<line-high-chart-widget ' +
        'is-hourly="isHourly" ' +
        'show-table="showTable" ' +
        'org-id="1234" ' +
        'site-id="56789" ' +
        'date-range-start="dateRangeStart" ' +
        'date-range-end="dateRangeEnd" ' +
        'compare-range-1-start="compareRange1Start" ' +
        'compare-range-1-end="compareRange1End" ' +
        'compare-range-2-start="compareRange2Start" ' +
        'compare-range-2-end="compareRange2End" ' +
        'first-day-of-week-setting="1" ' +
        'get-unique-returning="false" ' +
        'separate-summary-requests="true" ' +
        'summary-averages="true" ' +
        'return-data-precision="1" ' +
        'api-endpoint="kpis/fooKpi" ' +
        'api-return-key="barKey" ' +
        'show-metrics="false" ' +
        'kpi="kpi" ' +
        'current-organization="currentOrganizationMock" ' +
        'current-user="currentUserMock" ' +
        'current-sort-direction="currentSortDirection">' +
        '  </line-high-chart-widget>'
      );

      $compile(element)($scope);
      $scope.$digest();
      var lineHighChartWidget = element.controller('lineHighChartWidget');
      expect(lineHighChartWidget.valueLabel).toBe('');
    });

    it('should load the current site if the siteId is specified but no site object is passed in', function() {
      $scope.currentSite = undefined;
      $scope.siteId = 100;
      $scope.orgId = 1;

      lineHighChartWidget = renderDirectiveAndDigest();
      expect(lineHighChartWidget.currentSite.name).toBe('site 1');
    });

    it('should not load the current site if the currentSite is set', function() {
      $scope.currentSite = {
        id: 2,
        name: 'Some other site'
      };

      spyOn(mockSiteResource, 'get');

      lineHighChartWidget = renderDirectiveAndDigest(true);
      expect(mockSiteResource.get).not.toHaveBeenCalled();
    });

    describe('weather', function() {
      beforeEach(function() {
        $scope.orgId = 1;
        $scope.siteId = undefined;
      });

      it('should set weather to off is the currentUser is undefined', function() {
        $scope.currentUser = undefined;
        
        lineHighChartWidget = renderDirectiveAndDigest(true);
        expect(lineHighChartWidget.weatherEnabled).toBe(false);
      });

      it('should set weather to off if the user preferences are undefined', function() {
        $scope.currentUser = { };
        
        lineHighChartWidget = renderDirectiveAndDigest(true);
        expect(lineHighChartWidget.weatherEnabled).toBe(false);
      });

      it('should set weather to off if show weather is set to off for the whole widget', function() {
        $scope.currentUser = { 
          preferences: {}
        };

        $scope.showWeatherMetrics = false;
        
        lineHighChartWidget = renderDirectiveAndDigest(true);
        expect(lineHighChartWidget.weatherEnabled).toBe(false);    
      });

      it('should set weather to on if it is enabled for the widget and the user has enabled it', function() {
        $scope.currentUser = { 
          preferences: {
            weather_reporting: true
          }
        };

        $scope.showWeatherMetrics = true;
        
        lineHighChartWidget = renderDirectiveAndDigest(true);
        expect(lineHighChartWidget.weatherEnabled).toBe(true);   
      });

      it('should set weather to off if it is enabled for the widget and the user has disabled it', function() {
        $scope.currentUser = { 
          preferences: {
            weather_reporting: false
          }
        };

        $scope.showWeatherMetrics = true;
        
        lineHighChartWidget = renderDirectiveAndDigest(true);
        expect(lineHighChartWidget.weatherEnabled).toBe(false);   
      });
    });

    describe('initGroupBy', function() {
      it('should set group by to an empty object if not initialised on the root scope', function() {
        $rootScope.groupBy = undefined;
        $scope.groupBy = undefined;
        
        lineHighChartWidget = renderDirectiveAndDigest();

        expect($rootScope.groupBy).toBeDefined();
        expect(Object.keys($rootScope.groupBy).length).toBe(0);
      });

      it('should set the group by key if set on the scope', function() {
        $rootScope.groupBy = undefined;

        $scope.groupBy = {
          key: 'value'
        };

        lineHighChartWidget = renderDirectiveAndDigest();

        expect($rootScope.groupBy['someController-traffic'].key).toBe('value');
      });
    });
  });

  describe('hourly widgets', function() {
    describe('activate', function() {
      it('should default the current sort to the hourly column', function() {
        lineHighChartWidget = renderDirectiveAndDigest();
        expect(lineHighChartWidget.currentSort).toBe('hourSort');
      });

      it('should default to the table being displayed', function() {
        $scope.isHourly = true;
        $scope.showTable = undefined;

        lineHighChartWidget = renderDirectiveAndDigest();
        expect(lineHighChartWidget.showTable.selection).toBe(true);
      });
    });
  });

  describe('getCommonRequestParams', function () {
    it('should load right parameters', function () {
      lineHighChartWidget = renderDirectiveAndDigest();
      lineHighChartWidget.orgId = 1234;
      lineHighChartWidget.siteId = 567890;
      var params = lineHighChartWidget.getCommonRequestParams();
      expect(params.orgId).toBe(1234);
      expect(params.siteId).toBe(567890);
    });
  });

  describe('calculateDelta', function () {
    it('should calculate correct delta', function () {
      lineHighChartWidget = renderDirectiveAndDigest();
      var current = 375083;
      var compare = 402630;
      var delta = lineHighChartWidget.calculateDelta(current, compare);

      var manualCalculation = ((402630 - 375083) / 402630) * 100 * -1;
      expect(delta).toBe((manualCalculation));
    });
  });

  function renderDirectiveAndDigest(siteLevel) {
    var element;

    if(siteLevel === true) {
      element = createDirectiveElementForSiteLevel();
    } else {
      element = createDirectiveElement();
    }

    $compile(element)($scope);
    $scope.$digest();
    return element.controller('lineHighChartWidget');
  }

  function createDirectiveElement() {
    return angular.element(
      '<line-high-chart-widget ' +
      'is-hourly="isHourly" ' +
      'show-table="showTable" ' +
      'value-label="{{valueLabel}}" ' +
      'org-id="1234" ' +
      'site-id="56789" ' +
      'date-range-start="dateRangeStart" ' +
      'date-range-end="dateRangeEnd" ' +
      'compare-range-1-start="compareRange1Start" ' +
      'compare-range-1-end="compareRange1End" ' +
      'compare-range-2-start="compareRange2Start" ' +
      'compare-range-2-end="compareRange2End" ' +
      'first-day-of-week-setting="1" ' +
      'get-unique-returning="false" ' +
      'separate-summary-requests="true" ' +
      'summary-averages="true" ' +
      'return-data-precision="1" ' +
      'api-endpoint="kpis/fooKpi" ' +
      'api-return-key="barKey" ' +
      'show-metrics="false" ' +
      'group-by="groupBy" ' +
      'kpi="kpi" ' +
      'current-organization="currentOrganizationMock" ' +
      'current-user="currentUserMock" ' +
      'current-sort-direction="currentSortDirection">' +
      '  </line-high-chart-widget>'
    );
  }

  function createDirectiveElementForSiteLevel() {
    return angular.element(
      '<line-high-chart-widget ' +
      'is-hourly="isHourly" ' +
      'show-table="showTable" ' +
      'value-label="{{valueLabel}}" ' +
      'org-id="1234" ' +
      'site-id="siteId" ' +
      'date-range-start="dateRangeStart" ' +
      'date-range-end="dateRangeEnd" ' +
      'compare-range-1-start="compareRange1Start" ' +
      'compare-range-1-end="compareRange1End" ' +
      'compare-range-2-start="compareRange2Start" ' +
      'compare-range-2-end="compareRange2End" ' +
      'first-day-of-week-setting="1" ' +
      'get-unique-returning="false" ' +
      'separate-summary-requests="true" ' +
      'summary-averages="true" ' +
      'return-data-precision="1" ' +
      'api-endpoint="kpis/fooKpi" ' +
      'api-return-key="barKey" ' +
      'show-metrics="false" ' +
      'show-weather-metrics="showWeatherMetrics" ' +
      'current-site="currentSite" ' +
      'current-organization="currentOrganizationMock" ' +
      'current-user="currentUser" ' +
      'kpi="kpi" ' +
      'current-sort-direction="currentSortDirection">' +
      '  </line-high-chart-widget>'
    );
  }

  function putTemplateToTemplateCache($templateCache) {
    // Put an empty template to the template cache to prevent Angular from
    // trying to fetch it. We are only testing the controller here, so the
    // actual template is not needed.
    $templateCache.put(
      'components/widgets/line-high-chart-widget/line-high-chart-widget.partial.html',
      '<div></div>'
    );
  }

});
