(function () {
  'use strict';

  angular.module('shopperTrak.widgets')
    .directive('retailStoreSummaryWidget', retailStoreSummaryWidgetDirective);

  function retailStoreSummaryWidgetDirective() {
    return {
      templateUrl: 'components/widgets/retail-store-summary-widget/retail-store-summary-widget.partial.html',
      scope: {
        orgId: '=',
        currentUser: '=',
        dateRangeStart: '=',
        dateRangeEnd: '=',
        currentOrganization: '=',
        onExportClick: '&',
        exportIsDisabled: '=?',
        widgetTitle: '@',
        widgetIcon: '@',
        hideExportIcon: '=?',
        widgetData: '=?',
        orgSites: '=?',
        siteCategories: '=',
        selectedCategory: '=',
        selectedTags: '=',
        customTags: '=',
        filterText: '=',
        chartData: '=?',
        compStores: '=?',
        numberFormat: '=?',
        hasSales: '=?',
        hasInterior: '=?',
        hasLabor: '=?',
        includeExtremes: '=?',
        activeKpi: '=?',
        salesCategories: '=?',
        categories: '=?',
        widgetIsLoading: '=?widgetDataIsLoading',
        setSelectedWidget: '&',
        chartType: '=?'
      },
      controller: retailStoreSummaryWidgetController,
      controllerAs: 'vm',
      bindToController: true
    };
  }

  retailStoreSummaryWidgetController.$inject = [
    '$scope',
    '$rootScope',
    '$translate',
    '$timeout',
    '$q',
    '$filter',
    'LocalizationService',
    'SubscriptionsService',
    'SiteResource',
    'ObjectUtils',
    'retailOrganizationSummaryData',
    'retailStoreSummaryWidgetConstants',
    'metricConstants',
    'OrganizationResource',
    'maxSitesToCache',
    'currencyService'
  ];

  function retailStoreSummaryWidgetController(
    $scope,
    $rootScope,
    $translate,
    $timeout,
    $q,
    $filter,
    LocalizationService,
    SubscriptionsService,
    SiteResource,
    ObjectUtils,
    retailOrganizationSummaryData,
    constants,
    metricConstants,
    OrganizationResource,
    maxSitesToCache,
    currencyService
  ) {
    var vm = this;

    vm.hasData = hasData;
    vm.changeKpi = changeKpi;
    vm.toggleExtremes = toggleExtremes;
    vm.getSiteNameById = getSiteNameById;
    vm.transformData = transformData;
    vm.countTotals = countTotals;
    vm.renderWidget = renderWidget;
    vm.calculateAverages = calculateAverages;
    vm.filterCategory = filterCategory;
    vm.chartType = vm.chartType || 'column';

    vm.selectedMetrics = vm.selectedMetrics || [];
    vm.filter = vm.filter || {};
    vm.siteCategories = vm.siteCategories || {};
    if (_.isUndefined(vm.selectedCategory)) vm.selectedCategory = '';
    if (_.isUndefined(vm.includeExtremes)) vm.includeExtremes = false;
    vm.extremeToggle = { checkbox: (vm.includeExtremes) };
    vm.categories = vm.categories || ObjectUtils.getNestedProperty(vm, 'currentOrganization.portal_settings.sales_categories');
    vm.widgetDataIsLoading = true;

    if (ObjectUtils.isNullOrUndefined(vm.activeKpi)) {
      vm.activeKpi = 'conversion';
    }

    currencyService.getCurrencySymbol(vm.orgId).then(function (data) {
      vm.currencySymbol = data.currencySymbol;
      activate();
    });

    var chartConfig = constants.defaults;
    var unbindWidgetDataWatch;
    var unbindFilterTextWatch;
    var unbindFilterText;
    var unbindSelectedCategory;
    var unbindSalesCategories;
    var filterTextTimeout;


    function activate() {
      if (ObjectUtils.isNullOrUndefined(vm.orgSites)) {
        loadOrgSites();
      } else {
        vm.checkCache = vm.orgSites.length < maxSitesToCache;

        configureWatches();
        loadOrgSettings();
      }
    }

    function loadOrgSites() {
      vm.widgetDataIsLoading = true;

      SiteResource.query({ orgId: vm.orgId }).$promise.then(function(sites) {
        vm.orgSites = sites;
        vm.checkCache = ObjectUtils.isNullOrUndefined(vm.orgSites) || vm.orgSites.length < maxSitesToCache;
        configureWatches();
        loadOrgSettings();
      });
    }

    function configureWatches() {
      unbindWidgetDataWatch = $scope.$watchCollection('vm.widgetData', watchData);

      unbindFilterText = $scope.$watch('vm.filterText', function () {
        vm.filter.text = angular.copy(vm.filterText) || '';
      });

      unbindSelectedCategory = $scope.$watch('vm.selectedCategory', renderWidget);

      unbindSalesCategories = $scope.$watch('vm.salesCategories', getAllKpiData);

      unbindFilterTextWatch = $scope.$watch('vm.filter.text', function (val) {
        renderWidget();
        filter(val);
      });

      $scope.$on('$destroy', function () {
        if (typeof unbindFilterTextWatch === 'function') {
          unbindFilterTextWatch();
        }

        if (typeof unbindWidgetDataWatch === 'function') {
          unbindWidgetDataWatch();
        }

        if (typeof unbindWidgetDataWatch === 'function') {
          unbindWidgetDataWatch();
        }

        if (typeof unbindFilterText === 'function') {
          unbindFilterText();
        }

        if (typeof unbindSelectedCategory === 'function') {
          unbindSelectedCategory();
        }

        if (typeof unbindSalesCategories === 'function') {
          unbindSalesCategories();
        }
      });
    }

    function filter(val) {
      if (filterTextTimeout) {
        $timeout.cancel(filterTextTimeout);
      }

      var tempFilterText = val;
      filterTextTimeout = $timeout(function () {
        if (!ObjectUtils.isNullOrUndefined(vm.filter.text) && vm.filter.text.length > 2) {
          vm.filter.text = tempFilterText;
          vm.filterText = tempFilterText;
        } else {
          vm.filterText = tempFilterText;
          vm.currentDisplayUpTo = 25;
        }
      }, 250);
    }

    function loadOrgSettings() {
      OrganizationResource.get({orgId: vm.orgId}).$promise.then(function(org) {
        vm.currentOrg = org;

        var metricsNotShownOnWidget = [
          'upt',
          'traffic (pct)',
          'labor',
          'gsh',
          'opportunity',
          'abandonment_rate',
          'draw_rate'
          ];

        vm.metrics = metricConstants.metrics.filter(function(metric) {
          return !_.contains(metricsNotShownOnWidget, metric.value) &&
            SubscriptionsService.hasSubscriptions(metric.requiredPermissions, vm.currentOrg);
        });

        vm.numberFormat = LocalizationService.getCurrentNumberFormatName(vm.currentUser, vm.currentOrg);

        loadTranslations();
      });
    }

    $scope.$watchCollection('orgSites', function () {
      if (ObjectUtils.isNullOrUndefined(vm.widgetData)) {
        vm.widgetDataIsLoading = true;
        vm.widgetData = {};
        getAllKpiData();
      }
    });

    function watchData() {
      $timeout(function () {
        var dateRangeKey = retailOrganizationSummaryData.getDateRangeKey(vm.dateRangeStart, vm.dateRangeEnd);
        vm.rangeData = vm.widgetData[dateRangeKey];
        renderWidget();
      });
    }

    function loadKpiTranslations() {
      var kpiTransKeys = _.map(vm.metrics, function (metric) { return metric.shortTranslationLabel; });
      $translate(kpiTransKeys).then(function (result) {
        vm.kpTranslations = result;
      });
    }

    function addMetricPopUpRow(point) {
      var rows = '';

      var sortedMetrics = _.sortBy(vm.metrics, 'order');

      sortedMetrics.forEach(function (metric) {
        var metricValue = metric.value === 'traffic' ? point.x : point.kpis[metric.value];
        metricValue = metric.prefixSymbol + getFormattedValue(metricValue, metric) + metric.suffixSymbol;
        rows += '<tr><th>' + vm.kpTranslations[metric.shortTranslationLabel] + ': </th><td>' + metricValue + '</td></tr>';
      });

      return rows;
    }

    function getFormattedValue(value, metric) {
      return $filter('formatNumber')(value, metric.precision, vm.numberFormat);
    }

    function getToolTipHtmlFormat(point) {
      return '<div class="retail-summary-tooltip"><h3><a href="/#/' +
        vm.orgId +
        '/' + point.site_id + '">' +
        point.name + '</a></h3>' +
        '<table>' +
        addMetricPopUpRow(point) +
        '</table></div>';
    }

    function renderWidget() {
      if(!$rootScope.pdf && ObjectUtils.isNullOrUndefined(vm.orgSites)) {
        return;
      }

      vm.chartData = [];
      var dateRangeKey = retailOrganizationSummaryData.getDateRangeKey(vm.dateRangeStart, vm.dateRangeEnd);

      if (!ObjectUtils.isNullOrUndefined(vm.rangeData) && vm.rangeData.length > 0) {

        var min = {};
        var mid = {};
        var max = {};

        var chartData = transformData(vm.widgetData[dateRangeKey]);
        var total = countTotals(chartData);
        var avg = calculateAverages(chartData, total);
        vm.avg = avg;

        chartConfig.options.tooltip = {
          headerFormat: '',
          useHTML: true
        };

        chartConfig.options.tooltip.formatter = function () {
          return getToolTipHtmlFormat(this.point);
        };

        chartConfig.yAxis.title.text = $filter('translate')('kpis.kpiTitle.' + vm.activeKpi);
        chartConfig.xAxis.title.text = $filter('translate')('retailOrganization.DAILYTRAFFIC');

        max.x = Math.max.apply(Math, chartData.map(function (o) { return o.x; }));
        min.x = Math.min.apply(Math, chartData.map(function (o) { return o.x; }));
        max.y = Math.max.apply(Math, chartData.map(function (o) { return o.y; }));
        min.y = Math.min.apply(Math, chartData.map(function (o) { return o.y; }));

        min.x *= 0.90;
        max.x *= 1.10;
        min.y *= 0.90;
        max.y *= 1.10;

        if (avg.x - min.x < max.x - avg.x) {
          min.x = avg.x - (max.x - avg.x);
        } else {
          max.x = avg.x + (avg.x - min.x);
        }

        if (avg.y - min.y < max.y - avg.y) {
          min.y = avg.y - (max.y - avg.y);
        } else {
          max.y = avg.y + (avg.y - min.y);
        }

        mid = getMidValues(min, max);
        vm.mid = mid;

        if ((max.y - mid.y) > (mid.y - 0)) {
          min.y = mid.y - (max.y - mid.y);
        } else {
          min.y = 0;
          max.y = mid.y + mid.y;
        }

        if ((max.x - mid.x) > (mid.x - 0)) {
          min.x = mid.x - (max.x - mid.x);
        } else {
          min.x = 0;
          max.x = mid.x + mid.x;
        }

        chartConfig.options.yAxis.tickPositions = [min.y, mid.y, max.y];
        chartConfig.options.yAxis.plotLines.value = mid.y;

        chartConfig.options.xAxis.tickPositions = [min.x, mid.x, max.x];
        chartConfig.options.xAxis.plotLines.value = mid.x;

        chartConfig.options.plotOptions = {
          marker: {
            symbol: 'circle'
          }
        };

        chartConfig.options.yAxis.plotBands = [{
          from: min.y,
          to: mid.y,
          label: {
            text: $filter('translate')('retailOrganization.POORPERFORMERS'),
            align: 'left',
            verticalAlign: 'bottom',
            x: 10,
            y: -10,
            style: {
              color: '#929090',
              fontWeight: 'bold',
              textTransform: 'uppercase',
              fontFamily: 'Source Sans Pro',
            }
          }
        },
          {
            from: min.y,
            to: mid.y,
            label: {
              text: $filter('translate')('retailOrganization.INSTOREOPPORUNITY'),
              align: 'right',
              verticalAlign: 'bottom',
              x: -10,
              y: -10,
              style: {
                color: '#929090',
                fontWeight: 'bold',
                textTransform: 'uppercase',
                fontFamily: 'Source Sans Pro',
                margin: '10px'
              }
            }
          },
          {
            from: mid.y,
            to: max.y,
            label: {
              text: $filter('translate')('retailOrganization.OUTOFSTOREOPPORTUNITY'),
              align: 'left',
              verticalAlign: 'top',
              x: 10,
              y: 20,
              style: {
                color: '#929090',
                fontWeight: 'bold',
                textTransform: 'uppercase',
                fontFamily: 'Source Sans Pro',
                margin: '10px'
              }
            }
          },
          {
            from: mid.y,
            to: max.y,
            label: {
              text: $filter('translate')('retailOrganization.HIGHPERFORMERS'),
              align: 'right',
              verticalAlign: 'top',
              x: -10,
              y: 20,
              style: {
                color: '#929090',
                fontWeight: 'bold',
                textTransform: 'uppercase',
                fontFamily: 'Source Sans Pro',
                margin: '10px',
              }
            }
          }];
        var filteredData = angular.copy(chartData);
        filteredData = $filter('filter')(filteredData, vm.filter.text);

        vm.siteCategories = categorizeSites(vm.allSites);

        // narrow down to only those sites within the selected category
        if (vm.selectedCategory !== '') {
          filteredData = _.filter(filteredData, function (site) {
            return vm.siteCategories[site.site_id] === vm.selectedCategory;
          });
        }

        var cloneToolTip;
        var cloneToolTipContent;
        var currentChart;
        var openTooltipCoordinates;

        chartConfig.func = function (chart) {
          currentChart = chart;
        };

        chartConfig.series = [{
          name: 'Performance',
          type: 'scatter',
          data: filteredData,
          dataGrouping: {
            enabled: true
          },
          turboThreshold: 10000,
          enableMouseTracking: true,
          tooltip: {
            followPointer: false
          },
          marker: {
            symbol: 'circle'
          },
          point: {
            events: {
              click: function () {
                if (cloneToolTip) {
                  cloneToolTip.detach();

                  if (cloneToolTipContent) {
                    cloneToolTipContent.detach();
                  }

                  angular.element(currentChart.container).removeClass('has-fixed-popup');
                }

                var clickedTooltip = { x: this.series.chart.tooltip.now.x, y: this.series.chart.tooltip.now.y };

                // If clicked tooltip is not open already, open it.
                if (currentChart &&
                    currentChart.container &&
                    (openTooltipCoordinates === undefined || (openTooltipCoordinates && openTooltipCoordinates.x !== clickedTooltip.x && openTooltipCoordinates.y !== clickedTooltip.y))) {
                  // Clone the tooltip container
                  cloneToolTip = angular.element(this.series.chart.tooltip.label.element.cloneNode(true));
                  cloneToolTip.addClass('fixed');
                  angular.element(currentChart.container.firstChild).append(cloneToolTip);

                  // Clone the tooltip content
                  var toolTipContent = angular.element('div.highcharts-tooltip');
                  cloneToolTipContent = toolTipContent.clone(true);
                  cloneToolTipContent.addClass('fixed');
                  angular.element(currentChart.container).append(cloneToolTipContent);

                  // Mark chart-container for CSS styling
                  angular.element(currentChart.container).addClass('has-fixed-popup');
                }

                openTooltipCoordinates = clickedTooltip;
              }
            }
          }
        }];

        chartConfig.xAxis.min = min.x;
        chartConfig.xAxis.max = max.x;
        chartConfig.yAxis.min = min.y;
        chartConfig.yAxis.max = max.y;

        if($rootScope.pdf) {
          chartConfig.options.plotOptions.series = {
            animation: false
          }
        }

        vm.chartData = filteredData;
        vm.chartConfig = chartConfig;

        vm.widgetDataIsLoading = false;
      }
    }

    function transformData(sourceData) {
      if (!ObjectUtils.isNullOrUndefined(sourceData)) {
        var chartData = [];

        _.each(sourceData, function (site) {
          if (site.traffic > 0 && site[vm.activeKpi] > 0) {
            chartData.push({
              x: getTrafficValue(site.traffic),
              y: site[vm.activeKpi],
              name: getSiteNameById(site.site_id),
              site_id: site.site_id,
              kpis: {
                sales: site.sales,
                conversion: site.conversion,
                dwelltime: site.dwell_time,
                ats: site.ats,
                star: site.star,
                aur: site.aur,
                splh: site.splh,
                sps: site.sps,
                transactions: site.transactions
              }
            });
          }
        });

        vm.allSites = chartData;

        if (vm.includeExtremes === false) {
          var perce;
          chartData = $filter('orderBy')(chartData, 'x');
          perce = percentile(chartData, 0.9, 'x');
          chartData = _.filter(chartData, function (item) {
            return item.x <= perce;
          });
          chartData = $filter('orderBy')(chartData, 'y');
          perce = percentile(chartData, 0.9, 'y');
          chartData = _.filter(chartData, function (item) {
            return item.y <= perce;
          });
        }

        return chartData;
      }
    }

    function getTrafficValue(traffic) {
      if(typeof traffic === 'string') {
        return Number(traffic);
      }

      return traffic;
    }

    function hasData() {
      var dateRangeKey = retailOrganizationSummaryData.getDateRangeKey(vm.dateRangeStart, vm.dateRangeEnd);
      return !ObjectUtils.isNullOrUndefined(vm.widgetData) && !ObjectUtils.isNullOrUndefined(vm.widgetData[dateRangeKey]);
    }

    function toggleExtremes() {
      if (vm.extremeToggle.checkbox) {
        vm.includeExtremes = true;
      } else {
        vm.includeExtremes = false;
      }
      renderWidget();
    }

    function changeKpi(kpi) {
      vm.activeKpi = kpi;
      renderWidget();
    }

    function percentile(arr, p, property) {
      if (ObjectUtils.isNullOrUndefined(arr) || arr.length === 0) {
        return 0;
      }
      var length = arr.length;
      var index = Math.round(p * length);
      return arr[index - 1][property];
    }

    function countTotals(arr) {
      var total = {};
      total.x = _.reduce(arr, function (total, item) {
        return total + item.x;
      }, 0);
      total.y = _.reduce(arr, function (total, item) {
        return total + item.y;
      }, 0);
      return total;
    }

    function calculateAverages(arr, totals) {
      var avg = { x: 0, y: 0 };
      avg.x = totals.x / arr.length;
      avg.y = totals.y / arr.length;
      return avg;
    }

    function getSiteNameById(siteId) {
      if($rootScope.pdf === true) {
        return;
      }

      // In PDF exports, we do not need site names as we only show the chart.
      if (!ObjectUtils.isNullOrUndefined(vm.orgSites)) {
        var site = vm.orgSites.filter(function (obj) {
          return parseInt(obj.site_id) === parseInt(siteId);
        });
        if (!ObjectUtils.isNullOrUndefined(site[0])) {
          return site[0].name;
        }
      }
    }

    function getMidValues(min, max) {
      return { x: (min.x + ((max.x - min.x) / 2)), y: (min.y + ((max.y - min.y) / 2)) };
    }

    function categorizeSites(data) {
      var categories = {};

      angular.forEach(data, function (siteData) {

        var x = siteData.x;
        var y = siteData.y;
        var mid = vm.mid;

        var siteId = siteData.site_id;

        if (x < mid.x && y >= mid.y) {
          // 0 = out of store opportunity
          categories[siteId] = 0;
        } else if (x >= mid.x && y >= mid.y) {
          // 1 = high performer
          categories[siteId] = 1;
        } else if (x >= mid.x && y < mid.y) {
          // 2 = in store opportunity
          categories[siteId] = 2;
        } else if (x < mid.x && y < mid.y) {
          // 3 = low performer
          categories[siteId] = 3;
        }

      });

      return categories;
    }

    function filterCategory(cat) {
      vm.selectedCategory = cat;
    }

    function loadTranslations() {
      $translate.use(vm.language);
      loadKpiTranslations();
    }

    function getAllKpiData() {
      getReportData(vm.orgId, vm.dateRangeStart, vm.dateRangeEnd, vm.selectedTags);
    }

    function getReportData(orgId, dateRangeStart, dateRangeEnd, selectedTags) {
      var dateRangeKey = retailOrganizationSummaryData.getDateRangeKey(dateRangeStart, dateRangeEnd);

      if(ObjectUtils.isNullOrUndefined(vm.widgetData)) {
        vm.widgetData = [];
      }

      vm.widgetData[dateRangeKey] = [];
      vm.widgetDataIsLoading = true;

      var params = {
        orgId: orgId,
        comp_site: vm.compStores,
        dateRangeStart: dateRangeStart,
        dateRangeEnd: dateRangeEnd,
        selectedTags: selectedTags
      };

      if(!ObjectUtils.isNullOrUndefinedOrEmpty(vm.salesCategories)){
       params.sales_category_id = _.pluck(vm.salesCategories, 'id');
      }

      if( !ObjectUtils.isNullOrUndefinedOrEmpty(vm.customTags) ) {
        params.customTagId = vm.customTags;
      }

      retailOrganizationSummaryData.fetchReportData(params, vm.checkCache, function (data) {
        vm.widgetData[dateRangeKey] = data;
        watchData();
      });
    }
  }
})();
