(function () {
  'use strict';

  angular.module('shopperTrak')
  .directive('marketIntelligenceWidget', marketIntelligenceWidgetDirective);

  function marketIntelligenceWidgetDirective() {
    return {
      restrict: 'E',
      templateUrl: 'components/widgets/market-intelligence-widget/market-intelligence-widget.partial.html',
      scope: {
        segments: '=',
        selectedOptions: '=',
        geography: '=',
        trendAnalysisPanelArray: '=',
        numberFormatName: '=',
        dateFormat: '=',
        currentUser:'=',
        currentOrg: '=',
        showOrgIndex:'=',
        isPriorYear:'='
      },
      controller: marketIntelligenceWidgetController,
      controllerAs: 'vm',
      bindToController: true
    };
  }

  marketIntelligenceWidgetController.$inject = [
    '$translate',
    '$q',
    '$scope',
    '$filter',
    'ObjectUtils',
    'LocalizationService',
    'marketIntelligenceService',
    'utils'
  ];

  function marketIntelligenceWidgetController($translate, $q, $scope, $filter, ObjectUtils, LocalizationService, marketIntelligenceService, utils) {
    var vm = this;

    var baseDropdownOptions;

    // To Do: move this into init scope
    vm.isLoading = true;
    vm.toogleTableShow = toogleTableShow;
    vm.showTable = false;
    vm.trendAnalysisPanelArray = [];
    vm.marketIndexPoints = [];
    vm.orgIndexPoints = [];
    vm.dataPoints = [];
    var translation = {};
    var previousGeoTypeKey;

    activate();

    function activate() {
      setupLocalizationService();

      getMIWidgetTranslations().then(function (translations) {
        vm.translations = translations;
        buildPeriodToDateLabels(translations);
        buildTrendAnalysisOptions(translations);
        configureDefaultOptions();
        updateWidget(vm.marketIndexPoints, vm.orgIndexPoints);
      });
    }

    function marketOnlyColLabel(yoyMarket) {
      if (vm.isPriorYear) {
        return [[vm.translations.category, vm.translations.geographyLevel, vm.translations.geographyName, vm.translations.periodType, vm.translations.period, yoyMarket]];
      }
      return [[vm.translations.category, vm.translations.geographyLevel, vm.translations.geographyName, vm.translations.periodType, vm.translations.period, vm.translations.comparePeriod, yoyMarket]];
    }

    function orgAndMarketOnlyColLabel(yoyOrg, yoyMarket) {
      if (vm.isPriorYear) {
        return [[vm.translations.category, vm.translations.geographyLevel, vm.translations.geographyName, vm.translations.periodType, vm.translations.period, yoyMarket, yoyOrg]];
      }
      return [[vm.translations.category, vm.translations.geographyLevel, vm.translations.geographyName, vm.translations.periodType, vm.translations.period, vm.translations.comparePeriod, yoyMarket, yoyOrg]];
    }

    function marketOnlyColData(item, periodName, marketDataString) {
      if (vm.isPriorYear) {
        return [item.categoryname, item.geographytype, item.geographyname, periodName, item.startDates + ' - ' + item.endDates, marketDataString];
      }
      return [item.categoryname, item.geographytype, item.geographyname, periodName, item.startDates + ' - ' + item.endDates, item.comStartDates + ' - ' + item.comEndDates, marketDataString]
    }

    function orgAndMarketOnlyColData(item, periodName, marketDataString, orgDataString) {
      if (vm.isPriorYear) {
        return [item.categoryname, item.geographytype, item.geographyname, periodName, item.startDates + ' - ' + item.endDates, marketDataString, orgDataString];
      }
      return [item.categoryname, item.geographytype, item.geographyname, periodName, item.startDates + ' - ' + item.endDates, item.comStartDates + ' - ' + item.comEndDates, marketDataString, orgDataString]
    }

    function createCsvColumnLabels(yoyOrg, yoyMarket) {
      var createCellsArray;
      if (vm.showGeographyNames && vm.showOrgIndex) {
        createCellsArray = orgAndMarketOnlyColLabel(yoyOrg, yoyMarket)

      } else if (vm.showGeographyNames && !vm.showOrgIndex) {
        createCellsArray = marketOnlyColLabel(yoyMarket)
      } else {
        if (isCompareDates()) {
          if (vm.showOrgIndex) {
            createCellsArray = [[vm.translations.category, vm.translations.geographyLevel, vm.translations.geographyName, vm.translations.periodType, vm.translations.selectedPeriod, vm.translations.comparePeriod, yoyMarket, yoyOrg]];
          } else if (!vm.showOrgIndex) {
            createCellsArray = [[vm.translations.category, vm.translations.geographyLevel, vm.translations.geographyName, vm.translations.periodType, vm.translations.selectedPeriod, vm.translations.comparePeriod, yoyMarket]];
          }
        } else {
          if (vm.showOrgIndex) {
            createCellsArray = [[vm.translations.category, vm.translations.geographyLevel, vm.translations.geographyName, vm.translations.periodType, vm.translations.selectedPeriod, yoyMarket, yoyOrg]];
          } else if (!vm.showOrgIndex) {
            createCellsArray = [[vm.translations.category, vm.translations.geographyLevel, vm.translations.geographyName, vm.translations.periodType, vm.translations.selectedPeriod, yoyMarket]];
          }
        }
      }
      return createCellsArray;
    }

    vm.onCsvExportClick = function () {

      var cellsArray = createCsvColumnLabels(vm.translations.yoyOrg, vm.translations.yoyMarket);

      if (!vm.isPriorYear) {
        cellsArray = createCsvColumnLabels(vm.translations.customOrg, vm.translations.customMarket);
      }

      var cellsString = getCsvHeaderSection();

      var periodName = vm.showCustom;

      _.each(vm.dataPoints, function (item, index) {

        var marketDataString = getValueForCsv(item.marketIndexData);

        var orgDataString = getValueForCsv(item.orgIndexData);

        if (vm.periodType.period === 'periodToDate') {
          periodName = vm.periodToDateLabels[index];
        }

        if (vm.showGeographyNames && vm.showOrgIndex) {

          cellsArray.push(orgAndMarketOnlyColData(item, periodName, marketDataString, orgDataString));

        } else if (vm.showGeographyNames && !vm.showOrgIndex) {

          cellsArray.push(marketOnlyColData(item, periodName, marketDataString));

        } else {
          if (isCompareDates()) {
            var startDates = item.startDate;
            var compareDates = item.comStartDates;

            if (vm.periodType.period === 'periodToDate') {
              startDates = item.startDates + ' - ' + item.endDates;
              compareDates = item.comStartDates + ' - ' + item.comEndDates;
            }

            if (vm.showOrgIndex) {
              cellsArray.push([item.categoryname, item.geographytype, item.geographyname, periodName, startDates, compareDates, marketDataString, orgDataString]);
            } else if (!vm.showOrgIndex) {
              cellsArray.push([item.categoryname, item.geographytype, item.geographyname, periodName, startDates, compareDates, marketDataString]);
            }
          } else {
            if (vm.showOrgIndex) {
              cellsArray.push([item.categoryname, item.geographytype, item.geographyname, periodName, item.startDates + ' - ' + item.endDates, marketDataString, orgDataString]);
            } else if (!vm.showOrgIndex) {
              cellsArray.push([item.categoryname, item.geographytype, item.geographyname, periodName, item.startDates + ' - ' + item.endDates, marketDataString]);
            }
          }
        }
      });

      _.each(cellsArray, function (cellItem) {
        var dataString = cellItem.join(',');
        cellsString += dataString + '\n';
      });

      utils.saveFileAs('trend-analysis.csv', cellsString, 'text/csv');
    };

    function getValueForCsv(value) {
      return $filter('formatNumber')(value, 2, vm.numberFormatName) + '%';
    }

    function getCsvHeaderSection() {
      var dateFormatMask = LocalizationService.getCurrentDateFormat(vm.currentOrg);

      var datePeriod = '';

      if(vm.periodType.period === 'periodToDate') {
        datePeriod = translation.periodToDate + ': ' + vm.selectedOptions.dateEnd.format(dateFormatMask)
      } else {
        datePeriod = vm.translations.reportPeriod + vm.selectedOptions.dateStart.format(dateFormatMask) + ' - ' + vm.selectedOptions.dateEnd.format(dateFormatMask);
      }

      var calendarName = getCalendarName();

      var groupBy = vm.translations.groupBy + vm.selectedGroupingName;

      var reportDate = vm.translations.reportRun + moment().format(dateFormatMask);

      var cellsString = '';

      cellsString += vm.translations.organization + ': ' + vm.currentOrg.name + '\n';

      cellsString += datePeriod + '\n';

      cellsString += calendarName + '\n';

      cellsString += vm.translations.sameStoreIndex + '\n';

      cellsString += translation.geography + ': ' + vm.geographyDisplayType + '\n';

      cellsString += translation.category + ': ' + vm.categoryDisplayName + '\n';

      cellsString += groupBy + '\n';

      cellsString += reportDate + '\n \n';

      return cellsString;
    }

    function getCalendarName() {
      var currentCalendarName = LocalizationService.getCurrentCalendarName();

      if(!angular.isDefined(currentCalendarName)) {
        var calendarInfo = LocalizationService.getActiveCalendar();

        if(LocalizationService.isCalendarIdGregorianMonday(calendarInfo.id)) {
          currentCalendarName = translation.standardGregorianMonday;
        } else {
          currentCalendarName = translation.standardGregorianSunday;
        }
      }

      return vm.translations.calendar + ': ' + currentCalendarName;
    }

    function setupLocalizationService() {
      LocalizationService.setUser(vm.currentUser);
      LocalizationService.setOrganization(vm.currentOrg);
    }

    function getMIWidgetTranslations() {
      var deferred = $q.defer();

      var miWidgetTransKeys = [
        'marketIntelligence.XAXIS',
        'marketIntelligence.TIME',
        'marketIntelligence.DAY',
        'marketIntelligence.WEEK',
        'marketIntelligence.MONTH',
        'marketIntelligence.QUARTER',
        'marketIntelligence.YEAR',
        'marketIntelligence.SITEGROUPING',
        'marketIntelligence.REGIONS',
        'marketIntelligence.METROS',
        'marketIntelligence.MARKETINDEX',
        'marketIntelligence.ORGINDEX',
        'marketIntelligence.GEOGRAPHYNAME',
        'marketIntelligence.SELECTEDPERIOD',
        'marketIntelligence.COMPAREPERIOD',
        'marketIntelligence.SAMESTOREINDEX',
        'marketIntelligence.GROUPBY',
        'marketIntelligence.REPORTRUN',
        'marketIntelligence.REPORTPERIOD',
        'marketIntelligence.CATEGORY',
        'marketIntelligence.GEOGRAPHY',
        'common.ORGANIZATION',
        'common.CALENDAR',
        'common.STANDARDGREGORIANSUNDAY',
        'common.STANDARDGREGORIANMONDAY',
        'marketIntelligence.WTD',
        'marketIntelligence.QTD',
        'marketIntelligence.MTD',
        'marketIntelligence.YTD',
        'marketIntelligence.GEOGRAPHYLEVEL',
        'marketIntelligence.MODELTYPE',
        'marketIntelligence.PERIODTYPE',
        'marketIntelligence.PERIOD',
        'marketIntelligence.YOYCHANGEMKT',
        'marketIntelligence.YOYCHANGEORG',
        'marketIntelligence.CHANGEMKT',
        'marketIntelligence.CHANGEORG',
        'marketIntelligence.CUSTOM',
        'marketIntelligence.PERIODTODATE'
      ];

      $translate(miWidgetTransKeys).then(function (translations) {
        translation.xaxis = translations['marketIntelligence.XAXIS'];
        translation.time = translations['marketIntelligence.TIME'];
        translation.day = translations['marketIntelligence.DAY'];
        translation.week = translations['marketIntelligence.WEEK'];
        translation.month = translations['marketIntelligence.MONTH'];
        translation.quarter = translations['marketIntelligence.QUARTER'];
        translation.year = translations['marketIntelligence.YEAR'];
        translation.siteGrouping = translations['marketIntelligence.SITEGROUPING'];
        translation.regions = translations['marketIntelligence.REGIONS'];
        translation.metros = translations['marketIntelligence.METROS'];
        translation.marketIndex = translations['marketIntelligence.MARKETINDEX'];
        translation.orgIndex = translations['marketIntelligence.ORGINDEX'];
        translation.geographyName = translations['marketIntelligence.GEOGRAPHYNAME'];
        translation.selectedPeriod = translations['marketIntelligence.SELECTEDPERIOD'];
        translation.comparePeriod = translations['marketIntelligence.COMPAREPERIOD'];
        translation.sameStoreIndex = translations['marketIntelligence.SAMESTOREINDEX'];
        translation.groupBy = translations['marketIntelligence.GROUPBY'];
        translation.reportRun = translations['marketIntelligence.REPORTRUN'];
        translation.reportPeriod = translations['marketIntelligence.REPORTPERIOD'];
        translation.organization = translations['common.ORGANIZATION'];
        translation.calendar = translations['common.CALENDAR'];
        translation.standardGregorianMonday = translations['common.STANDARDGREGORIANMONDAY'];
        translation.standardGregorianSunday = translations['common.STANDARDGREGORIANSUNDAY'];
        translation.category = translations['marketIntelligence.CATEGORY'];
        translation.geography = translations['marketIntelligence.GEOGRAPHY'];
        translation.wtd = translations['marketIntelligence.WTD'];
        translation.qtd = translations['marketIntelligence.QTD'];
        translation.mtd = translations['marketIntelligence.MTD'];
        translation.ytd = translations['marketIntelligence.YTD'];
        translation.geographyLevel = translations['marketIntelligence.GEOGRAPHYLEVEL'];
        translation.modelType = translations['marketIntelligence.MODELTYPE'];
        translation.periodType = translations['marketIntelligence.PERIODTYPE'];
        translation.period = translations['marketIntelligence.PERIOD'];
        translation.yoyMarket = translations['marketIntelligence.YOYCHANGEMKT'];
        translation.yoyOrg = translations['marketIntelligence.YOYCHANGEORG'];
        translation.customMarket = translations['marketIntelligence.CHANGEMKT'];
        translation.customOrg = translations['marketIntelligence.CHANGEORG'];
        translation.custom = translations['marketIntelligence.CUSTOM'];
        translation.periodToDate = translations['marketIntelligence.PERIODTODATE'];

        deferred.resolve(translation);
      });

      return deferred.promise;
    }

    function buildTrendAnalysisOptions(translation) {
      baseDropdownOptions = [{
        name: translation.xaxis,
        type: 'group'
      }, {
        name: translation.time,
        type: 'group'
      }, {
        name: translation.day,
        type: 'option',
        cast: 'time',
        period: 'day'
      },{
        name: translation.week,
        type: 'option',
        cast:'time',
        period: 'week'
      }, {
        name: translation.month,
        type: 'option',
        cast:'time',
        period: 'month'
      }, {
        name: translation.quarter,
        type: 'option',
        cast:'time',
        period: 'quarter'
      }, {
        name: translation.year,
        type: 'option',
        cast: 'time',
        period: 'year'
      }, {
        name: translation.periodToDate,
        type: 'option',
        cast: 'timeToDate', // This forces the graph to render as bars
        period: 'periodToDate'
      }, {
        name: translation.siteGrouping,
        type: 'group'
      }, {
        name: translation.regions,
        type: 'option',
        geoTypeKey: 'REGION',
        cast: 'geo'
      }, {
        name: translation.metros,
        type: 'option',
        geoTypeKey: 'METRO',
        cast: 'geo'
      }];
    }

    function configureDefaultOptions() {
      vm.selectedItem = _.findWhere(baseDropdownOptions, {geoTypeKey: 'REGION'});

      $scope.$watch('vm.selectedOptions', function () {
        getDataForAdvanceOptions();
      });
    }

    function firstChildrenGeographies(geographies, geo) {
      var geos = [];
      _.each(geographies, function (g) {

        if (_.has(geo, 'childrenUuids')) {
          var geoChilds = _.find(geo.childrenUuids, function (item) {
            return item === g.uuid;
          });
        }
        if (!ObjectUtils.isNullOrUndefined(geoChilds)) {
          geos.push(g);
        }
      });
      return geos;
    }

    function findGeography(geographies, geographyUuid) {
      return _.find(geographies, function (g) {
        return g.uuid === geographyUuid;
      });
    }

    function createParamsObj(subsObject) {
      if (isCompareDates()) {
        return {
          dateStart: vm.selectedOptions.dateStart,
          dateEnd: vm.selectedOptions.dateEnd,
          compareStart: vm.selectedOptions.compareStart,
          compareEnd: vm.selectedOptions.compareEnd,
          subscriptions: subsObject
        };
      }
      return {
        dateStart: vm.selectedOptions.dateStart,
        dateEnd: vm.selectedOptions.dateEnd,
        subscriptions: subsObject
      };
    }

    function getDataForAdvanceOptions() {
      if (ObjectUtils.isNullOrUndefined(vm.selectedItem)) {
        vm.showCustom = vm.translations.custom;
      } else {
        if (vm.selectedItem.cast !== 'time') {
          vm.showCustom = vm.translations.custom;
        } else {
          vm.showCustom = vm.selectedItem.name;
        }
      }

      if (!ObjectUtils.isNullOrUndefined(vm.selectedOptions)) {
        vm.isLoading = true;
        var geoUuid = vm.selectedOptions.subscriptions.uuid;
        var geoUuidObj = findGeography(vm.geography, geoUuid);
        var geoUuidChildObjs = firstChildrenGeographies(vm.geography, geoUuidObj);
        var subscriptionObj = _.map([geoUuidObj].concat(geoUuidChildObjs), function (item) {
          return {
            category: vm.selectedOptions.subscriptions.category,
            geography: item,
            orgId: vm.selectedOptions.subscriptions.orgId
          };
        });

        vm.geographyDisplayType = geoUuidObj.name;
        vm.categoryDisplayName = vm.selectedOptions.subscriptions.category.name;

        updateDropdownOptions(geoUuidObj);

        updateTrendAnalysisArray(createParamsObj(subscriptionObj)).then(function(result) {
          switch(result) {
            case 'ok': {
              vm.requestFailed = false;
              vm.hasData = true;
              break;
            }
            case 'No Data': {
              vm.hasData = false;
              vm.requestFailed = false;
              break;
            }
            case 'error': {
              vm.requestFailed = true;
              vm.hasData = true;
              break;
            }
          }

          vm.isTimePeriodSelected = false;

          updateData('geo');
        });
      }
    }

    function isCompareDates() {
      return _.has(vm.selectedOptions, 'compareStart') && _.has(vm.selectedOptions, 'compareEnd')
    }

    vm.updateDataPoints = function (selectedDropDown) {
      if(_.has(vm.selectedOptions,'compareStart')){
        vm.showCompareColumn = true;
      }

      vm.periodType = angular.copy(selectedDropDown);
      vm.daySelected = (selectedDropDown.name === 'Day');

      if (selectedDropDown.selected === true) {
        return;
      }

      vm.isLoading = true;

      deselectAllTrendAnalysisOptions();

      selectedDropDown.selected = true;
      vm.selectedGroupingName = selectedDropDown.name;
      vm.selectedItem = selectedDropDown;
      vm.showCustom = selectedDropDown.name;
      
      if (vm.selectedItem.cast !== 'time') {
        vm.showCustom = vm.translations.custom;
      }

      if(angular.isDefined(selectedDropDown.geoTypeKey)) {
        vm.isTimePeriodSelected = false;
        getDataForAdvanceOptions();
        return;
      }

      vm.isTimePeriodSelected = true;
      var geoUuid = vm.selectedOptions.subscriptions.uuid;
      var geoUuidObj = findGeography(vm.geography, geoUuid);

      var category = vm.selectedOptions.subscriptions.category;
      var queryObj = {
        dateStart: vm.selectedOptions.dateStart,
        dateEnd: vm.selectedOptions.dateEnd,
        subscriptions: [{
          orgId: vm.selectedOptions.subscriptions.orgId,
          geography: geoUuidObj,
          category: category
        }],
        groupByDateRanges: []
      };

      var calendarId = LocalizationService.getActiveCalendar().id;
      var startPeriod = vm.selectedOptions.dateStart;
      var endPeriod = vm.selectedOptions.dateEnd;

      if (isCompareDates()) {
        var compareStartPeriod = vm.selectedOptions.compareStart;
        var compareEndPeriod = vm.selectedOptions.compareEnd;

        // If we're looking at a gregorian calendar, we just pass in the current dates and let the MI service calculate the prior dates for us
        if(selectedDropDown.period === 'periodToDate' && LocalizationService.isCurrentCalendarGregorian()) {
          compareStartPeriod = startPeriod;
          compareEndPeriod = endPeriod;
        }

        $q.all(
          [
            marketIntelligenceService.getCalendarDateRanges(calendarId, selectedDropDown.period, startPeriod, endPeriod, '', vm.currentUser, vm.currentOrg),
            marketIntelligenceService.getCalendarDateRanges(calendarId, selectedDropDown.period, compareStartPeriod, compareEndPeriod, 'compare', vm.currentUser, vm.currentOrg)
          ]
        )
        .then(function (res) {
          addDateRangesProperty(res[0], queryObj, 'groupByDateRanges', selectedDropDown.period);
          addDateRangesProperty(res[1], queryObj, 'groupByCompareDateRanges', selectedDropDown.period);
          updateTrendAnalysisArray(queryObj).then(function(res) {
            validateResult(res, selectedDropDown);
          });
        })
        .catch(displayError);

      } else {
        marketIntelligenceService.getCalendarDateRanges(calendarId, selectedDropDown.period, startPeriod, endPeriod)
        .then(function (res) {
          addDateRangesProperty(res, queryObj, 'groupByDateRanges', selectedDropDown.period);

          updateTrendAnalysisArray(queryObj).then(function(res) {
            validateResult(res, selectedDropDown);
          });
        })
        .catch(displayError);
      }
    };

    function addDateRangesProperty(passedArray, passedObj, propertyName, selectedPeriodName) {
      if(selectedPeriodName === 'periodToDate') {
        var results = passedArray.data.result[0];

        passedObj[propertyName] = [
          results.year,
          results.quarter,
          results.month,
          results.week
        ];

        return;
      }

      _.each(passedArray.data.result, function (eachItem) {
        if (eachItem.start === eachItem.end) {
          passedArray.data.result = _.without(passedArray.data.result, eachItem);
        }
        passedObj[propertyName] = passedArray.data.result;
      });
    }

    function validateResult(result, selectedDropDown) {
      if (result === 'Ok') {
        vm.requestFailed = false;
        vm.hasData = true;
      } else if (result === 'No Data') {
        vm.hasData = false;
        vm.requestFailed = false;
      } else {
        vm.requestFailed = true;
        vm.hasData = true;
      }

      updateData(selectedDropDown.cast);
    }

    function deselectAllTrendAnalysisOptions() {
      _.each(vm.trendAnalysisDropDownArray, function(dropdown) {
        dropdown.selected = false;
      });
    }

    function updateTrendAnalysisArray(obj) {
      var deferred = $q.defer();
      
      marketIntelligenceService.getIndexData(obj, vm.showOrgIndex).then(function (res) {

        if (ObjectUtils.isNullOrUndefined(vm.periodType)) {
          vm.periodType = {
            name: 'Custom'
          }
        } else if (vm.periodType.cast === 'geo') {
          vm.periodType.name = 'Custom'
        }

        vm.requestFailed = false;
        vm.hasData = true;
        vm.trendAnalysisPanelArray = [];
        for (var i = 0; i < res.index.length; i++) {
          var indexDataObj = {};

          if (res.index[i].valid === false && !ObjectUtils.isNullOrUndefined(res.index[i].errorMessage)) {
            indexDataObj.noMarketIndex = true;
          }

          indexDataObj.periodType = vm.periodType;
          indexDataObj.marketindexchange = res.index[i].value * 100;
          indexDataObj.categoryname = res.index[i].subscription.category.name;
          indexDataObj.geographyname = res.index[i].subscription.geography.name;
          indexDataObj.geographytype = res.index[i].subscription.geography.geoType;
          indexDataObj.startDate = res.index[i].dateStart;
          indexDataObj.endDate = res.index[i].dateEnd;
          indexDataObj.comStartDate = res.index[i].compDateStart;
          indexDataObj.comEndDate = res.index[i].compDateEnd;

          if (vm.showOrgIndex === true) {
            if (res.org[i].valid === false && !ObjectUtils.isNullOrUndefined(res.org[i].errorMessage)) {
              indexDataObj.noOrgIndex = true;
            }
            
            indexDataObj.orgindexchange = res.org[i].value * 100;
          }

          vm.trendAnalysisPanelArray.push(indexDataObj);
        }
        deferred.resolve('Ok');
      }).catch(function(error) {
        console.error(error);
        deferred.resolve(error);
      });

      return deferred.promise;
    }

    function updateDropdownOptions(geography) {
      vm.trendAnalysisDropDownArray = _.without(baseDropdownOptions, _.findWhere(baseDropdownOptions, {
        geoTypeKey: geography.geoType
      }));

      if(geography.geoType === 'COUNTRY') {
        vm.trendAnalysisDropDownArray = _.without(vm.trendAnalysisDropDownArray, _.findWhere(vm.trendAnalysisDropDownArray, {
          geoTypeKey: 'METRO'
        }));
      }

      var numOfDaysSelected = utils.getDaysBetweenDates(vm.selectedOptions.dateStart, vm.selectedOptions.dateEnd);

      if (numOfDaysSelected > 45) {
        vm.trendAnalysisDropDownArray = _.filter(_.without(vm.trendAnalysisDropDownArray, _.findWhere(baseDropdownOptions, {geoTypeKey: geography.geoType})),
          function (item) {
            return item.name !== 'Day'
          });
      }

      if(previousGeoTypeKey !== geography.geoType) {
        deselectAllTrendAnalysisOptions();
        previousGeoTypeKey = geography.geoTypeKey;
      }

      var availableDefaults = _.filter(vm.trendAnalysisDropDownArray, function(item) {
        return angular.isDefined(item.geoTypeKey);
      });

      availableDefaults[0].selected = true;

      vm.selectedGroupingName = availableDefaults[0].name;
    }

    function changeChartType(selectedData) {
      var chartType = 'column';
      var isTime = selectedData === 'time';
      if (isTime) {
        chartType = 'line';
      }
      return chartType;
    }

    function updateData(selectedType) {
      vm.marketIndexPoints = [];
      vm.orgIndexPoints = [];

      vm.xAxisLabels = {
          geographyNames : [],
          startDates : []
      };

      var tempDataPoints = [];

      _.each(vm.trendAnalysisPanelArray, function (item, index) {

        vm.marketIndexPoints.push(item.marketindexchange);
        if(!vm.showOrgIndex) {
          item.orgindexchange = null;
        }
        vm.orgIndexPoints.push(item.orgindexchange);
        vm.xAxisLabels.geographyNames.push(item.geographyname);

        var formattedStartDate = moment.utc(item.startDate).format(vm.dateFormat);
        var formattedEndDate = moment.utc(item.endDate).format(vm.dateFormat);
        var formattedComStartDate = moment.utc(item.comStartDate).format(vm.dateFormat);
        var formattedComEndDate = moment.utc(item.comEndDate).format(vm.dateFormat);
        var dataPoint = buildDataPoint(item, formattedStartDate, formattedEndDate, formattedComStartDate, formattedComEndDate);

        if(vm.periodType.period === 'periodToDate') {
          vm.xAxisLabels.startDates.push(vm.periodToDateLabels[index]);
        } else {
          vm.xAxisLabels.startDates.push(formattedStartDate);
        }

        tempDataPoints.push(dataPoint);
      });

      vm.dataPoints = tempDataPoints;
      
      function changeLabels() {
        if (vm.xAxisLabels.geographyNames.length > 1) {
          if (vm.xAxisLabels.geographyNames[0] !== vm.xAxisLabels.geographyNames[1]) {
            vm.xLabels = vm.xAxisLabels.geographyNames;
            vm.showGeographyNames = true;
          }
          else {
            vm.xLabels = vm.xAxisLabels.startDates;
            vm.showGeographyNames = false;
          }
        }
        else {
          vm.xLabels = vm.xAxisLabels.startDates;
          vm.showGeographyNames = false;
        }
      }
      changeLabels();
      updateWidget(vm.marketIndexPoints, vm.orgIndexPoints, changeChartType(selectedType));
    }

    function buildDataPoint(item, formattedStartDate, formattedEndDate, formattedComStartDate, formattedComEndDate) {

      return {
        periodtype:item.periodType,
        geographytype: item.geographytype,
        geographyname: item.geographyname,
        categoryname:item.categoryname,
        marketIndexData: item.marketindexchange,
        orgIndexData: item.orgindexchange,
        zeroMarketIndex:item.noMarketIndex,
        zeroOrgIndex:item.noOrgIndex,
        startDates: formattedStartDate,
        endDates: formattedEndDate,
        comStartDates: formattedComStartDate,
        comEndDates: formattedComEndDate,
        marketCssClass: getCssClass(item.marketindexchange),
        orgCssClass: getCssClass(item.orgindexchange),
        marketPrefix: getPrefix(item.marketindexchange),
        orgPrefix: getPrefix(item.orgindexchange)
      }
    }

    function getPrefix(change) {
      if(change >= 0) {
        return '+';
      }

      return '';
    }

    function getCssClass(change) {
      if(change >= 0) {
        return 'positive';
      }

      return 'negative'
    }

    function toogleTableShow() {
      vm.showTable = !vm.showTable
    }

    function toolTipFormatter() {
      var body = '<b>' + this.x + '</b>';
      $.each(this.points, function () {
        body += '<table>' +
          '<tr>' +
          '<td style=color:' + this.series.color + '>' + this.series.name + ':</td>' +
          '<td><b>' + $filter('formatNumber')(this.y, 2, vm.numberFormatName) + ' %</b></td>' +
          '</tr>' +
          '</table>'
      });

      return body;
    }

    function updateWidget(marketIndexData, orgIndexData, chartType) {
      vm.chartConfig = {
        options: {
          plotOptions: {
            column: {
              pointPadding: 5,
              borderWidth: 1
            }
          },
          chart: {
            type: chartType,
            height: 340,
            style: {
              'fontFamily': '\'Source Sans Pro\', sans-serif'
            },
            events: {
              load: function (event) {
                event.target.redraw();
              }
            }
          },
          tooltip: {
            shared: true,
            useHTML: true,
            backgroundColor: 'rgba(255, 255, 255, 0.8)',
            borderColor: '#e5e5e5',
            shadow: false,
            formatter : toolTipFormatter
          }
        },
        title: {
          text: ''
        },
        xAxis: {
          categories: vm.xLabels
        },
        yAxis: [
          {
            labels: {},
            title: {
              text: ''
            },
            allowDecimals: false,
            gridLineDashStyle: 'Dot',
            gridLineColor: '#b0b0b0',
            plotLines: [{
              color: '#b0b0b0',
              width: 2,
              value: 0,
              dashStyle: 'ShortDot',
              zIndex: 3
            }]
          }
        ],
        series: [{
          name: translation.marketIndex,
          data: marketIndexData,
          pointWidth: 25,
          color: '#84b412'
        }]
      };

      if(vm.showOrgIndex === true) {
        var orgSeries = {
          name: translation.orgIndex,
          data: orgIndexData,
          pointWidth: 25,
          color: '#09acfb'
        };

        vm.chartConfig.series.push(orgSeries);
      }

      vm.isLoading = false;
    }

    function buildPeriodToDateLabels(translations) {
      vm.periodToDateLabels = [];
      vm.periodToDateLabels.push(translations.ytd);
      vm.periodToDateLabels.push(translations.qtd);
      vm.periodToDateLabels.push(translations.mtd);
      vm.periodToDateLabels.push(translations.wtd);
    }

    function displayError(error) {
      console.error(error);
      vm.isLoading = false;
      vm.requestFailed = true;
      vm.hasData = true;
    }
  }
})();
