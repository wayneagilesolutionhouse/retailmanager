(function() {
  'use strict';

  angular.module('shopperTrak.profiling', [
    'shopperTrak.config'
  ])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('requestInterceptor');
  });
})();
