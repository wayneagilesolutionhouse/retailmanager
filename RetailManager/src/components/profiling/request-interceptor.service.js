(function() {
  'use strict';

  angular.module('shopperTrak.profiling')
  .factory('requestInterceptor', ['$q', '$rootScope', 'apiUrl', 'session', 'requestInfo', function($q, $rootScope, apiUrl, session, requestInfo) {

    function isApiRequest(url) {
      return url.substr(0, apiUrl.length) === apiUrl;
    }
    return {
      
      request: function(config) {
        if(!isApiRequest(config.url)) {
          return config;
        }

        requestInfo.startRequest(config.url, config.params);

        return config;
      },
      response: function(response) {
        if(!isApiRequest(response.config.url)) {
          return response;
        }
        
        requestInfo.endRequest(response.config.url, response.config.params, response.status, response.data);

        return response;
      },
      responseError: function(rejection) {
        if(!isApiRequest(rejection.config.url)) {
          return $q.reject(rejection);
        }
        requestInfo.endRequest(rejection.config.url, rejection.config.params, rejection.status);
        
        return $q.reject(rejection);
      }
    };
  }]);
})();
