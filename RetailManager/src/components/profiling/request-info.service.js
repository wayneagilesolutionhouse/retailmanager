
(function() {
  'use strict';

  angular.module('shopperTrak.profiling')
  .service('requestInfo',
  ['apiUrl', '$location', function(apiUrl, $location) {

    var requestInfo = [];

    function startRequest(url, params) {
      var request = {
        source: $location.$$absUrl,
        url: getUrl(url, params),
        endpoint: getEndPoint(url),
        startTime: Date.now(),
        duration: null
      };

      requestInfo.sourcePage = 
      requestInfo.push(request);
    }

    function endRequest(url, params, status, data) {
      var searchUrl = getUrl(url, params);

      var search = {url: searchUrl, duration: null};

      var request = _.findWhere(requestInfo, search);

      request.endTime = Date.now();

      request.duration = request.endTime - request.startTime;

      request.duration = (request.duration / 1000);

      request.status = status;

      request.count = _.where(requestInfo, {url: searchUrl}).length;

      if(typeof data !== 'undefined' && typeof data.result !== 'undefined') {
        request.responseLength = data.result.length;
      }
    }

    function listRequests() {
      var csvContent = buildCsvString();

      var blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });

      var link = document.createElement('a');

      if (link.download !== undefined) { // feature detection
          // Browsers that support HTML5 download attribute
          var url = URL.createObjectURL(blob);
          link.setAttribute('href', url);
          link.setAttribute('download', 'requests.csv');
          link.style.visibility = 'hidden';
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
    }

    function buildCsvString() {
      var csvString = 'SourcePage,Url,StartTime,EndTime,Duration,Status,ResponseLength,Count\r\n';
      _.each(requestInfo, function(req) {
        csvString += '"' + req.source + '",';
        csvString += '"' + req.url + '",';
        csvString += '"' + req.startTime + '",';
        csvString += '"' + req.endTime + '",';
        csvString += '"' + req.duration + '",';
        csvString += '"' + req.status + '",';
        csvString += '"' + req.responseLength + '",';
        csvString += '"' + req.count + '",';
        csvString += '"' + '"\r\n';
      });
      return csvString;
    }

    function getUrl(url, params) {
      if(typeof params === 'undefined') {
        return url;
      }

      return url + '?' +
        Object.keys(params)
        .sort()
        .map(function(key) {

          if(Array.isArray(params[key])) {
            var param = '';

            _.each(params[key], function(item) {
              if(param.length > 0) {
                param += '&';
              }
              param += (key + '=' + item);
            });

            return param;
          }

          return key + '=' + params[key];
        })
        .join('&');
    }

    function getEndPoint(url) {
      return url.replace(apiUrl, '');
    }

    //Run the following in your browser to view the request info:
    //angular.element(document.querySelector('html')).injector().get('requestInfo').listRequests();

    return {
      startRequest: startRequest,
      endRequest: endRequest,
      listRequests: listRequests
    };
  }]);
})();
