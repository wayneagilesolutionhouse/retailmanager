(function() {
  'use strict';

  angular.module('shopperTrak.locationSelector', [
    'mgcrea.ngStrap',
    'shopperTrak.constants',
    'shopperTrak.resources'
  ]);
})();
