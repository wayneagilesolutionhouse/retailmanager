(function() {
  'use strict';

  angular.module('shopperTrak.resources', [
    'shopperTrak.utils',
    'shopperTrak.config'
  ]);
})();
